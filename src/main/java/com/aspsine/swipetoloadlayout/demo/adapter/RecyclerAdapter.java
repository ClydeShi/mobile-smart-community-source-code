package com.aspsine.swipetoloadlayout.demo.adapter;

import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.aspsine.swipetoloadlayout.demo.model.Character;
import com.aspsine.swipetoloadlayout.demo.model.Section;
import android.util.Log;
import com.util.common.StrEdit;
import com.zhyq.R;
import com.zhyq.model.SysPassValue;
import com.squareup.picasso.CircleTransformation;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aspsine on 2015/9/9.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM_TYPE_HEADER = 0;
    private static final int ITEM_TYPE_CONTENT = 1;
    private static final int TYPE_CHILD = 2;

    //private final List<Character> mHeroes;

    private final List<Section> mSections;

    private final List<Integer> mGroupPositions;

    private LoopViewPagerAdapter mPagerAdapter;

    protected OnGroupItemClickListener mOnGroupItemClickListener;

    protected OnGroupItemLongClickListener mOnGroupItemLongClickListener;

    protected OnChildItemClickListener mOnChildItemClickListener;

    protected OnChildItemLongClickListener mOnChildItemLongClickListener;

    private final int mType;

    private List<SysPassValue> list=null;
    private List<SysPassValue> listHeader=null;

    private View.OnClickListener onClick1;

    private int mHeaderCount=1;//头部View个数

    public RecyclerAdapter(int type, List<SysPassValue> datas, List<SysPassValue> datasHeader) {
        mType = type;
        //mHeroes = new ArrayList<>();//轮播
        mSections = new ArrayList<>();
        mGroupPositions = new ArrayList<>();

        this.list = datas;
        this.listHeader = datasHeader;
    }

    public void setList(List<Character> heroes, List<Section> sections) {
        //mHeroes.clear();
        mSections.clear();
        //mHeroes.addAll(heroes);//去掉轮播图
        append(sections);
    }

    public void append(List<Section> sections) {
        mSections.addAll(sections);
        //notifyDataSetChanged();
        //initGroupPositions();
    }

    @Override
    public int getItemCount() {
        return mHeaderCount + getContentItemCount();
    }

    public void setOnClick1(View.OnClickListener onClick1){
        this.onClick1 = onClick1;
    }

    public int getContentItemCount(){
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (mHeaderCount != 0 && position < mHeaderCount) {
            //头部View
            return ITEM_TYPE_HEADER;
        } else {
            //内容View
            return ITEM_TYPE_CONTENT;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        if (viewType ==ITEM_TYPE_HEADER) {

            Log.d("luktel","onCreateViewHolder");
            //Log.d("luktel","listHeader.size"+listHeader.size());

            return new HorizontalViewHolder(R.layout.swipetoloadlayout_header_recycler, viewGroup, viewType);//测试recycler面板

            /*View headerView = null;
            headerView = inflate(viewGroup, R.layout.swipetoloadlayout_header_recycler);//item_wg_task_header,如果增加了headview，其高度不能为0
            HeaderViewHolder itemViewHolder=new HeaderViewHolder(headerView);
            return itemViewHolder;*/

            //return new HeaderViewHolder(mLayoutInflater.inflate(R.layout.item_wg_task_header, parent, false));
        } else if (viewType == ITEM_TYPE_CONTENT) {

            View itemView = null;
            itemView = inflate(viewGroup, R.layout.item_wg_feedback_recyclerlist);
            ItemViewHolder itemViewHolder=new ItemViewHolder(itemView);
            return itemViewHolder;

            //return  new ContentViewHolder(mLayoutInflater.inflate(R.layout.item_wg_task_feedbacklist, parent, false));
        }

        return null;

        //return new GroupHolder(itemView);

    }

    private View inflate(ViewGroup parent, int layoutRes) {
        return LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {//数据的绑定显示
        //if (holder instanceof HeaderViewHolder) {
        if (holder instanceof HorizontalViewHolder) {//测试recycler面板

            //SysPassValue datagrid=(SysPassValue)list.get(position);

            ((HorizontalViewHolder) holder).refreshData(listHeader, position);
            Log.d("luktel","onBindViewHolder");

            //holder.refreshData(list, position);

            /*CommonData taskdetail=data;
            String varValue1 = taskdetail.getVarValue1();
            String varValue2 = taskdetail.getVarValue2();
            String varValue3 = taskdetail.getVarValue3();
            String varValue4 = taskdetail.getVarValue4();
            String varValue5 = taskdetail.getVarValue5();
            String varValue6 = taskdetail.getVarValue6();
            String varValue7 = taskdetail.getVarValue7();
            String varValue8 = taskdetail.getVarValue8();
            String varValue9 = taskdetail.getVarValue9();
            String varValue10 = taskdetail.getVarValue10();*/
            //((HeaderViewHolder) holder).htextView1.setText("test123");
            /*((HeaderViewHolder) holder).htextView2.setText(varValue3);
            ((HeaderViewHolder) holder).htextView3.setText(varValue4);
            ((HeaderViewHolder) holder).htextView4.setText(varValue5);
            ((HeaderViewHolder) holder).htextView5.setText(varValue6);
            ((HeaderViewHolder) holder).htextView6.setText(varValue7);
            ((HeaderViewHolder) holder).htextView7.setText(varValue8);
            ((HeaderViewHolder) holder).htextView8.setText(varValue9);
            ((HeaderViewHolder) holder).htextView9.setText(varValue10);*/
            //Log.i("longhua","varValue2 = "+varValue2);

        } else if(holder instanceof ItemViewHolder) {
            SysPassValue datagrid=(SysPassValue)list.get(position - mHeaderCount);

            String sid =  datagrid.getId();
            String arg1 =  datagrid.getArg1();
            String arg2 =  datagrid.getArg2();
            String arg3 =  datagrid.getArg3();
            String arg4 =  datagrid.getArg4();

            ((ItemViewHolder)holder).item_tv1.setText(StrEdit.StringLeft(arg1, 20));
            ((ItemViewHolder)holder).item_tv2.setText(StrEdit.StringLeft(arg2, 20));
            ((ItemViewHolder)holder).item_tv3.setText(arg3);
            ((ItemViewHolder)holder).item_tv4.setText(arg4);
            ((ItemViewHolder)holder).item_btn1.setOnClickListener(onClick1);
            ((ItemViewHolder)holder).item_btn1.setTag(position - mHeaderCount);
            //holder.itemView.setTag(position);
        }
    }

    private void onBindViewPagerHolder(ViewPagerHolder holder) {
        /*if (holder.viewPager.getAdapter() == null) {
            mPagerAdapter = new LoopViewPagerAdapter(holder.viewPager, holder.indicators);
            holder.viewPager.setAdapter(mPagerAdapter);
            holder.viewPager.addOnPageChangeListener(mPagerAdapter);
            holder.viewPager.setBackgroundDrawable(holder.itemView.getResources().getDrawable(R.mipmap.bg_viewpager));
            mPagerAdapter.setList(mHeroes);
        } else {
            mPagerAdapter.setList(mHeroes);
        }*/
    }

    private void onBindGroupHolder(GroupHolder holder, int parentPosition) {
        holder.tvGroup.setText(mSections.get(parentPosition).getName());
    }

    private void onBindChildHolder(ChildHolder holder, int parentPosition, int childPosition) {
        Character character = mSections.get(parentPosition).getCharacters().get(childPosition);
        holder.tvName.setText(character.getName());
        Resources resources = holder.itemView.getResources();
        int size = resources.getDimensionPixelOffset(R.dimen.hero_avatar_size);
        int width = resources.getDimensionPixelOffset(R.dimen.hero_avatar_border);
        Picasso.with(holder.itemView.getContext())
                .load(character.getAvatar())
                .resize(size, size)
                .transform(new CircleTransformation(width))
                .into(holder.ivAvatar);
    }

    int getGroupPosition(int position) {
        int groupPosition = 1;
        for (int i = mGroupPositions.size() - 1; i >= 0; i--) {
            if (position >= mGroupPositions.get(i)) {
                groupPosition = i;
                break;
            }
        }
        return groupPosition;
    }

    int getChildPosition(int position) {
        int groupPosition = getGroupPosition(position);
        int absGroupPosition = mGroupPositions.get(groupPosition);
        int childPositionInGroup = position - absGroupPosition - 1;
        return childPositionInGroup;
    }

    public void start() {
        if (mPagerAdapter != null) {
            mPagerAdapter.start();
        }
    }

    public void stop() {
        if (mPagerAdapter != null) {
            mPagerAdapter.stop();
        }
    }

    static class ViewPagerHolder extends RecyclerView.ViewHolder {
        //ViewPager viewPager;
        //ViewGroup indicators;

        public ViewPagerHolder(View itemView) {
            super(itemView);
            //viewPager = (ViewPager) itemView.findViewById(R.id.viewPager);
            //indicators = (ViewGroup) itemView.findViewById(R.id.indicators);
        }
    }

    static class GroupHolder extends RecyclerView.ViewHolder {
        TextView tvGroup;

        public GroupHolder(View itemView) {
            super(itemView);
            tvGroup = (TextView) itemView.findViewById(R.id.tvHeader);
        }
    }

    static class ChildHolder extends RecyclerView.ViewHolder {
        ImageView ivAvatar;
        TextView tvName;

        public ChildHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            ivAvatar = (ImageView) itemView.findViewById(R.id.ivAvatar);
        }
    }

    //头部 ViewHolder
    public static class HeaderViewHolder extends RecyclerView.ViewHolder {
        private TextView htextView1;
        private TextView htextView2;
        private TextView htextView3;
        private TextView htextView4;
        private TextView htextView5;
        private TextView htextView6;
        private TextView htextView7;
        private TextView htextView8;
        private TextView htextView9;
        public HeaderViewHolder(View itemView) {
            super(itemView);
            htextView1=(TextView)itemView.findViewById(R.id.wg_taskdetail_layout2_textview);
            htextView2=(TextView)itemView.findViewById(R.id.wg_taskdetail_layout3_textview);
            htextView3=(TextView)itemView.findViewById(R.id.wg_taskdetail_layout4_textview);
            htextView4=(TextView)itemView.findViewById(R.id.wg_taskdetail_layout5_textview);
            htextView5=(TextView)itemView.findViewById(R.id.wg_taskdetail_layout6_textview);
            htextView6=(TextView)itemView.findViewById(R.id.wg_taskdetail_layout7_textview);
            htextView7=(TextView)itemView.findViewById(R.id.wg_taskdetail_layout8_textview);
            htextView8=(TextView)itemView.findViewById(R.id.wg_taskdetail_layout9_textview);
            htextView9=(TextView)itemView.findViewById(R.id.wg_taskdetail_layout10_textview);
        }
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        public TextView item_tv1;
        public TextView item_tv2;
        public TextView item_tv3;
        public TextView item_tv4;
        public Button item_btn1;
        public ItemViewHolder(View view){
            super(view);
            item_tv1 = (TextView) view.findViewById(R.id.item_tv1);
            item_tv2 = (TextView) view.findViewById(R.id.item_tv2);
            item_tv3 = (TextView) view.findViewById(R.id.item_tv3);
            item_tv4 = (TextView) view.findViewById(R.id.item_tv4);
            item_btn1 = (Button) view.findViewById(R.id.item_btn1);
        }
    }

    private int screenWidth;//屏幕宽度

    private int HORIZONTAL_VIEW_X = 0;//水平RecyclerView滑动的距离

    //----------------------Holder----------------------------

    /**
     * 嵌套的水平RecyclerView
     * 当条目被回收时，下次加载会重新回到之前的x轴
     */
    private class HorizontalViewHolder extends BaseHolder<List<SysPassValue>> {
        private RecyclerView hor_recyclerview;

        private List<SysPassValue> data;

        private int scrollX;//纪录X移动的距离

        private boolean isLoadLastState = false;//是否加载了之前的状态


        public HorizontalViewHolder(int viewId, ViewGroup parent, int viewType) {
            super(viewId, parent, viewType);
            hor_recyclerview = (RecyclerView) itemView.findViewById(R.id.swipe_target);
            //为了保存移动距离，所以添加滑动监听
            hor_recyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    //每次条目重新加载时，都会滑动到上次的距离
                    if (!isLoadLastState) {
                        isLoadLastState = true;
                        hor_recyclerview.scrollBy(HORIZONTAL_VIEW_X, 0);
                    }
                    //dx为每一次移动的距离，所以我们需要做累加操作
                    scrollX += dx;
                }
            });
        }

        @Override
        public void refreshData(List<SysPassValue> data, int position) {
            this.data = data;
            ViewGroup.LayoutParams layoutParams = hor_recyclerview.getLayoutParams();
            //高度等于＝条目的高度＋ 10dp的间距 ＋ 10dp（为了让条目居中）
            //layoutParams.height = screenWidth / 3 + dip2px(20);
            layoutParams.height = screenWidth / 3 + 100;
            hor_recyclerview.setLayoutParams(layoutParams);
            //hor_recyclerview.setLayoutManager(new LinearLayoutManager(RyRyActivity.this, LinearLayoutManager.HORIZONTAL, false));
            hor_recyclerview.setBackgroundResource(R.color.colorAccent);
            hor_recyclerview.setAdapter(new HorizontalAdapter());
            Log.d("luktel","HorizontalViewHolder refreshData");

        }

        /**
         * 在条目回收时调用，保存X轴滑动的距离
         */
        public void saveStateWhenDestory() {
            HORIZONTAL_VIEW_X = scrollX;
            isLoadLastState = false;
            scrollX = 0;
        }


        private class HorizontalAdapter extends RecyclerView.Adapter<BaseHolder> {

            @Override
            public BaseHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                return new ItemViewHolder2(R.layout.item_wg_task_feedbacklist, parent, viewType);
            }

            @Override
            public void onBindViewHolder(BaseHolder holder, int position) {//不知道为什么没有调用
                holder.refreshData(data.get(position), position);
                Log.d("luktel","ddtt");



                TextView htextView1=(TextView)itemView.findViewById(R.id.wg_feedbacklist_tv1);

                htextView1.setText("test123456");
            }

            @Override
            public int getItemCount() {
                return data.size();
            }
        }
    }

    /**
     * 通用子条目hodler
     */
    private class ItemViewHolder2 extends BaseHolder<List<SysPassValue>> {

        public TextView item_tv1;
        private ImageView imageview_item;

        public ItemViewHolder2(int viewId, ViewGroup parent, int viewType) {
            super(viewId, parent, viewType);
            //imageview_item = (ImageView) itemView.findViewById(R.id.wg_feedbacklist_tv1);
            item_tv1 = (TextView) itemView.findViewById(R.id.wg_feedbacklist_tv1);
            ViewGroup.LayoutParams layoutParams = imageview_item.getLayoutParams();
            layoutParams.width = layoutParams.height = screenWidth / 3;
            //imageview_item.setLayoutParams(layoutParams);
            item_tv1.setLayoutParams(layoutParams);


        }

        @Override
        public void refreshData(List<SysPassValue> data, final int position) {
            //imageview_item.setBackgroundResource(data);

            Log.d("luktel","ItemViewHolder2");

            TextView htextView1=(TextView)itemView.findViewById(R.id.wg_feedbacklist_tv1);
            htextView1.setText("refresh123456");

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(RyRyActivity.this, "position:" + position, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }


}
