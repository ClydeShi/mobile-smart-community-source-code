package com.aspsine.swipetoloadlayout.demo.adapter;

import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.aspsine.swipetoloadlayout.demo.model.Character;
import com.aspsine.swipetoloadlayout.demo.model.Section;
import com.util.common.StrEdit;;
import com.zhyq.R;
import com.zhyq.model.SysPassValue;
import com.squareup.picasso.CircleTransformation;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aspsine on 2015/9/9.
 */
public class SimpleRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_VIEWPAGER = 0;
    private static final int TYPE_GROUP = 1;
    private static final int TYPE_CHILD = 2;

    //private final List<Character> mHeroes;

    private final List<Section> mSections;

    private final List<Integer> mGroupPositions;

    private LoopViewPagerAdapter mPagerAdapter;

    protected OnGroupItemClickListener mOnGroupItemClickListener;

    protected OnGroupItemLongClickListener mOnGroupItemLongClickListener;

    protected OnChildItemClickListener mOnChildItemClickListener;

    protected OnChildItemLongClickListener mOnChildItemLongClickListener;

    private final int mType;

    private List<SysPassValue> list=null;

    private View.OnClickListener onClick1;

    public SimpleRecyclerAdapter(int type, List<SysPassValue> datas) {
        mType = type;
        //mHeroes = new ArrayList<>();//轮播
        mSections = new ArrayList<>();
        mGroupPositions = new ArrayList<>();

        this.list = datas;
    }

    public void setList(List<Character> heroes, List<Section> sections) {
        //mHeroes.clear();
        mSections.clear();
        //mHeroes.addAll(heroes);//去掉轮播图
        append(sections);
    }

    public void append(List<Section> sections) {
        mSections.addAll(sections);
        //notifyDataSetChanged();
        //initGroupPositions();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setOnClick1(View.OnClickListener onClick1){
        this.onClick1 = onClick1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View itemView = null;
        itemView = inflate(viewGroup, R.layout.item_wg_feedback_recyclerlist);
        ItemViewHolder itemViewHolder=new ItemViewHolder(itemView);
        return itemViewHolder;
        //return new GroupHolder(itemView);

    }

    private View inflate(ViewGroup parent, int layoutRes) {
        return LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {//数据的绑定显示
        if(holder instanceof ItemViewHolder) {
            SysPassValue datagrid=(SysPassValue)list.get(position);

            String sid =  datagrid.getId();
            String arg1 =  datagrid.getArg1();
            String arg2 =  datagrid.getArg2();
            String arg3 =  datagrid.getArg3();
            String arg4 =  datagrid.getArg4();

            ((ItemViewHolder)holder).item_tv1.setText(StrEdit.StringLeft(arg1, 20));
            ((ItemViewHolder)holder).item_tv2.setText(StrEdit.StringLeft(arg2, 20));
            ((ItemViewHolder)holder).item_tv3.setText(arg3);
            ((ItemViewHolder)holder).item_tv4.setText(arg4);
            //((ItemViewHolder)holder).item_btn1.setOnClickListener(onClick1);
            //((ItemViewHolder)holder).item_btn1.setTag(position);
            holder.itemView.setTag(position);
        }
    }

    private void onBindViewPagerHolder(ViewPagerHolder holder) {
        /*if (holder.viewPager.getAdapter() == null) {
            mPagerAdapter = new LoopViewPagerAdapter(holder.viewPager, holder.indicators);
            holder.viewPager.setAdapter(mPagerAdapter);
            holder.viewPager.addOnPageChangeListener(mPagerAdapter);
            holder.viewPager.setBackgroundDrawable(holder.itemView.getResources().getDrawable(R.mipmap.bg_viewpager));
            mPagerAdapter.setList(mHeroes);
        } else {
            mPagerAdapter.setList(mHeroes);
        }*/
    }

    private void onBindGroupHolder(GroupHolder holder, int parentPosition) {
        holder.tvGroup.setText(mSections.get(parentPosition).getName());
    }

    private void onBindChildHolder(ChildHolder holder, int parentPosition, int childPosition) {
        Character character = mSections.get(parentPosition).getCharacters().get(childPosition);
        holder.tvName.setText(character.getName());
        Resources resources = holder.itemView.getResources();
        int size = resources.getDimensionPixelOffset(R.dimen.hero_avatar_size);
        int width = resources.getDimensionPixelOffset(R.dimen.hero_avatar_border);
        Picasso.with(holder.itemView.getContext())
                .load(character.getAvatar())
                .resize(size, size)
                .transform(new CircleTransformation(width))
                .into(holder.ivAvatar);
    }

    int getGroupPosition(int position) {
        int groupPosition = 1;
        for (int i = mGroupPositions.size() - 1; i >= 0; i--) {
            if (position >= mGroupPositions.get(i)) {
                groupPosition = i;
                break;
            }
        }
        return groupPosition;
    }

    int getChildPosition(int position) {
        int groupPosition = getGroupPosition(position);
        int absGroupPosition = mGroupPositions.get(groupPosition);
        int childPositionInGroup = position - absGroupPosition - 1;
        return childPositionInGroup;
    }

    public void start() {
        if (mPagerAdapter != null) {
            mPagerAdapter.start();
        }
    }

    public void stop() {
        if (mPagerAdapter != null) {
            mPagerAdapter.stop();
        }
    }

    static class ViewPagerHolder extends RecyclerView.ViewHolder {
        //ViewPager viewPager;
        //ViewGroup indicators;

        public ViewPagerHolder(View itemView) {
            super(itemView);
            //viewPager = (ViewPager) itemView.findViewById(R.id.viewPager);
            //indicators = (ViewGroup) itemView.findViewById(R.id.indicators);
        }
    }

    static class GroupHolder extends RecyclerView.ViewHolder {
        TextView tvGroup;

        public GroupHolder(View itemView) {
            super(itemView);
            tvGroup = (TextView) itemView.findViewById(R.id.tvHeader);
        }
    }

    static class ChildHolder extends RecyclerView.ViewHolder {
        ImageView ivAvatar;
        TextView tvName;

        public ChildHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            ivAvatar = (ImageView) itemView.findViewById(R.id.ivAvatar);
        }
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        public TextView item_tv1;
        public TextView item_tv2;
        public TextView item_tv3;
        public TextView item_tv4;
        public Button item_btn1;
        public ItemViewHolder(View view){
            super(view);
            item_tv1 = (TextView) view.findViewById(R.id.item_tv1);
            item_tv2 = (TextView) view.findViewById(R.id.item_tv2);
            item_tv3 = (TextView) view.findViewById(R.id.item_tv3);
            item_tv4 = (TextView) view.findViewById(R.id.item_tv4);
            item_btn1 = (Button) view.findViewById(R.id.item_btn1);
        }
    }


}
