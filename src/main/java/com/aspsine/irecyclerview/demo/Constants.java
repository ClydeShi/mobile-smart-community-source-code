package com.aspsine.irecyclerview.demo;

/**
 * Created by aspsine on 16/4/6.
 * https://raw.githubusercontent.com/Aspsine/IRecyclerView/master/app/src/main/assets/api/superman_vs_batman/gallery/1.json
 */
public class Constants {
    private static final String GITHUB = "https://raw.githubusercontent.com/";
    private static final String AUTHOR = "Aspsine";
    private static final String BRANCH = "master";
    private static final String REPOSITORY = "IRecyclerView";
    private static final String ASSETS = "/app/src/main/assets";
    private static final String API = "api/superman_vs_batman/gallery";
    private static final String ENDPOINT = GITHUB + AUTHOR + "/" + REPOSITORY + "/" + BRANCH + ASSETS + "/" + API;

    public static final String BannerAPI = ENDPOINT + "/banner.json";

    //private static final String url = "http://test666.luktel.com/";
    private static final String url = "http://121.35.253.146:8418/";

    public static String ImagesAPI(int page) {
        return ENDPOINT + "/" + page + ".json";
    }

    public static String DataAPI(int page) {
        return url + "app.jsp?task=getFeedbackList&page="+page;
    }

}
