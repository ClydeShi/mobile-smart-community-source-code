package com.aspsine.irecyclerview.demo.ui.adapter;

import android.view.View;

/*
通用OnItemClickListener，考虑到要设置列表里面的ViewHolder，所以把OnItemClickListener接口定义到adapter里面了
 */

public interface OnItemClickListener<T> {
    void onItemClick(int position, T t, View v);
}
