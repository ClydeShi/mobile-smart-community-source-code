package com.aspsine.irecyclerview.demo.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.aspsine.irecyclerview.IViewHolder;
import com.util.common.StrEdit;
import com.zhyq.R;
import com.aspsine.irecyclerview.demo.model.Image;
import com.zhyq.model.SysPassValue;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aspsine on 16/4/5.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<IViewHolder> {

    private List<Image> mImages;

    private OnItemClickListener<Image> mOnItemClickListener;
    private List<SysPassValue> list=null;
    private View.OnClickListener onClick1;

    public RecyclerAdapter(List<SysPassValue> datas) {
        mImages = new ArrayList<>();
        this.list = datas;
    }

    public void setOnClick1(View.OnClickListener onClick1){
        this.onClick1 = onClick1;
    }

    public void setOnItemClickListener(OnItemClickListener<Image> listener) {
        this.mOnItemClickListener = listener;
    }

    public void setList(List<Image> images) {
        mImages.clear();
        append(images);
    }

    public void append(List<Image> images) {
        int positionStart = mImages.size();
        int itemCount = images.size();
        mImages.addAll(images);
        if (positionStart > 0 && itemCount > 0) {
            notifyItemRangeInserted(positionStart, itemCount);
        } else {
            notifyDataSetChanged();
        }
    }

    public void remove(int position) {
        mImages.remove(position);
        notifyItemRemoved(position);
    }

    public void clear(){
        mImages.clear();
        notifyDataSetChanged();
    }

    @Override
    public IViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Log.d("luktel", "onCreateViewHolder");
        View view=LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_irecyclerview_item_list, parent, false);
        //ImageView imageView = (ImageView) LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_image_item, parent, false);

        final ViewHolder holder = new ViewHolder(view);
        /*
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int position = holder.getIAdapterPosition();
                final Image image = mImages.get(position);
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(position, image, v);
                }
            }
        });
        */
        return holder;
    }

    @Override
    public void onBindViewHolder(IViewHolder holder, int position) {
        //Log.d("luktel", "onBindViewHolder");
        //ImageView imageView = (ImageView) holder.itemView;
        //Image image = mImages.get(position);
        //Glide.with(imageView.getContext()).load(image.image).dontAnimate().into(imageView);

        SysPassValue datagrid=(SysPassValue)list.get(position);

        String sid =  datagrid.getId();
        String arg1 =  datagrid.getArg1();
        String arg2 =  datagrid.getArg2();
        String arg3 =  datagrid.getArg3();
        String arg4 =  datagrid.getArg4();

        //Log.d("luktel", "arg1"+arg1);

        ((RecyclerAdapter.ViewHolder)holder).item_tv1.setText(StrEdit.StringLeft(arg1, 20));
        ((RecyclerAdapter.ViewHolder)holder).item_tv2.setText(StrEdit.StringLeft(arg2, 20));
        ((RecyclerAdapter.ViewHolder)holder).item_tv3.setText(arg3);
        ((RecyclerAdapter.ViewHolder)holder).item_tv4.setText(arg4);
        ((RecyclerAdapter.ViewHolder)holder).item_btn1.setOnClickListener(onClick1);
        ((RecyclerAdapter.ViewHolder)holder).item_btn1.setTag(position);
        holder.itemView.setTag(position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends IViewHolder {

        public TextView item_tv1;
        public TextView item_tv2;
        public TextView item_tv3;
        public TextView item_tv4;
        public Button item_btn1;
        public ViewHolder(View itemView){
            super(itemView);
            item_tv1 = (TextView) itemView.findViewById(R.id.item_tv1);
            item_tv2 = (TextView) itemView.findViewById(R.id.item_tv2);
            item_tv3 = (TextView) itemView.findViewById(R.id.item_tv3);
            item_tv4 = (TextView) itemView.findViewById(R.id.item_tv4);
            item_btn1 = (Button) itemView.findViewById(R.id.item_btn1);
        }

        //public ViewHolder(View itemView) {
        //super(itemView);
        //}
    }
}
