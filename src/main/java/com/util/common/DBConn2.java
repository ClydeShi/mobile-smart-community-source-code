package com.util.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

public class DBConn2 {
	
	private static DataSource dataSource = null;//先使用此代码
	//连接池使用org.springframework.jdbc.datasource.DriverManagerDataSource的时候使用此方式
	//private static DriverManagerDataSource dataSource = null;//https://blog.csdn.net/Crazy_Java1234/article/details/52837550
	//https://blog.csdn.net/myoursky/article/details/41982097
	//https://blog.csdn.net/pengyu1801/article/details/60143810
	//https://blog.csdn.net/u014514528/article/details/73613876
	//https://blog.csdn.net/top_code/article/details/51957718
	
	private Connection conn = null;
	private PreparedStatement prepstmt = null;
	private Statement stmt;
    private ResultSet rs = null;
    private int i = 0;
    
    public DataSource getDataSource() {
    	return dataSource;    	
    }
    
    /*public void setDataSource(DataSource dataSource) {
    	this.dataSource = dataSource;
    }*/
    
    static {
    	try {

        	System.out.println("获取Spring MVC里的连接池");//https://blog.csdn.net/rocklee/article/details/50541057
        	
    	} catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static synchronized Connection getConnection() {
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conn;
    }
    
    public synchronized Connection getNewConnection() {
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conn;
    }
    
    public ResultSet executeQuery(String sql) {
    	rs = null;
    	try {
    		if(conn==null || conn.isClosed())
    			conn = getConnection();
    		stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rs = stmt.executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("DBUtil executeQuery: " + e.getMessage());
		}
    	
    	return rs;
    }
    
    public int executeUpdate(String sql) {
    	Statement stmt;
		try {
			if(conn==null || conn.isClosed())
				conn = getConnection();			
			stmt = conn.createStatement();
			i = stmt.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("DBUtil executeUpdate: " + e.getMessage());
		}
        
        return i;
    }
    
    public int executeUpdateAutoCommit(String sql) {
    	int intValue = 0;
    	try {
    		//Connection conn = null;
    		//Statement stmt = null;
    		conn = getConnection();
    		conn.setAutoCommit(false);
    		stmt = conn.createStatement();
    		
    		stmt.execute(sql);
    		conn.commit();  //数据库操作最终提交给数据库
			conn.setAutoCommit(true);
			intValue = 1;
    		
        } catch (SQLException ex) {
        	intValue = 0;
    		try {
				conn.rollback();
			} catch (SQLException e) {
				e.printStackTrace();
			}
    		System.out.println("DBUtil executeUpdateAutoCommit:" + ex.getMessage());
        }
        return intValue;
    }
    
    public void close() {
    	if (rs != null) {
    		try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
    	}
    	if (stmt != null) {
    		try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
    	}
    	if (conn != null) {
    		try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
    	}
    }
    
}
