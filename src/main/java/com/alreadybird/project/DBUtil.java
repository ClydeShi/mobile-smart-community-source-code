package com.alreadybird.project;

import java.sql.*;
import org.apache.log4j.Logger;

public class DBUtil {
	private Connection conn = null;//for prepstmt
	private PreparedStatement prepstmt = null;//for prepstmt
	private Statement stmt;
    private ResultSet rs = null;
    private final String connSource = "proxool.wgblink";
    static Logger logger = Logger.getLogger(DBUtil.class.getName());
    private int i = 0;
    
    public DBUtil() {
    }
    
    public void getConnection() {
		conn = null;
		try {
			conn = DriverManager.getConnection(connSource);
		} catch (SQLException ex) {
        	if(logger.isInfoEnabled()){
        		logger.error(ex.getMessage());
        	}
        }
    }
    
    public Connection getNewConnection() {
    	conn = null;
		try {
			conn = DriverManager.getConnection(connSource);
		} catch (SQLException ex) {
        	if(logger.isInfoEnabled()){
        		logger.error(ex.getMessage());
        	}
        }
        return conn;
    }
    
    public ResultSet executeQuery(String sql) {
    	rs = null;
    	try {
    		if(conn==null || conn.isClosed())
                getConnection();
            stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);            
            rs = stmt.executeQuery(sql);
        } catch (SQLException ex) {
        	if(logger.isInfoEnabled()){
        		logger.error("sql:" + sql + "message:" + ex.getMessage());
        	}
        	System.out.println("executeUpdate: " + ex.getMessage());
        }
        return rs;
    }
    
    public ResultSet executeQueryUpdatableQuery(String sql) {/*可更新结果的查询操作*/
    	rs = null;
    	try {
    		if (conn == null || conn.isClosed())
                getConnection();
            Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);        
            rs = stmt.executeQuery(sql);    	
    	} catch (SQLException ex) {
    		if(logger.isInfoEnabled()){
        		logger.error("sql:" + sql + "message:" + ex.getMessage());
        	}
    		System.out.println("executeUpdate: " + ex.getMessage());
        }
        return rs;
    }
    
    public int executeUpdate(String sql) {
    	try {
    		if (conn == null || conn.isClosed())
                getConnection();
            Statement stmt = conn.createStatement();    	
            i = stmt.executeUpdate(sql);
        } catch (SQLException ex) {
        	if(logger.isInfoEnabled()){
        		logger.error("sql:" + sql + "message:" + ex.getMessage());
        	}
        	System.out.println("executeUpdate: " + ex.getMessage());
        }
        return i;
    }
    
    public int executeUpdateAutoCommit(String sql) {
    	int intValue = 0;
    	try {
    		//Connection conn = null;
    		//Statement stmt = null;
    		conn = this.getNewConnection();
    		conn.setAutoCommit(false);
    		stmt = conn.createStatement();
    		
    		stmt.execute(sql);
    		conn.commit();  //数据库操作最终提交给数据库
			conn.setAutoCommit(true);
			intValue = 1;
    		
        } catch (SQLException ex) {
        	intValue = 0;
    		try {
				conn.rollback();
			} catch (SQLException e) {
				e.printStackTrace();
			}
    		System.out.println("executeUpdate异常" + ex.getMessage());
        	if(logger.isInfoEnabled()){
        		logger.error("sql:" + sql + "message:" + ex.getMessage());
        	}
        }
        return intValue;
    }
    
	/*for prepareStatement use*/
	public void prepareStatement(String sql){
         try{
             prepstmt = this.conn.prepareStatement(sql);
         }
         catch(Exception e){
        	 if(logger.isInfoEnabled()){
        		 logger.error("sql:" + sql + "message:" + e.getMessage());
        	 }
         }
    }
	
	public void setString(int index,String value){
        try{
            //value = new String(value.getBytes("GBK"), "ISO8859_1");
            prepstmt.setString(index, value);
        }
        catch(Exception e){
        	if(logger.isInfoEnabled()){
        		logger.error("DBUtil setString: " + e.getMessage());
        	}
        }
    }
	
	public void setInt(int index,int value){
        try{
            prepstmt.setInt(index, value);
        }
        catch(Exception e){
        	if(logger.isInfoEnabled()){
        		logger.error("DBUtil setInt: " + e.getMessage());
        	}
            System.out.println("DBUtil setInt: " + e.getMessage());
        }
    }
	
	public void clearParameters(){
        try{
            prepstmt.clearParameters();
            prepstmt=null;
        }
        catch(Exception e){
        	if(logger.isInfoEnabled()){
        		logger.error("DBUtil clearParameters: " + e.getMessage());
        	}
            System.out.println("DBUtil clearParameters: " + e.getMessage());
        }
    }
	
	public PreparedStatement getPreparedStatement(){
        return prepstmt;
    }
	
	public ResultSet executeQuery(){
		ResultSet rs=null;
		try{
			if(prepstmt!= null){
				rs = prepstmt.executeQuery();
			}
		}catch(Exception e){
			if(logger.isInfoEnabled()){
        		logger.error("DBUtil executeQuery prepstmt: " + e.getMessage());
        	}
			System.out.println("DBUtil executeQuery prepstmt: " + e.getMessage());
		}
		
		return rs;
	}
	
	public int executeUpdate(){
		try{
			if(prepstmt!=null){
				i=prepstmt.executeUpdate();
			}
		}catch(Exception e){
			if(logger.isInfoEnabled()){
        		logger.error("DBUtil executeUpdate prepstmt: " + e.getMessage());
        	}
			System.out.println("DBUtil executeUpdate prepstmt: " + e.getMessage());
		}
		
		return i;
	}
	
	/*prepare end*/
	
	public void close() {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException ex) {
            }
            rs = null;
        }
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ex1) {
            }
            stmt = null;
        }
        if (conn != null) {
            try {
            	conn.close();
            } catch (SQLException ex2) {
            }
            conn = null;
        }
    }
	
	
	
}
