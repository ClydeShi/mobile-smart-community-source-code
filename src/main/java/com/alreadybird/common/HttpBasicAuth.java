package com.alreadybird.common;

import com.oreilly.servlet.Base64Encoder;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpBasicAuth {
	private static String userName = "eventWebApiUser";
	private static String userPassword = "lhcg2017api";
	
	public HttpBasicAuth() {
	}
	
	public static String getAuthContent(String aurl) {
		
		String result = "";
        try {
            URL url = new URL (aurl);
            String encoding = Base64Encoder.encode (userName+":"+userPassword);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            //connection.setRequestMethod("POST");
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);
            connection.setRequestProperty ("Authorization", "Basic " + encoding);
            /*
            InputStreamReader bis = new InputStreamReader(connection.getInputStream(),"utf-8");
            int c = 0;
            while((c = bis.read()) != -1){
                result = result + (char)c;
            }
            */
            
            InputStreamReader bis = new InputStreamReader(connection.getInputStream(),"utf-8");
            BufferedReader in = new BufferedReader(bis);
            String line;
            while ((line = in.readLine()) != null) {
            	result = result + line;
            }
            in.close();
            bis.close();
            connection.disconnect();
        } catch(Exception e) {
            e.printStackTrace();
        }
        return result;
    }
	
	public static InputStream getAuthStream(String aurl) {
		
		InputStream ism = null;
        try {
            URL url = new URL (aurl);
            String encoding = Base64Encoder.encode (userName+":"+userPassword);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            //connection.setRequestMethod("POST");
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);
            connection.setRequestProperty ("Authorization", "Basic " + encoding);
            ism = connection.getInputStream();
        } catch(Exception e) {
            e.printStackTrace();
        }
        return ism;
    }
	
	public static String getAuthHtml(String aurl) {
		
		String strTemp = "";
        try {
            URL url = new URL (aurl);
            String encoding = Base64Encoder.encode (userName+":"+userPassword);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(10000);
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);
            connection.setRequestProperty ("Authorization", "Basic " + encoding);
            InputStreamReader bis = new InputStreamReader(connection.getInputStream(),"utf-8");
            BufferedReader in = new BufferedReader(bis);
            String line;
            while ((line = in.readLine()) != null) {
            	strTemp = strTemp + line;
            }
            in.close();
            bis.close();
            connection.disconnect();
            strTemp = StrEdit.Replace(strTemp,"utf-16","utf-8");
        } catch(Exception e) {
            e.printStackTrace();
        }
        return strTemp;
    }
	
	public static String getHtml(String tempurl,String code){
		try{
			URL url = new URL(tempurl);
			HttpURLConnection conn = (HttpURLConnection)url.openConnection();
			conn.connect();
			InputStream is = conn.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is,code));
			String line="";
			StringBuffer resultBuffer = new StringBuffer();
			while((line = br.readLine())!=null){
				resultBuffer.append(line);
			}
			br.close();
			is.close();
			conn.disconnect();
			return resultBuffer.toString();
		} catch (Exception e) {
            e.printStackTrace();
        }
		return null;
	}
	
	
	
}
