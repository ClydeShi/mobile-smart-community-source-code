package com.alreadybird.common;

import org.xml.sax.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import java.io.*;

public class DomBean implements java.io.Serializable {
	private static String xmlStr = "";
	private static final String PATH = "file:///";
	
	public DomBean() {
	}
	
	public String getString() {
		return xmlStr;
	}
	
	public static Document getDocument(String filename) throws Exception {
		xmlStr = "";
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		// 设定解析的叁数
		dbf.setIgnoringComments(true);
		dbf.setIgnoringElementContentWhitespace(true);
		DocumentBuilder db = dbf.newDocumentBuilder();
		//导入XML文件
		//Document doc = db.parse(PATH+filename);
		Document doc = db.parse(filename);
		
		return doc;
	}
	
	public static Document getDocument(InputStream ism) throws Exception {
		xmlStr = "";
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		// 设定解析的叁数
		dbf.setIgnoringComments(true);
		dbf.setIgnoringElementContentWhitespace(true);
		DocumentBuilder db = dbf.newDocumentBuilder();
		//导入XML文件
		//Document doc = db.parse(PATH+filename);
		Document doc = db.parse(ism);
		
		return doc;
	}
	
	public void traverseTree(Node node) throws Exception {
        if(node == null) {
           return;
        }
        int type = node.getNodeType();

        switch (type) {
           // handle document nodes
           case Node.DOCUMENT_NODE: {
              xmlStr+="<tr>";
              traverseTree(((Document)node).getDocumentElement());
              break;
           }
           // handle element nodes
           case Node.ELEMENT_NODE: {
              String elementName = node.getNodeName();
              if(elementName.equals("item")) {
                xmlStr+="</tr><tr>";
              }
              NodeList childNodes = node.getChildNodes();
              if(childNodes != null) {
                 int length = childNodes.getLength();
                 for (int loopIndex = 0; loopIndex < length ; loopIndex++)
                 {
                    traverseTree(childNodes.item(loopIndex));
                 }
              }
              break;
           }
           // handle text nodes
           case Node.TEXT_NODE: {
              String data = node.getNodeValue().trim();
              if(data.length() > 0) {
                xmlStr+="<td>"+data+"</td>";
              }
           }
        }
	}
	
	
	
}
