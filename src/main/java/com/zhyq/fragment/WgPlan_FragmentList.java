package com.zhyq.fragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
 
 
import java.util.Date;
import java.util.List;

import com.handmark.pulltorefresh.library.ILoadingLayout;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshGridView;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.zhyq.HhApplication;
import com.zhyq.WgPlanViewActivity;

import com.zhyq.R;

import com.zhyq.adapter.WgPlanListAdapter;

import com.zhyq.libs.AndroidUtils;

import com.zhyq.model.SysPassValue;
import com.zhyq.model.User;

import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;

import com.zhyq.task.impl.sendOrderToServerForListYTask;

import android.annotation.SuppressLint;
import android.content.Intent;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
 
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;



@SuppressLint("ValidFragment") 
public class WgPlan_FragmentList extends Fragment implements OnItemClickListener,OnClickListener {
	
	public static final String BUNDLE_TITLE = "TITLES";
	private String newstitle = "全部";
	private String fenlei = "All";
 
	private String lastRefreshdt = "";
	
	private PullToRefreshGridView mPullRefreshGridView;//3列
	private PullToRefreshListView mPullRefreshListView;//1列
	
	private View view = null;
    private WgPlanListAdapter mAdapter;//数据适配器
    private List<SysPassValue> NewsList = new ArrayList<SysPassValue>();//数据
	private int currentPage = 1; 
   
    public WgPlan_FragmentList() {
		super();
		this.newstitle = "All";
	}
    
    public void setFenLei(String fenlei) {
 		this.fenlei = fenlei;
 		onRefresh();
 	}
    
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		AndroidUtils.removeParentView(view);
		if (view==null) {
			view = inflater.inflate(R.layout.tab_item_fragment_main_pulltorefresh, null);
		}
		
		Bundle b = getArguments();
		newstitle = b.getString(BUNDLE_TITLE);//传递过来的栏目
		//Log.i("longhua",newstitle);
		currentPage = 1;
		return  view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
	}
	
	@Override
	public void onResume() {
		
		/*
		 *  列表布局，用uniteListAdapter类读取并设置触发，传递列表布局内容过去item_wg_plan_list，并把内容NewsList设置过去，注意布局里的文本图片按钮要设置android:tag=""序号
		 */
		mAdapter = new WgPlanListAdapter(getActivity(),NewsList,"item_wg_plan_list");
		mAdapter.setOnClick1(this);//设置列表里的按钮点击事件
		
		/*
         * 初始化
         */
		//mXGridView = (XGridView) getView().findViewById(R.id.id_activity_xGridView);
	    
		//mPullRefreshGridView = (PullToRefreshGridView) getView().findViewById(R.id.id_activity_pullrefreshGridView);
		mPullRefreshListView = (PullToRefreshListView) getView().findViewById(R.id.id_activity_pullrefreshlistView);
		
		//初始化值
		initIndicator();
		//设置下拉，上移动 得到效果
		mPullRefreshListView.setOnRefreshListener(new OnRefreshListener2<ListView>()
        {
        	//下拉效果
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView)
            {
            	if(lastRefreshdt.equals("")) {
        	    	// 显示最后更新的时间
                    refreshView.getLoadingLayoutProxy().setLastUpdatedLabel("刚刚");  
       	        } else {
       	        	// 显示最后更新的时间
                    refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(lastRefreshdt);
       	        }
            	
            	SimpleDateFormat    formatter    =   new    SimpleDateFormat    ("MM月dd日    HH:mm:ss     ");       
       	        Date    curDate    =   new    Date(System.currentTimeMillis());//获取当前时间       
       	        lastRefreshdt =    formatter.format(curDate);
       	        
                currentPage = 1; 
           		LoadDatasTask(); 
            }

            //上拉效果
            @Override  
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView)
            {
                currentPage++;
            	LoadDatasTask();
            }
        });
		
		//获取控件并注册  这个是获取 GridView 控件
        //GridView actualGridView = mPullRefreshListView.getRefreshableView();
        //registerForContextMenu(actualGridView); 
        //actualGridView.setAdapter(mAdapter);
        
		//设置列表数据
		mPullRefreshListView.setAdapter(mAdapter);
		
        //进来时直接刷新
		onRefresh();
		super.onResume();
	}
	
	private void onRefresh(){
		mPullRefreshListView.setRefreshing(true);
        //首次加载任务数据 加载任务  
        currentPage=1; 
   		LoadDatasTask(); 
	}
	
	private void initIndicator(){
		ILoadingLayout startLabels = mPullRefreshListView.getLoadingLayoutProxy(true,false);//加载提示 pull_to_refresh_header_vertical.xml里面设置字体大小
		startLabels.setPullLabel("下拉刷新...");// 刚下拉时，显示的提示
		startLabels.setRefreshingLabel("更新中...");// 刷新时
		startLabels.setReleaseLabel("松开刷新...");// 下来达到一定距离时，显示的提示
		
		ILoadingLayout endLabels = mPullRefreshListView.getLoadingLayoutProxy(false,true);
		endLabels.setPullLabel("上拉刷新...");// 刚上拉时，显示的提示
		endLabels.setRefreshingLabel("更新中...");// 刷新时
		endLabels.setReleaseLabel("松开刷新...");// 上来达到一定距离时，显示的提示
		endLabels.setLastUpdatedLabel("");
	}
	
	/*
     * 记载数据的异步任务
     * 获取列表数据
     */  
	public void LoadDatasTask () {
		
		Long userid = null;
		User curuser = new User();
		curuser = HhApplication.getInstance(this.getActivity()).getHhCart().getUser();
		if(curuser==null){
			userid =-1l;
		}else{
			userid = HhApplication.getInstance(this.getActivity()).getHhCart().getUser().getId();
		}
		 
		SysPassValue temp = new SysPassValue();
		temp.setFunctionName("getPlanList");//得到计划列表
		temp.setPageNumber(String.valueOf(this.currentPage));
		temp.setArg1(String.valueOf(userid));
		temp.setArg2(this.newstitle);//栏目未执行 执行中 已完成
		temp.setArg3(fenlei);
		
		new sendOrderToServerForListYTask(new AbstractYGetTaskListener<List<SysPassValue>>() {
			@Override
			public void onPostExecute(String name, YGetTask<List<SysPassValue>> task) { 
				 		
				//loading.hideLoading();
				getData(task.getValue());
				mPullRefreshListView.onRefreshComplete();
			}
		},temp).execute();
	}
	
	public void getData(List<SysPassValue> list) {
		
		//通过数据适配器获取 数据列表
		View vnorecord = view.findViewById(R.id.activity_no_record_showpic);
		if (list==null){
			if(currentPage==1){
				this.NewsList.clear(); 
	    		mAdapter.notifyDataSetChanged();
	    		vnorecord.setVisibility(View.VISIBLE);
	    		return;
			}
			return;
		}
		vnorecord.setVisibility(View.GONE);
		
		//如果是第一页则需要清楚数据   currentPage
		if(currentPage==1){
    		this.NewsList.clear();
			this.NewsList.addAll(list);
    	} else{
			NewsList.addAll(list);
		}
		mAdapter.notifyDataSetChanged();
		
	}
	
	@Override
	public void onClick(View v) {
		
		if(R.id.wg_plan_list_btn==v.getId()){
			//ToastUtils.show(v.getContext(), "计划CLICK！");
			
			//如果使用UniteListAdapter
			//Object tag = v.getTag(R.id.button_tag1);
			//if (tag != null && tag instanceof Integer)
			
			Object tag = v.getTag();//传递参数过去
			
			if (tag != null) {
				
				int position = (Integer) tag;//点击button所在Item中的位置，通过这个位置就可以得到Item中的值
				String planId =  NewsList.get(position).getId();
				String planName =  NewsList.get(position).getArg1();
				String planStatus =   NewsList.get(position).getArg5();
				
			    //Log.i("longhua","onClick"+planName);
			    Intent in=new Intent(v.getContext(),WgPlanViewActivity.class);
				in.putExtra("planId", planId);
				in.putExtra("planName", planName);
				in.putExtra("planStatus", planStatus);
				in.putExtra("classType", this.newstitle);
				this.startActivity(in);
				(this.getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);//设置新面板右边进入，原来面板左边退出
			}
		}
		
	}
	
	//该函数的作用是 直接 打开对应的连接
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		Log.i("longhua","onItemClick");
	}
	 
	 
}
