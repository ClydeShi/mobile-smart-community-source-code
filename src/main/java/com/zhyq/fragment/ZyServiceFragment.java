package com.zhyq.fragment;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.zhyq.R;
import com.zhyq.WgCaptureActivity;
import com.zhyq.WgContactViewActivity;
import com.zhyq.WgLoginActivity;
import com.zhyq.ZyCleanAddActivity;
import com.zhyq.ZyMeetingRoomListActivity;
import com.zhyq.ZyMyPersonal;
import com.zhyq.ZyPictureActivity;
import com.zhyq.ZyVisitorAddActivity;
import com.zhyq.libs.AndroidUtils;
import com.zhyq.libs.PreferenceUtils;
import com.zhyq.libs.ProgersssDialog;
import com.zhyq.model.SysPassValue;
import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.sendOrderToServerForValueYTask;


public class ZyServiceFragment extends Fragment implements OnItemClickListener,OnClickListener {
	private View view = null;
	public static final int guest_login_result = 0;
	private boolean logined = false;
	private View includeTitle;

	private Long userId = null;
	private String userCode = "";
	private String userName = "";
	private String userType = "";
	private String userRole = "";

	private String companyId = "";

	public ProgersssDialog dialog;
	private final int SCANER_CODE = 1;

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		AndroidUtils.removeParentView(view);
		if (view==null) {
			view=inflater.inflate(R.layout.fragment_zy_service, null);
		}

		showProgressDialog();

		includeTitle = view.findViewById(R.id.zy_service_head);
		((TextView)includeTitle.findViewById(R.id.util_my_head_title)).setText("服务");
		Button btnReturn = (Button)includeTitle.findViewById(R.id.util_my_head_btn_return);
		btnReturn.setOnClickListener(this);

		View util_my_head_relativelayout1 = (View)includeTitle.findViewById(R.id.util_my_head_relativelayout1);
		util_my_head_relativelayout1.setVisibility(View.INVISIBLE);

		View util_my_head_relativelayout2 = (View)includeTitle.findViewById(R.id.util_my_head_relativelayout2);
		util_my_head_relativelayout2.setVisibility(View.INVISIBLE);

		/*Button btnSet = (Button)includeTitle.findViewById(R.id.util_my_head_btn_set);
		btnSet.setOnClickListener(this);

		((Button)view.findViewById(R.id.wg_update_btn)).setOnClickListener(this);
		((Button)view.findViewById(R.id.wg_logout_btn)).setOnClickListener(this);

		((Button)view.findViewById(R.id.util_my_head_btn_set)).setOnClickListener(this);
		((Button)view.findViewById(R.id.my_btn_list_8)).setOnClickListener(this);*/

		userId = PreferenceUtils.getLong(getActivity(), "userid", -1);
		userCode = PreferenceUtils.getString(getActivity(),"usercode","");
		userName = PreferenceUtils.getString(getActivity(),"username","");
		userType = PreferenceUtils.getString(getActivity(),"usertype","");
		userRole = PreferenceUtils.getString(getActivity(),"userrole","");
		//Log.i("luktel","userId"+userId);
		if (userId==-1) {
			AndroidUtils.start(getActivity(), WgLoginActivity.class);
			getActivity().finish();
			((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
		}

		Button service_btn1 =(Button)view.findViewById(R.id.service_btn1);
		service_btn1.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				userId = PreferenceUtils.getLong(getActivity(), "userid", -1);//重新获取登录值
				if (userId==-1) {
					Toast.makeText(v.getContext(), "请先登入,谢谢！", Toast.LENGTH_SHORT).show();
					return;
				}

				Intent intent=new Intent(v.getContext(), ZyCleanAddActivity.class);
				Bundle bundle=new Bundle();
				bundle.putString("channelId", "2");
				intent.putExtras(bundle);
				startActivity(intent);
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				return;
			}
		});

		Button service_btn2 =(Button)view.findViewById(R.id.service_btn2);
		service_btn2.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				userId = PreferenceUtils.getLong(getActivity(), "userid", -1);//重新获取登录值
				if (userId==-1) {
					Toast.makeText(v.getContext(), "请先登入,谢谢！", Toast.LENGTH_SHORT).show();
					return;
				}

				Intent intent=new Intent(v.getContext(), ZyCleanAddActivity.class);
				Bundle bundle=new Bundle();
				bundle.putString("channelId", "1");
				intent.putExtras(bundle);
				startActivity(intent);
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				return;
			}
		});

		Button service_btn3 =(Button)view.findViewById(R.id.service_btn3);
		service_btn3.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;
			}
		});

		Button service_btn4 =(Button)view.findViewById(R.id.service_btn4);
		service_btn4.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				userId = PreferenceUtils.getLong(getActivity(), "userid", -1);//重新获取登录值
				if (userId==-1) {
					Toast.makeText(v.getContext(), "请先登入,谢谢！", Toast.LENGTH_SHORT).show();
					return;
				}
				Intent intent=new Intent(v.getContext(), ZyVisitorAddActivity.class);
				Bundle bundle=new Bundle();
				bundle.putString("channelId", "1");
				intent.putExtras(bundle);
				startActivity(intent);
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				return;
			}
		});

		Button service_btn5 =(Button)view.findViewById(R.id.service_btn5);
		service_btn5.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;
			}
		});

		Button service_btn6 =(Button)view.findViewById(R.id.service_btn6);
		service_btn6.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;
			}
		});

		Button service_btn7 =(Button)view.findViewById(R.id.service_btn7);
		service_btn7.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;
			}
		});

		Button service_btn8 =(Button)view.findViewById(R.id.service_btn8);
		service_btn8.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;
			}
		});

		Button service_btn9 =(Button)view.findViewById(R.id.service_btn9);
		service_btn9.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;
			}
		});

		Button service_btn10 =(Button)view.findViewById(R.id.service_btn10);
		service_btn10.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;
			}
		});

		Button service_btn11 =(Button)view.findViewById(R.id.service_btn11);
		service_btn11.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				userId = PreferenceUtils.getLong(getActivity(), "userid", -1);//重新获取登录值
				if (userId==-1) {
					Toast.makeText(v.getContext(), "请先登入,谢谢！", Toast.LENGTH_SHORT).show();
					return;
				}
				Intent intent=new Intent(v.getContext(), ZyMeetingRoomListActivity.class);
				Bundle bundle=new Bundle();
				bundle.putString("channelId", "2");
				intent.putExtras(bundle);
				startActivity(intent);
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				return;
			}
		});

		Button service_btn12 =(Button)view.findViewById(R.id.service_btn12);
		service_btn12.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				userId = PreferenceUtils.getLong(getActivity(), "userid", -1);//重新获取登录值
				if (userId==-1) {
					Toast.makeText(v.getContext(), "请先登入,谢谢！", Toast.LENGTH_SHORT).show();
					return;
				}
				Intent intent=new Intent(v.getContext(), ZyMeetingRoomListActivity.class);
				Bundle bundle=new Bundle();
				bundle.putString("channelId", "1");
				intent.putExtras(bundle);
				startActivity(intent);
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				return;
			}
		});

		Button service_btn13 =(Button)view.findViewById(R.id.service_btn13);
		service_btn13.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();

				userId = PreferenceUtils.getLong(getActivity(), "userid", -1);//重新获取登录值
				if (userId==-1) {
					Toast.makeText(v.getContext(), "请先登入,谢谢！", Toast.LENGTH_SHORT).show();
					return;
				}
				Intent intent=new Intent(v.getContext(), ZyPictureActivity.class);
				Bundle bundle=new Bundle();
				bundle.putString("channelId", "1");
				intent.putExtras(bundle);
				startActivity(intent);
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				return;
			}
		});

		Button service_btn14 =(Button)view.findViewById(R.id.service_btn14);
		service_btn14.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;
			}
		});

		Button service_btn15 =(Button)view.findViewById(R.id.service_btn15);
		service_btn15.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;
			}
		});

		Button service_btn16 =(Button)view.findViewById(R.id.service_btn16);
		service_btn16.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;
			}
		});

		Button service_btn17 =(Button)view.findViewById(R.id.service_btn17);
		service_btn17.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;
			}
		});

		Button service_btn18 =(Button)view.findViewById(R.id.service_btn18);
		service_btn18.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;
			}
		});

		Button service_btn19 =(Button)view.findViewById(R.id.service_btn19);
		service_btn19.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;
			}
		});

		Button service_btn20 =(Button)view.findViewById(R.id.service_btn20);
		service_btn20.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;
			}
		});

		Button service_btn21 =(Button)view.findViewById(R.id.service_btn21);
		service_btn21.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;
			}
		});

		Button service_btn22 =(Button)view.findViewById(R.id.service_btn22);
		service_btn22.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;
			}
		});

		Button service_btn23 =(Button)view.findViewById(R.id.service_btn23);
		service_btn23.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;
			}
		});

		Button service_btn24 =(Button)view.findViewById(R.id.service_btn24);
		service_btn24.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;
			}
		});

		Button service_btn25 =(Button)view.findViewById(R.id.service_btn25);
		service_btn25.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;
			}
		});

		Button service_btn26 =(Button)view.findViewById(R.id.service_btn26);
		service_btn26.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;
			}
		});

		Button service_btn27 =(Button)view.findViewById(R.id.service_btn27);
		service_btn27.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;
			}
		});

		Button service_btn28 =(Button)view.findViewById(R.id.service_btn28);
		service_btn28.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;
			}
		});

		Button service_btn29 =(Button)view.findViewById(R.id.service_btn29);
		service_btn29.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;
			}
		});

		Button service_btn30 =(Button)view.findViewById(R.id.service_btn30);
		service_btn30.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;
			}
		});

		Button service_btn31 =(Button)view.findViewById(R.id.service_btn31);
		service_btn31.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;
			}
		});

		new Handler().postDelayed(new Runnable() {//延迟读取数据
			@Override
			public void run() {
				dialog.dismiss();
			}
		}, 200);

		new Handler().postDelayed(new Runnable() {//延迟读取数据
			@Override
			public void run() {
				dialog.dismiss();
				//setLoginInfo();
			}
		}, 200);

		return view;
	}

	@Override
	public void onResume() {//开始执行onCreateView然后onResume，返回的时候执行onResume
		super.onResume();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	public void setLoginInfo() {

		TextView my_personal_name = (TextView) view.findViewById(R.id.my_personal_name);
		TextView my_personal_company = (TextView) view.findViewById(R.id.my_personal_company);

		if (userId==-1) {
			dialog.dismiss();
			View my_personal_linearLayout_login =view.findViewById(R.id.my_personal_linearLayout_login);
			my_personal_linearLayout_login.setVisibility(View.VISIBLE);
			my_personal_name.setVisibility(View.VISIBLE);
			my_personal_company.setVisibility(View.INVISIBLE);
			my_personal_name.setText("请登录");
		} else {

			View my_personal_linearLayout_login =view.findViewById(R.id.my_personal_linearLayout_login);
			my_personal_linearLayout_login.setVisibility(View.INVISIBLE);
			/*my_personal_name.setVisibility(View.VISIBLE);
			my_personal_company.setVisibility(View.VISIBLE);
			my_personal_name.setText(userName);
			my_personal_company.setText(userCode);*/

			getData();

		}

	}
	 
	@Override
	public void onClick(View view) {

		if(R.id.btn_my_btn_login==view.getId()){//目前是登陆后才能进入主页
			//Log.i("luktel","login on");
			Intent intent = new Intent(getActivity(),WgLoginActivity.class);
			startActivityForResult(intent, guest_login_result);
			((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			return;
		}

		if (R.id.wg_head_main_btn==view.getId()) {
			Login();
		}

		if(R.id.util_my_head_btn_set==view.getId()){

			Intent intent=new Intent(view.getContext(), ZyMyPersonal.class);
			Bundle bundle=new Bundle();
			bundle.putString("id", "");
			intent.putExtras(bundle);
			startActivity(intent);
			this.getActivity().overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			return;
		}

		if(R.id.my_btn_list_8==view.getId()){

			Intent intent=new Intent(view.getContext(), ZyMyPersonal.class);
			Bundle bundle=new Bundle();
			bundle.putString("id", "");
			intent.putExtras(bundle);
			startActivity(intent);
			this.getActivity().overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			return;
		}

		return;
	}

	public void Login(){
		Intent intent = new Intent(getActivity(),WgLoginActivity.class);
		startActivityForResult(intent, guest_login_result);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		//Log.i("luktel","resultcode="+String.valueOf(resultCode)+"  requestCode="+String.valueOf(requestCode));

		if (resultCode == 9) {
			if (requestCode == SCANER_CODE) {
				Bundle bundle = data.getExtras();
				String scanResult = bundle.getString("result");

				Intent intent=new Intent(view.getContext(), WgContactViewActivity.class);
				Bundle bundle1=new Bundle();
				bundle1.putString("contactId", scanResult);
				intent.putExtras(bundle1);
				startActivity(intent);
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				return;
			}
		}
	}
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		if(arg3<0) {
	        // 点击的是headerView或者footerView
	        return;
		}
	}

	public void showProgressDialog(){
		dialog = new ProgersssDialog(this.getActivity());
		//dialog.show();
	}

	protected void getData() {
		SysPassValue temp = new SysPassValue();
		temp.setFunctionName("getUserDetails");
		temp.setId(String.valueOf(userId));
		temp.setUserId(String.valueOf(userId));

		new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
			@Override
			public void onPostExecute(String name, YGetTask<SysPassValue> task) {
				showData(task.getValue());
				dialog.dismiss();
			}
		},temp).execute();
	}

	private void showData(SysPassValue passValue) {
		if (passValue!=null) {

			String sid =  passValue.getId();
			String arg1 =  passValue.getArg1();
			String arg2 =  passValue.getArg2();
			String arg3 =  passValue.getArg3();
			String arg4 =  passValue.getArg4();
			String arg5 =  passValue.getArg5();
			String arg6 =  passValue.getArg6();
			String arg7 =  passValue.getArg7();
			String arg8 =  passValue.getArg8();
			String arg9 =  passValue.getArg9();
			String arg10 =  passValue.getArg10();
			String arg11 =  passValue.getArg11();
			String arg12 =  passValue.getArg12();
			String arg13 =  passValue.getArg13();
			String arg14 =  passValue.getArg14();
			String arg15 =  passValue.getArg15();
			String arg16 =  passValue.getArg16();
			String arg17 =  passValue.getArg17();
			String arg18 =  passValue.getArg18();
			String arg19 =  passValue.getArg19();
			String arg20 =  passValue.getArg20();
			String arg38 =  passValue.getArg38();
			String arg42 =  passValue.getArg42();

			TextView my_personal_name = (TextView) view.findViewById(R.id.my_personal_name);
			TextView my_personal_company = (TextView) view.findViewById(R.id.my_personal_company);
			my_personal_name.setText(arg3);
			my_personal_company.setText(arg20);

			String faceImageUrl = arg38;
			companyId = arg42;

			if(!faceImageUrl.equals("")){
				View v1 = view.findViewById(R.id.zy_my_personinfo_img);
				AndroidUtils.setWebImageCircleView(v1, R.id.zy_my_personinfo_img,faceImageUrl);
			}

		}
	}
	
	
	
}
