package com.zhyq.fragment;



import android.os.AsyncTask;
import android.widget.ScrollView;
import com.util.common.StrEdit;

import com.zhyq.WgCaptureActivity;
import com.zhyq.WgPlanNewActivity;
import com.zhyq.WgTaskNewActivity;
import com.zhyq.ZyNewsListActivity;
import com.zhyq.ZyScanDeviceResult;
import com.zhyq.ZyWorkActivity;
import com.zhyq.adapter.areaLevelClassAdapter;
import com.zhyq.libs.AndroidUtils;
import com.zhyq.libs.MyListView;
import com.zhyq.libs.PreferenceUtils;

import android.app.Activity;


import com.zhyq.WgChartZongHeActivity;
import com.zhyq.WgContactActivity;
import com.zhyq.WgContactViewActivity;
import com.zhyq.WgEventActivity;
import com.zhyq.WgFeedbackNewActivity;
import com.zhyq.WgLoginActivity;
import com.zhyq.ZyProblemAddActivity;
import com.zhyq.WgPlanActivity;

import com.zhyq.R;
import android.os.Bundle;  
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment; 
import android.util.DisplayMetrics;
import android.util.Log;
 
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener; 
import android.view.ViewGroup; 
import android.widget.AdapterView; 
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import android.widget.TextView;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;

/*
 * 主页面板
 * 读取布局，设置监听
 * 在onResume里通过setLoginInfo设置登录信息
 */

public class WgHomeFragmentHome extends Fragment implements OnClickListener,OnItemClickListener {
	private View view = null;
	private TextView tv_city = null;
	private final int SCANER_CODE = 1;
	public static final int guest_login_result = 0;
	
	private long userId = 0;
	private String userCode = "";
	private String userName = "";
	private String userRole = "";
	
	private PopupWindow popupWindow;
	
	//下面是二级菜单
	private MyListView mainlistView;//主类菜单 
	private areaLevelClassAdapter mainAdapter;//主要类数据适配器 
	String[] mainClass;//一级的文字 大类文字
	int images_all = R.drawable.ic_category_0;// 不限对应右边的 小图片
	int images_other = R.drawable.select_right;//  其他类右边的小图片对应的是向右的箭头
	
	private View includeTitle;
	private boolean logined = false;
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		AndroidUtils.removeParentView(view);
		if (view==null) {
			 
			view=inflater.inflate(R.layout.fragment_zy_home, null);
			//view.findViewById(R.id.wg_my_login).setOnClickListener(this);
			//view.findViewById(R.id.wg_head_main_btn).setOnClickListener(this);
		}

		/*includeTitle = view.findViewById(R.id.wg_main_head);
		((TextView)includeTitle.findViewById(R.id.tv_wg_head_add_title)).setText("智慧园区管理系统");*/

		((TextView)view.findViewById(R.id.zhuye_text_date)).setText(StrEdit.getDate("yyyy-MM-dd")+" "+StrEdit.getWeeks());
		
		/*
		User user = HhApplication.getInstance(this.getActivity()).getHhCart().getUser();
		if(user==null){
			logined = false;
		}else{
			logined = true;
		}
		*/
		
		//userId = PreferenceUtils.getString(getActivity(),"userid","");
		userId = PreferenceUtils.getLong(getActivity(), "userid", -1);
		userRole = PreferenceUtils.getString(getActivity(),"userrole","");
		//Log.i("longhua","userId"+userId);

		Button wg_main_btntop1 =(Button)view.findViewById(R.id.wg_main_btntop1);
		wg_main_btntop1.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;

			}
		});

		Button wg_main_btntop2 =(Button)view.findViewById(R.id.wg_main_btntop2);
		wg_main_btntop2.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				userId = PreferenceUtils.getLong(getActivity(), "userid", -1);//重新获取登录值
				if (userId==-1) {
					Toast.makeText(v.getContext(), "请先登入,谢谢！", Toast.LENGTH_SHORT).show();
					return;
				}

				Intent intent=new Intent(v.getContext(), ZyNewsListActivity.class);
				Bundle bundle=new Bundle();
				bundle.putString("channelId", "1");
				intent.putExtras(bundle);
				startActivity(intent);
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				return;
			}
		});

		Button wg_main_btntop3 =(Button)view.findViewById(R.id.wg_main_btntop3);
		wg_main_btntop3.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;

			}
		});

		Button wg_main_btntop4 =(Button)view.findViewById(R.id.wg_main_btntop4);
		wg_main_btntop4.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;

			}
		});

		Button wg_main_btn1 =(Button)view.findViewById(R.id.wg_main_btn1);
		wg_main_btn1.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				
				userId = PreferenceUtils.getLong(getActivity(), "userid", -1);//重新获取登录值
				if (userId==-1) {
					Toast.makeText(v.getContext(), "请先登入,谢谢！", Toast.LENGTH_SHORT).show();
		        	return;
				}

				//ToastUtils.show(v.getContext(), R.string.show_logon_error_msg);

				Intent itplan = new Intent(v.getContext(),ZyWorkActivity.class);//WgPlanNewActivity
				startActivity(itplan);
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				return;
				
				/*Intent intent=new Intent(v.getContext(), WgTaskNewActivity.class);//开始用的是WgTaskActivity2，使用xlistview，切换有点慢，WgTaskActivity使用WgTaskFragmentList
				Bundle bundle=new Bundle();
				bundle.putString("taskType", "我的工单");
				intent.putExtras(bundle);
				startActivity(intent);
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);//设置切换动画，从右边进入，左边退出
				//overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				//AndroidUtils.start(v.getContext(), HhSpecilityShopActivity.class);
				return;*/
			}
		});
		
		Button wg_main_btn2 =(Button)view.findViewById(R.id.wg_main_btn2);
		wg_main_btn2.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;
				
				/*String username = PreferenceUtils.getString(getActivity(),"username","");
				if (username.equals("")) {
					Toast.makeText(v.getContext(), "请先登入,谢谢！", Toast.LENGTH_SHORT).show();
		        	return;
				}
				
				Intent intent=new Intent(v.getContext(), WgMapActivity.class);
				Bundle bundle=new Bundle();
				bundle.putString("taskType", "检查定位");
				intent.putExtras(bundle);
				startActivity(intent);
				//设置切换动画，从右边进入，左边退出
				
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				
				return;*/
			}
		});
		
		/*
		Button wg_main_btn2 =(Button)view.findViewById(R.id.wg_main_btn2);
		wg_main_btn2.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				
				User user = HhApplication.getInstance(v.getContext()).getHhCart().getUser();
				
				if(user==null){
					Toast.makeText(v.getContext(), "请先登入,谢谢！", Toast.LENGTH_SHORT).show();
		        	return;
				}
				
				Intent itpinpai = new Intent(v.getContext(),SPCShouYe_PinPaiActivity.class);
				startActivityForResult(itpinpai, SCANER_CODE);
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left); 
				return;
			  }
			 });
			 */
		
		Button wg_main_btn3 =(Button)view.findViewById(R.id.wg_main_btn3);
		wg_main_btn3.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				
				if (userId==-1) {
					Toast.makeText(v.getContext(), "请先登入,谢谢！", Toast.LENGTH_SHORT).show();
		        	return;
				}

				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;
				
				/*Intent openCameraIntent = new Intent(v.getContext(), WgCaptureActivity.class);
				startActivityForResult(openCameraIntent, SCANER_CODE);//startActivityForResult需要返回数据
				//设置切换动画，从右边进入，左边退出
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left); 
				return;*/
			}
		});
		
		Button wg_main_btn4 =(Button)view.findViewById(R.id.wg_main_btn4);
		wg_main_btn4.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				
				if (userId==-1) {
					Toast.makeText(v.getContext(), "请先登入,谢谢！", Toast.LENGTH_SHORT).show();
		        	return;
				}

				Intent itplan = new Intent(v.getContext(),ZyProblemAddActivity.class);//WgPlanActivity
				startActivity(itplan);
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				return;
				
				/*if (StrEdit.isInclude(userRole,2002)) {
					Toast.makeText(v.getContext(), "您没有此权限！", Toast.LENGTH_SHORT).show();
		        	return;
				}
				
				if (StrEdit.isInclude(userRole,2001)) {
					Intent itplan = new Intent(v.getContext(),WgPlanNewActivity.class);//WgPlanActivity
					startActivity(itplan);
					((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left); 
					return;
				}*/
				
				/*
				Intent intent=new Intent(v.getContext(), WgPlanActivity.class); 
				Bundle bundle=new Bundle();
				bundle.putString("taskType", "检查计划");
				intent.putExtras(bundle);
				startActivity(intent);
				//设置切换动画，从右边进入，左边退出
				
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				//AndroidUtils.start(v.getContext(), HhSpecilityShopActivity.class);
				return;
				*/
			}
		});
		
		Button wg_main_btn5 =(Button)view.findViewById(R.id.wg_main_btn5);
		wg_main_btn5.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				
				if (userId==-1) {
					Toast.makeText(v.getContext(), "请先登入,谢谢！", Toast.LENGTH_SHORT).show();
		        	return;
				}

				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;
				
				/*Intent itfeedback = new Intent(v.getContext(),WgFeedbackNewActivity.class);//WgFeedbackActivity
				startActivity(itfeedback);
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left); 
				return;*/
			}
		});
		
		Button wg_main_btn6 =(Button)view.findViewById(R.id.wg_main_btn6);
		wg_main_btn6.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				
				if (userId==-1) {
					Toast.makeText(v.getContext(), "请先登入,谢谢！", Toast.LENGTH_SHORT).show();
		        	return;
				}

				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;
				
				/*Intent itchart = new Intent(v.getContext(),WgChartZongHeActivity.class);
				startActivity(itchart);
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left); 
				return;*/
			}
		});
		
		Button wg_main_btn7 =(Button)view.findViewById(R.id.wg_main_btn7);
		wg_main_btn7.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				
				if (userId==-1) {
					Toast.makeText(v.getContext(), "请先登入,谢谢！", Toast.LENGTH_SHORT).show();
		        	return;
				}

				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;
				
				/*Intent itevent = new Intent(v.getContext(),WgEventActivity.class);
				startActivity(itevent);
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left); 
				return;*/
			}
		});
		
		Button wg_main_btn8 =(Button)view.findViewById(R.id.wg_main_btn8);
		wg_main_btn8.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				
				if (userId==-1) {
					Toast.makeText(v.getContext(), "请先登入,谢谢！", Toast.LENGTH_SHORT).show();
		        	return;
				}

				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;

				//Intent itcontact = new Intent(v.getContext(),TestSwipetoloadlayoutActivity.class);
				//Intent itcontact = new Intent(v.getContext(),TestIRecyclerActivity.class);
				/*Intent itcontact = new Intent(v.getContext(),WgContactActivity.class);
				startActivity(itcontact);
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				return;*/
			}
		});

		Button wg_main_btn9 =(Button)view.findViewById(R.id.wg_main_btn9);
		wg_main_btn9.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;

			}
		});

		Button wg_main_btn10 =(Button)view.findViewById(R.id.wg_main_btn10);
		wg_main_btn10.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				Intent openCameraIntent = new Intent(v.getContext(), WgCaptureActivity.class);
				startActivityForResult(openCameraIntent, SCANER_CODE);
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				return;

			}
		});

		Button wg_main_btn11 =(Button)view.findViewById(R.id.wg_main_btn11);
		wg_main_btn11.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;

			}
		});

		Button wg_main_btn12 =(Button)view.findViewById(R.id.wg_main_btn12);
		wg_main_btn12.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;

			}
		});

		Button wg_main_btn13 =(Button)view.findViewById(R.id.wg_main_btn13);
		wg_main_btn13.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;

			}
		});

		Button wg_main_btn14 =(Button)view.findViewById(R.id.wg_main_btn14);
		wg_main_btn14.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;

			}
		});

		Button wg_main_btn15 =(Button)view.findViewById(R.id.wg_main_btn15);
		wg_main_btn15.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;

			}
		});

		Button wg_main_btn16 =(Button)view.findViewById(R.id.wg_main_btn16);
		wg_main_btn16.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;

			}
		});

		Button wg_main_btn17 =(Button)view.findViewById(R.id.wg_main_btn17);
		wg_main_btn17.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;

			}
		});

		Button wg_main_btn18 =(Button)view.findViewById(R.id.wg_main_btn18);
		wg_main_btn18.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;

			}
		});
		
		/*
		reSize();
		*/
		
		return view;
	}
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int positon, long arg3) {
		//ToastUtils.show(arg1.getContext(), R.string.show_logon_error_msg);
		
	}
	
	public void reSize(){
		
		//题头的比例尺寸大小
		setHeaderLayout();
		//设置轮播图片的高度
		/*setFlashLayoutHeight();*/
		//功能模块
		//setFunctionLayoutHeight();
		//设置店铺列表的高度
		//直接设置 图片的宽度和高度
		/*int totalsize = HhApplication.getInstance(getActivity()).sysArgument.getShopcount();
		 
		if(totalsize>0){
			setShopLayoutHeight(totalsize);
		}else{
			setShopLayoutHeight(2);
		}*/
	}
	
	@Override
	public void onResume() {
		super.onResume();
		setLoginInfo();
	}
	
	/*
	 * 设置登入人的信息
	 */
    public void setLoginInfo(){
    	TextView tvName = (TextView)view.findViewById(R.id.wg_my_showname);
		/*Button btnLogin = (Button)view.findViewById(R.id.wg_head_main_btn);*/
		
		long puserId = PreferenceUtils.getLong(getActivity(), "userid", -1);//登录成功后，重新获取登录值
		String puserName = PreferenceUtils.getString(getActivity(),"username","");
		
		//Log.i("longhua","setLoginInfo after"+puserId+puserName);
		
		//if (puserName.equals("")) {
		/*if (puserId==-1) {
			tvName.setVisibility(View.VISIBLE);
			btnLogin.setVisibility(View.VISIBLE);
			tvName.setText("请登录");
		} else {
			tvName.setVisibility(View.VISIBLE);
			btnLogin.setVisibility(View.INVISIBLE);
			tvName.setText(puserName);
		}*/
		
		/*
		if (StrEdit.isInclude(userRole,2002)) {
			RelativeLayout rl = (RelativeLayout)view.findViewById(R.id.wg_main_relativelayout4);
			rl.setVisibility(View.INVISIBLE);
		}
		*/
		
		/*
		logined = HhApplication.getInstance(this.getActivity()).getHhCart().isLogined();//注销后不会保留
		 
		if (logined) {
			username = HhApplication.getInstance(this.getActivity()).getHhCart().getUser().getName();
			tvName.setVisibility(View.VISIBLE);
			tvGuestLogin.setVisibility(View.INVISIBLE);
			btnLogin.setVisibility(View.INVISIBLE);
			tvName.setText(username);
		} else {
			tvName.setVisibility(View.INVISIBLE);
			tvGuestLogin.setVisibility(View.VISIBLE);
			btnLogin.setVisibility(View.VISIBLE);
			tvName.setText("");
		}
		*/
    }
    
	public void setHeaderLayout(){
		Log.i("huahua","shouye size test");
		AndroidUtils.setLinearLayoutSize(includeTitle,R.id.wg_main_head,"RelativeLayout","1",
				640,640,71,0,0,0,0);
	}
	
	public void setFunctionLayoutHeight(){
		
		View linelayout_function_list_layout = view.findViewById(R.id.ll_shouye_function_list_layout);
		DisplayMetrics dm = view.getContext().getResources().getDisplayMetrics();//得到屏幕宽度
		int screenWidth = dm.widthPixels;
		int defaultWidth = 640;
		int defaultHeight= 257;
		
		float scale =  (float) defaultWidth / (float) defaultHeight;
		int webHeight=  Math.round(screenWidth / scale);
		
		LinearLayout.LayoutParams params2= (LinearLayout.LayoutParams) linelayout_function_list_layout.getLayoutParams();
		
		params2.width  = screenWidth;
		params2.height = webHeight;
		linelayout_function_list_layout.setLayoutParams(params2);
	}
	
	/*
	 * 设置店铺轮播图片的高度  和轮播图片下面的 充值 布局的大小
	 */
	public void setFlashLayoutHeight(){
		
		com.zhyq.libs.RoomSlideShowView  lbview = (com.zhyq.libs.RoomSlideShowView)view.findViewById(R.id.zhuye_broast_lunbo);
		int defaultWidth = 640;
		int defaultHeight= 242;
		
		//得到屏幕宽度
		DisplayMetrics dm = view.getContext().getResources().getDisplayMetrics();
		int screenWidth = dm.widthPixels;
		//int webWidth = webImg.getMaxWidth();
		
		float scale =  (float) defaultWidth / (float) defaultHeight;
		int webHeight=  Math.round(screenWidth / scale);
		
		LinearLayout.LayoutParams params= (LinearLayout.LayoutParams) lbview.getLayoutParams();
		 
		params.width  = screenWidth;
		params.height = webHeight;
		lbview.setLayoutParams(params);
		
		View linelayout_chongzhi = view.findViewById(R.id.zhuye_chongzhi_grid);
		
		defaultWidth = 640; 
		defaultHeight= 226; 
		
		scale =  (float) defaultWidth / (float) defaultHeight;
		webHeight=  Math.round(screenWidth / scale);
		
		
		LinearLayout.LayoutParams params2= (LinearLayout.LayoutParams) linelayout_chongzhi.getLayoutParams();
		 
		params2.width  = screenWidth;
		params2.height = webHeight;
		linelayout_chongzhi.setLayoutParams(params2);
	}
	
	/*
	 * 设置店铺高度
	 */
	public void setShopLayoutHeight(int sizeofshop){
		
		View linelayout1 = view.findViewById(R.id.hh_linearlayout_shouye_nearby_shop);
		
		int marginpx= AndroidUtils.dip2px(view.getContext(), 5);
		
		int defaultWidth = 640;
		int defaultHeight= 251;
		
		//得到屏幕宽度
		DisplayMetrics dm = view.getContext().getResources().getDisplayMetrics();
		int screenWidth = dm.widthPixels;
		//int webWidth = webImg.getMaxWidth();
		
		float scale =  (float) defaultWidth / (float) defaultHeight;
		int webHeight=  Math.round(screenWidth / scale);
		
		//得到图片的高度  总高度  图片高度+ 28dp
		int bottomHeight = AndroidUtils.dip2px(view.getContext(), 46);
		
		int totalHeightPerShop = webHeight + bottomHeight;
		
		int totalHeight = totalHeightPerShop*sizeofshop;
		
		//LinearLayout.LayoutParams params= (LinearLayout.LayoutParams) linelayout1.getLayoutParams();
		//RelativeLayout
		LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) linelayout1.getLayoutParams();
		 
		params.width  = screenWidth;
		params.height = totalHeight;
		linelayout1.setLayoutParams(params);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		//Log.i("longhua","resultcode="+String.valueOf(resultCode)+"  requestCode="+String.valueOf(requestCode));
		
		if (resultCode == 9) {
			if (requestCode == SCANER_CODE) {
				Bundle bundle = data.getExtras();
				String scanResult = bundle.getString("result");
				
				Intent intent=new Intent(view.getContext(), ZyScanDeviceResult.class);
				Bundle bundle1=new Bundle();
				bundle1.putString("type", "1");
				bundle1.putString("id", scanResult);
				intent.putExtras(bundle1);
				startActivity(intent);
				//设置切换动画，从右边进入，左边退出
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				return;
			}
		}
		
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		//lGetgps.destroyAMapLocationListener();
	}
	
	@Override
	public void onAttach(Activity activity){
			super.onAttach(activity);
	}
	
	@Override
	public void onClick(View arg0) {
		
		/*if (R.id.wg_head_main_btn==arg0.getId()) {//如果是使用此功能，需要返回数据，使用startActivityForResult
			Login();
		}*/
		
		/*
		reinit();
		if(R.id.navi_huhua_log_search==arg0.getId()){
			Intent intent=new Intent(arg0.getContext(), HhSearchShopActivity.class); 
			Bundle bundle=new Bundle();
			bundle.putString("key", "");
			intent.putExtras(bundle); 
			startActivity(intent); 
			((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			return;
		}
		
		
		if(R.id.hh_shouye_area_text==arg0.getId()){
			tv_city.setTextColor(this.getResources().getColor(R.color.hh_select_fenlei_select)); 
			Drawable nav_down=getResources().getDrawable(R.drawable.hhareaselect_green); 
			nav_down.setBounds(0, 0, 30, 30);
			tv_city.setCompoundDrawables(null,null,nav_down,null); 
			popwindowSortByarea(tv_city);
			return;
		}
		*/
	}
	
	//地区类别
    void popwindowSortByarea(View view) {
        if (popupWindow != null && popupWindow.isShowing()) {
            return;
        }
        
        final LinearLayout layout = (LinearLayout) this.getActivity().getLayoutInflater().inflate(R.layout.util_onelevelmenu, null);
        popupWindow = new PopupWindow(layout,
                ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.FILL_PARENT,true);
        
        //ColorDrawable dw = new ColorDrawable(-00000);
		ColorDrawable dw = new ColorDrawable(getResources().getColor(00000));
        popupWindow.setBackgroundDrawable(dw);
        popupWindow.update();
        //popupWindow.getBackground().setAlpha(100);//0~255透明度值 ，0为完全
        
        //在PopupWindow里面就加上下面代码，让键盘弹出时，不会挡住pop窗口。  
        popupWindow.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED); 
        popupWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        
        //直接点 透明的地方 自动关闭
        final View v3 = layout.findViewById(R.id.hh_onelevel_all_layout);
        v3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                    //在此处添加你的按键处理 xxx 
                    popupWindow.dismiss(); 
                    reinit();
                    return;
                }
    				 
            }
        });
        
        //点击空白处时，隐藏掉pop窗口
        popupWindow.setFocusable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        //添加弹出、弹入的动画
        //popupWindow.setAnimationStyle(R.style.Popupwindow);
        popupWindow.showAsDropDown(view);
        //popupWindow.setAnimationStyle(R.style.PopupAnimation);
        int[] location = new int[2];
        view.getLocationOnScreen(location); 
    	
        popupWindow.showAtLocation(view, Gravity.LEFT , 0, location[1]+2);
        
        
        mainlistView=(MyListView)layout.findViewById(R.id.onelevellistView);
 	   
 	   //地区的图片
 	    mainAdapter=new areaLevelClassAdapter(layout.getContext(), mainClass);
 	    mainlistView.setAdapter(mainAdapter);
 	    
 	    final int curlocation=0;
 	    mainAdapter.setSelectedPosition(0);
 	    mainAdapter.notifyDataSetInvalidated();
 	    
 	    //下面代码是直接默认显示第一个子菜单，第一个子菜单显示 且选中第一条
 	    //如果 主类为不限制 ,则直接不在现实二级菜单
 	    
		//下面是定义一级分类的数据 的CLICK事件
		mainlistView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int position,
						long arg3) {
					// TODO Auto-generated method stub
					final int location=position;
					mainAdapter.setSelectedPosition(position);
					mainAdapter.notifyDataSetInvalidated(); 
					
					String mainclass = mainClass[location];
					//servertype = mainclass;
					
					//直接弹出  检索店铺的窗口 ，把地区传递过去。进行检索
					//HhSearchShopActivity
					popupWindow.dismiss(); 
	                reinit();
	                
					Intent intent=new Intent(arg0.getContext(), WgPlanActivity.class); 
					Bundle bundle=new Bundle();
					bundle.putString("key", mainclass);
					intent.putExtras(bundle); 
					startActivity(intent); 
					((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
					
	                return;
				}
			});
		
    }
    
    public void reinit(){
    	if(popupWindow != null && popupWindow.isShowing()){
            popupWindow.dismiss();
           //reini();
        }
    	
    	tv_city.setTextColor(this.getResources().getColor(R.color.hh_select_fenlei));
		 
		Drawable nav_down=getResources().getDrawable(R.drawable.hh_selectarea_arrow);
		//nav_down.setBounds(0, 0, nav_down.getMinimumWidth(), nav_down.getMinimumHeight()); 
		nav_down.setBounds(0, 0, 30, 30);
		tv_city.setCompoundDrawables(null,null,nav_down,null);
		
		
		//Drawable nav_down=getResources().getDrawable(R.drawable.hhareaselect); 
		//nav_down.setBounds(0, 0, 30, 30);
		//tv_servertype.setCompoundDrawables(null,null,nav_down,null); 
		//tv_productprice.setCompoundDrawables(null,null,nav_down,null);
		//tv_renqi.setCompoundDrawables(null,null,nav_down,null);
		
    }
    
    private class GetDataTask extends AsyncTask<Void, Void, String[]>{
    	@Override 
		protected String[] doInBackground(Void... params){
    		try{
    			Thread.sleep(1000);
    		} catch (InterruptedException e){
    		}
    		return null;
    	}
    }
	 
	//返回值得时候，回调触发是在启动 启动fragment的HhMainActivity 当中，WgLoginActivity必须在AndroidManifest中写入
	public void Login(){
		Intent intent = new Intent(getActivity(),WgLoginActivity.class);
        // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivityForResult(intent, guest_login_result);
	}
	
	
	
}
