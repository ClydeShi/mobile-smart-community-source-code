package com.zhyq.fragment;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.aspsine.irecyclerview.IRecyclerView;
import com.aspsine.irecyclerview.OnLoadMoreListener;
import com.aspsine.irecyclerview.OnRefreshListener;
import com.aspsine.irecyclerview.demo.ui.widget.footer.LoadMoreFooterView;
import com.zhyq.R;
import com.zhyq.WgContactViewActivity;
import com.zhyq.ZyCleanAddActivity;
import com.zhyq.ZyCompanyServiceOne;
import com.zhyq.ZyCompanyServiceThree;
import com.zhyq.ZyCompanyServiceTwo;
import com.zhyq.ZyMeetingRoomListActivity;
import com.zhyq.ZyNewsListActivity;
import com.zhyq.ZyNewsViewActivity;
import com.zhyq.ZyComplainAddActivity;
import com.zhyq.ZyVisitorAddActivity;
import com.zhyq.adapter.baseRecyleViewAdapter;
import com.zhyq.adapter.ZyWorkAdapter;
import com.zhyq.adapter.areaLevelClassAdapter;
import com.zhyq.libs.AndroidUtils;
import com.zhyq.libs.MyListView;
import com.zhyq.libs.PreferenceUtils;
import com.zhyq.libs.ProgersssDialog;
import com.zhyq.model.SysPassValue;
import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.sendOrderToServerForListYTask;

import java.util.ArrayList;
import java.util.List;

/*
 * 主页面板
 * 读取布局，设置监听
 * 在onResume里通过setLoginInfo设置登录信息
 */

public class WgHomeFragmentCompanyHome extends Fragment implements OnClickListener,OnItemClickListener, OnRefreshListener, OnLoadMoreListener {
	private View view = null;
	private TextView tv_city = null;
	private final int SCANER_CODE = 1;
	public static final int guest_login_result = 0;

	private long userId = 0;
	private String userCode = "";
	private String userName = "";
	private String userRole = "";

	private PopupWindow popupWindow;

	//下面是二级菜单
	private MyListView mainlistView;//主类菜单
	private areaLevelClassAdapter mainAdapter;//主要类数据适配器
	String[] mainClass;//一级的文字 大类文字
	int images_all = R.drawable.ic_category_0;// 不限对应右边的 小图片
	int images_other = R.drawable.select_right;//  其他类右边的小图片对应的是向右的箭头

	private View includeTitle;
	private IRecyclerView iRecyclerView;
	private View headerView = null;
	private LoadMoreFooterView loadMoreFooterView;
	private ZyWorkAdapter mAdapter1;
	private baseRecyleViewAdapter mAdapter;
	private int mPage;
	private int currentPage = 1;
	private List<SysPassValue> dataList = new ArrayList<SysPassValue>();//数据
	public ProgersssDialog dialog;

	//private View includeTitle;
	private boolean logined = false;

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		AndroidUtils.removeParentView(view);
		if (view==null) {

			view=inflater.inflate(R.layout.fragment_zy_companyhome, null);
			//view.findViewById(R.id.wg_my_login).setOnClickListener(this);
			//view.findViewById(R.id.wg_head_main_btn).setOnClickListener(this);
		}

		showProgressDialog();

		//includeTitle = view.findViewById(R.id.wg_util_head_feedback_list);
		//((TextView) includeTitle.findViewById(R.id.wg_head_title)).setText("检查反馈");
		//Button btnReturn = (Button) includeTitle.findViewById(R.id.wg_head_btn_return);
		//btnReturn.setOnClickListener(this);

		iRecyclerView = (IRecyclerView) view.findViewById(R.id.zy_qiye_recyclerview);

		iRecyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));

		headerView = LayoutInflater.from(this.getActivity()).inflate(R.layout.item_zy_companyhome, iRecyclerView.getHeaderContainer(), false);
		iRecyclerView.addHeaderView(headerView);//加入view

		//解决卡顿问题
		iRecyclerView.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
		iRecyclerView.setFocusable(true);
		iRecyclerView.setFocusableInTouchMode(true);
		iRecyclerView.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View view, MotionEvent motionEvent) {
				view.requestFocusFromTouch();
				return false;
			}
		});

		loadMoreFooterView = (LoadMoreFooterView) iRecyclerView.getLoadMoreFooterView();

		mAdapter = new baseRecyleViewAdapter(this.getActivity(),dataList,"item_zy_news_list");//通用adapter
		//mAdapter = new ZyWorkAdapter(dataList);//单独写adapter
		mAdapter.setOnClick1(this);
		iRecyclerView.setIAdapter(mAdapter);

		iRecyclerView.setOnRefreshListener(this);
		iRecyclerView.setOnLoadMoreListener(this);

		iRecyclerView.post(new Runnable() {
			@Override
			public void run() {
				iRecyclerView.setRefreshing(true);
			}
		});

		//Log.i("luktel","11111111");

		/*includeTitle = view.findViewById(R.id.wg_main_head);
		((TextView)includeTitle.findViewById(R.id.tv_wg_head_add_title)).setText("智慧园区管理系统");*/

		//((TextView)view.findViewById(R.id.zhuye_text_date)).setText(StrEdit.getDate("yyyy-MM-dd")+" "+StrEdit.getWeeks());

		/*
		User user = HhApplication.getInstance(this.getActivity()).getHhCart().getUser();
		if(user==null){
			logined = false;
		}else{
			logined = true;
		}
		*/

		//userId = PreferenceUtils.getString(getActivity(),"userid","");
		userId = PreferenceUtils.getLong(getActivity(), "userid", -1);
		userRole = PreferenceUtils.getString(getActivity(),"userrole","");
		//Log.i("longhua","userId"+userId);

		Button wg_main_btn1 =(Button)headerView.findViewById(R.id.wg_main_btn1);
		wg_main_btn1.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

                //Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
                //return;

				userId = PreferenceUtils.getLong(getActivity(), "userid", -1);//重新获取登录值
				if (userId==-1) {
					Toast.makeText(v.getContext(), "请先登入,谢谢！", Toast.LENGTH_SHORT).show();
					return;
				}

				Intent intent=new Intent(v.getContext(), ZyNewsListActivity.class);
				Bundle bundle=new Bundle();
				bundle.putString("channelId", "2");
				intent.putExtras(bundle);
				startActivity(intent);
				//设置切换动画，从右边进入，左边退出
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				//overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				//AndroidUtils.start(v.getContext(), HhSpecilityShopActivity.class);
				return;

			}
		});

        Button wg_main_btn2 =(Button)headerView.findViewById(R.id.wg_main_btn2);
        wg_main_btn2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                userId = PreferenceUtils.getLong(getActivity(), "userid", -1);//重新获取登录值
                if (userId==-1) {
                    Toast.makeText(v.getContext(), "请先登入,谢谢！", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent=new Intent(v.getContext(), ZyVisitorAddActivity.class);
                Bundle bundle=new Bundle();
                bundle.putString("channelId", "1");
                intent.putExtras(bundle);
                startActivity(intent);
                //设置切换动画，从右边进入，左边退出
                ((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                //overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                //AndroidUtils.start(v.getContext(), HhSpecilityShopActivity.class);
                return;

            }
        });

		Button wg_main_btn3 =(Button)headerView.findViewById(R.id.wg_main_btn3);
		wg_main_btn3.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;

			}
		});

		Button wg_main_btn4 =(Button)headerView.findViewById(R.id.wg_main_btn4);
		wg_main_btn4.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;

			}
		});

		Button wg_main_btn5 =(Button)headerView.findViewById(R.id.wg_main_btn5);
		wg_main_btn5.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;

			}
		});

		Button wg_main_btn6 =(Button)headerView.findViewById(R.id.wg_main_btn6);
		wg_main_btn6.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				userId = PreferenceUtils.getLong(getActivity(), "userid", -1);//重新获取登录值
				if (userId==-1) {
					Toast.makeText(v.getContext(), "请先登入,谢谢！", Toast.LENGTH_SHORT).show();
					return;
				}
				Intent intent=new Intent(v.getContext(), ZyMeetingRoomListActivity.class);
				Bundle bundle=new Bundle();
				bundle.putString("channelId", "2");
				intent.putExtras(bundle);
				startActivity(intent);
				//设置切换动画，从右边进入，左边退出
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				//overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				//AndroidUtils.start(v.getContext(), HhSpecilityShopActivity.class);
				return;

			}
		});

		Button wg_main_btn7 =(Button)headerView.findViewById(R.id.wg_main_btn7);
		wg_main_btn7.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				userId = PreferenceUtils.getLong(getActivity(), "userid", -1);//重新获取登录值
				if (userId==-1) {
					Toast.makeText(v.getContext(), "请先登入,谢谢！", Toast.LENGTH_SHORT).show();
					return;
				}

				Intent intent=new Intent(v.getContext(), ZyCleanAddActivity.class);
				Bundle bundle=new Bundle();
				bundle.putString("channelId", "2");
				intent.putExtras(bundle);
				startActivity(intent);
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				return;

			}
		});

		Button wg_main_btn8 =(Button)headerView.findViewById(R.id.wg_main_btn8);
		wg_main_btn8.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				if (userId==-1) {
					Toast.makeText(v.getContext(), "请先登入,谢谢！", Toast.LENGTH_SHORT).show();
					return;
				}

				Intent itplan = new Intent(v.getContext(),ZyComplainAddActivity.class);
				startActivity(itplan);
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				return;

				//Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				//return;

			}
		});

		Button wg_main_btn9 =(Button)headerView.findViewById(R.id.wg_main_btn9);
		wg_main_btn9.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				//Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				//return;

				userId = PreferenceUtils.getLong(getActivity(), "userid", -1);//重新获取登录值
				if (userId==-1) {
					Toast.makeText(v.getContext(), "请先登入,谢谢！", Toast.LENGTH_SHORT).show();
					return;
				}

				Intent intent=new Intent(v.getContext(), ZyCleanAddActivity.class);
				Bundle bundle=new Bundle();
				bundle.putString("channelId", "1");
				intent.putExtras(bundle);
				startActivity(intent);
				//设置切换动画，从右边进入，左边退出
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				//overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				//AndroidUtils.start(v.getContext(), HhSpecilityShopActivity.class);
				return;

			}
		});

		Button wg_main_btn10 =(Button)headerView.findViewById(R.id.wg_main_btn10);
		wg_main_btn10.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				userId = PreferenceUtils.getLong(getActivity(), "userid", -1);//重新获取登录值
				if (userId==-1) {
					Toast.makeText(v.getContext(), "请先登入,谢谢！", Toast.LENGTH_SHORT).show();
					return;
				}
				Intent intent=new Intent(v.getContext(), ZyMeetingRoomListActivity.class);
				Bundle bundle=new Bundle();
				bundle.putString("channelId", "1");
				intent.putExtras(bundle);
				startActivity(intent);
				//设置切换动画，从右边进入，左边退出
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				//overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				//AndroidUtils.start(v.getContext(), HhSpecilityShopActivity.class);
				return;

			}
		});

		Button wg_main_btnmiddle1 =(Button)headerView.findViewById(R.id.wg_main_btnmiddle1);
		wg_main_btnmiddle1.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				userId = PreferenceUtils.getLong(getActivity(), "userid", -1);//重新获取登录值
				if (userId==-1) {
					Toast.makeText(v.getContext(), "请先登入,谢谢！", Toast.LENGTH_SHORT).show();
					return;
				}
				Intent intent=new Intent(v.getContext(), ZyCompanyServiceOne.class);
				Bundle bundle=new Bundle();
				bundle.putString("type", "1");
				intent.putExtras(bundle);
				startActivity(intent);
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				return;

			}
		});

		Button wg_main_btnmiddle2 =(Button)headerView.findViewById(R.id.wg_main_btnmiddle2);
		wg_main_btnmiddle2.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				userId = PreferenceUtils.getLong(getActivity(), "userid", -1);//重新获取登录值
				if (userId==-1) {
					Toast.makeText(v.getContext(), "请先登入,谢谢！", Toast.LENGTH_SHORT).show();
					return;
				}
				Intent intent=new Intent(v.getContext(), ZyCompanyServiceTwo.class);
				Bundle bundle=new Bundle();
				bundle.putString("type", "2");
				intent.putExtras(bundle);
				startActivity(intent);
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				return;

			}
		});

		Button wg_main_btnmiddle3 =(Button)headerView.findViewById(R.id.wg_main_btnmiddle3);
		wg_main_btnmiddle3.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				userId = PreferenceUtils.getLong(getActivity(), "userid", -1);//重新获取登录值
				if (userId==-1) {
					Toast.makeText(v.getContext(), "请先登入,谢谢！", Toast.LENGTH_SHORT).show();
					return;
				}
				Intent intent=new Intent(v.getContext(), ZyCompanyServiceThree.class);
				Bundle bundle=new Bundle();
				bundle.putString("type", "3");
				intent.putExtras(bundle);
				startActivity(intent);
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				return;

			}
		});

		return view;
	}

	public void showProgressDialog(){
		dialog = new ProgersssDialog(this.getActivity());
		//dialog.show();
	}

	@Override
	public void onClick(View v) {

		if(R.id.item_zy_newslist_btn==v.getId()){

			//传递参数过去
			Object tag = v.getTag(R.id.button_tag1);

			if (tag != null && tag instanceof Integer) { //解决问题：如何知道你点击的按钮是哪一个列表项中的，通过Tag的position
				//ToastUtils.show(v.getContext(), "22详情  NULL");
				int position = (Integer) tag;
				String ids =  dataList.get(position).getId();
				//Toast.makeText(v.getContext(), "ids"+ids, Toast.LENGTH_SHORT).show();
                //Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
                //return;
				Intent intent=new Intent(v.getContext(), ZyNewsViewActivity.class);
				Bundle bundle=new Bundle();
				bundle.putString("id", ids);
				intent.putExtras(bundle);
				startActivity(intent);
				//设置切换动画，从右边进入，左边退出
				this.getActivity().overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			}
		}

	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int positon, long arg3) {
		//ToastUtils.show(arg1.getContext(), R.string.show_logon_error_msg);
	}

	@Override
	public void onRefresh() {
		//loadBanner();
		loadMoreFooterView.setStatus(LoadMoreFooterView.Status.GONE);
        currentPage = 1;
        getData();
	}

	@Override
	public void onLoadMore() {
		if (loadMoreFooterView.canLoadMore() && mAdapter.getItemCount() > 0) {
			loadMoreFooterView.setStatus(LoadMoreFooterView.Status.LOADING);
            getData();
		}
	}

	public void getData() {

		SysPassValue temp = new SysPassValue();
		temp.setFunctionName("getNewsList");
		temp.setPageNumber(String.valueOf(this.currentPage));
		temp.setArg1("2");//公告
		temp.setUserId(String.valueOf(userId));

		new sendOrderToServerForListYTask(new AbstractYGetTaskListener<List<SysPassValue>>() {
			@Override
			public void onPostExecute(String name, YGetTask<List<SysPassValue>> task) {
				iRecyclerView.setRefreshing(false);
				showData(task.getValue());
				dialog.dismiss();
			}
		}, temp).execute();
	}

	public void showData(final List<SysPassValue> list) {

		View vnoRecord = view.findViewById(R.id.activity_no_record_showpic);
		if (list == null) {
			loadMoreFooterView.setStatus(LoadMoreFooterView.Status.ERROR);
			//Toast.makeText(this, "网络连接失败", Toast.LENGTH_SHORT).show();

			if (currentPage == 1) {
				this.dataList.clear();
				mAdapter.notifyDataSetChanged();
				vnoRecord.setVisibility(View.INVISIBLE);
				return;
			}
			return;
		}

		if (list.size() == 0) {
			if (currentPage == 1) {
				this.dataList.clear();
				mAdapter.notifyDataSetChanged();
				vnoRecord.setVisibility(View.VISIBLE);//如果是第一页没有数据则显示无记录
			} else {
				loadMoreFooterView.setStatus(LoadMoreFooterView.Status.THE_END);
			}
			//Toast.makeText(this, "已经到了最后一页", Toast.LENGTH_SHORT).show();
		} else {
			if (currentPage == 1) {
				currentPage = 2;
				this.dataList.clear();
				this.dataList.addAll(list);
				vnoRecord.setVisibility(View.GONE);
			} else {

				loadMoreFooterView.postDelayed(new Runnable() {
					@Override
					public void run() {
						currentPage++;
						loadMoreFooterView.setStatus(LoadMoreFooterView.Status.GONE);
						dataList.addAll(list);
					}
				}, 1000);

				//loadMoreFooterView.setStatus(LoadMoreFooterView.Status.GONE);
				//dataList.addAll(list);
			}
			mAdapter.notifyDataSetChanged();
		}
		//vnoRecord.setVisibility(View.GONE);

		//如果第一页则需要清除数据

		if (dataList.size() == 0) {
			//vnoRecord.setVisibility(View.VISIBLE);
		}

		//Log.d("luktel", "showData");

	}

	@Override
	public void onResume() {
		super.onResume();
		//setLoginInfo();
	}

	/*
	 * 设置登入人的信息
	 */
    public void setLoginInfo(){
    	TextView tvName = (TextView)view.findViewById(R.id.wg_my_showname);
		/*Button btnLogin = (Button)view.findViewById(R.id.wg_head_main_btn);*/

		long puserId = PreferenceUtils.getLong(getActivity(), "userid", -1);//登录成功后，重新获取登录值
		String puserName = PreferenceUtils.getString(getActivity(),"username","");

		//Log.i("longhua","setLoginInfo after"+puserId+puserName);

		//if (puserName.equals("")) {
		/*if (puserId==-1) {
			tvName.setVisibility(View.VISIBLE);
			btnLogin.setVisibility(View.VISIBLE);
			tvName.setText("请登录");
		} else {
			tvName.setVisibility(View.VISIBLE);
			btnLogin.setVisibility(View.INVISIBLE);
			tvName.setText(puserName);
		}*/

		/*
		if (StrEdit.isInclude(userRole,2002)) {
			RelativeLayout rl = (RelativeLayout)view.findViewById(R.id.wg_main_relativelayout4);
			rl.setVisibility(View.INVISIBLE);
		}
		*/

		/*
		logined = HhApplication.getInstance(this.getActivity()).getHhCart().isLogined();//注销后不会保留

		if (logined) {
			username = HhApplication.getInstance(this.getActivity()).getHhCart().getUser().getName();
			tvName.setVisibility(View.VISIBLE);
			tvGuestLogin.setVisibility(View.INVISIBLE);
			btnLogin.setVisibility(View.INVISIBLE);
			tvName.setText(username);
		} else {
			tvName.setVisibility(View.INVISIBLE);
			tvGuestLogin.setVisibility(View.VISIBLE);
			btnLogin.setVisibility(View.VISIBLE);
			tvName.setText("");
		}
		*/
    }

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		//Log.i("longhua","resultcode="+String.valueOf(resultCode)+"  requestCode="+String.valueOf(requestCode));

		if (resultCode == 9) {
			if (requestCode == SCANER_CODE) {
				Bundle bundle = data.getExtras();
				String scanResult = bundle.getString("result");

				Intent intent=new Intent(view.getContext(), WgContactViewActivity.class);
				Bundle bundle1=new Bundle();
				bundle1.putString("contactId", scanResult);
				intent.putExtras(bundle1);
				startActivity(intent);
				//设置切换动画，从右边进入，左边退出
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				return;
			}
		}

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		//lGetgps.destroyAMapLocationListener();
	}

	@Override
	public void onAttach(Activity activity){
			super.onAttach(activity);
	}
	
	
}
