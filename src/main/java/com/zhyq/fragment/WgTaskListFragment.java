package com.zhyq.fragment;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
 
import java.util.Date;
import java.util.List;

 //com.unitesoft.findzixun.CpptFindzixunMainFragment

 
import com.zhyq.HhApplication;
import com.zhyq.WgTaskViewActivity;
 
 
import com.zhyq.R;
 

import com.zhyq.adapter.WgTaskListAdapter2;
  
import com.zhyq.libs.AndroidUtils;
 
import com.zhyq.libs.me.maxwin.view.IXListViewLoadMore;
import com.zhyq.libs.me.maxwin.view.IXListViewRefreshListener;
import com.zhyq.libs.me.maxwin.view.XListView;
 
import com.zhyq.model.CommonList;
 
import com.zhyq.model.User;
 
 
import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
 
 
import com.zhyq.task.impl.GetTaskListYTask;


import android.content.Intent;
 
import android.os.Bundle;
import android.support.v4.app.Fragment;
 
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;



public class WgTaskListFragment extends Fragment implements IXListViewRefreshListener, IXListViewLoadMore, OnItemClickListener,OnClickListener {
	
	private String newstitle ="全部";
 
	private String lastRefreshdt = "";
	
	private String str_hdtype = null;
	private String userid = "";
	private XListView mXListView;
	private View view = null;
    private WgTaskListAdapter2 mAdapter;//数据适配器
    private List<CommonList>  NewsList = new ArrayList<CommonList>();
	private int currentPage = 1;
	
	//设置参数 
    public void setArgument(String  title,String hdtype ){
    	  this.newstitle        = title;
    	  this.str_hdtype       = hdtype;       //活动类型 
    	  
    	  onRefresh();
    }
    
    public void setCurArguments(String type) {
		this.str_hdtype       = type;       //店铺类型
    }
    
    public WgTaskListFragment() {
		super();
		this.newstitle        = "全部";
  	    this.str_hdtype       = "全部";       //店铺类型
	}
    
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		AndroidUtils.removeParentView(view);
		if (view==null) {
			view = inflater.inflate(R.layout.tab_item_fragment_main_foractivity, null);
			 
		}
		currentPage=1;
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);  
		/*
		 *  初始化数据适配器，传递Content 过去
		 */
		mAdapter = new WgTaskListAdapter2(getActivity(), NewsList);
		/*
         * 初始化 
         */  
		mXListView = (XListView) getView().findViewById(R.id.id_activity_xlistView);
		
		mXListView.setAdapter(mAdapter);
		mXListView.setPullRefreshEnable(this);  
	    mXListView.setPullLoadEnable(this);
	    /*
	     * 进来时直接刷新
	     */
	    mXListView.startRefresh();
	    mXListView.setOnItemClickListener(this);
	     
	    //onRefresh();
	     
	    User user = new User();
		user =  HhApplication.getInstance(getActivity()).getHhCart().getUser();
		
		if(!(user==null)){
			userid =  String.valueOf(HhApplication.getInstance(getActivity()).getHhCart().getUser().getId());
			//userName = HhApplication.getInstance(getActivity()).getHhCart().getUser().getName();
		}else{
			userid="-1";
			return;
		}
		  
	}
	 
	@Override  
    public void onRefresh(){
		currentPage = 1; 
		LoadDatasTask(); 
		onLoad();
    }
	
	private void onLoad() {
		mXListView.stopRefresh();
		mXListView.stopLoadMore();
		// mXListView.setRefreshTime("刚刚");
	}
	
	@Override
	public void onLoadMore() {
		currentPage++;
		LoadDatasTask(); 
	}
	
	/*
     * 记载数据的异步任务
    
     */
	public void LoadDatasTask () {
		
		
		new GetTaskListYTask(new AbstractYGetTaskListener<List<CommonList>>() {
			@Override
			public void onPostExecute(String name, YGetTask<List<CommonList>> task) {  
				showDataList(task.getValue());
				mXListView.stopLoadMore();
			}
		},userid,str_hdtype,currentPage).execute(); 
	}
	
	public void showDataList(List<CommonList> list) {
		
		View v_nodatapic = view .findViewById(R.id.activity_no_record_showpic);
		
		 if (list==null){  
			 if(currentPage==1){
		    		this.NewsList.clear(); 
		    		mAdapter.notifyDataSetChanged(); 
		    		v_nodatapic.setVisibility(View.VISIBLE);
		    		return;
				} 
				
				return;
			} 
			v_nodatapic.setVisibility(View.GONE);
		
		//如果是第一页则需要清楚数据   currentPage
		
		//this.NewsList=list; 
		//CpptZixunItemListGridListAdapter adapter=new CpptZixunItemListGridListAdapter(list);  
		//mXListView.setAdapter(mAdapter);  
		 
		//mAdapter.addAll(NewsList);  
       // mAdapter.notifyDataSetChanged();  
        
    	if(currentPage==1){
    		this.NewsList.clear();
			this.NewsList.addAll(list); 
			
		} else{
			NewsList.addAll(list); 
		}
    	
    	 mAdapter.notifyDataSetChanged();   
        		
       if(lastRefreshdt.equals("")) {
    	   mXListView.setRefreshTime("刚刚"); 
       } else {
    	   mXListView.setRefreshTime(lastRefreshdt);  
       }
       
       SimpleDateFormat    formatter    =   new    SimpleDateFormat    ("MM月dd日    HH:mm:ss     ");       
       Date    curDate    =   new    Date(System.currentTimeMillis());//获取当前时间       
       lastRefreshdt =    formatter.format(curDate); 
       if(NewsList.size()==0){
          	v_nodatapic.setVisibility(View.VISIBLE);
          }
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}
	
	//该函数的作用是 直接 打开对应的咨询 的连接
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		// XML 中设置   ListView 中不要设置 OntouchMode = true .否则不会触发这个事件
		// XML 中设置  android:descendantFocusability="blocksDescendants"
		//Toast.makeText(arg0.getContext(),"gfgfgfgfgfg", Toast.LENGTH_SHORT).show();
		if(arg3<0) { 
			        // 点击的是headerView或者footerView 
			        return;
		}
		
		CommonList item = (CommonList)arg0.getItemAtPosition(arg2);
		
		String taskId;
		String taskName;
		
		taskId   = String.valueOf(item.getId());
		taskName = item.getVarValue1();
		  
		//使用Bundle  用来传递参数给  fragment   打开 具体的 活动页面
		 
		//点开详情
		Intent in=new Intent(arg0.getContext(),WgTaskViewActivity.class);
		in.putExtra("taskId", taskId);
		in.putExtra("taskName", taskName);
		
		this.startActivity(in);
		 
		(this.getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
	}
	
	public void refreshData(String sortColumn){
		 
		if(mAdapter==null){
			Log.i("huahua","current object is null");
			return;
		}
		Log.i("huahua","current object is o'k'k");
		mXListView.setAdapter(mAdapter);
		
		//mXListView.setAdapter(new AllHuodongImageAdapter(this.get, NewsList));  
		  
		mXListView.setAdapter(new WgTaskListAdapter2(view.getContext(),NewsList));
		mAdapter.notifyDataSetChanged();
	}
	
}

