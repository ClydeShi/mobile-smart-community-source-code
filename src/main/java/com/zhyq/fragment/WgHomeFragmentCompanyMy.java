package com.zhyq.fragment;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.github.javiersantos.appupdater.AppUpdater;
import com.github.javiersantos.appupdater.enums.Display;
import com.github.javiersantos.appupdater.enums.UpdateFrom;
import com.zhyq.HhApplication;
import com.zhyq.R;
import com.zhyq.WgLoginActivity;
import com.zhyq.ZyMyApplyListActivity;
import com.zhyq.ZyMyCleanListActivity;
import com.zhyq.ZyMyComplainListActivity;
import com.zhyq.ZyMyMeetingListActivity;
import com.zhyq.ZyMyPersonal;
import com.zhyq.libs.AndroidUtils;
import com.zhyq.libs.PreferenceUtils;
import com.zhyq.libs.ProgersssDialog;
import com.zhyq.model.SysPassValue;
import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.sendOrderToServerForValueYTask;


public class WgHomeFragmentCompanyMy extends Fragment implements OnItemClickListener,OnClickListener {
	private View view = null;
	public static final int guest_login_result = 0;
	private boolean logined = false;
	private View includeTitle;

	private Long userId = null;
	private String userCode = "";
	private String userName = "";
	private String userType = "";
	private String userRole = "";

	private String companyId = "";

	public ProgersssDialog dialog;

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		AndroidUtils.removeParentView(view);
		if (view==null) {
			view=inflater.inflate(R.layout.fragment_zy_companymy, null);
		}

		showProgressDialog();

		includeTitle = view.findViewById(R.id.zy_my_head);
		((TextView)includeTitle.findViewById(R.id.util_my_head_title)).setText("我的");
		Button btnReturn = (Button)includeTitle.findViewById(R.id.util_my_head_btn_return);
		btnReturn.setOnClickListener(this);

		View util_my_head_relativelayout1 = (View)includeTitle.findViewById(R.id.util_my_head_relativelayout1);
		util_my_head_relativelayout1.setVisibility(View.INVISIBLE);

		Button btnSet = (Button)includeTitle.findViewById(R.id.util_my_head_btn_set);
		btnSet.setOnClickListener(this);

		((Button)view.findViewById(R.id.wg_update_btn)).setOnClickListener(this);
		((Button)view.findViewById(R.id.wg_logout_btn)).setOnClickListener(this);

		((Button)view.findViewById(R.id.util_my_head_btn_set)).setOnClickListener(this);
		((Button)view.findViewById(R.id.my_btn_list_8)).setOnClickListener(this);

		userId = PreferenceUtils.getLong(getActivity(), "userid", -1);
		userCode = PreferenceUtils.getString(getActivity(),"usercode","");
		userName = PreferenceUtils.getString(getActivity(),"username","");
		userType = PreferenceUtils.getString(getActivity(),"usertype","");
		userRole = PreferenceUtils.getString(getActivity(),"userrole","");
		//Log.i("luktel","userId"+userId);
		if (userId==-1) {
			AndroidUtils.start(getActivity(), WgLoginActivity.class);
			getActivity().finish();
			((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
		}

		Button my_btn_1 =(Button)view.findViewById(R.id.my_btn_1);
		my_btn_1.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;

			}
		});

		Button my_btn_2 =(Button)view.findViewById(R.id.my_btn_2);
		my_btn_2.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;

			}
		});

		Button my_btn_list_1 =(Button)view.findViewById(R.id.my_btn_list_1);
		my_btn_list_1.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;

			}
		});

		Button my_btn_list_2 =(Button)view.findViewById(R.id.my_btn_list_2);
		my_btn_list_2.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				userId = PreferenceUtils.getLong(getActivity(), "userid", -1);//重新获取登录值
				if (userId==-1) {
					Toast.makeText(v.getContext(), "请先登入,谢谢！", Toast.LENGTH_SHORT).show();
					return;
				}
				Intent intent = new Intent(v.getContext(),ZyMyCleanListActivity.class);
				Bundle bundle=new Bundle();
				bundle.putString("channelId", "1");
				bundle.putString("userId", String.valueOf(userId));
				bundle.putString("companyId", companyId);
				intent.putExtras(bundle);
				startActivity(intent);
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				return;

			}
		});

		Button my_btn_list_3 =(Button)view.findViewById(R.id.my_btn_list_3);
		my_btn_list_3.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				userId = PreferenceUtils.getLong(getActivity(), "userid", -1);//重新获取登录值
				if (userId==-1) {
					Toast.makeText(v.getContext(), "请先登入,谢谢！", Toast.LENGTH_SHORT).show();
					return;
				}
				Intent intent=new Intent(v.getContext(), ZyMyCleanListActivity.class);
				Bundle bundle=new Bundle();
				bundle.putString("channelId", "2");
				bundle.putString("userId", String.valueOf(userId));
				bundle.putString("companyId", companyId);
				intent.putExtras(bundle);
				startActivity(intent);
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				return;

			}
		});

		Button my_btn_list_4 =(Button)view.findViewById(R.id.my_btn_list_4);
		my_btn_list_4.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				userId = PreferenceUtils.getLong(getActivity(), "userid", -1);//重新获取登录值
				if (userId==-1) {
					Toast.makeText(v.getContext(), "请先登入,谢谢！", Toast.LENGTH_SHORT).show();
					return;
				}
				Intent intent=new Intent(v.getContext(), ZyMyApplyListActivity.class);
				Bundle bundle=new Bundle();
				bundle.putString("channelId", "1");
				bundle.putString("userId", String.valueOf(userId));
				bundle.putString("companyId", companyId);
				intent.putExtras(bundle);
				startActivity(intent);
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				return;

			}
		});

		Button my_btn_list_5 =(Button)view.findViewById(R.id.my_btn_list_5);
		my_btn_list_5.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				userId = PreferenceUtils.getLong(getActivity(), "userid", -1);//重新获取登录值
				if (userId==-1) {
					Toast.makeText(v.getContext(), "请先登入,谢谢！", Toast.LENGTH_SHORT).show();
					return;
				}
				Intent intent=new Intent(v.getContext(), ZyMyMeetingListActivity.class);
				Bundle bundle=new Bundle();
				bundle.putString("channelId", "1");
				bundle.putString("userId", String.valueOf(userId));
				bundle.putString("companyId", companyId);
				intent.putExtras(bundle);
				startActivity(intent);
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				return;

			}
		});

		Button my_btn_list_6 =(Button)view.findViewById(R.id.my_btn_list_6);
		my_btn_list_6.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				userId = PreferenceUtils.getLong(getActivity(), "userid", -1);//重新获取登录值
				if (userId==-1) {
					Toast.makeText(v.getContext(), "请先登入,谢谢！", Toast.LENGTH_SHORT).show();
					return;
				}
				Intent intent=new Intent(v.getContext(), ZyMyMeetingListActivity.class);
				Bundle bundle=new Bundle();
				bundle.putString("channelId", "2");
				bundle.putString("userId", String.valueOf(userId));
				bundle.putString("companyId", companyId);
				intent.putExtras(bundle);
				startActivity(intent);
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				return;

			}
		});

		Button my_btn_list_7 =(Button)view.findViewById(R.id.my_btn_list_7);
		my_btn_list_7.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
				return;

			}
		});

		Button my_btn_list_9 =(Button)view.findViewById(R.id.my_btn_list_9);
		my_btn_list_9.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				userId = PreferenceUtils.getLong(getActivity(), "userid", -1);//重新获取登录值
				if (userId==-1) {
					Toast.makeText(v.getContext(), "请先登入,谢谢！", Toast.LENGTH_SHORT).show();
					return;
				}
				Intent intent = new Intent(v.getContext(),ZyMyComplainListActivity.class);
				Bundle bundle=new Bundle();
				bundle.putString("channelId", "4");
				bundle.putString("userId", String.valueOf(userId));
				bundle.putString("companyId", companyId);
				intent.putExtras(bundle);
				startActivity(intent);
				((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				return;

			}
		});

		new Handler().postDelayed(new Runnable() {//延迟读取数据
			@Override
			public void run() {
				setLoginInfo();
			}
		}, 200);

		return view;
	}

	@Override
	public void onResume() {//开始执行onCreateView然后onResume，返回的时候执行onResume
		//Log.i("luktel","onResume");
		setLoginInfo();
		super.onResume();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	public void setLoginInfo() {
		TextView my_personal_name = (TextView) view.findViewById(R.id.my_personal_name);
		TextView my_personal_company = (TextView) view.findViewById(R.id.my_personal_company);

		if (userId==-1) {
			dialog.dismiss();
			View my_personal_linearLayout_login =view.findViewById(R.id.my_personal_linearLayout_login);
			my_personal_linearLayout_login.setVisibility(View.VISIBLE);
			my_personal_name.setVisibility(View.VISIBLE);
			my_personal_company.setVisibility(View.INVISIBLE);
			my_personal_name.setText("请登录");
		} else {

			View my_personal_linearLayout_login =view.findViewById(R.id.my_personal_linearLayout_login);
			my_personal_linearLayout_login.setVisibility(View.INVISIBLE);
			/*my_personal_name.setVisibility(View.VISIBLE);
			my_personal_company.setVisibility(View.VISIBLE);
			my_personal_name.setText(userName);
			my_personal_company.setText(userCode);*/

			getData();

		}

	}
	 
	@Override
	public void onClick(View view) {

		if(R.id.btn_my_btn_login==view.getId()){//目前是登陆后才能进入主页
			//Log.i("luktel","login on");
			Intent intent = new Intent(getActivity(),WgLoginActivity.class);
			startActivityForResult(intent, guest_login_result);
			((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			return;
		}

		if (R.id.wg_head_main_btn==view.getId()) {
			Login();
		}

		if(R.id.util_my_head_btn_set==view.getId()){

			Intent intent=new Intent(view.getContext(), ZyMyPersonal.class);//SPCWoDe_ModifyPersonInfo
			Bundle bundle=new Bundle();
			bundle.putString("id", "");
			intent.putExtras(bundle);
			startActivity(intent);
			this.getActivity().overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			return;
		}

		if(R.id.my_btn_list_8==view.getId()){

			//Toast.makeText(view.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
			//return;

			Intent intent=new Intent(view.getContext(), ZyMyPersonal.class);
			Bundle bundle=new Bundle();
			bundle.putString("id", "");
			intent.putExtras(bundle);
			startActivity(intent);
			this.getActivity().overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			return;
		}
		
		if(R.id.wg_logout_btn==view.getId()){
			
			HhApplication.getInstance(view.getContext()).getHhCart().setUser(null);
			//PreferenceUtils.clearSharedPreference(getActivity(), "hh");  //清理之后，每次退出再重新进入再打开新手页面，也就是登录页面

			PreferenceUtils.setLong(getActivity(),"userid",-1);
			//PreferenceUtils.setString(getActivity(),"usercode","");
			PreferenceUtils.setString(getActivity(),"username","");
			PreferenceUtils.setString(getActivity(),"usertype","");
			PreferenceUtils.setString(getActivity(),"userrole","");
			
			AndroidUtils.start(getActivity(), WgLoginActivity.class);
			getActivity().finish();
			((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			return;
		}

		if(R.id.wg_update_btn==view.getId()){
			//Toast.makeText(getActivity(), "wg_update_btn click", Toast.LENGTH_SHORT).show();
			//https://github.com/javiersantos/AppUpdater
			//AppUpdater appUpdater = new AppUpdater(getActivity());
			//appUpdater.start();

			/*
			1. app/build.gradle里面修改最新版本号，打包apk上传到服务器
			2. 服务器的update.xml里面修改最新版本号，如果有新版本，则可以下载
			 */
			new AppUpdater(getActivity())
					//.setUpdateFrom(UpdateFrom.GITHUB)
					//.setGitHubUserAndRepo("javiersantos", "AppUpdater")
					.setUpdateFrom(UpdateFrom.XML)
					.setUpdateXML("http://139.9.1.194:82/zhyq/public/update.xml")
					.setDisplay(Display.DIALOG)
					.showAppUpdated(true)
					.setButtonDoNotShowAgain(null)
					.start();

			/*Intent itcontact = new Intent(view.getContext(),WgDownloadActivity.class);
			startActivity(itcontact);
			((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			return;*/

		}

		return;
	}

	public void Login(){
		Intent intent = new Intent(getActivity(),WgLoginActivity.class);
		startActivityForResult(intent, guest_login_result);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		if(arg3<0) {
	        // 点击的是headerView或者footerView
	        return;
		}
	}

	public void showProgressDialog(){
		dialog = new ProgersssDialog(this.getActivity());
		//dialog.show();
	}

	protected void getData() {
		SysPassValue temp = new SysPassValue();
		temp.setFunctionName("getUserDetails");
		temp.setId(String.valueOf(userId));
		temp.setUserId(String.valueOf(userId));

		new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
			@Override
			public void onPostExecute(String name, YGetTask<SysPassValue> task) {
				showData(task.getValue());
				dialog.dismiss();
			}
		},temp).execute();
	}

	private void showData(SysPassValue passValue) {
		if (passValue!=null) {

			String sid =  passValue.getId();
			String arg1 =  passValue.getArg1();
			String arg2 =  passValue.getArg2();
			String arg3 =  passValue.getArg3();
			String arg4 =  passValue.getArg4();
			String arg5 =  passValue.getArg5();
			String arg6 =  passValue.getArg6();
			String arg7 =  passValue.getArg7();
			String arg8 =  passValue.getArg8();
			String arg9 =  passValue.getArg9();
			String arg10 =  passValue.getArg10();
			String arg11 =  passValue.getArg11();
			String arg12 =  passValue.getArg12();
			String arg13 =  passValue.getArg13();
			String arg14 =  passValue.getArg14();
			String arg15 =  passValue.getArg15();
			String arg16 =  passValue.getArg16();
			String arg17 =  passValue.getArg17();
			String arg18 =  passValue.getArg18();
			String arg19 =  passValue.getArg19();
			String arg20 =  passValue.getArg20();
			String arg38 =  passValue.getArg38();
			String arg42 =  passValue.getArg42();

			TextView my_personal_name = (TextView) view.findViewById(R.id.my_personal_name);
			TextView my_personal_company = (TextView) view.findViewById(R.id.my_personal_company);
			my_personal_name.setText(arg3);
			my_personal_company.setText(arg20);

			String faceImageUrl = arg38;
			companyId = arg42;

			if(!faceImageUrl.equals("")){
				View v1 = view.findViewById(R.id.zy_my_personinfo_img);
				AndroidUtils.setWebImageCircleView(v1, R.id.zy_my_personinfo_img, faceImageUrl);
			}

			//((ImageView)includeTitle.findViewById(R.id.iv_hh_head_masterdetail_right_saygoods)).setBackgroundResource(R.drawable.praisetech);
		}
	}
	
	
	
}
