package com.zhyq.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.GsonRequest;
import com.aspsine.swipetoloadlayout.OnLoadMoreListener;
import com.aspsine.swipetoloadlayout.OnRefreshListener;
import com.aspsine.swipetoloadlayout.SwipeToLoadLayout;
import com.aspsine.swipetoloadlayout.demo.Constants;
import com.aspsine.swipetoloadlayout.demo.adapter.OnChildItemClickListener;
import com.aspsine.swipetoloadlayout.demo.adapter.OnChildItemLongClickListener;
import com.aspsine.swipetoloadlayout.demo.adapter.RecyclerAdapter;
import com.aspsine.swipetoloadlayout.demo.model.Character;
import com.aspsine.swipetoloadlayout.demo.model.SectionCharacters;
import com.zhyq.HhApplication;
import com.zhyq.R;
import com.zhyq.libs.BaseFragment;
import com.zhyq.model.SysPassValue;
import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.sendOrderToServerForListYTask;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class TestRecyclerFragment extends BaseFragment implements OnRefreshListener, OnLoadMoreListener,
        OnChildItemClickListener<Character>,
        OnChildItemLongClickListener<Character> {
    private static final String TAG = TestRecyclerFragment.class.getSimpleName();

    public static final int TYPE_LINEAR = 0;

    public static final int TYPE_GRID = 1;

    public static final int TYPE_STAGGERED_GRID = 2;

    private View view = null;

    private SwipeToLoadLayout swipeToLoadLayout;

    private RecyclerView recyclerView;

    //private SimpleRecyclerAdapter mAdapter;

    private RecyclerAdapter mAdapter;

    private List<SysPassValue> dataList = new ArrayList<SysPassValue>();//数据
    private List<SysPassValue> dataListHeader = new ArrayList<SysPassValue>();//数据
    private int currentPage = 1;

    private int mType;

    private int mPageNum;

    private View headerView = null;

    public static Fragment newInstance(int type) {
        TestRecyclerFragment fragment = new TestRecyclerFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("LAYOUT_MANAGER_TYPE", type);
        fragment.setArguments(bundle);
        return fragment;
    }

    public TestRecyclerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mType = getArguments().getInt("LAYOUT_MANAGER_TYPE", TYPE_LINEAR);
        //mAdapter = new SimpleRecyclerAdapter(mType,dataList);
        mAdapter = new RecyclerAdapter(mType,dataList,dataListHeader);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_twitter_recycler, container, false);
        if (view==null) {
            view = inflater.inflate(R.layout.swipetoloadlayout_fragment_recycler, container, false);
        }
        currentPage = 1;
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        swipeToLoadLayout = (SwipeToLoadLayout) view.findViewById(R.id.swipeToLoadLayout);
        recyclerView = (RecyclerView) view.findViewById(R.id.swipe_target);
        RecyclerView.LayoutManager layoutManager = null;
        if (mType == TYPE_LINEAR) {
            layoutManager = new LinearLayoutManager(getContext());
        } else if (mType == TYPE_GRID) {
            layoutManager = new GridLayoutManager(getContext(), 2);
        } else if (mType == TYPE_STAGGERED_GRID) {
            layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        }
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter);

        //测试能不能增加view
        //headerView = View.inflate(getContext(), R.layout.item_wg_event_detail, null);
        //headerView = LayoutInflater.from(getContext()).inflate(R.layout.item_wg_event_detail, null);
        //swipeToLoadLayout.addView(headerView);

        swipeToLoadLayout.setOnRefreshListener(this);
        swipeToLoadLayout.setOnLoadMoreListener(this);

        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE ){
                    if (!ViewCompat.canScrollVertically(recyclerView, 1)){
                        swipeToLoadLayout.setLoadingMore(true);
                    }
                }
            }
        });

        getListData();
    }

    public void getListData(){

        SysPassValue temp = new SysPassValue();
        temp.setFunctionName("getFeedbackList");//getContactList
        temp.setPageNumber(String.valueOf(this.currentPage));

        new sendOrderToServerForListYTask(new AbstractYGetTaskListener<List<SysPassValue>>() {
            @Override
            public void onPostExecute(String name, YGetTask<List<SysPassValue>> task) {

                showData2(task.getValue());
            }
        },temp).execute();
    }

    public void showData2(List<SysPassValue> list){

        if (list==null){
            this.dataListHeader.clear();
            mAdapter.notifyDataSetChanged();
            return;
        }

        dataListHeader.addAll(list);
        mAdapter.notifyDataSetChanged();

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        swipeToLoadLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeToLoadLayout.setRefreshing(true);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mAdapter.start();
    }

    @Override
    public void onPause() {
        super.onPause();
        HhApplication.getRequestQueue().cancelAll(TAG + "refresh" + mType);
        HhApplication.getRequestQueue().cancelAll(TAG + "loadmore" + mType);
        if (swipeToLoadLayout.isRefreshing()) {
            swipeToLoadLayout.setRefreshing(false);
        }
        if (swipeToLoadLayout.isLoadingMore()) {
            swipeToLoadLayout.setLoadingMore(false);
        }
        mAdapter.stop();
    }

    @Override
    public void onChildItemClick(int groupPosition, int childPosition, Character character, View view) {

    }

    @Override
    public boolean onClickItemLongClick(int groupPosition, int childPosition, Character character, View view) {
        return false;
    }

    @Override
    public void onLoadMore() {
        GsonRequest request = new GsonRequest<SectionCharacters>(Constants.API.CHARACTERS, SectionCharacters.class, new Response.Listener<SectionCharacters>() {
            @Override
            public void onResponse(SectionCharacters characters) {
                /*if (mPageNum < 3) {
                    mPageNum++;
                    mAdapter.append(characters.getSections().subList(mPageNum, mPageNum + 1));
                } else {
                    Toast.makeText(getContext(), "已经到最后一页", Toast.LENGTH_SHORT).show();
                }*/
                getData();
                currentPage++;
                swipeToLoadLayout.setLoadingMore(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                swipeToLoadLayout.setLoadingMore(false);
                volleyError.printStackTrace();
            }
        });
        HhApplication.getRequestQueue().add(request).setTag(TAG + "loadmore" + mType);
    }

    @Override
    public void onRefresh() {
        GsonRequest request = new GsonRequest<SectionCharacters>(Constants.API.CHARACTERS, SectionCharacters.class, new Response.Listener<SectionCharacters>() {
            @Override
            public void onResponse(SectionCharacters characters) {
                mPageNum = 0;
                currentPage = 1;
                getData();
                //mAdapter.setList(characters.getCharacters(), characters.getSections().subList(0, mPageNum + 1));
                swipeToLoadLayout.setRefreshing(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                swipeToLoadLayout.setRefreshing(false);
                volleyError.printStackTrace();
            }
        });
        HhApplication.getRequestQueue().add(request).setTag(TAG + "refresh" + mType);
    }

    public void getData(){

        SysPassValue temp = new SysPassValue();
        temp.setFunctionName("getContactList");//getFeedbackList
        temp.setPageNumber(String.valueOf(this.currentPage));
        temp.setArg1("");

        new sendOrderToServerForListYTask(new AbstractYGetTaskListener<List<SysPassValue>>() {
            @Override
            public void onPostExecute(String name, YGetTask<List<SysPassValue>> task) {

                showData(task.getValue());
            }
        },temp).execute();
    }

    public void showData(List<SysPassValue> list){

        //View vnorecord = view.findViewById(R.id.activity_no_record_showpic);
        if (list==null){
            if(currentPage==1){
                this.dataList.clear();
                mAdapter.notifyDataSetChanged();
                //vnorecord.setVisibility(View.VISIBLE);
                return;
            }//如果到了最后一页，再加载，接口返回来null了，就更新ui
            return;
        }
        //vnorecord.setVisibility(View.GONE);

        //如果第一页则需要清除数据
        if(currentPage==1){
            this.dataList.clear();
            this.dataList.addAll(list);
        }else{
            dataList.addAll(list);
        }
        mAdapter.notifyDataSetChanged();

    }
}
