package com.zhyq.fragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
 
 
import java.util.Date;
 
import java.util.List;

 //com.unitesoft.findzixun.CpptFindzixunMainFragment

 
 
import com.handmark.pulltorefresh.library.ILoadingLayout;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshGridView;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.zhyq.HhApplication;
import com.zhyq.WgTaskViewActivity;
 
 
 
 
import com.zhyq.R;

import com.zhyq.adapter.WgTaskListAdapter;
 
 
import com.zhyq.libs.AndroidUtils;


import com.zhyq.model.CommonList;
 
import com.zhyq.model.User;
 
 
import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
 
 
 
import com.zhyq.task.impl.GetTaskListYTask;


import android.annotation.SuppressLint;
import android.content.Intent;
 
 
import android.os.Bundle;
import android.support.v4.app.Fragment;
 
 
import android.util.Log;
 
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ListView;
 
 
 
 
import android.widget.AdapterView.OnItemClickListener;


@SuppressLint("ValidFragment") 
public class WgTaskFragmentList extends Fragment implements OnItemClickListener,OnClickListener {
	
	public static final String BUNDLE_TITLE = "TITLES";
	private String newstitle = "全部";
	private String fenlei = "All";
 
	private String lastRefreshdt = "";
	
	private PullToRefreshGridView mPullRefreshGridView;//3列
	private PullToRefreshListView mPullRefreshListView;//1列
	
	private View view = null;
    private WgTaskListAdapter mAdapter;//数据适配器
    private List<CommonList> NewsList = new ArrayList<CommonList>();//数据
	private int currentPage = 1; 
   
    public WgTaskFragmentList() {
		super();
		this.newstitle = "All";
	}
    
    public void setFenLei(String fenlei) {
 		this.fenlei = fenlei;
 		onRefresh();
 	}
    
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		AndroidUtils.removeParentView(view);
		if (view==null) {
			view = inflater.inflate(R.layout.tab_item_fragment_main_pulltorefresh, null);
		}
		
		Bundle b = getArguments();
		newstitle = b.getString(BUNDLE_TITLE);//传递过来的栏目
		//Log.i("longhua",newstitle);
		currentPage=1;
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
	}
	
	@Override
	public void onResume() {

		/*
		 *  列表布局，用uniteListAdapter类读取并设置触发，传递列表布局内容过去item_wg_plan_list，并把内容NewsList设置过去，注意布局里的文本图片按钮要设置android:tag=""序号
		 */
		mAdapter = new WgTaskListAdapter(getActivity(),NewsList,"item_wg_task_list");
		mAdapter.setOnClick1(this);//设置列表里的按钮点击事件

		/*
         * 初始化
         */
		//mXGridView = (XGridView) getView().findViewById(R.id.id_activity_xGridView);

		//mPullRefreshGridView = (PullToRefreshGridView) getView().findViewById(R.id.id_activity_pullrefreshGridView);
		mPullRefreshListView = (PullToRefreshListView) getView().findViewById(R.id.id_activity_pullrefreshlistView);

		//初始化值
		initIndicator();
		//设置下拉，上移动 得到效果
		mPullRefreshListView.setOnRefreshListener(new OnRefreshListener2<ListView>()
        {
        	//下拉效果
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView)
            {
            	if(lastRefreshdt.equals("")) {
            		// 显示最后更新的时间
                    refreshView.getLoadingLayoutProxy().setLastUpdatedLabel("刚刚");
       	        } else {
       	        	// 显示最后更新的时间
                    refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(lastRefreshdt);
       	        }

            	SimpleDateFormat    formatter    =   new    SimpleDateFormat    ("MM月dd日    HH:mm:ss     ");
       	        Date    curDate    =   new    Date(System.currentTimeMillis());//获取当前时间
       	        lastRefreshdt =    formatter.format(curDate);

                currentPage=1;
           		LoadDatasTask();
            }

            //上拉效果
            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView)
            {
                currentPage++;
            	LoadDatasTask();
            }
        });

		//获取控件并注册  这个是获取 GridView 控件
        //GridView actualGridView = mPullRefreshListView.getRefreshableView();
        //registerForContextMenu(actualGridView);
        //actualGridView.setAdapter(mAdapter);

		//设置列表数据
		mPullRefreshListView.setAdapter(mAdapter);

        //进来时直接刷新
		onRefresh();
		super.onResume();
	}
	
	private void onRefresh(){
		mPullRefreshListView.setRefreshing(true);
        //首次加载任务数据 加载任务  
        currentPage=1; 
   		LoadDatasTask(); 
	}
	
	private void initIndicator(){
		ILoadingLayout startLabels = mPullRefreshListView.getLoadingLayoutProxy(true,false);
		startLabels.setPullLabel("下拉刷新...");// 刚下拉时，显示的提示
		startLabels.setRefreshingLabel("更新中...");// 刷新时
		startLabels.setReleaseLabel("松开刷新...");// 下来达到一定距离时，显示的提示
		
		ILoadingLayout endLabels = mPullRefreshListView.getLoadingLayoutProxy(false,true);
		endLabels.setPullLabel("上拉刷新...");// 刚上拉时，显示的提示
		endLabels.setRefreshingLabel("更新中...");// 刷新时
		endLabels.setReleaseLabel("松开刷新...");// 上来达到一定距离时，显示的提示
		endLabels.setLastUpdatedLabel("");
	}
	
	/*
     * 记载数据的异步任务
     * 获取列表数据
     */  
	public void LoadDatasTask() {
		
		Long userid = null;
		User curuser = new User();
		curuser = HhApplication.getInstance(this.getActivity()).getHhCart().getUser();
		if(curuser==null){
			userid =-1l;
		}else{
			userid = HhApplication.getInstance(this.getActivity()).getHhCart().getUser().getId();
		}
		
		String strtype = this.newstitle;//栏目
		
		new GetTaskListYTask(new AbstractYGetTaskListener<List<CommonList>>() {
			@Override
			public void onPostExecute(String name, YGetTask<List<CommonList>> task) { 
				 		
				//loading.hideLoading();
				getData(task.getValue());
				mPullRefreshListView.onRefreshComplete();
			}
		},String.valueOf(userid),strtype,currentPage).execute();
	}
	
	public void getData(List<CommonList> list) {
		
		View v1 = view.findViewById(R.id.activity_no_record_showpic);
		if (list==null){
			if(currentPage==1){
				this.NewsList.clear(); 
			    mAdapter.notifyDataSetChanged(); 
			    v1.setVisibility(View.VISIBLE);
			    
			    return;
			}
			
			return;
		}
		
		v1.setVisibility(View.GONE);
		
		if(currentPage==1){
			this.NewsList.clear();
			this.NewsList.addAll(list);
		}else{
			NewsList.addAll(list);
		}
		
		mAdapter.notifyDataSetChanged();	
	}
	
	@Override
	public void onClick(View v) {
		
		if(R.id.wg_plan_list_btn==v.getId()){//这行可以不用判断，因为已经设置wg_plan_list_btn监听多个重复id目前没有问题
			
			//如果使用UniteListAdapter
			//Object tag = v.getTag(R.id.button_tag1);
			//if (tag != null && tag instanceof Integer)
			
			Object tag = v.getTag();//传递参数过去
			
			if (tag != null) {
				
				int position = (Integer) tag;//点击button所在Item中的位置，通过这个位置就可以得到Item中的值
				Long planId =  NewsList.get(position).getId();
				String planName =  NewsList.get(position).getVarValue1();
				
			    //Log.i("longhua","onClick"+planId);
			    Intent in=new Intent(v.getContext(),WgTaskViewActivity.class);
				in.putExtra("taskId", String.valueOf(planId));
				in.putExtra("taskName", planName);
				in.putExtra("className", this.newstitle);
				this.startActivity(in);
				(this.getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			}
		}
		
	}
	
	//该函数的作用是 直接 打开对应的咨询 的连接
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		
		Log.i("longhua","onItemClick");
		
	}
	 
	 
}
