package com.zhyq.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;

import com.aspsine.irecyclerview.IRecyclerView;
import com.aspsine.irecyclerview.OnLoadMoreListener;
import com.aspsine.irecyclerview.OnRefreshListener;
import com.aspsine.irecyclerview.demo.ui.widget.footer.LoadMoreFooterView;
import com.aspsine.irecyclerview.demo.ui.widget.header.BatVsSupperHeaderView;
import com.aspsine.irecyclerview.demo.ui.widget.header.ClassicRefreshHeaderView;
import com.aspsine.irecyclerview.demo.utils.DensityUtils;
import com.zhyq.HhApplication;
import com.zhyq.R;
import com.zhyq.WgPlanViewActivity;
import com.zhyq.ZyWorkViewActivity;
import com.zhyq.adapter.ZyWorkAdapter;
import com.zhyq.libs.AndroidUtils;
import com.zhyq.libs.PreferenceUtils;
import com.zhyq.model.SysPassValue;
import com.zhyq.model.User;
import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.sendOrderToServerForListYTask;

import java.util.ArrayList;
import java.util.List;


@SuppressLint("ValidFragment")
public class ZyWorkFragment extends Fragment implements OnItemClickListener, OnClickListener, OnRefreshListener, OnLoadMoreListener {

    public static final String BUNDLE_TITLE = "TITLES";
    private String className = "全部";
    private String typeName = "All";

    private View view = null;
    private int currentPage = 1;

    private IRecyclerView iRecyclerView;
    private LoadMoreFooterView loadMoreFooterView;
    private ZyWorkAdapter mAdapter;
    private List<SysPassValue> dataList = new ArrayList<SysPassValue>();//数据

    public ZyWorkFragment() {
        super();
        this.className = "All";
    }

    public void setFenLei(String typeName) {
        this.typeName = typeName;
        onRefresh();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        AndroidUtils.removeParentView(view);
        if (view == null) {
            view = inflater.inflate(R.layout.tab_item_fragment_main_irecyclerview, null);
        }

        iRecyclerView = (IRecyclerView) view.findViewById(R.id.tab_common_recyclerView);
        iRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        //toggleRefreshHeader();//切换上拉刷新面板
        loadMoreFooterView = (LoadMoreFooterView) iRecyclerView.getLoadMoreFooterView();

        Bundle b = getArguments();
        className = b.getString(BUNDLE_TITLE);//传递过来的栏目
        //Log.i("luktel",className);
        currentPage = 1;

        mAdapter = new ZyWorkAdapter(dataList);
        mAdapter.setOnClick1(this);
        iRecyclerView.setIAdapter(mAdapter);
        iRecyclerView.setOnRefreshListener(this);
        iRecyclerView.setOnLoadMoreListener(this);
        iRecyclerView.post(new Runnable() {
            @Override
            public void run() {
                iRecyclerView.setRefreshing(true);
            }
        });
        onRefresh();

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onRefresh() {
        loadMoreFooterView.setStatus(LoadMoreFooterView.Status.GONE);
        currentPage = 1;
        getData();
    }

    @Override
    public void onLoadMore() {
        if (loadMoreFooterView.canLoadMore() && mAdapter.getItemCount() > 0) {
            loadMoreFooterView.setStatus(LoadMoreFooterView.Status.LOADING);
            getData();
        }
    }

    private void toggleRefreshHeader() {//两个上拉刷新面板
        if (iRecyclerView.getRefreshHeaderView() instanceof BatVsSupperHeaderView) {
            ClassicRefreshHeaderView classicRefreshHeaderView = new ClassicRefreshHeaderView(getActivity());
            classicRefreshHeaderView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, DensityUtils.dip2px(getActivity(), 80)));
            // we can set view
            iRecyclerView.setRefreshHeaderView(classicRefreshHeaderView);
            //Toast.makeText(this, "Classic style", Toast.LENGTH_SHORT).show();
        } else if (iRecyclerView.getRefreshHeaderView() instanceof ClassicRefreshHeaderView) {
            // we can also set layout
            iRecyclerView.setRefreshHeaderView(R.layout.layout_irecyclerview_refresh_header_batvssupper);
            //Toast.makeText(this, "Bat man vs Super man style", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {

        if (R.id.item_btn1 == v.getId()) {

            //如果使用UniteListAdapter
            //Object tag = v.getTag(R.id.button_tag1);
            //if (tag != null && tag instanceof Integer)

            Object tag = v.getTag();//传递参数过去

            if (tag != null) {

                int position = (Integer) tag;//点击button所在Item中的位置，通过这个位置就可以得到Item中的值
                String id = dataList.get(position).getId();
                String name = dataList.get(position).getArg1();
                String status = dataList.get(position).getArg5();

                //Log.i("luktel","onClick"+taskId);
                Intent in=new Intent(v.getContext(),ZyWorkViewActivity.class);
                in.putExtra("id", id);
                in.putExtra("name", name);
                in.putExtra("status", status);
                in.putExtra("className", this.className);
                Log.i("luktel","className"+this.className);
                this.startActivity(in);
                (this.getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
            }
        }

    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        Log.i("luktel", "onItemClick");
    }

    public void getData() {

        Long userid = null;
        User curuser = new User();
        curuser = HhApplication.getInstance(this.getActivity()).getHhCart().getUser();
        if (curuser == null) {
            userid = -1l;
        } else {
            userid = HhApplication.getInstance(this.getActivity()).getHhCart().getUser().getId();
        }

        Long userId = PreferenceUtils.getLong(getActivity(), "userid", -1);
        String userCode = PreferenceUtils.getString(getActivity(),"usercode","");
        String userName = PreferenceUtils.getString(getActivity(),"username","");
        String userRole = PreferenceUtils.getString(getActivity(),"userrole","");

        String strClass = this.className;//栏目

        SysPassValue temp = new SysPassValue();
        temp.setFunctionName("getWorkList");
        temp.setPageNumber(String.valueOf(this.currentPage));
        temp.setArg1(strClass);
        temp.setUserId(String.valueOf(userId));

        new sendOrderToServerForListYTask(new AbstractYGetTaskListener<List<SysPassValue>>() {
            @Override
            public void onPostExecute(String name, YGetTask<List<SysPassValue>> task) {
                iRecyclerView.setRefreshing(false);
                showData(task.getValue());
            }
        }, temp).execute();
    }

    public void showData(final List<SysPassValue> list) {

        View vnoRecord = view.findViewById(R.id.activity_no_record_showpic);
        if (list == null) {
            loadMoreFooterView.setStatus(LoadMoreFooterView.Status.ERROR);
            //Toast.makeText(this, "网络连接失败", Toast.LENGTH_SHORT).show();

            if (currentPage == 1) {
                this.dataList.clear();
                mAdapter.notifyDataSetChanged();
                vnoRecord.setVisibility(View.GONE);
                return;
            }
            return;
        }

        if (list.size() == 0) {
            if (currentPage == 1) {
                this.dataList.clear();
                mAdapter.notifyDataSetChanged();
                vnoRecord.setVisibility(View.VISIBLE);//如果是第一页没有数据则显示无记录
            } else {
                loadMoreFooterView.setStatus(LoadMoreFooterView.Status.THE_END);
            }
            //Toast.makeText(this, "已经到了最后一页", Toast.LENGTH_SHORT).show();
        } else {
            if (currentPage == 1) {
                currentPage = 2;
                this.dataList.clear();
                this.dataList.addAll(list);
                vnoRecord.setVisibility(View.GONE);
            } else {

                loadMoreFooterView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        currentPage++;
                        loadMoreFooterView.setStatus(LoadMoreFooterView.Status.GONE);
                        dataList.addAll(list);
                    }
                }, 1000);

                //loadMoreFooterView.setStatus(LoadMoreFooterView.Status.GONE);
                //dataList.addAll(list);
            }
            mAdapter.notifyDataSetChanged();
        }
        //vnoRecord.setVisibility(View.GONE);

        if (dataList.size() == 0) {
            //vnoRecord.setVisibility(View.VISIBLE);
        }

        //Log.d("luktel", "showData");

    }


}
