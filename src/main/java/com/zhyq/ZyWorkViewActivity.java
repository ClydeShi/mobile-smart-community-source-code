package com.zhyq;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.zhyq.libs.AndroidUtils;
import com.zhyq.libs.PreferenceUtils;
import com.zhyq.libs.ToastUtils;
import com.zhyq.libs.iosdialog.widget.MyAlertDialog;
import com.zhyq.model.CommonData;
import com.zhyq.model.SysPassValue;
import com.zhyq.model.User;
import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.GetTaskDetailYTask;
import com.zhyq.task.impl.sendOrderToServerForValueYTask;
import com.zhyq.widget.LoadingRelativeLayout;


public class ZyWorkViewActivity extends FragmentActivity implements OnClickListener {
	
	private LoadingRelativeLayout loading = null;
	private View includeTitle;
	private View includeMaster;
	private ImageView returnImage;
	private TextView returnText;
	
	//private FragmentPagerAdapter adapter;
	
	
	private String curTitle = "全部";
	private int cur_position = 0;   // 当前page索引（切换之前）
	private String id = "";
	private String status = "";
	private String className = "";
	private Long userId = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_zy_work_detail);
		loading = LoadingRelativeLayout.getLoadingRelativeLayout(this);
		loading.showLoading();
		includeTitle = this.findViewById(R.id.wg_plandetail_head);
		((TextView)includeTitle.findViewById(R.id.wg_head_title)).setText("查看工单");
		Button btreturn = (Button)includeTitle.findViewById(R.id.wg_head_btn_return);
		btreturn.setOnClickListener(this);
		if(HhApplication.getInstance(this).getHhCart().getUser()==null){
			userId =-1l;
		}else{
			userId = HhApplication.getInstance(this).getHhCart().getUser().getId();
		}
		id = this.getIntent().getStringExtra("id");
		String planName = this.getIntent().getStringExtra("name");
		status = this.getIntent().getStringExtra("status");
		className = this.getIntent().getStringExtra("className");
		Log.i("luktel","status"+status);//Android Monitor logcat显示输出日志
		Log.i("luktel","className"+className);
		
		//this.findViewById(R.id.head_util_btn1).setOnClickListener(this);
		this.findViewById(R.id.head_util_btn2).setOnClickListener(this);
		
		//ImageView imgdel = (ImageView)this.findViewById(R.id.wg_util_head_img_del);
		//ImageView imgedit = (ImageView)this.findViewById(R.id.wg_util_head_img_edit);
		TextView text1 = (TextView)this.findViewById(R.id.head_util_text1);
		TextView text2 = (TextView)this.findViewById(R.id.head_util_text2);
		Button button1 = (Button)this.findViewById(R.id.head_util_btn1);
		Button button2 = (Button)this.findViewById(R.id.head_util_btn2);
		View head_relativelayout_1 = this.findViewById(R.id.head_relativelayout_1);
		View fankuineirong = this.findViewById(R.id.wg_taskdetail_layout12);
		View fankuishijian = this.findViewById(R.id.wg_taskdetail_relativelayout16);
		View wg_layout_box2 = this.findViewById(R.id.wg_layout_box2);
		View wg_taskdetail_line8 = this.findViewById(R.id.wg_taskdetail_line8);
		View wg_taskdetail_relativelayout16 = this.findViewById(R.id.wg_taskdetail_relativelayout16);
		View wg_taskdetail_line10 = this.findViewById(R.id.wg_taskdetail_line10);
		if(status.equals("0")){
			((TextView)includeTitle.findViewById(R.id.head_util_text2)).setText("接收");
			((TextView)includeTitle.findViewById(R.id.head_util_text1)).setText("");
			text1.setVisibility(View.INVISIBLE);
			button1.setVisibility(View.INVISIBLE);
			text2.setVisibility(View.VISIBLE);
			button2.setVisibility(View.VISIBLE);
			//head_relativelayout_1.setVisibility(View.INVISIBLE);
		}else if(status.equals("1")){
			((TextView)includeTitle.findViewById(R.id.head_util_text2)).setText("反馈");
			((TextView)includeTitle.findViewById(R.id.head_util_text1)).setText("");
			text1.setVisibility(View.INVISIBLE);
			button1.setVisibility(View.INVISIBLE);
			text2.setVisibility(View.VISIBLE);
			button2.setVisibility(View.VISIBLE);
		}else{
			((TextView)includeTitle.findViewById(R.id.head_util_text1)).setText("");
			((TextView)includeTitle.findViewById(R.id.head_util_text2)).setText("");
			text1.setVisibility(View.INVISIBLE);
			button1.setVisibility(View.INVISIBLE);
			text2.setVisibility(View.INVISIBLE);
			button2.setVisibility(View.INVISIBLE);
		}

		if(className.equals("已完成")){
			fankuineirong.setVisibility(View.VISIBLE);
			fankuishijian.setVisibility(View.VISIBLE);
			wg_layout_box2.setVisibility(View.VISIBLE);
			wg_taskdetail_line8.setVisibility(View.VISIBLE);
			wg_taskdetail_relativelayout16.setVisibility(View.VISIBLE);
			wg_taskdetail_line10.setVisibility(View.VISIBLE);
		}else{
			fankuineirong.setVisibility(View.INVISIBLE);
			fankuishijian.setVisibility(View.INVISIBLE);
			wg_layout_box2.setVisibility(View.INVISIBLE);
			wg_taskdetail_line8.setVisibility(View.INVISIBLE);
			wg_taskdetail_relativelayout16.setVisibility(View.INVISIBLE);
			wg_taskdetail_line10.setVisibility(View.INVISIBLE);
		}
		
		//AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout2_textview,taskName+taskId);		
		
		//输出日志，设置log tag，手机设为调试模式，在logcat里面就可以看到了
		//Log.i("longhua","goto first1111");
		
		/*
		mastername = this.getIntent().getStringExtra("mastername");
		
		//我是否点赞
		if(ihasdianzhan.equals("Y")){
			//设置手为绿色
			((ImageView)includeTitle.findViewById(R.id.iv_hh_head_masterdetail_right_saygoods)).setBackgroundResource(R.drawable.praisetech); 
		}else{
			
			((ImageView)includeTitle.findViewById(R.id.iv_hh_head_masterdetail_right_saygoods)).setBackgroundResource(R.drawable.praise_black_empty);
		}
	    ((TextView)includeMaster.findViewById(R.id.tv_masterdetail_head_image_mastername)).setText(mastername);
	    */
		
		/*
		 *
		 1. 调用com.unitesoft.task.impl.GetTaskDetailYTask，并传入taskId参数，GetTaskDetailYTask会调用com.unitesoft.data.DataAPI.getTaskDetail()
		 2. DataAPI的方法getTaskDetail会调用DemoDataInterface的方法getTaskDetail返回结果，因为DemoDataInterface是实现DataInterface接口，所以DataInterface也要定义方法getTaskDetail
		 3. 如果是调用远程数据，上面的DataAPI的方法getTaskDetail会调用ClientDataInterface，ClientDataInterface会带参数连接服务器，com.huahua.server.servelt.ClientApiServlet，ClientApiServlet通过判断参数，传入对象，分别调用com.huahua.server.data.DemoDataInterface里的方法，并返回对象
		 4. 使用SysPassValue来作为数据对象传递
		 */
		 
		 /*
		  * 
		  * GetTaskDetailYTask继承YGetTask，调用接口的时候执行构造函数GetTaskDetailYTask，传入YTaskListener和taskId参数
		  * 父类YGetTask会执行onExecute方法，方法里面有_get()，会通过DataAPI.getTaskDetail();返回结果
		  */
		
	    /*new GetTaskDetailYTask(new AbstractYGetTaskListener<CommonData>() {
			@Override
			public void onPostExecute(String name, YGetTask<CommonData> task) {//onPostExecute是AbstractYGetTaskListener里的方法
				
				getData(task.getValue());//task.getValue()是CommonData定义的对象，这个是返回的结果
				loading.hideLoading();
			}
		},planId,String.valueOf(this.userId)).execute();*/

		readData();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
	}
	
	@Override
	public void onClick(View view) {
		
		if(R.id.wg_head_btn_return==view.getId()){
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		}
		
		if(R.id.head_util_btn2==view.getId()){

			if(status.equals("0")){

				final MyAlertDialog dialog1 = new MyAlertDialog(ZyWorkViewActivity.this)
						.builder()
						.setTitle("请确认")
						.setMsg("确认接收该工单，确定吗?")
						.setNegativeButton("取消", new OnClickListener() {
							@Override
							public void onClick(View v) {

							}
						});
				dialog1.setPositiveButton("确认", new OnClickListener() {
					@Override
					public void onClick(View v) {
						setStatus();
					}
				});
				dialog1.show();

			}else if(status.equals("1")){

				Intent intent=new Intent(this, ZyWorkFeedbackActivity.class);
				Bundle bundle=new Bundle();

				Long userid = null;
				User curuser = new User();
				curuser = HhApplication.getInstance(this).getHhCart().getUser();
				if(curuser==null){
					userid =-1l;
				}else{
					userid = HhApplication.getInstance(this).getHhCart().getUser().getId();
				}

				//Long userid = HhApplication.getInstance(this).getHhCart().getUser().getId();
				Long userId = PreferenceUtils.getLong(this, "userid", -1);
				String usercode = PreferenceUtils.getString(this,"usercode","");

				bundle.putString("id", id);
				intent.putExtras(bundle);
				startActivity(intent);

				this.finish();
				overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				return;

			}

		}
		
		if(R.id.head_util_btn1==view.getId()){

		}
		
		return;
	}

	private void readData(){
		SysPassValue temp = new SysPassValue();
		temp.setFunctionName("getWorkDetail");
		temp.setId(String.valueOf(id));
		temp.setUserId(String.valueOf(userId));

		new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
			@Override
			public void onPostExecute(String name, YGetTask<SysPassValue> task) {
				getDataNew(task.getValue());
				loading.hideLoading();
			}
		},temp).execute();
	}

	private void getDataNew(SysPassValue passValue) {
		if (passValue==null) {
			ToastUtils.show(this,"没有获取到信息");
			return;
		} else {
			 /*((TextView)includeMaster.findViewById(R.id.tv_masterdetail_head_image_shopname)).setText(masterviewdetail.getArg1());
			 AndroidUtils.setWebImageView(includeMaster, R.id.hh_masterdetail_head_image_shoplogimage, masterviewdetail.getArg2());
			 AndroidUtils.setWebImageView(includeMaster, R.id.hh_masterdetail_head_image_masterimage, masterviewdetail.getArg3());*/

			String sid =  passValue.getId();
			String arg1 =  passValue.getArg1();
			String arg2 =  passValue.getArg2();
			String arg3 =  passValue.getArg3();
			String arg4 =  passValue.getArg4();
			String arg5 =  passValue.getArg5();
			String arg6 =  passValue.getArg6();
			String arg7 =  passValue.getArg7();
			String arg8 =  passValue.getArg8();
			String arg9 =  passValue.getArg9();
			String arg10 =  passValue.getArg10();

			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout2_textview,arg1);
			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout3_textview,arg2);
			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout4_textview,arg3);
			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout5_textview,arg4);
			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout6_textview,arg5);
			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout7_textview,arg6);
			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout8_textview,arg7);
			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout11_textview,arg8);
			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout9_textview,arg9);
			//AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout10_textview,arg10);

			//((ImageView)includeTitle.findViewById(R.id.iv_hh_head_masterdetail_right_saygoods)).setBackgroundResource(R.drawable.praisetech);
		}
	}
	
	private void getData(CommonData taskdetail) {
		if (taskdetail==null) {
			ToastUtils.show(this,"没有获取到信息");
			return;
		} else {
			 /*((TextView)includeMaster.findViewById(R.id.tv_masterdetail_head_image_shopname)).setText(masterviewdetail.getArg1());  
			 AndroidUtils.setWebImageView(includeMaster, R.id.hh_masterdetail_head_image_shoplogimage, masterviewdetail.getArg2()); 
			 AndroidUtils.setWebImageView(includeMaster, R.id.hh_masterdetail_head_image_masterimage, masterviewdetail.getArg3());*/
			 
			String varValue1 = taskdetail.getVarValue1();
			String varValue2 = taskdetail.getVarValue2();
			String varValue3 = taskdetail.getVarValue3();
			String varValue4 = taskdetail.getVarValue4();
			String varValue5 = taskdetail.getVarValue5();
			String varValue6 = taskdetail.getVarValue6();
			String varValue7 = taskdetail.getVarValue7();
			String varValue8 = taskdetail.getVarValue8();
			String varValue9 = taskdetail.getVarValue9();
			String varValue10 = taskdetail.getVarValue10();
			
			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout2_textview,varValue2);
			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout3_textview,varValue3);
			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout4_textview,varValue4);
			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout5_textview,varValue5);
			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout6_textview,varValue6);
			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout7_textview,varValue7);
			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout8_textview,varValue8);
			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout9_textview,varValue9);
			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout10_textview,varValue10);
			
			//((ImageView)includeTitle.findViewById(R.id.iv_hh_head_masterdetail_right_saygoods)).setBackgroundResource(R.drawable.praisetech);
		}
	}
	
	public void setStatus(){
		loading.showLoading();
		SysPassValue temp = new SysPassValue();
		temp.setFunctionName("setWorkStatus");
		temp.setUserId(String.valueOf(userId));
		temp.setId(this.id);
		temp.setArg1("1");//接收工单
		
		new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
			@Override
			public void onPostExecute(String name, YGetTask<SysPassValue> task) {
				loading.hideLoading();
				getResult(task.getValue());
			}
		},temp).execute();
	}
	
	private void getResult(SysPassValue passValue) {
		if (passValue==null) {
			ToastUtils.show(this, "没有获取到数据！");
		} else {
			String returnResult = passValue.getReturnResult();
			String taskId = passValue.getId();
			if(returnResult.equals("1")){
				this.finish();
				overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
				return;
			}else{
				ToastUtils.show(this, "接收失败！");
			}
		}
	}
		

}
