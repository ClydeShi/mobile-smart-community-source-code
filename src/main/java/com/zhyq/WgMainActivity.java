package com.zhyq;



import com.readystatesoftware.viewbadger.BadgeView;
import com.zhyq.fragment.WgHomeFragmentCompanyHome;
import com.zhyq.fragment.WgHomeFragmentCompanyMy;
import com.zhyq.fragment.ZyContactListFragment;
import com.zhyq.fragment.ZyNewsListFragment;
import com.zhyq.fragment.ZyServiceFragment;
import com.zhyq.libs.AndroidUtils;
import com.zhyq.libs.PreferenceUtils;
import com.zhyq.libs.TabFragmentHost;
import com.zhyq.model.User;

import com.zhyq.fragment.WgHomeFragmentMy;
import com.zhyq.fragment.WgHomeFragmentHome;

import android.R.color;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;

/*
 * 入口主要的，如果要修改com下面里面的目录，1 修改AndroidManifest的package="com.luktel"，2 修改java文件，3 需要修改layout布局，里面调用了lib目录的文件
 * 这里调用子窗口并决定进入哪个子窗口，是否提示新版本
 */

public class WgMainActivity extends FragmentActivity {
	private BadgeView cartBadge = null;
	private static String TAG = "WgMainActivity";
	String versionName = null;
	int size = 0;
	private boolean firstRun = true;  //用来控制 resume 只执行一次
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_wg_main);//https://github.com/niorgai/StatusBarCompat
		//5OZEe0uQLPOWG9g7lyQG5bXC
		//PushManager.startWork(getApplicationContext(),PushConstants.LOGIN_TYPE_API_KEY,"5OZEe0uQLPOWG9g7lyQG5bXC");

		firstRun = true;
		//FragmentTabHost 得到这个
		//FragmentTabHost mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);  
		TabFragmentHost mTabHost = (TabFragmentHost) findViewById(android.R.id.tabhost);  
        mTabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);
        
        //getWindow().setNavigationBarColor(color.white); //写法一
        mTabHost.setBackgroundColor(getResources().getColor(color.white));

		String usertype = PreferenceUtils.getString(this,"usertype","");

		if(usertype.equals("1001") || usertype.equals("1011") || usertype.equals("1021")){//园区人员
			//主页
			View indicator = getIndicatorView("主页", R.drawable.tab_hh_zhuye);
			mTabHost.addTab(mTabHost.newTabSpec("zhuye").setIndicator(indicator),WgHomeFragmentHome.class, null);

			indicator = getIndicatorView("新闻公告", R.drawable.tab_hh_dongtai);
			mTabHost.addTab(mTabHost.newTabSpec("dongtai").setIndicator(indicator), ZyNewsListFragment.class, null);

			indicator = getIndicatorView("通讯录", R.drawable.tab_hh_tongxunlu);
			mTabHost.addTab(mTabHost.newTabSpec("contact").setIndicator(indicator),ZyContactListFragment.class, null);

			indicator = getIndicatorView("我的", R.drawable.tab_hh_wode);
			mTabHost.addTab(mTabHost.newTabSpec("wode").setIndicator(indicator), WgHomeFragmentMy.class, null);
		}else{//企业人员1100
			//主页
			View indicator = getIndicatorView("大厅", R.drawable.tab_hh_dating);
			mTabHost.addTab(mTabHost.newTabSpec("zhuye").setIndicator(indicator),WgHomeFragmentCompanyHome.class, null);

			indicator = getIndicatorView("活动新闻", R.drawable.tab_hh_huodong);
			mTabHost.addTab(mTabHost.newTabSpec("dongtai").setIndicator(indicator), ZyNewsListFragment.class, null);

			indicator = getIndicatorView("服务", R.drawable.tab_hh_fuwu);
			mTabHost.addTab(mTabHost.newTabSpec("contact").setIndicator(indicator),ZyServiceFragment.class, null);

			indicator = getIndicatorView("我的", R.drawable.tab_hh_wodec);
			mTabHost.addTab(mTabHost.newTabSpec("wode").setIndicator(indicator), WgHomeFragmentCompanyMy.class, null);
		}
        
        /*
        indicator = getIndicatorView("动态", R.drawable.tab_hh_dongtai);
        mTabHost.addTab(
        		mTabHost.newTabSpec("dongtai").setIndicator(indicator), HhDynamicNewsFragmentActivity.class, null);
        
        indicator = getIndicatorView("活动", R.drawable.tab_hh_huodong);
        mTabHost.addTab(
        		mTabHost.newTabSpec("huodong").setIndicator(indicator),HhHomeFragmentActivity.class, null);
        
        indicator = getIndicatorView("产品", R.drawable.tab_hh_fuwu);
        mTabHost.addTab(
        		mTabHost.newTabSpec("chanping").setIndicator(indicator),SPCHome_FragmentHuoDong.class, null);
        
        indicator = getIndicatorView("我的", R.drawable.tab_hh_wode);
        mTabHost.addTab(
        		mTabHost.newTabSpec("wode").setIndicator(indicator), HhHomeFragmentWode.class, null);
        */
        
        new Handler().postDelayed(new Runnable(){
			@Override
			public void run(){
				//update();
				//发布的时候再显示
				}
			}, 5000);
        
	}
	
	private View getIndicatorView(String text,int imgId) {  
		
		//得到一个布局的句柄
        View v = getLayoutInflater().inflate(R.layout.component_tab_indicator, null);
        
        //设置标题
        AndroidUtils.setTextView(v, R.id.txtCount, text);
        ImageView i=(ImageView)v.findViewById(R.id.tab_item_image_id);
        
        //设置最下面的图片
        i.setImageResource(imgId);
        
        //如果是购物车的话 设置数字提醒 的标识
        if (imgId==R.drawable.tab_hh_wode) {
        	cartBadge=new BadgeView(this,i);
     		cartBadge.setBadgePosition(BadgeView.POSITION_TOP_RIGHT);
     		cartBadge.setBadgeMargin(2);
        }
        
        return v;
    }
	
	public void update() {

		/*
		UpdateManager manager = new UpdateManager(this);
        UpdateOptions options = new UpdateOptions.Builder(this)
        		.checkUrl("http://www.luktel.com/update/updateinfo.xml")
                .updateFormat(UpdateFormat.XML)
                .updatePeriod(new UpdatePeriod(UpdatePeriod.EACH_ONE_DAY))
                .checkPackageName(true)
                .build(); 
        manager.check(this.getApplicationContext(), options);
        */

	}
	
	@Override 
	public void finish() {
		super.finish();
	}
	
	public void GetTips(){
		
		User user = HhApplication.getInstance(this).getHhCart().getUser();
		
		if(user==null){
			 return;
		}
		
		String userid = HhApplication.getInstance(this).getHhCart().getUser().getCode();
		
		/*
		UserTips record = new UserTips();
		record.setId(1l);
		record.setName(userid);
		
		new GetUserTipsYTask(new AbstractYGetTaskListener<UserTips>() {
			@Override
			public void onPostExecute(String name, YGetTask<UserTips> task) { 
				GetData(task.getValue());					
			} 
		},record).execute();
		*/
	}
	
	/*
	private void GetData(UserTips record) {
		 
		if (record==null) { 
			ToastUtils.show(this, R.string.show_logon_error_msg);
			return;
		} else { 
			
			size = record.getWaitpayorderqty();
			//Log.i("huahua", "gouwuche="+String.valueOf(size));
			
			 //showCartBadge();
		}
	}
	*/
	
	//  该功能是用来设置提示的 小图标
	private void showCartBadge() { 
		if (size>0) {
			cartBadge.setText(String.valueOf(size));
			cartBadge.show();
		} else {
			cartBadge.hide();
		}
	}
	
	@Override
	protected void onResume() {   //注意 该函数，如果在打开了其他窗口，再关闭，这个函数也会执行
		super.onResume();
		//GetTips();
		if(firstRun==false) return;
		//直接到中间  	 
		new Handler().postDelayed(new Runnable(){
			@Override
			public void run(){
				//直接默认 定位到中间那个 页面
				//Log.i("longhua","gotoTab-3"); 
				gotoTab(0);
				firstRun=false;
			}
		}, 1000);
		
	}
	
	public void gotoMain() {
		//直接定位到中间
		gotoTab(0);
	}
	
	public void gotoSearch() {
		gotoTab(0);
	}
	
	public void gotoTab(int index) {
		//FragmentTabHost mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
		TabFragmentHost mTabHost = (TabFragmentHost) findViewById(android.R.id.tabhost);
		mTabHost.setCurrentTab(index);
	}
	
	@Override  
    public boolean onKeyDown(int keyCode, KeyEvent event){
        if (keyCode == KeyEvent.KEYCODE_BACK ){
            // 创建退出对话框
            AlertDialog isExit = new AlertDialog.Builder(this).create();
            // 设置对话框标题
            isExit.setTitle("系统提示");
            // 设置对话框消息
            isExit.setMessage("确定要退出智慧园区系统吗");
            // 添加选择按钮并注册监听
            isExit.setButton("确定", listener);
            isExit.setButton2("取消", listener);
            // 显示对话框
            isExit.show();
        }
        return false;
    }
	
    //监听对话框里面的button点击事件
    DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener()
    {
        public void onClick(DialogInterface dialog, int which)
        {
            switch (which)
            {
	            case AlertDialog.BUTTON_POSITIVE:// "确认"按钮退出程序
	                finish();
	                break;
	            case AlertDialog.BUTTON_NEGATIVE:// "取消"第二个按钮取消对话框
	                break;
	            default:
	                break;
            }
        }
    };
    
    //处理 登入的返回值，因为fragment 不能直接启动回调函数
    @Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		//结果码不等于取消时候
    	
		if (resultCode == WgLoginActivity.request_login_result) {
			
			super.onActivityResult(requestCode, resultCode, data);
			
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
    
    
    
}
