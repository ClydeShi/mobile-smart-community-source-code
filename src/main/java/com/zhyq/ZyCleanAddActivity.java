package com.zhyq;


import android.app.Activity;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

/*
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.widget.DatePicker;
import android.widget.TimePicker;
*/

import com.bigkoo.pickerview.TimePickerView;
import com.util.common.StrEdit;
import com.zhyq.libs.AndroidUtils;
import com.zhyq.libs.PreferenceUtils;
import com.zhyq.libs.ProgersssDialog;
import com.zhyq.libs.ToastUtils;
import com.zhyq.libs.view.wheelview.JudgeDate;
import com.zhyq.libs.view.wheelview.ScreenInfo;
import com.zhyq.libs.view.wheelview.WheelMain;
import com.zhyq.model.SysPassValue;
import com.zhyq.model.User;
import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.sendOrderToServerForValueYTask;
import com.zhyq.widget.LoadingRelativeLayout;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class ZyCleanAddActivity extends Activity implements OnClickListener {

	static boolean active = false;

	private LoadingRelativeLayout loading=null;
	private View includeTitle ;
	private ImageView returnImage;
	private TextView returnText;
	private Button btSave;
	private Button topSave;
	private Button btDel;
	WheelMain wheelMain;
	private PopupWindow popupWindow;
	private Long addressId = 0l;
	private float alpha = 1f;

	TextView txtstartdate;
	TextView txtenddate;
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	public ProgersssDialog dialog;

	private int id = 0;
	private Long userId = null;

	public static int request_Code = 9;
	public static int resultCode = 11;

	private String channelId = "2";
	private String channelName = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		showProgressDialog();

		this.setContentView(R.layout.activity_zy_clean_add);
		loading = LoadingRelativeLayout.getLoadingRelativeLayout(this);
		includeTitle = this.findViewById(R.id.zy_add_head);

		channelId = this.getIntent().getStringExtra("channelId");
		if(channelId==null){
			channelId = "1";
		}
		if(channelId.equals("1")){
			channelName = "保洁";
		}else if(channelId.equals("2")){
			channelName = "维修";
		}
		//Log.i("luktel","channelName"+channelName);

		((TextView)includeTitle.findViewById(R.id.wg_head_add_title)).setText(channelName+"申请");
		Button btreturn = (Button)includeTitle.findViewById(R.id.wg_head_add_btn_return);
		btreturn.setOnClickListener(this);

		((TextView)includeTitle.findViewById(R.id.wg_util_head_text_add)).setText("提交");

		((TextView)this.findViewById(R.id.zy_add_textviewtitle6)).setText(channelName+"内容");

		topSave =(Button)includeTitle.findViewById(R.id.wg_util_head_btn_add);
		topSave.setOnClickListener(this);

		btSave =(Button)this.findViewById(R.id.zy_save_button);
		btSave.setOnClickListener(this);

		(this.findViewById(R.id.zy_add_button_01)).setOnClickListener(this);
		//(this.findViewById(R.id.zy_add_button_02)).setOnClickListener(this);

		txtstartdate = ((TextView)this.findViewById(R.id.zy_field_05));
		this.findViewById(R.id.btn_start_date).setOnClickListener(this);

		/*(this.findViewById(R.id.wg_planadd5_btn)).setOnClickListener(this);
		(this.findViewById(R.id.wg_planadd6_btn)).setOnClickListener(this);
		(this.findViewById(R.id.wg_planadd7_btn)).setOnClickListener(this);

		txtenddate = ((TextView)this.findViewById(R.id.wg_date_end));
		this.findViewById(R.id.btn_end_date).setOnClickListener(this);*/

		id = StrEdit.StrToInt(this.getIntent().getStringExtra("id"));

		userId = PreferenceUtils.getLong(this,"userid",-1);

		if(userId>0){
			new Handler().postDelayed(new Runnable() {//延迟读取数据
				@Override
				public void run() {
					getData();//获取企业名称
					getArticleData();//获取服务介绍
				}
			}, 200);
			//loading.showLoading();
		}

		//ColorStateList redColor=getResources().getColorStateList(R.color.red);//删除按钮字体设置为红色
		//btDel.setTextColor(redColor);
	}

	public void showProgressDialog(){
		dialog = new ProgersssDialog(this);
		//dialog.show();
	}

	@Override
	protected void onResume() {
		super.onResume();

	}

	protected void getData() {
		SysPassValue temp = new SysPassValue();
		temp.setFunctionName("getUserDetail");
		temp.setId(String.valueOf(id));
		temp.setUserId(String.valueOf(userId));

		new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
			@Override
			public void onPostExecute(String name, YGetTask<SysPassValue> task) {
				showData(task.getValue());
				dialog.dismiss();
				//loading.hideLoading();
			}
		},temp).execute();
		/*new GetTaskDetailYTask(new AbstractYGetTaskListener<CommonData>() {
			@Override
			public void onPostExecute(String name, YGetTask<CommonData> task) {//onPostExecute是AbstractYGetTaskListener里的方法

				getData(task.getValue());//task.getValue()是CommonData定义的对象，这个是返回的结果
				loading.hideLoading();
			}
		},String.valueOf(planId),String.valueOf(this.userId)).execute();*/
	}

	protected void getArticleData() {
		SysPassValue temp = new SysPassValue();
		temp.setFunctionName("getArticleText1");
		temp.setId(String.valueOf(id));
		if(channelId.equals("1")){
			temp.setArg1("1002");
		}else if(channelId.equals("2")){
			temp.setArg1("1001");
		}

		new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
			@Override
			public void onPostExecute(String name, YGetTask<SysPassValue> task) {
				showArticleData(task.getValue());
				dialog.dismiss();
				//loading.hideLoading();
			}
		},temp).execute();
	}

	private void showData(SysPassValue passValue) {
		if (passValue==null) {
			ToastUtils.show(this,"没有获取到信息");
			return;
		} else {
			 /*((TextView)includeMaster.findViewById(R.id.tv_masterdetail_head_image_shopname)).setText(masterviewdetail.getArg1());
			 AndroidUtils.setWebImageView(includeMaster, R.id.hh_masterdetail_head_image_shoplogimage, masterviewdetail.getArg2());
			 AndroidUtils.setWebImageView(includeMaster, R.id.hh_masterdetail_head_image_masterimage, masterviewdetail.getArg3());*/

			String sid =  passValue.getId();
			String arg1 =  passValue.getArg1();
			String arg2 =  passValue.getArg2();
			String arg3 =  passValue.getArg3();
			String arg4 =  passValue.getArg4();
			String arg5 =  passValue.getArg5();
			String arg6 =  passValue.getArg6();
			String arg7 =  passValue.getArg7();
			String arg8 =  passValue.getArg8();
			String arg9 =  passValue.getArg9();
			String arg10 =  passValue.getArg10();
			String arg11 =  passValue.getArg11();
			String arg12 =  passValue.getArg12();
			String arg13 =  passValue.getArg13();
			String arg14 =  passValue.getArg14();
			String arg15 =  passValue.getArg15();

			AndroidUtils.setTextView(this, R.id.zy_field_01id,arg13);
			AndroidUtils.setTextView(this, R.id.zy_field_01,arg8);
			AndroidUtils.setEditText(this, R.id.zy_field_02,arg15);
			AndroidUtils.setEditText(this, R.id.zy_field_03,arg2);
			AndroidUtils.setEditText(this, R.id.zy_field_04,arg4);

			//((ImageView)includeTitle.findViewById(R.id.iv_hh_head_masterdetail_right_saygoods)).setBackgroundResource(R.drawable.praisetech);
		}
	}

	private void showArticleData(SysPassValue passValue) {
		if (passValue==null) {
			ToastUtils.show(this,"没有获取到信息");
			return;
		} else {

			String arg1 =  passValue.getArg1();
			String arg2 =  passValue.getArg2();

			AndroidUtils.setTextView(this, R.id.zy_article_desc,arg2);

		}
	}

	@Override
	public void onClick(View view) {

		if(R.id.wg_head_add_btn_return==view.getId()){
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		}

		if(R.id.zy_save_button==view.getId()){
			SavePlan();
			return;
		}

		if(R.id.wg_util_head_btn_add==view.getId()){
			SavePlan();
			return;
		}

		/*if(R.id.zy_add_button_01==view.getId()){//选择楼栋

			Long userId = PreferenceUtils.getLong(this, "userid", -1);
			String usercode = PreferenceUtils.getString(this,"usercode","");

			Intent intent = new Intent(this, ZySelectBuildingActivity.class);//WgPlanSelectCategoryActivity  WgPlanSelectWgUserNewActivity
			Bundle bundle = new Bundle();
			bundle.putString("clickfrom", "problemadd");
			bundle.putString("userId", String.valueOf(userId));
			intent.putExtras(bundle);
			startActivityForResult(intent,request_Code);//这里的request_Code要大于0，现在用的9是用来onActivityResult里判断由这里发出的新Activity，获取返回值的请求码
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);//设置新面板右边进入，原来面板左边退出
			return;
		}

		if(R.id.zy_add_button_02==view.getId()) {//紧急程度

			Intent intent = new Intent(this, ZyUrgentSelectActivity.class);
			Bundle bundle = new Bundle();
			bundle.putString("clickfrom", "problemadd");
			intent.putExtras(bundle);
			startActivityForResult(intent,request_Code);
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);//设置新面板右边进入，原来面板左边退出
			return;
		}*/

		if(R.id.btn_start_date==view.getId()){
			initTimePicker(txtstartdate);//https://blog.csdn.net/m0_37794706/article/details/78903576
			/*
			java.util.Calendar cl = Calendar.getInstance();
			showDatePickerDialog(this,2,txtstartdate,cl);//https://blog.csdn.net/qq_33756493/article/details/78120743
			*/
			/*bottombirthdaywindow(txtstartdate, "txtstartdate");
			new Thread(new Runnable(){
                @Override
                public void run() {
                    while(alpha>0.5f){
                        try {
                            //4是根据弹出动画时间和减少的透明度计算
                            Thread.sleep(4);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Message msg =mHandler.obtainMessage();
                        msg.what = 1;
                        //每次减少0.01，精度越高，变暗的效果越流畅
                        alpha-=0.01f;
                        msg.obj =alpha ;
                        mHandler.sendMessage(msg);
                    }
                }

            }).start();*/
		}

		/*if(R.id.btn_end_date==view.getId()){
			bottombirthdaywindow(txtenddate, "txtenddate");
			new Thread(new Runnable(){
                @Override
                public void run() {
                    while(alpha>0.5f){
                        try {
                            //4是根据弹出动画时间和减少的透明度计算
                            Thread.sleep(4);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Message msg =mHandler.obtainMessage();
                        msg.what = 1;
                        //每次减少0.01，精度越高，变暗的效果越流畅
                        alpha-=0.01f;
                        msg.obj =alpha ;
                        mHandler.sendMessage(msg);
                    }
                }

            }).start();
		}*/
	}

	private void initTimePicker1(final TextView tvTime){
		TimePickerView pvTime = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
			@Override
			public void onTimeSelect(Date date,View v) {//选中事件回调
				tvTime.setText(getTime(date));
			}
		}).build();
		pvTime.setDate(Calendar.getInstance());//注：根据需求来决定是否使用该方法（一般是精确到秒的情况），此项可以在弹出选择器的时候重新设置当前时间，避免在初始化之后由于时间已经设定，导致选中时间与当前时间不匹配的问题。
		pvTime.show();
	}

	private void initTimePicker(final TextView tvTime) {//选择出生年月日
		//控制时间范围(如果不设置范围，则使用默认时间1900-2100年，此段代码可注释)
		//因为系统Calendar的月份是从0-11的,所以如果是调用Calendar的set方法来设置时间,月份的范围也要是从0-11
		Date curDate = new Date(System.currentTimeMillis());//获取当前时间
		SimpleDateFormat formatter_year = new SimpleDateFormat("yyyy ");
		String year_str = formatter_year.format(curDate);
		int year_int = (int) Double.parseDouble(year_str);

		SimpleDateFormat formatter_mouth = new SimpleDateFormat("MM ");
		String mouth_str = formatter_mouth.format(curDate);
		int mouth_int = (int) Double.parseDouble(mouth_str);

		SimpleDateFormat formatter_day = new SimpleDateFormat("dd ");
		String day_str = formatter_day.format(curDate);
		int day_int = (int) Double.parseDouble(day_str);

		Calendar selectedDate = Calendar.getInstance();//系统当前时间
		Calendar startDate = Calendar.getInstance();
		startDate.set(1900, 0, 1);
		Calendar endDate = Calendar.getInstance();
		endDate.set(year_int, mouth_int - 1, day_int);

		//时间选择器
		TimePickerView pvTime = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
			@Override
			public void onTimeSelect(Date date, View v) {//选中事件回调
				// 这里回调过来的v,就是show()方法里面所添加的 View 参数，如果show的时候没有添加参数，v则为null
				tvTime.setText(getTime(date));
			}
		})

				.setType(new boolean[]{true, true, true, true, true, false}) //年月日时分秒 的显示与否，不设置则默认全部显示
				.setLabel("年", "月", "日", "时", "分", "")//默认设置为年月日时分秒
				.isCenterLabel(false)
				.setDividerColor(Color.RED)
				.setTextColorCenter(Color.RED)//设置选中项的颜色
				.setTextColorOut(Color.BLACK)//设置没有被选中项的颜色
				.setContentSize(21)
				.setDate(selectedDate)
				.setLineSpacingMultiplier(1.2f)
				.setTextXOffset(-10, 0,10, 0, 0, 0)//设置X轴倾斜角度[ -90 , 90°]
				//.setRangDate(startDate, endDate)
				//.setBackgroundId(0x00FFFFFF) //设置外部遮罩颜色
				.setDecorView(null)
				.build();
		pvTime.setDate(Calendar.getInstance());//注：根据需求来决定是否使用该方法（一般是精确到秒的情况），此项可以在弹出选择器的时候重新设置当前时间，避免在初始化之后由于时间已经设定，导致选中时间与当前时间不匹配的问题。
		pvTime.show();
	}

	private String getTime(Date date) {//可根据需要自行截取数据显示
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");//yyyy-MM-dd HH:mm:ss
		//SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.format(date);
	}

	/**
	 * 日期选择
	 */
	/*
	public static void showDatePickerDialog(Activity activity, int themeResId, final TextView tv, Calendar calendar) { // 直接创建一个DatePickerDialog对话框实例，并将它显示出来
		new DatePickerDialog(activity , themeResId,new DatePickerDialog.OnDateSetListener() { // 绑定监听器(How the parent is notified that the date is set.)
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) { // 此处得到选择的时间，可以进行你想要的操作
				int month = monthOfYear + 1;
				String months = String.valueOf(month);
				if(month<10){
					months = "0" + month;
				}
				String days = String.valueOf(dayOfMonth);
				if(dayOfMonth<10){
					days = "0" + dayOfMonth;
				}
				tv.setText(year + "-" + months + "-" + days + "");

			} } // 设置初始日期
				, calendar.get(Calendar.YEAR) ,calendar.get(Calendar.MONTH) ,calendar.get(Calendar.DAY_OF_MONTH)).show();
	}
	*/

	/**
	 * 时间选择
	 */
	/*
	public static void showTimePickerDialog(Activity activity, int themeResId, final TextView tv, Calendar calendar) {
		// Calendar c = Calendar.getInstance();
		// 创建一个TimePickerDialog实例，并把它显示出来
		// 解释一哈，Activity是context的子类
		new TimePickerDialog( activity,themeResId,
				// 绑定监听器
				new TimePickerDialog.OnTimeSetListener() {
					@Override
					public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
						tv.setText("您选择了：" + hourOfDay + "时" + minute  + "分");
					}
				}
				// 设置初始时间
				, calendar.get(Calendar.HOUR_OF_DAY)
				, calendar.get(Calendar.MINUTE)
				// true表示采用24小时制
				,true).show();
	}
	*/

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == request_Code) {

			if (resultCode == 1){//结果标识
				if (data != null) {
					Bundle b = data.getExtras();
					String categoryId = b.getString("id");
	         	 	String categoryName = b.getString("name");
	         	 	AndroidUtils.setTextView(this, R.id.zy_field_01id,categoryId);
	            	AndroidUtils.setTextView(this, R.id.zy_field_01,categoryName);
				}
			}

			if (resultCode == 2){//紧急程度
				if (data != null) {
					Bundle b = data.getExtras();
	         	 	String name = b.getString("name");
	         	 	if(!name.equals("")){
	         	 		AndroidUtils.setTextView(this, R.id.zy_field_02,name);
	         	 	}
				}
			}

			if (resultCode == 3){//网格员
				if (data != null) {
					Bundle b = data.getExtras();
					//String id = b.getString("id");
					String name = b.getString("name");
					if(!name.equals("")){
						AndroidUtils.setTextView(this, R.id.wg_planadd_layout6_textview3,name);
					}
				}
			}

			if (resultCode == 5){//检查人员
				if (data != null) {
					Bundle b = data.getExtras();
					String id = b.getString("id");
					String name = b.getString("name");
					if(!name.equals("")){
						AndroidUtils.setTextView(this, R.id.wg_planadd_layout7_textview3_id,id);
		            	AndroidUtils.setTextView(this, R.id.wg_planadd_layout7_textview3,name);
					}
				}
			}

			if (resultCode == 10){//街道
				if (data != null) {
					Bundle b = data.getExtras();
	         	 	String name = b.getString("name");
	         	 	if(!name.equals("")){
	         	 		AndroidUtils.setTextView(this, R.id.wg_planadd_layout4_textview3,name);
	         	 	}
				}
			}
		}

		super.onActivityResult(requestCode, resultCode, data);
	}

	public void SavePlan(){
    	Long userid = null;
    	User curuser = new User();
    	curuser = HhApplication.getInstance(this).getHhCart().getUser();
    	if(curuser==null){
    		userid =-1l;
      	}else{
      		userid = HhApplication.getInstance(this).getHhCart().getUser().getId();
      	}
    	//Long userid = HhApplication.getInstance(this).getHhCart().getUser().getId();//未登录会出错

    	Long userId = PreferenceUtils.getLong(this,"userid",-1);
    	String userCode = PreferenceUtils.getString(this,"usercode","");
		String userName = PreferenceUtils.getString(this,"username","");

		String zy_field_01id = ((TextView)this.findViewById(R.id.zy_field_01id)).getText().toString();
		String zy_field_01 = ((TextView)this.findViewById(R.id.zy_field_01)).getText().toString();
		String zy_field_02 = AndroidUtils.getEditText(this, R.id.zy_field_02);
		String zy_field_03 = AndroidUtils.getEditText(this, R.id.zy_field_03);
		String zy_field_04 = AndroidUtils.getEditText(this, R.id.zy_field_04);
		String zy_field_05 = ((TextView)this.findViewById(R.id.zy_field_05)).getText().toString();
		String zy_field_06 = AndroidUtils.getEditText(this, R.id.zy_field_06);
		//String date1 = StrEdit.getDate("yyyy-MM-dd HH:mm:ss");

  	    /*String title = AndroidUtils.getEditText(this, R.id.wg_planadd_edittext_name);

  	    TextView txtcategory = ((TextView)this.findViewById(R.id.wg_planadd_layout3_textview3_id));
		String category = txtcategory.getText().toString();*/

  	    /*if(buildingName.equals("") || buildingName.equals("请选择")){
  	    	ToastUtils.show(this, "请选择楼栋！");
  		    return;
  	    }*/

  	    if(zy_field_02.equals("")){
	    	ToastUtils.show(this, "请输入详细地址！");
		    return;
	    }

  	    if(zy_field_03.equals("")){
  	    	ToastUtils.show(this, "请输入联系人！");
  		    return;
  	    }

  	    if(zy_field_04.equals("")){
	    	ToastUtils.show(this, "请输入联系电话！");
		    return;
	    }

  	    if(zy_field_05.equals("")||zy_field_05.equals("请选择")){
	    	ToastUtils.show(this, "请选择预约时间！");
		    return;
	    }

		if(zy_field_06.equals("")){
			ToastUtils.show(this, "请填写要求内容！");
			return;
		}

  	    /*if(date1.equals("请选择")){
	    	ToastUtils.show(this, "请选择发现时间！");
		    return;
	    }*/

  	    /*if(enddate.equals("请选择")){
	    	ToastUtils.show(this, "请选择结束时间！");
		    return;
	    }*/

  	    loading.showLoading();

  	    SysPassValue temp = new SysPassValue();//buildingId buildingName address degree date1 title content
		temp.setFunctionName("saveCleanAdd");
		temp.setUserId(String.valueOf(userId));
		temp.setId(String.valueOf(id));
		temp.setArg1(channelId);
		temp.setArg2(zy_field_01);
		temp.setArg3(zy_field_02);
		temp.setArg4(zy_field_03);
		temp.setArg5(zy_field_04);
		temp.setArg6(userName);
		temp.setArg7(zy_field_01id);//企业id
		temp.setArg8(zy_field_06);
		temp.setArg9(zy_field_05);

		new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
			@Override
			public void onPostExecute(String name, YGetTask<SysPassValue> task) {
				loading.hideLoading();
				getData(task.getValue());
			}
		},temp).execute();
	}

	private void getData(SysPassValue passValue) {
		if (passValue==null) {
			ToastUtils.show(this, "保存失败！");
		} else {
			String returnResult = passValue.getReturnResult();
			String planId = "";
			if(returnResult.equals("1")){
				Intent intent=new Intent(this, ZySaveSuccessActivity.class);
				Bundle bundle=new Bundle();
				bundle.putString("id", String.valueOf(planId));
				intent.putExtras(bundle);
				startActivity(intent);
				this.finish();
				overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				return;
			}else{
				ToastUtils.show(this, "保存失败，请检查输入的文字！");
			}
		}
	}

    void bottomSelectwindow(TextView view) {
        if (popupWindow != null && popupWindow.isShowing()) {
            return;
        }

      //修改的话，这个地方要修改 *****************
        LinearLayout layout = (LinearLayout) getLayoutInflater().inflate(R.layout.activity_wg_street_popupwindow, null);
        popupWindow = new PopupWindow(layout,
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        //点击空白处时，隐藏掉pop窗口
        popupWindow.setFocusable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        //添加弹出、弹入的动画
        popupWindow.setAnimationStyle(R.style.Popupwindow);
        int[] location = new int[2];
        view.getLocationOnScreen(location);
        popupWindow.showAtLocation(view, Gravity.LEFT | Gravity.BOTTOM, 0, -location[1]);

        //修改的话，这个地方要修改 *****************
        //添加按键事件监听
        setSelectButtonListeners(layout);
        //添加pop窗口关闭事件，主要是实现关闭时改变背景的透明度
        popupWindow.setOnDismissListener(new poponDismissListener());
        backgroundAlpha(1f);
    }

    private void setSelectButtonListeners(LinearLayout layout) {

        Button btn_street_lh = (Button) layout.findViewById(R.id.btn_street_lh);
        Button btn_street_mz = (Button) layout.findViewById(R.id.btn_street_mz);
        Button btn_street_gh = (Button) layout.findViewById(R.id.btn_street_gh);
        Button btn_street_hc = (Button) layout.findViewById(R.id.btn_street_hc);
        Button btn_street_gl = (Button) layout.findViewById(R.id.btn_street_gl);
        Button btn_street_dl = (Button) layout.findViewById(R.id.btn_street_dl);
        Button bt_cancel = (Button) layout.findViewById(R.id.btn_sex_cancel);

        btn_street_lh.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                    //在此处添加你的按键处理
                    popupWindow.dismiss();
                    backgroundAlpha(1f);
                    //loading.showLoading();
                    userModifyData("1","龙华");
                    /*
        			user.setUserSex("男");
        			user.setUserModifyColumnType("2");

        			new HhUserModifyPersonInfoYTask(new AbstractYGetTaskListener<User>() {
        				@Override
        				public void onPostExecute(String name, YGetTask<User> task) {
        					loading.hideLoading();
        					userModifyData(task.getValue(),"2");
        				}
        			},user,"2").execute();
        			*/

                    return;
                }
            }
        });
        btn_street_mz.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                	 //在此处添加你的按键处理
                    popupWindow.dismiss();
                    backgroundAlpha(1f);
                    userModifyData("1","民治");
                    return;
                }
            }
        });
        btn_street_gh.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                	 //在此处添加你的按键处理
                    popupWindow.dismiss();
                    backgroundAlpha(1f);
                    userModifyData("1","观湖");
                    return;
                }
            }
        });
        btn_street_hc.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                	 //在此处添加你的按键处理
                    popupWindow.dismiss();
                    backgroundAlpha(1f);
                    userModifyData("1","福城");
                    return;
                }
            }
        });
        btn_street_gl.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                	 //在此处添加你的按键处理
                    popupWindow.dismiss();
                    backgroundAlpha(1f);
                    userModifyData("1","观澜");
                    return;
                }
            }
        });
        btn_street_dl.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                	 //在此处添加你的按键处理
                    popupWindow.dismiss();
                    backgroundAlpha(1f);
                    userModifyData("1","大浪");
                    return;
                }
            }
        });
        bt_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                    popupWindow.dismiss();
                }
            }
        });
    }

    private void userModifyData(String modifyType,String modifyValue) {

    	if(modifyType.equals("1")){
    		//TextView streetView =(TextView)this.findViewById(R.id.wg_planadd_layout4_textview3);
    		//streetView.setVisibility(View.INVISIBLE);//隐藏选择街道
  			AndroidUtils.setTextView(this, R.id.wg_planadd_layout4_textview3,modifyValue);
    	}

    }

    public String getNowDate(){
        Date currTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd",Locale.US);
        String nowdate = null;
        try {
            nowdate = new String(formatter.format(currTime).getBytes("iso-8859-1"));
        } catch (UnsupportedEncodingException ex) {
        }

        return nowdate;
    }

	//选择日期
	void bottombirthdaywindow(TextView view, String dateType) {
        if (popupWindow != null && popupWindow.isShowing()) {
            return;
        }
        LinearLayout timepickerview = (LinearLayout) getLayoutInflater().inflate(R.layout.timepicker, null);
        wheelMain = new WheelMain(timepickerview);
        ScreenInfo screenInfo = new ScreenInfo(ZyCleanAddActivity.this);
		wheelMain.screenheight = screenInfo.getHeight();
		String time = txtstartdate.getText().toString();
		Calendar calendar = Calendar.getInstance();
		if (JudgeDate.isDate(time, "yyyy-MM-dd")) {
			try {
				calendar.setTime(dateFormat.parse(time));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		wheelMain.initDateTimePicker(year, month, day);

        popupWindow = new PopupWindow(timepickerview,
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        //点击空白处时，隐藏掉pop窗口
        popupWindow.setFocusable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        //添加弹出、弹入的动画
        popupWindow.setAnimationStyle(R.style.Popupwindow);
        int[] location = new int[2];
        view.getLocationOnScreen(location);
        popupWindow.showAtLocation(view, Gravity.LEFT | Gravity.BOTTOM, 0, -location[1]);

        //修改的话，这个地方要修改 *****************
        //添加按键事件监听
        setBirthdayListeners(timepickerview, dateType);
        //添加pop窗口关闭事件，主要是实现关闭时改变背景的透明度
        popupWindow.setOnDismissListener(new poponDismissListener());
        backgroundAlpha(1f);
    }

	//监听日期
	private void setBirthdayListeners(LinearLayout layout, String dateType) {

    	Button bt_select_birthday_ok = (Button) layout.findViewById(R.id.btn_select_birthday_ok);
    	Button bt_select_birthday_cancel = (Button) layout.findViewById(R.id.btn_select_birthday_cancel);

    	ColorStateList whiteColor=getResources().getColorStateList(R.color.popwindowselectok);
    	bt_select_birthday_ok.setTextColor(whiteColor);

    	final String fdateType = dateType;

    	bt_select_birthday_ok.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                    //在此处添加你的按键处理 xxx

                	Log.i("longhua",wheelMain.getTime());

                    popupWindow.dismiss();
                    backgroundAlpha(1f);

                    //loading.showLoading();

                    if(fdateType.equals("txtstartdate")){
                    	txtstartdate.setText(wheelMain.getTime());
                    }

                    if(fdateType.equals("txtenddate")){
                    	txtenddate.setText(wheelMain.getTime());
                    }

                    //loading.hideLoading();
                    /*
        			new HhUserModifyPersonInfoYTask(new AbstractYGetTaskListener<User>() {
        				@Override
        				public void onPostExecute(String name, YGetTask<User> task) {

        					userModifyData(task.getValue(),"3");
        				}
        			},user,"3").execute();
        			*/

                }
            }
        });
    	bt_select_birthday_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                    //在此处添加你的按键处理 xxx
                	 //选照片相
                	popupWindow.dismiss();
                	backgroundAlpha(1f);

                    return;

                }
            }
        });

    	bt_select_birthday_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                    popupWindow.dismiss();
                }
            }
        });
    }
	
	/*
     * 设置添加屏幕的背景透明度
     * @param bgAlpha
     */
    public void backgroundAlpha(float bgAlpha)
    {
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.alpha = bgAlpha; //0.0-1.0
        getWindow().setAttributes(lp);           
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        
       /*
        *    WindowManager.LayoutParams lp = activity.getWindow().getAttributes();  
        lp.alpha = bgAlpha;  
        if (bgAlpha == 1) {  
            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);//不移除该Flag的话,在有视频的页面上的视频会出现黑屏的bug  
        } else {  
            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);//此行代码主要是解决在华为手机上半透明效果无效的bug  
        }  
        activity.getWindow().setAttributes(lp); 
        */
    }
	
	/*
     * 返回或者点击空白位置的时候将背景透明度改回来
     */
    class poponDismissListener implements PopupWindow.OnDismissListener{

        @Override
        public void onDismiss() {
            // TODO Auto-generated method stub
            new Thread(new Runnable(){
                @Override
                public void run() {
                    //此处while的条件alpha不能<= 否则会出现黑屏
                    while(alpha<1f){
                        try {
                            Thread.sleep(4);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Log.d("longhua","alpha:"+alpha);
                        Message msg =mHandler.obtainMessage();
                        msg.what = 1;
                        alpha+=0.01f;
                        msg.obj =alpha ;
                        mHandler.sendMessage(msg);
                    }
                }

            }).start();
        }
    }
    
    Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case 1: 
                	backgroundAlpha(alpha);
                    //backgroundAlpha((float)msg.obj);
                    break;
            }
        }
    };
    
}
