package com.zhyq;


import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.zhyq.libs.ProgersssDialog;
import com.zhyq.libs.ToastUtils;
import com.zhyq.widget.LoadingRelativeLayout;


public class ZyScanDeviceResult extends Activity implements OnClickListener {

	public ProgersssDialog dialog;

	private String userCode = "";
	private String userName = "";
	private Long userId = null;
	private String id = "";

	private LoadingRelativeLayout loading = null;
	public int resultCode = 0;
	public static boolean active = false;

	private View includeTitle ;

	private String type = "";
	private String name = "";
	private String channelName = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		showProgressDialog();

		this.setContentView(R.layout.activity_zy_scan_device_detail);
		loading = LoadingRelativeLayout.getLoadingRelativeLayout(this);

		id = this.getIntent().getStringExtra("id");
		name = this.getIntent().getStringExtra("name");
		type = this.getIntent().getStringExtra("type");
		if(type==null){
			ToastUtils.show(this, "参数错误！");
		}
		if(type.equals("1")){
			name = "设备信息";
		}

		includeTitle = this.findViewById(R.id.zy_common_head);
		((TextView)includeTitle.findViewById(R.id.util_my_head_title)).setText(name);
		Button btnReturn = (Button)includeTitle.findViewById(R.id.util_my_head_btn_return);
		btnReturn.setOnClickListener(this);

		View util_my_head_relativelayout1 = (View)includeTitle.findViewById(R.id.util_my_head_relativelayout1);
		util_my_head_relativelayout1.setVisibility(View.VISIBLE);

		View util_my_head_relativelayout2 = (View)includeTitle.findViewById(R.id.util_my_head_relativelayout2);
		util_my_head_relativelayout2.setVisibility(View.INVISIBLE);

		((TextView)this.findViewById(R.id.text_view_01)).setText(id);

		//(this.findViewById(R.id.service_btn15)).setOnClickListener(this);
		//(this.findViewById(R.id.service_btn16)).setOnClickListener(this);

		new Handler().postDelayed(new Runnable() {//延迟读取数据
			@Override
			public void run() {
				dialog.dismiss();
			}
		}, 200);

	}

	public void showProgressDialog(){
		dialog = new ProgersssDialog(this);
		//dialog.show();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	public void onClick(View view) {
		if(R.id.util_my_head_btn_return==view.getId()){
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		}


	}


}
