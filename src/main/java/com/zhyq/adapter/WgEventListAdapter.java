package com.zhyq.adapter;

import java.util.List;


import com.zhyq.libs.AndroidUtils;
 
 
import com.zhyq.model.SysPassValue;
 
 
 
import com.zhyq.R;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
 
import android.widget.BaseAdapter; 
import android.widget.Button;


public class WgEventListAdapter extends BaseAdapter {
	private List<SysPassValue> list=null;
	private LayoutInflater mInflater;
	
	
	public WgEventListAdapter(List<SysPassValue> list) {
		super();
		this.list = list;
	}
	
	public WgEventListAdapter(Context context, List<SysPassValue> datas)
    {
        this.list = datas;
        mInflater = LayoutInflater.from(context);
       // imageLoader.init(ImageLoaderConfiguration.createDefault(context));
    }
	
	
	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return  Long.valueOf(list.get(position).getId());
	}
	
	//第一步，设置接口
	
	//设置当前地址为 默认的地址
	private View.OnClickListener onSelectValue;
	
	//private View.OnClickListener onDeleteRecord;
 	//第二步，设置接口方法 ()
	
	
	public void setOnClick1(View.OnClickListener onSelectValue){
        this.onSelectValue = onSelectValue;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView==null) {
			convertView=mInflater.inflate(R.layout.item_wg_event_list, null);
			//convertView.setTag(R.id.tag_type, "item_wg_event_list");//添加一个标签
		}
		
		SysPassValue datagrid=(SysPassValue)list.get(position);
		
		convertView.setTag(datagrid);
		
		String sid =  datagrid.getId();
		String arg1 =  datagrid.getArg1();
		String arg2 =  datagrid.getArg2();
		String arg3 =  datagrid.getArg3();
		String arg4 =  datagrid.getArg4();
		
		/*
		AndroidUtils.setWebImageCircleView(convertView, R.id.wg_contact_round_img,arg5);
		//AndroidUtils.setWebImageView(convertView, R.id.wg_contact_img ,arg5);
		
		((Button) convertView.findViewById(R.id.wg_contact_list_btn)).setTag(position);//button添加一个setTag方法，通过setTag方法传入一个position索引值
		((Button) convertView.findViewById(R.id.wg_contact_list_btn)).setOnClickListener(onClick1);
		*/
		
		AndroidUtils.setTextView(convertView, R.id.wg_eventlist_title,arg1);
		AndroidUtils.setTextView(convertView, R.id.wg_eventlist_address,arg2);
		AndroidUtils.setTextView(convertView, R.id.wg_eventlist_description,arg3);
		AndroidUtils.setTextView(convertView, R.id.wg_eventlist_date,arg4);
		
		((Button) convertView.findViewById(R.id.wg_eventlist_btn)).setTag(position);//button添加一个setTag方法，通过setTag方法传入一个position索引值
		((Button) convertView.findViewById(R.id.wg_eventlist_btn)).setOnClickListener(onSelectValue);
		
		
		
	    //SpannableStringBuilder style=new SpannableStringBuilder(usercode); 
	    //style.setSpan(new BackgroundColorSpan(Color.RED),fstart,fend,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);   
	    //style.setSpan(new ForegroundColorSpan(Color.RED),0,keyword.length(),Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
		
		
		//String str = "<span style='color:#ff0000;'>"+keyword+"</span>" +  "<span style='color:#ff0000;'>"+usercode.substring(keyword.length())+"</span>" ;
		
		
		//Log.i("spc",style); 
				
				
	 	//AndroidUtils.setTextView(convertView, R.id.item_spc_transfer_user_telephone,usercode); 
	    //TextView tvColor=(TextView) convertView.findViewById(R.id.item_spc_transfer_user_telephone);
        //tvColor.setText(style);
		
		
		
		//convertView.setTag(datagrid);
		
		return convertView;
	}
	
	public void addAll(List<SysPassValue> mDatas)  
    {   
		this.list.clear();
		this.list.addAll(mDatas);    
		this.notifyDataSetChanged();
    } 
	
	 
}
