package com.zhyq.adapter;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.util.common.StrEdit;
import com.zhyq.R;
import com.zhyq.model.SysPassValue;

import java.util.List;

public class WgFeedbackListRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    public static final int PULLUP_LOAD_MORE=0;//上拉加载更多
    public static final int LOADING_MORE=1;//正在加载中
    private int load_more_status=0;//上拉加载更多状态-默认为0
    private LayoutInflater mInflater;
    private List<SysPassValue> list=null;
    private static final int TYPE_ITEM = 0;//普通Item View
    private static final int TYPE_FOOTER = 1;//顶部FootView
    private View.OnClickListener onClick1;
    private static int pageSize = 10;
    private static int totalRecord = 60;
    
    public WgFeedbackListRecyclerAdapter(Context context, List<SysPassValue> datas) {
        mInflater = LayoutInflater.from(context);
        this.list = datas;
    }
    
    /**
     * item显示类型
     * @param parent
     * @param viewType
     * @return
     */
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //进行判断显示类型，来创建返回不同的View，该属性是在getItemViewType(int position)中进行设置获取的
        if(viewType==TYPE_ITEM){
            View view=mInflater.inflate(R.layout.item_wg_feedback_recyclerlist,parent,false);
            //这边可以做一些属性设置，甚至事件监听绑定
            //view.setBackgroundColor(Color.RED);
            ItemViewHolder itemViewHolder=new ItemViewHolder(view);
            return itemViewHolder;
        }else if(viewType==TYPE_FOOTER){
            View foot_view=mInflater.inflate(R.layout.recycler_load_more_layout,parent,false);
            //这边可以做一些属性设置，甚至事件监听绑定
            //view.setBackgroundColor(Color.RED);
            FootViewHolder footViewHolder=new FootViewHolder(foot_view);
            return footViewHolder;
        }
       return null;
    }
    
    public void setOnClick1(View.OnClickListener onClick1){
        this.onClick1 = onClick1;
	}
    
    /**
     * 数据的绑定显示
     * @param holder
     * @param position
     */
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof ItemViewHolder) {
        	SysPassValue datagrid=(SysPassValue)list.get(position);
        	
        	String sid =  datagrid.getId();
    		String arg1 =  datagrid.getArg1();
    		String arg2 =  datagrid.getArg2();
    		String arg3 =  datagrid.getArg3();
    		String arg4 =  datagrid.getArg4();
    		
            ((ItemViewHolder)holder).item_tv1.setText(StrEdit.StringLeft(arg1, 20));
            ((ItemViewHolder)holder).item_tv2.setText(StrEdit.StringLeft(arg2, 20));
            ((ItemViewHolder)holder).item_tv3.setText(arg3);
            ((ItemViewHolder)holder).item_tv4.setText(arg4);
            ((ItemViewHolder)holder).item_btn1.setOnClickListener(onClick1);
            ((ItemViewHolder)holder).item_btn1.setTag(position);
            holder.itemView.setTag(position);
        }else if(holder instanceof FootViewHolder){
        	showFootView();
            FootViewHolder footViewHolder=(FootViewHolder)holder;
            switch (load_more_status){
                case PULLUP_LOAD_MORE:
                    footViewHolder.foot_view_item_tv.setText("上拉加载更多...");
                    break;
                case LOADING_MORE:
                    footViewHolder.foot_view_item_tv.setText("正在加载更多数据...");
                    break;
            }
            hiddenFootView();
        }
    }

    /**
     * 进行判断是普通Item视图还是FootView视图
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {
    	// 最后一个item设置为footerView
    	if (position + 1 == getItemCount()) {
    		/*if(totalRecord>pageSize&&getItemCount()<totalRecord){//测试没问题，只是要传递totalRecord
    			return TYPE_FOOTER;
    		}else{
    			return TYPE_ITEM;
    		}*/
    		return TYPE_FOOTER;
    	} else {
    		return TYPE_ITEM;
    	}
    }
    @Override
    public int getItemCount() {
    	//return list.size();//结合上面的totalRecord使用
        return list.size()+1;
    }
    
    public void showFootView() {
    	FootViewHolder.foot_view_layout.setVisibility(View.VISIBLE);
    	//FootViewHolder.foot_view_item_tv.setVisibility(View.VISIBLE);
    }
    
    public void hiddenFootView() {
    	new Handler().postDelayed(new Runnable() {//延迟
			@Override
			public void run() {
				FootViewHolder.foot_view_layout.setVisibility(View.INVISIBLE);
			}
		}, 100);
    }
    
    public void hiddenFootView2() {
    	FootViewHolder.foot_view_layout.setVisibility(View.INVISIBLE);
    	//FootViewHolder.foot_view_item_tv.setVisibility(View.INVISIBLE);
    }
    
    //自定义的ViewHolder，持有每个Item的的所有界面元素
    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        public TextView item_tv1;
        public TextView item_tv2;
        public TextView item_tv3;
        public TextView item_tv4;
        public Button item_btn1;
        public ItemViewHolder(View view){
            super(view);
            item_tv1 = (TextView) view.findViewById(R.id.item_tv1);
            item_tv2 = (TextView) view.findViewById(R.id.item_tv2);
            item_tv3 = (TextView) view.findViewById(R.id.item_tv3);
            item_tv4 = (TextView) view.findViewById(R.id.item_tv4);
            item_btn1 = (Button) view.findViewById(R.id.item_btn1);
        }
    }
    /**
     * 底部FootView布局
     */
    public static class FootViewHolder extends RecyclerView.ViewHolder{
        private TextView foot_view_item_tv;
        private static LinearLayout foot_view_layout;
        public FootViewHolder(View view) {
            super(view);
            foot_view_item_tv=(TextView)view.findViewById(R.id.foot_view_item_tv);
            foot_view_layout=(LinearLayout)view.findViewById(R.id.foot_view_layout);
        }
    }
    
    //添加数据
    public void addItem(List<SysPassValue> newDatas) {
        //mTitles.add(position, data);
        //notifyItemInserted(position);
        newDatas.addAll(list);
        list.removeAll(list);
        list.addAll(newDatas);
        notifyDataSetChanged();
    }
    
    public void addMoreItem(List<SysPassValue> newDatas) {
    	list.addAll(newDatas);
        notifyDataSetChanged();
    }
    
    /**
     * //上拉加载更多
     * PULLUP_LOAD_MORE=0;
     *  //正在加载中
     * LOADING_MORE=1;
     * //加载完成已经没有更多数据了
     * NO_MORE_DATA=2;
     * @param status
     */
    public void changeMoreStatus(int status){
        load_more_status=status;
        notifyDataSetChanged();
    }
    
}
