package com.zhyq.adapter;

import java.util.List;

import com.util.common.StrEdit;
import com.zhyq.libs.AndroidUtils;
import com.zhyq.model.SysPassValue;

import com.zhyq.R;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
 
import android.widget.BaseAdapter; 
import android.widget.Button;


public class WgFeedbackListAdapter extends BaseAdapter {
	private List<SysPassValue> list=null;
	private LayoutInflater mInflater;
	
	public WgFeedbackListAdapter(List<SysPassValue> list) {
		super();
		this.list = list;
	}
	
	public WgFeedbackListAdapter(Context context, List<SysPassValue> datas) {
		this.list = datas;
        mInflater = LayoutInflater.from(context);
       // imageLoader.init(ImageLoaderConfiguration.createDefault(context));
	}
	
	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return  Long.valueOf(list.get(position).getId());
	}
	
	private View.OnClickListener onClick1;
	
	public void setOnClick1(View.OnClickListener onClick1){
        this.onClick1 = onClick1;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView==null) {
			convertView=mInflater.inflate(R.layout.item_wg_feedback_list, null); 
		}
		
		SysPassValue datagrid=(SysPassValue)list.get(position);
		
		convertView.setTag(datagrid);
		 
		
		Long id=0l;
		String userid,usercode,userrealname,usernetimg,keyword;
		String selectflag="N";
		 
		 
		//valueObject1.setArg1("13888882938");    //电话号码
		//valueObject1.setArg2("刘大哥");          		 //用户真名
		//valueObject1.setArg3("http://test1.zb388.com/upload/face/13923344901.jpg");   //网络头像图 
		//valueObject2.setArg4("N");  //未选中
		
		String sid =  datagrid.getId();
		String arg1 =  datagrid.getArg1();
		String arg2 =  datagrid.getArg2();
		String arg3 =  datagrid.getArg3();
		String arg4 =  datagrid.getArg4();
		
		AndroidUtils.setTextView(convertView, R.id.wg_plan_title,StrEdit.StringLeft(arg1, 20));
		AndroidUtils.setTextView(convertView, R.id.wg_plan_desc,StrEdit.StringLeft(arg2, 20));
		AndroidUtils.setTextView(convertView, R.id.wg_plan_type,arg3);
		AndroidUtils.setTextView(convertView, R.id.wg_plan_street,arg4);
		
		((Button) convertView.findViewById(R.id.wg_plan_list_btn)).setTag(position);//button添加一个setTag方法，通过setTag方法传入一个position索引值
		((Button) convertView.findViewById(R.id.wg_plan_list_btn)).setOnClickListener(onClick1);
		
		
	 
	    //SpannableStringBuilder style=new SpannableStringBuilder(usercode); 
	    //style.setSpan(new BackgroundColorSpan(Color.RED),fstart,fend,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);   
	    //style.setSpan(new ForegroundColorSpan(Color.RED),0,keyword.length(),Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
		
		
		//String str = "<span style='color:#ff0000;'>"+keyword+"</span>" +  "<span style='color:#ff0000;'>"+usercode.substring(keyword.length())+"</span>" ;
		
		
		//Log.i("spc",style); 
				
				
	 	//AndroidUtils.setTextView(convertView, R.id.item_spc_transfer_user_telephone,usercode); 
	    //TextView tvColor=(TextView) convertView.findViewById(R.id.item_spc_transfer_user_telephone);
        //tvColor.setText(style);

		
	 	//选中，和未选中

	 
		///onDuiHuanProduct
		 
		convertView.setTag(datagrid);
		
		return convertView;
	}
	
	public void addAll(List<SysPassValue> mDatas){
		this.list.clear();
		this.list.addAll(mDatas);    
		this.notifyDataSetChanged();
	}
	
}
