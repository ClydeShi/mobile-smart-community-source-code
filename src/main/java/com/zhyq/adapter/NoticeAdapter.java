package com.zhyq.adapter;

import java.util.List;

import com.zhyq.libs.NoticeView;
import com.zhyq.model.NoticeEntity;

import com.zhyq.R;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class NoticeAdapter {
    private List<NoticeEntity> mDatas;

    public NoticeAdapter(List<NoticeEntity> datas) {
        this.mDatas = datas;
        if (datas == null || datas.isEmpty()) {
            throw new RuntimeException("nothing to show");
        }
    }

    /**
     * 获取数据的条数
     * 
     * @return
     */
    public int getCount() {
        return mDatas == null ? 0 : mDatas.size();
    }

    /**
     * 获取摸个数据
     * 
     * @param position
     * @return
     */
    public NoticeEntity getItem(int position) {
        return mDatas.get(position);
    }

    /**
     * 获取条目布局
     * 
     * @param parent
     * @return
     */
    public View getView(NoticeView parent) {
        return LayoutInflater.from(parent.getContext()).inflate(R.layout.util_news_lunbo, null);
    }

    /**
     * 条目数据适配
     * 
     * @param view
     * @param data
     */
    public void setItem(final View view, final NoticeEntity data) {
        TextView tv = (TextView) view.findViewById(R.id.newstitle);
        tv.setText(data.gettitle());
        TextView tag = (TextView) view.findViewById(R.id.newstag);
        tag.setText(data.geturl());
        // 你可以增加点击事件
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 比如打开url
                Toast.makeText(view.getContext(), data.geturl(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}