package com.zhyq.adapter;

import java.util.List;

import com.util.common.StrEdit;
import com.zhyq.libs.AndroidUtils;
import com.zhyq.model.CommonList;
import com.zhyq.R;


import android.content.Context; 
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup; 
import android.widget.BaseAdapter; 
import android.widget.Button;


//公司数据适配器组建： 适用于xlistview 中的数据显示数据
//                   点击该记录,
//                   如果要显示 数字 或者 显示网络图片  直接在 布局文件当中的tag写上 1,2,3,4,5,6,7
//                   如果要响应click事件,最多响应 7个button的事件.直接在布局文件中button 的tag 写上 1,2,3,4,5,6,7
//                   在调用的窗口中写上  mAdapter.setOnClick1(this)    mAdapter.setOnClick2(this)   ...    mAdapter.setOnClick7(this)


public class WgTaskListAdapter extends BaseAdapter {
	private List<CommonList> list=null; 
	private LayoutInflater mInflater; 
	private Context  mContext =null;
	private String   mchildrenLayout ="";  //布局的名称
	
	public WgTaskListAdapter(List<CommonList> list) {
		super();
		this.list = list;
	}
	
	public WgTaskListAdapter(Context context, List<CommonList> datas,String childrenLayout){
		mContext = context;
        this.list = datas;  
        mInflater = LayoutInflater.from(context);
        mchildrenLayout = childrenLayout;
    }

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return  Long.valueOf(list.get(position).getId());
	}
	
	private View.OnClickListener onClick1;
	private View.OnClickListener onClick2;
	
	public void setOnClick1(View.OnClickListener onClick1){
        this.onClick1 = onClick1;
	}
	
	public void setOnClick2(View.OnClickListener onClick2){
        this.onClick2 = onClick2;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView==null) {
			
			int curResource = 0;
			curResource = getIdFromR(mchildrenLayout,"layout",mContext.getPackageName());
			
			convertView=mInflater.inflate(curResource, null);
			 
			//convertView=View.inflate(parent.getContext(),R.layout.item_cppt_room_gridcaipiao, null);
			//设置一个标签
			convertView.setTag(R.id.tag_type, mchildrenLayout);
		}
		
		final CommonList datagrid=(CommonList)list.get(position);
		convertView.setTag(datagrid.getId());
		
		//Log.i("longhua","goto111"+datagrid.getArg1());
		
		String id =  datagrid.getId().toString();
		String arg1 =  datagrid.getVarValue1();
		String arg2 =  datagrid.getVarValue2();
		String arg3 =  datagrid.getVarValue3();
		String arg4 =  datagrid.getVarValue4();
		
		AndroidUtils.setTextView(convertView, R.id.wg_plan_title,StrEdit.StringLeft(arg1, 20));
		AndroidUtils.setTextView(convertView, R.id.wg_plan_desc,StrEdit.StringLeft(arg2, 20));
		AndroidUtils.setTextView(convertView, R.id.wg_plan_type,arg3);
		AndroidUtils.setTextView(convertView, R.id.wg_plan_street,arg4);
		
		((Button) convertView.findViewById(R.id.wg_plan_list_btn)).setTag(position);//button添加一个setTag方法，通过setTag方法传入一个position索引值
		((Button) convertView.findViewById(R.id.wg_plan_list_btn)).setOnClickListener(onClick1);
		
		convertView.setTag(datagrid);
		return convertView;
	}
	
	public void addAll(List<CommonList> mDatas)
    {
		this.list.clear();
		this.list.addAll(mDatas);    
		this.notifyDataSetChanged();
    }
	
	/*
	 *  1.其中 strId：资源name，控件id(@+id/txtview).
		2.type：id,drawable,layout等
		3.packageName：R.Java文件的包名
		    通过返回的id,然后得到控件或者layout
		    
		    比如 传入的 参数为 layout   getIdFromR("item_spc_mymessage","layout",R.Java)
	 */
	//根据名称得到ID
	private int getIdFromR(String strId, String type, String packageName) {
              Resources resources = mContext.getResources(); 
              int id = resources.getIdentifier(strId, type, packageName);
              return id;
	}
 
	public int getResource(String imageName){
		//Context ctx=convertView.
		int resId = mContext.getResources().getIdentifier(imageName, "drawable",mContext.getPackageName());
		return resId;
    }
	
}
