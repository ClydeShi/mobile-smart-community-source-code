package com.zhyq.adapter;

import java.util.List;

import com.util.common.StrEdit;
import com.zhyq.libs.AndroidUtils;
import com.zhyq.model.SysPassValue;
import com.zhyq.R;


import android.content.Context; 
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup; 
import android.widget.BaseAdapter; 
import android.widget.Button;
import android.widget.TextView;
 
 
 
//公司数据适配器组建： 适用于xlistview 中的数据显示数据
//                   点击该记录,
//                   如果要显示 数字 或者 显示网络图片  直接在 布局文件当中的tag写上 1,2,3,4,5,6,7
//                   如果要响应click事件,最多响应 7个button的事件.直接在布局文件中button 的tag 写上 1,2,3,4,5,6,7
//                   在调用的窗口中写上  mAdapter.setOnClick1(this)    mAdapter.setOnClick2(this)   ...    mAdapter.setOnClick7(this)


public class WgPlanListAdapter extends BaseAdapter {
	private List<SysPassValue> list = null;
	private LayoutInflater mInflater;
	private Context mContext = null;
	private String mchildrenLayout = "";  //布局的名称
	
	
	public WgPlanListAdapter(List<SysPassValue> list) {
		super();
		this.list = list;
	}
	
	public WgPlanListAdapter(Context context, List<SysPassValue> datas,String childrenLayout)  
    {  
		mContext = context;
        this.list = datas;  
        mInflater = LayoutInflater.from(context);
        mchildrenLayout = childrenLayout;  //子布局的名称 
        
       //imageLoader.init(ImageLoaderConfiguration.createDefault(context));   
    }
	
	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return  Long.valueOf(list.get(position).getId());
	}
	
	private View.OnClickListener onClick1;
	private View.OnClickListener onClick2;
	
	public void setOnClick1(View.OnClickListener onClick1){
        this.onClick1 = onClick1;
	}
	
	
	public void setOnClick2(View.OnClickListener onClick2){
        this.onClick2 = onClick2;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view;
		ViewHolder viewHolder;
		if (convertView == null) {
			int curResource = 0;
			curResource = getIdFromR(mchildrenLayout,"layout",mContext.getPackageName());
			view = mInflater.inflate(curResource, null);
			//view.setTag(R.id.tag_type, mchildrenLayout);//设置一个标签
			
			viewHolder = new ViewHolder();
			viewHolder.item_tv1 = (TextView)view.findViewById(R.id.wg_plan_title);
			viewHolder.item_tv2 = (TextView)view.findViewById(R.id.wg_plan_desc);
			viewHolder.item_tv3 = (TextView)view.findViewById(R.id.wg_plan_type);
			viewHolder.item_tv4 = (TextView)view.findViewById(R.id.wg_plan_street);
			viewHolder.item_btn1 = (Button)view.findViewById(R.id.wg_plan_list_btn);
			view.setTag(viewHolder);//将viewHolder存储在View中
		} else {
			view = convertView;
			viewHolder = (ViewHolder)view.getTag();
		}
		
		final SysPassValue datagrid=(SysPassValue)list.get(position);
		
		//Log.i("longhua","goto111"+datagrid.getArg1());
		
		String id =  datagrid.getId();
		String arg1 =  datagrid.getArg1();
		String arg2 =  datagrid.getArg2();
		String arg3 =  datagrid.getArg3();
		String arg4 =  datagrid.getArg4();
		
		viewHolder.item_tv1.setText(StrEdit.StringLeft(arg1, 20));
		viewHolder.item_tv2.setText(StrEdit.StringLeft(arg2, 20));
		viewHolder.item_tv3.setText(arg3);
		viewHolder.item_tv4.setText(arg4);
		viewHolder.item_btn1.setTag(position);
		viewHolder.item_btn1.setOnClickListener(onClick1);
		
		return view;
	}
	
	public View getViewbak(int position, View convertView, ViewGroup parent) {
		if (convertView==null) {
			
			int curResource = 0;
			curResource = getIdFromR(mchildrenLayout,"layout",mContext.getPackageName());
			
			convertView=mInflater.inflate(curResource, null);
			 
			//convertView=View.inflate(parent.getContext(),R.layout.item_cppt_room_gridcaipiao, null);
			//设置一个标签
			convertView.setTag(R.id.tag_type, mchildrenLayout);
		}
		
		final SysPassValue datagrid=(SysPassValue)list.get(position);
		convertView.setTag(datagrid.getId());
		
		//Log.i("longhua","goto111"+datagrid.getArg1());
		
		String id =  datagrid.getId();
		String arg1 =  datagrid.getArg1();
		String arg2 =  datagrid.getArg2();
		String arg3 =  datagrid.getArg3();
		String arg4 =  datagrid.getArg4();
		
		AndroidUtils.setTextView(convertView, R.id.wg_plan_title,StrEdit.StringLeft(arg1, 20));
		AndroidUtils.setTextView(convertView, R.id.wg_plan_desc,StrEdit.StringLeft(arg2, 20));
		AndroidUtils.setTextView(convertView, R.id.wg_plan_type,arg3);
		AndroidUtils.setTextView(convertView, R.id.wg_plan_street,arg4);
		
		((Button) convertView.findViewById(R.id.wg_plan_list_btn)).setTag(position);//button添加一个setTag方法，通过setTag方法传入一个position索引值
		((Button) convertView.findViewById(R.id.wg_plan_list_btn)).setOnClickListener(onClick1);
		
		convertView.setTag(datagrid);
		return convertView;
	}
	
	public void addAll(List<SysPassValue> mDatas){
		this.list.clear();
		this.list.addAll(mDatas);    
		this.notifyDataSetChanged();
    }
	
	/*
	 *  1.其中 strId：资源name，控件id(@+id/txtview).
		2.type：id,drawable,layout等
		3.packageName：R.Java文件的包名
		    通过返回的id,然后得到控件或者layout
		    
		    比如 传入的 参数为 layout   getIdFromR("item_spc_mymessage","layout",R.Java)
	 */
	//根据名称得到ID
	private int getIdFromR(String strId, String type, String packageName) {
              Resources resources = mContext.getResources(); 
              int id = resources.getIdentifier(strId, type, packageName);
              return id;
	}
 
	public int getResource(String imageName){
		//Context ctx=convertView.
		int resId = mContext.getResources().getIdentifier(imageName, "drawable",mContext.getPackageName());
		return resId;
    }
	
	class ViewHolder{
		TextView item_tv1;
        TextView item_tv2;
        TextView item_tv3;
        TextView item_tv4;
        Button item_btn1;
	}
	
}
