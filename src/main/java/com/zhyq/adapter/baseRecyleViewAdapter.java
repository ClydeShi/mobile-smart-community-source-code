package com.zhyq.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.http.CircleNetworkImage;
import com.android.http.WebImageView;
import com.aspsine.irecyclerview.IViewHolder;
import com.github.snowdream.android.util.Log;
import com.zhyq.R;
import com.zhyq.libs.AndroidUtils;
import com.zhyq.model.SysPassValue;

import java.util.ArrayList;
import java.util.List;

//通用的 Adapter + RecyclerView
//classname: baseRecyleViewAdapter
//           所有的adapter直接集成该类进行使用  特使控制直接使用  setspecialControl  进行扩展
// example:  案例直接参考 havefinishpinglun_AllListAdapter


public class baseRecyleViewAdapter extends RecyclerView.Adapter<IViewHolder> {

   // private List<Image> mImages;

    private List<SysPassValue> list=null;
    private LayoutInflater        mInflater;
    private Context               mContext =null;
    private String                mchildrenLayout ="";  //布局的名称


    public baseRecyleViewAdapter(Context context, List<SysPassValue> datas, String childrenLayout) {
        mContext = context;
        this.list = datas;
        mInflater = LayoutInflater.from(context);
        mchildrenLayout = childrenLayout;  //子布局的名称
        Log.i("spc",mchildrenLayout);

    }

    protected View.OnClickListener onClick1;
    protected View.OnClickListener onClick2;    //下面是拓展用的
    protected View.OnClickListener onClick3;
    protected View.OnClickListener onClick4;
    protected View.OnClickListener onClick5;
    protected View.OnClickListener onClick6;
    protected View.OnClickListener onClick7;

    public void setOnClick1(View.OnClickListener onClick1){
        this.onClick1 = onClick1;
    }
    public void setOnClick2(View.OnClickListener onClick2){
        this.onClick2 = onClick2;
    }
    public void setOnClick3(View.OnClickListener onClick3){
        this.onClick3 = onClick3;
    }
    public void setOnClick4(View.OnClickListener onClick4){
        this.onClick4 = onClick4;
    }
    public void setOnClick5(View.OnClickListener onClick5){
        this.onClick5 = onClick5;
    }
    public void setOnClick6(View.OnClickListener onClick6){
        this.onClick6 = onClick6;
    }
    public void setOnClick7(View.OnClickListener onClick7){
        this.onClick7 = onClick7;
    }


    //private OnItemClickListener<Image> mOnItemClickListener;
    //public void setOnItemClickListener(OnItemClickListener<Image> listener) {
   //     this.mOnItemClickListener = listener;
    //}




    @Override
    public IViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        int curResource = 0;
        curResource = getIdFromR(mchildrenLayout,"layout",mContext.getPackageName());

        //View view=mInflater.inflate(curResource, null);
        View view=LayoutInflater.from(parent.getContext()).inflate(curResource,parent,false);

        Log.i("spc","mchildrenLayout ="+mchildrenLayout);
        final ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    //绑定数据
    @Override
    public void onBindViewHolder(IViewHolder holder, int position) {


        SysPassValue datagrid=(SysPassValue)list.get(position);

        String[] arg=new String[30];

        arg[0] =  datagrid.getArg1();
        arg[1] =  datagrid.getArg2();
        arg[2] =  datagrid.getArg3();
        arg[3] =  datagrid.getArg4();
        arg[4] =  datagrid.getArg5();



        arg[5] =  datagrid.getArg6();
        arg[6] =  datagrid.getArg7();
        arg[7] =  datagrid.getArg8();
        arg[8] =  datagrid.getArg9();
        arg[9] =  datagrid.getArg10();


        arg[10] =  datagrid.getArg11();
        arg[11] =  datagrid.getArg12();
        arg[12] =  datagrid.getArg13();
        arg[13] =  datagrid.getArg14();
        arg[14] =  datagrid.getArg15();
        arg[15] =  datagrid.getArg16();
        arg[16] =  datagrid.getArg17();
        arg[17] =  datagrid.getArg18();
        arg[18] =  datagrid.getArg19();
        arg[19] =  datagrid.getArg20();


        arg[20] =  datagrid.getArg21();
        arg[21] =  datagrid.getArg22();
        arg[22] =  datagrid.getArg23();
        arg[23] =  datagrid.getArg24();
        arg[24] =  datagrid.getArg25();
        arg[25] =  datagrid.getArg26();
        arg[26] =  datagrid.getArg27();
        arg[27] =  datagrid.getArg28();
        arg[28] =  datagrid.getArg29();
        arg[29] =  datagrid.getArg30();

        //Log.d("luktel", "arg1"+arg1);

        //先遍历 TextView
        String tags="";
        int textViewSize = ((ViewHolder)holder).listTextView.size();
        for(int i=0; i<textViewSize; i++){
            TextView set_text = (TextView)((ViewHolder)holder).listTextView.get(i);
            tags = (String)set_text.getTag();

            //Log.i("spc","tags="+tags);
            //tags 从1 开始
            if (tags!=null){
                for(int j = 1;j<=30;j++){
                    if(tags.equals(String.valueOf(j))){
                        set_text.setText(arg[j-1]);
                    }
                }
            }
        }

        //遍历网络图片
        tags="";
        int webimageid;
        int webimageSize = ((ViewHolder)holder).listWebImageView.size();
        for(int i=0; i<webimageSize; i++){

            WebImageView set_webImage = (WebImageView)((ViewHolder)holder).listWebImageView.get(i);

            webimageid = set_webImage.getId();

            tags = (String)set_webImage.getTag();
            //tags 从1 开始
            if (tags==null){
                tags="";
            }

            if (!tags.equals("")){
                //Log.i("spc",tags);
                for(int j = 1;j<=30;j++){

                    if(tags.equals(String.valueOf(j))){
                        //Log.i("spc","url="+arg[j-1]);
                        AndroidUtils.setWebImageView(((ViewHolder) holder).itemView, webimageid,arg[j-1]);
                    }
                }
            }
        }


        //便历网络圆形图片
        tags="";
        int webcircleimageid;
        int circleimageSize = ((ViewHolder)holder).listCircleNetworkImage.size();

        for(int i=0; i<circleimageSize; i++){

            CircleNetworkImage set_webImage = (CircleNetworkImage)((ViewHolder)holder).listCircleNetworkImage.get(i);
            webcircleimageid = set_webImage.getId();
            tags = (String)set_webImage.getTag();
            //tags 从1 开始
            if (tags==null){
                tags="";
            }

            if (!tags.equals("")){
                //Log.i("spc",tags);
                for(int j = 1;j<=30;j++){

                    if(tags.equals(String.valueOf(j))){
                        //Log.i("spc","url="+arg[j-1]);
                        AndroidUtils.setWebImageCircleView(((ViewHolder) holder).itemView, webcircleimageid,arg[j-1]);
                        //AndroidUtils.setWebImageCircleView(convertView, webcircleimageid,arg[j-1]);
                    }
                }
            }
        }

        //遍历本地图片。主要是针对要切换的图片
        tags="";
        int localimageid;
        int imageViewSize = ((ViewHolder)holder).listImageView.size();
        for(int i=0; i<imageViewSize; i++){
            ImageView set_Image = (ImageView)((ViewHolder)holder).listImageView.get(i);
            localimageid = set_Image.getId();
            tags = (String)set_Image.getTag();

            //tags 从1 开始
            if (tags!=null){
                for(int j = 1;j<=30;j++){
                    if(tags.equals(String.valueOf(j))){

                        ((ImageView)((ViewHolder) holder).itemView.findViewById(localimageid)).setImageResource(getResource(arg[j-1]));

                    }
                }
            }
        }

        //自定义button click 事件
        tags="";
        int buttonid;

        int curResourceButton = 0;

        int buttonSize = ((ViewHolder)holder).listButton.size();
        for(int i=0; i<buttonSize; i++){
            Button set_button = (Button)((ViewHolder)holder).listButton.get(i);

            buttonid = set_button.getId();
            tags = String.valueOf(set_button.getTag());

            //定义 7个button 的事件
            if (tags!=null){
                if(tags.equals("1")){    //对应第一个button的事件
                    //得到button的名称
                    //注意,因为 要用到setTag值,这样就把原来的值给替换掉了,所以需要使用多个 tag 值 ,这个情况下 在,activity中调用的时候　。获取position的时候必须用
                    //getTag(R.id.button_tag1) 来进行获取位置数据
                    ((Button) ((ViewHolder)holder).itemView.findViewById(buttonid)).setTag(R.id.button_tag1,position);
                    ((Button) ((ViewHolder)holder).itemView.findViewById(buttonid)).setOnClickListener(onClick1);
                }

                if(tags.equals("2")){    //对应第一个button的事件
                    //得到button的名称
                    ((Button) ((ViewHolder)holder).itemView.findViewById(buttonid)).setTag(R.id.button_tag1,position);
                    ((Button) ((ViewHolder)holder).itemView.findViewById(buttonid)).setOnClickListener(onClick2);
                }

                if(tags.equals("3")){    //对应第一个button的事件
                    //得到button的名称
                    ((Button) ((ViewHolder)holder).itemView.findViewById(buttonid)).setTag(R.id.button_tag1,position);
                    ((Button) ((ViewHolder)holder).itemView.findViewById(buttonid)).setOnClickListener(onClick3);
                }

                if(tags.equals("4")){    //对应第一个button的事件
                    //得到button的名称
                    ((Button) ((ViewHolder)holder).itemView.findViewById(buttonid)).setTag(R.id.button_tag1,position);
                    ((Button) ((ViewHolder)holder).itemView.findViewById(buttonid)).setOnClickListener(onClick4);
                }


                if(tags.equals("5")){    //对应第一个button的事件
                    //得到button的名称
                    ((Button) ((ViewHolder)holder).itemView.findViewById(buttonid)).setTag(R.id.button_tag1,position);
                    ((Button) ((ViewHolder)holder).itemView.findViewById(buttonid)).setOnClickListener(onClick5);
                }

                if(tags.equals("6")){    //对应第一个button的事件
                    //得到button的名称
                    ((Button) ((ViewHolder)holder).itemView.findViewById(buttonid)).setTag(R.id.button_tag1,position);
                    ((Button) ((ViewHolder)holder).itemView.findViewById(buttonid)).setOnClickListener(onClick6);
                }

                if(tags.equals("7")){    //对应第一个button的事件
                    //得到button的名称
                    ((Button) ((ViewHolder)holder).itemView.findViewById(buttonid)).setTag(R.id.button_tag1,position);
                    ((Button) ((ViewHolder)holder).itemView.findViewById(buttonid)).setOnClickListener(onClick7);
                }
            }
        }

        //其他控件，先暂时自定义
        setspecialControl(datagrid,position,((ViewHolder)holder).itemView);

        //
        ((ViewHolder)holder).itemView.setTag(datagrid);


        //holder.itemView.setTag(position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends IViewHolder {


        List<View> listTextView             = null;
        List<View> listWebImageView        = null;
        List<View> listCircleNetworkImage = null;
        List<View> listImageView           = null;
        List<View> listButton               = null;

        public ViewHolder(View itemView){
            super(itemView);
            //得到文本控件
            listTextView = getAllChildren((ViewGroup)itemView, TextView.class);
            //得到网络图片控件
            listWebImageView = getAllChildren((ViewGroup)itemView, WebImageView.class);
            //得到网络圆形图片控件
            listCircleNetworkImage = getAllChildren((ViewGroup)itemView, CircleNetworkImage.class);
            //得到本地图片控件
            listImageView = getAllChildren((ViewGroup)itemView, ImageView.class);
            //得到本地Button
            listButton = getAllChildren((ViewGroup)itemView, Button.class);

        }

        //public ViewHolder(View itemView) {
        //super(itemView);
        //}
    }

    //碰到特殊处理的。直接重载该函数即可
    public void setspecialControl(SysPassValue dbResult,int position, View convertView){

    }


    public void addAll(List<SysPassValue> mDatas)
    {
        this.list.clear();
        this.list.addAll(mDatas);
        this.notifyDataSetChanged();
    }


    /*
     *  1.其中 strId：资源name，控件id(@+id/txtview).
        2.type：id,drawable,layout等
        3.packageName：R.Java文件的包名
            通过返回的id,然后得到控件或者layout

            比如 传入的 参数为 layout   getIdFromR("item_spc_mymessage","layout",R.Java)
     */
    //根据名称得到ID
    private int getIdFromR(String strId, String type, String packageName) {
        Resources resources = mContext.getResources();
        int id = resources.getIdentifier(strId, type, packageName);
        return id;
    }




    /**
     * 找出layout下所有指定类型的控件 view
     * @param viewGroup layout
     * @param viewType 指定view的类型
     * @return 按指定类型找到的所有view
     *
     */
    public static List<View> getAllChildren(ViewGroup viewGroup, Class<?> viewType){
        //获取布局中的子控件数目
        int count = viewGroup.getChildCount();
        //新建一个列表List<View>用于存放控件
        List<View> lists = new ArrayList<View>();
        //根据子控件的数目，加载所有的子控件到List<View>中
        for(int i=0; i<count; i++) {
            View v = viewGroup.getChildAt(i);
            if ((viewType!=null))
            {
                if (v.getClass().equals(viewType)) {
                    lists.add(v);
                }
            }
            else lists.add(v);
            if (v instanceof ViewGroup)
            {
                ViewGroup vg = (ViewGroup)v;
                lists.addAll(getAllChildren(vg, viewType));
            }
        }
        return lists;
    }


	/*
		 * 使用方法:
		例如点击listview:里面的某个item,选中的图片显示打钩。

		//当前选中的item里面的图片，默认是隐藏的
		ImageView selected_image = (ImageView) view.findViewWithTag("district_tag_img");
		//所有itemd的勾选图片都隐藏
		List<View> listviews = getAllChildren((ViewGroup)listView, ImageView.class);
		for(int i=0; i<listviews.size(); i++){
					        	ImageView set_image = (ImageView)listviews.get(i);
					        	if ( set_image.getTag().equals("district_tag_img") ) {
					        		set_image.setVisibility(View.INVISIBLE);
					            }
					        }
		//当前的imageview显示出来
		selected_image.setVisibility(View.VISIBLE);
	 */

    public int getResource(String imageName){
        //Context ctx=convertView.
        int resId = mContext.getResources().getIdentifier(imageName, "drawable",mContext.getPackageName());
        return resId;
    }
}
