package com.zhyq.adapter;

import java.util.ArrayList;
import java.util.List; 
 
import com.android.http.WebImageView;
import com.zhyq.libs.AndroidUtils;
import com.zhyq.model.SysPassValue;
import com.zhyq.R;


import android.content.Context; 
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup; 
import android.widget.BaseAdapter; 
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
 
 
 
//公司数据适配器组建： 适用于xlistview 中的数据显示数据
//                   点击该记录,
//                   如果要显示 数字 或者 显示网络图片  直接在 布局文件当中的tag写上 1,2,3,4,5,6,7
//                   如果要响应click事件,最多响应 7个button的事件.直接在布局文件中button 的tag 写上 1,2,3,4,5,6,7
//                   在调用的窗口中写上  mAdapter.setOnClick1(this)    mAdapter.setOnClick2(this)   ...    mAdapter.setOnClick7(this)


public class UniteListAdapter extends BaseAdapter {
	private List<SysPassValue> list=null; 
	private LayoutInflater mInflater; 
	private Context  mContext =null;
	private String   mchildrenLayout ="";  //布局的名称
 	
	
	
	public UniteListAdapter(List<SysPassValue> list) {
		super();
		this.list = list;
	}
	
	public UniteListAdapter(Context context, List<SysPassValue> datas,String childrenLayout)  
    {  
		mContext = context;
        this.list = datas;  
        mInflater = LayoutInflater.from(context);
        mchildrenLayout = childrenLayout;  //子布局的名称 
        
       // imageLoader.init(ImageLoaderConfiguration.createDefault(context));   
    }  
	 
	

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return  Long.valueOf(list.get(position).getId());
	}
	
	//第一步，设置接口 
	
	//设置当前地址为 默认的地址
	private View.OnClickListener onClick1;   //必须要的 
	private View.OnClickListener onClick2;    //下面是拓展用的
	private View.OnClickListener onClick3;
	private View.OnClickListener onClick4;
	private View.OnClickListener onClick5;
	private View.OnClickListener onClick6;
	private View.OnClickListener onClick7;
	
	 
 	//使用方法
	
	//浏览店铺信息  这个是默认的第一个 click 其他的有多个，如果没有响应。这个这个用来默认 点击 渐变使用
	public void setOnClick1(View.OnClickListener onClick1){
        this.onClick1 = onClick1;
	}
	
	
	public void setOnClick2(View.OnClickListener onClick2){
        this.onClick2 = onClick2;
	}
	
	public void setOnClick3(View.OnClickListener onClick3){
        this.onClick3 = onClick3;
	}	
	
	public void setOnClick4(View.OnClickListener onClick4){
        this.onClick4 = onClick4;
	}	
	
	public void setOnClick5(View.OnClickListener onClick5){
        this.onClick5 = onClick5;
	}

	public void setOnClick6(View.OnClickListener onClick6){
        this.onClick6 = onClick6;
	}
	
	
	public void setOnClick7(View.OnClickListener onClick7){
        this.onClick7 = onClick7;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView==null) {
			
			int curResource = 0;
			curResource = getIdFromR(mchildrenLayout,"layout",mContext.getPackageName());
			
			convertView=mInflater.inflate(curResource, null);
			 
			//convertView=View.inflate(parent.getContext(),R.layout.item_cppt_room_gridcaipiao, null);
			//设置一个标签
			convertView.setTag(R.id.tag_type, mchildrenLayout);
		}
		
		final SysPassValue datagrid=(SysPassValue)list.get(position);
		convertView.setTag(datagrid.getId());
		 
		 
		String[] arg=new String[30]; 
		
		arg[0] =  datagrid.getArg1(); 
		
		 
		arg[1] =  datagrid.getArg2(); 
		arg[2] =  datagrid.getArg3(); 
		arg[3] =  datagrid.getArg4(); 
		arg[4] =  datagrid.getArg5(); 
		arg[5] =  datagrid.getArg6(); 
		arg[6] =  datagrid.getArg7(); 
		arg[7] =  datagrid.getArg8(); 
		arg[8] =  datagrid.getArg9(); 
		arg[9] =  datagrid.getArg10(); 
		
		
		arg[10] =  datagrid.getArg11(); 
		arg[11] =  datagrid.getArg12(); 
		arg[12] =  datagrid.getArg13(); 
		arg[13] =  datagrid.getArg14(); 
		arg[14] =  datagrid.getArg15(); 
		arg[15] =  datagrid.getArg16(); 
		arg[16] =  datagrid.getArg17(); 
		arg[17] =  datagrid.getArg18(); 
		arg[18] =  datagrid.getArg19(); 
		arg[19] =  datagrid.getArg20();	
		
		
		arg[20] =  datagrid.getArg21(); 
		arg[21] =  datagrid.getArg22(); 
		arg[22] =  datagrid.getArg23(); 
		arg[23] =  datagrid.getArg24(); 
		arg[24] =  datagrid.getArg25(); 
		arg[25] =  datagrid.getArg26(); 
		arg[26] =  datagrid.getArg27(); 
		arg[27] =  datagrid.getArg28(); 
		arg[28] =  datagrid.getArg29(); 
		arg[29] =  datagrid.getArg30(); 	
		
		
		
		//先遍历 TextView
		String tags="";
		List<View> listviews = getAllChildren((ViewGroup)convertView, TextView.class);	
		for(int i=0; i<listviews.size(); i++){       	
			TextView set_text = (TextView)listviews.get(i);  
			
			//Log.i("spc",set_text.getText().toString());
  	
			tags = (String)set_text.getTag();
			
			//Log.i("spc","tags="+tags);
			//tags 从1 开始
			if (tags!=null){
				for(int j = 1;j<=30;j++){
					if(tags.equals(String.valueOf(j))){
						set_text.setText(arg[j-1]);
					}
				} 
			}
        }
		
		//遍历网络图片
		tags="";
		int webimageid;
		List<View> listviews1 = getAllChildren((ViewGroup)convertView, WebImageView.class);	
		for(int i=0; i<listviews1.size(); i++){  
			 
			WebImageView set_webImage = (WebImageView)listviews1.get(i);
			
			//set_webImage.get
			
			webimageid = set_webImage.getId();
			//Log.i("spc","spcfffffff+"+String.valueOf(i));
			
			
  	
			tags = (String)set_webImage.getTag();
			 
			//Log.i("spc","spcfffffff+"+String.valueOf(i));
			
			//tags 从1 开始
			if (tags==null){
				tags="";
			}
			
			if (!tags.equals("")){
				//Log.i("spc",tags);
				for(int j = 1;j<=30;j++){
					
					if(tags.equals(String.valueOf(j))){
						//Log.i("spc","url="+arg[j-1]);
						AndroidUtils.setWebImageView(convertView, webimageid,arg[j-1]); 
					}
				} 
			}
        }
		
		//遍历本地图片。主要是针对要切换的图片
		tags="";
		int localimageid;
		List<View> lvimageid = getAllChildren((ViewGroup)convertView, ImageView.class);	
		for(int i=0; i<lvimageid.size(); i++){       	
			ImageView set_Image = (ImageView)lvimageid.get(i);  
			
			localimageid = set_Image.getId();
  	
			tags = (String)set_Image.getTag();
			
			//tags 从1 开始
			if (tags!=null){
				for(int j = 1;j<=30;j++){
					if(tags.equals(String.valueOf(j))){
						
						((ImageView)convertView.findViewById(localimageid)).setImageResource(getResource(arg[j-1]));
					
					}
				} 
			}
        }
		
		
		
		
		
		//自定义button click 事件
		tags="";
		int buttonid;
		
		int curResourceButton = 0; 
		List<View> listviews2 = getAllChildren((ViewGroup)convertView, Button.class);
		
		
		//Log.i("spc","size of spc");
		
		//Log.i("spc","size of is :"+String.valueOf(listviews2.size()));
		
		for(int i=0; i<listviews2.size(); i++){       	
			Button set_button = (Button)listviews2.get(i);  
			
			buttonid = set_button.getId();
  	
			tags = String.valueOf(set_button.getTag());
			
			//Log.i("spc","cur tag values is :"+tags);
			
			//定义 7个button 的事件
			if (tags!=null){
				if(tags.equals("1")){    //对应第一个button的事件
					//得到button的名称
					//注意,因为 要用到setTag值,这样就把原来的值给替换掉了,所以需要使用多个 tag 值 ,这个情况下 在,activity中调用的时候　。获取position的时候必须用
					//getTag(R.id.button_tag1) 来进行获取位置数据,button_tag1在attrs.xml里面设置,给控件附加一些信息
					((Button) convertView.findViewById(buttonid)).setTag(R.id.button_tag1,position);
					((Button) convertView.findViewById(buttonid)).setOnClickListener(onClick1);
				}
				
				if(tags.equals("2")){    //对应第一个button的事件
					//得到button的名称
					((Button) convertView.findViewById(buttonid)).setTag(R.id.button_tag1,position);
					((Button) convertView.findViewById(buttonid)).setOnClickListener(onClick2);
				}
				
				if(tags.equals("3")){    //对应第一个button的事件
					//得到button的名称
					((Button) convertView.findViewById(buttonid)).setTag(R.id.button_tag1,position);  
					((Button) convertView.findViewById(buttonid)).setOnClickListener(onClick3);
				}			
				
				if(tags.equals("4")){    //对应第一个button的事件
					//得到button的名称
					((Button) convertView.findViewById(buttonid)).setTag(R.id.button_tag1,position);  
					((Button) convertView.findViewById(buttonid)).setOnClickListener(onClick4);
				}		
				
				
				if(tags.equals("5")){    //对应第一个button的事件
					//得到button的名称
					((Button) convertView.findViewById(buttonid)).setTag(R.id.button_tag1,position);  
					((Button) convertView.findViewById(buttonid)).setOnClickListener(onClick5);
				}	
				 
				if(tags.equals("6")){    //对应第一个button的事件
					//得到button的名称
					((Button) convertView.findViewById(buttonid)).setTag(R.id.button_tag1,position);  
					((Button) convertView.findViewById(buttonid)).setOnClickListener(onClick6);
				}    
			
				if(tags.equals("7")){    //对应第一个button的事件
					//得到button的名称
					((Button) convertView.findViewById(buttonid)).setTag(R.id.button_tag1,position);  
					((Button) convertView.findViewById(buttonid)).setOnClickListener(onClick7);
				}
			}
		}
		
		
		
		//其他控件，先暂时自定义
		   
		 
		//
		convertView.setTag(datagrid);
		return convertView;
	}
	
	
	
 
	
	public void addAll(List<SysPassValue> mDatas)  
    {   
		this.list.clear();
		this.list.addAll(mDatas);    
		this.notifyDataSetChanged();
    } 
	
	
	/*
	 *  1.其中 strId：资源name，控件id(@+id/txtview).
		2.type：id,drawable,layout等
		3.packageName：R.Java文件的包名
		    通过返回的id,然后得到控件或者layout
		    
		    比如 传入的 参数为 layout   getIdFromR("item_spc_mymessage","layout",R.Java)
	 */
	//根据名称得到ID
	private int getIdFromR(String strId, String type, String packageName) {
              Resources resources = mContext.getResources(); 
              int id = resources.getIdentifier(strId, type, packageName);
              return id;
		}
	
	
	
	
    /**
     * 找出layout下所有指定类型的控件 view
     * @param viewGroup layout
     * @param viewType 指定view的类型
     * @return 按指定类型找到的所有view
     *
     */
    public List<View> getAllChildren(ViewGroup viewGroup, Class<?> viewType){
    	//获取布局中的子控件数目
        int count = viewGroup.getChildCount();
        //新建一个列表List<View>用于存放控件
        List<View> lists = new ArrayList<View>();
        //根据子控件的数目，加载所有的子控件到List<View>中
        for(int i=0; i<count; i++) {
            View v = viewGroup.getChildAt(i);
            if ((viewType!=null))
            {
                if (v.getClass().equals(viewType)) {
                    lists.add(v);
                }                
            }
            else lists.add(v);
            if (v instanceof ViewGroup)
            {
                ViewGroup vg = (ViewGroup)v;
                lists.addAll(getAllChildren(vg, viewType));
            }
        }
        return lists;
    }
	
	
	/*
		 * 使用方法:
		例如点击listview:里面的某个item,选中的图片显示打钩。
		
		//当前选中的item里面的图片，默认是隐藏的
		ImageView selected_image = (ImageView) view.findViewWithTag("district_tag_img");
		//所有itemd的勾选图片都隐藏
		List<View> listviews = getAllChildren((ViewGroup)listView, ImageView.class);	
		for(int i=0; i<listviews.size(); i++){       	
					        	ImageView set_image = (ImageView)listviews.get(i);
					        	if ( set_image.getTag().equals("district_tag_img") ) {           		
					        		set_image.setVisibility(View.INVISIBLE);
					            }
					        }
		//当前的imageview显示出来
		selected_image.setVisibility(View.VISIBLE);	
	 */
 
    public int getResource(String imageName){
    	//Context ctx=convertView.
        int resId = mContext.getResources().getIdentifier(imageName, "drawable",mContext.getPackageName());
        return resId;
    }
    
    
    
}
