package com.zhyq.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.aspsine.irecyclerview.IViewHolder;
import com.zhyq.R;
import com.zhyq.model.SysPassValue;

import java.util.List;


public class WgMultiSelectAdapterCheckboxNew extends RecyclerView.Adapter<IViewHolder> {

    private OnItemClickListener<SysPassValue> mOnItemClickListener;
    private List<SysPassValue> list=null;
    private View.OnClickListener onClick1;

    public WgMultiSelectAdapterCheckboxNew(List<SysPassValue> datas) {
        this.list = datas;
    }

    public void setOnClick1(View.OnClickListener onClick1){
        this.onClick1 = onClick1;
    }

    public void setOnItemClickListener(OnItemClickListener<SysPassValue> listener) {
        this.mOnItemClickListener = listener;
    }

    @Override
    public IViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Log.d("luktel", "onCreateViewHolder");
        View itemView=LayoutInflater.from(parent.getContext()).inflate(R.layout.item_wg_multiselect_checkboxnew, parent, false);
        //ImageView imageView = (ImageView) LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_image_item, parent, false);

        final ViewHolder holder = new ViewHolder(itemView);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int absolutePosition = holder.getAdapterPosition();
                final int position = holder.getIAdapterPosition();
                SysPassValue datagrid=(SysPassValue)list.get(position);
                //设置触发onItemClick
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(holder, position, datagrid, view);
                }
                //Toast.makeText(view.getContext(), "test", Toast.LENGTH_SHORT).show();
                //Toast.makeText(view.getContext(), list.get(position).getArg1(), Toast.LENGTH_SHORT).show();
            }
        });

        /*
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int position = holder.getIAdapterPosition();
                final Image image = mImages.get(position);
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(position, image, v);
                }
            }
        });
        */
        return holder;
    }

    @Override
    public void onBindViewHolder(IViewHolder holder, int position) {
        //Log.d("luktel", "onBindViewHolder");
        //ImageView imageView = (ImageView) holder.itemView;
        //Image image = mImages.get(position);
        //Glide.with(imageView.getContext()).load(image.image).dontAnimate().into(imageView);

        SysPassValue datagrid=(SysPassValue)list.get(position);

        String sid =  datagrid.getId();
        String arg1 =  datagrid.getArg1();
        String arg2 =  datagrid.getArg2();
        String arg3 =  datagrid.getArg3();
        String arg4 =  datagrid.getArg4();

        //Log.d("luktel", "arg1"+arg1);

        ((WgMultiSelectAdapterCheckboxNew.ViewHolder)holder).item_tv1.setText(arg1);
        ((WgMultiSelectAdapterCheckboxNew.ViewHolder)holder).item_tv2.setText(arg2);
        ((WgMultiSelectAdapterCheckboxNew.ViewHolder)holder).item_tv3.setText(arg3);
        ((WgMultiSelectAdapterCheckboxNew.ViewHolder)holder).item_tv4.setText(arg4);
        ((WgMultiSelectAdapterCheckboxNew.ViewHolder)holder).item_btn1.setOnClickListener(onClick1);
        ((WgMultiSelectAdapterCheckboxNew.ViewHolder)holder).item_btn1.setTag(position);
        ((WgMultiSelectAdapterCheckboxNew.ViewHolder)holder).checkBox.setChecked(list.get(position).getBo());
        holder.itemView.setTag(position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface OnItemClickListener<T> {
        void onItemClick(WgMultiSelectAdapterCheckboxNew.ViewHolder viewHolder, int position, T t, View v);
    }

    public static class ViewHolder extends IViewHolder {

        public TextView item_tv1;
        public TextView item_tv2;
        public TextView item_tv3;
        public TextView item_tv4;
        public Button item_btn1;
        public CheckBox checkBox;
        public ViewHolder(View itemView){
            super(itemView);
            item_tv1 = (TextView) itemView.findViewById(R.id.item_tv1);
            item_tv2 = (TextView) itemView.findViewById(R.id.item_tv2);
            item_tv3 = (TextView) itemView.findViewById(R.id.item_tv3);
            item_tv4 = (TextView) itemView.findViewById(R.id.item_tv4);
            item_btn1 = (Button) itemView.findViewById(R.id.item_btn1);
            checkBox = (CheckBox) itemView.findViewById(R.id.checkbox);
        }

        //public ViewHolder(View itemView) {
        //super(itemView);
        //}
    }
}
