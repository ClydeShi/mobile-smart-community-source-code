package com.zhyq.adapter;

import java.util.List;

 
 
import com.zhyq.libs.AndroidUtils;
 
 
import com.zhyq.model.SysPassValue;
 
 
 
import com.zhyq.R;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
 
import android.widget.BaseAdapter; 
import android.widget.Button;
import android.widget.ImageView;

/*
 * 多选，在onClick里面设置NewsList.get(position).setArg4("Y");表示选中，暂时不用
 */

public class WgMultiSelectAdapter extends BaseAdapter {
	private List<SysPassValue> list=null;
	private LayoutInflater mInflater;
	
	public WgMultiSelectAdapter(List<SysPassValue> list) {
		super();
		this.list = list;
	}
	
	public WgMultiSelectAdapter(Context context, List<SysPassValue> datas)
    {
        this.list = datas;
        mInflater = LayoutInflater.from(context);
       // imageLoader.init(ImageLoaderConfiguration.createDefault(context));
    }
	
	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return Long.valueOf(list.get(position).getId());
	}
	
	private View.OnClickListener onClick1;
	
	
	public void setOnClick1(View.OnClickListener onClick1){
        this.onClick1 = onClick1;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView==null) {
			convertView=mInflater.inflate(R.layout.item_wg_singleselect_list, null);
		}
		
		SysPassValue datagrid=(SysPassValue)list.get(position);
		
		convertView.setTag(datagrid);
		
		String sid =  datagrid.getId();
		String arg1 =  datagrid.getArg1();
		String arg2 =  datagrid.getArg2();
		String arg3 =  datagrid.getArg3();
		String selectflag =  datagrid.getArg4();
		String arg5 =  datagrid.getArg5();
		String arg6 =  datagrid.getArg6();
		String arg7 =  datagrid.getArg7();
		
		AndroidUtils.setTextView(convertView, R.id.wg_transferact_layout3_textview3,arg2);
		
		if(selectflag.equals("Y")){
	 		((ImageView)convertView.findViewById(R.id.wg_singleselect_layout3_yixuan_img)).setImageResource(R.drawable.bt_fk_x3x);
	 	}else{
	 		((ImageView)convertView.findViewById(R.id.wg_singleselect_layout3_yixuan_img)).setImageResource(R.drawable.bt_fk_xz_select3x);//setBackgroundResource
	 	}
		
		((Button) convertView.findViewById(R.id.wg_wode_singleselect3_btn)).setTag(position);//button添加一个setTag方法，通过setTag方法传入一个position索引值
		((Button) convertView.findViewById(R.id.wg_wode_singleselect3_btn)).setOnClickListener(onClick1);
		
		convertView.setTag(datagrid);
		
		return convertView;
	}
	
	public void addAll(List<SysPassValue> mDatas){
		this.list.clear();
		this.list.addAll(mDatas);    
		this.notifyDataSetChanged();
	}
	
	 
}
