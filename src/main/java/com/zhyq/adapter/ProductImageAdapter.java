package com.zhyq.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.zhyq.R;
import com.zhyq.libs.AndroidUtils;

//主分类
public class ProductImageAdapter extends BaseAdapter {

	Context context;
	LayoutInflater inflater;
	 
	int last_item;
	String [] images;
	private int selectedPosition = -1;    
	
	 
	public ProductImageAdapter(Context context,String[] images){
		this.context = context; 
		this.images = images;
		inflater=LayoutInflater.from(context);
	}
	
	public ProductImageAdapter(Context context ){
		this.context = context; 
		inflater=LayoutInflater.from(context);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return images.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		 
	    if(convertView==null){
		    convertView = inflater.inflate(R.layout.item_imagelist, null);  
	    }
	  
	    AndroidUtils.setWebImageView(convertView, R.id.webiv_list_image,images[position]);
	    
	    //Log.i("longhua", images[position]);
	    convertView.setTag(images); 
	    
	    reSize(convertView);
	    
		return convertView;
	}
    
	 

	public void setSelectedPosition(int position) {   
	   selectedPosition = position;   
	}  
	
	public void reSize(View convertView){
		//设置整个框架大小  中间的那个框 需要去掉左右2边的空白  和上下的 空白
		AndroidUtils.setLinearLayoutSize(convertView,R.id.ll_webiv_list_image,"LinearLayout","1",
				640,600,382,0,0,0,0); 
		
		//左边设置第一列的图片大小 
		AndroidUtils.setLinearLayoutSize(convertView,R.id.webiv_list_image,"LinearLayout","4",
				640,600,382,0,0,0,0);
	}

}
