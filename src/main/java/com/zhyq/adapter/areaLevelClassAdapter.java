package com.zhyq.adapter;
 

import android.content.Context;
 
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
 
import android.widget.LinearLayout;
import android.widget.TextView;
import com.zhyq.R;

//一个级别的分类 地区分类
public class areaLevelClassAdapter extends BaseAdapter {

	Context context;
	LayoutInflater inflater;
	String [] mainclass;
	 
	int last_item;
	int [] images;
	private int selectedPosition = -1;    
	
	 
	public areaLevelClassAdapter(Context context,String [] mainclass,int[] images){
		this.context = context;
		this.mainclass = mainclass;
		this.images = images;
		inflater=LayoutInflater.from(context);
	}
	
	public areaLevelClassAdapter(Context context,String [] mainclass){
		this.context = context;
		this.mainclass = mainclass; 
		inflater=LayoutInflater.from(context);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mainclass.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder  holder = null;
	    if(convertView==null){
		    convertView = inflater.inflate(R.layout.onelevelclass_item, null);
		    holder = new ViewHolder();
	        holder.textView =(TextView)convertView.findViewById(R.id.onelevelclasslist_textview); 
	        holder.layout=(LinearLayout)convertView.findViewById(R.id.onelevelclass_colorlayout);
	        convertView.setTag(holder);
	    }
	    else{
	    holder=(ViewHolder)convertView.getTag();
	    }
	    // 选中行变颜色
	     if(selectedPosition == position)   
	    {   
	    	 
	    	 //holder.textView.setTextColor(Color.BLUE);   
	    	 holder.textView.setTextColor(Color.parseColor("#FFFFFF")); 
 
	    	 holder.layout.setBackgroundColor(Color.parseColor("#4DA900"));
	    	 //holder.layout.setBackgroundColor(Color.LTGRAY);   
	   } else {  
		   
	 
		   holder.textView.setTextColor(Color.BLACK);   
		   holder.layout.setBackgroundColor(Color.parseColor("#FFFFFF"));   
	     }   

	   
	    holder.textView.setText(mainclass[position]);
	    //holder.textView.setTextColor(Color.BLACK);
	     
	     
	    
		return convertView;
	}

	public static class ViewHolder{
		public TextView textView;
		 
		public LinearLayout layout;
	}

	public void setSelectedPosition(int position) {   
	   selectedPosition = position;   
	}   

}
