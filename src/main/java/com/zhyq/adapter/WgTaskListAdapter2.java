package com.zhyq.adapter;

 

 
import java.util.List;

import android.content.Context;
 
 
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
 
import com.zhyq.R;
import com.zhyq.libs.AndroidUtils;

import com.zhyq.model.CommonList;
 
 

//首页技师列表
public class WgTaskListAdapter2 extends BaseAdapter {

	//private Context mcontext; 
	private List<CommonList> list=null; 
	private LayoutInflater mInflater;  
	
	
	public WgTaskListAdapter2(List<CommonList> list) {
		super();
		this.list = list;
	}
	
	public WgTaskListAdapter2(Context context, List<CommonList> datas)  
    {  
		//mcontext= context;
        this.list = datas;  
        mInflater = LayoutInflater.from(context);  
       // imageLoader.init(ImageLoaderConfiguration.createDefault(context));   
    }  
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return list.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		 
	    if(convertView==null){
		    convertView = mInflater.inflate(R.layout.item_wg_task_list2, null);//item_hh_shopmaster_list，这个布局不能用button水波纹，不然会打不开详情不知道为什么
	    }
	   
	    
	    CommonList hhlistitem=(CommonList)list.get(position);
		
		//这个标识第几行
		//convertView.setTag(String.valueOf(hhlistitem.getId()));
		//convertView.setTag(String.valueOf(position));
		
		
		convertView.setTag(hhlistitem);
		
	  
		
		
		
	    //AndroidUtils.setWebImageView(convertView, R.id.hh_webimg_shopmaster_list_product_img ,hhlistitem.getMasterUrl());
		
		/*
		AndroidUtils.setTextView(convertView, R.id.hh_tv_shopmaster_list_name,hhlistitem.getVarValue1());
	    AndroidUtils.setTextView(convertView, R.id.hh_tv_shopmaster_list_level,hhlistitem.getVarValue2());
		 */
	    
		AndroidUtils.setTextView(convertView, R.id.wg_task_title,hhlistitem.getVarValue1());
	    AndroidUtils.setTextView(convertView, R.id.wg_task_desc,hhlistitem.getVarValue2());
	    AndroidUtils.setTextView(convertView, R.id.wg_task_type,hhlistitem.getVarValue3());
	    AndroidUtils.setTextView(convertView, R.id.wg_task_street,hhlistitem.getVarValue4());
	    //AndroidUtils.setTextView(convertView, R.id.hh_img_shopmaster_work_process,hhlistitem.getMasterGongZuoJingyan()+"|"+hhlistitem.getMasteServerTimes()+"次");
	    
	    
	    
	    convertView.setTag(hhlistitem); 
	    
	    //reSize(convertView);
	    
		return convertView;
	   
	}
    
	  
	
	public void reSize(View convertView){
		 //设置整个框架大小  中间的那个框 需要去掉左右2边的空白  和上下的 空白
		 // AndroidUtils.setLinearLayoutSize(convertView,R.id.hh_ll_shouye_recharge_layout,"RelativeLayout","1",
			 	// 640,580,185,30,30,0,0); 
		
		//设置底图大小 
		AndroidUtils.setLinearLayoutSize(convertView,R.id.hh_ll_shopmaster_list_layout,"LinearLayout","1",
				640,600,170,20,20,0,0); 
		 
		//图片外框的大小 
		AndroidUtils.setLinearLayoutSize(convertView,R.id.hh_ll_shopmaster_list_product_img,"LinearLayout","1",
				640,140,140,0,0,10,10);
		
		//设置图片大小  
		AndroidUtils.setLinearLayoutSize(convertView,R.id.hh_webimg_shopmaster_list_product_img,"LinearLayout","4",
				640,140,140,0,0,15,15);
		
		//
		AndroidUtils.setLinearLayoutSize(convertView,R.id.hh_ll_shopmaster_list_right_rect,"LinearLayout","1",
				640,458,140,20,0,10,10);
		
		
		//
		AndroidUtils.setLinearLayoutSize(convertView,R.id.hh_iv_shopmaster_list_bottom_line,"LinearLayout","1",
				640,600,1,20,20,0,0);	
	}
	
	public void addAll(List<CommonList> mDatas)  
    {   
		this.list.clear();
		this.list.addAll(mDatas);    
		this.notifyDataSetChanged();
    }
	
	
	
}
