package com.zhyq.adapter;

import java.util.List;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.util.common.StrEdit;
import com.zhyq.R;

import com.zhyq.model.CommonData;
import com.zhyq.model.SysPassValue;

public class WgTaskFeedbackDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    //item类型
    public static final int ITEM_TYPE_HEADER = 0;
    public static final int ITEM_TYPE_CONTENT = 1;
    public static final int ITEM_TYPE_BOTTOM = 2;
    //模拟数据
    public String [] texts={"java","python","C++","Php",".NET","js","Ruby","Swift","OC"};
    private LayoutInflater mLayoutInflater;
    private Context mContext;
    private int mHeaderCount=1;//头部View个数
    private int mBottomCount=1;//底部View个数
    public CommonData data=null;
    private List<SysPassValue> list=null;
    private View.OnClickListener onClick1;
    
    public WgTaskFeedbackDetailAdapter(Context context, CommonData dataDetail, List<SysPassValue> dataList) {
    	mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
        this.data = dataDetail;
        this.list = dataList;
    }
    
    //内容长度
    public int getContentItemCount(){
        return list.size();
    }
    //判断当前item是否是HeadView
    public boolean isHeaderView(int position) {
        return mHeaderCount != 0 && position < mHeaderCount;
    }
    //判断当前item是否是FooterView
    public boolean isBottomView(int position) {
        return mBottomCount != 0 && position >= (mHeaderCount + getContentItemCount());
    }
    
    public void setOnClick1(View.OnClickListener onClick1){
        this.onClick1 = onClick1;
	}
    
    //判断当前item类型
    @Override
    public int getItemViewType(int position) {
        int dataItemCount = getContentItemCount();
        if (mHeaderCount != 0 && position < mHeaderCount) {
            //头部View
            return ITEM_TYPE_HEADER;
        } else if (mBottomCount != 0 && position >= (mHeaderCount + dataItemCount)) {
            //底部View
            return ITEM_TYPE_BOTTOM;
        } else {
            //内容View
            return ITEM_TYPE_CONTENT;
        }
    }
    
    //内容 ViewHolder
    public static class ContentViewHolder extends RecyclerView.ViewHolder {
        private TextView textView1;
        private TextView textView2;
        private TextView textView3;
        private TextView textView4;
        private TextView textView5;
        private TextView textView6;
        private TextView textView7;
        private TextView textView8;
        private TextView textView9;
        private TextView textView10;
        private TextView textView11;
        private TextView textView12;
        private TextView textView13;
        private TextView textView14;
        private TextView textView15;
        private TextView textView16;
        public Button item_btn1;
        public ContentViewHolder(View itemView) {
            super(itemView);
            textView1=(TextView)itemView.findViewById(R.id.wg_feedbacklist_tv1);
            textView2=(TextView)itemView.findViewById(R.id.wg_feedbacklist_tv2);
            textView3=(TextView)itemView.findViewById(R.id.wg_feedbacklist_tv3);
            textView4=(TextView)itemView.findViewById(R.id.wg_feedbacklist_tv4);
            textView5=(TextView)itemView.findViewById(R.id.wg_feedbacklist_tv5);
            textView6=(TextView)itemView.findViewById(R.id.wg_feedbacklist_tv6);
            textView7=(TextView)itemView.findViewById(R.id.wg_feedbacklist_tv7);
            textView8=(TextView)itemView.findViewById(R.id.wg_feedbacklist_tv8);
            textView9=(TextView)itemView.findViewById(R.id.wg_feedbacklist_tv9);
            textView10=(TextView)itemView.findViewById(R.id.wg_feedbacklist_tv10);
            textView11=(TextView)itemView.findViewById(R.id.wg_feedbacklist_tv11);
            textView12=(TextView)itemView.findViewById(R.id.wg_feedbacklist_tv12);
            textView13=(TextView)itemView.findViewById(R.id.wg_feedbacklist_tv13);
            textView14=(TextView)itemView.findViewById(R.id.wg_feedbacklist_tv14);
            textView15=(TextView)itemView.findViewById(R.id.wg_feedbacklist_date1);
            textView16=(TextView)itemView.findViewById(R.id.wg_feedbacklist_date2);
            item_btn1 = (Button)itemView.findViewById(R.id.wg_feedbacklist_btn1);
        }
    }
    //头部 ViewHolder
    public static class HeaderViewHolder extends RecyclerView.ViewHolder {
    	private TextView htextView1;
    	private TextView htextView2;
    	private TextView htextView3;
    	private TextView htextView4;
    	private TextView htextView5;
    	private TextView htextView6;
    	private TextView htextView7;
    	private TextView htextView8;
    	private TextView htextView9;
        public HeaderViewHolder(View itemView) {
            super(itemView);
            htextView1=(TextView)itemView.findViewById(R.id.wg_taskdetail_layout2_textview);
            htextView2=(TextView)itemView.findViewById(R.id.wg_taskdetail_layout3_textview);
            htextView3=(TextView)itemView.findViewById(R.id.wg_taskdetail_layout4_textview);
            htextView4=(TextView)itemView.findViewById(R.id.wg_taskdetail_layout5_textview);
            htextView5=(TextView)itemView.findViewById(R.id.wg_taskdetail_layout6_textview);
            htextView6=(TextView)itemView.findViewById(R.id.wg_taskdetail_layout7_textview);
            htextView7=(TextView)itemView.findViewById(R.id.wg_taskdetail_layout8_textview);
            htextView8=(TextView)itemView.findViewById(R.id.wg_taskdetail_layout9_textview);
            htextView9=(TextView)itemView.findViewById(R.id.wg_taskdetail_layout10_textview);
        }
    }
    //底部 ViewHolder
    public static class BottomViewHolder extends RecyclerView.ViewHolder {
        public BottomViewHolder(View itemView) {
            super(itemView);
        }
    }
    
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType ==ITEM_TYPE_HEADER) {
            return new HeaderViewHolder(mLayoutInflater.inflate(R.layout.item_wg_task_header, parent, false));
        } else if (viewType == ITEM_TYPE_CONTENT) {
            return  new ContentViewHolder(mLayoutInflater.inflate(R.layout.item_wg_task_feedbacklist, parent, false));
        } else if (viewType == ITEM_TYPE_BOTTOM) {
            return new BottomViewHolder(mLayoutInflater.inflate(R.layout.item_wg_task_footer, parent, false));
        }
        return null;
    }
    
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof HeaderViewHolder) {
        	CommonData taskdetail=data;
        	String varValue1 = taskdetail.getVarValue1();
			String varValue2 = taskdetail.getVarValue2();
			String varValue3 = taskdetail.getVarValue3();
			String varValue4 = taskdetail.getVarValue4();
			String varValue5 = taskdetail.getVarValue5();
			String varValue6 = taskdetail.getVarValue6();
			String varValue7 = taskdetail.getVarValue7();
			String varValue8 = taskdetail.getVarValue8();
			String varValue9 = taskdetail.getVarValue9();
			String varValue10 = taskdetail.getVarValue10();
			((HeaderViewHolder) holder).htextView1.setText(varValue2);
			((HeaderViewHolder) holder).htextView2.setText(varValue3);
			((HeaderViewHolder) holder).htextView3.setText(varValue4);
			((HeaderViewHolder) holder).htextView4.setText(varValue5);
			((HeaderViewHolder) holder).htextView5.setText(varValue6);
			((HeaderViewHolder) holder).htextView6.setText(varValue7);
			((HeaderViewHolder) holder).htextView7.setText(varValue8);
			((HeaderViewHolder) holder).htextView8.setText(varValue9);
			((HeaderViewHolder) holder).htextView9.setText(varValue10);
			//Log.i("longhua","varValue2 = "+varValue2);

        } else if (holder instanceof ContentViewHolder) {
        	SysPassValue datagrid=(SysPassValue)list.get(position - mHeaderCount);//位置要减去头部view
        	String sid =  datagrid.getId();
    		String arg1 =  datagrid.getArg1();
    		String arg2 =  datagrid.getArg2();
    		String arg3 =  datagrid.getArg3();
    		String arg4 =  datagrid.getArg4();
    		String arg5 =  datagrid.getArg5();
    		String arg6 =  datagrid.getArg6();
    		String arg7 =  datagrid.getArg7();
    		String arg8 =  datagrid.getArg8();
    		String arg9 =  datagrid.getArg9();
    		String arg10 =  datagrid.getArg10();
    		String arg11 =  datagrid.getArg11();
    		String arg12 =  datagrid.getArg12();
            String arg13 =  datagrid.getArg13();
            String arg14 =  datagrid.getArg14();
            String arg15 =  datagrid.getArg15();
            String arg16 =  datagrid.getArg16();
        	//Log.i("longhua","arg1 = "+arg1);
        	//Log.i("longhua","position = "+position);
        	//Log.i("longhua","list = "+list.size());
        	((ContentViewHolder) holder).textView1.setText(arg1);
        	((ContentViewHolder) holder).textView2.setText(arg2);
        	((ContentViewHolder) holder).textView3.setText(arg3);
        	((ContentViewHolder) holder).textView4.setText(arg4);
        	((ContentViewHolder) holder).textView5.setText(arg5);
        	((ContentViewHolder) holder).textView6.setText(arg6);
        	((ContentViewHolder) holder).textView7.setText(arg7);
        	((ContentViewHolder) holder).textView8.setText(arg8);
        	((ContentViewHolder) holder).textView9.setText(arg9);
        	((ContentViewHolder) holder).textView10.setText(arg10);
        	((ContentViewHolder) holder).textView11.setText(arg11);
        	((ContentViewHolder) holder).textView12.setText(arg12);
            ((ContentViewHolder) holder).textView13.setText(arg13);
            ((ContentViewHolder) holder).textView14.setText(arg14);
            ((ContentViewHolder) holder).textView15.setText(StrEdit.StringLeft(arg15, 16));
            ((ContentViewHolder) holder).textView16.setText(StrEdit.StringLeft(arg16, 16));
        	((ContentViewHolder) holder).item_btn1.setOnClickListener(onClick1);
            ((ContentViewHolder) holder).item_btn1.setTag(position - mHeaderCount);
    		//((ContentViewHolder) holder).textView1.setText(texts[position - mHeaderCount]);

        } else if (holder instanceof BottomViewHolder) {

        }
    }
    
    @Override
    public int getItemCount() {
       return mHeaderCount + getContentItemCount() + mBottomCount;
    }
    
    
    
}
