package com.zhyq;

/*
 类名:   HhApplication 
 作用：     全部变量类
 	   HhApplication类的作用是为了放一些全局的和一些上下文都要用到变量和方法之类的，系统会自动运行。
 */
 

import com.allthelucky.common.view.network.NetworkApp;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.location.LocationClientOption.LocationMode;
import com.baidu.mapapi.SDKInitializer;
 
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.zhyq.libs.AndroidUtils;
import com.zhyq.libs.ToastUtils;
 
import com.zhyq.model.HhCart;
import com.zhyq.model.HhSysArgument;
 
import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.HhGetSystemArgumentYTask;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.os.Build;
import android.os.StrictMode;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.aspsine.swipetoloadlayout.BuildConfig;

import com.aspsine.irecyclerview.demo.network.OkHttp;


public class HhApplication extends NetworkApp {
	public static HhApplication instance = null;
	public static HhApplication getInstance(Activity activity) {
		if(activity==null){
			Log.i("luktel","activity is null");
			return null;
		}
		return (HhApplication)activity.getApplication();
	}
	public static HhApplication getInstance(Context context) {
		return (HhApplication)context.getApplicationContext();
	}

	public static HhApplication getInstance() {
		return instance;
	}
	
	private IWXAPI msgApi;//微信支付
	
	public IWXAPI getMsgApi() {
		if (msgApi == null) {
            msgApi = WXAPIFactory.createWXAPI(this, null);
        }
        return msgApi;
	}
	
	/*  定义购物车 */
	private HhCart hhcart = new HhCart();
	public HhCart getHhCart() {
		return hhcart;
	}

	/*
	 *  系统参数作为全局值
	 */
	public HhSysArgument sysArgument = null;
	private String gs_Latitude = "0.0";           //精度
	private String gs_Longitude = "0.0";          //维度

	private LocationClient locationClient = null;
    private static final int UPDATE_TIME = 5000;
    private static int LOCATION_COUTNS = 0;

	@Override
	public void onCreate() {
		super.onCreate();

		//HhSysArgument.init(this);//add for download

        SDKInitializer.initialize(this);// 在使用 SDK 各组间之前初始化 context 信息，传入 ApplicationContext，百度地图开发加入的
		instance = this;

		//使用在TwitterRecyclerFragment组件
		setStrictMode();
		sContext = getApplicationContext();

		OkHttp.init(getApplicationContext());

        msgApi = WXAPIFactory.createWXAPI(this, null);//微信api初始化

		if(!AndroidUtils.isNetworkAvailable(getApplicationContext())){
			ToastUtils.show(this, R.string.get_intent_fail); 
			return;
		}

		/*
		异步执行函数 获取当前的系统参数
		 */
		new HhGetSystemArgumentYTask(new AbstractYGetTaskListener<HhSysArgument>() {
			@Override
			public void onPostExecute(String name, YGetTask<HhSysArgument> task) { 
				getArgumentValue(task.getValue());
			}
		}).execute();

		locationClient = new LocationClient(this);
		//(mContext.getApplicationContext());
		//设置定位条件
        LocationClientOption option = new LocationClientOption();
        option.setLocationMode(LocationMode.Hight_Accuracy);//设置定位模式
        option.setOpenGps(true);        //是否打开GPS
        option.setCoorType("gcj02");       //设置返回值的坐标类型。
        option.setProdName("huahuasystem"); //设置产品线名称。强烈建议您使用自定义的产品线名称，方便我们以后为您提供更高效准确的定位服务。
        option.setScanSpan(UPDATE_TIME);    //设置定时定位的时间间隔。单位毫秒
        locationClient.setLocOption(option);
        
        locationClient.registerLocationListener(new BDLocationListener() {
			
			@Override
			public void onReceiveLocation(BDLocation location) {
				// TODO Auto-generated method stub
				//Log.i("huahua","精度1");
				if(location == null){
					return;
				}
				StringBuffer sb = new StringBuffer(265);
				sb.append("Time : ");
				sb.append(location.getTime());
				sb.append("\nError code : ");
				sb.append(location.getLocType());
				sb.append("\nLatitude : ");
				sb.append(location.getLatitude());
				sb.append("\nLontitude : ");
				sb.append(location.getLongitude());
				sb.append("\nRadius : ");
				sb.append(location.getRadius());
				if (location.getLocType() == BDLocation.TypeGpsLocation){
					sb.append("\nSpeed : ");
					sb.append(location.getSpeed());
					sb.append("\nSatellite : ");
					sb.append(location.getSatelliteNumber());
					} else if (location.getLocType() == BDLocation.TypeNetWorkLocation){
					sb.append("\nAddress : ");
					sb.append(location.getAddrStr());
					}
				LOCATION_COUTNS ++;
				sb.append("\n检查位置更新次数：");
				sb.append(String.valueOf(LOCATION_COUTNS));
				//locationInfoTextView.setText(sb.toString());
				gs_Latitude  = String.valueOf(location.getLatitude());
				gs_Longitude = String.valueOf(location.getLongitude());
				//Log.i("huahua","精度:"+String.valueOf(location.getLatitude())+"  维度:"+String.valueOf(location.getLongitude()));
			}
			public void onReceivePoi(BDLocation location) {
				Log.i("luktel","精度2");
            }
		});
        
        locationClient.start();
		locationClient.requestLocation();
		
	}
	
	protected void getArgumentValue(HhSysArgument argument) {
		this.sysArgument = argument;
	}
	
	public void setLatitude(String temp){
		gs_Latitude = temp; 
	}
	
	public void setLongitude(String temp){
		gs_Longitude = temp; 
	}
	
	public String getLatitude(){
		return gs_Latitude;
	}
	
	public String getLongitude(){
		return gs_Longitude;
	}

	private static RequestQueue sRequestQueue;

	private static Context sContext;

	/* TwitterRecyclerFragment */
	private void setStrictMode() {
		if (BuildConfig.DEBUG && Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
			StrictMode.enableDefaults();
		}
	}

	/* TwitterRecyclerFragment */
	public static RequestQueue getRequestQueue() {
		if (sRequestQueue == null) {
			sRequestQueue = Volley.newRequestQueue(sContext);
		}
		return sRequestQueue;
	}
	
}
