package com.zhyq;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.aspsine.irecyclerview.IRecyclerView;
import com.aspsine.irecyclerview.OnLoadMoreListener;
import com.aspsine.irecyclerview.OnRefreshListener;
import com.aspsine.irecyclerview.demo.ui.widget.BannerView;
import com.aspsine.irecyclerview.demo.ui.widget.footer.LoadMoreFooterView;
import com.zhyq.adapter.baseRecyleViewAdapter;
import com.zhyq.libs.BaseActivity;
import com.zhyq.libs.PreferenceUtils;
import com.zhyq.model.SysPassValue;
import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.sendOrderToServerForListYTask;

import java.util.ArrayList;
import java.util.List;


public class ZyUrgentSelectActivity extends BaseActivity implements OnClickListener, OnItemClickListener, OnRefreshListener, OnLoadMoreListener {

    //private LoadingRelativeLayout loading = null;
    private View includeTitle;
    private IRecyclerView iRecyclerView;
    private BannerView bannerView;
    private LoadMoreFooterView loadMoreFooterView;

    private baseRecyleViewAdapter mAdapter;

    private int currentPage = 1;
    private List<SysPassValue> dataList = new ArrayList<SysPassValue>();//数据
    List<String> chooseIdList = new ArrayList<String>();
    private String street = "";
    private String clickFrom = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zy_select);
        //loading = LoadingRelativeLayout.getLoadingRelativeLayout(this);
        includeTitle = this.findViewById(R.id.zy_include_select_head);
        Button btnReturn = (Button) includeTitle.findViewById(R.id.util_head_btn_return);
        btnReturn.setOnClickListener(this);
        //Button btnConfirm = (Button) includeTitle.findViewById(R.id.wg_head_btn_confirm);
        //btnConfirm.setOnClickListener(this);
        ((TextView) includeTitle.findViewById(R.id.wg_head_title)).setText("选择紧急程度");

        ((Button) this.findViewById(R.id.wg_cancelselect_btn)).setOnClickListener(this);
        street = this.getIntent().getStringExtra("street");
        clickFrom = this.getIntent().getStringExtra("clickfrom");

        iRecyclerView = (IRecyclerView) findViewById(R.id.wg_irecyclerview_list);
        iRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        bannerView = (BannerView) LayoutInflater.from(this).inflate(R.layout.layout_irecyclerview_banner_view, iRecyclerView.getHeaderContainer(), false);
        //iRecyclerView.addHeaderView(bannerView);

        loadMoreFooterView = (LoadMoreFooterView) iRecyclerView.getLoadMoreFooterView();

        mAdapter = new baseRecyleViewAdapter(this,dataList,"item_zy_singleselect");//通用adapter
        mAdapter.setOnClick1(this);
        iRecyclerView.setIAdapter(mAdapter);

        iRecyclerView.setOnRefreshListener(this);
        iRecyclerView.setOnLoadMoreListener(this);

        iRecyclerView.post(new Runnable() {
            @Override
            public void run() {
                iRecyclerView.setRefreshing(true);
            }
        });
    }

    @Override
    public void onClick(View view) {

        if (R.id.util_head_btn_return == view.getId()) {
            this.finish();
            overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
            return;
        }

        if (R.id.wg_cancelselect_btn == view.getId()) {
            if (clickFrom.equals("problemadd")) {
                Intent intent = new Intent(this, ZyUrgentSelectActivity.class);
                intent.putExtra("id", "");
                intent.putExtra("name", "请选择");
                setResult(2, intent);
            } else {
                Intent intent = new Intent(this, ZyUrgentSelectActivity.class);
                intent.putExtra("id", "");
                intent.putExtra("name", "");
                setResult(2, intent);
            }
            overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
            this.finish();
            return;
        }

        if(R.id.item_btn1==view.getId()){//通用adapter获取点击事件，按钮设置android:tag="1"

            //传递参数过去
            Object tag = view.getTag(R.id.button_tag1);

            if (tag != null && tag instanceof Integer) { //解决问题：如何知道你点击的按钮是哪一个列表项中的，通过Tag的position
                //ToastUtils.show(view.getContext(), "22详情  NULL");
                int position = (Integer) tag;
                String taskId = dataList.get(position).getId();
                String taskName = dataList.get(position).getArg1();
                //Toast.makeText(view.getContext(), "ids"+ids, Toast.LENGTH_SHORT).show();
                //Toast.makeText(view.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
                //return;

                if (clickFrom.equals("problemadd")) {
                    Intent intent=new Intent(view.getContext(), ZyUrgentSelectActivity.class);
                    Bundle bundle=new Bundle();
                    bundle.putString("id", taskId);
                    bundle.putString("name", taskName);
                    intent.putExtras(bundle);
                    setResult(2,intent);//对应上个页面的onActivityResult里面的resultCode，1是结果标识
                } else {
                    Intent intent=new Intent(view.getContext(), ZyUrgentSelectActivity.class);
                    Bundle bundle=new Bundle();
                    bundle.putString("id", taskId);
                    bundle.putString("name", taskName);
                    intent.putExtras(bundle);
                    setResult(2,intent);//对应上个页面的onActivityResult里面的resultCode，1是结果标识
                }

                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                this.finish();
                return;
            }
        }

    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int positon, long arg3) {
        //ToastUtils.show(arg1.getContext(), R.string.show_logon_error_msg);
    }

    @Override
    public void onRefresh() {
        //loadBanner();
        loadMoreFooterView.setStatus(LoadMoreFooterView.Status.GONE);
        currentPage = 1;
        getData();
    }

    @Override
    public void onLoadMore() {
        if (loadMoreFooterView.canLoadMore() && mAdapter.getItemCount() > 0) {
            loadMoreFooterView.setStatus(LoadMoreFooterView.Status.LOADING);
            getData();
            //Toast.makeText(this, "已经到了"+currentPage, Toast.LENGTH_SHORT).show();
        }
    }

    public void getData() {

        //loading.showLoading();
        Long userId = PreferenceUtils.getLong(this, "userid", -1);
        String usercode = PreferenceUtils.getString(this, "usercode", "");
        SysPassValue temp = new SysPassValue();
        temp.setFunctionName("getUrgentList");
        temp.setPageNumber(String.valueOf(this.currentPage));
        temp.setUserId(String.valueOf(userId));
        temp.setArg1("1");
        temp.setArg2(street);

        new sendOrderToServerForListYTask(new AbstractYGetTaskListener<List<SysPassValue>>() {
            @Override
            public void onPostExecute(String name, YGetTask<List<SysPassValue>> task) {
                iRecyclerView.setRefreshing(false);
                showData(task.getValue());
                //loading.hideLoading();
            }
        }, temp).execute();
    }

    public void showData(final List<SysPassValue> list) {

        View vnoRecord = this.findViewById(R.id.activity_no_record_showpic);
        if (list == null) {
            loadMoreFooterView.setStatus(LoadMoreFooterView.Status.ERROR);
            //Toast.makeText(this, "网络连接失败", Toast.LENGTH_SHORT).show();

            if (currentPage == 1) {
                this.dataList.clear();
                mAdapter.notifyDataSetChanged();
                vnoRecord.setVisibility(View.INVISIBLE);
                return;
            }
            return;
        }

        if (list.size() == 0) {
            if (currentPage == 1) {
                this.dataList.clear();
                mAdapter.notifyDataSetChanged();
                vnoRecord.setVisibility(View.VISIBLE);//如果是第一页没有数据则显示无记录
            } else {
                loadMoreFooterView.setStatus(LoadMoreFooterView.Status.THE_END);
            }
            //Toast.makeText(this, "已经到了最后一页", Toast.LENGTH_SHORT).show();
        } else {
            if (currentPage == 1) {
                currentPage = 2;
                this.dataList.clear();
                this.dataList.addAll(list);
                vnoRecord.setVisibility(View.GONE);
            } else {

                loadMoreFooterView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        currentPage++;
                        loadMoreFooterView.setStatus(LoadMoreFooterView.Status.GONE);
                        dataList.addAll(list);
                    }
                }, 0);

                //loadMoreFooterView.setStatus(LoadMoreFooterView.Status.GONE);
                //dataList.addAll(list);
            }
            mAdapter.notifyDataSetChanged();
        }
        //vnoRecord.setVisibility(View.GONE);

        //如果第一页则需要清除数据

        if (dataList.size() == 0) {
            //vnoRecord.setVisibility(View.VISIBLE);
        }

        //Log.d("luktel", "showData");

    }


}
