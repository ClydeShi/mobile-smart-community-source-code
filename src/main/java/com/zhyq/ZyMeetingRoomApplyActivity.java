package com.zhyq;


import android.app.Activity;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.bigkoo.pickerview.TimePickerView;
import com.util.common.StrEdit;
import com.zhyq.libs.AndroidUtils;
import com.zhyq.libs.PreferenceUtils;
import com.zhyq.libs.ProgersssDialog;
import com.zhyq.libs.Session;
import com.zhyq.libs.ToastUtils;
import com.zhyq.libs.view.wheelview.JudgeDate;
import com.zhyq.libs.view.wheelview.ScreenInfo;
import com.zhyq.libs.view.wheelview.WheelMain;
import com.zhyq.model.SysPassValue;
import com.zhyq.model.User;
import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.sendOrderToServerForValueYTask;
import com.zhyq.widget.LoadingRelativeLayout;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;


public class ZyMeetingRoomApplyActivity extends Activity implements OnClickListener {

	static boolean active = false;

	private LoadingRelativeLayout loading=null;
	private View includeTitle ;
	private ImageView returnImage;
	private TextView returnText;
	private Button btSave;
	private Button topSave;
	private Button btDel;
	WheelMain wheelMain;
	private PopupWindow popupWindow;
	private Long addressId = 0l;
	private float alpha = 1f;

	TextView txtstartdate;
	TextView txtenddate;
	TextView txtdate1;
	TextView txtdate2;
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	public ProgersssDialog dialog;

	private int id = 0;
	private Long userId = null;
	private String name = "";

	public static int request_Code = 9;
	public static int resultCode = 11;
	public static int resultCodeDelete = 12;

	private String channelId = "1";
	private String channelName = "会议室";
    private boolean isFree = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		showProgressDialog();

		this.setContentView(R.layout.activity_zy_meetingroom_add);
		loading = LoadingRelativeLayout.getLoadingRelativeLayout(this);
		includeTitle = this.findViewById(R.id.zy_add_head);

		id = StrEdit.StrToInt(this.getIntent().getStringExtra("id"));
		name = this.getIntent().getStringExtra("name");
		//Log.i("luktel","id"+id);
		//Toast.makeText(this, "id"+id, Toast.LENGTH_SHORT).show();
		//Toast.makeText(this, "name"+name, Toast.LENGTH_SHORT).show();

		channelId = this.getIntent().getStringExtra("channelId");
		if(channelId==null){
			channelId = "1";
		}
		if(channelId.equals("1")){
			channelName = "会议室";
		}else if(channelId.equals("2")){
			channelName = "场地";
		}
		//Log.i("luktel","channelName"+channelName);

		((TextView)includeTitle.findViewById(R.id.wg_head_add_title)).setText(name+channelName+"申请");
		Button btnReturn = (Button)includeTitle.findViewById(R.id.wg_head_add_btn_return);
		btnReturn.setOnClickListener(this);

		((TextView)includeTitle.findViewById(R.id.wg_util_head_text_add)).setText("提交");

		((TextView)this.findViewById(R.id.zy_field_title)).setText(channelName+"名称");

		//((TextView)this.findViewById(R.id.zy_add_textviewtitle6)).setText(channelName+"内容");

		topSave =(Button)includeTitle.findViewById(R.id.wg_util_head_btn_add);
		topSave.setOnClickListener(this);

		btSave =(Button)this.findViewById(R.id.zy_save_button);
		btSave.setOnClickListener(this);

		//(this.findViewById(R.id.zy_add_button_01)).setOnClickListener(this);
		//(this.findViewById(R.id.zy_add_button_02)).setOnClickListener(this);

		txtstartdate = ((TextView)this.findViewById(R.id.zy_field_01));
		this.findViewById(R.id.btn_start_date).setOnClickListener(this);

		txtdate1 = ((TextView)this.findViewById(R.id.zy_field_02));
		this.findViewById(R.id.btn_date1).setOnClickListener(this);
		txtdate2 = ((TextView)this.findViewById(R.id.zy_field_03));
		this.findViewById(R.id.btn_date2).setOnClickListener(this);

		//txtenddate = ((TextView)this.findViewById(R.id.zy_field_07));
		//this.findViewById(R.id.btn_end_date).setOnClickListener(this);

		/*(this.findViewById(R.id.wg_planadd5_btn)).setOnClickListener(this);
		(this.findViewById(R.id.wg_planadd6_btn)).setOnClickListener(this);
		(this.findViewById(R.id.wg_planadd7_btn)).setOnClickListener(this);

		txtenddate = ((TextView)this.findViewById(R.id.wg_date_end));
		this.findViewById(R.id.btn_end_date).setOnClickListener(this);*/

		View zy_relativelayout_01 = this.findViewById(R.id.zy_relativelayout_01);
		zy_relativelayout_01.setVisibility(View.GONE);

		userId = PreferenceUtils.getLong(this,"userid",-1);
		if(userId>0){
			new Handler().postDelayed(new Runnable() {//延迟读取数据
				@Override
				public void run() {
					getData();
				}
			}, 200);
			//loading.showLoading();
		}

		//ColorStateList redColor=getResources().getColorStateList(R.color.red);//删除按钮字体设置为红色
		//btDel.setTextColor(redColor);
	}

	public void showProgressDialog(){
		dialog = new ProgersssDialog(this);
		//dialog.show();
	}

	@Override
	protected void onResume() {
		super.onResume();

	}

	protected void getData() {
		SysPassValue temp = new SysPassValue();
		temp.setFunctionName("getUserDetail");
		temp.setId(String.valueOf(id));
		temp.setUserId(String.valueOf(userId));

		new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
			@Override
			public void onPostExecute(String name, YGetTask<SysPassValue> task) {
				showData(task.getValue());
				dialog.dismiss();
				//loading.hideLoading();
			}
		},temp).execute();

		SysPassValue temp2 = new SysPassValue();
		temp2.setFunctionName("getMeetingRoomDetail");
		temp2.setArg1(channelId);
		temp2.setId(String.valueOf(id));
		temp2.setUserId(String.valueOf(userId));

		new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
			@Override
			public void onPostExecute(String name, YGetTask<SysPassValue> task) {
				showData2(task.getValue());
				dialog.dismiss();
			}
		},temp2).execute();
	}

	private void showData(SysPassValue passValue) {
		if (passValue==null) {
			ToastUtils.show(this,"没有获取到信息");
			return;
		} else {
			 /*((TextView)includeMaster.findViewById(R.id.tv_masterdetail_head_image_shopname)).setText(masterviewdetail.getArg1());
			 AndroidUtils.setWebImageView(includeMaster, R.id.hh_masterdetail_head_image_shoplogimage, masterviewdetail.getArg2());
			 AndroidUtils.setWebImageView(includeMaster, R.id.hh_masterdetail_head_image_masterimage, masterviewdetail.getArg3());*/

			String sid =  passValue.getId();
			String arg1 =  passValue.getArg1();
			String arg2 =  passValue.getArg2();
			String arg3 =  passValue.getArg3();
			String arg4 =  passValue.getArg4();
			String arg5 =  passValue.getArg5();
			String arg6 =  passValue.getArg6();
			String arg7 =  passValue.getArg7();
			String arg8 =  passValue.getArg8();
			String arg9 =  passValue.getArg9();
			String arg10 =  passValue.getArg10();
			String arg11 =  passValue.getArg11();
			String arg12 =  passValue.getArg12();
			String arg13 =  passValue.getArg13();
			String arg14 =  passValue.getArg14();
			String arg15 =  passValue.getArg15();

			AndroidUtils.setTextView(this, R.id.zy_field_04id,arg13);
			AndroidUtils.setEditText(this, R.id.zy_field_04,arg8);
			AndroidUtils.setEditText(this, R.id.zy_field_05,arg2);
			AndroidUtils.setEditText(this, R.id.zy_field_06,arg4);

			//((ImageView)includeTitle.findViewById(R.id.iv_hh_head_masterdetail_right_saygoods)).setBackgroundResource(R.drawable.praisetech);
		}
	}

	private void showData2(SysPassValue passValue) {
		if (passValue==null) {
			ToastUtils.show(this,"没有获取到信息");
			return;
		} else {
			 /*((TextView)includeMaster.findViewById(R.id.tv_masterdetail_head_image_shopname)).setText(masterviewdetail.getArg1());
			 AndroidUtils.setWebImageView(includeMaster, R.id.hh_masterdetail_head_image_shoplogimage, masterviewdetail.getArg2());
			 AndroidUtils.setWebImageView(includeMaster, R.id.hh_masterdetail_head_image_masterimage, masterviewdetail.getArg3());*/

			String priceShow = "";
			String sid =  passValue.getId();
			String arg1 =  passValue.getArg1();
			String arg2 =  passValue.getArg2();
			String arg3 =  passValue.getArg3();
			String arg4 =  passValue.getArg4();
			String arg5 =  passValue.getArg5();
			String arg6 =  passValue.getArg6();
			String arg7 =  passValue.getArg7();
			String arg8 =  passValue.getArg8();
			String arg9 =  passValue.getArg9();
			String arg10 =  passValue.getArg10();
			String arg11 =  passValue.getArg11();
			String arg12 =  passValue.getArg12();
			String arg38 =  passValue.getArg38();
			String arg41 =  passValue.getArg41();
			String arg43 =  passValue.getArg43();
			String arg50 =  passValue.getArg50();
			//ToastUtils.show(this,"arg38"+arg38);

			AndroidUtils.setTextView(this, R.id.zy_field_name,arg1);
			AndroidUtils.setTextView(this, R.id.zy_field_availabletime,arg11+" - "+arg12);
			if(arg7.equals("Y")) {
				isFree = true;
				arg7 = "免费";
				priceShow = "免费";
				/*View zy_relativelayout_price1 = this.findViewById(R.id.zy_relativelayout_price1);
				zy_relativelayout_price1.setVisibility(View.INVISIBLE);
				View zy_relativelayout_price2 = this.findViewById(R.id.zy_relativelayout_price2);
				zy_relativelayout_price2.setVisibility(View.VISIBLE);

				TextView zy_field_minutestr1 = (TextView) this.findViewById(R.id.zy_field_minutestr1);
				zy_field_minutestr1.setVisibility(View.INVISIBLE);

				TextView zy_field_minutestr2 = (TextView) this.findViewById(R.id.zy_field_minutestr2);
				zy_field_minutestr2.setVisibility(View.INVISIBLE);*/

				AndroidUtils.setTextView(this, R.id.zy_field_uniteprice,arg41);
				AndroidUtils.setTextView(this, R.id.zy_field_minutes,arg4);
				AndroidUtils.setTextView(this, R.id.zy_field_pricestr,priceShow);
			}else{
				isFree = false;
				arg7 = "收费";
				priceShow = "￥"+arg41+"/"+arg4+"分钟";
				/*View zy_relativelayout_price1 = this.findViewById(R.id.zy_relativelayout_price1);
				zy_relativelayout_price1.setVisibility(View.VISIBLE);
				View zy_relativelayout_price2 = this.findViewById(R.id.zy_relativelayout_price2);
				zy_relativelayout_price2.setVisibility(View.INVISIBLE);*/

				AndroidUtils.setTextView(this, R.id.zy_field_uniteprice,arg41);
				AndroidUtils.setTextView(this, R.id.zy_field_minutes,arg4);
				AndroidUtils.setTextView(this, R.id.zy_field_pricestr,priceShow);
			}
		}
	}

	protected void showPreOrder() {
		loading.showLoading();
		String zy_field_01 = ((TextView)this.findViewById(R.id.zy_field_01)).getText().toString();
		SysPassValue temp = new SysPassValue();
		temp.setFunctionName("getPreOrderData");
		temp.setId(String.valueOf(id));
		temp.setArg1(channelId);
		temp.setArg2(zy_field_01);
		temp.setUserId(String.valueOf(userId));

		new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
			@Override
			public void onPostExecute(String name, YGetTask<SysPassValue> task) {
				showPreOrderData(task.getValue());
				loading.hideLoading();
			}
		},temp).execute();
	}

	private void showPreOrderData(SysPassValue passValue) {
		if (passValue==null) {
			ToastUtils.show(this, "获取数据失败！");
		} else {
			String returnResult = passValue.getReturnResult();
			View zy_relativelayout_01 = this.findViewById(R.id.zy_relativelayout_01);
			if(returnResult.equals("1")){
				String arg1 =  passValue.getArg1();
				zy_relativelayout_01.setVisibility(View.VISIBLE);
				AndroidUtils.setTextView(this, R.id.zy_add_textview_preorder,arg1);
				return;
			}else{
				zy_relativelayout_01.setVisibility(View.GONE);
			}
		}
	}

	@Override
	public void onClick(View view) {

		if(R.id.wg_head_add_btn_return==view.getId()){
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		}

		if(R.id.zy_save_button==view.getId()){
			SaveData();
			return;
		}

		if(R.id.wg_util_head_btn_add==view.getId()){
			SaveData();
			return;
		}

		/*if(R.id.zy_add_button_01==view.getId()){//选择楼栋

			Long userId = PreferenceUtils.getLong(this, "userid", -1);
			String usercode = PreferenceUtils.getString(this,"usercode","");

			Intent intent = new Intent(this, ZySelectBuildingActivity.class);//WgPlanSelectCategoryActivity  WgPlanSelectWgUserNewActivity
			Bundle bundle = new Bundle();
			bundle.putString("clickfrom", "problemadd");
			bundle.putString("userId", String.valueOf(userId));
			intent.putExtras(bundle);
			startActivityForResult(intent,request_Code);//这里的request_Code要大于0，现在用的9是用来onActivityResult里判断由这里发出的新Activity，获取返回值的请求码
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);//设置新面板右边进入，原来面板左边退出
			return;
		}

		if(R.id.zy_add_button_02==view.getId()) {//紧急程度

			Intent intent = new Intent(this, ZyUrgentSelectActivity.class);
			Bundle bundle = new Bundle();
			bundle.putString("clickfrom", "problemadd");
			intent.putExtras(bundle);
			startActivityForResult(intent,request_Code);
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);//设置新面板右边进入，原来面板左边退出
			return;
		}*/

		if(R.id.btn_start_date==view.getId()){
			initDatePicker(txtstartdate);//https://blog.csdn.net/m0_37794706/article/details/78903576
			/*
			java.util.Calendar cl = Calendar.getInstance();
			showDatePickerDialog(this,2,txtstartdate,cl);//https://blog.csdn.net/qq_33756493/article/details/78120743
			*/
		}

		if(R.id.btn_date1==view.getId()){
			java.util.Calendar cl = Calendar.getInstance();
			showTimePickerDialog(this,2,txtdate1,cl);
			//showDatePickerDialog(this,2,txtdate1,cl);
		}

		if(R.id.btn_date2==view.getId()){
			String zy_field_02 = ((TextView)this.findViewById(R.id.zy_field_02)).getText().toString();
			if(zy_field_02.equals("")||zy_field_02.equals("请选择")){
				ToastUtils.show(this, "请先选择开始时间！");
				return;
			}
			java.util.Calendar cl = Calendar.getInstance();
			showTimePickerDialog(this,2,txtdate2,cl);
		}
	}

	private void initTimePicker1(final TextView tvTime){
		TimePickerView pvTime = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
			@Override
			public void onTimeSelect(Date date,View v) {//选中事件回调
				tvTime.setText(getTime(date));
			}
		}).build();
		pvTime.setDate(Calendar.getInstance());//注：根据需求来决定是否使用该方法（一般是精确到秒的情况），此项可以在弹出选择器的时候重新设置当前时间，避免在初始化之后由于时间已经设定，导致选中时间与当前时间不匹配的问题。
		pvTime.show();
	}

	private void initDatePicker(final TextView tvTime) {//选择出生年月日
		//控制时间范围(如果不设置范围，则使用默认时间1900-2100年，此段代码可注释)
		//因为系统Calendar的月份是从0-11的,所以如果是调用Calendar的set方法来设置时间,月份的范围也要是从0-11
		Date curDate = new Date(System.currentTimeMillis());//获取当前时间
		SimpleDateFormat formatter_year = new SimpleDateFormat("yyyy ");
		String year_str = formatter_year.format(curDate);
		int year_int = (int) Double.parseDouble(year_str);

		SimpleDateFormat formatter_mouth = new SimpleDateFormat("MM ");
		String mouth_str = formatter_mouth.format(curDate);
		int mouth_int = (int) Double.parseDouble(mouth_str);

		SimpleDateFormat formatter_day = new SimpleDateFormat("dd ");
		String day_str = formatter_day.format(curDate);
		int day_int = (int) Double.parseDouble(day_str);

		Calendar selectedDate = Calendar.getInstance();//系统当前时间
		Calendar startDate = Calendar.getInstance();
		startDate.set(1900, 0, 1);
		Calendar endDate = Calendar.getInstance();
		endDate.set(year_int, mouth_int - 1, day_int);

		//时间选择器
		TimePickerView pvTime = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
			@Override
			public void onTimeSelect(Date date, View v) {//选中事件回调
				// 这里回调过来的v,就是show()方法里面所添加的 View 参数，如果show的时候没有添加参数，v则为null
				tvTime.setText(getTime(date));
				showPreOrder();
			}
		})

				.setType(new boolean[]{true, true, true, false, false, false}) //年月日时分秒 的显示与否，不设置则默认全部显示
				.setLabel("年", "月", "日", "", "", "")//默认设置为年月日时分秒
				.isCenterLabel(false)
				.setDividerColor(Color.RED)
				.setTextColorCenter(Color.RED)//设置选中项的颜色
				.setTextColorOut(Color.BLACK)//设置没有被选中项的颜色
				.setContentSize(21)
				.setDate(selectedDate)
				.setLineSpacingMultiplier(1.2f)
				.setTextXOffset(-10, 0,10, 0, 0, 0)//设置X轴倾斜角度[ -90 , 90°]
				//.setRangDate(startDate, endDate)
				//.setBackgroundId(0x00FFFFFF) //设置外部遮罩颜色
				.setDecorView(null)
				.build();
		pvTime.setDate(Calendar.getInstance());//注：根据需求来决定是否使用该方法（一般是精确到秒的情况），此项可以在弹出选择器的时候重新设置当前时间，避免在初始化之后由于时间已经设定，导致选中时间与当前时间不匹配的问题。
		pvTime.show();
	}

	private String getTime(Date date) {//可根据需要自行截取数据显示
		//SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");//yyyy-MM-dd HH:mm:ss
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.format(date);
	}

	/**
	 * 日期选择
	 */

	public static void showDatePickerDialog(Activity activity, int themeResId, final TextView tv, Calendar calendar) { // 直接创建一个DatePickerDialog对话框实例，并将它显示出来
		new DatePickerDialog(activity , themeResId,new DatePickerDialog.OnDateSetListener() { // 绑定监听器(How the parent is notified that the date is set.)
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) { // 此处得到选择的时间，可以进行你想要的操作
				int month = monthOfYear + 1;
				String months = String.valueOf(month);
				if(month<10){
					months = "0" + month;
				}
				String days = String.valueOf(dayOfMonth);
				if(dayOfMonth<10){
					days = "0" + dayOfMonth;
				}
				tv.setText(year + "-" + months + "-" + days + "");

			} } // 设置初始日期
				, calendar.get(Calendar.YEAR) ,calendar.get(Calendar.MONTH) ,calendar.get(Calendar.DAY_OF_MONTH)).show();
	}

	/**
	 * 时间选择
	 */
	public static void showTimePickerDialog(Activity activity, int themeResId, final TextView tv, Calendar calendar) {
		// Calendar c = Calendar.getInstance();
		// 创建一个TimePickerDialog实例，并把它显示出来
		// 解释一哈，Activity是context的子类
		new TimePickerDialog( activity,themeResId,
				// 绑定监听器
				new TimePickerDialog.OnTimeSetListener() {
					@Override
					public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
						String hours = String.valueOf(hourOfDay);
						String minutes = String.valueOf(minute);
						if(hourOfDay<10){
							hours = "0" + hourOfDay;
						}
						if(minute<10){
							minutes = "0" + minute;
						}
						tv.setText(hours + ":" + minutes);
					}
				}
				// 设置初始时间
				, calendar.get(Calendar.HOUR_OF_DAY)
				, calendar.get(Calendar.MINUTE)
				// true表示采用24小时制
				,true).show();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == request_Code) {

			if (resultCode == 1){//结果标识
				if (data != null) {
					Bundle b = data.getExtras();
					String categoryId = b.getString("id");
	         	 	String categoryName = b.getString("name");
	         	 	AndroidUtils.setTextView(this, R.id.zy_field_01id,categoryId);
	            	AndroidUtils.setTextView(this, R.id.zy_field_01,categoryName);
				}
			}

			if (resultCode == 2){//紧急程度
				if (data != null) {
					Bundle b = data.getExtras();
	         	 	String name = b.getString("name");
	         	 	if(!name.equals("")){
	         	 		AndroidUtils.setTextView(this, R.id.zy_field_02,name);
	         	 	}
				}
			}
		}

		super.onActivityResult(requestCode, resultCode, data);
	}

	public void SaveData(){
    	Long userid = null;
    	User curuser = new User();
    	curuser = HhApplication.getInstance(this).getHhCart().getUser();
    	if(curuser==null){
    		userid =-1l;
      	}else{
      		userid = HhApplication.getInstance(this).getHhCart().getUser().getId();
      	}
    	//Long userid = HhApplication.getInstance(this).getHhCart().getUser().getId();//未登录会出错

    	Long userId = PreferenceUtils.getLong(this,"userid",-1);
    	String userCode = PreferenceUtils.getString(this,"usercode","");
		String userName = PreferenceUtils.getString(this,"username","");

		String zy_field_01 = ((TextView)this.findViewById(R.id.zy_field_01)).getText().toString();
		String zy_field_02 = ((TextView)this.findViewById(R.id.zy_field_02)).getText().toString();
		String zy_field_03 = ((TextView)this.findViewById(R.id.zy_field_03)).getText().toString();
		String zy_field_04id = ((TextView)this.findViewById(R.id.zy_field_04id)).getText().toString();
		String zy_field_04 = AndroidUtils.getEditText(this, R.id.zy_field_04);
		String zy_field_05 = AndroidUtils.getEditText(this, R.id.zy_field_05);
		String zy_field_06 = AndroidUtils.getEditText(this, R.id.zy_field_06);
		String zy_field_07 = AndroidUtils.getEditText(this, R.id.zy_field_07);
		String zy_field_08 = AndroidUtils.getEditText(this, R.id.zy_field_08);
		String zy_field_uniteprice = ((TextView)this.findViewById(R.id.zy_field_uniteprice)).getText().toString();
		String zy_field_minutes = ((TextView)this.findViewById(R.id.zy_field_minutes)).getText().toString();

  	    if(zy_field_01.equals("")||zy_field_01.equals("请选择")){
	    	ToastUtils.show(this, "请选择预定日期！");
		    return;
	    }

		if(zy_field_02.equals("")||zy_field_02.equals("请选择")){
			ToastUtils.show(this, "请选择开始时间！");
			return;
		}

		if(zy_field_03.equals("")||zy_field_03.equals("请选择")){
			ToastUtils.show(this, "请选择结束时间！");
			return;
		}

		if(zy_field_04.equals("")){
			ToastUtils.show(this, "请输入企业名称！");
			return;
		}

		if(zy_field_05.equals("")){
			ToastUtils.show(this, "请输入联系人！");
			return;
		}

		if(zy_field_06.equals("")){
			ToastUtils.show(this, "请输入手机！");
			return;
		}

  	    loading.showLoading();
		final int random = (int)((Math.random()*9+1)*100000);
		String startDate = zy_field_01 + " " + zy_field_02 + ":00";
		String endDate = zy_field_01 + " " + zy_field_03 + ":00";
		Double standard = StrEdit.StrToDouble(zy_field_minutes);
		Double unitePrice = StrEdit.StrToDouble(zy_field_uniteprice);
		Long totalMinute = StrEdit.getDateMinutes(startDate,endDate);
		Double totalAmount = 0.0;
		if(standard>0 && unitePrice>0){
			double times = totalMinute/StrEdit.StrToDouble(zy_field_minutes);//总分钟除以收费标准分钟，得到次数，余数不能为0
			double cishu = Math.ceil(times);//有小数+1，没满一次算一次
			totalAmount = unitePrice*cishu;
		}
		//String dateNow = StrEdit.getDate("yyyy-MM-dd HH:mm:ss");
  	    SysPassValue temp = new SysPassValue();//buildingId buildingName address degree date1 title content
		if(channelId.equals("1")){
			temp.setFunctionName("saveMeetingRoomApplyAdd");
		}else if(channelId.equals("2")){
			temp.setFunctionName("saveMeetingRoomApplyAdd");
		}
		temp.setUserId(String.valueOf(userId));
		temp.setUserName(userName);
		temp.setId(String.valueOf(id));
		temp.setArg1(channelId);
		temp.setArg2(name);
		temp.setArg3(String.valueOf(id));
		temp.setArg4("申请"+channelName);
		temp.setArg5(zy_field_04);
		temp.setArg6("1");
		if(zy_field_uniteprice.equals("0.00")){
			temp.setArg7("5");//已付款
		}else{
			temp.setArg7("4");//待付款
		}
		temp.setArg8("");
		temp.setArg9(zy_field_05);
		temp.setArg10(zy_field_06);
		temp.setArg11(zy_field_07);
		temp.setArg12(userName);
		temp.setArg13(String.valueOf(unitePrice));
		temp.setArg14(String.valueOf(totalAmount));
		temp.setArg15(zy_field_04id);//企业id
		temp.setArg16(zy_field_08);
		temp.setArg17("");//dateNow
		temp.setArg18("");
		temp.setArg19(zy_field_01);
		temp.setArg20(zy_field_02);
		temp.setArg21(zy_field_03);
		temp.setArg22(zy_field_minutes);//收费标准分钟

		final String visitor = zy_field_01;

		new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
			@Override
			public void onPostExecute(String name, YGetTask<SysPassValue> task) {
				loading.hideLoading();
				getData(task.getValue(),visitor,random);
			}
		},temp).execute();
	}

	private void getData(SysPassValue passValue,String name,int number) {
		if (passValue==null) {
			ToastUtils.show(this, "保存失败！");
		} else {
			String returnResult = passValue.getReturnResult();
			//String message = "尊敬的访客"+name+"，您的访客码是"+number+"，访客码在来访时间有效";
			if(returnResult.equals("1")){
				String amount = passValue.getArg3();
				if(amount.equals("0.00")){
					Session session = Session.getSession();
					session.put("message", "预定提交成功");
					session.put("pay_amount", "0");
					session.put("pay_result", "success");
					Intent intent=new Intent(this, ZyPayResultActivity.class);
					Bundle bundle=new Bundle();
					bundle.putString("id", passValue.getArg1());
					bundle.putString("name", passValue.getArg2());
					intent.putExtras(bundle);
					startActivity(intent);
					this.finish();
					overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
					return;
				}else{
					Intent intent=new Intent(this, ZyPayActivity.class);
					Bundle bundle=new Bundle();
					bundle.putString("id", passValue.getArg1());
					bundle.putString("name", passValue.getArg2());
					bundle.putString("amount", amount);
					intent.putExtras(bundle);
					startActivity(intent);
					this.finish();
					overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
					return;
				}
			}else if(returnResult.equals("2")) {
				ToastUtils.show(this, passValue.getArg1());
			}else{
				ToastUtils.show(this, "保存失败，请检查输入的文字！");
			}
		}
	}

    public String getNowDate(){
        Date currTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd",Locale.US);
        String nowdate = null;
        try {
            nowdate = new String(formatter.format(currTime).getBytes("iso-8859-1"));
        } catch (UnsupportedEncodingException ex) {
        }

        return nowdate;
    }
    
}
