package com.zhyq;

 
 

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
 
 
 
 
import android.view.View.OnClickListener;
 
 
import android.widget.Button;
import android.widget.GridView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
 
 
 

import com.zhyq.adapter.SPC_MyOrder_uniteListAdapter;
import com.zhyq.fragment.WgTaskFragmentList;
import com.zhyq.libs.CommonUtils;
import com.zhyq.libs.ViewPagerScroller;

import com.zhyq.libs.TabPageIndicator;
import com.zhyq.model.SysPassValue;
import com.zhyq.model.User;
 
/*
 * 计划列表
 * 主页WgHomeFragmentZhuye使用startActivity(intent);打开WgTaskActivity
 * WgTaskActivity设置栏目，导入栏目，调用WgTaskFragmentList获取列表内容，WgTaskFragmentList通过WgTaskListAdapter导入列表布局文件和数据设置触发
 */

public class WgTaskActivity extends FragmentActivity implements OnClickListener {
	 
	private View  includeTitle ;
	private TabPageIndicator mIndicator;//栏目
 
	private ViewPager mViewPager;//列表
	
	private boolean hasClickClass = false;
	private Button  m_btnShowPop;
	
	
	 /*
     * 分类数据适配器
     */
	private SPC_MyOrder_uniteListAdapter mAdapterClass;
	
	/** 
	 * 数据 
	 */
	private List<SysPassValue>  NewsListClass=new ArrayList<SysPassValue>();


	//弹出窗口
	private PopupWindow popupWindow; 
	private String fenleiname="";
	
	
	private BasePagerAdapter adapter;
		
	private static final String[] TITLE = new String[] { "未执行", "执行中", "已完成" };
 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		this.setContentView(R.layout.activity_wg_task_list);
		
		includeTitle = this.findViewById(R.id.wg_util_head_plan_list);
		
		Button btreturn = (Button)includeTitle.findViewById(R.id.wg_head_add_btn_return);
		btreturn.setOnClickListener(this);
		
		((TextView)includeTitle.findViewById(R.id.wg_head_add_title)).setText("我的任务");
		
		Button btadd = (Button)includeTitle.findViewById(R.id.wg_util_head_btn_add);
		btadd.setOnClickListener(this);
		
		//隐藏新增功能
		TextView addtext = (TextView)this.findViewById(R.id.wg_util_head_text_add);
		Button addbutton = (Button)this.findViewById(R.id.wg_util_head_btn_add);
		addtext.setVisibility(View.INVISIBLE);
		addbutton.setVisibility(View.INVISIBLE);
		
	 	//二级分类面板，用SPC_MyOrder_uniteListAdapter类读取并设置触发
        mAdapterClass = new SPC_MyOrder_uniteListAdapter(this, NewsListClass,"item_spc_pinpai_fenlei_query");//二级分类面板，未使用
        mAdapterClass.setOnClick1(this);
        
        //LoadDatasTask();//获取二级分类数据
		
		initView();
	}
	
	@Override
	public void onClick(View view) {
		 
		if(R.id.wg_head_add_btn_return==view.getId()){
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		}
		
		if(R.id.wg_util_head_btn_add==view.getId()){
			
			Intent intent=new Intent(this, WgPlanAdd.class);
			Bundle bundle=new Bundle();
			
			Long userid = null;
			User curuser = new User();
			curuser = HhApplication.getInstance(this).getHhCart().getUser();
			if(curuser==null){
				userid =-1l;
			}else{
				userid = HhApplication.getInstance(this).getHhCart().getUser().getId();
			}
			
			//Long userid = HhApplication.getInstance(this).getHhCart().getUser().getId();
			
			bundle.putString("userid", String.valueOf(userid));
			intent.putExtras(bundle);
			
			startActivityForResult(intent, 1);//这里采用startActivityForResult来做跳转，此处的0为一个依据，可以写其他的值，但一定要>=0
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
		}
		
		//二级分类的按钮点击事件，点击更新分类名称和数据，未使用
		if(view.getId()==R.id.hh_btn_pinpai_function){
			
			//ToastUtils.show(view.getContext(), "详情  NULL！");
			
			//传递参数过去 
			Object tag = view.getTag();
			   
			if (tag != null && tag instanceof Integer) { //解决问题：如何知道你点击的按钮是哪一个列表项中的，通过Tag的position
				
				int position = (Integer) tag; 
				fenleiname =  NewsListClass.get(position).getArg1();
				
				//ToastUtils.show(view.getContext(), fenleiname);
				
				popupWindow.dismiss(); 
                reinit();
                
                retrieveByClass(fenleiname);
		       
			}
		}
		return;
    }
	
	/*
	 * 初始化栏目和列表，读取栏目id和列表id
	 */
	private void initView()
	{
		mViewPager = (ViewPager) findViewById(R.id.vp_plan_list_view_pager);//内容框
		//initViewPagerScroll();//滚动的时候滑动慢一点的效果
		//mViewPager.setOffscreenPageLimit(0);
		
		mIndicator = (TabPageIndicator) findViewById(R.id.vpi_id_indicator_category);//切换栏目
		
		adapter = new BasePagerAdapter(getSupportFragmentManager());//获取列表内容
		
		mViewPager.setAdapter(adapter);
		mIndicator.setViewPager(mViewPager);
        setTabPagerIndicator();
	}
	 

	class BasePagerAdapter extends FragmentPagerAdapter {
		String[] titles;

		WgTaskFragmentList fragment = null;
		WgTaskFragmentList mCurrentFragment = null;

        public BasePagerAdapter(FragmentManager fm) {
            super(fm);
            this.titles = TITLE;//CommonUtils.getStringArray(R.array.expand_titles);
        }

        @Override
        public Fragment getItem(int position) {
        	
        	//如果  Fragment 类别不同 ，着在这个地方 position 进行判断
        	fragment = new WgTaskFragmentList();
            Bundle args0 = new Bundle();
            args0.putString("TITLES", TITLE[position]);//切换的时候，用Bundle传递参数
            fragment.setArguments(args0);
            return fragment;
        }

        @Override
        public int getCount() {
            return titles.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }
        
        //ViewPager在切换的时候，如果频繁销毁和加载Fragment，就容易产生卡顿现象，
        //阻止Fragment的销毁可有效减缓卡顿现象。
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
        	//super.destroyItem(container, position, object);      
        }
        
        //----------------------下面才是重点-----------------  
        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object){
        	mCurrentFragment=(WgTaskFragmentList)object;
        	super.setPrimaryItem(container,position,object);
        }
        public WgTaskFragmentList getCurrentFragment(){
        	return mCurrentFragment;
        }
	}

	 public void retrieveByClass(String fenlei){
		 WgTaskFragmentList curfragment = adapter.getCurrentFragment();
		 curfragment.setFenLei(fenlei);
		 //mCurrentFragment.setFenLei(fenlei);
	 }
	 
	 /*
	  * 设置颜色字体
	  */
	 private void setTabPagerIndicator() {
		 mIndicator.setIndicatorMode(TabPageIndicator.IndicatorMode.MODE_NOWEIGHT_EXPAND_NOSAME);// 设置模式，一定要先设置模式
		 mIndicator.setDividerColor(Color.parseColor("#00bbcf"));// 设置分割线的颜色
		 mIndicator.setDividerPadding(CommonUtils.dip2px(getApplicationContext(), 10));
		 mIndicator.setIndicatorColor(Color.parseColor("#00b2f0"));// 设置底部导航线的颜色
		 mIndicator.setTextColorSelected(Color.parseColor("#00b2f0"));// 设置tab标题选中的颜色
		 mIndicator.setTextColor(Color.parseColor("#797979"));// 设置tab标题未被选中的颜色
		 mIndicator.setTextSize(CommonUtils.sp2px(getApplicationContext(), 16));// 设置字体大小
	 }
	 
	 /*
	  * 设置ViewPager的滑动速度
	  */
	 private void initViewPagerScroll(){
		  try {
			  Field mScroller = null;
			  mScroller = ViewPager.class.getDeclaredField("mScroller");
			  mScroller.setAccessible(true); 
		      ViewPagerScroller scroller = new ViewPagerScroller( mViewPager.getContext( ) );
			  mScroller.set(mViewPager, scroller);
		  }catch(NoSuchFieldException e){
		  }catch (IllegalArgumentException e){
		  }catch (IllegalAccessException e){
		  }
		  
	 }
	 
	 public void LoadDatasTask() {
		 
			/*
			 * 
			 1. 将参数装入对象
			 2. 调用com.unitesoft.task.impl.sendOrderToServerForListYTask，并传入参数对象，sendOrderToServerForListYTask会调用com.unitesoft.data.DataAPI.sendOrderToServerForList()
			 3. DataAPI的方法sendOrderToServerForList会调用DemoDataInterface的方法sendOrderToServerForList返回结果，因为DemoDataInterface是实现DataInterface接口，可以DataInterface也要定义方法sendOrderToServerForList
			 4. 如果是调用远程数据，上面的DataAPI的方法sendOrderToServerForList会调用ClientDataInterface，ClientDataInterface会带参数和对象连接服务器，com.huahua.server.servelt.ClientApiServlet，ClientApiServlet通过判断参数，传入对象，分别调用com.huahua.server.data.DemoDataInterface里的方法，并返回对象
			 */
			 
			 /*
			  * 
			  * sendOrderToServerForListYTask继承YGetTask，调用接口的时候执行构造函数sendOrderToServerForListYTask，传入YTaskListener和temp参数对象
			  * 父类YGetTask会执行onExecute方法，方法里面有_get()，会通过DataAPI.sendOrderToServerForList(temp);返回结果
			  */
			 
			/*
			SysPassValue temp = new SysPassValue();
			temp.setFunctionname("getShopClass");
			temp.setPagernumber("1");
			 
			new sendOrderToServerForListYTask(new AbstractYGetTaskListener<List<SysPassValue>>() {
				@Override
				public void onPostExecute(String name, YGetTask<List<SysPassValue>> task) {//onPostExecute是AbstractYGetTaskListener里的方法
					getData(task.getValue());//task.getValue()是SysPassValue定义的List，这个是返回的结果
				}
			},temp).execute();
			*/
		}
		
		public void getData(List<SysPassValue> list) {
			this.NewsListClass.clear();
			this.NewsListClass.addAll(list);
			mAdapterClass.notifyDataSetChanged();
		}
		
		void popwindowClass(View view) {
	        if (popupWindow != null && popupWindow.isShowing()) {
	            return;
	        }
	        
	        final RelativeLayout layout = (RelativeLayout) this.getLayoutInflater().inflate(R.layout.util_spc_grid, null);
	        popupWindow = new PopupWindow(layout,
	                ViewGroup.LayoutParams.FILL_PARENT,
	                ViewGroup.LayoutParams.FILL_PARENT,true);
	        
	        ColorDrawable dw = new ColorDrawable(-00000);
	        popupWindow.setBackgroundDrawable(dw);
	        popupWindow.update();
	        //popupWindow.getBackground().setAlpha(100);//0~255透明度值 ，0为完全
	        
	        //在PopupWindow里面就加上下面代码，让键盘弹出时，不会挡住pop窗口。  
	        popupWindow.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED); 
	        popupWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
	        
	        //直接点 透明的地方 自动关闭
	        final View v3 = layout.findViewById(R.id.popupLayout_grid3);
	        v3.setOnClickListener(new View.OnClickListener() {
	            @Override
	            public void onClick(View view) {
	                if (popupWindow != null && popupWindow.isShowing()) {
	                    //在此处添加你的按键处理 xxx 
	                    popupWindow.dismiss(); 
	                    reinit();
	                    return;
	                }
	    				 
	            }
	        });
	        
	        //点击空白处时，隐藏掉pop窗口
	        popupWindow.setFocusable(true);
	        popupWindow.setBackgroundDrawable(new BitmapDrawable());
	        //添加弹出、弹入的动画
	        //popupWindow.setAnimationStyle(R.style.Popupwindow);
	        popupWindow.showAsDropDown(view);
	        //popupWindow.setAnimationStyle(R.style.PopupAnimation);
	        int[] location = new int[2];
	        view.getLocationOnScreen(location);
	        
	        
	        popupWindow.showAtLocation(view, Gravity.LEFT , 0, location[1]+2);
	        
	        GridView classView=(GridView)layout.findViewById(R.id.popupLayout_gv3);
	        
	        classView.setAdapter(mAdapterClass);
	        
	 	    //final int curlocation=0;
	        
	 	    //下面代码是直接默认显示第一个子菜单，第一个子菜单显示 且选中第一条
	 	    //如果 主类为不限制 ,则直接不在现实二级菜单
	        /*
	        classView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					// TODO Auto-generated method stub
					if(view.getId()==R.id.hh_btn_pinpai_function){
						ToastUtils.show(view.getContext(), "详情  NULL！");
						
						//传递参数过去 
						Object tag = view.getTag();
						   
						if (tag != null && tag instanceof Integer) { //解决问题：如何知道你点击的按钮是哪一个列表项中的，通过Tag的position
							
							
							int position = (Integer) tag; 
							fenleiname =  NewsListClass.get(position).getArg1();
							
							ToastUtils.show(view.getContext(), fenleiname);
							
							popupWindow.dismiss(); 
			                reinit();
					       
						}
					}
				}
	        	
	         });
		  */
			
			//下面是定义一级分类的数据 的CLICK事件
	        /*
	 	   classView.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1, int position,
							long arg3) {
						// TODO Auto-generated method stub
						 
						 
						String fenlei =  NewsListClass.get(position).getArg1();
						//直接弹出  检索店铺的窗口 ，把地区传递过去。进行检索 
						popupWindow.dismiss(); 
		                reinit();
						 
						
						//Intent intent=new Intent(arg0.getContext(), HhSearchShopActivity.class); 
						//Bundle bundle=new Bundle();
						//bundle.putString("key", mainclass);
						//intent.putExtras(bundle); 
						//startActivity(intent); 
						//((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
						
						
		                return;
					}
				});
			
			 */
	    }
		
		public void reinit(){
	    	if(popupWindow != null && popupWindow.isShowing()){
	            popupWindow.dismiss(); 
	        }
	    	//View  v1 = this.findViewById(R.id.wode_head_below_line_blank); 
			hasClickClass = false;
			//v1.setVisibility(View.VISIBLE);
		}
		
		
		
}
