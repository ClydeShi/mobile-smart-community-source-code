package com.zhyq;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.util.common.StrEdit;
import com.zhyq.libs.AndroidUtils;
import com.zhyq.libs.Session;
import com.zhyq.libs.ToastUtils;
import com.zhyq.model.SysPassValue;
import com.zhyq.model.User;
import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.sendOrderToServerForListYTask;
import com.zhyq.widget.LoadingRelativeLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class ZyPayActivity extends Activity implements OnClickListener {
	
	private LoadingRelativeLayout loading = null;
	
	private View includeTitle;
	 
	private String userCode = "";
	private String userName = "";
	private String userId = "";
	private int id = 0;
	private String name = "";
	private String amount = "";
	private String payWay = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.setContentView(R.layout.activity_zy_pay);
		loading = LoadingRelativeLayout.getLoadingRelativeLayout(this);
		//getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		includeTitle = this.findViewById(R.id.zy_include_pay_head);
		
		Button btnReturn = (Button)includeTitle.findViewById(R.id.util_head_btn_return);
		btnReturn.setOnClickListener(this);
		
		((TextView)includeTitle.findViewById(R.id.wg_head_title)).setText("付款");

		((Button)this.findViewById(R.id.spc_overpay5_btn)).setOnClickListener(this);
		((Button)this.findViewById(R.id.spc_overpay6_btn)).setOnClickListener(this);
		((Button)this.findViewById(R.id.spc_overpay7_btn)).setOnClickListener(this);
		((Button)this.findViewById(R.id.btn_overpay_linearlayout8_btn_pay)).setOnClickListener(this);

		//this.findViewById(R.id.wg_contact_btn1).setOnClickListener(this);
		//this.findViewById(R.id.wg_contact_btn2).setOnClickListener(this);

		TextView tv1 = (TextView)this.findViewById(R.id.spc_overpay_edittext_money);
		TextView tv2 = (TextView)this.findViewById(R.id.spc_transferact_layout3_textview3);

		id = StrEdit.StrToInt(this.getIntent().getStringExtra("id"));
		name = this.getIntent().getStringExtra("name");
		amount = this.getIntent().getStringExtra("amount");
		if(amount==null){
			amount = "0";
		}

		tv1.setText(name);
		tv2.setText("￥"+amount);
		payWay = "weixin";
	}
	
	//onResume是在启动activity启动之后才能执行的，也就是恢复执行。程序正常启动：onCreate()->onStart()->onResume();
	@Override
	protected void onResume() {   //该函数，如果在打开了其他窗口，再关闭，这个函数也会执行
		super.onResume();
		
	}
	
	@Override
	public void onClick(View view) {

		if (R.id.util_head_btn_return == view.getId()) {
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		}

		if (R.id.spc_overpay5_btn == view.getId()) {
			((ImageView) this.findViewById(R.id.spc_overpay_layout5_right_img)).setImageResource(R.drawable.bt_fk_x3x);
			((ImageView) this.findViewById(R.id.spc_overpay_layout6_right_img)).setImageResource(R.drawable.bt_fk_xz_select3x);
			((ImageView) this.findViewById(R.id.spc_overpay_layout7_right_img)).setImageResource(R.drawable.bt_fk_xz_select3x);
			payWay = "weixin";
		}


		if (R.id.spc_overpay6_btn == view.getId()) {
			((ImageView) this.findViewById(R.id.spc_overpay_layout5_right_img)).setImageResource(R.drawable.bt_fk_xz_select3x);
			((ImageView) this.findViewById(R.id.spc_overpay_layout6_right_img)).setImageResource(R.drawable.bt_fk_x3x);
			((ImageView) this.findViewById(R.id.spc_overpay_layout7_right_img)).setImageResource(R.drawable.bt_fk_xz_select3x);
			payWay = "alipay";
		}


		if (R.id.spc_overpay7_btn == view.getId()) {
			((ImageView) this.findViewById(R.id.spc_overpay_layout5_right_img)).setImageResource(R.drawable.bt_fk_xz_select3x);
			((ImageView) this.findViewById(R.id.spc_overpay_layout6_right_img)).setImageResource(R.drawable.bt_fk_xz_select3x);
			((ImageView) this.findViewById(R.id.spc_overpay_layout7_right_img)).setImageResource(R.drawable.bt_fk_x3x);
			payWay = "cash";
		}

		if (R.id.btn_overpay_linearlayout8_btn_pay == view.getId()) {

			if (payWay.equals("weixin")) {
				if (!HhApplication.getInstance().getMsgApi().isWXAppInstalled()) {
					ToastUtils.show(this, "付款接口开发中，请选择线下支付");
					//ToastUtils.show(this, "对不起，您的手机未安装微信，不能使用微信支付。");
					return;
				}
			}

			if (payWay.equals("alipay")) {
				if (Double.valueOf(amount) <= 0) {
					ToastUtils.show(this, "支付金额必须大于零");
					return;
				}
			}

			if (payWay.equals("cash")) {
				Session session = Session.getSession();
				session.put("message", "请支付现金，由收费员审核付款状态");
				session.put("pay_amount", amount);
				session.put("pay_result", "success");
				Intent intent=new Intent(this, ZyPayResultActivity.class);
				Bundle bundle=new Bundle();
				intent.putExtras(bundle);
				startActivity(intent);
				this.finish();
				overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				return;
			}

			ToastUtils.show(this, "付款接口开发中，请选择线下支付");

			//直接付款
			//payMoney();
		}

	}
 
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}

 
}
