package com.zhyq;




import java.util.ArrayList;
import java.util.List;
import com.zhyq.adapter.WgSingleSelectAdapter;


import com.zhyq.libs.PreferenceUtils;


import com.zhyq.model.SysPassValue;


import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;

import com.zhyq.task.impl.sendOrderToServerForListYTask;

import com.zhyq.widget.LoadingRelativeLayout;


import android.app.Activity;
import android.content.Intent;


import android.os.Bundle;


import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class WgEventSelectMapIDActivity extends Activity implements OnItemClickListener,OnClickListener {

    private LoadingRelativeLayout loading = null;
    private View includeTitle;
    private int currentPage = 1;
    private WgSingleSelectAdapter mAdapter;//数据适配器
    private List<SysPassValue> NewsList = new ArrayList<SysPassValue>();//数据
    private String street = "";
    private String clickfrom = "";
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.setContentView(R.layout.activity_wg_singleselect_listview);
        loading = LoadingRelativeLayout.getLoadingRelativeLayout(this);
        includeTitle = this.findViewById(R.id.wg_include_singleselect_head);
        Button btreturn = (Button)includeTitle.findViewById(R.id.wg_head_btn_return);
        btreturn.setOnClickListener(this);
        ((TextView)includeTitle.findViewById(R.id.wg_head_title)).setText("选择区域");

        ((Button)this.findViewById(R.id.wg_singleselect1_btn)).setOnClickListener(this);
        street = this.getIntent().getStringExtra("street");
        clickfrom = this.getIntent().getStringExtra("clickfrom");

        listView = (ListView) findViewById(R.id.lv_data);
        mAdapter = new WgSingleSelectAdapter(this, NewsList);
        mAdapter.setOnClick1(this);

        listView.setAdapter(mAdapter);

        onRefresh();
    }

    private void onRefresh(){
        //首次加载任务数据 加载任务
        currentPage = 1;
        GetData(currentPage);
    }

    //onResume是在启动activity启动之后才能执行的，也就是恢复执行。程序正常启动：onCreate()->onStart()->onResume();
    @Override
    protected void onResume() {   //注意 该函数，如果在打开了其他窗口，再关闭，这个函数也会执行
        super.onResume();

    }

    public void GetData(int curpage){

        loading.showLoading();
        Long userId = PreferenceUtils.getLong(this, "userid", -1);
        String usercode = PreferenceUtils.getString(this,"usercode","");

        SysPassValue temp = new SysPassValue();
        temp.setFunctionName("getWgMapIDList");
        temp.setUserId(String.valueOf(userId));
        temp.setArg1("1");
        temp.setArg2("");
        temp.setPageNumber(String.valueOf(curpage));

        //调用接口得到数据
        new sendOrderToServerForListYTask(new AbstractYGetTaskListener<List<SysPassValue>>() {
            @Override
            public void onPostExecute(String name, YGetTask<List<SysPassValue>> task) {

                showData(task.getValue());
                loading.hideLoading();
            }
        },temp).execute();

    }

    public void showData(List<SysPassValue> list) {

        //通过数据适配器获取 数据列表
        View vnorecord = this.findViewById(R.id.activity_no_record_showpic);
        if (list==null){
            if(currentPage==1){
                this.NewsList.clear();
                mAdapter.notifyDataSetChanged();
                vnorecord.setVisibility(View.VISIBLE);
                return;
            }
            return;
        }
        vnorecord.setVisibility(View.GONE);

        //如果是第一页则需要清楚数据   currentPage
        if(currentPage==1){
            this.NewsList.clear();
            this.NewsList.addAll(list);
        } else{
            NewsList.addAll(list);
        }
        mAdapter.notifyDataSetChanged();
        if(NewsList.size()==0){
            vnorecord.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onClick(View view) {

        if(R.id.wg_head_btn_return==view.getId()){
            this.finish();
            overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
            return;
        }

        if(R.id.wg_singleselect1_btn==view.getId()){
            Intent intent = new Intent(this,WgEventHighSearchActivity.class);
            Bundle bundle=new Bundle();
            bundle.putString("categoryId", "");
            bundle.putString("categoryName", "全部");
            intent.putExtras(bundle);
            setResult(2,intent);
            overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
            this.finish();
            return;
        }

        if(R.id.wg_wode_singleselect3_btn==view.getId()){

            Object tag = view.getTag();
            //Object tag = view.getTag(R.id.button_tag1);

            if (tag != null && tag instanceof Integer) {
                int position = (Integer) tag;
                String categoryId =  NewsList.get(position).getId();
                String categoryName =  NewsList.get(position).getArg2();

                //Log.i("longhua","categoryId"+categoryId);

                Intent intent=new Intent(this, WgEventHighSearchActivity.class);
                Bundle bundle=new Bundle();
                bundle.putString("categoryId", categoryId);
                bundle.putString("categoryName", categoryName);
                intent.putExtras(bundle);
                setResult(2,intent);//对应上个页面的onActivityResult里面的resultCode，2是结果标识
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                this.finish();
                return;
            }
        }

    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        // TODO Auto-generated method stub

        Log.i("longhua","onItemClick");
        if(arg3 == -1) {
            // 点击的是headerView或者footerView
            return;
        }
        //arg2是当前item的ID。这个id根据你在适配器中的写法可以自己定义
        //ToastUtils.show(arg1.getContext(), R.string.show_logon_error_msg)
        //Object tag2 = arg1.getTag();
        int position = (Integer) arg2;
        position = position -1;
        return;

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

}
