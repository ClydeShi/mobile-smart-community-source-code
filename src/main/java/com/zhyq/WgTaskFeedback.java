package com.zhyq;



import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.util.common.StrEdit;
import com.zhyq.libs.view.wheelcity.AddressData;
import com.zhyq.libs.view.wheelcity.OnWheelChangedListener;
 
import com.zhyq.libs.view.wheelcity.WheelView;
import com.zhyq.libs.view.wheelcity.adapters.AbstractWheelTextAdapter;
import com.zhyq.libs.view.wheelcity.adapters.ArrayWheelAdapter;

import com.zhyq.libs.view.wheelview.JudgeDate;
import com.zhyq.libs.view.wheelview.ScreenInfo;
import com.zhyq.libs.view.wheelview.WheelMain;


import com.zhyq.libs.AndroidUtils;
import com.zhyq.libs.PreferenceUtils;
import com.zhyq.libs.ToastUtils;


import com.zhyq.model.SysPassValue;
import com.zhyq.model.User;
import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.sendOrderToServerForValueYTask;
import com.zhyq.widget.LoadingRelativeLayout;
import com.zhyq.libs.iosdialog.widget.MyAlertDialog;
 
 

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;

import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;


public class WgTaskFeedback extends Activity implements OnClickListener {
	
	static boolean active = false;
	
	private LoadingRelativeLayout loading = null; 
	private View includeTitle;
	private ImageView returnImage;
	private TextView returnText;
	
	private Button btSave;
	private Button topSave;
	private Button btDel;
	WheelMain wheelMain;
	private PopupWindow popupWindow;
	private Long addressId = 0l;
	private float alpha = 1f;
	private int taskId = 0;
	private int feedbackId = 0;
	private String taskName = null;
	private String cityTxt = null;
	private String curprovince = null;  
	private String curcity = null;
	private String curcountry = null;
	private String detailaddress = null;
	private String personname = null;
	private String telephone = null;
	private Long userId = 0l;
	
	private EditText vCity = null;
	
	public static int request_Code = 9;
	public static int resultCode = 11;//保存
	public static int resultCodeDelete = 12;//删除

	TextView txtstartdate;
	TextView txtenddate;
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");
   
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.setContentView(R.layout.activity_wg_task_feedback);
		loading=LoadingRelativeLayout.getLoadingRelativeLayout(this);
		
		includeTitle = this.findViewById(R.id.wg_feedbackadd_head);
		((TextView)includeTitle.findViewById(R.id.wg_head_title)).setText("反馈登记");//wg_head_save_title
		Button btreturn = (Button)includeTitle.findViewById(R.id.wg_head_btn_return);//wg_head_save_btn_return
		btreturn.setOnClickListener(this);
		
		topSave =(Button)includeTitle.findViewById(R.id.wg_util_head_btn_edit);//wg_util_head_btn_save
		topSave.setOnClickListener(this);
		
		btSave =(Button)this.findViewById(R.id.wg_feedbackadd_btn_save);
		btSave.setOnClickListener(this);

		(this.findViewById(R.id.wg_planadd4_btn)).setOnClickListener(this);
		(this.findViewById(R.id.wg_planadd5_btn)).setOnClickListener(this);
		(this.findViewById(R.id.wg_btn_feedback_wgy)).setOnClickListener(this);
		
		//(this.findViewById(R.id.wg_feedbackadd4_btn)).setOnClickListener(this);
		
		taskId = StrEdit.StrToInt(this.getIntent().getStringExtra("taskId"));
		taskName = this.getIntent().getStringExtra("taskName");
		
		feedbackId = StrEdit.StrToInt(this.getIntent().getStringExtra("feedbackId"));
		
		userId = PreferenceUtils.getLong(this, "userid", -1);
		String usercode = PreferenceUtils.getString(this,"usercode","");
		
		TextView savetext = (TextView)includeTitle.findViewById(R.id.wg_util_head_text_edit);
		savetext.setText("保存");
		
		TextView deltext = (TextView)includeTitle.findViewById(R.id.wg_util_head_text_del);
		Button delbutton = (Button)includeTitle.findViewById(R.id.wg_util_head_btn_del);
		delbutton.setOnClickListener(this);
		
		if(feedbackId>0){
			readData();
		}else{
			deltext.setVisibility(View.INVISIBLE);
			delbutton.setVisibility(View.INVISIBLE);
		}

		txtstartdate = ((TextView)this.findViewById(R.id.wg_date_start));
		this.findViewById(R.id.btn_start_date).setOnClickListener(this);

		txtenddate = ((TextView)this.findViewById(R.id.wg_date_end));
		this.findViewById(R.id.btn_end_date).setOnClickListener(this);
		
		//Log.i("longhua","taskId"+taskId);
		
		//ColorStateList redColor=getResources().getColorStateList(R.color.red);//删除按钮字体设置为红色
		//btDel.setTextColor(redColor);
		
		//vCity = (EditText)this.findViewById(R.id.et_edit_receive_goods_address);//显示 省市县
		//vCity.setOnClickListener(this);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
	}
	
	private void readData(){
		SysPassValue temp = new SysPassValue();
		temp.setFunctionName("getFeedbackDetail");
		temp.setId(String.valueOf(feedbackId));
		temp.setUserId(String.valueOf(userId));
		
		new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
			@Override
			public void onPostExecute(String name, YGetTask<SysPassValue> task) {
				
				getFeedbackData(task.getValue());
				//loading.hideLoading();
			}
		},temp).execute();
	}
	
	private void getFeedbackData(SysPassValue passValue) {
		if (passValue==null) {
			ToastUtils.show(this,"没有获取到信息");
			return;
		} else {
			
			String sid =  passValue.getId();
			String arg1 =  passValue.getArg1();
			String arg2 =  passValue.getArg2();
			String arg3 =  passValue.getArg3();
			String arg4 =  passValue.getArg4();
			String arg5 =  passValue.getArg5();
			String arg6 =  passValue.getArg6();
			String arg7 =  passValue.getArg7();
			String arg8 =  passValue.getArg8();
			String arg9 =  passValue.getArg9();
			String arg10 =  passValue.getArg10();
			String arg11 =  passValue.getArg11();
			String arg12 =  passValue.getArg12();
			String arg13 =  passValue.getArg13();
			String arg14 =  passValue.getArg14();
			String arg15 =  passValue.getArg15();
			
			AndroidUtils.setTextView(this, R.id.wg_feedbackadd_textview_feedbackid,sid);
			AndroidUtils.setTextView(this, R.id.wg_textview3_wguser,arg11);
			AndroidUtils.setTextView(this, R.id.wg_planadd_layout4_textview3,arg12);
			AndroidUtils.setTextView(this, R.id.wg_planadd_layout5_textview3,arg13);
			AndroidUtils.setTextView(this, R.id.wg_date_start,StrEdit.StringLeft(arg14,16));
			AndroidUtils.setTextView(this, R.id.wg_date_end,StrEdit.StringLeft(arg15,16));
			AndroidUtils.setEditText(this, R.id.wg_feedbackadd_edittext2,arg1);
			AndroidUtils.setEditText(this, R.id.wg_feedbackadd_edittext3,arg2);
			AndroidUtils.setEditText(this, R.id.wg_feedbackadd_edittext4,arg3);
			AndroidUtils.setEditText(this, R.id.wg_feedbackadd_edittext5,arg4);
			AndroidUtils.setEditText(this, R.id.wg_feedbackadd_edittext6,arg5);
			AndroidUtils.setEditText(this, R.id.wg_feedbackadd_edittext7,arg6);
			AndroidUtils.setEditText(this, R.id.wg_feedbackadd_edittext8,arg7);
			AndroidUtils.setEditText(this, R.id.wg_feedbackadd_edittext9,arg8);
			AndroidUtils.setEditText(this, R.id.wg_feedbackadd_edittext10,arg9);
			AndroidUtils.setEditText(this, R.id.wg_feedbackadd_edittext11,arg10);
			
			/*
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					dialog.hide();
				}
			}, 1000);
			*/
			
			//((ImageView)includeTitle.findViewById(R.id.iv_hh_head_masterdetail_right_saygoods)).setBackgroundResource(R.drawable.praisetech);
			 
		}
	}
	
	@Override
	public void onClick(View view) {
		
		if(R.id.wg_head_btn_return==view.getId()){
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		}

		if(R.id.wg_feedbackadd_btn_save==view.getId()){
			SaveFeedback();
			return;
		}

		if(R.id.wg_util_head_btn_edit==view.getId()){
			SaveFeedback();
			return;
		}

		if(R.id.wg_util_head_btn_del==view.getId()){
			
			final MyAlertDialog dialog1 = new MyAlertDialog(WgTaskFeedback.this)
			.builder()
			.setTitle("删除")
			.setMsg("删除当前反馈，确定吗?")
			.setNegativeButton("取消", new OnClickListener() {
				@Override
				public void onClick(View v) {
					
				}
			});
			dialog1.setPositiveButton("确认", new OnClickListener() {
				@Override
				public void onClick(View v) {
					delFeedback();
				}
			});
			dialog1.show();
		}

		if(R.id.wg_planadd4_btn==view.getId()) {//选择街道

			Intent intent=new Intent(this, WgPlanSelectStreetActivity.class);
			Bundle bundle=new Bundle();
			bundle.putString("street", "");
			bundle.putString("clickfrom", "taskfeedback");
			intent.putExtras(bundle);
			startActivityForResult(intent,request_Code);
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);//设置新面板右边进入，原来面板左边退出
			return;
		}

		if(R.id.wg_planadd5_btn==view.getId()){//选择网格

			Long userId = PreferenceUtils.getLong(this, "userid", -1);
			String usercode = PreferenceUtils.getString(this,"usercode","");

			TextView textview_street = ((TextView)this.findViewById(R.id.wg_planadd_layout4_textview3));//把街道传递过去
			String street = textview_street.getText().toString();
			if(street.equals("选择街道")){
				street = "";
			}

			if(street.equals("")){
	  	    	ToastUtils.show(this, "请先选择街道！");
	  		    return;
	  	    }

			Intent intent=new Intent(this, WgPlanSelectWangGeActivity.class);
			Bundle bundle=new Bundle();
			bundle.putString("street", street);
			bundle.putString("clickfrom", "taskfeedback");
			intent.putExtras(bundle);
			startActivityForResult(intent,request_Code);
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);//设置新面板右边进入，原来面板左边退出
			return;
		}

		if(R.id.wg_btn_feedback_wgy==view.getId()){//选择网格员
			
			Long userId = PreferenceUtils.getLong(this, "userid", -1);
			String usercode = PreferenceUtils.getString(this,"usercode","");
			
			String street = "";

			//ToastUtils.show(this, "选择");
			
			Intent intent=new Intent(this, WgPlanSelectWgUserNewActivity.class);//WgPlanSelectWgUserActivity
			Bundle bundle=new Bundle();
			bundle.putString("street", street);
			bundle.putString("clickfrom", "taskfeedback");
			intent.putExtras(bundle);
			startActivityForResult(intent,request_Code);
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);//设置新面板右边进入，原来面板左边退出
			return;
		}

		/*
		if(R.id.wg_feedbackadd4_btn==view.getId()) {
						 
			TextView vSex = (TextView)this.findViewById(R.id.wg_feedbackadd_street);
			bottomSexwindow(vSex);
			new Thread(new Runnable(){
	                @Override
	                public void run() {
	                    while(alpha>0.5f){
	                        try {
	                            //4是根据弹出动画时间和减少的透明度计算
	                            Thread.sleep(4);
	                        } catch (InterruptedException e) {
	                            e.printStackTrace();
	                        }
	                        Message msg =mHandler.obtainMessage();
	                        msg.what = 1;
	                        //每次减少0.01，精度越高，变暗的效果越流畅
	                        alpha-=0.01f;
	                        msg.obj =alpha ;
	                        mHandler.sendMessage(msg);
	                    }
	                }

	            }).start();
	            
		}*/

		if(R.id.et_edit_receive_goods_address==view.getId()){ // 显示 省市县
			Log.i("huahua","browse area");
			//modifuBirthDay();
			
			View img_browse = (View)this.findViewById(R.id.ll_edit_receive_goods_address);
			
			bottomcitywindow(img_browse);
			new Thread(new Runnable(){
                @Override
                public void run() {
                    while(alpha>0.5f){
                        try {
                            //4是根据弹出动画时间和减少的透明度计算
                            Thread.sleep(4);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Message msg =mHandler.obtainMessage();
                        msg.what = 1;
                        //每次减少0.01，精度越高，变暗的效果越流畅
                        alpha-=0.01f;
                        msg.obj =alpha ;
                        mHandler.sendMessage(msg);
                    }
                }

            }).start();
			
		}

		if(R.id.btn_start_date==view.getId()){
			bottombirthdaywindow(txtstartdate, "txtstartdate");
			new Thread(new Runnable(){
				@Override
				public void run() {
					while(alpha>0.5f){
						try {
							//4是根据弹出动画时间和减少的透明度计算
							Thread.sleep(4);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						Message msg =mHandler.obtainMessage();
						msg.what = 1;
						//每次减少0.01，精度越高，变暗的效果越流畅
						alpha-=0.01f;
						msg.obj =alpha ;
						mHandler.sendMessage(msg);
					}
				}

			}).start();
		}

		if(R.id.btn_end_date==view.getId()){
			bottombirthdaywindow(txtenddate, "txtenddate");
			new Thread(new Runnable(){
				@Override
				public void run() {
					while(alpha>0.5f){
						try {
							//4是根据弹出动画时间和减少的透明度计算
							Thread.sleep(4);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						Message msg =mHandler.obtainMessage();
						msg.what = 1;
						//每次减少0.01，精度越高，变暗的效果越流畅
						alpha-=0.01f;
						msg.obj =alpha ;
						mHandler.sendMessage(msg);
					}
				}

			}).start();
		}

	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		if (requestCode == request_Code) {
			
			if (resultCode == 3){//网格员
				if (data != null) {
					Bundle b = data.getExtras();
					//String id = b.getString("id");
					String name = b.getString("name");
					if(!name.equals("")){
						AndroidUtils.setTextView(this, R.id.wg_textview3_wguser,name);
					}
				}
			}

			if (resultCode == 4){//街道
				if (data != null) {
					Bundle b = data.getExtras();
					//String id = b.getString("id");
					String name = b.getString("name");
					if(!name.equals("")){
						AndroidUtils.setTextView(this, R.id.wg_planadd_layout4_textview3,name);
					}
				}
			}

			if (resultCode == 5){//网格
				if (data != null) {
					Bundle b = data.getExtras();
					//String id = b.getString("id");
					String name = b.getString("name");
					if(!name.equals("")){
						AndroidUtils.setTextView(this, R.id.wg_planadd_layout5_textview3,name);
					}
				}
			}
			
		}
		
		super.onActivityResult(requestCode, resultCode, data);
	}
	
    //针对选择地区，从下面弹出窗口进行选择
    void bottomcitywindow(View view) {
        if (popupWindow != null && popupWindow.isShowing()) {
            return;
        }

        //修改的话，这个地方要修改 *****************
        //LinearLayout citypickerview = (LinearLayout) getLayoutInflater().inflate(R.layout.wheelcity_cities_layout, null);

        View view1 = dialogm();

        popupWindow = new PopupWindow(view1,
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        //点击空白处时，隐藏掉pop窗口
        popupWindow.setFocusable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        //添加弹出、弹入的动画
        popupWindow.setAnimationStyle(R.style.Popupwindow);
        int[] location = new int[2];
        view.getLocationOnScreen(location);
        popupWindow.showAtLocation(view, Gravity.LEFT | Gravity.BOTTOM, 0, -location[1]);
       
        //修改的话，这个地方要修改 *****************
        //添加按键事件监听
        setAddressListeners(view1);
        //添加pop窗口关闭事件，主要是实现关闭时改变背景的透明度
        popupWindow.setOnDismissListener(new poponDismissListener());
        backgroundAlpha(1f);
    }
    
    //下面是监听 地址
    private void setAddressListeners(View layout) {
    	 
    	Button bt_select_city_ok = (Button) layout.findViewById(R.id.btn_select_city_ok);
    	Button bt_select_city_cancel = (Button) layout.findViewById(R.id.btn_select_city_cancel);
    	
    	ColorStateList whiteColor=getResources().getColorStateList(R.color.popwindowselectok);
    	bt_select_city_ok.setTextColor(whiteColor); 
    	
    	 
    	bt_select_city_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                    //在此处添加你的按键处理 xxx
                	//Log.i("huahua","address listeners---111");
                	//Log.i("huahua",wheelMain.getTime());
                	
                    popupWindow.dismiss(); 
                    backgroundAlpha(1f);  
                    
                    //Log.i("huahua",cityTxt); 
                    
                    //下面是增加地址的地方  cityTxt
                    
                    vCity.setText(curprovince+" "+ curcity +"  " +curcountry);
                }
            }
        });
    	bt_select_city_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                    //在此处添加你的按键处理 xxx
                	 //选照片相 
                	popupWindow.dismiss();
                	backgroundAlpha(1f);
                    return;
                    
                }
            }
        });
    	
    }
    
    /*
     * 设置添加屏幕的背景透明度
     * @param bgAlpha
     */
    public void backgroundAlpha(float bgAlpha)
    {
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.alpha = bgAlpha; //0.0-1.0
        getWindow().setAttributes(lp);           
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
    }
    
    /*
     * 返回或者点击空白位置的时候将背景透明度改回来
     */
    class poponDismissListener implements PopupWindow.OnDismissListener{

        @Override
        public void onDismiss() {
            // TODO Auto-generated method stub
            new Thread(new Runnable(){
                @Override
                public void run() {
                    //此处while的条件alpha不能<= 否则会出现黑屏
                    while(alpha<1f){
                        try {
                            Thread.sleep(4);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Log.d("HeadPortrait","alpha:"+alpha);
                        Message msg =mHandler.obtainMessage();
                        msg.what = 1;
                        alpha+=0.01f;
                        msg.obj =alpha ;
                        mHandler.sendMessage(msg);
                    }
                }

            }).start();
        }

    }
    
    Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case 1: 
                	backgroundAlpha(alpha);
                    //backgroundAlpha((float)msg.obj);
                    break;
            }
        }
    };
    
    private View dialogm() {
		View contentView = LayoutInflater.from(this).inflate(
				R.layout.wheelcity_cities_layout, null);
		final WheelView country = (WheelView) contentView
				.findViewById(R.id.wheelcity_country);
		country.setVisibleItems(3);
		country.setViewAdapter(new CountryAdapter(this));

		final String cities[][] = AddressData.CITIES;
		final String ccities[][][] = AddressData.COUNTIES;
		final WheelView city = (WheelView) contentView
				.findViewById(R.id.wheelcity_city);
		city.setVisibleItems(0);

		// 地区选择
		final WheelView ccity = (WheelView) contentView
				.findViewById(R.id.wheelcity_ccity);
		ccity.setVisibleItems(0);// 不限城市

		country.addChangingListener(new OnWheelChangedListener() {
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				updateCities(city, cities, newValue);
				
				curprovince = AddressData.PROVINCES[country.getCurrentItem()];
				curcity     = AddressData.CITIES[country.getCurrentItem()][city.getCurrentItem()];
				curcountry  = AddressData.COUNTIES[country.getCurrentItem()][city.getCurrentItem()][ccity.getCurrentItem()];
				
				cityTxt = AddressData.PROVINCES[country.getCurrentItem()]
						+ " | "
						+ AddressData.CITIES[country.getCurrentItem()][city
								.getCurrentItem()]
						+ " | "
						+ AddressData.COUNTIES[country.getCurrentItem()][city
								.getCurrentItem()][ccity.getCurrentItem()];
			}
		});

		city.addChangingListener(new OnWheelChangedListener() {
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				updatecCities(ccity, ccities, country.getCurrentItem(),
						newValue);
				cityTxt = AddressData.PROVINCES[country.getCurrentItem()]
						+ " | "
						+ AddressData.CITIES[country.getCurrentItem()][city
								.getCurrentItem()]
						+ " | "
						+ AddressData.COUNTIES[country.getCurrentItem()][city
								.getCurrentItem()][ccity.getCurrentItem()];
				
				
				curprovince = AddressData.PROVINCES[country.getCurrentItem()];
				curcity     = AddressData.CITIES[country.getCurrentItem()][city.getCurrentItem()];
				curcountry  = AddressData.COUNTIES[country.getCurrentItem()][city.getCurrentItem()][ccity.getCurrentItem()];
				
				
			}
		});

		ccity.addChangingListener(new OnWheelChangedListener() {
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				cityTxt = AddressData.PROVINCES[country.getCurrentItem()]
						+ " | "
						+ AddressData.CITIES[country.getCurrentItem()][city
								.getCurrentItem()]
						+ " | "
						+ AddressData.COUNTIES[country.getCurrentItem()][city
								.getCurrentItem()][ccity.getCurrentItem()];
				
				curprovince = AddressData.PROVINCES[country.getCurrentItem()];
				curcity     = AddressData.CITIES[country.getCurrentItem()][city.getCurrentItem()];
				curcountry  = AddressData.COUNTIES[country.getCurrentItem()][city.getCurrentItem()][ccity.getCurrentItem()];
			}
		});

		country.setCurrentItem(1);// 设置北京
		city.setCurrentItem(1);
		ccity.setCurrentItem(1);
		return contentView;
	}
    
	/*
	 * Updates the city wheel
	 */
	private void updateCities(WheelView city, String cities[][], int index) {
		ArrayWheelAdapter<String> adapter = new ArrayWheelAdapter<String>(this,
				cities[index]);
		adapter.setTextSize(18);
		city.setViewAdapter(adapter);
		city.setCurrentItem(0);
	}
	
	/*
	 * Updates the ccity wheel
	 */
	private void updatecCities(WheelView city, String ccities[][][], int index,
			int index2) {
		ArrayWheelAdapter<String> adapter = new ArrayWheelAdapter<String>(this,
				ccities[index][index2]);
		adapter.setTextSize(18);
		city.setViewAdapter(adapter);
		city.setCurrentItem(0);
	}
	
	/*
	 * Adapter for countries
	 */
	private class CountryAdapter extends AbstractWheelTextAdapter {
		// Countries names
		private String countries[] = AddressData.PROVINCES;

		/**
		 * Constructor
		 */
		protected CountryAdapter(Context context) {
			super(context, R.layout.wheelcity_country_layout, NO_RESOURCE);
			setItemTextResource(R.id.wheelcity_country_name);
		}

		@Override
		public View getItem(int index, View cachedView, ViewGroup parent) {
			View view = super.getItem(index, cachedView, parent);
			return view;
		}

		@Override
		public int getItemsCount() {
			return countries.length;
		}

		@Override
		protected CharSequence getItemText(int index) {
			return countries[index];
		}
	}
	
    //保存信息
    public void SaveFeedback(){
    	Long userid = null;
    	User curuser = new User();
  	    curuser = HhApplication.getInstance(this).getHhCart().getUser();
	    if(curuser==null){
	    	userid =-1l;
	    }else{
	    	userid = HhApplication.getInstance(this).getHhCart().getUser().getId();
	    }
	    
	    long userId = PreferenceUtils.getLong(this, "userid", -1);
  	  
  	    //Long userid = HhApplication.getInstance(this).getHhCart().getUser().getId();//未登录会出错
	    
	    String feedbackId = ((TextView)this.findViewById(R.id.wg_feedbackadd_textview_feedbackid)).getText().toString();
	    String text21 = AndroidUtils.getEditText(this, R.id.wg_feedbackadd_edittext2);
  	    String text22 = AndroidUtils.getEditText(this, R.id.wg_feedbackadd_edittext3);
	    String text23 = AndroidUtils.getEditText(this, R.id.wg_feedbackadd_edittext4);
	    String text24 = AndroidUtils.getEditText(this, R.id.wg_feedbackadd_edittext5);
	    String text25 = AndroidUtils.getEditText(this, R.id.wg_feedbackadd_edittext6);
  	    String text26 = AndroidUtils.getEditText(this, R.id.wg_feedbackadd_edittext7);
	    String text27 = AndroidUtils.getEditText(this, R.id.wg_feedbackadd_edittext8);
	    String text28 = AndroidUtils.getEditText(this, R.id.wg_feedbackadd_edittext9);
	    String text29 = AndroidUtils.getEditText(this, R.id.wg_feedbackadd_edittext10);
  	    String text30 = AndroidUtils.getEditText(this, R.id.wg_feedbackadd_edittext11);
  	    String street = ((TextView)this.findViewById(R.id.wg_planadd_layout4_textview3)).getText().toString();
		String wangge = ((TextView)this.findViewById(R.id.wg_planadd_layout5_textview3)).getText().toString();
		String wanggeuser = ((TextView)this.findViewById(R.id.wg_textview3_wguser)).getText().toString();
		String startdate = ((TextView)this.findViewById(R.id.wg_date_start)).getText().toString();
		String enddate = ((TextView)this.findViewById(R.id.wg_date_end)).getText().toString();

		if(street.equals("选择街道")){
			street = "";
		}

		if (street.equals("")){
			ToastUtils.show(this, "请选择街道！");
			return;
		}

  	    if (text21.equals("")){ 
  		    ToastUtils.show(this, "请填写工作证工作服穿戴！");
  		    return;
  	    }

  	    if (text22.equals("")){
  		    ToastUtils.show(this, "请填写安全隐患事件检查！");
  		    return;
  	    }

		if(wangge.equals("选择网格")){
			wangge = "";
		}

		if(wanggeuser.equals("选择网格员")){
			wanggeuser = "";
		}

		if (startdate.equals("请选择")){
			ToastUtils.show(this, "请填写抽查时间！");
			return;
		}

		if (enddate.equals("请选择")){
			ToastUtils.show(this, "请填写采集员到达时间！");
			return;
		}

  	    loading.showLoading();
  	    
  	    SysPassValue temp = new SysPassValue();
		temp.setFunctionName("saveFeedback");
		temp.setUserId(String.valueOf(userId));
		temp.setId(String.valueOf(taskId));
		temp.setArg1(feedbackId);
		temp.setArg2(wanggeuser);
		temp.setArg3(street);
		temp.setArg4(wangge);
		temp.setArg5(startdate);
		temp.setArg6(enddate);
		temp.setArg21(text21);
		temp.setArg22(text22);
		temp.setArg23(text23);
		temp.setArg24(text24);
		temp.setArg25(text25);
		temp.setArg26(text26);
		temp.setArg27(text27);
		temp.setArg28(text28);
		temp.setArg29(text29);
		temp.setArg30(text30);
		
		new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
			@Override
			public void onPostExecute(String name, YGetTask<SysPassValue> task) {
				loading.hideLoading();
				getData(task.getValue());
			}
		},temp).execute();
		
		/*
		new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<List<SysPassValue>>() {
			@Override
			public void onPostExecute(String name, YGetTask<List<SysPassValue>> task) {
				//loading.hideLoading();
				getData(task.getValue());
			}
		},temp).execute();
		*/
    }
    
    private void getData(SysPassValue passValue) {
		if (passValue==null) {
			ToastUtils.show(this, "提交反馈失败！");
		} else {
			String returnResult = passValue.getReturnResult();
			String taskId = passValue.getArg2();
			if(returnResult.equals("1")){
				Intent intent=new Intent(this, WgTaskFeedbackResultActivity.class);
				Bundle bundle=new Bundle();
				bundle.putString("taskId", String.valueOf(taskId));
				bundle.putString("taskName", taskName);
				intent.putExtras(bundle);
				startActivity(intent);
				this.finish();
				overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				return;
			}else{
				ToastUtils.show(this, "提交反馈失败，请检查输入的文字！");
			}
		}
	}
    
	public void delFeedback(){
		
		SysPassValue temp = new SysPassValue();
		temp.setFunctionName("delFeedback");
		temp.setUserId(String.valueOf(userId));
		temp.setId(String.valueOf(feedbackId));
		
		new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
			@Override
			public void onPostExecute(String name, YGetTask<SysPassValue> task) {
				//loading.hideLoading();
				getResult(task.getValue());
			}
		},temp).execute();
	}
	
	private void getResult(SysPassValue passValue) {
		if (passValue==null) {
			ToastUtils.show(this, "没有获取到数据！");
		} else {
			String returnResult = passValue.getReturnResult();
			String taskId = passValue.getId();
			if(returnResult.equals("1")){
				
				this.finish();
				overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
				return;
			}else{
				ToastUtils.show(this, "删除失败！");
				loading.hideLoading();
			}
		}
	}
    
    //针对性别来修改的方式 加载POP窗口
    void bottomSexwindow(TextView view) {
        if (popupWindow != null && popupWindow.isShowing()) {
            return;
        }
        
      //修改的话，这个地方要修改 *****************
        LinearLayout layout = (LinearLayout) getLayoutInflater().inflate(R.layout.activity_sex_popupwindow, null);
        popupWindow = new PopupWindow(layout,
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        //点击空白处时，隐藏掉pop窗口
        popupWindow.setFocusable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        //添加弹出、弹入的动画
        popupWindow.setAnimationStyle(R.style.Popupwindow);
        int[] location = new int[2];
        view.getLocationOnScreen(location);
        popupWindow.showAtLocation(view, Gravity.LEFT | Gravity.BOTTOM, 0, -location[1]);
       
        //修改的话，这个地方要修改 *****************
        //添加按键事件监听
        setSexButtonListeners(layout);
        //添加pop窗口关闭事件，主要是实现关闭时改变背景的透明度
        popupWindow.setOnDismissListener(new poponDismissListener());
        backgroundAlpha(1f);
    }
    
    private void setSexButtonListeners(LinearLayout layout) {
    	
        Button bt_boy = (Button) layout.findViewById(R.id.btn_sex_boys);
        Button bt_girl = (Button) layout.findViewById(R.id.btn_sex_girl); 
        Button bt_cancel = (Button) layout.findViewById(R.id.btn_sex_cancel);

        bt_boy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                    //在此处添加你的按键处理
                    popupWindow.dismiss(); 
                    backgroundAlpha(1f);
                    //loading.showLoading();
                    userModifyData("1","男");
                    /*
        			user.setUserSex("男");
        			user.setUserModifyColumnType("2"); 
        			 
        			new HhUserModifyPersonInfoYTask(new AbstractYGetTaskListener<User>() {
        				@Override
        				public void onPostExecute(String name, YGetTask<User> task) {
        					loading.hideLoading();
        					userModifyData(task.getValue(),"2");					
        				}
        			},user,"2").execute();
        			*/
        			
                    return;
                }
            }
        });
        bt_girl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                	 //在此处添加你的按键处理
                    popupWindow.dismiss();
                    backgroundAlpha(1f);
                    userModifyData("1","女");
                    //照相 选择了男生
                    /*
                    User user = new User();
                    loading.showLoading();  
        			user.setCode(usercode);
        			user.setUserSex("女");
        			user.setUserModifyColumnType("2"); 
        			 
        			new HhUserModifyPersonInfoYTask(new AbstractYGetTaskListener<User>() {
        				@Override
        				public void onPostExecute(String name, YGetTask<User> task) {
        					loading.hideLoading();
        					userModifyData(task.getValue(),"2");					
        				}
        			},user,"2").execute();
        			*/
        			
                    return;
                }
            }
        }); 
        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                    popupWindow.dismiss();
                }
            }
        });
    }
    
    private void userModifyData(String modifyType,String modifyValue) {
    	
    	if(modifyType.equals("1")){
    		//TextView streetView =(TextView)this.findViewById(R.id.wg_planadd_layout4_textview3);
    		//streetView.setVisibility(View.INVISIBLE);//隐藏选择街道
  			AndroidUtils.setTextView(this, R.id.wg_planadd_layout4_textview3,modifyValue);
    	}
    }

	//选择日期
	void bottombirthdaywindow(TextView view, String dateType) {
		if (popupWindow != null && popupWindow.isShowing()) {
			return;
		}
		LinearLayout timepickerview = (LinearLayout) getLayoutInflater().inflate(R.layout.timepicker, null);
		wheelMain = new WheelMain(timepickerview, true);
		ScreenInfo screenInfo = new ScreenInfo(WgTaskFeedback.this);
		wheelMain.screenheight = screenInfo.getHeight();
		String time = txtstartdate.getText().toString();
		Calendar calendar = Calendar.getInstance();
		if (JudgeDate.isDate(time, "yyyy-MM-dd hh:mm")) {
			try {
				calendar.setTime(dateFormat.parse(time));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		int min = calendar.get(Calendar.MINUTE);
		//wheelMain.initDateTimePicker(year, month, day);
		wheelMain.initDateTimePicker(year, month, day, hour, min);

		popupWindow = new PopupWindow(timepickerview,
				ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.WRAP_CONTENT);
		//点击空白处时，隐藏掉pop窗口
		popupWindow.setFocusable(true);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());
		//添加弹出、弹入的动画
		popupWindow.setAnimationStyle(R.style.Popupwindow);
		int[] location = new int[2];
		view.getLocationOnScreen(location);
		popupWindow.showAtLocation(view, Gravity.LEFT | Gravity.BOTTOM, 0, -location[1]);

		//修改的话，这个地方要修改 *****************
		//添加按键事件监听
		setBirthdayListeners(timepickerview, dateType);
		//添加pop窗口关闭事件，主要是实现关闭时改变背景的透明度
		popupWindow.setOnDismissListener(new WgTaskFeedback.poponDismissListener());
		backgroundAlpha(1f);
	}

	//监听日期
	private void setBirthdayListeners(LinearLayout layout, String dateType) {

		Button bt_select_birthday_ok = (Button) layout.findViewById(R.id.btn_select_birthday_ok);
		Button bt_select_birthday_cancel = (Button) layout.findViewById(R.id.btn_select_birthday_cancel);

		ColorStateList whiteColor=getResources().getColorStateList(R.color.popwindowselectok);
		bt_select_birthday_ok.setTextColor(whiteColor);

		final String fdateType = dateType;

		bt_select_birthday_ok.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (popupWindow != null && popupWindow.isShowing()) {
					//在此处添加你的按键处理 xxx

					Log.i("longhua",wheelMain.getTime());

					popupWindow.dismiss();
					backgroundAlpha(1f);

					//loading.showLoading();

					if(fdateType.equals("txtstartdate")){
						txtstartdate.setText(wheelMain.getTime());
					}

					if(fdateType.equals("txtenddate")){
						txtenddate.setText(wheelMain.getTime());
					}

					//loading.hideLoading();
                    /*
        			new HhUserModifyPersonInfoYTask(new AbstractYGetTaskListener<User>() {
        				@Override
        				public void onPostExecute(String name, YGetTask<User> task) {

        					userModifyData(task.getValue(),"3");
        				}
        			},user,"3").execute();
        			*/

				}
			}
		});
		bt_select_birthday_cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (popupWindow != null && popupWindow.isShowing()) {
					//在此处添加你的按键处理 xxx
					//选照片相
					popupWindow.dismiss();
					backgroundAlpha(1f);

					return;

				}
			}
		});

		bt_select_birthday_cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (popupWindow != null && popupWindow.isShowing()) {
					popupWindow.dismiss();
				}
			}
		});
	}
    
    
    
}
