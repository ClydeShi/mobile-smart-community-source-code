package com.zhyq;

 
 

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
 
import com.handmark.pulltorefresh.library.ILoadingLayout;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.zhyq.adapter.WgMultiSelectAdapterCheckbox;
import com.zhyq.adapter.WgMultiSelectAdapterCheckbox.ViewHolder;
 
 
import com.zhyq.libs.PreferenceUtils;
import com.zhyq.libs.ToastUtils;
 
 
import com.zhyq.model.SysPassValue;


import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
 
import com.zhyq.task.impl.sendOrderToServerForListYTask;
 
import com.zhyq.widget.LoadingRelativeLayout;


import android.app.Activity;
import android.content.Intent;
 
 
import android.os.Bundle;
 
 
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;



public class WgPlanSelectWgUserActivity extends Activity implements OnItemClickListener,OnClickListener {
	
	private LoadingRelativeLayout loading = null;
	private View includeTitle;
	private PullToRefreshListView mPullRefreshListView;//扩展的ListView
	private int currentPage = 1; 
	private String lastRefreshdt = "";
    private WgMultiSelectAdapterCheckbox mAdapter;//数据适配器
    private List<SysPassValue> NewsList = new ArrayList<SysPassValue>();//数据
    List<String> chooseIdList = new ArrayList<String>();
    private String street = "";
    private String clickfrom = "";
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.setContentView(R.layout.activity_wg_multiselect);
		loading = LoadingRelativeLayout.getLoadingRelativeLayout(this);
		includeTitle = this.findViewById(R.id.wg_include_singleselect_head);
		Button btreturn = (Button)includeTitle.findViewById(R.id.wg_head_btn_return);
		btreturn.setOnClickListener(this);
		Button btconfirm = (Button)includeTitle.findViewById(R.id.wg_head_btn_confirm);
		btconfirm.setOnClickListener(this);
		((TextView)includeTitle.findViewById(R.id.wg_head_title)).setText("选择网格员");
		
		((Button)this.findViewById(R.id.wg_singleselect1_btn)).setOnClickListener(this);
		street = this.getIntent().getStringExtra("street");
		clickfrom = this.getIntent().getStringExtra("clickfrom");
		
		/*
		 *  初始化数据适配器，传递Content 过去
		 */
		mAdapter = new WgMultiSelectAdapterCheckbox(this, NewsList);
		//mAdapter.setOnClick1(this);
		
		/*
         * 初始化 
         */  
		mPullRefreshListView = (PullToRefreshListView) this.findViewById(R.id.xl_wg_singleselect_PulllistView);
		mPullRefreshListView.setMode(Mode.PULL_FROM_END);
		//初始化值
		initIndicator();
		//设置下拉，上移动 得到效果
        mPullRefreshListView.setOnRefreshListener(new OnRefreshListener2<ListView>()
        {
        	//下拉效果
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView)
            {
            	refreshView.getLoadingLayoutProxy().setPullLabel("下拉刷新...");// 刚下拉时，显示的提示
            	refreshView.getLoadingLayoutProxy().setRefreshingLabel("加载中...");// 刷新时
            	refreshView.getLoadingLayoutProxy().setReleaseLabel("松开刷新...");// 下来达到一定距离时，显示的提示
                
        	   if(lastRefreshdt.equals("")) {
        	    	// 显示最后更新的时间
                   refreshView.getLoadingLayoutProxy().setLastUpdatedLabel("刚刚");
       	       } else {
       	    	// 显示最后更新的时间
                   refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(lastRefreshdt);
       	       }
        	   
        	   SimpleDateFormat    formatter    =   new    SimpleDateFormat    ("MM月dd日    HH:mm:ss     "); 
       	       Date    curDate    =   new    Date(System.currentTimeMillis());//获取当前时间
       	       lastRefreshdt =    formatter.format(curDate);
       	       
                //加载任务
                currentPage = 1;
                GetData(currentPage);
            }
            
            //上拉效果
            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView)
            {
                //加载任务  
            	refreshView.getLoadingLayoutProxy().setPullLabel("上拉刷新...");// 刚上拉时，显示的提示
            	refreshView.getLoadingLayoutProxy().setRefreshingLabel("加载中...");// 刷新时
            	refreshView.getLoadingLayoutProxy().setReleaseLabel("松开刷新...");// 上来达到一定距离时，显示的提示
                currentPage++;
                GetData(currentPage);
            }
        });
        
		//获取控件并注册  这个是获取 listview 控件
        //ListView actualListView = mPullRefreshListView.getRefreshableView();
        //registerForContextMenu(actualListView); 
        //actualListView.setAdapter(mAdapter);
        
        mPullRefreshListView.setAdapter(mAdapter);
        
        /*
         * 在OnItemClickListener方法参数position，从1开始，而不是像ListView那般从0开始，原因是因为PullToRefreshListView有Header，占用了一个item的位置，所以position的值是从1开始
         */
        mPullRefreshListView.setOnItemClickListener(new OnItemClickListener() {
        	
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                    long arg3) {
                // TODO Auto-generated method stub
                // 取得ViewHolder对象
                ViewHolder viewHolder = (ViewHolder) arg1.getTag();
                // 改变CheckBox的状态
                viewHolder.checkBox.toggle();
                // 将CheckBox的选中状况记录下来
                NewsList.get(arg2-1).setBo(viewHolder.checkBox.isChecked());
                
                // 调整选定条目
                if (viewHolder.checkBox.isChecked() == true) {
                	Log.i("longhua","点击"+NewsList.get(arg2-1).getArg2());
                    //num++;
                    //pric += Integer.parseInt(NewsList.get(arg2).getName());
                	/*if(wangge.equals("")){
                		wangge = NewsList.get(arg2).getArg2();
                	}else{
                		wangge = wangge + "," + NewsList.get(arg2).getArg2();
                	}*/
                	chooseIdList.add(NewsList.get(arg2-1).getArg2());
                } else {
                    //num--;
                    //pric -= Integer.parseInt(list.get(arg2).getName());
                	chooseIdList.remove(NewsList.get(arg2-1).getArg2());
                }
                // 用TextView显示
                //price.setText("一共选了" + num + "件," + "价格是" + pric + "元");
            }
        });
        
        onRefresh();
	}
	
	private void onRefresh(){
		mPullRefreshListView.setRefreshing(true);
        //首次加载任务数据 加载任务  
        currentPage = 1;
        GetData(currentPage);
	}
	
	private void initIndicator()
	{
		ILoadingLayout startLabels = mPullRefreshListView.getLoadingLayoutProxy(true,false);
		
		//startLabels.setTextTypeface()
		startLabels.setPullLabel("下拉刷新...");// 刚下拉时，显示的提示
		startLabels.setRefreshingLabel("加载中...");// 刷新时
		startLabels.setReleaseLabel("松开刷新...");// 下来达到一定距离时，显示的提示
		
		ILoadingLayout endLabels = mPullRefreshListView.getLoadingLayoutProxy(false,true);
		endLabels.setPullLabel("上拉刷新...");// 刚上拉时，显示的提示
		endLabels.setRefreshingLabel("加载中...");// 刷新时
		endLabels.setReleaseLabel("松开刷新...");// 上来达到一定距离时，显示的提示
		endLabels.setLastUpdatedLabel("");
	}
	
	//onResume是在启动activity启动之后才能执行的，也就是恢复执行。程序正常启动：onCreate()->onStart()->onResume();
	@Override
	protected void onResume() {   //注意 该函数，如果在打开了其他窗口，再关闭，这个函数也会执行
		super.onResume();
		
	}
	
	public void GetData(int curpage){
		
		ToastUtils.show(this, "获取网格员资料，请稍等");
		loading.showLoading();
		Long userId = PreferenceUtils.getLong(this, "userid", -1);
		String usercode = PreferenceUtils.getString(this,"usercode","");
		
		SysPassValue temp = new SysPassValue();
		temp.setFunctionName("getWgUserList");//getWgUserListDom
		temp.setUserId(String.valueOf(userId));
		temp.setArg1("1");
		temp.setArg2(street);
		temp.setPageNumber(String.valueOf(curpage));
		
		//调用接口得到数据
		new sendOrderToServerForListYTask(new AbstractYGetTaskListener<List<SysPassValue>>() {
			@Override
			public void onPostExecute(String name, YGetTask<List<SysPassValue>> task) {
				
				showData(task.getValue());
				mPullRefreshListView.onRefreshComplete();
				loading.hideLoading();
			}
		},temp).execute();
		
	}
	
	public void showData(List<SysPassValue> list) {
		
		//通过数据适配器获取 数据列表
		View vnorecord = this.findViewById(R.id.activity_no_record_showpic);
		if (list==null){
			if(currentPage==1){
				this.NewsList.clear(); 
	    		mAdapter.notifyDataSetChanged();
	    		vnorecord.setVisibility(View.VISIBLE);
	    		return;
			}
			return;
		}
		vnorecord.setVisibility(View.GONE);
		
		//如果是第一页则需要清楚数据   currentPage
		if(currentPage==1){
    		this.NewsList.clear();
			this.NewsList.addAll(list);
    	} else{
			NewsList.addAll(list);
		}
		mAdapter.notifyDataSetChanged();
		if(NewsList.size()==0){
			vnorecord.setVisibility(View.VISIBLE);
		}
		
	}
	
	@Override
	public void onClick(View view) {
		
		 if(R.id.wg_head_btn_return==view.getId()){
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		  }	
		  
		 if(R.id.wg_singleselect1_btn==view.getId()){
			 Intent intent = new Intent(this,WgPlanAdd.class);
	         intent.putExtra("name", "全部");
	         setResult(3, intent);
	         overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			 this.finish();
			 return;
		 }
		 
		 if(R.id.wg_head_btn_confirm==view.getId()){
			 
			 String strList = "";
			 
			 for(int i = 0;i < chooseIdList.size(); i ++){
				 if(strList.equals("")){
					 strList = chooseIdList.get(i);
				 }else{
					 strList = strList + "," + chooseIdList.get(i);
				 }
			 }
			 
			 Log.i("luktel","获取到"+strList);
			 
			 if(clickfrom.equals("taskfeedback")){
				 Intent intent = new Intent(this,WgTaskFeedback.class);
		         intent.putExtra("name", strList);
		         setResult(3, intent);
			 }else{
				 Intent intent = new Intent(this,WgPlanAdd.class);
		         intent.putExtra("name", strList);
		         setResult(3, intent);
			 }
			 
	         overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			 this.finish();
			 return;
		 }
		 
		 /*
		 if(R.id.wg_wode_singleselect3_btn==view.getId()){
			 
			 //Object tag = view.getTag();
			 Object tag = view.getTag(R.id.button_tag1);
			 
			 if (tag != null && tag instanceof Integer) {
				 int position = (Integer) tag;
				 String id =  NewsList.get(position).getArg1();
				 String name =  NewsList.get(position).getArg2();
				
				 Intent intent=new Intent(this, WgPlanAdd.class);
				 Bundle bundle=new Bundle();
				 bundle.putString("id", id);
				 bundle.putString("name", name);
				 intent.putExtras(bundle);
				 setResult(3,intent);
				 overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				 this.finish();
				 return;
			 }
		 }
		 */
	}
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		
		Log.i("longhua","onItemClick");
		if(arg3 == -1) {
	        // 点击的是headerView或者footerView
	        return;
	    }
		//arg2是当前item的ID。这个id根据你在适配器中的写法可以自己定义
		//ToastUtils.show(arg1.getContext(), R.string.show_logon_error_msg)
		//Object tag2 = arg1.getTag(); 
        int position = (Integer) arg2;
        position = position -1;
		return;
		
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}
	
}
