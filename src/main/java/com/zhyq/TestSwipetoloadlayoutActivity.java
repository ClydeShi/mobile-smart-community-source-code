package com.zhyq;

/*
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.aspsine.fragmentnavigator.FragmentNavigator;
import com.luktel.fragment.BaseToolbarFragment;

import com.luktel.fragment.MainFragmentAdapter;

import java.util.Arrays;
import java.util.List;

public class TestSwipetoloadlayoutActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener
        , BaseToolbarFragment.ToggleDrawerCallBack {

    private static final Integer ID_ARRAY[] = {
            R.id.nav_Twitter_style,
            R.id.nav_google_style,
            R.id.nav_yalantis_style,
            R.id.nav_jd_style,
            R.id.nav_set_header_footer_via_code
    };

    private static final List<Integer> IDS = Arrays.asList(ID_ARRAY);

    private static final int DEFAULT_POSITION = 0;

    private DrawerLayout drawerLayout;

    private FragmentNavigator mFragmentNavigator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mFragmentNavigator = new FragmentNavigator(getSupportFragmentManager(), new MainFragmentAdapter(), R.id.container);

        mFragmentNavigator.setDefaultPosition(DEFAULT_POSITION);

        mFragmentNavigator.onCreate(savedInstanceState);

        setContentView(R.layout.activity_test_fragment);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigationView);

        navigationView.setNavigationItemSelectedListener(this);

        navigationView.setCheckedItem(IDS.get(DEFAULT_POSITION));

        mFragmentNavigator.showFragment(mFragmentNavigator.getCurrentPosition());
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mFragmentNavigator.onSaveInstanceState(outState);
    }

    @Override
    public void openDrawer() {
        drawerLayout.openDrawer(GravityCompat.START);
    }

    @Override
    public boolean onNavigationItemSelected(final MenuItem menuItem) {
        drawerLayout.closeDrawer(GravityCompat.START);
        drawerLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                int itemId = menuItem.getItemId();
                if (itemId == R.id.nav_about) {
                    //startActivity(new Intent(WgFragmentViewActivity.this, AboutActivity.class));
                } else {
                    mFragmentNavigator.showFragment(IDS.indexOf(itemId));
                }
            }
        }, 200);
        return true;
    }
}
*/

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.zhyq.fragment.TestRecyclerFragment;


public class TestSwipetoloadlayoutActivity extends AppCompatActivity implements OnClickListener  {

    private View includeTitle;
    private String id = "";
    private Long userId = null;

    //private TwitterRecyclerFragment content;
    private Fragment content;
    private FragmentManager manager;
    private FragmentTransaction transaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.swipetoloadlayout_test_fragment);
        includeTitle = this.findViewById(R.id.wg_eventdetail_head);
        ((TextView)includeTitle.findViewById(R.id.wg_head_title)).setText("下拉测试");//案件查看
        Button btreturn = (Button)includeTitle.findViewById(R.id.wg_head_btn_return);
        btreturn.setOnClickListener(this);

        manager = getSupportFragmentManager();
        transaction = manager.beginTransaction();
        //content = new TwitterRecyclerFragment();
        //content = TwitterRecyclerFragment.newInstance(TwitterRecyclerFragment.TYPE_LINEAR);
        content = TestRecyclerFragment.newInstance(TestRecyclerFragment.TYPE_LINEAR);//RecyclerFragment里面用的是GsonRequest获取远程数据，需要研究下怎么加载数据进去
        transaction.replace(R.id.container, content);
        transaction.commit();

    }

    @Override
    protected void onResume() {//该函数，如果在打开了其他窗口，再关闭，这个函数也会执行
        // TODO Auto-generated method stub
        super.onResume();
        //Log.i("huahua","taskId = "+taskId);
    }

    @Override
    public void onClick(View view) {

        if(R.id.wg_head_btn_return==view.getId()){
            this.finish();
            overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
            return;
        }

        return;
    }



}