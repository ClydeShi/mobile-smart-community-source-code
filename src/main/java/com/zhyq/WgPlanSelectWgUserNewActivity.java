package com.zhyq;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.aspsine.irecyclerview.IRecyclerView;
import com.aspsine.irecyclerview.OnLoadMoreListener;
import com.aspsine.irecyclerview.OnRefreshListener;
import com.aspsine.irecyclerview.demo.ui.widget.BannerView;
import com.aspsine.irecyclerview.demo.ui.widget.footer.LoadMoreFooterView;
import com.zhyq.adapter.WgMultiSelectAdapterCheckboxNew;
import com.zhyq.adapter.WgMultiSelectAdapterCheckboxNew.OnItemClickListener;
import com.zhyq.libs.BaseActivity;
import com.zhyq.libs.PreferenceUtils;
import com.zhyq.libs.ToastUtils;
import com.zhyq.model.SysPassValue;
import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.sendOrderToServerForListYTask;
import com.zhyq.widget.LoadingRelativeLayout;

import java.util.ArrayList;
import java.util.List;


public class WgPlanSelectWgUserNewActivity extends BaseActivity implements OnClickListener, OnItemClickListener<SysPassValue>, OnRefreshListener, OnLoadMoreListener {

    private LoadingRelativeLayout loading = null;
    private View includeTitle;
    private IRecyclerView iRecyclerView;
    private BannerView bannerView;
    private LoadMoreFooterView loadMoreFooterView;

    private WgMultiSelectAdapterCheckboxNew mAdapter;

    private int currentPage = 1;
    private List<SysPassValue> dataList = new ArrayList<SysPassValue>();//数据
    List<String> chooseIdList = new ArrayList<String>();
    private String street = "";
    private String clickFrom = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wg_multiselect_new);
        loading = LoadingRelativeLayout.getLoadingRelativeLayout(this);
        includeTitle = this.findViewById(R.id.wg_include_multiselect_head);
        Button btnReturn = (Button) includeTitle.findViewById(R.id.wg_head_btn_return);
        btnReturn.setOnClickListener(this);
        Button btnConfirm = (Button) includeTitle.findViewById(R.id.wg_head_btn_confirm);
        btnConfirm.setOnClickListener(this);
        ((TextView) includeTitle.findViewById(R.id.wg_head_title)).setText("选择网格员");

        ((Button) this.findViewById(R.id.wg_multiselect1_btn)).setOnClickListener(this);
        street = this.getIntent().getStringExtra("street");
        clickFrom = this.getIntent().getStringExtra("clickfrom");

        iRecyclerView = (IRecyclerView) findViewById(R.id.wg_irecyclerview_list);
        iRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        bannerView = (BannerView) LayoutInflater.from(this).inflate(R.layout.layout_irecyclerview_banner_view, iRecyclerView.getHeaderContainer(), false);
        //iRecyclerView.addHeaderView(bannerView);

        loadMoreFooterView = (LoadMoreFooterView) iRecyclerView.getLoadMoreFooterView();

        mAdapter = new WgMultiSelectAdapterCheckboxNew(dataList);
        mAdapter.setOnClick1(this);
        iRecyclerView.setIAdapter(mAdapter);

        iRecyclerView.setOnRefreshListener(this);
        iRecyclerView.setOnLoadMoreListener(this);

        mAdapter.setOnItemClickListener(this);

        iRecyclerView.post(new Runnable() {
            @Override
            public void run() {
                iRecyclerView.setRefreshing(true);
            }
        });
    }

    @Override
    public void onClick(View view) {

        if (R.id.wg_head_btn_return == view.getId()) {
            this.finish();
            overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
            return;
        }

        if (R.id.wg_multiselect1_btn == view.getId()) {
            if (clickFrom.equals("taskfeedback")) {
                Intent intent = new Intent(this, WgTaskFeedback.class);
                intent.putExtra("name", "全部");
                setResult(3, intent);
            } else {
                Intent intent = new Intent(this, WgPlanAdd.class);
                intent.putExtra("name", "全部");
                setResult(3, intent);
            }
            overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
            this.finish();
            return;
        }

        if (R.id.wg_head_btn_confirm == view.getId()) {

            String strList = "";

            for (int i = 0; i < chooseIdList.size(); i++) {
                if (strList.equals("")) {
                    strList = chooseIdList.get(i);
                } else {
                    strList = strList + "," + chooseIdList.get(i);
                }
            }

            //Log.i("luktel", "获取到" + strList);

            if (clickFrom.equals("taskfeedback")) {
                Intent intent = new Intent(this, WgTaskFeedback.class);
                intent.putExtra("name", strList);
                setResult(3, intent);
            } else {
                Intent intent = new Intent(this, WgPlanAdd.class);
                intent.putExtra("name", strList);
                setResult(3, intent);
            }
            overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
            this.finish();
            return;
        }

        if (R.id.item_btn1 == view.getId()) {//未测试通过，因为获取不到ViewHolder
            Object tag = view.getTag();
            if (tag != null) {
                int position = (Integer) tag;
                //Toast.makeText(this, dataList.get(position).getArg1(), Toast.LENGTH_SHORT).show();
                /*
                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                int firstItemPosition = layoutManager.findFirstVisibleItemPosition();
                if (position - firstItemPosition >= 0) {
                    //得到要更新的item的view
                    View view2 = iRecyclerView.getChildAt(position - firstItemPosition + 1);
                    if (null != iRecyclerView.getChildViewHolder(view)) {
                        WgMultiSelectAdapterCheckboxNew.ViewHolder viewHolder = (WgMultiSelectAdapterCheckboxNew.ViewHolder) iRecyclerView.getChildViewHolder(view);
                        viewHolder.checkBox.toggle();
                    }
                }
                */

                /*WgMultiSelectAdapterCheckboxNew.ViewHolder viewHolder = (WgMultiSelectAdapterCheckboxNew.ViewHolder)iRecyclerView.getChildViewHolder(iRecyclerView.getChildAt(position+2));
                viewHolder.checkBox.toggle();
                dataList.get(position-1).setBo(viewHolder.checkBox.isChecked());
                if (viewHolder.checkBox.isChecked() == true) {
                    chooseIdList.add(dataList.get(position-1).getArg2());
                } else {
                    chooseIdList.remove(dataList.get(position-1).getArg2());
                }
                return;*/
            }
        }

    }

    @Override
    public void onItemClick(WgMultiSelectAdapterCheckboxNew.ViewHolder viewHolder, int position, SysPassValue data, View v) {//可以参考原文件实现
        //Toast.makeText(this, dataList.get(position).getArg1(), Toast.LENGTH_SHORT).show();
        //WgMultiSelectAdapterCheckboxNew.ViewHolder viewHolder = (WgMultiSelectAdapterCheckboxNew.ViewHolder)iRecyclerView.getChildViewHolder(iRecyclerView.getChildAt(position));
        viewHolder.checkBox.toggle();
        dataList.get(position).setBo(viewHolder.checkBox.isChecked());
        if (viewHolder.checkBox.isChecked() == true) {
            chooseIdList.add(dataList.get(position).getArg2());
        } else {
            chooseIdList.remove(dataList.get(position).getArg2());
        }
        return;
    }

    @Override
    public void onRefresh() {
        //loadBanner();
        loadMoreFooterView.setStatus(LoadMoreFooterView.Status.GONE);
        currentPage = 1;
        getData();
    }

    @Override
    public void onLoadMore() {
        if (loadMoreFooterView.canLoadMore() && mAdapter.getItemCount() > 0) {
            loadMoreFooterView.setStatus(LoadMoreFooterView.Status.LOADING);
            getData();
            //Toast.makeText(this, "已经到了"+currentPage, Toast.LENGTH_SHORT).show();
        }
    }

    public void getData() {

        ToastUtils.show(this, "获取网格员资料，请稍等");
        loading.showLoading();
        Long userId = PreferenceUtils.getLong(this, "userid", -1);
        String usercode = PreferenceUtils.getString(this, "usercode", "");
        SysPassValue temp = new SysPassValue();
        temp.setFunctionName("getWgUserList");//getContactList
        temp.setPageNumber(String.valueOf(this.currentPage));
        temp.setUserId(String.valueOf(userId));
        temp.setArg1("1");
        temp.setArg2(street);

        new sendOrderToServerForListYTask(new AbstractYGetTaskListener<List<SysPassValue>>() {
            @Override
            public void onPostExecute(String name, YGetTask<List<SysPassValue>> task) {
                iRecyclerView.setRefreshing(false);
                showData(task.getValue());
                loading.hideLoading();
            }
        }, temp).execute();
    }

    public void showData(final List<SysPassValue> list) {

        View vnoRecord = this.findViewById(R.id.activity_no_record_showpic);
        if (list == null) {
            loadMoreFooterView.setStatus(LoadMoreFooterView.Status.ERROR);
            //Toast.makeText(this, "网络连接失败", Toast.LENGTH_SHORT).show();

            if (currentPage == 1) {
                this.dataList.clear();
                mAdapter.notifyDataSetChanged();
                vnoRecord.setVisibility(View.INVISIBLE);
                return;
            }
            return;
        }

        if (list.size() == 0) {
            if (currentPage == 1) {
                this.dataList.clear();
                mAdapter.notifyDataSetChanged();
                vnoRecord.setVisibility(View.VISIBLE);//如果是第一页没有数据则显示无记录
            } else {
                loadMoreFooterView.setStatus(LoadMoreFooterView.Status.THE_END);
            }
            //Toast.makeText(this, "已经到了最后一页", Toast.LENGTH_SHORT).show();
        } else {
            if (currentPage == 1) {
                currentPage = 2;
                this.dataList.clear();
                this.dataList.addAll(list);
                vnoRecord.setVisibility(View.GONE);
            } else {

                loadMoreFooterView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        currentPage++;
                        loadMoreFooterView.setStatus(LoadMoreFooterView.Status.GONE);
                        dataList.addAll(list);
                    }
                }, 0);

                //loadMoreFooterView.setStatus(LoadMoreFooterView.Status.GONE);
                //dataList.addAll(list);
            }
            mAdapter.notifyDataSetChanged();
        }
        //vnoRecord.setVisibility(View.GONE);

        //如果第一页则需要清除数据

        if (dataList.size() == 0) {
            //vnoRecord.setVisibility(View.VISIBLE);
        }

        //Log.d("luktel", "showData");

    }


}
