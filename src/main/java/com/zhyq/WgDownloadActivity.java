package com.zhyq;
 
import com.zhyq.download.DownLoadUtils;
import com.zhyq.download.DownloadApk;


import android.app.Activity;


import android.os.Bundle;


import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;


public class WgDownloadActivity extends Activity implements OnClickListener {
    
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {//实现了静默下载功能，但是通知的时候会卡住，需要再看看怎么处理，文件放在download目录下，HhApplication调用HhSysArgument.init(this);已经隐藏
		Log.d("luktel", "WgDownloadActivity:");//define activity in the AndroidManifest.xml
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_wg_contact_list);

		//1.注册下载广播接收器
		DownloadApk.registerBroadcast(this);
		//2.删除已存在的Apk
		DownloadApk.removeFile(this);

		//3.如果手机已经启动下载程序，执行downloadApk。否则跳转到设置界面
		if (DownLoadUtils.getInstance(getApplicationContext()).canDownload()) {
			DownloadApk.downloadApk(getApplicationContext(), "http://121.35.253.146:8418/public/app-release.apk", "案件监督检查系统更新", "龙华区社区网格管理");
			//DownloadApk.downloadApk(getApplicationContext(), "http://www.huiqu.co/public/download/apk/huiqu.apk", "Hobbees更新", "Hobbees");
		} else {
			DownLoadUtils.getInstance(getApplicationContext()).skipToDownloadManager();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onDestroy() {

		//4.反注册广播接收器
		DownloadApk.unregisterBroadcast(this);
		super.onDestroy();
	}
	
	@Override
	public void onClick(View view) {
	}

 
}
