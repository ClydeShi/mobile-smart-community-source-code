package com.zhyq;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.util.common.StrEdit;
import com.zhyq.libs.PreferenceUtils;
import com.zhyq.libs.ProgersssDialog;
import com.zhyq.libs.ToastUtils;
import com.zhyq.libs.iosdialog.widget.MyAlertDialog;
import com.zhyq.model.SysPassValue;
import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.sendOrderToServerForValueYTask;
import com.zhyq.widget.LoadingRelativeLayout;


public class ZyMyModifyPassword extends Activity implements OnClickListener {

	public ProgersssDialog dialog;

	private String userCode = "";
	private String userName = "";
	private Long userId = null;
	private int id = 0;

	private LoadingRelativeLayout loading = null;
	public int resultCode = 0;

	private View includeTitle ;

	private Button  returnButton;
	private Button  saveButton;

	private EditText et_name=null;
	private Button bt_clear=null;
	private EditText et_name2=null;
	private Button bt_clear2=null;

	private String name = "";
	private String type = "";
	private String channelName = "";


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		showProgressDialog();

		this.setContentView(R.layout.activity_my_editpassword);

		loading = LoadingRelativeLayout.getLoadingRelativeLayout(this);

		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		includeTitle = this.findViewById(R.id.my_edit_personinfo_head);

		resultCode = StrEdit.StrToInt(this.getIntent().getStringExtra("resultCode"));//接收用来返回给onActivityResult里判断类型
		name = this.getIntent().getStringExtra("name");
		type = this.getIntent().getStringExtra("type");
		if(type==null){
			ToastUtils.show(this, "参数错误！");
		}
		if(type.equals("password")){
			channelName = "修改密码";
		}

		((TextView)includeTitle.findViewById(R.id.tv_head_save_title)).setText(channelName);

		returnButton = (Button)includeTitle.findViewById(R.id.bt_head_save_return);
		returnButton.setOnClickListener(this);

		saveButton = (Button)this.findViewById(R.id.bt_head_right_save);
		saveButton.setOnClickListener(this);

		et_name = (EditText)this.findViewById(R.id.et_my_personal_input_name);
		bt_clear = (Button)this.findViewById(R.id.bt_my_personal_input_clear);
		bt_clear.setOnClickListener(this);

		et_name2 = (EditText)this.findViewById(R.id.et_my_personal_input_name2);
		bt_clear2 = (Button)this.findViewById(R.id.bt_my_personal_input_clear2);
		bt_clear2.setOnClickListener(this);

		//et_name.setText(name);
		//old_name = name;

		new Handler().postDelayed(new Runnable() {//延迟读取数据
			@Override
			public void run() {
				dialog.dismiss();
			}
		}, 200);

	}

	public void showProgressDialog(){
		dialog = new ProgersssDialog(this);
		//dialog.show();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	public void onClick(View view) {
		if (R.id.bt_head_right_save==view.getId()) {
			Commit();
			return;
		}else if(R.id.bt_head_save_return==view.getId()){
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		}else if(R.id.bt_my_personal_input_clear==view.getId()){
			et_name.setText("");
			return;
		}else if(R.id.bt_my_personal_input_clear2==view.getId()){
			et_name2.setText("");
			return;
		}

	}

	private void closeWindow(){
		this.finish();
		overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
	}

	private void Commit(){

		loading.showLoading();

		Long userId = PreferenceUtils.getLong(this, "userid", -1);
		String usercode = PreferenceUtils.getString(this,"usercode","");

		//String new_input_name = AndroidUtils.getEditText(this, R.id.et_my_personal_input_name);
		String new_input_name = this.et_name.getText().toString();
		String new_input_name2 = this.et_name2.getText().toString();
		if(new_input_name.equals("") || new_input_name2.equals("")){
			loading.hideLoading();
			ToastUtils.show(this,"密码不能为空！");
			return;
		}
		if(!new_input_name.equals(new_input_name2)){
			loading.hideLoading();
			ToastUtils.show(this,"两次输入的密码不同！");
			return;
		}
		SysPassValue temp = new SysPassValue();
		temp.setFunctionName("modifyPassword");
		temp.setUserId(String.valueOf(userId));
		temp.setArg1(new_input_name);

		new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
			@Override
			public void onPostExecute(String name, YGetTask<SysPassValue> task) {
				loading.hideLoading();
				getData(task.getValue());
			}
		},temp).execute();

		return;
	}

	private void getData(SysPassValue passValue) {
		if (passValue==null) {
			ToastUtils.show(this, "数据错误，修改失败！");
			return;
		} else {

			String returnResult = passValue.getReturnResult();
			if(returnResult.equals("1")){

				String newName = this.et_name.getText().toString();

				if(type.equals("password")){
					Intent intent = new Intent(this,ZyMyModifyPassword.class);
					intent.putExtra("newName", "");
					setResult(resultCode, intent);//对应上个页面的onActivityResult里面的resultCode，结果标识
					overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
					this.finish();
					return;
				}

			}else{

				ToastUtils.show(this, "修改失败！");
				return;

			}

		}
	}


}
