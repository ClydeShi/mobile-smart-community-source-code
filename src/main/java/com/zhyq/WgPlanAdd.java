package com.zhyq;

 
 
 
 
 
 

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.util.common.StrEdit;
import com.zhyq.libs.view.wheelview.JudgeDate;
import com.zhyq.libs.view.wheelview.ScreenInfo;
import com.zhyq.libs.view.wheelview.WheelMain;


import com.zhyq.libs.AndroidUtils;
import com.zhyq.libs.PreferenceUtils;
import com.zhyq.libs.ToastUtils;
import com.zhyq.model.CommonData;
import com.zhyq.model.SysPassValue;
import com.zhyq.model.User;
import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.GetTaskDetailYTask;
import com.zhyq.task.impl.sendOrderToServerForValueYTask;
import com.zhyq.widget.LoadingRelativeLayout;


import android.app.Activity;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;


public class WgPlanAdd extends Activity implements OnClickListener {
	
	static boolean active = false;

	private LoadingRelativeLayout loading=null; 
	private View includeTitle ;
	private ImageView returnImage;
	private TextView returnText;
	private Button btSave;
	private Button topSave;
	private Button btDel;
	WheelMain wheelMain;
	private PopupWindow popupWindow;
	private Long addressId = 0l;
	private float alpha = 1f;
	
	TextView txtstartdate;
	TextView txtenddate;
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	private int planId = 0;
	private Long userId = null;
    
	public static int request_Code = 9;
	public static int resultCode = 11;
	public static int resultCodeDelete = 12;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.setContentView(R.layout.activity_wg_plan_add);
		loading=LoadingRelativeLayout.getLoadingRelativeLayout(this);
		includeTitle = this.findViewById(R.id.wg_planadd_head);
		((TextView)includeTitle.findViewById(R.id.wg_head_add_title)).setText("发布计划");
		Button btreturn = (Button)includeTitle.findViewById(R.id.wg_head_add_btn_return);
		btreturn.setOnClickListener(this);
		
		((TextView)includeTitle.findViewById(R.id.wg_util_head_text_add)).setText("保存");
		
		topSave =(Button)includeTitle.findViewById(R.id.wg_util_head_btn_add);
		topSave.setOnClickListener(this);
		
		btSave =(Button)this.findViewById(R.id.wg_planadd_btn_save);
		btSave.setOnClickListener(this);
		
		(this.findViewById(R.id.wg_planadd3_btn)).setOnClickListener(this);
		(this.findViewById(R.id.wg_planadd4_btn)).setOnClickListener(this);
		(this.findViewById(R.id.wg_planadd5_btn)).setOnClickListener(this);
		(this.findViewById(R.id.wg_planadd6_btn)).setOnClickListener(this);
		(this.findViewById(R.id.wg_planadd7_btn)).setOnClickListener(this);
		
		txtstartdate = ((TextView)this.findViewById(R.id.wg_date_start));
		this.findViewById(R.id.btn_start_date).setOnClickListener(this);
		
		txtenddate = ((TextView)this.findViewById(R.id.wg_date_end));
		this.findViewById(R.id.btn_end_date).setOnClickListener(this);
		
		planId = StrEdit.StrToInt(this.getIntent().getStringExtra("planId"));
		
		if(planId>0){
			loading.showLoading();
			getPlanData();
		}
		
		//ColorStateList redColor=getResources().getColorStateList(R.color.red);//删除按钮字体设置为红色
		//btDel.setTextColor(redColor);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
	}
	
	protected void getPlanData() {
		new GetTaskDetailYTask(new AbstractYGetTaskListener<CommonData>() {
			@Override
			public void onPostExecute(String name, YGetTask<CommonData> task) {//onPostExecute是AbstractYGetTaskListener里的方法
				
				getData(task.getValue());//task.getValue()是CommonData定义的对象，这个是返回的结果
				loading.hideLoading();
			}
		},String.valueOf(planId),String.valueOf(this.userId)).execute();
	}
	
	private void getData(CommonData taskdetail) {
		if (taskdetail==null) {
			ToastUtils.show(this,"没有获取到信息");
			return;
		} else {
			 /*((TextView)includeMaster.findViewById(R.id.tv_masterdetail_head_image_shopname)).setText(masterviewdetail.getArg1());  
			 AndroidUtils.setWebImageView(includeMaster, R.id.hh_masterdetail_head_image_shoplogimage, masterviewdetail.getArg2()); 
			 AndroidUtils.setWebImageView(includeMaster, R.id.hh_masterdetail_head_image_masterimage, masterviewdetail.getArg3());*/
			 
			String varValue1 = taskdetail.getVarValue1();
			String title = taskdetail.getVarValue2();
			String categoryName = taskdetail.getVarValue3();
			String street = taskdetail.getVarValue4();
			String wangge = taskdetail.getVarValue5();
			String wanggeuser = taskdetail.getVarValue6();
			String checkuser = taskdetail.getVarValue7();
			String text = taskdetail.getVarValue8();
			String startdate = taskdetail.getVarValue9();
			String enddate = taskdetail.getVarValue10();
			String categoryId = taskdetail.getVarValue11();
			String checkuserId = taskdetail.getVarValue12();
			
			AndroidUtils.setEditText(this, R.id.wg_planadd_edittext_name,title);
			AndroidUtils.setEditText(this, R.id.wg_planadd_layout9_edittext,text);
			AndroidUtils.setTextView(this, R.id.wg_planadd_layout3_textview3_id,categoryId);
			AndroidUtils.setTextView(this, R.id.wg_planadd_layout3_textview3,categoryName);
			AndroidUtils.setTextView(this, R.id.wg_planadd_layout4_textview3,street);
			AndroidUtils.setTextView(this, R.id.wg_planadd_layout5_textview3,wangge);
			AndroidUtils.setTextView(this, R.id.wg_planadd_layout6_textview3,wanggeuser);
			AndroidUtils.setTextView(this, R.id.wg_planadd_layout7_textview3_id,checkuserId);
			AndroidUtils.setTextView(this, R.id.wg_planadd_layout7_textview3,checkuser);
			AndroidUtils.setTextView(this, R.id.wg_date_start,startdate);
			AndroidUtils.setTextView(this, R.id.wg_date_end,enddate);
			
			//((ImageView)includeTitle.findViewById(R.id.iv_hh_head_masterdetail_right_saygoods)).setBackgroundResource(R.drawable.praisetech);
		}
	}
	
	@Override
	public void onClick(View view) {
		
		if(R.id.wg_head_add_btn_return==view.getId()){
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		}
		
		if(R.id.wg_planadd_btn_save==view.getId()){
			SavePlan();
			return;
		}
		
		if(R.id.wg_util_head_btn_add==view.getId()){
			SavePlan();
			return;
		}
		
		if(R.id.wg_planadd3_btn==view.getId()){//选择类别
			
			Long userId = PreferenceUtils.getLong(this, "userid", -1);
			String usercode = PreferenceUtils.getString(this,"usercode","");
			
			Intent intent=new Intent(this, WgPlanSelectCategoryActivity.class);
			Bundle bundle=new Bundle();
			bundle.putString("userId", String.valueOf(userId));
			intent.putExtras(bundle);
			startActivityForResult(intent,request_Code);//这里的request_Code要大于0，现在用的9是用来onActivityResult里判断由这里发出的新Activity，获取返回值的请求码
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);//设置新面板右边进入，原来面板左边退出
			return;
		}
		
		if(R.id.wg_planadd4_btn==view.getId()) {//选择街道
			
			Intent intent=new Intent(this, WgPlanSelectStreetActivity.class);
			Bundle bundle=new Bundle();
			bundle.putString("street", "");
			bundle.putString("clickfrom", "");
			intent.putExtras(bundle);
			startActivityForResult(intent,request_Code);
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);//设置新面板右边进入，原来面板左边退出
			return;
			
			/*
			TextView vcategory = (TextView)this.findViewById(R.id.wg_planadd_layout4_textview3);
			bottomSelectwindow(vcategory);
			new Thread(new Runnable(){
	                @Override
	                public void run() {
	                    while(alpha>0.5f){
	                        try {
	                            //4是根据弹出动画时间和减少的透明度计算
	                            Thread.sleep(4);
	                        } catch (InterruptedException e) {
	                            e.printStackTrace();
	                        }
	                        Message msg =mHandler.obtainMessage();
	                        msg.what = 1;
	                        //每次减少0.01，精度越高，变暗的效果越流畅
	                        alpha-=0.01f;
	                        msg.obj =alpha ;
	                        mHandler.sendMessage(msg);
	                    }
	                }
	            }).start();
			*/
		}
		
		if(R.id.wg_planadd5_btn==view.getId()){//选择网格
			
			Long userId = PreferenceUtils.getLong(this, "userid", -1);
			String usercode = PreferenceUtils.getString(this,"usercode","");
			
			TextView textview_street = ((TextView)this.findViewById(R.id.wg_planadd_layout4_textview3));//把街道传递过去
			String street = textview_street.getText().toString();
			if(street.equals("选择街道")){
				street = "";
			}
			
			/*
			if(street.equals("选择街道")){
	  	    	ToastUtils.show(this, "请先选择街道！");
	  		    return;
	  	    }
	  	    */
			
			Intent intent=new Intent(this, WgPlanSelectWangGeActivity.class);
			Bundle bundle=new Bundle();
			bundle.putString("street", street);
			bundle.putString("clickfrom", "");
			intent.putExtras(bundle);
			startActivityForResult(intent,request_Code);
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);//设置新面板右边进入，原来面板左边退出
			return;
		}
		
		if(R.id.wg_planadd6_btn==view.getId()){//选择网格员
			
			Long userId = PreferenceUtils.getLong(this, "userid", -1);
			String usercode = PreferenceUtils.getString(this,"usercode","");
			
			TextView textview_street = ((TextView)this.findViewById(R.id.wg_planadd_layout4_textview3));//把街道传递过去
			String street = textview_street.getText().toString();
			if(street.equals("选择街道")){
				street = "";
			}
			
			/*
			if(street.equals("选择街道")){
	  	    	ToastUtils.show(this, "请先选择街道！");
	  		    return;
	  	    }
	  	    */
			
			Intent intent=new Intent(this, WgPlanSelectWgUserNewActivity.class);//WgPlanSelectWgUserActivity
			Bundle bundle=new Bundle();
			bundle.putString("street", street);
			bundle.putString("clickfrom", "");
			intent.putExtras(bundle);
			startActivityForResult(intent,request_Code);
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);//设置新面板右边进入，原来面板左边退出
			return;
		}
		
		if(R.id.wg_planadd7_btn==view.getId()){//选择检查人员
			
			Long userId = PreferenceUtils.getLong(this, "userid", -1);
			String usercode = PreferenceUtils.getString(this,"usercode","");
			
			Intent intent=new Intent(this, WgPlanSelectUserActivity.class);
			Bundle bundle=new Bundle();
			bundle.putString("userId", String.valueOf(userId));
			intent.putExtras(bundle);
			startActivityForResult(intent,request_Code);
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);//设置新面板右边进入，原来面板左边退出
			return;
		}
		
		if(R.id.btn_start_date==view.getId()){
			bottombirthdaywindow(txtstartdate, "txtstartdate");
			new Thread(new Runnable(){
                @Override
                public void run() {
                    while(alpha>0.5f){
                        try {
                            //4是根据弹出动画时间和减少的透明度计算
                            Thread.sleep(4);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Message msg =mHandler.obtainMessage();
                        msg.what = 1;
                        //每次减少0.01，精度越高，变暗的效果越流畅
                        alpha-=0.01f;
                        msg.obj =alpha ;
                        mHandler.sendMessage(msg);
                    }
                }
                
            }).start();
		}
	 
		if(R.id.btn_end_date==view.getId()){
			bottombirthdaywindow(txtenddate, "txtenddate");
			new Thread(new Runnable(){
                @Override
                public void run() {
                    while(alpha>0.5f){
                        try {
                            //4是根据弹出动画时间和减少的透明度计算
                            Thread.sleep(4);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Message msg =mHandler.obtainMessage();
                        msg.what = 1;
                        //每次减少0.01，精度越高，变暗的效果越流畅
                        alpha-=0.01f;
                        msg.obj =alpha ;
                        mHandler.sendMessage(msg);
                    }
                }
                
            }).start();
		}
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		if (requestCode == request_Code) {
			
			/*if (resultCode == 8){
				if (data != null) {
					Bundle b = data.getExtras();
					String categoryId = b.getString("categoryId");
	         	 	String categoryName = b.getString("categoryName");
	         	 	AndroidUtils.setTextView(this, R.id.wg_planadd_layout3_textview3_id,categoryId);
	            	AndroidUtils.setTextView(this, R.id.wg_planadd_layout3_textview3,categoryName);
				}
			}*/
			
			if (resultCode == 1){//分类结果标识
				if (data != null) {
					Bundle b = data.getExtras();
					String categoryId = b.getString("categoryId");
	         	 	String categoryName = b.getString("categoryName");
	         	 	AndroidUtils.setTextView(this, R.id.wg_planadd_layout3_textview3_id,categoryId);
	            	AndroidUtils.setTextView(this, R.id.wg_planadd_layout3_textview3,categoryName);
				}
			}
			
			if (resultCode == 2){//网格
				if (data != null) {
					Bundle b = data.getExtras();
	         	 	String name = b.getString("name");
	         	 	if(!name.equals("")){
	         	 		AndroidUtils.setTextView(this, R.id.wg_planadd_layout5_textview3,name);
	         	 	}
				}
			}
			
			if (resultCode == 3){//网格员
				if (data != null) {
					Bundle b = data.getExtras();
					//String id = b.getString("id");
					String name = b.getString("name");
					if(!name.equals("")){
						AndroidUtils.setTextView(this, R.id.wg_planadd_layout6_textview3,name);
					}
				}
			}
			
			if (resultCode == 5){//检查人员
				if (data != null) {
					Bundle b = data.getExtras();
					String id = b.getString("id");
					String name = b.getString("name");
					if(!name.equals("")){
						AndroidUtils.setTextView(this, R.id.wg_planadd_layout7_textview3_id,id);
		            	AndroidUtils.setTextView(this, R.id.wg_planadd_layout7_textview3,name);
					}
				}
			}
			
			if (resultCode == 10){//街道
				if (data != null) {
					Bundle b = data.getExtras();
	         	 	String name = b.getString("name");
	         	 	if(!name.equals("")){
	         	 		AndroidUtils.setTextView(this, R.id.wg_planadd_layout4_textview3,name);
	         	 	}
				}
			}
		}
		
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	public void SavePlan(){
    	Long userid = null;
    	User curuser = new User();
    	curuser = HhApplication.getInstance(this).getHhCart().getUser();
    	if(curuser==null){
    		userid =-1l;
      	}else{
      		userid = HhApplication.getInstance(this).getHhCart().getUser().getId();
      	}
    	//Long userid = HhApplication.getInstance(this).getHhCart().getUser().getId();//未登录会出错
    	
    	Long userId = PreferenceUtils.getLong(this,"userid",-1);
    	String userCode = PreferenceUtils.getString(this,"usercode","");
    	
  	    String title = AndroidUtils.getEditText(this, R.id.wg_planadd_edittext_name);
  	    
  	    TextView txtcategory = ((TextView)this.findViewById(R.id.wg_planadd_layout3_textview3_id));
	    String category = txtcategory.getText().toString();
	    
	    TextView txtstreet = ((TextView)this.findViewById(R.id.wg_planadd_layout4_textview3));
  	    String street = txtstreet.getText().toString();
  	    
	    String wangge = ((TextView)this.findViewById(R.id.wg_planadd_layout5_textview3)).getText().toString();
	    String wanggeuser = ((TextView)this.findViewById(R.id.wg_planadd_layout6_textview3)).getText().toString();
	    String checkuserId = ((TextView)this.findViewById(R.id.wg_planadd_layout7_textview3_id)).getText().toString();
	    String checkuserName = ((TextView)this.findViewById(R.id.wg_planadd_layout7_textview3)).getText().toString();
	    String startdate = ((TextView)this.findViewById(R.id.wg_date_start)).getText().toString();
	    String enddate = ((TextView)this.findViewById(R.id.wg_date_end)).getText().toString();
  	    String content = AndroidUtils.getEditText(this, R.id.wg_planadd_layout9_edittext);
  	    
  	    if(title.equals("")){
  	    	ToastUtils.show(this, "请输入计划名称！");
  		    return;
  	    }

  	    if(category.equals("")){
	    	ToastUtils.show(this, "请选择类别！");
		    return;
	    }

  	    if(street.equals("选择街道")){
  	    	ToastUtils.show(this, "请选择街道！");
  		    return;
  	    }
  	    
  	    if(checkuserName.equals("选择检查人员")){
	    	ToastUtils.show(this, "请选择检查人员！");
		    return;
	    }
  	    
  	    if(content.equals("")){
	    	ToastUtils.show(this, "请输入计划内容！");
		    return;
	    }

  	    if(startdate.equals("请选择")){
	    	ToastUtils.show(this, "请选择开始时间！");
		    return;
	    }
  	    
  	    if(enddate.equals("请选择")){
	    	ToastUtils.show(this, "请选择结束时间！");
		    return;
	    }
  	    
  	    loading.showLoading();
  	    
  	    SysPassValue temp = new SysPassValue();
		temp.setFunctionName("savePlan");
		temp.setUserId(String.valueOf(userId));
		temp.setId(String.valueOf(planId));
		temp.setArg1(title);
		temp.setArg2(category);
		temp.setArg3(street);
		temp.setArg4(wangge);
		temp.setArg5(wanggeuser);
		temp.setArg6(checkuserId);
		temp.setArg7(checkuserName);
		temp.setArg8(startdate);
		temp.setArg9(enddate);
		temp.setArg10(content);
		temp.setArg11("1");//shopid
		
		new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
			@Override
			public void onPostExecute(String name, YGetTask<SysPassValue> task) {
				loading.hideLoading();
				getData(task.getValue());
			}
		},temp).execute();
	}
	
	private void getData(SysPassValue passValue) {
		if (passValue==null) {
			ToastUtils.show(this, "保存失败！");
		} else {
			String returnResult = passValue.getReturnResult();
			String planId = "";
			if(returnResult.equals("1")){
				Intent intent=new Intent(this, WgPlanAddResultActivity.class);
				Bundle bundle=new Bundle();
				bundle.putString("planId", String.valueOf(planId));
				intent.putExtras(bundle);
				startActivity(intent);
				this.finish();
				overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				return;
			}else{
				ToastUtils.show(this, "保存失败，请检查输入的文字！");
			}
		}
	}
    
    void bottomSelectwindow(TextView view) {
        if (popupWindow != null && popupWindow.isShowing()) {
            return;
        }
        
      //修改的话，这个地方要修改 *****************
        LinearLayout layout = (LinearLayout) getLayoutInflater().inflate(R.layout.activity_wg_street_popupwindow, null);
        popupWindow = new PopupWindow(layout,
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        //点击空白处时，隐藏掉pop窗口
        popupWindow.setFocusable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        //添加弹出、弹入的动画
        popupWindow.setAnimationStyle(R.style.Popupwindow);
        int[] location = new int[2];
        view.getLocationOnScreen(location);
        popupWindow.showAtLocation(view, Gravity.LEFT | Gravity.BOTTOM, 0, -location[1]);
       
        //修改的话，这个地方要修改 *****************
        //添加按键事件监听
        setSelectButtonListeners(layout);
        //添加pop窗口关闭事件，主要是实现关闭时改变背景的透明度
        popupWindow.setOnDismissListener(new poponDismissListener());
        backgroundAlpha(1f);
    }
    
    private void setSelectButtonListeners(LinearLayout layout) {
    	
        Button btn_street_lh = (Button) layout.findViewById(R.id.btn_street_lh);
        Button btn_street_mz = (Button) layout.findViewById(R.id.btn_street_mz);
        Button btn_street_gh = (Button) layout.findViewById(R.id.btn_street_gh);
        Button btn_street_hc = (Button) layout.findViewById(R.id.btn_street_hc);
        Button btn_street_gl = (Button) layout.findViewById(R.id.btn_street_gl);
        Button btn_street_dl = (Button) layout.findViewById(R.id.btn_street_dl);
        Button bt_cancel = (Button) layout.findViewById(R.id.btn_sex_cancel);

        btn_street_lh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                    //在此处添加你的按键处理
                    popupWindow.dismiss(); 
                    backgroundAlpha(1f);
                    //loading.showLoading();
                    userModifyData("1","龙华");
                    /*
        			user.setUserSex("男");
        			user.setUserModifyColumnType("2"); 
        			 
        			new HhUserModifyPersonInfoYTask(new AbstractYGetTaskListener<User>() {
        				@Override
        				public void onPostExecute(String name, YGetTask<User> task) {
        					loading.hideLoading();
        					userModifyData(task.getValue(),"2");					
        				}
        			},user,"2").execute();
        			*/
        			
                    return;
                }
            }
        });
        btn_street_mz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                	 //在此处添加你的按键处理
                    popupWindow.dismiss();
                    backgroundAlpha(1f);
                    userModifyData("1","民治");
                    return;
                }
            }
        });
        btn_street_gh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                	 //在此处添加你的按键处理
                    popupWindow.dismiss();
                    backgroundAlpha(1f);
                    userModifyData("1","观湖");
                    return;
                }
            }
        });
        btn_street_hc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                	 //在此处添加你的按键处理
                    popupWindow.dismiss();
                    backgroundAlpha(1f);
                    userModifyData("1","福城");
                    return;
                }
            }
        });
        btn_street_gl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                	 //在此处添加你的按键处理
                    popupWindow.dismiss();
                    backgroundAlpha(1f);
                    userModifyData("1","观澜");
                    return;
                }
            }
        });
        btn_street_dl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                	 //在此处添加你的按键处理
                    popupWindow.dismiss();
                    backgroundAlpha(1f);
                    userModifyData("1","大浪");
                    return;
                }
            }
        });
        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                    popupWindow.dismiss();
                }
            }
        });
    }
    
    private void userModifyData(String modifyType,String modifyValue) {
    	
    	if(modifyType.equals("1")){
    		//TextView streetView =(TextView)this.findViewById(R.id.wg_planadd_layout4_textview3);
    		//streetView.setVisibility(View.INVISIBLE);//隐藏选择街道
  			AndroidUtils.setTextView(this, R.id.wg_planadd_layout4_textview3,modifyValue);
    	}
    	
    }
    
    public String getNowDate(){
        Date currTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd",Locale.US);
        String nowdate = null;
        try {
            nowdate = new String(formatter.format(currTime).getBytes("iso-8859-1"));
        } catch (UnsupportedEncodingException ex) {
        }

        return nowdate;
    }
	
	//选择日期
	void bottombirthdaywindow(TextView view, String dateType) {
        if (popupWindow != null && popupWindow.isShowing()) {
            return;
        }
        LinearLayout timepickerview = (LinearLayout) getLayoutInflater().inflate(R.layout.timepicker, null);
        wheelMain = new WheelMain(timepickerview);
        ScreenInfo screenInfo = new ScreenInfo(WgPlanAdd.this);
		wheelMain.screenheight = screenInfo.getHeight();
		String time = txtstartdate.getText().toString();
		Calendar calendar = Calendar.getInstance();
		if (JudgeDate.isDate(time, "yyyy-MM-dd")) {
			try {
				calendar.setTime(dateFormat.parse(time));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		wheelMain.initDateTimePicker(year, month, day);
		
        popupWindow = new PopupWindow(timepickerview,
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        //点击空白处时，隐藏掉pop窗口
        popupWindow.setFocusable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        //添加弹出、弹入的动画
        popupWindow.setAnimationStyle(R.style.Popupwindow);
        int[] location = new int[2];
        view.getLocationOnScreen(location);
        popupWindow.showAtLocation(view, Gravity.LEFT | Gravity.BOTTOM, 0, -location[1]);
       
        //修改的话，这个地方要修改 *****************
        //添加按键事件监听
        setBirthdayListeners(timepickerview, dateType);
        //添加pop窗口关闭事件，主要是实现关闭时改变背景的透明度
        popupWindow.setOnDismissListener(new poponDismissListener());
        backgroundAlpha(1f);
    }
	
	//监听日期
	private void setBirthdayListeners(LinearLayout layout, String dateType) {
    	
    	Button bt_select_birthday_ok = (Button) layout.findViewById(R.id.btn_select_birthday_ok);
    	Button bt_select_birthday_cancel = (Button) layout.findViewById(R.id.btn_select_birthday_cancel);
    	
    	ColorStateList whiteColor=getResources().getColorStateList(R.color.popwindowselectok);
    	bt_select_birthday_ok.setTextColor(whiteColor);
    	
    	final String fdateType = dateType;
    	
    	bt_select_birthday_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                    //在此处添加你的按键处理 xxx
                	
                	Log.i("longhua",wheelMain.getTime());
                	
                    popupWindow.dismiss(); 
                    backgroundAlpha(1f);
                    
                    //loading.showLoading();
                    
                    if(fdateType.equals("txtstartdate")){
                    	txtstartdate.setText(wheelMain.getTime());
                    }
                    
                    if(fdateType.equals("txtenddate")){
                    	txtenddate.setText(wheelMain.getTime());
                    }
                    
                    //loading.hideLoading();
                    /*
        			new HhUserModifyPersonInfoYTask(new AbstractYGetTaskListener<User>() {
        				@Override
        				public void onPostExecute(String name, YGetTask<User> task) {
        					
        					userModifyData(task.getValue(),"3");					
        				}
        			},user,"3").execute();
        			*/
                     
                }
            }
        });
    	bt_select_birthday_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                    //在此处添加你的按键处理 xxx
                	 //选照片相 
                	popupWindow.dismiss();
                	backgroundAlpha(1f);
                	
                    return;

                }
            }
        });
        
    	bt_select_birthday_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                    popupWindow.dismiss();
                }
            }
        });
    }
	
	/*
     * 设置添加屏幕的背景透明度
     * @param bgAlpha
     */
    public void backgroundAlpha(float bgAlpha)
    {
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.alpha = bgAlpha; //0.0-1.0
        getWindow().setAttributes(lp);           
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        
       /*
        *    WindowManager.LayoutParams lp = activity.getWindow().getAttributes();  
        lp.alpha = bgAlpha;  
        if (bgAlpha == 1) {  
            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);//不移除该Flag的话,在有视频的页面上的视频会出现黑屏的bug  
        } else {  
            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);//此行代码主要是解决在华为手机上半透明效果无效的bug  
        }  
        activity.getWindow().setAttributes(lp); 
        */
    }
	
	/*
     * 返回或者点击空白位置的时候将背景透明度改回来
     */
    class poponDismissListener implements PopupWindow.OnDismissListener{

        @Override
        public void onDismiss() {
            // TODO Auto-generated method stub
            new Thread(new Runnable(){
                @Override
                public void run() {
                    //此处while的条件alpha不能<= 否则会出现黑屏
                    while(alpha<1f){
                        try {
                            Thread.sleep(4);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Log.d("longhua","alpha:"+alpha);
                        Message msg =mHandler.obtainMessage();
                        msg.what = 1;
                        alpha+=0.01f;
                        msg.obj =alpha ;
                        mHandler.sendMessage(msg);
                    }
                }

            }).start();
        }
    }
    
    Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case 1: 
                	backgroundAlpha(alpha);
                    //backgroundAlpha((float)msg.obj);
                    break;
            }
        }
    };
    
}
