package com.zhyq.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

 
import com.tencent.mm.sdk.constants.ConstantsAPI;
import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.zhyq.R;
import com.zhyq.libs.Session;
import com.zhyq.libs.ToastUtils;
import com.zhyq.model.SysPassValue;
import com.zhyq.wechat.WechatConstants;
 

/**
 * 微信支付完成后回调类
 * 此类必须放在包名 + wxapi之下
 * @author lhd
 */
public class WXPayEntryActivity extends Activity implements IWXAPIEventHandler, View.OnClickListener {


    private String order_number;  // 订单号
    private String service_name; // 服务名称
    private String service_price; // 服务价格
    private String pay_style; // 支付方式
    private String pay_resulturl; // 支付结果刷新界面
    private String pay_shopid; //付款店铺
    private String pay_userid; // 付款USERID
    private String pay_telephone; //付款电话
    
    private ImageView iv_fkimage;
    private TextView  tv_fkstatus;
    

    private IWXAPI api;

    private static final String TAG = "MicroMsg.SDKSample.WXPayEntryActivity";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_result_for_huahua);
        api = WXAPIFactory.createWXAPI(this, WechatConstants.APP_ID);
        api.handleIntent(getIntent(), this);
        
        
        Session session = Session.getSession();
        order_number = (String) session.get("Out_trade_no");
        service_name = (String) session.get("Fee_name");
        service_price = (String) session.get("Fee_money");
        pay_style = (String) session.get("zhifuway");
        
        pay_shopid = (String) session.get("pay_shopid");
        pay_userid = (String) session.get("pay_userid");
        pay_telephone = (String) session.get("pay_telephone");
        
        
        iv_fkimage =(ImageView) this.findViewById(R.id.iv_pay_result_forhh);
        tv_fkstatus  = (TextView)this.findViewById(R.id.tv_pay_result_forhh);
        
        
        TextView tv_paymoney = (TextView)this.findViewById(R.id.tv_result_paymoney);
        tv_paymoney.setText("￥"+service_price+"");
        
        
        
        
     

        
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        api.handleIntent(intent, this);
    }

    @Override
    public void onReq(BaseReq req) {
    }

    @Override
    public void onResp(BaseResp resp) {
        if (resp.getType() == ConstantsAPI.COMMAND_PAY_BY_WX) {
            if (BaseResp.ErrCode.ERR_OK == resp.errCode) {                // 成功
            	iv_fkimage.setImageResource(R.drawable.circle_green_blackground);
            	tv_fkstatus.setText("支付成功"); 
            } else if (BaseResp.ErrCode.ERR_USER_CANCEL == resp.errCode){ // 取消支付
            	iv_fkimage.setImageResource(R.drawable.hh_ic_zhifu_false);
  				tv_fkstatus.setText("取消支付");	
            } else {                                                      // 支付失败
            	//hh_ic_zhifu_false
  				iv_fkimage.setImageResource(R.drawable.hh_ic_zhifu_false);
  				tv_fkstatus.setText("支付失败");
            }
            /*
            //读取会员余额
            new GetHuiYuanYuOuYTask(new AbstractYGetTaskListener<SysPassValue>() {
       			@Override
       			public void onPostExecute(String name, YGetTask<SysPassValue> task) {
       				//loading.hideLoading();
       				getData(task.getValue());					
       			}
       		},pay_shopid,pay_userid,pay_telephone).execute();
       		*/
        }
    }

    
  //正常登入
  	private void getData(SysPassValue result) {
  		if (result==null) {
  			ToastUtils.show(this, R.string.show_logon_error_msg);
  			return;
  		} else {
  			String shifouweivip = result.getArg1();
  			String yuoue = result.getArg2();
  			
  			if(shifouweivip.equals("Y")){
  				
  	  			View v1 = this.findViewById(R.id.rl_service_huiyuan_yuou);
  				if(yuoue.equals("")){
  					v1.setVisibility(View.GONE);
  				}else{
  					v1.setVisibility(View.VISIBLE);
  					
  					((TextView)this.findViewById(R.id.tv_service_price)).setText("￥"+yuoue+"元");
  				}  				
  				
  			}else{
  				View v1 = this.findViewById(R.id.rl_service_huiyuan_yuou);
  				v1.setVisibility(View.GONE);
  			}
  			

  		   
  		}
  	}
    

    @Override
    public void onClick(View view) {

    }
}