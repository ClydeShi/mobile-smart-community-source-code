package com.zhyq;


import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.zhyq.libs.AndroidUtils;


public class ZySaveSuccessActivity extends FragmentActivity implements OnClickListener {
	
	private View includeTitle;
	private Long userId = null;

	private String type = "";
	private String message = "";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_wg_task_feedbackresult);
		includeTitle = this.findViewById(R.id.wg_feedbackresult_head);

		message = this.getIntent().getStringExtra("message");
		if(message==null){
			message = "";
		}

		((TextView)includeTitle.findViewById(R.id.wg_head_title)).setText("操作结果");
		Button btReturn = (Button)includeTitle.findViewById(R.id.util_head_btn_return);
		btReturn.setOnClickListener(this);
		
		AndroidUtils.setTextView(this, R.id.wg_feedbackresult_textview_result,"保存成功");

		AndroidUtils.setTextView(this, R.id.save_message,message);
		
		if(HhApplication.getInstance(this).getHhCart().getUser()==null){
			userId =-1l;
		}else{
			userId = HhApplication.getInstance(this).getHhCart().getUser().getId();
		}
		
		//String id = this.getIntent().getStringExtra("id");
		//Log.i("longhua","id = "+id);
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

	}
	
	@Override
	public void onClick(View view) {
		
		if(R.id.util_head_btn_return==view.getId()){
			/*
			Intent intent=new Intent(this, WgTaskActivity.class);
			Bundle bundle=new Bundle();
			bundle.putString("taskType", "检查任务");
			intent.putExtras(bundle);
			startActivity(intent);
			*/
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		}
		
		return;
	}
	
	
}
