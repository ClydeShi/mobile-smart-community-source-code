package com.zhyq;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.android.http.CircleNetworkImage;
import com.github.javiersantos.appupdater.AppUpdater;
import com.github.javiersantos.appupdater.enums.Display;
import com.github.javiersantos.appupdater.enums.UpdateFrom;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.tools.PictureFileUtils;
import com.util.common.StrEdit;
import com.zhyq.libs.AndroidUtils;
import com.zhyq.libs.PreferenceUtils;
import com.zhyq.libs.ProgersssDialog;
import com.zhyq.libs.ToastUtils;
import com.zhyq.libs.Tools;
import com.zhyq.libs.iosdialog.widget.MyAlertDialog;
import com.zhyq.libs.view.wheelview.WheelMain;
import com.zhyq.model.SysPassValue;
import com.zhyq.model.User;
import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.sendOrderToServerForValueYTask;
import com.zhyq.widget.LoadingRelativeLayout;

import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;


public class ZyMyPersonal extends Activity implements OnClickListener , EasyPermissions.PermissionCallbacks {

	public ProgersssDialog dialog;

	private Long userId = null;
	private int id = 0;
	private String name = "";
	private String userCode = "";
	private String userName = "";

	public static final int request_person_result = 8;

	static boolean active = false;
	private  String nickname=null;
	private  String realname=null;
	private  String xing=null;
	private  String ming=null;
	private  String carnumber=null;
	private  String usersex =null;
	private  String userbirthday=null;
	private  String useraddress=null;

	private  String province=null;
	private  String city=null;
	private  String country=null;

	private View  includeTitle ;
	private Button returnImage;
	private TextView  returnText;

	private PopupWindow popupWindow;
	private User user = new User();

	private float alpha = 1f;

	private CircleNetworkImage faceImage = null;

	private String faceImageNames = "hh_userface.png";
	private LoadingRelativeLayout loading = null;

	private Button bt_logout = null;

	WheelMain wheelMain;
	TextView txtbirthday;
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	File fileDir;
	String path = Environment.getExternalStorageDirectory()
			+ "/myfaceImg/";// 文件目录




	private String[] items = new String[] { "选择本地图片", "拍照" };
	/*头像名称*/
	private static final String faceimage_file="faceImage.jpg";
	/* 请求码*/
	private static final int IMAGE_REQUEST_CODE = 0;
	private static final int CAMERA_REQUEST_CODE = 1;
	private static final int RESULT_REQUEST_CODE = 2;

	public static final int WRITE_EXTERNAL_STORAGE = 100;
	private boolean isFirst = false;

	//权限参数 读写参数
	String[] params = {Manifest.permission.WRITE_EXTERNAL_STORAGE};

	private int requestCode;  //请求码
	private boolean firstopen = true;   //第一打开 用遮挡的面板 遮挡住

	private String usercode = "";
	private List<LocalMedia> selectList = new ArrayList<>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		showProgressDialog();

		this.setContentView(R.layout.activity_zy_my_personal);
		loading = LoadingRelativeLayout.getLoadingRelativeLayout(this);
		//getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		includeTitle = this.findViewById(R.id.zy_my_head);

		Button btnReturn = (Button)includeTitle.findViewById(R.id.util_my_head_btn_return);
		btnReturn.setOnClickListener(this);

		((TextView)includeTitle.findViewById(R.id.util_my_head_title)).setText("个人资料");

		View util_my_head_linearLayout3 = (View)includeTitle.findViewById(R.id.util_my_head_linearLayout2);
		util_my_head_linearLayout3.setVisibility(View.INVISIBLE);

		((Button)this.findViewById(R.id.btn_modify_personinfo_header)).setOnClickListener(this);
		((Button)this.findViewById(R.id.my_btn_nickname)).setOnClickListener(this);
		((Button)this.findViewById(R.id.my_btn_gender)).setOnClickListener(this);
		((Button)this.findViewById(R.id.my_btn_address)).setOnClickListener(this);
		((Button)this.findViewById(R.id.my_btn_password)).setOnClickListener(this);

		((Button)this.findViewById(R.id.wg_update_btn)).setOnClickListener(this);
		((Button)this.findViewById(R.id.wg_logout_btn)).setOnClickListener(this);

		txtbirthday = ((TextView)this.findViewById(R.id.text_personinfo_gender));

		faceImage  = (CircleNetworkImage)this.findViewById(R.id.zy_modify_personinfo_header);

		userId = PreferenceUtils.getLong(this,"userid",-1);
		usercode = PreferenceUtils.getString(this,"usercode","");

		if(userId>0){
			new Handler().postDelayed(new Runnable() {//延迟读取数据
				@Override
				public void run() {
					getDetailData();
				}
			}, 200);
		}else{
			ToastUtils.show(this, "数据错误，请重新登陆");
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (isFirst) {
			//因为要通过一个Fragment来弹出弹出框，所以activity这里的onResume执行了两次，这里进行判断
			isFirst = false;
			if (!EasyPermissions.hasPermissions(this, params)) {
				EasyPermissions.requestPermissions(this, "需要读写本地权限(1)", WRITE_EXTERNAL_STORAGE, params);
			}
		}

	}

	@Override
	public void onClick(View view) {

		if (R.id.util_my_head_btn_return == view.getId()) {
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		}

		if(R.id.btn_modify_personinfo_header==view.getId()) { // 修改头像

			bottomwindow(faceImage);
			new Thread(new Runnable(){
				@Override
				public void run() {
					while(alpha>0.5f){
						try {
							//4是根据弹出动画时间和减少的透明度计算
							Thread.sleep(4);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						Message msg =mHandler.obtainMessage();
						msg.what = 1;
						//每次减少0.01，精度越高，变暗的效果越流畅
						alpha-=0.01f;
						msg.obj =alpha ;
						mHandler.sendMessage(msg);
					}
				}

			}).start();

		}

		if(R.id.my_btn_nickname==view.getId()) { //修改昵称
			//Log.i("huahua","nickname");
			Intent intent=new Intent(this, ZyMyModifyName.class);
			Bundle bundle=new Bundle();

			TextView tvName = (TextView)this.findViewById(R.id.text_personinfo_nickname);
			String name = tvName.getText().toString();

			bundle.putString("type", "nickname");
			bundle.putString("name", name);
			bundle.putString("resultCode", "1");//返回参数
			intent.putExtras(bundle);

			requestCode = 8;//大参数
			startActivityForResult(intent, requestCode);//这里的requestCode要大于0，现在用的8是用来onActivityResult里判断由这里发出的新Activity，获取返回值的请求码
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
		}

		if(R.id.my_btn_address==view.getId()) { //修改地址
			//Log.i("luktel","nickname");
			Intent intent=new Intent(this, ZyMyModifyName.class);
			Bundle bundle=new Bundle();

			TextView tvName = (TextView)this.findViewById(R.id.text_personinfo_address);
			String name = tvName.getText().toString();

			bundle.putString("type", "address");
			bundle.putString("name", name);
			bundle.putString("resultCode", "2");//返回参数
			intent.putExtras(bundle);

			requestCode = 8;//大参数
			startActivityForResult(intent, requestCode);//这里的requestCode要大于0，现在用的8是用来onActivityResult里判断由这里发出的新Activity，获取返回值的请求码
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
		}

		if(R.id.my_btn_gender==view.getId()) { //修改性别

			TextView vSex = (TextView)this.findViewById(R.id.text_personinfo_gender);
			bottomSexwindow(vSex);
			new Thread(new Runnable(){
				@Override
				public void run() {
					while(alpha>0.5f){
						try {
							//4是根据弹出动画时间和减少的透明度计算
							Thread.sleep(4);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						Message msg =mHandler.obtainMessage();
						msg.what = 1;
						//每次减少0.01，精度越高，变暗的效果越流畅
						alpha-=0.01f;
						msg.obj =alpha ;
						mHandler.sendMessage(msg);
					}
				}

			}).start();

		}

		if(R.id.my_btn_password == view.getId()){

			//item_input_pwd
			View v1 = null;
			LayoutInflater mLayoutInflater=LayoutInflater.from(this);
			v1=mLayoutInflater.inflate(R.layout.item_input_pwd,null);

			final EditText et1 = (EditText)v1.findViewById(R.id.et_input_pwd);

			final MyAlertDialog dialog1 = new MyAlertDialog(ZyMyPersonal.this)
					.builder()
					.setTitle("验证原密码")
					.setMsg("为保障你的数据安全,修改密码前请填写原登入密码。")
					.setView(v1)
					.setNegativeButton("取消", new OnClickListener() {
						@Override
						public void onClick(View v) {

						}
					});
			dialog1.setPositiveButton("确认", new OnClickListener() {
				@Override
				public void onClick(View v) {

					String oldpwd = et1.getText().toString();

					if(oldpwd==null){
						oldpwd="";
					}

					if(oldpwd.equals("")){
						//Toast.makeText(getApplicationContext(),"请输入APP登入密码!", 1).show();
						return;
					}
					verifyLogPassword(oldpwd);
				}
			});
			dialog1.show();

		}

		if(R.id.wg_logout_btn==view.getId()){

			HhApplication.getInstance(view.getContext()).getHhCart().setUser(null);
			//PreferenceUtils.clearSharedPreference(getActivity(), "hh");  //清理之后，每次退出再重新进入再打开新手页面，也就是登录页面

			PreferenceUtils.setLong(this,"userid",-1);
			//PreferenceUtils.setString(this,"usercode","");
			PreferenceUtils.setString(this,"username","");
			PreferenceUtils.setString(this,"usertype","");
			PreferenceUtils.setString(this,"userrole","");

			AndroidUtils.start(this, WgLoginActivity.class);
			this.finish();
			((Activity) this).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			return;
		}

		if(R.id.wg_update_btn==view.getId()){
			//Toast.makeText(getActivity(), "wg_update_btn click", Toast.LENGTH_SHORT).show();
			//https://github.com/javiersantos/AppUpdater
			//AppUpdater appUpdater = new AppUpdater(getActivity());
			//appUpdater.start();

			/*
			1. app/build.gradle里面修改最新版本号，打包apk上传到服务器
			2. 服务器的update.xml里面修改最新版本号，如果有新版本，则可以下载
			 */
			new AppUpdater(this)
					//.setUpdateFrom(UpdateFrom.GITHUB)
					//.setGitHubUserAndRepo("javiersantos", "AppUpdater")
					.setUpdateFrom(UpdateFrom.XML)
					.setUpdateXML("http://139.9.1.194:82/zhyq/public/update.xml")
					.setDisplay(Display.DIALOG)
					.showAppUpdated(true)
					.setButtonDoNotShowAgain(null)
					.start();

			/*Intent itcontact = new Intent(view.getContext(),WgDownloadActivity.class);
			startActivity(itcontact);
			((Activity) getActivity()).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			return;*/

		}

	}

	public void showProgressDialog(){
		dialog = new ProgersssDialog(this);
		//dialog.show();
	}

	protected void getDetailData() {
		SysPassValue temp = new SysPassValue();
		temp.setFunctionName("getUserDetails");
		temp.setId(String.valueOf(id));
		temp.setUserId(String.valueOf(userId));

		new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
			@Override
			public void onPostExecute(String name, YGetTask<SysPassValue> task) {
				showDetailData(task.getValue());
				dialog.dismiss();
			}
		},temp).execute();
	}

	private void showDetailData(SysPassValue passValue) {
		if (passValue==null) {
			ToastUtils.show(this,"没有获取到信息");
			return;
		} else {
			 /*((TextView)includeMaster.findViewById(R.id.tv_masterdetail_head_image_shopname)).setText(masterviewdetail.getArg1());
			 AndroidUtils.setWebImageView(includeMaster, R.id.hh_masterdetail_head_image_shoplogimage, masterviewdetail.getArg2());
			 AndroidUtils.setWebImageView(includeMaster, R.id.hh_masterdetail_head_image_masterimage, masterviewdetail.getArg3());*/

			String sid =  passValue.getId();
			String arg1 =  passValue.getArg1();
			String arg2 =  passValue.getArg2();
			String arg3 =  passValue.getArg3();
			String arg4 =  passValue.getArg4();
			String arg5 =  passValue.getArg5();
			String arg6 =  passValue.getArg6();
			String arg7 =  passValue.getArg7();
			String arg8 =  passValue.getArg8();
			String arg9 =  passValue.getArg9();
			String arg10 =  passValue.getArg10();
			String arg11 =  passValue.getArg11();
			String arg12 =  passValue.getArg12();
			String arg13 =  passValue.getArg13();
			String arg14 =  passValue.getArg14();
			String arg15 =  passValue.getArg15();
			String arg16 =  passValue.getArg16();
			String arg17 =  passValue.getArg17();
			String arg18 =  passValue.getArg18();
			String arg19 =  passValue.getArg19();
			String arg20 =  passValue.getArg20();
			String arg38 =  passValue.getArg38();

			AndroidUtils.setTextView(this, R.id.text_personinfo_username,arg1);
			AndroidUtils.setTextView(this, R.id.text_personinfo_nickname,arg5);
			AndroidUtils.setTextView(this, R.id.text_personinfo_name,arg3);
			AndroidUtils.setTextView(this, R.id.text_personinfo_gender,arg4);
			AndroidUtils.setTextView(this, R.id.text_personinfo_mobile,arg8);
			AndroidUtils.setTextView(this, R.id.text_personinfo_address,arg19);

			View v1 = this.findViewById(R.id.zy_modify_personinfo_header);
			AndroidUtils.setWebImageCircleView(v1, R.id.zy_modify_personinfo_header, arg38);

			//((ImageView)includeTitle.findViewById(R.id.iv_hh_head_masterdetail_right_saygoods)).setBackgroundResource(R.drawable.praisetech);
		}
	}


	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
	}

	@Override
	public void onPermissionsGranted(int requestCode, List<String> perms) {

	}

	@Override
	public void onPermissionsDenied(int requestCode, List<String> perms) {
		if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
			//这个方法有个前提是，用户点击了“不再询问”后，才判断权限没有被获取到
			new AppSettingsDialog.Builder(this)
					.setRationale("没有该权限，此应用程序可能无法正常工作。打开应用设置界面以修改应用权限")
					.setTitle("必需权限")
					.build()
					.show();
		} else if (!EasyPermissions.hasPermissions(this, params)) {
			//这里响应的是除了AppSettingsDialog这个弹出框，剩下的两个弹出框被拒绝或者取消的效果
			finish();
		}

	}

	class FileupThread extends Thread{
		String ls_imagename;
		String ls_telephone;
		public FileupThread(String imagename,String telephone){
			ls_imagename = imagename;
			ls_telephone = telephone;
		}
		@Override
		public void run() {
			//这里写运行方法，可以直接调用参数 ;
			uploadFile(ls_imagename,ls_telephone) ;
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		//结果码不等于取消时候
		//if (resultCode != Activity.RESULT_OK)
		//	  return;



		//下面是针对 图片裁剪  IMAGE_REQUEST_CODE
		switch (requestCode) {

			case PictureConfig.CHOOSE_REQUEST:
				// 图片选择结果回调
				selectList = PictureSelector.obtainMultipleResult(data);
				// 例如 LocalMedia 里面返回三种path
				// 1.media.getPath(); 为原图path
				// 2.media.getCutPath();为裁剪后path，需判断media.isCut();是否为true
				// 3.media.getCompressPath();为压缩后path，需判断media.isCompressed();是否为true
				// 如果裁剪并压缩了，已取压缩路径为准，因为是先裁剪后压缩的
				for (LocalMedia media : selectList) {
					Log.i("luktel","图片-----》"+media.getPath());
					String ls_imagename = media.getPath();
					ls_imagename = media.getCutPath();
					String ls_face = usercode + userId + StrEdit.getDateForFileName() + ".jpg";
					Log.i("luktel","ls_imagename="+ls_imagename);
					Log.i("luktel","ls_face="+ls_face);
					new FileupThread(ls_imagename,ls_face).start();
				}
				//adapter.setList(selectList);
				//adapter.notifyDataSetChanged();
				break;

		}

		//Log.i("luktel","requestCode="+String.valueOf(requestCode)+", resultCode="+String.valueOf(resultCode));
		if(requestCode == 8) {

			if (resultCode == 1){//setResult(resultCode, intent);返回的值
				if (data != null) {
					Bundle b=data.getExtras();
					String newName=b.getString("newName");
					//Log.i("luktel","nickName="+newName);
					AndroidUtils.setTextView(this, R.id.text_personinfo_nickname,newName);
				}
			}

			if (resultCode == 2){
				if (data != null) {
					Bundle b=data.getExtras();
					String newName=b.getString("newName");
					AndroidUtils.setTextView(this, R.id.text_personinfo_address,newName);
				}
			}

			if (resultCode == 3){
				if (data != null) {
					ToastUtils.show(this, "修改成功！");
				}
			}

		}

		super.onActivityResult(requestCode, resultCode, data);
	}

	private void uploadFile(String upfile,String newName){
		//showDialog("开始上传图片");
		String end = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		String actionUrl = "http://139.9.1.194:82/zhyq/clientupfile";
		//String actionUrl = "http://112.74.57.196/hgc/clientupfile";

		try{
			//showDialog("11-上传成功");
			URL url =new URL(actionUrl);
			HttpURLConnection con=(HttpURLConnection)url.openConnection();
            /* 允许Input、Output，不使用Cache */
			con.setDoInput(true);
			con.setDoOutput(true);
			con.setUseCaches(false);
            /* 设置传送的method=POST */
			con.setRequestMethod("POST");
            /* setRequestProperty */
			con.setRequestProperty("Connection", "Keep-Alive");
			con.setRequestProperty("Charset", "UTF-8");
			con.setRequestProperty("Content-Type",
					"multipart/form-data;boundary="+boundary);
            /* 设置DataOutputStream */
			DataOutputStream ds =           new DataOutputStream(con.getOutputStream());
			ds.writeBytes(twoHyphens + boundary + end);
			ds.writeBytes("Content-Disposition: form-data; " +
					"name=\"file1\";filename=\"" +
					newName +"\"" + end);
			ds.writeBytes(end);

            /* 取得文件的FileInputStream */
			FileInputStream fStream = new FileInputStream(upfile);
            /* 设置每次写入1024bytes */
			int bufferSize = 1024;
			byte[] buffer = new byte[bufferSize];

			int length = -1;
            /* 从文件读取数据至缓冲区 */
			while((length = fStream.read(buffer)) != -1){
              /* 将资料写入DataOutputStream中 */
				ds.write(buffer, 0, length);
			}
			ds.writeBytes(end);
			ds.writeBytes(twoHyphens + boundary + twoHyphens + end);

            /* close streams */
			fStream.close();
			ds.flush();

            /* 取得Response内容 */
			InputStream is = con.getInputStream();
			int ch;
			StringBuffer b =new StringBuffer();
			while( ( ch = is.read() ) != -1 ){
				b.append( (char)ch );
			}
            /* 将Response显示于Dialog */
			//showDialog("上传成功"+b.toString().trim());
            /* 关闭DataOutputStream */
			ds.close();
		}catch(Exception e){
			showDialog("上传失败"+e);
		}

		PictureFileUtils.deleteCacheDirFile(ZyMyPersonal.this);//清理缓存

		//更新数据库
		//Log.i("huahua","begin updage");
		//user.setnetpic(newName) ;
		//user.setUserModifyColumnType("0");

		SysPassValue temp = new SysPassValue();//buildingId buildingName address degree date1 title content
		temp.setFunctionName("modifyPhoto");
		temp.setUserId(String.valueOf(userId));
		temp.setId(String.valueOf(id));
		temp.setArg1("upload/picture/" + newName);
		temp.setArg2(usercode);

		new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
			@Override
			public void onPostExecute(String name, YGetTask<SysPassValue> task) {
				loading.hideLoading();
				userModifyData(task.getValue(),"0");
			}
		},temp).execute();

	}

	private void showDialog(String mess){
		Looper.prepare();
		Toast.makeText(getApplicationContext(), "test", Toast.LENGTH_LONG).show();
		new AlertDialog.Builder(ZyMyPersonal.this).setTitle("Message")
				.setMessage(mess)
				.setNegativeButton("确定",new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface dialog, int which)
					{
					}
				})
				.show();
		Looper.loop();
	}

	public boolean fileIsExists(String filename){
		try{
			File f=new File(filename);
			if(!f.exists()){
				return false;
			}
		}catch (Exception e) {
			// TODO: handle exception
			return false;
		}
		return true;
	}

	//异步下载网络图片
	/**
	 * 使用异步任务的规则：
	 * 1、申明的类继承AsyncTask 标注三个参数的类型
	 * 2、第一个参数表示要执行的任务，通常是网络的路径；
	 * 第二个参数表示进度的刻度，
	 * 第三个参数表示任务执行的返回结果
	 *
	 * @author liende
	 *
	 */
	public class MYTask extends AsyncTask<String, Void, Bitmap> {
		/**
		 * 表示任务执行之前的操作
		 */
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			//dialog.show();
		}

		/**
		 * 主要是完成耗时的操作
		 */
		@Override
		protected Bitmap doInBackground(String... arg0) {

			// TODO Auto-generated method stub
			// 使用网络连接类HttpClient类王城对网络数据的提取
			HttpClient httpClient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(arg0[0]);
			//Log.i("huahua","cur link is1 = "+arg0);
			//Log.i("huahua","cur link is = "+arg0[0]);

			Bitmap bitmap = null;
			try {
				HttpResponse httpResponse = httpClient.execute(httpGet);
				if (httpResponse.getStatusLine().getStatusCode() == 200) {
					HttpEntity httpEntity = httpResponse.getEntity();
					byte[] data = EntityUtils.toByteArray(httpEntity);
					bitmap = BitmapFactory
							.decodeByteArray(data, 0, data.length);
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			return bitmap;
		}

		/**
		 * 主要是更新UI的操作
		 */
		@Override
		protected void onPostExecute(Bitmap result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(result==null){
				Log.i("huahua","cur bitmap is null");
				return;
			}

			faceImage.setImageBitmap(result);
			//dialog.dismiss();
		}

	}

	void bottomwindow(CircleNetworkImage view) {
		if (popupWindow != null && popupWindow.isShowing()) {
			return;
		}
		LinearLayout layout = (LinearLayout) getLayoutInflater().inflate(R.layout.activity_photo_popupwindow1, null);
		popupWindow = new PopupWindow(layout,
				ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.WRAP_CONTENT);
		//点击空白处时，隐藏掉pop窗口
		popupWindow.setFocusable(true);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());
		//添加弹出、弹入的动画
		popupWindow.setAnimationStyle(R.style.Popupwindow);
		int[] location = new int[2];
		view.getLocationOnScreen(location);
		popupWindow.showAtLocation(view, Gravity.LEFT | Gravity.BOTTOM, 0, -location[1]);
		//添加按键事件监听
		setButtonListeners(layout);
		//添加pop窗口关闭事件，主要是实现关闭时改变背景的透明度
		popupWindow.setOnDismissListener(new poponDismissListener());
		backgroundAlpha(1f);
	}

	//针对性别来修改的方式 加载POP窗口
	void bottomSexwindow(TextView view) {
		if (popupWindow != null && popupWindow.isShowing()) {
			return;
		}

		//修改的话，这个地方要修改 *****************
		LinearLayout layout = (LinearLayout) getLayoutInflater().inflate(R.layout.activity_sex_popupwindow, null);
		popupWindow = new PopupWindow(layout,
				ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.WRAP_CONTENT);
		//点击空白处时，隐藏掉pop窗口
		popupWindow.setFocusable(true);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());
		//添加弹出、弹入的动画
		popupWindow.setAnimationStyle(R.style.Popupwindow);
		int[] location = new int[2];
		view.getLocationOnScreen(location);
		popupWindow.showAtLocation(view, Gravity.LEFT | Gravity.BOTTOM, 0, -location[1]);

		//修改的话，这个地方要修改 *****************
		//添加按键事件监听
		setSexButtonListeners(layout);
		//添加pop窗口关闭事件，主要是实现关闭时改变背景的透明度
		popupWindow.setOnDismissListener(new poponDismissListener());
		backgroundAlpha(1f);
	}

	private void setButtonListeners(LinearLayout layout) {
		Button camera = (Button) layout.findViewById(R.id.camera);
		Button gallery = (Button) layout.findViewById(R.id.gallery);
		Button savepicture = (Button) layout.findViewById(R.id.savepicture);
		Button cancel = (Button) layout.findViewById(R.id.cancel);

		camera.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (popupWindow != null && popupWindow.isShowing()) {
					//在此处添加你的按键处理 xxx
					popupWindow.dismiss();
					backgroundAlpha(1f);

					PictureSelector.create(ZyMyPersonal.this)
							.openCamera(PictureMimeType.ofImage())
							.previewImage(true)
							.enableCrop(true)
							.compress(true)
							.compressMode(PictureConfig.SYSTEM_COMPRESS_MODE)
							.glideOverride(300,300)
							.withAspectRatio(1,1)
							.hideBottomControls(true)
							.showCropFrame(true)
							.cropCompressQuality(90)
							.compressMaxKB(200)
							.compressWH(300,300)
							.cropWH(300,300)
							.forResult(PictureConfig.CHOOSE_REQUEST);

					return;

				}
			}
		});
		gallery.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (popupWindow != null && popupWindow.isShowing()) {
					//在此处添加你的按键处理 xxx
					//选照相片
					popupWindow.dismiss();
					backgroundAlpha(1f);

					PictureSelector.create(ZyMyPersonal.this)
							.openGallery(PictureMimeType.ofImage())
							.maxSelectNum(1)
							.minSelectNum(1)
							.imageSpanCount(4)
							.selectionMode(PictureConfig.SINGLE)
							.isCamera(true)
							.previewImage(true)
							.enableCrop(true)
							.compress(true)
							.compressMode(PictureConfig.SYSTEM_COMPRESS_MODE)
							.glideOverride(300,300)
							.withAspectRatio(1,1)
							.hideBottomControls(true)
							.showCropFrame(true)
							.cropCompressQuality(90)
							.compressMaxKB(200)
							.compressWH(300,300)
							.cropWH(300,300)
							.forResult(PictureConfig.CHOOSE_REQUEST);

					return;

				}
			}
		});
		savepicture.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (popupWindow != null && popupWindow.isShowing()) {
					//在此处添加你的按键处理 xxx
					popupWindow.dismiss();
				}
			}
		});
		cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (popupWindow != null && popupWindow.isShowing()) {
					popupWindow.dismiss();
				}
			}
		});
	}

	//相应选择性别的窗口  的CLICK事件
	//            0  修改头像
	//            1 修改昵称
	//            2 修改性别
	//            3 修改生日
	//            4 修改收货日期
	private void setSexButtonListeners(LinearLayout layout) {

		Button bt_boy = (Button) layout.findViewById(R.id.btn_sex_boys);
		Button bt_girl = (Button) layout.findViewById(R.id.btn_sex_girl);
		Button bt_cancel = (Button) layout.findViewById(R.id.btn_sex_cancel);

		bt_boy.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (popupWindow != null && popupWindow.isShowing()) {
					//在此处添加你的按键处理 xxx
					popupWindow.dismiss();
					backgroundAlpha(1f);
					//选择了男生
					loading.showLoading();

					SysPassValue temp = new SysPassValue();//buildingId buildingName address degree date1 title content
					temp.setFunctionName("modifyGender");
					temp.setUserId(String.valueOf(userId));
					temp.setId(String.valueOf(id));
					temp.setArg1("男");

					new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
						@Override
						public void onPostExecute(String name, YGetTask<SysPassValue> task) {
							loading.hideLoading();
							userModifyData(task.getValue(),"2");
						}
					},temp).execute();

					return;
				}

			}
		});
		bt_girl.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (popupWindow != null && popupWindow.isShowing()) {
					//在此处添加你的按键处理 xxx
					popupWindow.dismiss();
					backgroundAlpha(1f);
					//选择了男生
					loading.showLoading();
					SysPassValue temp = new SysPassValue();
					temp.setFunctionName("modifyGender");
					temp.setUserId(String.valueOf(userId));
					temp.setId(String.valueOf(id));
					temp.setArg1("女");

					new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
						@Override
						public void onPostExecute(String name, YGetTask<SysPassValue> task) {
							loading.hideLoading();
							userModifyData(task.getValue(),"2");
						}
					},temp).execute();

					return;

				}
			}
		});
		bt_cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (popupWindow != null && popupWindow.isShowing()) {
					popupWindow.dismiss();
				}
			}
		});
	}

	private void userModifyData(SysPassValue user, String modifyType) {
		if (user==null) {
			ToastUtils.show(this, "修改失败");
		} else {

			if(modifyType.equals("2")){
				String sex = user.getArg1();
				AndroidUtils.setTextView(this, R.id.text_personinfo_gender,sex);//修改性别
			}

			if(modifyType.equals("0")){
				String returnResult = user.getReturnResult();
				if(returnResult.equals("1")){
					String pic = user.getArg1();
					//ToastUtils.show(this, "修改成功！"+pic);
					View v1 = this.findViewById(R.id.zy_modify_personinfo_header);
					AndroidUtils.setWebImageCircleView(v1, R.id.zy_modify_personinfo_header, pic);
					//Log.i("luktel","reload image "+pic);
				}else{
					ToastUtils.show(this, "修改失败！");
					return;
				}

			}

		}
	}

	/**
	 * 设置添加屏幕的背景透明度
	 * @param bgAlpha
	 */
	public void backgroundAlpha(float bgAlpha)
	{
		WindowManager.LayoutParams lp = getWindow().getAttributes();
		lp.alpha = bgAlpha; //0.0-1.0
		getWindow().setAttributes(lp);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

       /*
        *    WindowManager.LayoutParams lp = activity.getWindow().getAttributes();  
        lp.alpha = bgAlpha;  
        if (bgAlpha == 1) {  
            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);//不移除该Flag的话,在有视频的页面上的视频会出现黑屏的bug  
        } else {  
            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);//此行代码主要是解决在华为手机上半透明效果无效的bug  
        }  
        activity.getWindow().setAttributes(lp);
        */


	}


	/**
	 * 返回或者点击空白位置的时候将背景透明度改回来
	 */
	class poponDismissListener implements PopupWindow.OnDismissListener{

		@Override
		public void onDismiss() {
			// TODO Auto-generated method stub
			new Thread(new Runnable(){
				@Override
				public void run() {
					//此处while的条件alpha不能<= 否则会出现黑屏
					while(alpha<1f){
						try {
							Thread.sleep(4);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						Log.d("HeadPortrait","alpha:"+alpha);
						Message msg =mHandler.obtainMessage();
						msg.what = 1;
						alpha+=0.01f;
						msg.obj =alpha ;
						mHandler.sendMessage(msg);
					}
				}

			}).start();
		}

	}

	Handler mHandler = new Handler(){
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what){
				case 1:
					backgroundAlpha(alpha);
					//backgroundAlpha((float)msg.obj);
					break;
			}
		}
	};

	private void verifyLogPassword(String password){
		loading.showLoading();
		SysPassValue temp = new SysPassValue();
		temp.setFunctionName("verifyPassword");
		temp.setUserId(String.valueOf(userId));
		temp.setArg1(password);

		new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
			@Override
			public void onPostExecute(String name, YGetTask<SysPassValue> task) {
				loading.hideLoading();
				updatePassword(task.getValue());
			}
		},temp).execute();
	}

	private void updatePassword(SysPassValue dbResult){
		if (dbResult==null) {
			ToastUtils.show(this, "数据错误！");
			return;
		} else {

			String returnResult = dbResult.getReturnResult();
			if(returnResult.equals("1")){

				Intent intent=new Intent(this, ZyMyModifyPassword.class);
				Bundle bundle=new Bundle();

				bundle.putString("type", "password");
				bundle.putString("resultCode", "3");//返回参数
				intent.putExtras(bundle);

				requestCode = 8;//大参数
				startActivityForResult(intent, requestCode);//这里的requestCode要大于0，现在用的8是用来onActivityResult里判断由这里发出的新Activity，获取返回值的请求码
				overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);

			}else{

				ToastUtils.show(this, "原密码错误！");
				return;

			}

		}

	}


}
