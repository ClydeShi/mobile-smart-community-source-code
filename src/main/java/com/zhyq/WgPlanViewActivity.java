package com.zhyq;


import android.content.Intent;
import android.os.Bundle;

import android.support.v4.app.FragmentActivity;

import android.view.View;
import android.view.View.OnClickListener; 
import android.widget.Button;
import android.widget.ImageView; 
import android.widget.TextView;


import com.zhyq.libs.AndroidUtils;
import com.zhyq.libs.PreferenceUtils;
 
import com.zhyq.libs.ToastUtils;
import com.zhyq.libs.iosdialog.widget.MyAlertDialog;
import com.zhyq.model.CommonData;
import com.zhyq.model.SysPassValue;
import com.zhyq.model.User;
 
import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.GetTaskDetailYTask;
import com.zhyq.task.impl.sendOrderToServerForValueYTask;
import com.zhyq.widget.LoadingRelativeLayout;



public class WgPlanViewActivity extends FragmentActivity implements OnClickListener { 
	
	private LoadingRelativeLayout loading = null;
	private View includeTitle;
	private View includeMaster;
	private ImageView returnImage;
	private TextView returnText;
	
	//private FragmentPagerAdapter adapter;
	
	
	private String curTitle = "全部";
	private int cur_position = 0;   // 当前page索引（切换之前）
	private String planId = "";
	private String planStatus = "";
	private Long userId = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_wg_plan_detail);
		loading=LoadingRelativeLayout.getLoadingRelativeLayout(this);
		loading.showLoading();
		includeTitle = this.findViewById(R.id.wg_plandetail_head);
		((TextView)includeTitle.findViewById(R.id.wg_head_title)).setText("查看计划");
		Button btreturn = (Button)includeTitle.findViewById(R.id.wg_head_btn_return);
		btreturn.setOnClickListener(this);
		if(HhApplication.getInstance(this).getHhCart().getUser()==null){
			userId =-1l;
		}else{
			userId = HhApplication.getInstance(this).getHhCart().getUser().getId();
		}
		planId = this.getIntent().getStringExtra("planId");
		String planName = this.getIntent().getStringExtra("planName");
		planStatus = this.getIntent().getStringExtra("planStatus");
		
		this.findViewById(R.id.wg_util_head_btn_del).setOnClickListener(this);
		this.findViewById(R.id.wg_util_head_btn_edit).setOnClickListener(this);
		
		//ImageView imgdel = (ImageView)this.findViewById(R.id.wg_util_head_img_del);
		//ImageView imgedit = (ImageView)this.findViewById(R.id.wg_util_head_img_edit);
		TextView deltext = (TextView)this.findViewById(R.id.wg_util_head_text_del);
		TextView edittext = (TextView)this.findViewById(R.id.wg_util_head_text_edit);
		Button delbutton = (Button)this.findViewById(R.id.wg_util_head_btn_del);
		Button editbutton = (Button)this.findViewById(R.id.wg_util_head_btn_edit);
		if(planStatus.equals("2")){
			deltext.setVisibility(View.VISIBLE);
			edittext.setVisibility(View.VISIBLE);
			delbutton.setVisibility(View.VISIBLE);
			editbutton.setVisibility(View.VISIBLE);
		}else{
			deltext.setVisibility(View.INVISIBLE);
			edittext.setVisibility(View.INVISIBLE);
			delbutton.setVisibility(View.INVISIBLE);
			editbutton.setVisibility(View.INVISIBLE);
		}
		
		//AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout2_textview,taskName+taskId);		
		
		//输出日志，设置log tag，手机设为调试模式，在logcat里面就可以看到了
		//Log.i("longhua","goto first1111");
		
		/*
		mastername = this.getIntent().getStringExtra("mastername");
		
		//我是否点赞
		if(ihasdianzhan.equals("Y")){
			//设置手为绿色
			((ImageView)includeTitle.findViewById(R.id.iv_hh_head_masterdetail_right_saygoods)).setBackgroundResource(R.drawable.praisetech); 
		}else{
			
			((ImageView)includeTitle.findViewById(R.id.iv_hh_head_masterdetail_right_saygoods)).setBackgroundResource(R.drawable.praise_black_empty);
		}
	    ((TextView)includeMaster.findViewById(R.id.tv_masterdetail_head_image_mastername)).setText(mastername);
	    */
		
		/*
		 *
		 1. 调用com.unitesoft.task.impl.GetTaskDetailYTask，并传入taskId参数，GetTaskDetailYTask会调用com.unitesoft.data.DataAPI.getTaskDetail()
		 2. DataAPI的方法getTaskDetail会调用DemoDataInterface的方法getTaskDetail返回结果，因为DemoDataInterface是实现DataInterface接口，所以DataInterface也要定义方法getTaskDetail
		 3. 如果是调用远程数据，上面的DataAPI的方法getTaskDetail会调用ClientDataInterface，ClientDataInterface会带参数连接服务器，com.huahua.server.servelt.ClientApiServlet，ClientApiServlet通过判断参数，传入对象，分别调用com.huahua.server.data.DemoDataInterface里的方法，并返回对象
		 4. 使用SysPassValue来作为数据对象传递
		 */
		 
		 /*
		  * 
		  * GetTaskDetailYTask继承YGetTask，调用接口的时候执行构造函数GetTaskDetailYTask，传入YTaskListener和taskId参数
		  * 父类YGetTask会执行onExecute方法，方法里面有_get()，会通过DataAPI.getTaskDetail();返回结果
		  */
		
	    new GetTaskDetailYTask(new AbstractYGetTaskListener<CommonData>() {
			@Override
			public void onPostExecute(String name, YGetTask<CommonData> task) {//onPostExecute是AbstractYGetTaskListener里的方法
				
				getData(task.getValue());//task.getValue()是CommonData定义的对象，这个是返回的结果
				loading.hideLoading();
			}
		},planId,String.valueOf(this.userId)).execute();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
	}
	
	@Override
	public void onClick(View view) {
		
		if(R.id.wg_head_btn_return==view.getId()){
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		}
		
		if(R.id.wg_util_head_btn_edit==view.getId()){
			
			Intent intent=new Intent(this, WgPlanAdd.class);
			Bundle bundle=new Bundle();
			
			Long userid = null;
			User curuser = new User();
			curuser = HhApplication.getInstance(this).getHhCart().getUser();
			if(curuser==null){
				userid =-1l;
			}else{
				userid = HhApplication.getInstance(this).getHhCart().getUser().getId();
			}
			
			//Long userid = HhApplication.getInstance(this).getHhCart().getUser().getId();
			Long userId = PreferenceUtils.getLong(this, "userid", -1);
			String usercode = PreferenceUtils.getString(this,"usercode","");
			
			bundle.putString("planId", planId);
			intent.putExtras(bundle);
			startActivity(intent);
			
			this.finish();
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			return;
		}
		
		if(R.id.wg_util_head_btn_del==view.getId()){
			
			final MyAlertDialog dialog1 = new MyAlertDialog(WgPlanViewActivity.this)
			.builder()
			.setTitle("删除")
			.setMsg("删除当前计划，确定吗?")
			.setNegativeButton("取消", new OnClickListener() {
				@Override
				public void onClick(View v) {
					
				}
			});
			dialog1.setPositiveButton("确认", new OnClickListener() {
				@Override
				public void onClick(View v) {
					delPlan();
				}
			});
			dialog1.show();
		}
		
		return;
	}
	
	private void getData(CommonData taskdetail) {
		if (taskdetail==null) {
			ToastUtils.show(this,"没有获取到信息");
			return;
		} else {
			 /*((TextView)includeMaster.findViewById(R.id.tv_masterdetail_head_image_shopname)).setText(masterviewdetail.getArg1());  
			 AndroidUtils.setWebImageView(includeMaster, R.id.hh_masterdetail_head_image_shoplogimage, masterviewdetail.getArg2()); 
			 AndroidUtils.setWebImageView(includeMaster, R.id.hh_masterdetail_head_image_masterimage, masterviewdetail.getArg3());*/
			 
			String varValue1 = taskdetail.getVarValue1();
			String varValue2 = taskdetail.getVarValue2();
			String varValue3 = taskdetail.getVarValue3();
			String varValue4 = taskdetail.getVarValue4();
			String varValue5 = taskdetail.getVarValue5();
			String varValue6 = taskdetail.getVarValue6();
			String varValue7 = taskdetail.getVarValue7();
			String varValue8 = taskdetail.getVarValue8();
			String varValue9 = taskdetail.getVarValue9();
			String varValue10 = taskdetail.getVarValue10();
			
			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout2_textview,varValue2);
			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout3_textview,varValue3);
			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout4_textview,varValue4);
			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout5_textview,varValue5);
			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout6_textview,varValue6);
			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout7_textview,varValue7);
			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout8_textview,varValue8);
			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout9_textview,varValue9);
			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout10_textview,varValue10);
			
			//((ImageView)includeTitle.findViewById(R.id.iv_hh_head_masterdetail_right_saygoods)).setBackgroundResource(R.drawable.praisetech);
		}
	}
	
	public void delPlan(){
		
		SysPassValue temp = new SysPassValue();
		temp.setFunctionName("delPlan");
		temp.setUserId(String.valueOf(userId));
		temp.setId(this.planId);
		
		new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
			@Override
			public void onPostExecute(String name, YGetTask<SysPassValue> task) {
				//loading.hideLoading();
				getResult(task.getValue());
			}
		},temp).execute();
	}
	
	private void getResult(SysPassValue passValue) {
		if (passValue==null) {
			ToastUtils.show(this, "没有获取到数据！");
		} else {
			String returnResult = passValue.getReturnResult();
			String taskId = passValue.getId();
			if(returnResult.equals("1")){
				
				this.finish();
				overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
				return;
			}else{
				ToastUtils.show(this, "删除失败！");
				loading.hideLoading();
			}
		}
	}
		

}
