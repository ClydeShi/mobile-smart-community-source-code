package com.zhyq;


import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.util.TypedValue;
import android.view.View; 
import android.view.View.OnClickListener; 
import android.widget.Button;
import android.widget.TextView;
 

import com.zhyq.adapter.WgTaskFeedbackDetailAdapter;

import com.zhyq.libs.ToastUtils;
import com.zhyq.libs.iosdialog.widget.MyAlertDialog;
import com.zhyq.model.CommonData;
import com.zhyq.model.SysPassValue;
import com.zhyq.model.User;
 
import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.GetTaskDetailYTask;
import com.zhyq.task.impl.sendOrderToServerForListYTask;
import com.zhyq.task.impl.sendOrderToServerForValueYTask;
import com.zhyq.widget.LoadingRelativeLayout;

/*
 * 使用SwipeRefreshLayout和RecyclerView加载详情，WgTaskFeedbackDetailAdapter中加载头部，列表和底部面板，实现下拉刷新，没有实现上拉刷新
 */

public class WgTaskViewActivity extends FragmentActivity implements OnClickListener {
	
	private LoadingRelativeLayout loading = null;
	private View includeTitle;
	private SwipeRefreshLayout demo_swiperefreshlayout;
	private String taskId = "";
	private String taskName = "";
	private String className = "";
	private Long userId = null;
	
	private RecyclerView mRecyclerView;
    private WgTaskFeedbackDetailAdapter adapter;
    LinearLayoutManager layoutManager;
    private CommonData dataDetail = new CommonData();//数据
    //private List<CommonData> dataDetail = new ArrayList<CommonData>();
    private List<SysPassValue> dataList = new ArrayList<SysPassValue>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_wg_task_detail_feedback);
		//loading=LoadingRelativeLayout.getLoadingRelativeLayout(this);
		//loading.showLoading();
		includeTitle = this.findViewById(R.id.wg_taskdetail_head);
		((TextView)includeTitle.findViewById(R.id.wg_head_title)).setText("查看任务");
		Button btreturn = (Button)includeTitle.findViewById(R.id.wg_head_btn_return);
		btreturn.setOnClickListener(this);
		
		TextView addtext = (TextView)includeTitle.findViewById(R.id.wg_util_head_text_edit);
		((TextView)includeTitle.findViewById(R.id.wg_util_head_text_edit)).setText("反馈");
		Button btnadd = (Button)includeTitle.findViewById(R.id.wg_util_head_btn_edit);
		btnadd.setOnClickListener(this);
		
		TextView deltext = (TextView)includeTitle.findViewById(R.id.wg_util_head_text_del);
		deltext.setText("完成");
		Button delbutton = (Button)includeTitle.findViewById(R.id.wg_util_head_btn_del);
		delbutton.setOnClickListener(this);
		
		if(HhApplication.getInstance(this).getHhCart().getUser()==null){
			userId =-1l;
		}else{
			userId = HhApplication.getInstance(this).getHhCart().getUser().getId();
		}
		
		taskId = this.getIntent().getStringExtra("taskId");
		className = this.getIntent().getStringExtra("className");
		String taskName = this.getIntent().getStringExtra("taskName");
		//AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout2_textview,taskName);
		
		if(className.equals("未执行")){
			((TextView)includeTitle.findViewById(R.id.wg_util_head_text_edit)).setText("执行");
			((TextView)includeTitle.findViewById(R.id.wg_util_head_text_del)).setText("");
			deltext.setVisibility(View.INVISIBLE);
			delbutton.setVisibility(View.INVISIBLE);
		}
		
		if(className.equals("已完成")){
			((TextView)includeTitle.findViewById(R.id.wg_util_head_text_edit)).setText("");
			((TextView)includeTitle.findViewById(R.id.wg_util_head_text_del)).setText("");
			deltext.setVisibility(View.INVISIBLE);
			delbutton.setVisibility(View.INVISIBLE);
			addtext.setVisibility(View.INVISIBLE);
			btnadd.setVisibility(View.INVISIBLE);
		}
		
		demo_swiperefreshlayout=(SwipeRefreshLayout)this.findViewById(R.id.demo_swiperefreshlayout);
		mRecyclerView=(RecyclerView)findViewById(R.id.rv_feedbacklist);
		
		//设置刷新时动画的颜色，可以设置4个
        demo_swiperefreshlayout.setProgressBackgroundColorSchemeResource(android.R.color.white);
		//demo_swiperefreshlayout.setProgressBackgroundColor(android.R.color.white);
        demo_swiperefreshlayout.setColorSchemeResources(android.R.color.holo_blue_light,
                android.R.color.holo_red_light, android.R.color.holo_orange_light,
                android.R.color.holo_green_light);
        demo_swiperefreshlayout.setProgressViewOffset(false, 0, (int) TypedValue
                .applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, getResources()
                        .getDisplayMetrics()));
        
        layoutManager=new LinearLayoutManager(this);//List布局
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager);
        adapter=new WgTaskFeedbackDetailAdapter(this, dataDetail, dataList);
        if(className.equals("执行中")){//是否可以点开修改
        	adapter.setOnClick1(this);
        }
        mRecyclerView.setAdapter(adapter);
        
        demo_swiperefreshlayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
            	//Log.d("longhua", "invoke onRefresh...");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
            			
                    	getDetailData();
                	    getListData();
                        demo_swiperefreshlayout.setRefreshing(false);
                    }
                }, 100);
            }
        });
        
        getDetailData();
	    getListData();
        
	}
	
	private void getDetailData(){
		new GetTaskDetailYTask(new AbstractYGetTaskListener<CommonData>() {
			@Override
			public void onPostExecute(String name, YGetTask<CommonData> task) {//onPostExecute是AbstractYGetTaskListener里的方法
				
				getData(task.getValue());//task.getValue()是SysPassValue定义的List，这个是返回的结果
				//loading.hideLoading();
			}
		},taskId,String.valueOf(this.userId)).execute();
	}
	
	private void getData(CommonData taskdetail) {
		if (taskdetail==null) {
			ToastUtils.show(this,"没有获取到信息");
			dataDetail = null;
			adapter.notifyDataSetChanged();
			return;
		} else {
			
			//dataDetail = taskdetail;
			adapter.data = taskdetail;
			adapter.notifyDataSetChanged();
			
		}
	}
	
	private void getListData(){
		Long userid = null;
		User curuser = new User();
		curuser = HhApplication.getInstance(this).getHhCart().getUser();
		if(curuser==null){
			userid =-1l;
		}else{
			userid = HhApplication.getInstance(this).getHhCart().getUser().getId();
		}
		
		SysPassValue temp = new SysPassValue();
		temp.setFunctionName("getTaskFeedbackList");//getContactList
		temp.setPageNumber("1");
		temp.setId(taskId);
		temp.setArg1(String.valueOf(userid));
		
		new sendOrderToServerForListYTask(new AbstractYGetTaskListener<List<SysPassValue>>() {
			@Override
			public void onPostExecute(String name, YGetTask<List<SysPassValue>> task) {
				
				showData(task.getValue());
			}
		},temp).execute();
    }
	
	private void showData(List<SysPassValue> list){
		
		if (list==null){
			this.dataList.clear();
			adapter.notifyDataSetChanged();
			return;
		}
		
		this.dataList.clear();//清除原来加载的记录
		dataList.addAll(list);
		adapter.notifyDataSetChanged();
		
	}
	
	//onResume是在启动activity启动之后才能执行的，也就是恢复执行。程序正常启动：onCreate()->onStart()->onResume();
	@Override
	protected void onResume() {//该函数，如果在打开了其他窗口，再关闭，这个函数也会执行
		// TODO Auto-generated method stub
		super.onResume();
		//Log.i("longhua","taskId = "+taskId);
		getDetailData();//返回之后更新数据
	    getListData();
	}
	
	@Override
	public void onClick(View view) {
		
		if(R.id.wg_head_btn_return==view.getId()){
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		}
		
		if(R.id.wg_util_head_btn_edit==view.getId()){
			
			if(className.equals("未执行")){
				final MyAlertDialog dialog1 = new MyAlertDialog(WgTaskViewActivity.this)
				.builder()
				.setTitle("执行")
				.setMsg("执行当前任务，确定吗?")
				.setNegativeButton("取消", new OnClickListener() {
					@Override
					public void onClick(View v) {
						
					}
				});
				dialog1.setPositiveButton("确认", new OnClickListener() {
					@Override
					public void onClick(View v) {
						doTask();
					}
				});
				dialog1.show();
				
			}else if(className.equals("执行中")){
				Intent intent=new Intent(this, WgTaskFeedback.class);
				Bundle bundle=new Bundle();
				Long userid = null;
				User curuser = new User();
				curuser = HhApplication.getInstance(this).getHhCart().getUser();
				if(curuser==null){
					userid =-1l;
				}else{
					userid = HhApplication.getInstance(this).getHhCart().getUser().getId();
				}
				
				bundle.putString("userId", String.valueOf(userid));
				bundle.putString("taskId", String.valueOf(taskId));
				bundle.putString("taskName", taskName);
				intent.putExtras(bundle);
				/*startActivity(intent);
				this.finish();
				overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);*/
				this.startActivity(intent);
				overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				return;
			}else if(className.equals("已完成")){
				
			}
			
		}
		
		if(R.id.wg_util_head_btn_del==view.getId()){
			
			if(className.equals("执行中")){
				final MyAlertDialog dialog1 = new MyAlertDialog(WgTaskViewActivity.this)
				.builder()
				.setTitle("完成任务")
				.setMsg("完成当前任务后，其他人员无法再进行反馈，确定吗?")
				.setNegativeButton("取消", new OnClickListener() {
					@Override
					public void onClick(View v) {
						
					}
				});
				dialog1.setPositiveButton("确认", new OnClickListener() {
					@Override
					public void onClick(View v) {
						finishTask();
					}
				});
				dialog1.show();
			}
			
		}
		
		if(R.id.wg_feedbacklist_btn1==view.getId()){
			
			Object tag = view.getTag();
			
			if (tag != null) {
				
				int position = (Integer) tag;
				//Log.d("longhua", "position:" + position);
				String feedbackId =  dataList.get(position).getId();
				//String name =  dataList.get(position).getArg12();
			    Intent in=new Intent(view.getContext(),WgTaskFeedback.class);
				in.putExtra("feedbackId", String.valueOf(feedbackId));
				//in.putExtra("name", name);
				this.startActivity(in);
				overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				return;
			}
			
		}
		
		return;
	}
	
	public void doTask(){
		
		SysPassValue temp = new SysPassValue();
		temp.setFunctionName("startTask");
		temp.setUserId(String.valueOf(userId));
		temp.setId(this.taskId);
		temp.setArg1("5");
		
		new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
			@Override
			public void onPostExecute(String name, YGetTask<SysPassValue> task) {
				//loading.hideLoading();
				getResult(task.getValue());
			}
		},temp).execute();
	}
	
	public void finishTask(){
		
		SysPassValue temp = new SysPassValue();
		temp.setFunctionName("startTask");
		temp.setUserId(String.valueOf(userId));
		temp.setId(this.taskId);
		temp.setArg1("7");
		
		new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
			@Override
			public void onPostExecute(String name, YGetTask<SysPassValue> task) {
				//loading.hideLoading();
				getResult(task.getValue());
			}
		},temp).execute();
	}
	
	private void getResult(SysPassValue passValue) {
		if (passValue==null) {
			ToastUtils.show(this, "没有获取到数据！");
		} else {
			String returnResult = passValue.getReturnResult();
			String taskId = passValue.getId();
			if(returnResult.equals("1")){
				
				this.finish();
				overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
				return;
			}else{
				ToastUtils.show(this, "提交执行失败！");
				loading.hideLoading();
			}
		}
	}
	
}
