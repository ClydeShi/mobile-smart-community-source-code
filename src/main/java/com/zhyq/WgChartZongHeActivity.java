package com.zhyq;


import android.app.Activity;
import android.content.res.ColorStateList;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;

import com.zhyq.libs.AndroidUtils;
import com.zhyq.libs.PieChartView;
import com.zhyq.libs.ToastUtils;
import com.zhyq.libs.view.wheelview.JudgeDate;
import com.zhyq.libs.view.wheelview.ScreenInfo;
import com.zhyq.libs.view.wheelview.WheelMain;
import com.zhyq.model.SysPassValue;
import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.sendOrderToServerForListYTask;
import com.zhyq.widget.LoadingRelativeLayout;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class WgChartZongHeActivity extends Activity implements OnClickListener {

	private LoadingRelativeLayout loading = null;
	private View includeTitle;

	private PopupWindow popupWindow;
	private float alpha = 1f;

	WheelMain wheelMain;
	TextView txtstartdate;
	TextView txtenddate;
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	private String taskType = "getToday";

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_wg_chart_zonghe);
		loading = LoadingRelativeLayout.getLoadingRelativeLayout(this);
		includeTitle = this.findViewById(R.id.wg_chart_head);
		Button btreturn = (Button)includeTitle.findViewById(R.id.wg_head_btn_return);
		btreturn.setOnClickListener(this);
		((TextView)includeTitle.findViewById(R.id.wg_head_title)).setText("数据分析");

		txtstartdate = ((TextView)this.findViewById(R.id.wg_event_date_start));
		(this.findViewById(R.id.btn_start_date)).setOnClickListener(this);

		txtenddate = ((TextView)this.findViewById(R.id.wg_event_date_end));
		(this.findViewById(R.id.btn_end_date)).setOnClickListener(this);

		this.findViewById(R.id.wg_btn_day).setOnClickListener(this);
		this.findViewById(R.id.wg_btn_week).setOnClickListener(this);
		this.findViewById(R.id.wg_btn_month).setOnClickListener(this);
		this.findViewById(R.id.wg_btn_season).setOnClickListener(this);
		this.findViewById(R.id.wg_btn_year).setOnClickListener(this);
		this.findViewById(R.id.wg_btn_date).setOnClickListener(this);

		this.findViewById(R.id.wg_chart_btn).setOnClickListener(this);
		this.findViewById(R.id.wg_chart_btn2).setOnClickListener(this);

        loading.showLoading();
        GetData(taskType);
	}

	public void GetData(String task){

		String startdate = txtstartdate.getText().toString();
		String enddate = txtenddate.getText().toString();

		SysPassValue temp = new SysPassValue();
		temp.setFunctionName("getWgReportZongHeData");//getUserReport
		temp.setArg1(task);
		temp.setArg2(startdate);
		temp.setArg3(enddate);

		new sendOrderToServerForListYTask(new AbstractYGetTaskListener<List<SysPassValue>>() {//获取检查人员数量
			@Override
			public void onPostExecute(String name, YGetTask<List<SysPassValue>> task) {
				showPersonData(task.getValue());
				loading.hideLoading();
			}
		},temp).execute();

		SysPassValue temp2 = new SysPassValue();
		temp2.setFunctionName("getWgReportZongHeData2");
		temp2.setArg1(task);
		temp2.setArg2(startdate);
		temp2.setArg3(enddate);

		new sendOrderToServerForListYTask(new AbstractYGetTaskListener<List<SysPassValue>>() {
			@Override
			public void onPostExecute(String name, YGetTask<List<SysPassValue>> task) {
				showPersonData2(task.getValue());
				loading.hideLoading();
			}
		},temp2).execute();

		SysPassValue temp3 = new SysPassValue();
		temp3.setFunctionName("getWgReportZongHeData3");
		temp3.setArg1(task);
		temp3.setArg2(startdate);
		temp3.setArg3(enddate);

		new sendOrderToServerForListYTask(new AbstractYGetTaskListener<List<SysPassValue>>() {//获取检查人员数量
			@Override
			public void onPostExecute(String name, YGetTask<List<SysPassValue>> task) {
				showPersonData3(task.getValue());
				loading.hideLoading();
			}
		},temp3).execute();
	}

	public void showPersonData(List<SysPassValue> list){

		float total = 0, data1 = 0, data2 = 0, data3 = 0, data4 = 0, data5 = 0, data6 = 0;
		if(list!=null){
			if(list.size()==6){
				data1 = Float.valueOf(list.get(0).getReturnResult());
				data2 = Float.valueOf(list.get(1).getReturnResult());
				data3 = Float.valueOf(list.get(2).getReturnResult());
				data4 = Float.valueOf(list.get(3).getReturnResult());
				data5 = Float.valueOf(list.get(4).getReturnResult());
				data6 = Float.valueOf(list.get(5).getReturnResult());

				/*data1 = 621;
				data2 = 156;
				data3 = 900;
				data4 = 207;
				data5 = 466;
				data6 = 160;*/
			}
		}

		total = data1 + data2 + data3 + data4 + data5 + data6;
		AndroidUtils.setTextView(this, R.id.wg_report_zonghe_textview_1,"全区共"+(int)total);
		AndroidUtils.setTextView(this, R.id.wg_report_zonghe_textview1,""+(int)data1);//龙华
		AndroidUtils.setTextView(this, R.id.wg_report_zonghe_textview2,""+(int)data2);//民治
		AndroidUtils.setTextView(this, R.id.wg_report_zonghe_textview3,""+(int)data3);//观澜
		AndroidUtils.setTextView(this, R.id.wg_report_zonghe_textview4,""+(int)data4);//大浪
		AndroidUtils.setTextView(this, R.id.wg_report_zonghe_textview5,""+(int)data5);//福城
		AndroidUtils.setTextView(this, R.id.wg_report_zonghe_textview6,""+(int)data6);//观湖

		AndroidUtils.setTextView(this, R.id.wg_report_zonghe_street1_textview1,""+(int)data1);//龙华
		AndroidUtils.setTextView(this, R.id.wg_report_zonghe_street2_textview1,""+(int)data2);//民治
		AndroidUtils.setTextView(this, R.id.wg_report_zonghe_street3_textview1,""+(int)data3);//观澜
		AndroidUtils.setTextView(this, R.id.wg_report_zonghe_street4_textview1,""+(int)data4);//大浪
		AndroidUtils.setTextView(this, R.id.wg_report_zonghe_street5_textview1,""+(int)data5);//福城
		AndroidUtils.setTextView(this, R.id.wg_report_zonghe_street6_textview1,""+(int)data6);//观湖

		PieChartView pieChartView = (PieChartView)findViewById(R.id.pie);
        PieChartView.PieItemBean[] items = new PieChartView.PieItemBean[]{
                new PieChartView.PieItemBean("龙华", data1),
                new PieChartView.PieItemBean("民治", data2),
                new PieChartView.PieItemBean("观澜", data3),
                new PieChartView.PieItemBean("大浪", data4),
                new PieChartView.PieItemBean("福城", data5),
                new PieChartView.PieItemBean("观湖", data6)
        };
        pieChartView.setPieItems(items);
	}

	public void showPersonData2(List<SysPassValue> list){

		float total = 0, data1 = 0, data2 = 0, data3 = 0, data4 = 0, data5 = 0, data6 = 0;
		if(list!=null){
			if(list.size()==6){
				data1 = Float.valueOf(list.get(0).getReturnResult());
				data2 = Float.valueOf(list.get(1).getReturnResult());
				data3 = Float.valueOf(list.get(2).getReturnResult());
				data4 = Float.valueOf(list.get(3).getReturnResult());
				data5 = Float.valueOf(list.get(4).getReturnResult());
				data6 = Float.valueOf(list.get(5).getReturnResult());

				/*data1 = 206;
				data2 = 200;
				data3 = 600;
				data4 = 216;
				data5 = 160;
				data6 = 100;*/
			}
		}

		total = data1 + data2 + data3 + data4 + data5 + data6;
		AndroidUtils.setTextView(this, R.id.wg_report_zonghe2_textview_1,"全区共"+(int)total);
		AndroidUtils.setTextView(this, R.id.wg_report_zonghe2_textview1,""+(int)data1);//龙华
		AndroidUtils.setTextView(this, R.id.wg_report_zonghe2_textview2,""+(int)data2);//民治
		AndroidUtils.setTextView(this, R.id.wg_report_zonghe2_textview3,""+(int)data3);//观澜
		AndroidUtils.setTextView(this, R.id.wg_report_zonghe2_textview4,""+(int)data4);//大浪
		AndroidUtils.setTextView(this, R.id.wg_report_zonghe2_textview5,""+(int)data5);//福城
		AndroidUtils.setTextView(this, R.id.wg_report_zonghe2_textview6,""+(int)data6);//观湖

		AndroidUtils.setTextView(this, R.id.wg_report_zonghe_street1_textview2,""+(int)data1);//龙华
		AndroidUtils.setTextView(this, R.id.wg_report_zonghe_street2_textview2,""+(int)data2);//民治
		AndroidUtils.setTextView(this, R.id.wg_report_zonghe_street3_textview2,""+(int)data3);//观澜
		AndroidUtils.setTextView(this, R.id.wg_report_zonghe_street4_textview2,""+(int)data4);//大浪
		AndroidUtils.setTextView(this, R.id.wg_report_zonghe_street5_textview2,""+(int)data5);//福城
		AndroidUtils.setTextView(this, R.id.wg_report_zonghe_street6_textview2,""+(int)data6);//观湖

		PieChartView pieChartView = (PieChartView)findViewById(R.id.pie2);
		PieChartView.PieItemBean[] items = new PieChartView.PieItemBean[]{
				new PieChartView.PieItemBean("龙华", data1),
				new PieChartView.PieItemBean("民治", data2),
				new PieChartView.PieItemBean("观澜", data3),
				new PieChartView.PieItemBean("大浪", data4),
				new PieChartView.PieItemBean("福城", data5),
				new PieChartView.PieItemBean("观湖", data6)
		};
		pieChartView.setPieItems(items);
	}

	public void showPersonData3(List<SysPassValue> list){

		float total = 0, data1 = 0, data2 = 0, data3 = 0, data4 = 0, data5 = 0, data6 = 0;
		if(list!=null){
			if(list.size()==6){
				data1 = Float.valueOf(list.get(0).getReturnResult());
				data2 = Float.valueOf(list.get(1).getReturnResult());
				data3 = Float.valueOf(list.get(2).getReturnResult());
				data4 = Float.valueOf(list.get(3).getReturnResult());
				data5 = Float.valueOf(list.get(4).getReturnResult());
				data6 = Float.valueOf(list.get(5).getReturnResult());

				/*data1 = 600;
				data2 = 160;
				data3 = 800;
				data4 = 200;
				data5 = 123;
				data6 = 100;*/
			}
		}

		total = data1 + data2 + data3 + data4 + data5 + data6;
		AndroidUtils.setTextView(this, R.id.wg_report_zonghe3_textview_1,"全区共"+(int)total);
		AndroidUtils.setTextView(this, R.id.wg_report_zonghe3_textview1,""+(int)data1);//龙华
		AndroidUtils.setTextView(this, R.id.wg_report_zonghe3_textview2,""+(int)data2);//民治
		AndroidUtils.setTextView(this, R.id.wg_report_zonghe3_textview3,""+(int)data3);//观澜
		AndroidUtils.setTextView(this, R.id.wg_report_zonghe3_textview4,""+(int)data4);//大浪
		AndroidUtils.setTextView(this, R.id.wg_report_zonghe3_textview5,""+(int)data5);//福城
		AndroidUtils.setTextView(this, R.id.wg_report_zonghe3_textview6,""+(int)data6);//观湖

		AndroidUtils.setTextView(this, R.id.wg_report_zonghe_street1_textview3,""+(int)data1);//龙华
		AndroidUtils.setTextView(this, R.id.wg_report_zonghe_street2_textview3,""+(int)data2);//民治
		AndroidUtils.setTextView(this, R.id.wg_report_zonghe_street3_textview3,""+(int)data3);//观澜
		AndroidUtils.setTextView(this, R.id.wg_report_zonghe_street4_textview3,""+(int)data4);//大浪
		AndroidUtils.setTextView(this, R.id.wg_report_zonghe_street5_textview3,""+(int)data5);//福城
		AndroidUtils.setTextView(this, R.id.wg_report_zonghe_street6_textview3,""+(int)data6);//观湖

		PieChartView pieChartView = (PieChartView)findViewById(R.id.pie3);
		PieChartView.PieItemBean[] items = new PieChartView.PieItemBean[]{
				new PieChartView.PieItemBean("龙华", data1),
				new PieChartView.PieItemBean("民治", data2),
				new PieChartView.PieItemBean("观澜", data3),
				new PieChartView.PieItemBean("大浪", data4),
				new PieChartView.PieItemBean("福城", data5),
				new PieChartView.PieItemBean("观湖", data6)
		};
		pieChartView.setPieItems(items);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	public void onClick(View view) {

		if(R.id.wg_head_btn_return==view.getId()){
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		}

		if(R.id.wg_btn_day==view.getId()) {
			loading.showLoading();
			GetData("getToday");
		}

		if(R.id.wg_btn_week==view.getId()) {
			loading.showLoading();
			GetData("getWeek");
		}

		if(R.id.wg_btn_month==view.getId()) {
			loading.showLoading();
			GetData("getMonth");
		}

		if(R.id.wg_btn_season==view.getId()) {
			loading.showLoading();
			GetData("getSeason");
		}

		if(R.id.wg_btn_year==view.getId()) {
			loading.showLoading();
			GetData("getYear");
		}

		if(R.id.wg_btn_date==view.getId()) {

			String startdate = txtstartdate.getText().toString();
			String enddate = txtenddate.getText().toString();

			if(startdate.equals("开始日期")){
				ToastUtils.show(this, "请选择查询开始日期");
				return;
			}

			if(enddate.equals("结束日期")){
				ToastUtils.show(this, "请选择查询结束日期");
				return;
			}

			loading.showLoading();
			GetData("getBetweenDate");
		}

		if(R.id.btn_start_date==view.getId()){
			bottombirthdaywindow(txtstartdate, "txtstartdate");
			new Thread(new Runnable(){
                @Override
                public void run() {
                    while(alpha>0.5f){
                        try {
                            //4是根据弹出动画时间和减少的透明度计算
                            Thread.sleep(4);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Message msg =mHandler.obtainMessage();
                        msg.what = 1;
                        //每次减少0.01，精度越高，变暗的效果越流畅
                        alpha-=0.01f;
                        msg.obj =alpha ;
                        mHandler.sendMessage(msg);
                    }
                }

            }).start();
		}

		if(R.id.btn_end_date==view.getId()){
			bottombirthdaywindow(txtenddate, "txtenddate");
			new Thread(new Runnable(){
                @Override
                public void run() {
                    while(alpha>0.5f){
                        try {
                            //4是根据弹出动画时间和减少的透明度计算
                            Thread.sleep(4);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Message msg =mHandler.obtainMessage();
                        msg.what = 1;
                        //每次减少0.01，精度越高，变暗的效果越流畅
                        alpha-=0.01f;
                        msg.obj =alpha ;
                        mHandler.sendMessage(msg);
                    }
                }

            }).start();
		}

		if(R.id.wg_chart_btn==view.getId()) {
			AndroidUtils.start(this, WgChartActivity.class);
			this.finish();
			//overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			return;
		}

		if(R.id.wg_chart_btn2==view.getId()) {
			AndroidUtils.start(this, WgChartZongHeFenXiActivity.class);
			this.finish();
			//overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			return;
		}

		return;
    }

	public String getNowDate(){
        Date currTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd",Locale.US);
        String nowdate = null;
        try {
            nowdate = new String(formatter.format(currTime).getBytes("iso-8859-1"));
        } catch (UnsupportedEncodingException ex) {
        }

        return nowdate;
    }

	//选择日期
	void bottombirthdaywindow(TextView view, String dateType) {
        if (popupWindow != null && popupWindow.isShowing()) {
            return;
        }
        LinearLayout timepickerview = (LinearLayout) getLayoutInflater().inflate(R.layout.timepicker, null);
        wheelMain = new WheelMain(timepickerview);
        ScreenInfo screenInfo = new ScreenInfo(WgChartZongHeActivity.this);
		wheelMain.screenheight = screenInfo.getHeight();
		String time = txtstartdate.getText().toString();
		Calendar calendar = Calendar.getInstance();
		if (JudgeDate.isDate(time, "yyyy-MM-dd")) {
			try {
				calendar.setTime(dateFormat.parse(time));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		wheelMain.initDateTimePicker(year, month, day);

        popupWindow = new PopupWindow(timepickerview,
                LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT);
        //点击空白处时，隐藏掉pop窗口
        popupWindow.setFocusable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        //添加弹出、弹入的动画
        popupWindow.setAnimationStyle(R.style.Popupwindow);
        int[] location = new int[2];
        view.getLocationOnScreen(location);
        popupWindow.showAtLocation(view, Gravity.LEFT | Gravity.BOTTOM, 0, -location[1]);

        //修改的话，这个地方要修改 *****************
        //添加按键事件监听
        setBirthdayListeners(timepickerview, dateType);
        //添加pop窗口关闭事件，主要是实现关闭时改变背景的透明度
        popupWindow.setOnDismissListener(new poponDismissListener());
        backgroundAlpha(1f);
    }

	//监听日期
	private void setBirthdayListeners(LinearLayout layout, String dateType) {

    	Button bt_select_birthday_ok = (Button) layout.findViewById(R.id.btn_select_birthday_ok);
    	Button bt_select_birthday_cancel = (Button) layout.findViewById(R.id.btn_select_birthday_cancel);

    	ColorStateList whiteColor=getResources().getColorStateList(R.color.popwindowselectok);
    	bt_select_birthday_ok.setTextColor(whiteColor);

    	final String fdateType = dateType;

    	bt_select_birthday_ok.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                    //在此处添加你的按键处理 xxx

                	Log.i("longhua",wheelMain.getTime());

                    popupWindow.dismiss();
                    backgroundAlpha(1f);

                    //loading.showLoading();

                    if(fdateType.equals("txtstartdate")){
                    	txtstartdate.setText(wheelMain.getTime());
                    }

                    if(fdateType.equals("txtenddate")){
                    	txtenddate.setText(wheelMain.getTime());
                    }

                    //loading.hideLoading();
                    /*
        			new HhUserModifyPersonInfoYTask(new AbstractYGetTaskListener<User>() {
        				@Override
        				public void onPostExecute(String name, YGetTask<User> task) {

        					userModifyData(task.getValue(),"3");
        				}
        			},user,"3").execute();
        			*/

                }
            }
        });
    	bt_select_birthday_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                    //在此处添加你的按键处理 xxx
                	 //选照片相
                	popupWindow.dismiss();
                	backgroundAlpha(1f);

                    return;

                }
            }
        });

    	bt_select_birthday_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                    popupWindow.dismiss();
                }
            }
        });
    }

	/*
     * 设置添加屏幕的背景透明度
     * @param bgAlpha
     */
    public void backgroundAlpha(float bgAlpha)
    {
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.alpha = bgAlpha; //0.0-1.0
        getWindow().setAttributes(lp);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

       /*
        *    WindowManager.LayoutParams lp = activity.getWindow().getAttributes();  
        lp.alpha = bgAlpha;  
        if (bgAlpha == 1) {  
            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);//不移除该Flag的话,在有视频的页面上的视频会出现黑屏的bug  
        } else {  
            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);//此行代码主要是解决在华为手机上半透明效果无效的bug  
        }  
        activity.getWindow().setAttributes(lp);
        */
    }

	/*
     * 返回或者点击空白位置的时候将背景透明度改回来
     */
    class poponDismissListener implements OnDismissListener{

        @Override
        public void onDismiss() {
            // TODO Auto-generated method stub
            new Thread(new Runnable(){
                @Override
                public void run() {
                    //此处while的条件alpha不能<= 否则会出现黑屏
                    while(alpha<1f){
                        try {
                            Thread.sleep(4);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Log.d("longhua","alpha:"+alpha);
                        Message msg =mHandler.obtainMessage();
                        msg.what = 1;
                        alpha+=0.01f;
                        msg.obj =alpha ;
                        mHandler.sendMessage(msg);
                    }
                }

            }).start();
        }
    }
    
    Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case 1: 
                	backgroundAlpha(alpha);
                    //backgroundAlpha((float)msg.obj);
                    break;
            }
        }
    };
    
    /*
    class ThreadDemo implements Runnable {
        public void run() {
        	//Log.i("longhua", "1 run tempUserId"+tempUserId);
        	GetData(taskType);
            myHandler.sendEmptyMessage(0);
        }
    }
    
    Handler myHandler = new Handler() {
    	
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case 0:
            	Log.i("longhua", "handleMessage有数据");
            	//drawPolyLine(trackLatLngs);
            	//thread = new WgMapThread(trackLatLngs,mMapView,mMoveMarker,mHandler);
				//thread.start();
                //loading.hideLoading();
                break;
            case 1:
                Log.i("longhua", "handleMessage无数据");
                loading.hideLoading();
                break;
            }
            super.handleMessage(msg);
        }
    };
    */
		 	
}
