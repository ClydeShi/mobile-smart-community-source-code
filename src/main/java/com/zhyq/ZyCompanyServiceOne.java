package com.zhyq;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.util.common.StrEdit;
import com.zhyq.libs.PreferenceUtils;
import com.zhyq.libs.ProgersssDialog;
import com.zhyq.libs.ToastUtils;
import com.zhyq.libs.iosdialog.widget.MyAlertDialog;
import com.zhyq.model.SysPassValue;
import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.sendOrderToServerForValueYTask;
import com.zhyq.widget.LoadingRelativeLayout;


public class ZyCompanyServiceOne extends Activity implements OnClickListener {

	public ProgersssDialog dialog;

	private String userCode = "";
	private String userName = "";
	private Long userId = null;
	private int id = 0;

	private LoadingRelativeLayout loading = null;
	public int resultCode = 0;
	public static boolean active = false;

	private View includeTitle ;

	private String type = "";
	private String name = "";
	private String channelName = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		showProgressDialog();

		this.setContentView(R.layout.activity_zy_service_one);
		loading = LoadingRelativeLayout.getLoadingRelativeLayout(this);

		name = this.getIntent().getStringExtra("name");
		type = this.getIntent().getStringExtra("type");
		if(type==null){
			ToastUtils.show(this, "参数错误！");
		}
		if(type.equals("1")){
			name = "创投服务";
		}else if(type.equals("2")){
			name = "出行服务";
		}else if(type.equals("3")){
			name = "日常服务";
		}

		includeTitle = this.findViewById(R.id.zy_service_head);
		((TextView)includeTitle.findViewById(R.id.util_my_head_title)).setText(name);
		Button btnReturn = (Button)includeTitle.findViewById(R.id.util_my_head_btn_return);
		btnReturn.setOnClickListener(this);

		View util_my_head_relativelayout1 = (View)includeTitle.findViewById(R.id.util_my_head_relativelayout1);
		util_my_head_relativelayout1.setVisibility(View.VISIBLE);

		View util_my_head_relativelayout2 = (View)includeTitle.findViewById(R.id.util_my_head_relativelayout2);
		util_my_head_relativelayout2.setVisibility(View.INVISIBLE);

		(this.findViewById(R.id.service_btn15)).setOnClickListener(this);
		(this.findViewById(R.id.service_btn16)).setOnClickListener(this);
		(this.findViewById(R.id.service_btn17)).setOnClickListener(this);
		(this.findViewById(R.id.service_btn18)).setOnClickListener(this);
		(this.findViewById(R.id.service_btn19)).setOnClickListener(this);
		(this.findViewById(R.id.service_btn20)).setOnClickListener(this);
		(this.findViewById(R.id.service_btn21)).setOnClickListener(this);
		(this.findViewById(R.id.service_btn22)).setOnClickListener(this);
		(this.findViewById(R.id.service_btn23)).setOnClickListener(this);
		(this.findViewById(R.id.service_btn24)).setOnClickListener(this);
		(this.findViewById(R.id.service_btn25)).setOnClickListener(this);
		(this.findViewById(R.id.service_btn26)).setOnClickListener(this);
		(this.findViewById(R.id.service_btn27)).setOnClickListener(this);
		(this.findViewById(R.id.service_btn28)).setOnClickListener(this);
		(this.findViewById(R.id.service_btn29)).setOnClickListener(this);
		(this.findViewById(R.id.service_btn30)).setOnClickListener(this);
		(this.findViewById(R.id.service_btn31)).setOnClickListener(this);

		new Handler().postDelayed(new Runnable() {//延迟读取数据
			@Override
			public void run() {
				dialog.dismiss();
			}
		}, 200);

	}

	public void showProgressDialog(){
		dialog = new ProgersssDialog(this);
		//dialog.show();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	public void onClick(View view) {
		if(R.id.util_my_head_btn_return==view.getId()){
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		}

		/*if(R.id.service_btn11==view.getId()){
			Intent intent=new Intent(view.getContext(), ZyMeetingRoomListActivity.class);
			Bundle bundle=new Bundle();
			bundle.putString("channelId", "2");
			intent.putExtras(bundle);
			startActivity(intent);
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			return;
		}

		if(R.id.service_btn12==view.getId()){
			Intent intent=new Intent(view.getContext(), ZyMeetingRoomListActivity.class);
			Bundle bundle=new Bundle();
			bundle.putString("channelId", "1");
			intent.putExtras(bundle);
			startActivity(intent);
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			return;
		}

		if(R.id.service_btn13==view.getId()){
			Toast.makeText(view.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
			return;
		}

		if(R.id.service_btn14==view.getId()){
			Toast.makeText(view.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
			return;
		}*/

		if(R.id.service_btn15==view.getId()){
			Toast.makeText(view.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
			return;
		}

		if(R.id.service_btn16==view.getId()){
			Toast.makeText(view.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
			return;
		}

		if(R.id.service_btn17==view.getId()){
			Toast.makeText(view.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
			return;
		}

		if(R.id.service_btn18==view.getId()){
			Toast.makeText(view.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
			return;
		}

		if(R.id.service_btn19==view.getId()){
			Toast.makeText(view.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
			return;
		}

		if(R.id.service_btn20==view.getId()){
			Toast.makeText(view.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
			return;
		}

		if(R.id.service_btn21==view.getId()){
			Toast.makeText(view.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
			return;
		}

		if(R.id.service_btn22==view.getId()){
			Toast.makeText(view.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
			return;
		}

		if(R.id.service_btn23==view.getId()){
			Toast.makeText(view.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
			return;
		}

		if(R.id.service_btn24==view.getId()){
			Toast.makeText(view.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
			return;
		}

		if(R.id.service_btn25==view.getId()){
			Toast.makeText(view.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
			return;
		}

		if(R.id.service_btn26==view.getId()){
			Toast.makeText(view.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
			return;
		}

		if(R.id.service_btn27==view.getId()){
			Toast.makeText(view.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
			return;
		}

		if(R.id.service_btn28==view.getId()){
			Toast.makeText(view.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
			return;
		}

		if(R.id.service_btn29==view.getId()){
			Toast.makeText(view.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
			return;
		}

		if(R.id.service_btn30==view.getId()){
			Toast.makeText(view.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
			return;
		}

		if(R.id.service_btn31==view.getId()){
			Toast.makeText(view.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
			return;
		}


	}


}
