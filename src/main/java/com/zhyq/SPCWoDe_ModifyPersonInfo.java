package com.zhyq;


import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;


import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;



import com.android.http.CircleNetworkImage;
import com.util.common.StrEdit;
import com.zhyq.libs.AndroidUtils;

import com.zhyq.libs.CustomProgressDialog;
import com.zhyq.libs.PreferenceUtils;
import com.zhyq.libs.ToastUtils;
import com.zhyq.libs.Tools;
import com.zhyq.libs.iosdialog.widget.MyAlertDialog;
import com.zhyq.libs.view.wheelview.JudgeDate;

import com.zhyq.libs.view.wheelview.ScreenInfo;
import com.zhyq.libs.view.wheelview.WheelMain;
import com.zhyq.model.SysPassValue;
import com.zhyq.model.User;
import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.UserLoginYTask;
import com.zhyq.task.impl.sendOrderToServerForValueYTask;
import com.zhyq.widget.LoadingRelativeLayout;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;


import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;


import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;


//修改个人信息

public class SPCWoDe_ModifyPersonInfo extends Activity implements OnClickListener , EasyPermissions.PermissionCallbacks {
	public static final int request_person_result = 8;

	static boolean active = false;
	private  String usercode=null;
	private  String nickname=null;
	private  String realname=null;
	private  String xing=null;
	private  String ming=null;
	private  String carnumber=null;
	private  String usersex =null;
	private  String userbirthday=null;
	private  String useraddress=null;

	private  String province=null;
	private  String city=null;
	private  String country=null;

	private View  includeTitle ;
	private Button returnImage;
	private TextView  returnText;

	private PopupWindow popupWindow;
	private User user=new User();

	private float alpha=1f;

	private CircleNetworkImage   faceImage=null;

	private String faceImageNames = "hh_userface.png";
	private LoadingRelativeLayout loading=null;

	private Button bt_logout = null;

	WheelMain wheelMain;
	TextView txtbirthday;
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	File fileDir;
	String path = Environment.getExternalStorageDirectory()
			+ "/myfaceImg/";// 文件目录




	private String[] items = new String[] { "选择本地图片", "拍照" };
	/*头像名称*/
	private static final String faceimage_file="faceImage.jpg";
	/* 请求码*/
	private static final int IMAGE_REQUEST_CODE = 0;
	private static final int CAMERA_REQUEST_CODE = 1;
	private static final int RESULT_REQUEST_CODE = 2;

	public static final int WRITE_EXTERNAL_STORAGE = 100;
	private boolean isFirst = false;

	//权限参数 读写参数
	String[] params = {Manifest.permission.WRITE_EXTERNAL_STORAGE};

	private int requestCode;  //请求码
	public CustomProgressDialog dialog;
	private boolean firstopen = true;   //第一打开 用遮挡的面板 遮挡住


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


		this.setContentView(R.layout.activity_spc_wode_personinfo);//util_spc_head_title_panel
		loading=LoadingRelativeLayout.getLoadingRelativeLayout(this);

		//打开加载窗口
		showMyDialog();

		includeTitle = this.findViewById(R.id.spc_modify_personinfo_head);

		((TextView)includeTitle.findViewById(R.id.tv_spc_head_title)).setText("个人信息 ");
		returnImage = (Button)includeTitle.findViewById(R.id.bt_spc_head_return);
		//returnImage = (ImageView)includeTitle.findViewById(R.id.iv_spc_head_return);
		returnImage.setOnClickListener(this);



		//btn_spc_modify_personinfo_header
		faceImage  = (CircleNetworkImage)this.findViewById(R.id.spc_modify_personinfo_header);



		/*usercode = HhApplication.getInstance().getHhCart().getUser().getCode();
		nickname = HhApplication.getInstance().getHhCart().getUser().getnickname();
		realname = HhApplication.getInstance().getHhCart().getUser().getName();
		carnumber = "";
		usersex  = HhApplication.getInstance().getHhCart().getUser().getUserSex();
		userbirthday  = HhApplication.getInstance().getHhCart().getUser().getuserbirthday();
		useraddress   = HhApplication.getInstance().getHhCart().getUser().getdetailAddress();
		province      = HhApplication.getInstance().getHhCart().getUser().getProvince();
		city          = HhApplication.getInstance().getHhCart().getUser().getCity();
		country       = HhApplication.getInstance().getHhCart().getUser().getCounty();


		xing       = "";
		ming       = "";*/





		txtbirthday = ((TextView)this.findViewById(R.id.spc_modify_personinfo_birthday));


		(this.findViewById(R.id.btn_spc_modify_personinfo_header)).setOnClickListener(this);
		(this.findViewById(R.id.btn_modify_personinfo_nickname)).setOnClickListener(this);
		(this.findViewById(R.id.btn_modify_personinfo_realname)).setOnClickListener(this);
		(this.findViewById(R.id.btn_modify_personinfo_stop_number)).setOnClickListener(this);
		(this.findViewById(R.id.btn_modify_pay_person_password)).setOnClickListener(this);

		//

		//

		(this.findViewById(R.id.btn_modify_personinfo_sex)).setOnClickListener(this);
		(this.findViewById(R.id.btn_modify_personinfo_birthday)).setOnClickListener(this);

		(this.findViewById(R.id.btn_modify_personinfo_receiveaddress)).setOnClickListener(this);

		bt_logout = (Button)this.findViewById(R.id.spc_change_curuser);
		bt_logout.setOnClickListener(this);


		//图片地址

		//名称
		AndroidUtils.setTextView(this, R.id.spc_modify_personinfo_nickname,nickname);

		//名称
		AndroidUtils.setTextView(this, R.id.spc_modify_personinfo_realname,xing+ming);


		//性别
		AndroidUtils.setTextView(this, R.id.spc_modify_personinfo_sex,usersex);
		//生日
		AndroidUtils.setTextView(this, R.id.spc_modify_personinfo_birthday,userbirthday);

		//车牌
		AndroidUtils.setTextView(this, R.id.spc_modify_personinfo_stop_bus_number,carnumber);
		//地址  这个地方不能赋予值给他
		//AndroidUtils.setTextView(this, R.id.hh_modify_personinfo_receiveaddress,province+"  "+city+"  "+country+"  "+useraddress );





		//下载，并且设置头像  localfaceImage
		String faceImageUrl =null;
		//faceImageUrl = HhApplication.getInstance().getHhCart().getUser().getnetpic();

		if(faceImageUrl==null){
			faceImageUrl="";
		}



		//spc_modify_personinfo_header

		if(!faceImageUrl.equals("")){
			View v1 = this.findViewById(R.id.spc_modify_personinfo_header);
			AndroidUtils.setWebImageCircleView(v1, R.id.spc_modify_personinfo_header,faceImageUrl);
		}

		firstopen = false;
		closeMyDialog();




	}

	@Override
	protected void onResume() {
		super.onResume();
		if (isFirst) {
			//因为要通过一个Fragment来弹出弹出框，所以activity这里的onResume执行了两次，这里进行判断
			isFirst = false;
			if (!EasyPermissions.hasPermissions(this, params)) {
				EasyPermissions.requestPermissions(this, "需要读写本地权限(1)", WRITE_EXTERNAL_STORAGE, params);
			}
		}

	}

	@Override
	public void onClick(View view) {

		//重新设置USER
		user = HhApplication.getInstance().getHhCart().getUser();

		//
		if(R.id.bt_spc_head_return==view.getId()){
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		}else if(R.id.btn_spc_modify_personinfo_header==view.getId()) { // 修改头像

			bottomwindow(faceImage);
			new Thread(new Runnable(){
				@Override
				public void run() {
					while(alpha>0.5f){
						try {
							//4是根据弹出动画时间和减少的透明度计算
							Thread.sleep(4);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						Message msg =mHandler.obtainMessage();
						msg.what = 1;
						//每次减少0.01，精度越高，变暗的效果越流畅
						alpha-=0.01f;
						msg.obj =alpha ;
						mHandler.sendMessage(msg);
					}
				}

			}).start();

		}else if(R.id.btn_modify_personinfo_nickname==view.getId()) { //修改昵称



			//Log.i("huahua","nickname");
			/*Intent intent=new Intent(this, SPCWoDe_ModifyPersonNickNameActivity.class);
			Bundle bundle=new Bundle();

			TextView tvNickName = (TextView)this.findViewById(R.id.spc_modify_personinfo_nickname);
			nickname = tvNickName.getText().toString();

			bundle.putString("nickname", nickname);
			intent.putExtras(bundle);

			requestCode = 8;
			startActivityForResult(intent, requestCode);//这里采用startActivityForResult来做跳转，此处的0为一个依据，可以写其他的值，但一定要>=0
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);*/


		}else if(R.id.btn_modify_personinfo_sex==view.getId()) { // 修改性别


			TextView vSex = (TextView)this.findViewById(R.id.spc_modify_personinfo_sex);
			bottomSexwindow(vSex);
			new Thread(new Runnable(){
				@Override
				public void run() {
					while(alpha>0.5f){
						try {
							//4是根据弹出动画时间和减少的透明度计算
							Thread.sleep(4);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						Message msg =mHandler.obtainMessage();
						msg.what = 1;
						//每次减少0.01，精度越高，变暗的效果越流畅
						alpha-=0.01f;
						msg.obj =alpha ;
						mHandler.sendMessage(msg);
					}
				}

			}).start();

		}else if(R.id.btn_modify_personinfo_birthday==view.getId()){ // 修改生日
			Log.i("huahua","modify birthday1");
			//modifuBirthDay();
			bottombirthdaywindow(txtbirthday);
			new Thread(new Runnable(){
				@Override
				public void run() {
					while(alpha>0.5f){
						try {
							//4是根据弹出动画时间和减少的透明度计算
							Thread.sleep(4);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						Message msg =mHandler.obtainMessage();
						msg.what = 1;
						//每次减少0.01，精度越高，变暗的效果越流畅
						alpha-=0.01f;
						msg.obj =alpha ;
						mHandler.sendMessage(msg);
					}
				}

			}).start();


		}else if (R.id.btn_modify_personinfo_receiveaddress==view.getId()){ //修改收货地址
			//HhBrowseUserAddressActivity



		}else if(R.id.btn_modify_personinfo_realname==view.getId()) { //修改名字







		}else if(R.id.btn_modify_personinfo_stop_number==view.getId()) { //修改车牌





		}else if(R.id.spc_change_curuser == view.getId()){

			PreferenceUtils.setLong(this,"userlogin",0);
			HhApplication.getInstance().getHhCart().setUser(null);

			//注意： 必须是传递给 Activity ,不能传递给 我的 fragment,否者获取不到信息
			Intent intent = new Intent(this,WgMainActivity.class);
			setResult(request_person_result, intent);
			//可能会让你选择
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
			//
		}else if(R.id.btn_modify_pay_person_password == view.getId()){

			//item_input_pwd
			View v1 = null;
			LayoutInflater mLayoutInflater=LayoutInflater.from(this);
			v1=mLayoutInflater.inflate(R.layout.item_input_pwd,null);

			final EditText et1 = (EditText)v1.findViewById(R.id.et_input_pwd);

			final MyAlertDialog dialog1 = new MyAlertDialog(SPCWoDe_ModifyPersonInfo.this)
					.builder()
					.setTitle("验证原密码")
					.setMsg("为保障你的数据安全,修改密码前请填写APP登入密码。")
					.setView(v1)
					.setNegativeButton("取消", new OnClickListener() {
						@Override
						public void onClick(View v) {

						}
					});
			dialog1.setPositiveButton("确认", new OnClickListener() {
				@Override
				public void onClick(View v) {

					String oldpwd = et1.getText().toString();

					if(oldpwd==null){
						oldpwd="";
					}

					if(oldpwd.equals("")){
						//Toast.makeText(getApplicationContext(),"请输入APP登入密码!", 1).show();
						return;
					}
					verifyLogPassword(oldpwd);
				}
			});
			dialog1.show();







		}
	}

	//高于7.0版本的需要使用危险权限授权
	/**
	 * 检查权限
	 */
	@AfterPermissionGranted(WRITE_EXTERNAL_STORAGE)
	private void checkPerm() {

		if (EasyPermissions.hasPermissions(this, params)) {
			//已经获取到权限
			bottomwindow(faceImage);
			new Thread(new Runnable(){
				@Override
				public void run() {
					while(alpha>0.5f){
						try {
							//4是根据弹出动画时间和减少的透明度计算
							Thread.sleep(4);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						Message msg =mHandler.obtainMessage();
						msg.what = 1;
						//每次减少0.01，精度越高，变暗的效果越流畅
						alpha-=0.01f;
						msg.obj =alpha ;
						mHandler.sendMessage(msg);
					}
				}

			}).start();

		} else {
			EasyPermissions.requestPermissions(this, "需要读写本地权限(2)", WRITE_EXTERNAL_STORAGE, params);
		}


	}


	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
	}

	@Override
	public void onPermissionsGranted(int requestCode, List<String> perms) {

	}

	@Override
	public void onPermissionsDenied(int requestCode, List<String> perms) {
		if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
			//这个方法有个前提是，用户点击了“不再询问”后，才判断权限没有被获取到
			new AppSettingsDialog.Builder(this)
					.setRationale("没有该权限，此应用程序可能无法正常工作。打开应用设置界面以修改应用权限")
					.setTitle("必需权限")
					.build()
					.show();
		} else if (!EasyPermissions.hasPermissions(this, params)) {
			//这里响应的是除了AppSettingsDialog这个弹出框，剩下的两个弹出框被拒绝或者取消的效果
			finish();
		}

	}


	//文件上传
	class FileupThread extends Thread{
		String ls_imagename;
		String ls_telephone;
		public FileupThread(String imagename,String telephone){
			ls_imagename = imagename;
			ls_telephone = telephone;
		}
		@Override
		public void run() {
			//这里写运行方法，可以直接调用  参数 ;
			uploadFile(ls_imagename,ls_telephone) ;
		}
	}



	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		//结果码不等于取消时候
		//if (resultCode != Activity.RESULT_OK)
		//	  return;



		//下面是针对 图片裁剪  IMAGE_REQUEST_CODE
		switch (requestCode) {
			case IMAGE_REQUEST_CODE:
				if(data!=null){
					startPhotoZoom(data.getData());
				}
				break;
			case CAMERA_REQUEST_CODE:

				if (Tools.hasSdcard()) {

					Log.i("huahua","hasSdcard");

					Uri uri;

					if (Build.VERSION.SDK_INT >= 23) {
						Log.i("huahua","hasSdcard>=23");
						uri = FileProvider.getUriForFile(this, "com.zhyq.fileprovider", new File(Environment.getExternalStorageDirectory() + "/" + faceimage_file));

					}else {
						Log.i("huahua","uri");
						uri = Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/" + faceimage_file));
					}
					startPhotoZoom(uri);
				} else {
					Toast.makeText(SPCWoDe_ModifyPersonInfo.this, "未找到存储卡，无法存储照片！", Toast.LENGTH_LONG).show();
				}
				break;


			case RESULT_REQUEST_CODE:
				if (data != null) {
					//显示图片
					String ls_path = Environment .getExternalStorageDirectory().toString();
					getImageToView(data,ls_path);
					String ls_imagename = Environment .getExternalStorageDirectory()+  "/" + faceimage_file;  // 这个是图片路径 直接保存到网络上去

					String ls_face = usercode+ StrEdit.getDateForFileName()+".jpg";
					// 启动数据上传功能
					new FileupThread(ls_imagename,ls_face).start();
				}
				break;

		}
		//ModifyType： 修改类型
		//            0  修改头像
		//            1 修改昵称
		//            2 修改性别
		//            3 修改生日
		//            4 修改收货地址
		//            5 车牌
		//            6 姓名

		//Log.i("huahua","requestCode="+String.valueOf(requestCode)+", resultCode="+String.valueOf(resultCode));
		if(requestCode==8) {

			//昵称返回的值
			//setResult(request_editnickname_result, intent);
			if (resultCode == 0){
				if (data != null) {
					Bundle b=data.getExtras(); //data为B中回传的Intent
					String  newnickName=b.getString("nickName");
					Log.i("huahua","nickName="+newnickName);

					AndroidUtils.setTextView(this, R.id.spc_modify_personinfo_nickname,newnickName);
				}
			}
			//修改汽车车牌号码
			if (resultCode == 5){
				if (data != null) {
					Bundle b=data.getExtras(); //data为B中回传的Intent
					carnumber=b.getString("carnumber");
					AndroidUtils.setTextView(this, R.id.spc_modify_personinfo_stop_bus_number,carnumber);
				}
			}


			//返回姓名  	         intent.putExtra("xing", newnickname);
			//intent.putExtra("ming", ming);
			//setResult(request_editrealname_result, intent);
			if (resultCode == 6){
				if (data != null) {
					Bundle b=data.getExtras(); //data为B中回传的Intent
					xing=b.getString("xing");
					ming=b.getString("ming");

					AndroidUtils.setTextView(this, R.id.spc_modify_personinfo_realname,xing+ming);
				}
			}



			if (resultCode == 1){
				if (data != null) {
					Bundle b=data.getExtras(); //data为B中回传的Intent

					String status = b.getString("status");
					if(status.equals("3")){ //修改生日
						String  userSex=b.getString("birthday");
						AndroidUtils.setTextView(this, R.id.spc_modify_personinfo_birthday,userSex);

					}else if(status.equals("4")){ //修改收货地址
						String  address=b.getString("address");
						AndroidUtils.setTextView(this, R.id.spc_modify_personinfo_receiveaddress,address);

					}
				}
			}
		}


		super.onActivityResult(requestCode, resultCode, data);
	}



	/**
	 * 裁剪图片方法实现
	 *
	 * @param uri
	 */
	private Uri imageUri;
	public void startPhotoZoom(Uri uri) {

		if(uri==null){
			Log.i("huahua","uri is null");
		}

		Intent intent = new Intent("com.android.camera.action.CROP");

		//下面这个段代码如果不填写 android 7.0 那么 照相裁剪的时候就不能裁剪 图片
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
			intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
		}


		intent.setDataAndType(uri, "image/*");
		// 设置裁剪
		intent.putExtra("crop", "true");
		//intent.putExtra(MediaStore.EXTRA_OUTPUT,imageUri);//图像输出  
		// aspectX aspectY 是宽高的比例
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		// outputX outputY 是裁剪图片宽高
		intent.putExtra("outputX", 300);
		intent.putExtra("outputY", 300);
		//intent.putExtra("outputFormat",Bitmap.CompressFormat.JPEG.toString());
		//intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
		intent.putExtra("return-data", true);
		startActivityForResult(intent, 2);
	}

	/**
	 * 保存裁剪之后的图片数据
	 *
	 * @param
	 */
	private void getImageToView(Intent data,String filepath) {
		Bundle extras = data.getExtras();
		if (extras != null) {
			Bitmap photo = extras.getParcelable("data");
			Drawable drawable = new BitmapDrawable(photo);
			faceImage.setImageDrawable(drawable);
			savePic(photo,filepath);

		}
	}

	/* 上传文件至Server的方法 */
	/*   http://112.74.57.196/clientupfile   指向的是  web.xml中的serverlet 为 clientupfile */
	private void uploadFile(String upfile,String newName)
	{
		String end = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		String actionUrl = "http://112.74.57.196/hgc/clientupfile";

		try
		{


			//showDialog("11-上传成功");
			URL url =new URL(actionUrl);
			HttpURLConnection con=(HttpURLConnection)url.openConnection();
        /* 允许Input、Output，不使用Cache */
			con.setDoInput(true);
			con.setDoOutput(true);
			con.setUseCaches(false);
        /* 设置传送的method=POST */
			con.setRequestMethod("POST");
        /* setRequestProperty */
			con.setRequestProperty("Connection", "Keep-Alive");
			con.setRequestProperty("Charset", "UTF-8");
			con.setRequestProperty("Content-Type",
					"multipart/form-data;boundary="+boundary);
        /* 设置DataOutputStream */
			DataOutputStream ds =           new DataOutputStream(con.getOutputStream());
			ds.writeBytes(twoHyphens + boundary + end);
			ds.writeBytes("Content-Disposition: form-data; " +
					"name=\"file1\";filename=\"" +
					newName +"\"" + end);
			ds.writeBytes(end);

        /* 取得文件的FileInputStream */
			FileInputStream fStream = new FileInputStream(upfile);
        /* 设置每次写入1024bytes */
			int bufferSize = 1024;
			byte[] buffer = new byte[bufferSize];

			int length = -1;
        /* 从文件读取数据至缓冲区 */
			while((length = fStream.read(buffer)) != -1)
			{
          /* 将资料写入DataOutputStream中 */
				ds.write(buffer, 0, length);
			}
			ds.writeBytes(end);
			ds.writeBytes(twoHyphens + boundary + twoHyphens + end);

        /* close streams */
			fStream.close();
			ds.flush();

        /* 取得Response内容 */
			InputStream is = con.getInputStream();
			int ch;
			StringBuffer b =new StringBuffer();
			while( ( ch = is.read() ) != -1 )
			{
				b.append( (char)ch );
			}
        /* 将Response显示于Dialog */
			//showDialog("上传成功"+b.toString().trim());
        /* 关闭DataOutputStream */
			ds.close();
		}
		catch(Exception e)
		{
			showDialog("上传失败"+e);
		}

		//更新数据库
		//Log.i("huahua","begin updage");
		user.setnetpic(newName) ;
		user.setUserModifyColumnType("0");

		/*new HhUserModifyPersonInfoYTask(new AbstractYGetTaskListener<User>() {
			@Override
			public void onPostExecute(String name, YGetTask<User> task) {
				//loading.hideLoading();
				userModifyData(task.getValue(),"0");
			}
		},user,"0").execute();*/

	}

	/* 显示Dialog的method */
	private void showDialog(String mess)
	{
		new AlertDialog.Builder(SPCWoDe_ModifyPersonInfo.this).setTitle("Message")
				.setMessage(mess)
				.setNegativeButton("确定",new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface dialog, int which)
					{
					}
				})
				.show();
	}

	public boolean fileIsExists(String filename){
		try{
			File f=new File(filename);
			if(!f.exists()){
				return false;
			}
		}catch (Exception e) {
			// TODO: handle exception
			return false;                }
		return true;
	}


	public  void savePic(Bitmap b,String sdpath) {

		FileOutputStream fos = null;
		try {

			File f = new File(sdpath ,faceimage_file);
			if (f.exists()) {
				f.delete();
			}

			fos = new FileOutputStream(f);

			if (null != fos) {
				b.compress(Bitmap.CompressFormat.PNG, 90, fos);
				fos.flush();
				fos.close();

			}
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
	}


	//异步下载网络图片
	/**
	 * 使用异步任务的规则：
	 * 1、申明的类继承AsyncTask 标注三个参数的类型
	 * 2、第一个参数表示要执行的任务，通常是网络的路径；
	 * 第二个参数表示进度的刻度，
	 * 第三个参数表示任务执行的返回结果
	 *
	 * @author liende
	 *
	 */
	public class MYTask extends AsyncTask<String, Void, Bitmap> {
		/**
		 * 表示任务执行之前的操作
		 */
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			//dialog.show();
		}

		/**
		 * 主要是完成耗时的操作
		 */
		@Override
		protected Bitmap doInBackground(String... arg0) {

			// TODO Auto-generated method stub
			// 使用网络连接类HttpClient类王城对网络数据的提取
			HttpClient httpClient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(arg0[0]);
			//Log.i("huahua","cur link is1 = "+arg0);
			//Log.i("huahua","cur link is = "+arg0[0]);

			Bitmap bitmap = null;
			try {
				HttpResponse httpResponse = httpClient.execute(httpGet);
				if (httpResponse.getStatusLine().getStatusCode() == 200) {
					HttpEntity httpEntity = httpResponse.getEntity();
					byte[] data = EntityUtils.toByteArray(httpEntity);
					bitmap = BitmapFactory
							.decodeByteArray(data, 0, data.length);
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			return bitmap;
		}

		/**
		 * 主要是更新UI的操作
		 */
		@Override
		protected void onPostExecute(Bitmap result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(result==null){
				Log.i("huahua","cur bitmap is null");
				return;
			}

			faceImage.setImageBitmap(result);
			//dialog.dismiss();
		}

	}


	//针对相机来进行 选择
	void bottomwindow(CircleNetworkImage view) {
		if (popupWindow != null && popupWindow.isShowing()) {
			return;
		}
		LinearLayout layout = (LinearLayout) getLayoutInflater().inflate(R.layout.activity_photo_popupwindow1, null);
		popupWindow = new PopupWindow(layout,
				ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.WRAP_CONTENT);
		//点击空白处时，隐藏掉pop窗口
		popupWindow.setFocusable(true);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());
		//添加弹出、弹入的动画
		popupWindow.setAnimationStyle(R.style.Popupwindow);
		int[] location = new int[2];
		view.getLocationOnScreen(location);
		popupWindow.showAtLocation(view, Gravity.LEFT | Gravity.BOTTOM, 0, -location[1]);
		//添加按键事件监听
		setButtonListeners(layout);
		//添加pop窗口关闭事件，主要是实现关闭时改变背景的透明度
		popupWindow.setOnDismissListener(new poponDismissListener());
		backgroundAlpha(1f);
	}

	//针对性别来修改的方式 加载POP窗口
	void bottomSexwindow(TextView view) {
		if (popupWindow != null && popupWindow.isShowing()) {
			return;
		}

		//修改的话，这个地方要修改 *****************
		LinearLayout layout = (LinearLayout) getLayoutInflater().inflate(R.layout.activity_sex_popupwindow, null);
		popupWindow = new PopupWindow(layout,
				ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.WRAP_CONTENT);
		//点击空白处时，隐藏掉pop窗口
		popupWindow.setFocusable(true);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());
		//添加弹出、弹入的动画
		popupWindow.setAnimationStyle(R.style.Popupwindow);
		int[] location = new int[2];
		view.getLocationOnScreen(location);
		popupWindow.showAtLocation(view, Gravity.LEFT | Gravity.BOTTOM, 0, -location[1]);

		//修改的话，这个地方要修改 *****************
		//添加按键事件监听
		setSexButtonListeners(layout);
		//添加pop窗口关闭事件，主要是实现关闭时改变背景的透明度
		popupWindow.setOnDismissListener(new poponDismissListener());
		backgroundAlpha(1f);
	}


	//针对选择生日，从下面弹出窗口进行选择
	void bottombirthdaywindow(TextView view) {
		if (popupWindow != null && popupWindow.isShowing()) {
			return;
		}

		//修改的话，这个地方要修改 *****************
		LinearLayout timepickerview = (LinearLayout) getLayoutInflater().inflate(R.layout.timepicker, null);


		wheelMain = new WheelMain(timepickerview);


		ScreenInfo screenInfo = new ScreenInfo(SPCWoDe_ModifyPersonInfo.this);
		wheelMain.screenheight = screenInfo.getHeight();


		String time = txtbirthday.getText().toString();
		Calendar calendar = Calendar.getInstance();
		if (JudgeDate.isDate(time, "yyyy-MM-dd")) {
			try {
				calendar.setTime(dateFormat.parse(time));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		wheelMain.initDateTimePicker(year, month, day);


		popupWindow = new PopupWindow(timepickerview,
				ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.WRAP_CONTENT);
		//点击空白处时，隐藏掉pop窗口
		popupWindow.setFocusable(true);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());
		//添加弹出、弹入的动画
		popupWindow.setAnimationStyle(R.style.Popupwindow);
		int[] location = new int[2];
		view.getLocationOnScreen(location);
		popupWindow.showAtLocation(view, Gravity.LEFT | Gravity.BOTTOM, 0, -location[1]);

		//修改的话，这个地方要修改 *****************
		//添加按键事件监听
		setBirthdayListeners(timepickerview);
		//添加pop窗口关闭事件，主要是实现关闭时改变背景的透明度
		popupWindow.setOnDismissListener(new poponDismissListener());
		backgroundAlpha(1f);
	}

	//下面是监听 选择 日期，时间
	private void setBirthdayListeners(LinearLayout layout) {

		Button bt_select_birthday_ok = (Button) layout.findViewById(R.id.btn_select_birthday_ok);
		Button bt_select_birthday_cancel = (Button) layout.findViewById(R.id.btn_select_birthday_cancel);


		//ColorStateList whiteColor=getResources().getColorStateList(R.color.popwindowselectok);
		bt_select_birthday_ok.setTextColor(Color.parseColor("#00b2f0"));

		bt_select_birthday_ok.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (popupWindow != null && popupWindow.isShowing()) {
					//在此处添加你的按键处理 xxx

					//Log.i("huahua",wheelMain.getTime());

					popupWindow.dismiss();
					backgroundAlpha(1f);

					loading.showLoading();
					user.setuserbirthday(wheelMain.getTime());
					user.setUserModifyColumnType("3");

					/*new HhUserModifyPersonInfoYTask(new AbstractYGetTaskListener<User>() {
						@Override
						public void onPostExecute(String name, YGetTask<User> task) {
							loading.hideLoading();
							userModifyData(task.getValue(),"3");
						}
					},user,"3").execute();*/

				}
			}
		});
		bt_select_birthday_cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (popupWindow != null && popupWindow.isShowing()) {
					//在此处添加你的按键处理 xxx
					//选照片相
					popupWindow.dismiss();
					backgroundAlpha(1f);

					return;

				}
			}
		});

		bt_select_birthday_cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (popupWindow != null && popupWindow.isShowing()) {
					popupWindow.dismiss();
				}
			}
		});
	}




	private void setButtonListeners(LinearLayout layout) {
		Button camera = (Button) layout.findViewById(R.id.camera);
		Button gallery = (Button) layout.findViewById(R.id.gallery);
		Button savepicture = (Button) layout.findViewById(R.id.savepicture);
		Button cancel = (Button) layout.findViewById(R.id.cancel);

		camera.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (popupWindow != null && popupWindow.isShowing()) {
					//在此处添加你的按键处理 xxx
					popupWindow.dismiss();
					backgroundAlpha(1f);

					//照相
					Intent intentFromCapture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					// 判断存储卡是否可以用，可用进行存储
					if (Tools.hasSdcard()) {

						//String storagePath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "camerademo";
						///   storage/emulated/0  默认路径
						String filepath = Environment.getExternalStorageDirectory().getAbsolutePath();

						Log.i("huahua",filepath);

						File file = new File(filepath, faceimage_file);
						file.mkdirs();

						Uri uri;

						if (Build.VERSION.SDK_INT >= 23) {

							uri = FileProvider.getUriForFile(view.getContext(), "com.zhyq.fileprovider", file);

							int checkCallPhonePermission = ContextCompat.checkSelfPermission(view.getContext(), Manifest.permission.CAMERA);
							if(checkCallPhonePermission != PackageManager.PERMISSION_GRANTED){
								Log.i("huahua","没有授权  高版本");
								//ActivityCompat.requestPermissions(view,new String[]{Manifest.permission.CAMERA},222);

								//提示 摄像头 权限被 限制

								final MyAlertDialog dialog1 = new MyAlertDialog(SPCWoDe_ModifyPersonInfo.this)
										.builder()
										.setTitle("开启相机权限")
										.setMsg("相机权限已经被禁止,请在权限管理中开启，现在设置？")
										.setNegativeButton("取消", new OnClickListener() {
											@Override
											public void onClick(View v) {

											}
										});
								dialog1.setPositiveButton("设置", new OnClickListener() {
									@Override
									public void onClick(View v) {

										Intent intent =new Intent(Settings.ACTION_SETTINGS);

										startActivity(intent);
										return;
									}
								});
								dialog1.show();
								return;
							}



								/*

								List resInfoList = getPackageManager().queryIntentActivities(intentFromCapture, PackageManager.MATCH_DEFAULT_ONLY);
								Iterator resInfoIterator = resInfoList.iterator();
								while (resInfoIterator.hasNext()) {
									ResolveInfo resolveInfo = (ResolveInfo) resInfoIterator.next();
									String packageName = resolveInfo.activityInfo.packageName;
									grantUriPermission(packageName, uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
								}

								*/
							intentFromCapture.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
							//intentFromCapture.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

						} else {
							uri = Uri.fromFile(file);
						}


						intentFromCapture.putExtra( MediaStore.EXTRA_OUTPUT, uri);
						//intentFromCapture.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
						//将拍照结果保存至photo_file的Uri中，不保留在相册中
						startActivityForResult(intentFromCapture,CAMERA_REQUEST_CODE);
					} else {
						ToastUtils.show(view.getContext(),"设备没有SD卡！");
						return;
					}







				}
			}
		});
		gallery.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (popupWindow != null && popupWindow.isShowing()) {
					//在此处添加你的按键处理 xxx
					//选照片相
					popupWindow.dismiss();
					backgroundAlpha(1f);

					// 激活系统图库，选择一张图片
					// 40         Intent intent = new Intent(Intent.ACTION_PICK);
					// 41         intent.setType("image/*");
					// 42         // 开启一个带有返回值的Activity，请求码为PHOTO_REQUEST_GALLERY
					// 43         startActivityForResult(intent, PHOTO_REQUEST_GALLERY);



					Intent intentFromGallery = new Intent(Intent.ACTION_PICK);
					intentFromGallery.setType("image/*"); // 设置文件类型
					//intentFromGallery.setAction(Intent.ACTION_GET_CONTENT);
					startActivityForResult(intentFromGallery,IMAGE_REQUEST_CODE);




					return;

				}
			}
		});
		savepicture.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (popupWindow != null && popupWindow.isShowing()) {
					//在此处添加你的按键处理 xxx
					popupWindow.dismiss();
				}
			}
		});
		cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (popupWindow != null && popupWindow.isShowing()) {
					popupWindow.dismiss();
				}
			}
		});
	}

	/**
	 * 打开文件
	 * 兼容7.0
	 *
	 * @param context     activity
	 * @param file        File
	 * @param contentType 文件类型如：文本（text/html）
	 *                    当手机中没有一个app可以打开file时会抛ActivityNotFoundException
	 */
	public   void startActionFile(Context context, File file, String contentType) throws ActivityNotFoundException {
		if (context == null) {
			return;
		}

		//ACTION_PICK  选择图片
		//ACTION_VIEW  查看图片
		Intent intent = new Intent(Intent.ACTION_PICK);
		intent.addCategory(Intent.CATEGORY_DEFAULT);
		intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
		intent.setFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);//增加读写权限
		intent.setDataAndType(getUriForFile(context, file), contentType);
		if (!(context instanceof Activity)) {
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		}
		startActivityForResult(intent,IMAGE_REQUEST_CODE);
		//context.startActivity(intent);
	}

	public static Uri getUriForFile(Context context, File file) {
		if (context == null || file == null) {
			throw new NullPointerException();
		}
		Uri uri;
		if (Build.VERSION.SDK_INT >= 23) {
			Log.i("huahua","高版本 ");
			uri = FileProvider.getUriForFile(context.getApplicationContext(), "com.zhyq.fileprovider", file);
		} else {
			uri = Uri.fromFile(file);
		}
		return uri;
	}




	//相应选择性别的窗口  的CLICK事件
	//            0  修改头像
	//            1 修改昵称
	//            2 修改性别
	//            3 修改生日
	//            4 修改收货日期
	private void setSexButtonListeners(LinearLayout layout) {



		Button bt_boy = (Button) layout.findViewById(R.id.btn_sex_boys);
		Button bt_girl = (Button) layout.findViewById(R.id.btn_sex_girl);
		Button bt_cancel = (Button) layout.findViewById(R.id.btn_sex_cancel);

		bt_boy.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (popupWindow != null && popupWindow.isShowing()) {
					//在此处添加你的按键处理 xxx
					popupWindow.dismiss();
					backgroundAlpha(1f);
					//照相 选择了男生
					loading.showLoading();
					user.setUserSex("男");
					user.setUserModifyColumnType("2");

					/*new HhUserModifyPersonInfoYTask(new AbstractYGetTaskListener<User>() {
						@Override
						public void onPostExecute(String name, YGetTask<User> task) {
							loading.hideLoading();
							userModifyData(task.getValue(),"2");
						}
					},user,"2").execute();*/

					return;
				}

			}
		});
		bt_girl.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (popupWindow != null && popupWindow.isShowing()) {
					//在此处添加你的按键处理 xxx
					popupWindow.dismiss();
					backgroundAlpha(1f);
					//照相 选择了男生
					//User user = new User();
					loading.showLoading();
					user.setCode(usercode);
					user.setUserSex("女");
					user.setUserModifyColumnType("2");

					/*new HhUserModifyPersonInfoYTask(new AbstractYGetTaskListener<User>() {
						@Override
						public void onPostExecute(String name, YGetTask<User> task) {
							loading.hideLoading();
							userModifyData(task.getValue(),"2");
						}
					},user,"2").execute();*/

					return;

				}
			}
		});
		bt_cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (popupWindow != null && popupWindow.isShowing()) {
					popupWindow.dismiss();
				}
			}
		});
	}




	//修改人员信息即可偶
	private void userModifyData(User user,String modifyType) {
		if (user==null) {
			ToastUtils.show(this, R.string.show_logon_error_msg);
		} else {
			//Log.i("huahua","onClick3");
			HhApplication.getInstance().getHhCart().setUser(user);
			//AndroidUtils.start(this, SelectShopActivity.class);
			String loginid="";
			loginid = user.getCode();
			PreferenceUtils.setString(this,"userlogin",loginid);


			//修改性别
			if(modifyType.equals("2")){
				String sex="";
				sex = user.getUserSex();
				AndroidUtils.setTextView(this, R.id.spc_modify_personinfo_sex,sex);
			}

			//修改生日
			if(modifyType.equals("3")){
				String tbirthday="";
				tbirthday = user.getuserbirthday();
				txtbirthday.setText(tbirthday);
				//AndroidUtils.setTextView(this, R.id.hh_modify_personinfo_sex,tbirthday);
			}
			if(modifyType.equals("0")){
				String pic="";
				pic = user.getnetpic();


				View v1 = this.findViewById(R.id.spc_modify_personinfo_header);
				AndroidUtils.setWebImageCircleView(v1, R.id.spc_modify_personinfo_header,pic);
				Log.i("spc","reload image "+pic);
				//AndroidUtils.setTextView(this, R.id.spc_modify_personinfo_sex,sex);
			}

		}
	}




	/**
	 * 设置添加屏幕的背景透明度
	 * @param bgAlpha
	 */
	public void backgroundAlpha(float bgAlpha)
	{
		WindowManager.LayoutParams lp = getWindow().getAttributes();
		lp.alpha = bgAlpha; //0.0-1.0
		getWindow().setAttributes(lp);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

       /*
        *    WindowManager.LayoutParams lp = activity.getWindow().getAttributes();  
        lp.alpha = bgAlpha;  
        if (bgAlpha == 1) {  
            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);//不移除该Flag的话,在有视频的页面上的视频会出现黑屏的bug  
        } else {  
            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);//此行代码主要是解决在华为手机上半透明效果无效的bug  
        }  
        activity.getWindow().setAttributes(lp);
        */


	}


	/**
	 * 返回或者点击空白位置的时候将背景透明度改回来
	 */
	class poponDismissListener implements PopupWindow.OnDismissListener{

		@Override
		public void onDismiss() {
			// TODO Auto-generated method stub
			new Thread(new Runnable(){
				@Override
				public void run() {
					//此处while的条件alpha不能<= 否则会出现黑屏
					while(alpha<1f){
						try {
							Thread.sleep(4);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						Log.d("HeadPortrait","alpha:"+alpha);
						Message msg =mHandler.obtainMessage();
						msg.what = 1;
						alpha+=0.01f;
						msg.obj =alpha ;
						mHandler.sendMessage(msg);
					}
				}

			}).start();
		}

	}

	Handler mHandler = new Handler(){
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what){
				case 1:
					backgroundAlpha(alpha);
					//backgroundAlpha((float)msg.obj);
					break;
			}
		}
	};



	//下面是弹出窗口选择 日期
	public void modifuBirthDay(){


		LayoutInflater inflater = LayoutInflater.from(SPCWoDe_ModifyPersonInfo.this);
		final View timepickerview = inflater.inflate(R.layout.timepicker,null);
		ScreenInfo screenInfo = new ScreenInfo(SPCWoDe_ModifyPersonInfo.this);


		//Log.i("huahua","modify birthday2");

		wheelMain = new WheelMain(timepickerview);

		wheelMain.screenheight = screenInfo.getHeight();
		String time = txtbirthday.getText().toString();


		//Log.i("huahua","modify birthday3");
		Calendar calendar = Calendar.getInstance();
		if (JudgeDate.isDate(time, "yyyy-MM-dd")) {
			try {
				calendar.setTime(dateFormat.parse(time));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		int day = calendar.get(Calendar.DAY_OF_MONTH);

		//Log.i("huahua","wheelMain");

		wheelMain.initDateTimePicker(year, month, day);
		new AlertDialog.Builder(SPCWoDe_ModifyPersonInfo.this)
				.setTitle("选择时间")
				.setView(timepickerview)
				.setPositiveButton("确定",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
												int which) {
								txtbirthday.setText(wheelMain.getTime());
							}
						})
				.setNegativeButton("取消",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
												int which) {
							}
						}).show();

	}

	//下面是校验 登入密码是否正确
	private void verifyLogPassword(String password ){

		User curUser = HhApplication.getInstance().getHhCart().getUser();


	}

	private void updateData(SysPassValue dbResult){
		String returnValue="";

		returnValue = dbResult.getArg1();




	}
	public void showMyDialog(){

		/*if(dialog==null){
			dialog = new CustomProgressDialog(this, "正在加载中",R.anim.spinner);
		}else{
			dialog.reLoad();
		}
		dialog.show();
		Window dialogWindow = dialog.getWindow();
		WindowManager.LayoutParams lp = dialogWindow.getAttributes();
		dialogWindow.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
		dialogWindow.setGravity(Gravity.BOTTOM);

		//显示的坐标
		Button bt_reload_data = (Button) dialog.findViewById(R.id.bt_reload_data);
		bt_reload_data.setOnClickListener(this);

		//得到状态栏的高度

		int statusBarHeight = AndroidUtils.getStatusBarHeight(this.getApplicationContext());
		WindowManager windowManager = getWindowManager();
		Display display = windowManager.getDefaultDisplay();
		//dialog的大小
		lp.width = (int)(display.getWidth()); //设置宽度
		//lp.width = width;
		//注意： 高度是  39.9dp + 状态栏的高度
		int height =  (int)(display.getHeight());
		int headerheight = AndroidUtils.dip2px(this.getApplicationContext(),39.9f) + statusBarHeight;
		lp.height = height - headerheight;
		dialogWindow.setAttributes(lp);*/
	}
	public void closeMyDialog(){
		/*new Handler().postDelayed(new Runnable() {//延迟
			@Override
			public void run() {
				dialog.hide();
			}
		}, 200);*/
	}
}


