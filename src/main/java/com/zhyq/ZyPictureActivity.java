package com.zhyq;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.util.common.StrEdit;
import com.zhyq.libs.PreferenceUtils;
import com.zhyq.libs.ProgersssDialog;
import com.zhyq.libs.ToastUtils;
import com.zhyq.libs.iosdialog.widget.MyAlertDialog;
import com.zhyq.model.SysPassValue;
import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.sendOrderToServerForValueYTask;
import com.zhyq.widget.LoadingRelativeLayout;

import java.util.ArrayList;
import java.util.List;


public class ZyPictureActivity extends Activity implements OnClickListener {

	public ProgersssDialog dialog;

	private String userCode = "";
	private String userName = "";
	private Long userId = null;
	private int id = 0;

	private LoadingRelativeLayout loading = null;
	public int resultCode = 0;   //回传数据使用
	public static boolean active = false;               //回传数据使用



	private View  includeTitle ;

	private Button  returnButton;
	private Button  saveButton;


	private EditText  et_name=null;
	private String    old_name="";  //修改昵称
	private Button    bt_clear=null;

	private String name = "";
	private String type = "";
	private String channelName = "";

	private List<LocalMedia> selectList = new ArrayList<>();
	private int maxSelectNum = 9;
	private int chooseMode = PictureMimeType.ofAll();




	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		showProgressDialog();

		this.setContentView(R.layout.activity_my_editname);

		loading = LoadingRelativeLayout.getLoadingRelativeLayout(this);

		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		includeTitle = this.findViewById(R.id.my_edit_personinfo_head);

		//resultCode = StrEdit.StrToInt(this.getIntent().getStringExtra("resultCode"));//接收用来返回给onActivityResult里判断类型
		//name = this.getIntent().getStringExtra("name");
		//type = this.getIntent().getStringExtra("type");
		if(type==null){
			type = "";
			ToastUtils.show(this, "参数错误！");
		}
		if(type.equals("nickname")){
			channelName = "修改昵称";
		}else if(type.equals("address")){
			channelName = "修改地址";
		}
		Log.i("luktel","channelName"+channelName);

		((TextView)includeTitle.findViewById(R.id.tv_head_save_title)).setText(channelName);

		returnButton = (Button)includeTitle.findViewById(R.id.bt_head_save_return);
		returnButton.setOnClickListener(this);

		saveButton = (Button)this.findViewById(R.id.bt_head_right_save);
		saveButton.setOnClickListener(this);

		et_name = (EditText)this.findViewById(R.id.et_my_personal_input_name);
		bt_clear = (Button)this.findViewById(R.id.bt_my_personal_input_clear);
		bt_clear.setOnClickListener(this);

		et_name.setText(name);
		old_name = name;

		new Handler().postDelayed(new Runnable() {//延迟读取数据
			@Override
			public void run() {
				dialog.dismiss();
			}
		}, 200);

	}

	public void showProgressDialog(){
		dialog = new ProgersssDialog(this);
		//dialog.show();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
				case PictureConfig.CHOOSE_REQUEST:
					// 图片选择结果回调
					selectList = PictureSelector.obtainMultipleResult(data);
					// 例如 LocalMedia 里面返回三种path
					// 1.media.getPath(); 为原图path
					// 2.media.getCutPath();为裁剪后path，需判断media.isCut();是否为true
					// 3.media.getCompressPath();为压缩后path，需判断media.isCompressed();是否为true
					// 如果裁剪并压缩了，已取压缩路径为准，因为是先裁剪后压缩的
					for (LocalMedia media : selectList) {
						Log.i("luktel","图片-----》"+media.getPath());//参考ZyMyPersonal的new FileupThread(ls_imagename,ls_face).start();
					}
					//adapter.setList(selectList);
					//adapter.notifyDataSetChanged();
					break;
			}
		}
	}

	@Override
	public void onClick(View view) {
		if (R.id.bt_head_right_save==view.getId()) {
			Commit();
			return;
		} else if(R.id.bt_head_save_return==view.getId()){//https://blog.csdn.net/qq_35373333/article/details/76577915

			/*PictureSelector.create(ZyPictureActivity.this)
					.openGallery(PictureMimeType.ofImage())
					.forResult(PictureConfig.CHOOSE_REQUEST);*/

			PictureSelector.create(ZyPictureActivity.this)
					.openCamera(PictureMimeType.ofImage())
					.forResult(PictureConfig.CHOOSE_REQUEST);

			return;

		}else if(R.id.bt_my_personal_input_clear==view.getId()){
			et_name.setText("");
			return;
		}


	}

	private void Commit(){

		loading.showLoading();

		Long userId = PreferenceUtils.getLong(this, "userid", -1);
		String usercode = PreferenceUtils.getString(this,"usercode","");

		//String new_input_name = AndroidUtils.getEditText(this, R.id.et_my_personal_input_name);
		String new_input_name = this.et_name.getText().toString();
		if(new_input_name.equals("")){
			loading.hideLoading();
			ToastUtils.show(this,"不能为空！");
			return;
		}
		SysPassValue temp = new SysPassValue();
		if(type.equals("nickname")){
			temp.setFunctionName("modifyNickName");
		}else if(type.equals("address")){
			temp.setFunctionName("modifyAddress");
		}
		temp.setUserId(String.valueOf(userId));
		temp.setArg1(new_input_name);

		new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
			@Override
			public void onPostExecute(String name, YGetTask<SysPassValue> task) {
				loading.hideLoading();
				getData(task.getValue());
			}
		},temp).execute();

		return;
	}

	private void getData(SysPassValue passValue) {
		if (passValue==null) {
			ToastUtils.show(this, "数据错误，修改失败！");
			return;
		} else {

			String returnResult = passValue.getReturnResult();
			if(returnResult.equals("1")){

				String newName = this.et_name.getText().toString();

				if(type.equals("nickname")){
					Intent intent = new Intent(this,ZyPictureActivity.class);
					intent.putExtra("newName", newName);
					setResult(resultCode, intent);//对应上个页面的onActivityResult里面的resultCode，结果标识
					overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
					this.finish();
					return;
				}else if(type.equals("address")){
					Intent intent = new Intent(this,ZyPictureActivity.class);
					intent.putExtra("newName", newName);
					setResult(resultCode, intent);//对应上个页面的onActivityResult里面的resultCode，结果标识
					overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
					this.finish();
					return;
				}

			}else{

				ToastUtils.show(this, "修改失败！");
				return;

			}

		}
	}


}
