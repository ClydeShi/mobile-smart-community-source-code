package com.zhyq;

 
 

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
 
import com.handmark.pulltorefresh.library.ILoadingLayout;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
 
 
import com.zhyq.libs.AndroidUtils;
import com.zhyq.libs.PreferenceUtils;


import com.zhyq.model.SysPassValue;


import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
 
import com.zhyq.task.impl.sendOrderToServerForListYTask;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
 
 
import android.os.Bundle;
 
 
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

/*
 * 目前测试这个文件
 * http://blog.csdn.net/jifashihan/article/details/50512753  http://blog.csdn.net/sinstar1/article/details/69942460  http://blog.csdn.net/nupt123456789/article/details/39432781
 * 选择后翻页，也有的会选住，目前没有解决
 */

public class WgPlanSelectUserActivityTest extends Activity implements OnItemClickListener,OnClickListener {
	
	private View includeTitle;
	private PullToRefreshListView mPullRefreshListView;//扩展的ListView
	private int currentPage = 1; 
	private String lastRefreshdt="";
    private WgSingleSelectAdapter mAdapter;//数据适配器
    private List<SysPassValue> NewsList = new ArrayList<SysPassValue>();//数据
    
    private int orderPosition = 0;
    private ArrayList<Integer> orderCheckIconList = new ArrayList<Integer>();
    private boolean isShowDeleteIcon=false;
    List<String> chooseIdList = new ArrayList<String>();
    
    ListView actualListView;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.setContentView(R.layout.activity_wg_multiselect);
		includeTitle = this.findViewById(R.id.wg_include_singleselect_head);
		Button btreturn = (Button)includeTitle.findViewById(R.id.wg_head_btn_return);
		btreturn.setOnClickListener(this);
		Button btconfirm = (Button)includeTitle.findViewById(R.id.wg_head_btn_confirm);
		btconfirm.setOnClickListener(this);
		((TextView)includeTitle.findViewById(R.id.wg_head_title)).setText("选择检查人员");
		
		((Button)this.findViewById(R.id.wg_singleselect1_btn)).setOnClickListener(this);
		
		/*
		 *  初始化数据适配器，传递Content 过去
		 */
		mAdapter = new WgSingleSelectAdapter(this, NewsList);
		mAdapter.setOnClick1(this);
		
		/*
         * 初始化 
         */  
		mPullRefreshListView = (PullToRefreshListView) this.findViewById(R.id.xl_wg_singleselect_PulllistView);
		mPullRefreshListView.setMode(Mode.BOTH);
		//初始化值
		initIndicator();
		//设置下拉，上移动 得到效果
        mPullRefreshListView.setOnRefreshListener(new OnRefreshListener2<ListView>()
        {
        	//下拉效果
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView)
            {
            	refreshView.getLoadingLayoutProxy().setPullLabel("下拉刷新...");// 刚下拉时，显示的提示
            	refreshView.getLoadingLayoutProxy().setRefreshingLabel("加载中...");// 刷新时
            	refreshView.getLoadingLayoutProxy().setReleaseLabel("松开刷新...");// 下来达到一定距离时，显示的提示
                
        	   if(lastRefreshdt.equals("")) {
        	    	// 显示最后更新的时间
                   refreshView.getLoadingLayoutProxy().setLastUpdatedLabel("刚刚");
       	       } else {
       	    	// 显示最后更新的时间
                   refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(lastRefreshdt);
       	       }
        	   
        	   SimpleDateFormat    formatter    =   new    SimpleDateFormat    ("MM月dd日    HH:mm:ss     "); 
       	       Date    curDate    =   new    Date(System.currentTimeMillis());//获取当前时间
       	       lastRefreshdt =    formatter.format(curDate);
       	       
                //加载任务
                currentPage = 1;
                GetData(currentPage);
            }
            
            //上拉效果
            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView)
            {
                //加载任务
            	refreshView.getLoadingLayoutProxy().setPullLabel("上拉刷新...");// 刚上拉时，显示的提示
            	refreshView.getLoadingLayoutProxy().setRefreshingLabel("加载中...");// 刷新时
            	refreshView.getLoadingLayoutProxy().setReleaseLabel("松开刷新...");// 上来达到一定距离时，显示的提示
                currentPage++;
                GetData(currentPage);
            }
        });
        
        for(int i = 0;i<100;i++){
            //orderCheckIconList.add(R.drawable.bt_fk_xz_select3x);//未选择
            orderCheckIconList.add(0);//未选择
        }
        
		//获取控件并注册  这个是获取 listview 控件
        actualListView = mPullRefreshListView.getRefreshableView();
        
        actualListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });
        
        registerForContextMenu(actualListView);
        actualListView.setAdapter(mAdapter);
        
        //mPullRefreshListView.setAdapter(mAdapter);
        onRefresh();
	}
	
	private void onRefresh(){
		mPullRefreshListView.setRefreshing(true);
        //首次加载任务数据 加载任务  
        currentPage = 1;
        GetData(currentPage);
	}
	
	private void initIndicator()
	{
		ILoadingLayout startLabels = mPullRefreshListView.getLoadingLayoutProxy(true,false);
		
		//startLabels.setTextTypeface()
		startLabels.setPullLabel("下拉刷新...");// 刚下拉时，显示的提示
		startLabels.setRefreshingLabel("加载中...");// 刷新时
		startLabels.setReleaseLabel("松开刷新...");// 下来达到一定距离时，显示的提示
		
		ILoadingLayout endLabels = mPullRefreshListView.getLoadingLayoutProxy(false,true);
		endLabels.setPullLabel("上拉刷新...");// 刚上拉时，显示的提示
		endLabels.setRefreshingLabel("加载中...");// 刷新时
		endLabels.setReleaseLabel("松开刷新...");// 上来达到一定距离时，显示的提示
		endLabels.setLastUpdatedLabel("");
	}
	
	//onResume是在启动activity启动之后才能执行的，也就是恢复执行。程序正常启动：onCreate()->onStart()->onResume();
	@Override
	protected void onResume() {   //注意 该函数，如果在打开了其他窗口，再关闭，这个函数也会执行
		super.onResume();
		
	}
	
	public void GetData(int curpage){
		
		Long userId = PreferenceUtils.getLong(this, "userid", -1);
		String usercode = PreferenceUtils.getString(this,"usercode","");
		
		SysPassValue temp = new SysPassValue();
		temp.setFunctionName("getUserList");
		temp.setUserId(String.valueOf(userId));
		temp.setArg1("1");
		temp.setPageNumber(String.valueOf(curpage));
		
		//调用接口得到数据
		new sendOrderToServerForListYTask(new AbstractYGetTaskListener<List<SysPassValue>>() {
			@Override
			public void onPostExecute(String name, YGetTask<List<SysPassValue>> task) {
				
				showData(task.getValue());
				mPullRefreshListView.onRefreshComplete();
			}
		},temp).execute();
		
	}
	
	public void showData(List<SysPassValue> list) {
		
		//通过数据适配器获取 数据列表
		View vnorecord = this.findViewById(R.id.activity_no_record_showpic);
		if (list==null){
			if(currentPage==1){
				this.NewsList.clear(); 
	    		mAdapter.notifyDataSetChanged();
	    		vnorecord.setVisibility(View.VISIBLE);
	    		return;
			}
			return;
		}
		vnorecord.setVisibility(View.GONE);
		
		//如果是第一页则需要清楚数据   currentPage
		if(currentPage==1){
    		this.NewsList.clear();
			this.NewsList.addAll(list);
    	} else{
			NewsList.addAll(list);
		}
		mAdapter.notifyDataSetChanged();
		if(NewsList.size()==0){
			vnorecord.setVisibility(View.VISIBLE);
		}
		
	}
	
	@Override
	public void onClick(View view) {
		
		 if(R.id.wg_head_btn_return==view.getId()){
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		  }	
		  
		 if(R.id.wg_singleselect1_btn==view.getId()){
			 Intent intent = new Intent(this,WgPlanAdd.class);
	         intent.putExtra("name", "选择检查员");
	         setResult(5, intent);
	         overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			 this.finish();
			 return;
		 }
		 
		 if(R.id.wg_head_btn_confirm==view.getId()){
			 
			 for(int i = 0;i < chooseIdList.size(); i ++){
				 Log.i("longhua","获取到"+chooseIdList.get(i));
			 }
			 /*
			 Intent intent = new Intent(this,WgPlanAdd.class);
	         intent.putExtra("name", "选择检查员");
	         setResult(5, intent);
	         overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			 this.finish();
			 return;
			 */
		 }
		 
		 /*if(R.id.wg_wode_singleselect3_btn==view.getId()){
			 
			 Object tag = view.getTag();
			 
			 if (tag != null) {
				 int position = (Integer) tag;
				 String id =  NewsList.get(position).getArg1();
				 String name =  NewsList.get(position).getArg2();
				 
				 Log.i("longhua","id"+name);
				 
				 ImageView viewHolder2 = (ImageView)view.findViewById(R.id.wg_singleselect_layout3_yixuan_img);
				 
				 final ImageView finalViewHolder = viewHolder2;
				 
				 if(orderCheckIconList.get(position)==0){//选择图标
	                	chooseIdList.add(name);
	                    orderCheckIconList.set(position, 1);//选择图标
	                    //finalViewHolder.setImageResource(R.drawable.bt_fk_x3x);
	                }else{//取消选择
	                	chooseIdList.remove(name);
	                    orderCheckIconList.set(position, 0);
	                    //finalViewHolder.setImageResource(R.drawable.bt_fk_xz_select3x);
	                }
				 
				 boolean isComment = false;
				 WgSingleSelectAdapter.ViewHolder holder = (WgSingleSelectAdapter.ViewHolder) view.getTag();
				 if(!isComment){
					 isComment = true;
					 holder.imgselect.setImageResource(R.drawable.bt_fk_x3x);
				 }else{
					 isComment = false;
					 holder.imgselect.setImageResource(R.drawable.bt_fk_xz_select3x);
				 }*/
				 
				 
				 
				 
				 
				 /*				
				 Intent intent=new Intent(this, WgPlanAdd.class);
				 Bundle bundle=new Bundle();
				 bundle.putString("id", id);
				 bundle.putString("name", name);
				 intent.putExtras(bundle);
				 setResult(5,intent);
				 overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				 this.finish();
				 return;
				 */
			 //}
		 //}
	}
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		
		Log.i("longhua","onItemClick");
		if(arg3 == -1) {
	        // 点击的是headerView或者footerView
	        return;
	    }
		//arg2是当前item的ID。这个id根据你在适配器中的写法可以自己定义
		//ToastUtils.show(arg1.getContext(), R.string.show_logon_error_msg)
		//Object tag2 = arg1.getTag(); 
        int position = (Integer) arg2;
        position = position -1;
		return;
		
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	private void updateProgressPartly(int position){
		
		Log.i("longhua","position："+position);
		
        int firstVisiblePosition = actualListView.getFirstVisiblePosition();  
        int lastVisiblePosition = actualListView.getLastVisiblePosition();  
        if(position>=firstVisiblePosition && position<=lastVisiblePosition){  
            View view = actualListView.getChildAt(position - firstVisiblePosition);
            
            //Object tag = view.getTag();
            //if (position != null) {
            	
            	//int position = (Integer) tag;
				 String id =  NewsList.get(position).getArg1();
				 String name =  NewsList.get(position).getArg2();
				 
				 Log.i("longhua","id"+name);
            	
            	int position2 = position;
            	
            	String name2 =  NewsList.get(position2).getArg2();
            	
            	Log.i("longhua","name2"+name2);
            	
            	ImageView viewHolder2 = (ImageView)view.findViewById(R.id.wg_singleselect_layout3_yixuan_img);
            	
            	Log.i("longhua","id"+orderCheckIconList.get(position2));
            	Log.i("longhua","position"+position2);
                if(orderCheckIconList.get(position2)==0){//选择图标
                	chooseIdList.add(name);
                    orderCheckIconList.set(position2, 1);//选择图标
                    viewHolder2.setImageResource(R.drawable.bt_fk_x3x);
                }else{//取消选择
                	chooseIdList.remove(name);
                    orderCheckIconList.set(position2, 0);
                    viewHolder2.setImageResource(R.drawable.bt_fk_xz_select3x);
                }
           
        }  
    }
	
	public class WgSingleSelectAdapter extends BaseAdapter {
		private List<SysPassValue> list=null;
		private LayoutInflater mInflater;
		
		public WgSingleSelectAdapter(List<SysPassValue> list) {
			super();
			this.list = list;
		}
		
		public WgSingleSelectAdapter(Context context, List<SysPassValue> datas)
	    {
	        this.list = datas;
	        mInflater = LayoutInflater.from(context);
	       // imageLoader.init(ImageLoaderConfiguration.createDefault(context));
	    }
		
		@Override
		public int getCount() {
			return list.size();
		}

		@Override
		public Object getItem(int position) {
			return list.get(position);
		}

		@Override
		public long getItemId(int position) {
			return Long.valueOf(list.get(position).getId());
		}
		
		private View.OnClickListener onClick1;
		
		
		public void setOnClick1(View.OnClickListener onClick1){
	        this.onClick1 = onClick1;
		}
		
		public class ViewHolder {  
			TextView textname;
            ImageView imgselect;
            Button btnselect;
	    }
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = null;
			if (convertView==null) {
				convertView=mInflater.inflate(R.layout.item_wg_singleselect_list, null);
			}
			
			SysPassValue datagrid=(SysPassValue)list.get(position);
			
			final int position2 = position;
			
			//convertView.setTag(datagrid);
			
			String sid =  datagrid.getId();
			String arg1 =  datagrid.getArg1();
			String arg2 =  datagrid.getArg2();
			String arg3 =  datagrid.getArg3();
			String arg4 =  datagrid.getArg4();
			String arg5 =  datagrid.getArg5();
			String arg6 =  datagrid.getArg6();
			String arg7 =  datagrid.getArg7();
			
			holder = new ViewHolder();
			
			ImageView viewHolder2 = (ImageView)convertView.findViewById(R.id.wg_singleselect_layout3_yixuan_img);
			
			Button bviewHolder2 = ((Button) convertView.findViewById(R.id.wg_wode_singleselect3_btn));
			
			final ImageView finalViewHolder = viewHolder2;
			final String name = arg2;
			bviewHolder2.setOnClickListener(new View.OnClickListener() {//这里定义了的话，下面要取消设置监听
	            @Override
	            public void onClick(View v) {
	            	
	            	Log.i("longhua","position"+position2);
	            	
	            	/*Log.i("longhua","id"+orderCheckIconList.get(position2));
	            	Log.i("longhua","position"+position2);
	                if(orderCheckIconList.get(position2)==R.drawable.bt_fk_xz_select3x){//没有选择图标
	                	chooseIdList.add(name);
	                    orderCheckIconList.set(position2, R.drawable.bt_fk_x3x);//选择图标
	                }else{
	                	chooseIdList.remove(name);
	                    orderCheckIconList.set(position2, R.drawable.bt_fk_xz_select3x);
	                }
	                
	                finalViewHolder.setImageResource(orderCheckIconList.get(position2));*/
	                
	            	Log.i("longhua","id"+orderCheckIconList.get(position2));
	            	Log.i("longhua","position"+position2);
	                if(orderCheckIconList.get(position2)==0){//选择图标
	                	chooseIdList.add(name);
	                    orderCheckIconList.set(position2, 1);//选择图标
	                    finalViewHolder.setImageResource(R.drawable.bt_fk_x3x);
	                }else{//取消选择
	                	chooseIdList.remove(name);
	                    orderCheckIconList.set(position2, 0);
	                    finalViewHolder.setImageResource(R.drawable.bt_fk_xz_select3x);
	                }
	            	
	            	//updateProgressPartly(position2);
	                
	            }
	        });
			
			holder.textname = (TextView) convertView.findViewById(R.id.wg_transferact_layout3_textview3);
			holder.imgselect = (ImageView) convertView.findViewById(R.id.wg_singleselect_layout3_yixuan_img);
			holder.btnselect = ((Button) convertView.findViewById(R.id.wg_wode_singleselect3_btn));
			
			
			
			
			AndroidUtils.setTextView(convertView, R.id.wg_transferact_layout3_textview3,arg2);			
			//((Button) convertView.findViewById(R.id.wg_wode_singleselect3_btn)).setTag(R.id.button_tag1,position);			
			//((Button) convertView.findViewById(R.id.wg_wode_singleselect3_btn)).setTag(position);//button添加一个setTag方法，通过setTag方法传入一个position索引值
			//((Button) convertView.findViewById(R.id.wg_wode_singleselect3_btn)).setOnClickListener(onClick1);
			
			
			convertView.setTag(holder);
			
			//convertView.setTag(datagrid);
			
			return convertView;
		}
		
		public void addAll(List<SysPassValue> mDatas){
			this.list.clear();
			this.list.addAll(mDatas);    
			this.notifyDataSetChanged();
		}
		
		
		
		 
	}
	
	
	
}
