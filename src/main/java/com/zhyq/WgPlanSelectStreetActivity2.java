package com.zhyq;

 
 

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import com.zhyq.widget.LoadingRelativeLayout;


import android.app.Activity;
import android.content.Intent;
 
 
import android.os.Bundle;


import android.util.SparseBooleanArray;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

/*
 * 使用ListView实现多选功能，选择后背景色改变 item_wg_multiselect_listview需要配合com.luktel.libs.CheckableLinearLayout使用
 */

public class WgPlanSelectStreetActivity2 extends Activity implements OnClickListener {
	
	private LoadingRelativeLayout loading = null;
	private View includeTitle;
	private ListView lv_data;
	private Button bt;
	private boolean state = true;
	List<String> chooseIdList = new ArrayList<String>();
	private String[] sid = { "1", "2", "3", "4", "5", "6" };
	private String[] name = { "龙华", "民治", "观湖", "福城", "观澜", "大浪" };
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wg_multiselect_listview);
		lv_data = (ListView) findViewById(R.id.lv_data);
		bt = (Button) findViewById(R.id.wg_singleselect2_btn);
		
		loading = LoadingRelativeLayout.getLoadingRelativeLayout(this);
		includeTitle = this.findViewById(R.id.wg_include_singleselect_head);
		Button btreturn = (Button)includeTitle.findViewById(R.id.wg_head_btn_return);
		btreturn.setOnClickListener(this);
		Button btconfirm = (Button)includeTitle.findViewById(R.id.wg_head_btn_confirm);
		btconfirm.setOnClickListener(this);
		((TextView)includeTitle.findViewById(R.id.wg_head_title)).setText("选择街道");
		((Button)this.findViewById(R.id.wg_singleselect1_btn)).setOnClickListener(this);
		
		final List<Map<String, Object>> listems = new ArrayList<Map<String, Object>>();
		
		for (int i = 0; i < name.length; i++) {
            Map<String, Object> listem = new HashMap<String, Object>();
            listem.put("sid", sid[i]);
            listem.put("name", name[i]);
            listems.add(listem);
        }
		
		lv_data.getCheckedItemIds();
		SimpleAdapter adapter = new SimpleAdapter(this, listems,
				R.layout.item_wg_multiselect_listview, new String[] { "name" },
				new int[] { R.id.tv }) {
			@Override
			public boolean hasStableIds() {
				return true;
			}
			
			@Override
			public long getItemId(int position) {
				return position;
			}
		};
		lv_data.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		lv_data.setAdapter(adapter);
		
		bt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				for (int i = 0; i < lv_data.getAdapter().getCount(); i++) {
					lv_data.setItemChecked(i, state);
				}
				state = !state;
			}
		});
		
		lv_data.addFooterView(new View(this));
		//System.out.println(((HeaderViewListAdapter)lv_data.getAdapter()).getWrappedAdapter().getCount());
		lv_data.setOnItemClickListener(new OnItemClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if (ListView.CHOICE_MODE_MULTIPLE == lv_data.getChoiceMode()) {
					/*HashMap<String, String> item = (HashMap<String, String>) lv_data.getItemAtPosition(position);
					Toast.makeText(
							WgPlanSelectStreetActivity.this,
							"当前为多选模式\n选中的条数：" + lv_data.getCheckedItemCount()
									+ "\n" + "点击的数据：" + item.get("item"), 2)
							.show();
					
					long[] ids = lv_data.getCheckedItemIds();
					for (int i = 0; i < ids.length; i++) {
						Log.i("longhua", ids[i]+"data");//如果选中了前四条，打印：100，101，102，103;由前面的adapter getItemId()返回的
					}*/
					
					chooseIdList.clear();
					SparseBooleanArray array = lv_data.getCheckedItemPositions();
					long[] ids = lv_data.getCheckedItemIds();
	                for (int i = 0; i < listems.size(); i++) {
	                	for (int j = 0; j < ids.length; j++) {
	                		if (Integer.valueOf(ids[j]+"") == i) {
	                			chooseIdList.add(String.valueOf(listems.get(i).get("name")));
	                			//Log.i("longhua", listems.get(i)+"");
		                    	//Log.i("longhua", listems.get(i).get("sid")+"");
	                			//Log.i("longhua", listems.get(i).get("name")+"");
		                        //Log.i("longhua", "value:"+lv_data.getAdapter().getItemId((array.keyAt(i))));
	                		}
	                	}
	                }
	 
				}
			}
		});
	}
	
	@Override
	public void onClick(View view) {
		
		if(R.id.wg_head_btn_return==view.getId()){
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		}
		
		if(R.id.wg_singleselect1_btn==view.getId()){
			
			Intent intent = new Intent(this,WgPlanAdd.class);
	        intent.putExtra("name", "全部");
	        setResult(10, intent);
	        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			this.finish();
			return;
		}
		
		if(R.id.wg_head_btn_confirm==view.getId()){
			
			String street = "";
			for(int i = 0;i < chooseIdList.size(); i ++){
				if(street.equals("")){
					street = chooseIdList.get(i);
				}else{
					street = street + "," + chooseIdList.get(i);
				}
			 }
			
			Intent intent = new Intent(this,WgPlanAdd.class);
	        intent.putExtra("name", street);
	        setResult(10, intent);
	        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			this.finish();
			return;
		}
		 
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}
	
}
