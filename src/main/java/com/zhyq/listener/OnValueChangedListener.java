package com.zhyq.listener;

import android.view.View;

public interface OnValueChangedListener {
	public void onValueChanged(View source,Object value);
}
