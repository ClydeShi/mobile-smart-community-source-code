package com.zhyq;

 
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
  
import com.handmark.pulltorefresh.library.ILoadingLayout;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.zhyq.adapter.WgFeedbackListAdapter;

import com.zhyq.model.SysPassValue;
import com.zhyq.model.User;

import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.sendOrderToServerForListYTask;
import com.zhyq.widget.LoadingRelativeLayout;

import android.app.Activity;
import android.content.Intent;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;


public class WgFeedbackActivity extends Activity implements OnItemClickListener,OnClickListener {
	
	private LoadingRelativeLayout loading = null;
	private View includeTitle;
	public static final int request_mycollection_result = 1;//回传的请求码
	private PullToRefreshListView mPullRefreshListView;//扩展的ListView
	private String lastRefreshdt = "";
	private int position = 0;
	private WgFeedbackListAdapter mAdapter;//数据适配器
	private List<SysPassValue> NewsList = new ArrayList<SysPassValue>();//数据
	private int currentPage = 1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.setContentView(R.layout.activity_wg_feedback_list);
		loading=LoadingRelativeLayout.getLoadingRelativeLayout(this);
		
		includeTitle = this.findViewById(R.id.wg_util_head_feedback_list);
		((TextView)includeTitle.findViewById(R.id.wg_head_title)).setText("检查反馈");
		Button btreturn = (Button)includeTitle.findViewById(R.id.wg_head_btn_return);
		btreturn.setOnClickListener(this);
		mAdapter = new WgFeedbackListAdapter(this, NewsList);
		mAdapter.setOnClick1(this);//设置列表里的按钮点击事件
		mPullRefreshListView = (PullToRefreshListView) this.findViewById(R.id.xl_browse_wg_feedback_record_PulllistView);
		mPullRefreshListView.setMode(Mode.BOTH);
		initIndicator();
		//设置下拉，上移动 得到效果
        mPullRefreshListView.setOnRefreshListener(new OnRefreshListener2<ListView>()
        {
        	//下拉效果
            @Override  
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView)
            {
            	refreshView.getLoadingLayoutProxy().setPullLabel("下拉刷新...");// 刚下拉时，显示的提示
            	refreshView.getLoadingLayoutProxy().setRefreshingLabel("更新中...");// 刷新时
            	refreshView.getLoadingLayoutProxy().setReleaseLabel("松开刷新...");// 下来达到一定距离时，显示的提示
                
        	   if(lastRefreshdt.equals("")) {
        	    	// 显示最后更新的时间
                   refreshView.getLoadingLayoutProxy().setLastUpdatedLabel("刚刚");
       	       } else {
       	    	// 显示最后更新的时间
                   refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(lastRefreshdt);
       	       }
        	   
        	   
        	   SimpleDateFormat    formatter    =   new    SimpleDateFormat    ("MM月dd日    HH:mm:ss     ");
       	       Date    curDate    =   new    Date(System.currentTimeMillis());//获取当前时间
       	       lastRefreshdt =    formatter.format(curDate);
       	       
       	       //加载任务
       	       currentPage = 1;
       	       GetFeedbackData();
            }
            
            //上拉效果
            @Override  
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView)  
            {
                //加载任务
            	refreshView.getLoadingLayoutProxy().setPullLabel("上拉刷新...");// 刚上拉时，显示的提示
            	refreshView.getLoadingLayoutProxy().setRefreshingLabel("更新中...");// 刷新时
            	refreshView.getLoadingLayoutProxy().setReleaseLabel("松开刷新...");// 上来达到一定距离时，显示的提示
                currentPage++;
                GetFeedbackData();
            }
        });
        
		//获取控件并注册  这个是获取 listview 控件
        //ListView actualListView = mPullRefreshListView.getRefreshableView();
        //registerForContextMenu(actualListView); 
        //actualListView.setAdapter(mAdapter);
        
        mPullRefreshListView.setAdapter(mAdapter);
        onRefresh();//进来时直接刷新
	}
	
	private void onRefresh(){
		mPullRefreshListView.setRefreshing(true); 
        //首次加载任务数据 加载任务  
        currentPage = 1;
	    //getMyScore();
	    GetFeedbackData();
        //GetData(currentPage);
	}
	
	private void initIndicator(){
		ILoadingLayout startLabels = mPullRefreshListView.getLoadingLayoutProxy(true,false);
		
		//startLabels.setTextTypeface()
		startLabels.setPullLabel("下拉刷新...");// 刚下拉时，显示的提示  
		startLabels.setRefreshingLabel("更新中...");// 刷新时  
		startLabels.setReleaseLabel("松开刷新...");// 下来达到一定距离时，显示的提示
		
		ILoadingLayout endLabels = mPullRefreshListView.getLoadingLayoutProxy(false,true);
		endLabels.setPullLabel("上拉刷新...");// 刚上拉时，显示的提示  
		endLabels.setRefreshingLabel("更新中...");// 刷新时  
		endLabels.setReleaseLabel("松开刷新...");// 上来达到一定距离时，显示的提示
		endLabels.setLastUpdatedLabel("");
	}
	
	//onResume是在启动activity启动之后才能执行的，也就是恢复执行。程序正常启动：onCreate()->onStart()->onResume();
	@Override
	protected void onResume() {   //注意 该函数，如果在打开了其他窗口，再关闭，这个函数也会执行
		super.onResume();
	}
	
	public void GetFeedbackData(){
		Long userid = null;
		User curuser = new User();
		curuser = HhApplication.getInstance(this).getHhCart().getUser();
		if(curuser==null){
			userid =-1l;
		}else{
			userid = HhApplication.getInstance(this).getHhCart().getUser().getId();
		}
		
		SysPassValue temp = new SysPassValue();
		temp.setFunctionName("getFeedbackList");
		temp.setPageNumber(String.valueOf(this.currentPage));
		temp.setArg1(String.valueOf(userid));
		
		new sendOrderToServerForListYTask(new AbstractYGetTaskListener<List<SysPassValue>>() {
			@Override
			public void onPostExecute(String name, YGetTask<List<SysPassValue>> task) { 
				 
				showData(task.getValue());
				mPullRefreshListView.onRefreshComplete();
			}
		},temp).execute();
	}
	
	public void showData(List<SysPassValue> list) {
		
		//通过数据适配器获取 数据列表
		View vnorecord = this.findViewById(R.id.activity_no_record_showpic);
		if (list==null){
			if(currentPage==1){
				this.NewsList.clear(); 
	    		mAdapter.notifyDataSetChanged();
	    		vnorecord.setVisibility(View.VISIBLE);
	    		return;
			}
			return;
		}
		vnorecord.setVisibility(View.GONE);
		
		//如果是第一页则需要清楚数据   currentPage
		if(currentPage==1){
    		this.NewsList.clear();
			this.NewsList.addAll(list);
    	} else{
			NewsList.addAll(list);
		}
		mAdapter.notifyDataSetChanged();
		if(NewsList.size()==0){
			vnorecord.setVisibility(View.VISIBLE);
		}
		
	}
	
	@Override
	public void onClick(View view) {
		
		if(R.id.wg_head_btn_return==view.getId()){
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		}
		
		if(R.id.wg_plan_list_btn==view.getId()){//这行可以不用判断，因为已经设置wg_plan_list_btn监听多个重复id目前没有问题
			
			//传递参数过去
			Object tag = view.getTag();
			
			if (tag != null) {
				
				int position = (Integer) tag;//点击button所在Item中的位置，通过这个位置就可以得到Item中的值
				String taskId =  NewsList.get(position).getId();
				String taskName =  NewsList.get(position).getArg1();
			    Intent in=new Intent(view.getContext(),WgFeedbackViewActivity.class);
				in.putExtra("taskId", String.valueOf(taskId));
				in.putExtra("taskName", taskName);
				this.startActivity(in);
				overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				return;
			}
			
		}
	}
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		return;
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	
	
}
