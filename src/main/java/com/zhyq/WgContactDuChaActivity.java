package com.zhyq;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.handmark.pulltorefresh.library.ILoadingLayout;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.zhyq.adapter.WgContactListAdapterDucha;
import com.zhyq.libs.AndroidUtils;
import com.zhyq.libs.ToastUtils;
import com.zhyq.model.SysPassValue;
import com.zhyq.model.User;
import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.sendOrderToServerForListYTask;
import com.zhyq.widget.LoadingRelativeLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class WgContactDuChaActivity extends Activity implements OnItemClickListener,OnClickListener {
	
	private LoadingRelativeLayout loading = null;
	
	private View includeTitle;
	//private ImageView bottomImage;
	//private TextView bottomWord;
	
	private PullToRefreshListView mPullRefreshListView;//扩展的ListView
	private int currentPage = 1; 
	private String lastRefreshdt = "";
	 
	 
	private EditText et_keyword=null; 
	private Button bt_clear=null;
	 
	private String userCode = "";
	private String userName = "";
	private String userId = "";
	private String userface = "";
    private WgContactListAdapterDucha mAdapter;//数据适配器  用户列表适配器
    private List<SysPassValue> NewsList = new ArrayList<SysPassValue>();//数据
    
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.setContentView(R.layout.activity_wg_contact_list_ducha);
		loading=LoadingRelativeLayout.getLoadingRelativeLayout(this);
		//getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		includeTitle = this.findViewById(R.id.wg_util_head_contact_list);
		
		Button btreturn = (Button)includeTitle.findViewById(R.id.wg_head_btn_return);
		btreturn.setOnClickListener(this);
		
		((TextView)includeTitle.findViewById(R.id.wg_head_title)).setText("通讯录");
		
		et_keyword = (EditText)this.findViewById(R.id.wg_div_input_user_keywords);
		bt_clear    =(Button)this.findViewById(R.id.wg_contact_button_clear);
		bt_clear.setOnClickListener(this);
		//bottomImage = (ImageView)this.findViewById(R.id.iv_feedbackaccount_emptydata_img);
		//bottomWord = (TextView)this.findViewById(R.id.iv_feedbackaccount_emptydata_text);

		this.findViewById(R.id.wg_contact_btn1).setOnClickListener(this);
		this.findViewById(R.id.wg_contact_btn2).setOnClickListener(this);
		
		//设置数据
		
		et_keyword.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				GetData(currentPage);
				
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				//textview.setText(edittext.getText());
				
			}
			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
				//Log.i("spc","afterTextChanged");
				//computerRealAmt(et_keyword.getText().toString(),yuihuiAmt);
				
			}
		});
		
		//GetData( currentPage);
		//Log.i("longhua","start");
		
		/*
		 *  初始化数据适配器，传递Content 过去
		 */
		mAdapter = new WgContactListAdapterDucha(this, NewsList);
		mAdapter.setOnClick1(this);
		
		/*
         * 初始化 
         */  
		mPullRefreshListView = (PullToRefreshListView) this.findViewById(R.id.xl_contact_PulllistView);
		mPullRefreshListView.setMode(Mode.BOTH);
		//初始化值
		initIndicator();
		//设置下拉，上移动 得到效果
        mPullRefreshListView.setOnRefreshListener(new OnRefreshListener2<ListView>()
        {
        	//下拉效果
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView)
            {
            	refreshView.getLoadingLayoutProxy().setPullLabel("下拉刷新...");// 刚下拉时，显示的提示
            	refreshView.getLoadingLayoutProxy().setRefreshingLabel("更新中...");// 刷新时
            	refreshView.getLoadingLayoutProxy().setReleaseLabel("松开刷新...");// 下来达到一定距离时，显示的提示
            	
            	if(lastRefreshdt.equals("")) {
        	    	// 显示最后更新的时间
                    refreshView.getLoadingLayoutProxy().setLastUpdatedLabel("刚刚");
       	        } else {
       	        	// 显示最后更新的时间
       	        	refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(lastRefreshdt);
       	        }
            	
            	SimpleDateFormat    formatter    =   new    SimpleDateFormat    ("MM月dd日    HH:mm:ss     ");
       	        Date    curDate    =   new    Date(System.currentTimeMillis());//获取当前时间
       	        lastRefreshdt =    formatter.format(curDate);
       	        
                currentPage = 1;
                GetData(currentPage);
            }
            
            //上拉效果
            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView)
            {
                //加载任务  
            	refreshView.getLoadingLayoutProxy().setPullLabel("上拉刷新...");// 刚上拉时，显示的提示  
            	refreshView.getLoadingLayoutProxy().setRefreshingLabel("更新中...");// 刷新时  
            	refreshView.getLoadingLayoutProxy().setReleaseLabel("松开刷新...");// 上来达到一定距离时，显示的提示 
                currentPage++;
                GetData(currentPage);
            }
        });
        
		//获取控件并注册  这个是获取 listview 控件
        //ListView actualListView = mPullRefreshListView.getRefreshableView();
        //registerForContextMenu(actualListView); 
        //actualListView.setAdapter(mAdapter);
        
        mPullRefreshListView.setAdapter(mAdapter);
        onRefresh();//进来时直接刷新
	}
	
	private void onRefresh(){
		mPullRefreshListView.setRefreshing(true);
        //首次加载任务数据 加载任务
        currentPage = 1;
        GetData(currentPage);
	}
	
	private void initIndicator(){
		ILoadingLayout startLabels = mPullRefreshListView.getLoadingLayoutProxy(true,false);
		
		//startLabels.setTextTypeface()
		startLabels.setPullLabel("下拉刷新...");// 刚下拉时，显示的提示
		startLabels.setRefreshingLabel("更新中...");// 刷新时
		startLabels.setReleaseLabel("松开刷新...");// 下来达到一定距离时，显示的提示
		
		ILoadingLayout endLabels = mPullRefreshListView.getLoadingLayoutProxy(false,true);
		endLabels.setPullLabel("上拉刷新...");// 刚上拉时，显示的提示
		endLabels.setRefreshingLabel("更新中...");// 刷新时
		endLabels.setReleaseLabel("松开刷新...");// 上来达到一定距离时，显示的提示
		endLabels.setLastUpdatedLabel("");
	}
	
	//onResume是在启动activity启动之后才能执行的，也就是恢复执行。程序正常启动：onCreate()->onStart()->onResume();
	@Override
	protected void onResume() {   //该函数，如果在打开了其他窗口，再关闭，这个函数也会执行
		super.onResume();
		
	}
	
	public void GetData(int curpage){
		
		Long userid = null;
		User curuser = HhApplication.getInstance(this).getHhCart().getUser();
		if(curuser==null){
			userid =-1l;
		}else{
			userid = HhApplication.getInstance(this).getHhCart().getUser().getId();
		}
		
		String keyword = "";
		keyword = et_keyword.getText().toString();
		SysPassValue temp = new SysPassValue();
		temp.setFunctionName("getContactList");
		temp.setPageNumber(String.valueOf(curpage));
		temp.setArg1(String.valueOf(userid));
		temp.setArg2(keyword);
		temp.setArg3("2");
		
		new sendOrderToServerForListYTask(new AbstractYGetTaskListener<List<SysPassValue>>() {
			@Override
			public void onPostExecute(String name, YGetTask<List<SysPassValue>> task) { 
				 
				getData(task.getValue());
				mPullRefreshListView.onRefreshComplete();
			}
		},temp).execute();
		  
	}
	
	public void getData(List<SysPassValue> list) {
		
		//通过数据适配器获取 数据列表
		View vnorecord = this.findViewById(R.id.activity_no_record_showpic);
		if (list==null){
			if(currentPage==1){
				this.NewsList.clear(); 
	    		mAdapter.notifyDataSetChanged();
	    		vnorecord.setVisibility(View.VISIBLE);
	    		//bottomImage.setVisibility(View.VISIBLE);
	    		//bottomWord.setVisibility(View.VISIBLE);
	    		return;
			}
			return;
		}
		vnorecord.setVisibility(View.GONE);
		//bottomImage.setVisibility(View.GONE);
		//bottomWord.setVisibility(View.GONE);
		
		//如果是第一页则需要清楚数据   currentPage
		if(currentPage==1){
    		this.NewsList.clear();
			this.NewsList.addAll(list);
    	} else{
			NewsList.addAll(list);
		}
		mAdapter.notifyDataSetChanged();
		if(NewsList.size()==0){
			vnorecord.setVisibility(View.VISIBLE);
			//bottomImage.setVisibility(View.VISIBLE);
    		//bottomWord.setVisibility(View.VISIBLE);
		}
		
	}
	
	@Override
	public void onClick(View view) {
		
		if(R.id.wg_head_btn_return==view.getId()) {
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		}
		
		if(R.id.wg_contact_list_btn==view.getId()){//这行可以不用判断，因为已经设置wg_contact_list_btn监听多个重复id目前没有问题
			
			//传递参数过去
			Object tag = view.getTag();
			if (tag != null) {
				int position = (Integer) tag;//点击button所在Item中的位置，通过这个位置就可以得到Item中的值
				String contactId =  NewsList.get(position).getId();
				String contactName =  NewsList.get(position).getArg1();
			    Intent in=new Intent(view.getContext(),WgContactViewActivity.class);
				in.putExtra("contactId", String.valueOf(contactId));
				this.startActivity(in);
				overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);//设置新面板右边进入，原来面板左边退出
				return;
			}
			
		}

		if(R.id.wg_contact_btn1==view.getId()) {
			AndroidUtils.start(this, WgContactActivity.class);
			this.finish();
			//overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			return;
		}
		
		if(R.id.wg_contact_button_clear==view.getId()){
			 et_keyword.setText("");
			 onRefresh();
			 return;
		 }
		
	}
	
	private void showDataList(List<SysPassValue> list) {
		if (list==null) {
			ToastUtils.show(this, "获取数据失败");
			return;
		} else {
			mAdapter.addAll(list);
			mAdapter.notifyDataSetChanged();
			return;
		}
	}
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		Log.i("huahua","onItemClick");
		if(arg3 == -1) {
	        // 点击的是headerView或者footerView
	        return;
	    }
		//arg2是当前item的ID。这个id根据你在适配器中的写法可以自己定义
		//ToastUtils.show(arg1.getContext(), R.string.show_logon_error_msg);
		//Object tag2 = arg1.getTag(); 
        int position = (Integer) arg2;
        position = position -1;
		return;
	}
 
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}

 
}
