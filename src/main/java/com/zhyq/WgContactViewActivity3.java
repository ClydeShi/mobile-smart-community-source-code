package com.zhyq;


import android.os.Bundle;

import android.support.v4.app.FragmentActivity;

import android.view.View;
import android.view.View.OnClickListener; 
import android.widget.Button;
import android.widget.TextView;

import com.zhyq.libs.AndroidUtils;
import com.zhyq.libs.CustomProgressDialog;
import com.zhyq.libs.ToastUtils;
import com.zhyq.model.SysPassValue;

import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.sendOrderToServerForValueYTask;


public class WgContactViewActivity3 extends FragmentActivity implements OnClickListener  {
 
	private View  includeTitle;
	private String contactId = "";
	private Long userId = null;
	public CustomProgressDialog dialog;
	
	SysPassValue mValue = new SysPassValue();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		showMyDialog();
		
		if(HhApplication.getInstance(this).getHhCart().getUser()==null){
			userId =-1l;
		}else{
			userId = HhApplication.getInstance(this).getHhCart().getUser().getId();
		}
		contactId = this.getIntent().getStringExtra("contactId");
		
		SysPassValue temp = new SysPassValue();
		temp.setFunctionName("getContactDetail");
		temp.setId(String.valueOf(contactId));
		temp.setUserId(String.valueOf(userId));
		
		new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
			@Override
			public void onPostExecute(String name, YGetTask<SysPassValue> task) {
				//loading.hideLoading();
				getData(task.getValue());
			}
		},temp).execute();
		
		/*
		mLayout = new DefaultLoadingLayout(this, mValue);
		
		new Thread(new Runnable() {
            @Override
            public void run() {
                // 这里是模拟下载数据的耗时过程
            	SysPassValue temp = new SysPassValue();
        		temp.setFunctionName("getContactDetail");
        		temp.setId(String.valueOf(contactId));
        		temp.setUserId(String.valueOf(userId));
        		
        		new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
        			@Override
        			public void onPostExecute(String name, YGetTask<SysPassValue> task) {
        				//loading.hideLoading();
        				getData(task.getValue());
        			}
        		},temp).execute();
                // 数据下载完毕后,通知handler
                mHandler.sendEmptyMessage(1);
            }
        }).start();
        */
		
	}
	
	/*
	private Handler mHandler = new Handler(){
		@Override
        public void handleMessage(Message msg) {
            // 数据下载完成，转换状态，显示内容视图
            //mLayout.onDone();
			//loadLayout();
        }
	};
	*/
	
	public void loadLayout(){
		setContentView(R.layout.activity_wg_contact_detail);		
		includeTitle = this.findViewById(R.id.wg_contactdetail_head);		
		((TextView)includeTitle.findViewById(R.id.wg_head_title)).setText("通讯录");		
		Button btreturn = (Button)includeTitle.findViewById(R.id.wg_head_btn_return);
		btreturn.setOnClickListener(this);
	}
	
	public void showMyDialog(){
		dialog = new CustomProgressDialog(this);
		dialog.show();
	}
	
	//onResume是在启动activity启动之后才能执行的，也就是恢复执行。程序正常启动：onCreate()->onStart()->onResume();
	@Override
	protected void onResume() {//该函数，如果在打开了其他窗口，再关闭，这个函数也会执行
		// TODO Auto-generated method stub
		super.onResume();
		//Log.i("huahua","taskId = "+taskId);
	}




	@Override
	public void onClick(View view) {
		
		if(R.id.wg_head_btn_return==view.getId()){
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		}
		
		return;
	}
		
	//正常登入
		private void getData(SysPassValue passValue) {
			if (passValue==null) {
				ToastUtils.show(this,"没有获取到信息");
				return;
			} else {
				 /*((TextView)includeMaster.findViewById(R.id.tv_masterdetail_head_image_shopname)).setText(masterviewdetail.getArg1());  
				 AndroidUtils.setWebImageView(includeMaster, R.id.hh_masterdetail_head_image_shoplogimage, masterviewdetail.getArg2()); 
				 AndroidUtils.setWebImageView(includeMaster, R.id.hh_masterdetail_head_image_masterimage, masterviewdetail.getArg3());*/
				 
				String sid =  passValue.getId();
				String arg1 =  passValue.getArg1();
				String arg2 =  passValue.getArg2();
				String arg3 =  passValue.getArg3();
				String arg4 =  passValue.getArg4();
				String arg5 =  passValue.getArg5();
				String arg6 =  passValue.getArg6();
				String arg7 =  passValue.getArg7();
				String arg8 =  passValue.getArg8();
				
				dialog.cancel();
				loadLayout();
				
				AndroidUtils.setTextView(this, R.id.wg_contactdetail_textview2,arg2);
				AndroidUtils.setTextView(this, R.id.wg_contactdetail_textview3,arg3);
				AndroidUtils.setTextView(this, R.id.wg_contactdetail_textview4,arg4);
				AndroidUtils.setTextView(this, R.id.wg_contactdetail_textview5,arg5);
				AndroidUtils.setTextView(this, R.id.wg_contactdetail_textview6,arg6);
				AndroidUtils.setTextView(this, R.id.wg_contactdetail_textview7,arg7);
				AndroidUtils.setTextView(this, R.id.wg_contactdetail_textview8,arg8);
				AndroidUtils.setWebImageView(this, R.id.wg_contactdetail_img, arg1);
				
				//mLayout.notifyDataSetChanged();
				
				//((ImageView)includeTitle.findViewById(R.id.iv_hh_head_masterdetail_right_saygoods)).setBackgroundResource(R.drawable.praisetech);
				 
			}
		}
		
		
		
}
