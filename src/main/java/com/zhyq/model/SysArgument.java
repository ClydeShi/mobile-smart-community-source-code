package com.zhyq.model;

public class SysArgument {
	private Long id=null;
	private String name=null;
	private String descript=null;
	
	private String shoplist_picurl_pic = null;             //首页的广告图片连接
	private String shoplist_picurl_type = null;     //首页的广告类型 (店铺,服务,活动)
	private String shoplist_picurl_value = null; 	 //首页的广告图片连接类型的值
	
	private String shoucang_picurl_pic = null;            //收藏的广告图片 
	private String shoucang_picurl_type = null;    //收藏的广告图类型 (店铺,服务,活动)
	private String shoucang_picurl_value = null; 	//收藏的广告图连接类型的值	
	
	
	
	private String huodong_lunbo1_picurl = null; //活动轮播图1
	private String huodong_lunbo1_picurl_type = null; //活动轮播图1对应的链接(店铺,服务,活动)
	private String huodong_lunbo1_picurl_value = null; //活动轮播图1对应的值
	
	private String huodong_lunbo2_picurl = null; //活动轮播图1
	private String huodong_lunbo2_picurl_type = null; //活动轮播图1对应的链接(店铺,服务,活动)
	private String huodong_lunbo2_picurl_value = null; //活动轮播图1对应的值
	
	private String huodong_lunbo3_picurl = null; //活动轮播图1
	private String huodong_lunbo3_picurl_type = null; //活动轮播图1对应的链接(店铺,服务,活动)
	private String huodong_lunbo3_picurl_value = null; //活动轮播图1对应的值	
	
	private String huodong_lunbo4_picurl = null; //活动轮播图1
	private String huodong_lunbo4_picurl_type = null; //活动轮播图1对应的链接(店铺,服务,活动)
	private String huodong_lunbo4_picurl_value = null; //活动轮播图1对应的值	
	
	private String shangwubanname =null;
	private String xiawubanname=null;
	private String wanshangbanname = null;
	
	private double shangwubanci_startdt =0.00;
	private double xiawubanci_startdt =0.00;
	private double wanshangbanci_startdt =0.00;
	

	 
	public SysArgument(){
		super();
	}
	 
	public SysArgument(Long id, String name, String descript,
			String shoplist_picurl_pic,String shoplist_picurl_type,String shoplist_picurl_value,
			String shoucang_picurl_pic,String shoucang_picurl_type,String shoucang_picurl_value,
			String huodong_lunbo1_picurl,String huodong_lunbo1_picurl_type,String huodong_lunbo1_picurl_value,
			String huodong_lunbo2_picurl,String huodong_lunbo2_picurl_type,String huodong_lunbo2_picurl_value,
			String huodong_lunbo3_picurl,String huodong_lunbo3_picurl_type,String huodong_lunbo3_picurl_value,
			String huodong_lunbo4_picurl,String huodong_lunbo4_picurl_type,String huodong_lunbo4_picurl_value	,
			String shangwubanname,String xiawubanname,String wanshangbanname,
			double shangwubanci_startdt,double xiawubanci_startdt,double wanshangbanci_startdt 
			) {
		super();
		this.id = id;
		this.name = name;
		this.descript = descript;
		
		//首页的广告图片连接
		this.shoplist_picurl_pic = shoplist_picurl_pic;
		this.shoplist_picurl_type = shoplist_picurl_type;		
		this.shoplist_picurl_value = shoplist_picurl_value;	
		
		
		this.shoucang_picurl_pic = shoucang_picurl_pic;            //收藏的广告图片 
		this.shoucang_picurl_type = shoucang_picurl_type;    //收藏的广告图类型 (店铺,服务,活动)
		this.shoucang_picurl_value = shoucang_picurl_value; 	//收藏的广告图连接类型的值
		
		this.huodong_lunbo1_picurl = huodong_lunbo1_picurl; //活动轮播图1
		this.huodong_lunbo1_picurl_type = huodong_lunbo1_picurl_type; //活动轮播图1对应的链接(店铺,服务,活动)
		this.huodong_lunbo1_picurl_value = huodong_lunbo1_picurl_value; //活动轮播图1对应的值		
		

		this.huodong_lunbo2_picurl = huodong_lunbo2_picurl; //活动轮播图2
		this.huodong_lunbo2_picurl_type = huodong_lunbo2_picurl_type; //活动轮播图2对应的链接(店铺,服务,活动)
		this.huodong_lunbo2_picurl_value = huodong_lunbo2_picurl_value; //活动轮播图2对应的值			
		
		this.huodong_lunbo3_picurl           = huodong_lunbo3_picurl; //活动轮播图3
		this.huodong_lunbo3_picurl_type    = huodong_lunbo3_picurl_type; //活动轮播图3对应的链接(店铺,服务,活动)
		this.huodong_lunbo3_picurl_value = huodong_lunbo3_picurl_value; //活动轮播图3对应的值		
		
		this.huodong_lunbo4_picurl           = huodong_lunbo4_picurl; //活动轮播图4
		this.huodong_lunbo4_picurl_type    = huodong_lunbo4_picurl_type; //活动轮播图4对应的链接(店铺,服务,活动)
		this.huodong_lunbo4_picurl_value = huodong_lunbo4_picurl_value; //活动轮播图4对应的值	
		
		this.shangwubanname =shangwubanname;
		this.xiawubanname=xiawubanname;
		this.wanshangbanname = wanshangbanname;
		
		
		this.shangwubanci_startdt =shangwubanci_startdt;
		this.xiawubanci_startdt =xiawubanci_startdt;
		this.wanshangbanci_startdt =wanshangbanci_startdt;
		
		
	}
	
	
	//班次 早上 开始时间 
		public double get_shangwubanci_startdt() {
			return shangwubanci_startdt;
		}
		public void set_shangwubanci_startdt(double  shangwubanci_startdt) {
			this.shangwubanci_startdt = shangwubanci_startdt;
		}
	
		//班次 下午 开始时间 
		public double get_xiawubanci_startdt() {
			return xiawubanci_startdt;
		}
		public void set_xiawubanci_startdt(double  xiawubanci_startdt) {
			this.xiawubanci_startdt = xiawubanci_startdt;
		}	
	
		//班次 晚上 开始时间 
		public double get_wanshangbanci_startdt() {
			return wanshangbanci_startdt;
		}
		public void set_wanshangbanci_startdt(double  wanshangbanci_startdt) {
			this.wanshangbanci_startdt = wanshangbanci_startdt;
		}		
	
	
	
	//班次名称 早班
	public String get_shangwubanname() {
		return shangwubanname;
	}

	public void set_shangwubanname(String shangwubanname) {
		this.shangwubanname = shangwubanname;
	}
	
	//班次名称 下午班
	public String get_xiawubanname() {
		return xiawubanname;
	}

	public void set_xiawubanname(String xiawubanname) {
		this.xiawubanname = xiawubanname;
	}
	
	//班次名称 晚班
	public String get_wanshangbanname() {
		return wanshangbanname;
	}

	public void set_wanshangbanname(String wanshangbanname) {
		this.wanshangbanname = wanshangbanname;
	}
	
	
	
	//首页图片的 广告图片的链接地址   get set 
	public String getshoplist_picurl() {
		return shoplist_picurl_pic;
	}

	public void setshoplist_picurl(String shoplist_picurl_pic) {
		this.shoplist_picurl_pic = shoplist_picurl_pic;
	}
	
	//首页图片的 广告图片的类型 get set 	
	public String getshoplist_picurl_type() {
		return shoplist_picurl_type;
	}


	public void setshoplist_picurl_type(String shoplist_picurl_type) {
		this.shoplist_picurl_type = shoplist_picurl_type;
	}
	
	//首页图片的 广告图片的值 get set 	
	public String getshoplist_picurl_value() {
		return shoplist_picurl_value;
	}

	public void setshoplist_picurl_value(String shoplist_picurl_value) {
		this.shoplist_picurl_value = shoplist_picurl_value;
	}	
 
	
 //下面是收藏
	public String getshoucang_picurl() {
		return shoucang_picurl_pic;
	}

	public void setshoucang_picurl(String shoucang_picurl_pic) {
		this.shoucang_picurl_pic = shoucang_picurl_pic;
	}
	
	//收藏图片的 广告图片的类型 get set 	
	public String getshoucang_picurl_type() {
		return shoucang_picurl_type;
	}


	public void setshoucang_picurl_type(String shoucang_picurl_type) {
		this.shoucang_picurl_type = shoucang_picurl_type;
	}
	
	//收藏图片的 广告图片的值 get set 	
	public String getshoucang_picurl_value() {
		return shoucang_picurl_value;
	}

	public void setshoucang_picurl_value(String shoucang_picurl_value) {
		this.shoucang_picurl_value = shoucang_picurl_value;
	}	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//huodong_lunbo1_picurl    --轮播图片 的图片链接
	public String get_huodong_lunbo_picurl(int i ) {
		
		if (i ==1 ){
			return huodong_lunbo1_picurl;
		}
		
		if (i ==2 ){
			return huodong_lunbo2_picurl;
		}	
		
		if (i ==3 ){
			return huodong_lunbo3_picurl;
		}	
		
		if (i ==4 ){
			return huodong_lunbo4_picurl;
		}		
		
	return "";
	}

	
public void set_huodong_lunbo_picurl(int i, String value) {
		
		if (i ==1 ){
			this.huodong_lunbo1_picurl =value ;
		}
		
		if (i ==2 ){
			this.huodong_lunbo2_picurl =value ;
		}	
		
		if (i ==3 ){
			this.huodong_lunbo3_picurl =value ;
		}	
		
		if (i ==4 ){
			this.huodong_lunbo4_picurl =value ;
		}		 
	}		


//huodong_lunbo1_picurl    --轮播图片 的图片 类型
public String get_huodong_lunbo_picurl_type(int i ) {
	
	if (i ==1 ){
		return huodong_lunbo1_picurl_type;
	}
	
	if (i ==2 ){
		return huodong_lunbo2_picurl_type;
	}	
	
	if (i ==3 ){
		return huodong_lunbo3_picurl_type;
	}	
	
	if (i ==4 ){
		return huodong_lunbo4_picurl_type;
	}
	
return "";
}


public void set_huodong_lunbo_picurl_type(int i, String value) {
	
	if (i ==1 ){
		this.huodong_lunbo1_picurl_type =value ;
	}
	
	if (i ==2 ){
		this.huodong_lunbo2_picurl_type =value ;
	}	
	
	if (i ==3 ){
		this.huodong_lunbo3_picurl_type =value ;
	}	
	
	if (i ==4 ){
		this.huodong_lunbo4_picurl_type =value ;
	}		 
}		
	 
//huodong_lunbo1_picurl    --轮播图片 的图片 值
public String get_huodong_lunbo_picurl_value(int i ) {
	
	if (i ==1 ){
		return huodong_lunbo1_picurl_value;
	}
	
	if (i ==2 ){
		return huodong_lunbo2_picurl_value;
	}	
	
	if (i ==3 ){
		return huodong_lunbo3_picurl_value;
	}	
	
	if (i ==4 ){
		return huodong_lunbo4_picurl_value;
	}	
	
return "";
}


public void set_huodong_lunbo_picurl_value(int i, String value) {
	
	if (i ==1 ){
		this.huodong_lunbo1_picurl_value =value ;
	}
	
	if (i ==2 ){
		this.huodong_lunbo2_picurl_value =value ;
	}	
	
	if (i ==3 ){
		this.huodong_lunbo3_picurl_value =value ;
	}	
	
	if (i ==4 ){
		this.huodong_lunbo4_picurl_value =value ;
	}		 
}			
	
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescript() {
		return descript;
	}

	public void setDescript(String descript) {
		this.descript = descript;
	}

	 
}
