package com.zhyq.model;

 //购物车信息，主要包含操作员的用户信息，当前的精度，维度

public class HhCart {
	private User user=null;
	private HhShop shop=null;
	//private CpptCaiPiaoShop shop=null;
	
	private double Latitude =0.00; 
	private double Longitude =0.00;
	
 
    public void set_Shop(HhShop shop){
    	this.shop = shop;
    }
    
	public HhShop get_Shop() {
		return shop;
	}
	
	
	public double get_Latitude() {
		return Latitude;
	}
	
	public double get_Longitude() {
		return Longitude;
	}
	
	
	public void set_Location(double Latitude,double Longitude) {
		this.Latitude    = Latitude;
		this.Longitude = Longitude;		
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public boolean isLogined() {
		return user!=null;
	}

	 
}
