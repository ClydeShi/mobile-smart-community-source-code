package com.zhyq.model;

public class User {
	private Long id = null;
	private String code = null;
	private String name = null;
	private String password = null;
	private String curbitmap = null;
	private String netpic = null;
	private String usersex = null;
	private String userbirthday = null;
	private String userRole = null;
	private String userType = null;

	private String baiduappid = null;
	private String baiduuserid = null;
	private String baiduchannelid = null;
	private String baidurequestid = null;
	private String tuijianrentelephone = null;

	private String province = null;   //收货地址省份
	private String city = null;       //收货地址市
	private String county = null;     //收货地址县
	private String detailAddress = null;  //收货具体地址

	private String shopuser = "N";  //店铺用户 "Y" 是   "N" 否
	private String shopcode = ""; //店铺编号
	private String shopname = ""; //店铺名称

	private String realname = "";   //真实名字
	private String sfznumber = "";
	private String nickname = "";  //  昵称

	private String khbankname = "";
	private String khbankprovince = null;
	private String khbankcity = null;
	private String khbankcountry = null;
	private String khbankdetailAddress = null; //开户银行具体地址
	private String khbankcardnumber = "";       //卡号
	private String userModifyColumnType = "";

	public void setkhBankInfo(String temp1,String temp2,String temp3,String temp4,String temp5,String temp6) {
		this.khbankname = temp1;
		this.khbankprovince = temp2;
		this.khbankcity = temp3;
		this.khbankcountry = temp4;
		this.khbankdetailAddress = temp5;
		this.khbankcardnumber = temp6;
	}
	
	//用户表字段修改类型
	public String getUserModifyColumnType() {
		return userModifyColumnType;
	}
	
	public void setUserModifyColumnType(String temp) {
		userModifyColumnType = temp;
	}
	
	public String getuserbirthday() {
		return userbirthday;
	}
	
	public void setuserbirthday(String birthday) {
		userbirthday = birthday;
	}
	
	public String getKhbankName() {
		return khbankname;
	}
	public String getKhbankProvince() {
		return khbankprovince;
	}
	public String getKhbankCity() {
		return khbankcity;
	}	
	public String getKhbankCountry() {
		return khbankcountry;
	}	
	public String getKhbankAdress() {
		return khbankdetailAddress;
	}		
	
	public String getKhbankCardNumber() {
		return khbankcardnumber;
	}	
	
	//设置为店铺用户
	public void setrealname(String temp) {
		this.realname = temp;
	}
	
	public String getrealname() {
		return realname;
	}
	
	public void setsfznumber(String temp) {
		this.sfznumber = temp;
	}
	
	public String getsfznumber() {
		return sfznumber;
	}
	
	public void setnickname(String temp) {
		this.nickname = temp;
	}
	
	public String getnickname() {
		return nickname;
	}
	
	public User() {
		super();
	}
	public User(Long id, String name, String password) {
		super();
		this.id = id;
		this.name = name;
		this.password = password;
	}
	
	public User(Long id, String code,String name, String usersex,String password,String curbitmap,String netpic,String tuijianrentelephone) {
		super();
		this.id              = id;
		this.code            = code;
		this.name            = name;
		this.usersex         = usersex;
		this.password        = password;
		this.curbitmap       = curbitmap;
		this.netpic          = netpic;
		this.tuijianrentelephone = tuijianrentelephone;
	}
	
	public User(String code,String name, String baiduappid,String baiduuserid,String baiduchannelid,String baidurequestid) {
		super(); 
		this.code = code;
		this.name = name;
		this.baiduappid = baiduappid;
		this.baiduuserid = baiduuserid;
		this.baiduchannelid = baiduchannelid;
		this.baidurequestid = baidurequestid;
	}
	
	
	public String getUserSex() {
		return usersex;
	}
	
	//设置为店铺用户
	public void setUserSex(String usersex) {
		this.usersex = usersex;
	}
	
	//设置为店铺用户
	public void setShopCode(String shopcode) {
		this.shopcode = shopcode;
	}
	
	public String getshopcode() {
		return shopcode;
	}
	
	//设置为店铺用户
	public void setShopname(String shopname) {
		this.shopname = shopname;
	}
	
	public String getshopname() {
		return shopname;
	}
	
	//设置为店铺用户
	public void setShopuser(String shopuser) {
		this.shopuser = shopuser;
	}
	
	public String getshopuser() {
		return shopuser;
	}
	
	public void setProvince(String province) {
		this.province = province;
	}
	
	public String getProvince() {
		return province;
	}
	
	public void setCounty(String county) {
		this.county = county;
	}
	
	public String getCounty() {
		return county;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setdetailAddress(String detailAddress) {
		this.detailAddress = detailAddress;
	}
	
	public String getdetailAddress() {
		return detailAddress;
	}
	
	public void setbaiduuserid(String baiduuserid) {
		this.baiduuserid = baiduuserid;
	}
	
	public void settuijianrentelephone(String tuijianrentelephone) {
		this.tuijianrentelephone = tuijianrentelephone;
	}
	
	public String gettuijianrentelephone(String tuijianrentelephone) {
		return this.tuijianrentelephone;
	}
	
	public String gettuijianrentelephone() {
		return tuijianrentelephone;
	}
	
	public String getbaiduuserid() {
		return baiduuserid;
	}
	
	public void setbaiduchannelid(String baiduchannelid) {
		this.baiduchannelid = baiduchannelid;
	}
	
	public String getbaiduchannelid() {
		return baiduchannelid;
	}
	
	public void setbaiduappid(String baiduappid) {
		this.baiduappid = baiduappid;
	}
	
	public String getbaiduappid() {
		return baiduappid;
	}
	
	public void setbaidurequestid(String baidurequestid) {
		this.baidurequestid = baidurequestid;
	}
	
	public String getbaidurequestid() {
		return baidurequestid;
	}
	
	public void setpic(String curbitmap) {
		this.curbitmap = curbitmap;
	}
	public String getpic() {
		return curbitmap;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public void setnetpic(String netpic) {
		this.netpic = netpic;
	}
	
	public String getnetpic() {
		return netpic;
	}
	
	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}
}