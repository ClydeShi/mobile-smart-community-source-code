package com.zhyq.model;

import java.util.List;
  
/*
 * 店铺数据 
 */
public class HhShop {
	private Long id=null;
	private String name=null;
	private String address=null;
	private String phone=null;
	private String descript=null;  //这个变量作为 城市 
	private Double longitude=null;
	private Double latitude=null;
	private Long    dianzhanqty = 0l; //点赞数
	private String jieshaotext = null; //店铺介绍
	private String shopdetail_broadcast_bigpic = null;  //店铺详细介绍中的 大广告图片
	private String shopdetail_broadcast_smallpic = null; //店铺详细介绍中的 小广告图片
	private String listviewpic = null;
	
	private String zhaobanname = null;
	private String xiawubanname=null;
	private String wanbanname    = null;
	
	private double zhaobanstartdt =0.0;
	private double xiawubanstartdt =0.0;
	private double wanshangbanstartdt =0.0;
	
	private String distance=null;
	private String shoplog=null;
	private String userid=null;   //当前用户内部ID
	private String hasDianZhan=null; //我是否点赞   "Y 表示点赞了  N表示没有点赞"
	private String hasCollection=null; //我是否收藏该店铺 "Y 表示收藏了  N表示没有收藏"
	private String productqty=null;    //产品数量
	private String productcardqty=null;   //产品卡数量
	private String serviceqty=null;    //服务数量
	private String masterqty=null;     //技师数量
	private String recentnewsqty=null; //最新动态数量
	private String usershopyuou=null;  //会员店铺余额
	private String isShopMemeber=null; //是否为 该店铺会员 
	
	public String getIsShopMemeber() {
		return isShopMemeber;
	}

	public void setIsShopMemeber(String isShopMemeber) {
		this.isShopMemeber = isShopMemeber;
	}

	private List<SysPassValue>   huodong;  //读取多个活动 的ID,图片URL
	private List<String>   shophuanjing; //图片URL
	
	private List<String>   shophuodongId; //活动ID
	private List<String>   shophuodongimageurl; //活动图片
	private List<String>   shophuodongname; //活动名称
	private List<String>   shophuodongdianzhanqty; //点赞数量
	private List<String>   shophuodongdate; //活动日期
	
	
	public List<String> getShophuodongimageurl() {
		return shophuodongimageurl;
	}

	public void setShophuodongimageurl(List<String> shophuodongimageurl) {
		this.shophuodongimageurl = shophuodongimageurl;
	}

	public List<String> getShophuodongname() {
		return shophuodongname;
	}

	public void setShophuodongname(List<String> shophuodongname) {
		this.shophuodongname = shophuodongname;
	}

	public List<String> getShophuodongdianzhanqty() {
		return shophuodongdianzhanqty;
	}

	public void setShophuodongdianzhanqty(List<String> shophuodongdianzhanqty) {
		this.shophuodongdianzhanqty = shophuodongdianzhanqty;
	}

	public List<String> getShophuodongdate() {
		return shophuodongdate;
	}

	public void setShophuodongdate(List<String> shophuodongdate) {
		this.shophuodongdate = shophuodongdate;
	}

	public List<SysPassValue> getHuodong() {
		return huodong;
	}

	public Long getDianzhanqty() {
		return dianzhanqty;
	}

	public String getProductcardqty() {
		return productcardqty;
	}

	public void setProductcardqty(String productcardqty) {
		this.productcardqty = productcardqty;
	}

	public void setDianzhanqty(Long dianzhanqty) {
		this.dianzhanqty = dianzhanqty;
	}

	public String getHasDianZhan() {
		return hasDianZhan;
	}

	public void setHasDianZhan(String hasDianZhan) {
		this.hasDianZhan = hasDianZhan;
	}

	public void setHuodong(List<SysPassValue> huodong) {
		this.huodong = huodong;
	}

	public List<String> getShophuanjing() {
		return shophuanjing;
	}

	public void setShophuanjing(List<String> shophuanjing) {
		this.shophuanjing = shophuanjing;
	}

	public String getHasCollection() {
		return hasCollection;
	}

	public void setHasCollection(String hasCollection) {
		this.hasCollection = hasCollection;
	}

	public String getProductqty() {
		return productqty;
	}

	public void setProductqty(String productqty) {
		this.productqty = productqty;
	}

	public String getServiceqty() {
		return serviceqty;
	}

	public void setServiceqty(String serviceqty) {
		this.serviceqty = serviceqty;
	}

	public String getMasterqty() {
		return masterqty;
	}

	public void setMasterqty(String masterqty) {
		this.masterqty = masterqty;
	}

	public String getRecentnewsqty() {
		return recentnewsqty;
	}

	public void setRecentnewsqty(String recentnewsqty) {
		this.recentnewsqty = recentnewsqty;
	}

	public HhShop() {
		super();
	}
	
	public HhShop(Long id, String name, String address, String phone,
			String descript, Double longitude, Double latitude,Long  dianzhanqty,String jieshaotext,String shopdetail_broadcast_bigpic,String shopdetail_broadcast_smallpic,String listviewpic,
			String zhaobanname,String xiawubanname,String wanbanname,double zhaobanstartdt,double xiawubanstartdt ,double wanshangbanstartdt,String shoplog,
			String distance,String userid,String hasDianZhan 
			) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.descript = descript;
		this.longitude = longitude;
		this.latitude = latitude;
		this.dianzhanqty = dianzhanqty;
		this.jieshaotext = jieshaotext;
		this.shopdetail_broadcast_smallpic = shopdetail_broadcast_smallpic;
		this.shopdetail_broadcast_bigpic    = shopdetail_broadcast_bigpic;
		this.listviewpic = listviewpic;
		
		this.zhaobanname = zhaobanname;
		this.xiawubanname = xiawubanname;
		this.wanbanname = wanbanname;
		
		this.zhaobanstartdt =zhaobanstartdt;
		this.xiawubanstartdt =xiawubanstartdt;
		this.wanshangbanstartdt =wanshangbanstartdt;
		
		this.shoplog = shoplog;
		
		this.distance = distance;  //当前距离
		
		this.userid = userid;  //记忆当前的操作员，检测该店铺是否被我点赞过
		this.hasDianZhan = hasDianZhan; //是否点赞
		 
	 	
		
		
		
	}
	
	
	public String getUsershopyuou() {
		return usershopyuou;
	}

	public void setUsershopyuou(String usershopyuou) {
		this.usershopyuou = usershopyuou;
	}

	public String get_userid() {
		return userid;
	}
	
	public String get_hasDianZhan() {
		return hasDianZhan;
	}
	
	
	public void set_userid(String userid) {
		this.userid = userid;
	}
	public void set_hasDianZhan(String hasDianZhan) {
		this.hasDianZhan = hasDianZhan;
	}
	
	
	
	public String get_shoplog() {
		return shoplog;
	}
	
	public double get_zhaobanstartdt() {
		return zhaobanstartdt;
	}
	public double get_xiawubanstartdt() {
		return xiawubanstartdt;
	}
	public double get_wanshangbanstartdt() {
		return wanshangbanstartdt;
	}
	 
	//getDistance
	public String getDistance() {
		return distance;
	}
	
	public void setDistance(String distance) {
		this.distance = distance ;
	}	
	
	public String get_zhaobanname() {
		return zhaobanname;
	}
	
	public String get_xiawubanname() {
		return xiawubanname;
	}	
	
	
	public String get_wanbanname() {
		return wanbanname;
	}
	
	
	public String getlistviewpic() {
		return listviewpic;
	}
	public void setlistviewpic(String listviewpic) {
		this.listviewpic = listviewpic;
	}
	
	
	public String getViewBroadcastBigPic() {
		return shopdetail_broadcast_bigpic;
	}
	public void setViewBroadcastBigPic(String shopdetail_broadcast_bigpic) {
		this.shopdetail_broadcast_bigpic = shopdetail_broadcast_bigpic;
	}
	
	public String getViewBroadcastSmallPic() {
		return shopdetail_broadcast_smallpic;
	}
	public void setViewBroadcastSmallPic(String shopdetail_broadcast_smallpic) {
		this.shopdetail_broadcast_smallpic = shopdetail_broadcast_smallpic;
	}
	
	

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getDescript() {
		return descript;
	}
	public void setDescript(String descript) {
		this.descript = descript;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	
	public Long getdianzhanqty() {
		return dianzhanqty;
	}
	public void setdianzhanqty(Long dianzhanqty) {
		this.dianzhanqty = dianzhanqty;
	}
	
	public List<String> getShophuodongId() {
		return shophuodongId;
	}

	public void setShophuodongId(List<String> shophuodongId) {
		this.shophuodongId = shophuodongId;
	}

	public String getjieshao() {
		return jieshaotext;
	}
	public void setjieshao(String dianzhanqty) {
		this.jieshaotext = dianzhanqty;
	}  
	public void setxiawubanname(String xiawubanname) {
		this.xiawubanname = xiawubanname;
	}
	
	public void setwanbanname(String wanbanname) {
		this.wanbanname = wanbanname;
	}
}
