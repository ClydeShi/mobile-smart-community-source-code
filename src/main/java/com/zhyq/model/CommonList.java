package com.zhyq.model;


/*
 * 通用javabean，获取列表内容，在DemoDataInterface里面，使用new CommonList("")传入数据加入List，然后使用CommonList hhlistitem=(CommonList)list.get(position);hhlistitem获取数据
 */


public class CommonList {
	private Long id = null;
	private String varValue1 = null;
	private String varValue2 = null;
	private String varValue3 = null;
	private String varValue4 = null;
	private String varValue5 = null;
	private String varValue6 = null;
	private String varValue7 = null;
	private String varValue8 = null;
	private String varValue9 = null;
	private String varValue10 = null;
	private String varValue11 = null;
	private String varValue12 = null;
	private String varValue13 = null;
	private String varValue14 = null;
	private String varValue15 = null;
	private String varValue16 = null;
	private String varValue17 = null;
	private String varValue18 = null;
	private String varValue19 = null;
	private String varValue20 = null;
	private String varValue21 = null;
	private String varValue22 = null;
	private String varValue23 = null;
	private String varValue24 = null;
	private String varValue25 = null;
	private String varValue26 = null;
	private String varValue27 = null;
	private String varValue28 = null;
	private String varValue29 = null;
	private String varValue30 = null;
	
	public CommonList() {
		super();
	}
	
	public CommonList(Long id, String varValue1, String varValue2,
			String varValue3, String varValue4, String varValue5,
			String varValue6, String varValue7,
			String varValue8, String varValue9, String varValue10) {
		super();
		this.id = id;
		this.varValue1 = varValue1;
		this.varValue2 = varValue2;
		this.varValue3 = varValue3;
		this.varValue4 = varValue4;
		this.varValue5 = varValue5;
		this.varValue6 = varValue6;
		this.varValue7 = varValue7;
		this.varValue8 = varValue8;
		this.varValue9 = varValue9;
		this.varValue10 = varValue10;
	}
	
	public CommonList(Long id, String varValue1, String varValue2,
			String varValue3, String varValue4, String varValue5,
			String varValue6, String varValue7,
			String varValue8, String varValue9, String varValue10, String varValue11,
			String varValue12) {
		super();
		this.id = id;
		this.varValue1 = varValue1;
		this.varValue2 = varValue2;
		this.varValue3 = varValue3;
		this.varValue4 = varValue4;
		this.varValue5 = varValue5;
		this.varValue6 = varValue6;
		this.varValue7 = varValue7;
		this.varValue8 = varValue8;
		this.varValue9 = varValue9;
		this.varValue10 = varValue10;
		this.varValue11 = varValue11;
		this.varValue12 = varValue12;
	}
	
	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public String getVarValue1() {
		return varValue1;
	}



	public void setVarValue1(String varValue1) {
		this.varValue1 = varValue1;
	}



	public String getVarValue2() {
		return varValue2;
	}



	public void setVarValue2(String varValue2) {
		this.varValue2 = varValue2;
	}



	public String getVarValue3() {
		return varValue3;
	}



	public void setVarValue3(String varValue3) {
		this.varValue3 = varValue3;
	}



	public String getVarValue4() {
		return varValue4;
	}



	public void setVarValue4(String varValue4) {
		this.varValue4 = varValue4;
	}



	public String getVarValue5() {
		return varValue5;
	}



	public void setVarValue5(String varValue5) {
		this.varValue5 = varValue5;
	}



	public String getVarValue6() {
		return varValue6;
	}



	public void setVarValue6(String varValue6) {
		this.varValue6 = varValue6;
	}



	public String getVarValue7() {
		return varValue7;
	}



	public void setVarValue7(String varValue7) {
		this.varValue7 = varValue7;
	}



	public String getVarValue8() {
		return varValue8;
	}



	public void setVarValue8(String varValue8) {
		this.varValue8 = varValue8;
	}



	public String getVarValue9() {
		return varValue9;
	}



	public void setVarValue9(String varValue9) {
		this.varValue9 = varValue9;
	}



	public String getVarValue10() {
		return varValue10;
	}



	public void setVarValue10(String varValue10) {
		this.varValue10 = varValue10;
	}



	public String getVarValue11() {
		return varValue11;
	}



	public void setVarValue11(String varValue11) {
		this.varValue11 = varValue11;
	}



	public String getVarValue12() {
		return varValue12;
	}



	public void setVarValue12(String varValue12) {
		this.varValue12 = varValue12;
	}



	public String getVarValue13() {
		return varValue13;
	}



	public void setVarValue13(String varValue13) {
		this.varValue13 = varValue13;
	}



	public String getVarValue14() {
		return varValue14;
	}



	public void setVarValue14(String varValue14) {
		this.varValue14 = varValue14;
	}



	public String getVarValue15() {
		return varValue15;
	}



	public void setVarValue15(String varValue15) {
		this.varValue15 = varValue15;
	}



	public String getVarValue16() {
		return varValue16;
	}



	public void setVarValue16(String varValue16) {
		this.varValue16 = varValue16;
	}



	public String getVarValue17() {
		return varValue17;
	}



	public void setVarValue17(String varValue17) {
		this.varValue17 = varValue17;
	}



	public String getVarValue18() {
		return varValue18;
	}



	public void setVarValue18(String varValue18) {
		this.varValue18 = varValue18;
	}



	public String getVarValue19() {
		return varValue19;
	}



	public void setVarValue19(String varValue19) {
		this.varValue19 = varValue19;
	}



	public String getVarValue20() {
		return varValue20;
	}



	public void setVarValue20(String varValue20) {
		this.varValue20 = varValue20;
	}



	public String getVarValue21() {
		return varValue21;
	}



	public void setVarValue21(String varValue21) {
		this.varValue21 = varValue21;
	}



	public String getVarValue22() {
		return varValue22;
	}



	public void setVarValue22(String varValue22) {
		this.varValue22 = varValue22;
	}



	public String getVarValue23() {
		return varValue23;
	}



	public void setVarValue23(String varValue23) {
		this.varValue23 = varValue23;
	}



	public String getVarValue24() {
		return varValue24;
	}



	public void setVarValue24(String varValue24) {
		this.varValue24 = varValue24;
	}



	public String getVarValue25() {
		return varValue25;
	}



	public void setVarValue25(String varValue25) {
		this.varValue25 = varValue25;
	}



	public String getVarValue26() {
		return varValue26;
	}



	public void setVarValue26(String varValue26) {
		this.varValue26 = varValue26;
	}



	public String getVarValue27() {
		return varValue27;
	}



	public void setVarValue27(String varValue27) {
		this.varValue27 = varValue27;
	}



	public String getVarValue28() {
		return varValue28;
	}



	public void setVarValue28(String varValue28) {
		this.varValue28 = varValue28;
	}



	public String getVarValue29() {
		return varValue29;
	}



	public void setVarValue29(String varValue29) {
		this.varValue29 = varValue29;
	}



	public String getVarValue30() {
		return varValue30;
	}



	public void setVarValue30(String varValue30) {
		this.varValue30 = varValue30;
	}
	
}
