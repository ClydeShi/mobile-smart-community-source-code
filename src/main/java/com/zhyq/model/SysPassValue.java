package com.zhyq.model;

import java.util.List;

//传递 参数对象   
//所有的对象都用这个对象来传递
//总共 50个字符串字符
//9个字符集合
//arg1 是 功能名称 ，是为了接口使用 前后台必须一直


//新版本
//这个也作为参数传递
//function   : 获取什么的功能名称
//pagenumber : 当前是第几页码数量,没有码数的默认为 1


public class SysPassValue {
	private String id="";
	private String userId="";
	private String userName="";
	private String typeId="";
	private String typeId2="";
	private String typeId3="";
	private String typeId4="";
	private String typeId5="";
	private String typeId6="";
	private String status="";
	private String functionName="";
	private String pageNumber="1";
	private String returnResult="0";
	private String returnResult2="0";
	private String arg1="";    
	private String arg2="";    
	private String arg3="";
	private String arg4="";
	private String arg5="";
	private String arg6="";
	private String arg7="";
	private String arg8="";
	private String arg9="";
	private String arg10="";
	private String arg11="";
	private String arg12="";
	private String arg13="";
	private String arg14="";
	private String arg15="";
	private String arg16="";
	private String arg17="";
	private String arg18="";
	
	private String arg19="";
	private String arg20="";
	private String arg21="";
	private String arg22="";
	private String arg23="";
	private String arg24="";
	
	private String arg25="";
	private String arg26="";
	private String arg27="";
	private String arg28="";
	private String arg29="";
	private String arg30="";
	
	private String arg31="";
	private String arg32="";
	private String arg33="";
	private String arg34="";
	private String arg35="";
	private String arg36="";	
	
	private String arg37="";
	private String arg38="";
	private String arg39="";
	private String arg40="";
	
	private String arg41="";
	private String arg42="";
	private String arg43="";
	private String arg44="";
	private String arg45="";
	private String arg46="";
	
	private String arg47="";
	private String arg48="";
	private String arg49="";
	private String arg50="";
	private boolean bo = false;
	
	public boolean getBo() {
		return bo;
	}
	public void setBo(boolean bo) {
		this.bo = bo;
	}
	
	List<String>  strList1 = null;
	List<String>  strList2 = null;
	List<String>  strList3 = null;
	List<String>  strList4 = null;
	List<String>  strList5 = null;
	List<String>  strList6 = null;
	
	//下面是传递对象 
	List<SysPassList>  list1 = null;
	List<SysPassList>  list2 = null;
	List<SysPassList>  list3 = null;
	List<SysPassList>  list4 = null;
	List<SysPassList>  list5 = null;
	List<SysPassList>  list6 = null;
	List<SysPassList>  list7 = null;
	
	public List<String> getStrList1() {
		return strList1;
	}
	public void setStrList1(List<String> strList1) {
		this.strList1 = strList1;
	}
	public List<String> getStrList2() {
		return strList2;
	}
	public void setStrList2(List<String> strList2) {
		this.strList2 = strList2;
	}
	public List<String> getStrList3() {
		return strList3;
	}
	public void setStrList3(List<String> strList3) {
		this.strList3 = strList3;
	}
	public List<String> getStrList4() {
		return strList4;
	}
	public void setStrList4(List<String> strList4) {
		this.strList4 = strList4;
	}
	public List<String> getStrList5() {
		return strList5;
	}
	public void setStrList5(List<String> strList5) {
		this.strList5 = strList5;
	}
	public List<String> getStrList6() {
		return strList6;
	}
	public void setStrList6(List<String> strList6) {
		this.strList6 = strList6;
	}
	List<SysPassList>  list8 = null;
	List<SysPassList>  list9 = null;
	
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getFunctionName() {
		return functionName;
	}
	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}
	public String getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(String pageNumber) {
		this.pageNumber = pageNumber;
	}
	public String getReturnResult() {
		return returnResult;
	}
	public void setReturnResult(String returnResult) {
		this.returnResult = returnResult;
	}
	public String getReturnResult2() {
		return returnResult2;
	}
	public void setReturnResult2(String returnResult2) {
		this.returnResult2 = returnResult2;
	}
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getTypeId() {
		return typeId;
	}
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
	public String getTypeId2() {
		return typeId2;
	}
	public void setTypeId2(String typeId2) {
		this.typeId2 = typeId2;
	}
	public String getTypeId3() {
		return typeId3;
	}
	public void setTypeId3(String typeId3) {
		this.typeId3 = typeId3;
	}
	public String getTypeId4() {
		return typeId4;
	}
	public void setTypeId4(String typeId4) {
		this.typeId4 = typeId4;
	}
	public String getTypeId5() {
		return typeId5;
	}
	public void setTypeId5(String typeId5) {
		this.typeId5 = typeId5;
	}
	public String getTypeId6() {
		return typeId6;
	}
	public void setTypeId6(String typeId6) {
		this.typeId6 = typeId6;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getArg1() {
		return arg1;
	}
	public void setArg1(String arg1) {
		this.arg1 = arg1;
	}
	public String getArg2() {
		return arg2;
	}
	public void setArg2(String arg2) {
		this.arg2 = arg2;
	}
	public String getArg3() {
		return arg3;
	}
	public void setArg3(String arg3) {
		this.arg3 = arg3;
	}
	public String getArg4() {
		return arg4;
	}
	public void setArg4(String arg4) {
		this.arg4 = arg4;
	}
	public String getArg5() {
		return arg5;
	}
	public void setArg5(String arg5) {
		this.arg5 = arg5;
	}
	public String getArg6() {
		return arg6;
	}
	public void setArg6(String arg6) {
		this.arg6 = arg6;
	}
	public String getArg7() {
		return arg7;
	}
	public void setArg7(String arg7) {
		this.arg7 = arg7;
	}
	public String getArg8() {
		return arg8;
	}
	public void setArg8(String arg8) {
		this.arg8 = arg8;
	}
	public String getArg9() {
		return arg9;
	}
	public void setArg9(String arg9) {
		this.arg9 = arg9;
	}
	public String getArg10() {
		return arg10;
	}
	public void setArg10(String arg10) {
		this.arg10 = arg10;
	}
	public String getArg11() {
		return arg11;
	}
	public void setArg11(String arg11) {
		this.arg11 = arg11;
	}
	public String getArg12() {
		return arg12;
	}
	public void setArg12(String arg12) {
		this.arg12 = arg12;
	}
	public String getArg13() {
		return arg13;
	}
	public void setArg13(String arg13) {
		this.arg13 = arg13;
	}
	public String getArg14() {
		return arg14;
	}
	public void setArg14(String arg14) {
		this.arg14 = arg14;
	}
	public String getArg15() {
		return arg15;
	}
	public void setArg15(String arg15) {
		this.arg15 = arg15;
	}
	public String getArg16() {
		return arg16;
	}
	public void setArg16(String arg16) {
		this.arg16 = arg16;
	}
	public String getArg17() {
		return arg17;
	}
	public void setArg17(String arg17) {
		this.arg17 = arg17;
	}
	public String getArg18() {
		return arg18;
	}
	public void setArg18(String arg18) {
		this.arg18 = arg18;
	}
	public String getArg19() {
		return arg19;
	}
	public void setArg19(String arg19) {
		this.arg19 = arg19;
	}
	public String getArg20() {
		return arg20;
	}
	public void setArg20(String arg20) {
		this.arg20 = arg20;
	}
	public String getArg21() {
		return arg21;
	}
	public void setArg21(String arg21) {
		this.arg21 = arg21;
	}
	public String getArg22() {
		return arg22;
	}
	public void setArg22(String arg22) {
		this.arg22 = arg22;
	}
	public String getArg23() {
		return arg23;
	}
	public void setArg23(String arg23) {
		this.arg23 = arg23;
	}
	public String getArg24() {
		return arg24;
	}
	public void setArg24(String arg24) {
		this.arg24 = arg24;
	}
	public String getArg25() {
		return arg25;
	}
	public void setArg25(String arg25) {
		this.arg25 = arg25;
	}
	public String getArg26() {
		return arg26;
	}
	public void setArg26(String arg26) {
		this.arg26 = arg26;
	}
	public String getArg27() {
		return arg27;
	}
	public void setArg27(String arg27) {
		this.arg27 = arg27;
	}
	public String getArg28() {
		return arg28;
	}
	public void setArg28(String arg28) {
		this.arg28 = arg28;
	}
	public String getArg29() {
		return arg29;
	}
	public void setArg29(String arg29) {
		this.arg29 = arg29;
	}
	public String getArg30() {
		return arg30;
	}
	public void setArg30(String arg30) {
		this.arg30 = arg30;
	}
	public String getArg31() {
		return arg31;
	}
	public void setArg31(String arg31) {
		this.arg31 = arg31;
	}
	public String getArg32() {
		return arg32;
	}
	public void setArg32(String arg32) {
		this.arg32 = arg32;
	}
	public String getArg33() {
		return arg33;
	}
	public void setArg33(String arg33) {
		this.arg33 = arg33;
	}
	public String getArg34() {
		return arg34;
	}
	public void setArg34(String arg34) {
		this.arg34 = arg34;
	}
	public String getArg35() {
		return arg35;
	}
	public void setArg35(String arg35) {
		this.arg35 = arg35;
	}
	public String getArg36() {
		return arg36;
	}
	public void setArg36(String arg36) {
		this.arg36 = arg36;
	}
	public String getArg37() {
		return arg37;
	}
	public void setArg37(String arg37) {
		this.arg37 = arg37;
	}
	public String getArg38() {
		return arg38;
	}
	public void setArg38(String arg38) {
		this.arg38 = arg38;
	}
	public String getArg39() {
		return arg39;
	}
	public void setArg39(String arg39) {
		this.arg39 = arg39;
	}
	public String getArg40() {
		return arg40;
	}
	public void setArg40(String arg40) {
		this.arg40 = arg40;
	}
	public String getArg41() {
		return arg41;
	}
	public void setArg41(String arg41) {
		this.arg41 = arg41;
	}
	public String getArg42() {
		return arg42;
	}
	public void setArg42(String arg42) {
		this.arg42 = arg42;
	}
	public String getArg43() {
		return arg43;
	}
	public void setArg43(String arg43) {
		this.arg43 = arg43;
	}
	public String getArg44() {
		return arg44;
	}
	public void setArg44(String arg44) {
		this.arg44 = arg44;
	}
	public String getArg45() {
		return arg45;
	}
	public void setArg45(String arg45) {
		this.arg45 = arg45;
	}
	public String getArg46() {
		return arg46;
	}
	public void setArg46(String arg46) {
		this.arg46 = arg46;
	}
	public String getArg47() {
		return arg47;
	}
	public void setArg47(String arg47) {
		this.arg47 = arg47;
	}
	public String getArg48() {
		return arg48;
	}
	public void setArg48(String arg48) {
		this.arg48 = arg48;
	}
	public String getArg49() {
		return arg49;
	}
	public void setArg49(String arg49) {
		this.arg49 = arg49;
	}
	public String getArg50() {
		return arg50;
	}
	public void setArg50(String arg50) {
		this.arg50 = arg50;
	}
	public SysPassValue(String id, String arg1, String arg2, String arg3,
			String arg4, String arg5, String arg6, String arg7, String arg8,
			String arg9, String arg10, String arg11, String arg12,
			String arg13, String arg14, String arg15, String arg16,
			String arg17, String arg18, String arg19, String arg20,
			String arg21, String arg22, String arg23, String arg24,
			String arg25, String arg26, String arg27, String arg28,
			String arg29, String arg30, String arg31, String arg32,
			String arg33, String arg34, String arg35, String arg36,
			String arg37, String arg38, String arg39, String arg40,
			String arg41, String arg42, String arg43, String arg44,
			String arg45, String arg46, String arg47, String arg48,
			String arg49, String arg50, List<SysPassList> list1,
			List<SysPassList> list2, List<SysPassList> list3,
			List<SysPassList> list4, List<SysPassList> list5,
			List<SysPassList> list6, List<SysPassList> list7,
			List<SysPassList> list8, List<SysPassList> list9) {
		super();
		this.id = id;
		this.arg1 = arg1;
		this.arg2 = arg2;
		this.arg3 = arg3;
		this.arg4 = arg4;
		this.arg5 = arg5;
		this.arg6 = arg6;
		this.arg7 = arg7;
		this.arg8 = arg8;
		this.arg9 = arg9;
		this.arg10 = arg10;
		this.arg11 = arg11;
		this.arg12 = arg12;
		this.arg13 = arg13;
		this.arg14 = arg14;
		this.arg15 = arg15;
		this.arg16 = arg16;
		this.arg17 = arg17;
		this.arg18 = arg18;
		this.arg19 = arg19;
		this.arg20 = arg20;
		this.arg21 = arg21;
		this.arg22 = arg22;
		this.arg23 = arg23;
		this.arg24 = arg24;
		this.arg25 = arg25;
		this.arg26 = arg26;
		this.arg27 = arg27;
		this.arg28 = arg28;
		this.arg29 = arg29;
		this.arg30 = arg30;
		this.arg31 = arg31;
		this.arg32 = arg32;
		this.arg33 = arg33;
		this.arg34 = arg34;
		this.arg35 = arg35;
		this.arg36 = arg36;
		this.arg37 = arg37;
		this.arg38 = arg38;
		this.arg39 = arg39;
		this.arg40 = arg40;
		this.arg41 = arg41;
		this.arg42 = arg42;
		this.arg43 = arg43;
		this.arg44 = arg44;
		this.arg45 = arg45;
		this.arg46 = arg46;
		this.arg47 = arg47;
		this.arg48 = arg48;
		this.arg49 = arg49;
		this.arg50 = arg50;
		this.list1 = list1;
		this.list2 = list2;
		this.list3 = list3;
		this.list4 = list4;
		this.list5 = list5;
		this.list6 = list6;
		this.list7 = list7;
		this.list8 = list8;
		this.list9 = list9;
	}
	public SysPassValue() {
		super();
		// TODO Auto-generated constructor stub
	}
	public List<SysPassList> getList1() {
		return list1;
	}
	public void setList1(List<SysPassList> list1) {
		this.list1 = list1;
	}
	public List<SysPassList> getList2() {
		return list2;
	}
	public void setList2(List<SysPassList> list2) {
		this.list2 = list2;
	}
	public List<SysPassList> getList3() {
		return list3;
	}
	public void setList3(List<SysPassList> list3) {
		this.list3 = list3;
	}
	public List<SysPassList> getList4() {
		return list4;
	}
	public void setList4(List<SysPassList> list4) {
		this.list4 = list4;
	}
	public List<SysPassList> getList5() {
		return list5;
	}
	public void setList5(List<SysPassList> list5) {
		this.list5 = list5;
	}
	public List<SysPassList> getList6() {
		return list6;
	}
	public void setList6(List<SysPassList> list6) {
		this.list6 = list6;
	}
	public List<SysPassList> getList7() {
		return list7;
	}
	public void setList7(List<SysPassList> list7) {
		this.list7 = list7;
	}
	public List<SysPassList> getList8() {
		return list8;
	}
	public void setList8(List<SysPassList> list8) {
		this.list8 = list8;
	}
	public List<SysPassList> getList9() {
		return list9;
	}
	public void setList9(List<SysPassList> list9) {
		this.list9 = list9;
	}
	
	
	 
}
