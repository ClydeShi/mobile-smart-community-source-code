package com.zhyq.model;

public class NoticeEntity {
	private String stype;
	private String title;
	private String url;
    public NoticeEntity(String stype,String title, String url) {
        super();
        this.stype = stype;
        this.title = title;
        this.url = url;
    }
    
    public String getstype(){
    	return this.stype;
    }
    
    public String gettitle(){
    	return this.title;
    }
    public String geturl(){
    	return this.url;
    }   
}