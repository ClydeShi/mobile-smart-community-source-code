package com.zhyq;

import android.os.Handler;

import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.model.LatLng;


public class WgMapThread extends Thread {
	
	MapView mMapView;
	private Marker mMoveMarker;
    private Handler mHandler;
	
	private volatile boolean isStop = false;
	
	private static final int TIME_INTERVAL = 80;
    private static final double DISTANCE = 0.00006;//0.00002
	
	final LatLng[] userlatlngs;
	
	WgMapThread(LatLng[] userlatlngs,MapView mMapView, Marker mMoveMarker, Handler mHandler){
		this.userlatlngs = userlatlngs;
		this.mMoveMarker = mMoveMarker;
		this.mMapView = mMapView;
		this.mHandler = mHandler;
	}
	
	//调用interrupt之前，把isStop置为false
	@Override
    public void interrupt(){
		isStop = true;  
	    super.interrupt();
    }
	
	//检查stop的状态
	@Override
    public void run(){
		
		while (!isStop) {

            for (int i = 0; i < userlatlngs.length - 1; i++) {


                final LatLng startPoint = userlatlngs[i];
                final LatLng endPoint = userlatlngs[i + 1];
                mMoveMarker
                        .setPosition(startPoint);

                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        // refresh marker's rotate
                        if (mMapView == null) {
                            return;
                        }
                        mMoveMarker.setRotate((float) getAngle(startPoint,
                                endPoint));
                    }
                });
                double slope = getSlope(startPoint, endPoint);
                // 是不是正向的标示
                boolean isReverse = (startPoint.latitude > endPoint.latitude);

                double intercept = getInterception(slope, startPoint);

                double xMoveDistance = isReverse ? getXMoveDistance(slope) : -1 * getXMoveDistance(slope);


                for (double j = startPoint.latitude; !((j > endPoint.latitude) ^ isReverse);
                     j = j - xMoveDistance) {
                    LatLng latLng = null;
                    if (slope == Double.MAX_VALUE) {
                        latLng = new LatLng(j, startPoint.longitude);
                    } else {
                        latLng = new LatLng(j, (j - intercept) / slope);
                    }

                    final LatLng finalLatLng = latLng;
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (mMapView == null) {
                                return;
                            }
                            mMoveMarker.setPosition(finalLatLng);
                        }
                    });
                    try {
                        Thread.sleep(TIME_INTERVAL);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
		}

	}

    /**
     * 根据两点算取图标转的角度
     */
    private double getAngle(LatLng fromPoint, LatLng toPoint) {
        double slope = getSlope(fromPoint, toPoint);
        if (slope == Double.MAX_VALUE) {
            if (toPoint.latitude > fromPoint.latitude) {
                return 0;
            } else {
                return 180;
            }
        }
        float deltAngle = 0;
        if ((toPoint.latitude - fromPoint.latitude) * slope < 0) {
            deltAngle = 180;
        }
        double radio = Math.atan(slope);
        double angle = 180 * (radio / Math.PI) + deltAngle - 90;
        return angle;
    }

    /**
     * 根据点和斜率算取截距
     */
    private double getInterception(double slope, LatLng point) {

        double interception = point.latitude - slope * point.longitude;
        return interception;
    }

    /**
     * 算斜率
     */
    private double getSlope(LatLng fromPoint, LatLng toPoint) {
        if (toPoint.longitude == fromPoint.longitude) {
            return Double.MAX_VALUE;
        }
        double slope = ((toPoint.latitude - fromPoint.latitude) / (toPoint.longitude - fromPoint.longitude));
        return slope;

    }
    
    /**
     * 计算x方向每次移动的距离
     */
    private double getXMoveDistance(double slope) {
        if (slope == Double.MAX_VALUE) {
            return DISTANCE;
        }
        return Math.abs((DISTANCE * slope) / Math.sqrt(1 + slope * slope));
    }
    
	
}
