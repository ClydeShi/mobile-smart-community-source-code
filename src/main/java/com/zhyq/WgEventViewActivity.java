package com.zhyq;

 
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
 

import com.handmark.pulltorefresh.library.ILoadingLayout;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
 
import com.zhyq.adapter.ProductImageAdapter;
import com.zhyq.adapter.UniteListAdapter;
 
 
import com.zhyq.libs.AndroidUtils;
import com.zhyq.libs.ToastUtils;
 
 
 
import com.zhyq.model.SysPassList;
import com.zhyq.model.SysPassValue;
 

import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
 
import com.zhyq.task.impl.sendOrderToServerForValueYTask;


import android.app.Activity;


import android.os.Bundle;
import android.os.Handler;
 
 
 
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

/*
 * 使用前需要在AndroidManifest里定义，刷新工程
 * 使用ListView来加载内容，分别加载列表面板和内容面板
 * 先显示外布局activity_wg_event_detail，初始化数据适配器，然后加载内容面板，设置详情数据，把详情里面的子列表数据更新到PullToRefreshListView的Adapter里面，适合详情里面有列表内容的类
 * 布局面板activity_wg_event_detail，内容面板item_wg_event_detail，列表面板item_wg_event_detail_list
 * PullToRefreshListView赋值给ListView，然后使用setAdapter方法加入内容面板，内容面板使用addHeaderView加入ListView（应该addView方法也可以），要加载内容面板和列表面板，所以使用ListView
 */


public class WgEventViewActivity extends Activity implements OnItemClickListener,OnClickListener {
	
	private View includeTitle;
	//private ImageView bottomImage;
	//private TextView bottomWord;
	private PullToRefreshListView mPullRefreshListView;//扩展的ListView
	private ListView listView = null;
	private View contentView = null;      //listView 的内容
	private int currentPage = 1;
	private String lastRefreshdt = "";
	private String id = "";
	private Long userId = null;
    private UniteListAdapter mAdapter;//数据适配器
    private List<SysPassValue> NewsList = new ArrayList<SysPassValue>();//数据
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_wg_event_detail);
		includeTitle = this.findViewById(R.id.wg_eventdetail_head);
		Button btreturn = (Button)includeTitle.findViewById(R.id.wg_head_btn_return);
		btreturn.setOnClickListener(this);
		((TextView)includeTitle.findViewById(R.id.wg_head_title)).setText("案件查看");
		
		//bottomImage = (ImageView)this.findViewById(R.id.iv_myorder_waitforpay_list_emptydata_log);
		//bottomWord = (TextView)this.findViewById(R.id.iv_myorder_waitforpay_list_emptydata_log_word);
		
		id = this.getIntent().getStringExtra("id");
		
		/*
		 *  初始化数据适配器，传递Content过去，将详情里面的列表面板传递过去
		 */
		mAdapter = new UniteListAdapter(this, NewsList,"item_wg_event_detail_list");
		//mAdapter.setOnClick1(this);
		
		/*
		 * 初始化
		 */
		mPullRefreshListView = (PullToRefreshListView) this.findViewById(R.id.xl_browse_eventdetail_waitforpay_PulllistView);
		mPullRefreshListView.setMode(Mode.PULL_DOWN_TO_REFRESH);//下拉刷新
		//mPullRefreshListView.setMode(Mode.BOTH);//下拉上拉都可以刷新 详情就只下拉刷新吧
		
		listView = mPullRefreshListView.getRefreshableView();//加载刷新列表
		listView.setDividerHeight(0);// 隐藏listview默认的divider
		listView.setSelector(android.R.color.transparent);// 隐藏listview默认的selector
		
		//初始化值
		initIndicator();
		//设置下拉，上移动 得到效果，每拉一次，都会重新获取数据
        mPullRefreshListView.setOnRefreshListener(new OnRefreshListener2<ListView>()
        {
        	//下拉效果
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView)
            {
            	refreshView.getLoadingLayoutProxy().setPullLabel("下拉刷新...");// 刚下拉时，显示的提示
            	refreshView.getLoadingLayoutProxy().setRefreshingLabel("更新中...");// 第二次刷新时显示的提示
            	refreshView.getLoadingLayoutProxy().setReleaseLabel("松开刷新...");// 下来达到一定距离时，显示的提示
            	
            	if(lastRefreshdt.equals("")) {
            		refreshView.getLoadingLayoutProxy().setLastUpdatedLabel("刚刚");//显示最后更新的时间
            	} else {
            		refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(lastRefreshdt);//显示最后更新的时间
            	}
            	
        	    SimpleDateFormat    formatter    =   new    SimpleDateFormat    ("MM月dd日    HH:mm:ss     ");       
       	        Date    curDate    =   new    Date(System.currentTimeMillis());//获取当前时间       
       	        lastRefreshdt =    formatter.format(curDate);
       	        
                currentPage = 1;
                GetData(currentPage);
            }
            
            //上拉效果
            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView)  
            {
            	refreshView.getLoadingLayoutProxy().setPullLabel("下拉刷新...");// 刚下拉时，显示的提示  
            	refreshView.getLoadingLayoutProxy().setRefreshingLabel("更新中...");// 刷新时  
            	refreshView.getLoadingLayoutProxy().setReleaseLabel("松开刷新...");// 下来达到一定距离时，显示的提示 
                currentPage++;
                GetData(currentPage);
            }
        });
		
		//获取控件并注册  这个是获取 listview 控件
        //ListView actualListView = mPullRefreshListView.getRefreshableView();
        //registerForContextMenu(actualListView); 
        //actualListView.setAdapter(mAdapter);
        
        mPullRefreshListView.setAdapter(mAdapter);
        
        new Handler().postDelayed(new Runnable() {//延迟读取数据
			@Override
			public void run() {
				onRefresh();
			}
		}, 100);
        
        //onRefresh();//进来时直接刷新，直接刷新没有加载图标，不知道为什么
	}
	
	private void onRefresh(){
		mPullRefreshListView.setRefreshing(true);
        //首次加载任务数据 加载任务
        currentPage = 1;
        GetData(currentPage);
	}
	
	private void initIndicator(){
		ILoadingLayout startLabels = mPullRefreshListView.getLoadingLayoutProxy(true,false);
		//startLabels.setTextTypeface()
		startLabels.setPullLabel("下拉刷新...");// 刚下拉时，显示的提示
		startLabels.setRefreshingLabel("加载中...");// 第一次打开和第一次下拉时显示的文字
		startLabels.setReleaseLabel("松开刷新...");// 下来达到一定距离时，显示的提示
		
		ILoadingLayout endLabels = mPullRefreshListView.getLoadingLayoutProxy(false,true);
		endLabels.setPullLabel("上拉刷新...");// 刚上拉时，显示的提示
		endLabels.setRefreshingLabel("加载中...");// 刷新时
		endLabels.setReleaseLabel("松开刷新...");// 上来达到一定距离时，显示的提示
		endLabels.setLastUpdatedLabel("");
	}
	
	//onResume是在启动activity启动之后才能执行的，也就是恢复执行。程序正常启动：onCreate()->onStart()->onResume();
	@Override
	protected void onResume() {   //注意 该函数，如果在打开了其他窗口，再关闭，这个函数也会执行
		super.onResume();
		
	}
	
	public void GetData(int curpage){
		SysPassValue temp = new SysPassValue();
		temp.setFunctionName("getEventDetail");
		temp.setPageNumber(String.valueOf(curpage));
		temp.setId(String.valueOf(id));
		temp.setUserId(String.valueOf(userId));
		
		//调用接口得到数据
		new sendOrderToServerForValueYTask(new AbstractYGetTaskListener< SysPassValue>() {
			@Override
			public void onPostExecute(String name, YGetTask< SysPassValue > task) { 
				mPullRefreshListView.onRefreshComplete(); 
				getData(task.getValue());
			}
		},temp).execute();
	}
	
	public void getData(SysPassValue dbresult) {//通过数据适配器获取 数据列表
		
		View vnorecord = this.findViewById(R.id.activity_no_record_showpic);
		
		if (dbresult==null){
			//ToastUtils.show(this, "NULL！");
			if(currentPage==1){
				this.NewsList.clear();
		    	mAdapter.notifyDataSetChanged();
		    	vnorecord.setVisibility(View.VISIBLE);
		    	//bottomImage.setVisibility(View.VISIBLE);
		    	//bottomWord.setVisibility(View.VISIBLE);
		    	return;
			}
			return;
			
		}
		
		vnorecord.setVisibility(View.GONE);
		
		//bottomImage.setVisibility(View.GONE);
		//bottomWord.setVisibility(View.GONE);
		
		if(contentView==null){
			initContentView();
		}
		
		//如果是第一页则需要清楚数据   currentPage
		
		List<String> listimage1 = dbresult.getStrList1();
		String[] t1 = listimage1.toArray(new String[listimage1.size()]);
		ListView productImagelistView=(ListView)this.findViewById(R.id.xl_wg_event_xlistView);//使用ListView加载列表
		ProductImageAdapter imageAdapter=new ProductImageAdapter(this, t1);
		productImagelistView.setAdapter(imageAdapter);
 	    imageAdapter.setSelectedPosition(0);
 	    imageAdapter.notifyDataSetInvalidated();
 	    int lengths = t1.length;//重新设置图片的高度
 	    int totalpix = lengths * 500;//图片高度*1.75
 	    View v1 = contentView.findViewById(R.id.wg_linearlayout_event_detail_imglist);
		AndroidUtils.setLinearLayoutSize(v1,R.id.wg_linearlayout_event_detail_imglist,"RelativeLayout","1",720,720,totalpix,30,30,0,20);//30是15dp
		
		List<String> listimage2 = dbresult.getStrList2();
		String[] t2 = listimage2.toArray(new String[listimage2.size()]);
		ListView productImagelistView2=(ListView)this.findViewById(R.id.xl_wg_event_xlistView2);
		ProductImageAdapter imageAdapter2=new ProductImageAdapter(this, t2);
		productImagelistView2.setAdapter(imageAdapter2);
 	    imageAdapter2.setSelectedPosition(0);
 	    imageAdapter2.notifyDataSetInvalidated();
 	    int lengths2 = t2.length;
 	    int totalpix2 = lengths2 * 500;
 	    View v2 = contentView.findViewById(R.id.wg_linearlayout_event_detail_imglist2);
		AndroidUtils.setLinearLayoutSize(v2,R.id.wg_linearlayout_event_detail_imglist2,"RelativeLayout","1",720,720,totalpix2,30,30,0,20);
		
		List<SysPassList> list1 = dbresult.getList1();
		
		List<SysPassValue> list = new ArrayList<SysPassValue>();//为列表面板设置参数
		
		String sid =  dbresult.getId();
		String arg1 =  dbresult.getArg1();
		String arg2 =  dbresult.getArg2();
		String arg3 =  dbresult.getArg3();
		String arg4 =  dbresult.getArg4();
		String arg5 =  dbresult.getArg5();
		String arg6 =  dbresult.getArg6();
		String arg7 =  dbresult.getArg7();
		//String arg8 =  dbresult.getArg8();
		String arg9 =  dbresult.getArg9();
		String arg10 =  dbresult.getArg10();
		//String arg11 =  dbresult.getArg11();
		//String arg12 =  dbresult.getArg12();
		
		AndroidUtils.setTextView(this, R.id.wg_eventdetail_textview1,arg1);
		AndroidUtils.setTextView(contentView, R.id.wg_eventdetail_textview2,arg2);
		AndroidUtils.setTextView(contentView, R.id.wg_eventdetail_textview3,arg3);
		AndroidUtils.setTextView(contentView, R.id.wg_eventdetail_textview4,arg4);
		AndroidUtils.setTextView(contentView, R.id.wg_eventdetail_textview5,arg5);
		AndroidUtils.setTextView(contentView, R.id.wg_eventdetail_textview6,arg6);
		AndroidUtils.setTextView(contentView, R.id.wg_eventdetail_textview7,arg7);
		//AndroidUtils.setTextView(contentView, R.id.wg_eventdetail_textview8,arg8);
		AndroidUtils.setTextView(contentView, R.id.wg_eventdetail_textview9,arg9);
		AndroidUtils.setTextView(contentView, R.id.wg_eventdetail_textview10,arg10);
		
		//明细数据
		for(int i=0;i<list1.size();i++){
			SysPassValue curobject = new SysPassValue(); 
			curobject.setId(String.valueOf(i));//list1.get(i).getArgid()，这里还是设置一下setId吧，因为UniteListAdapter里面getView会调用id，datagrid.getId();
			curobject.setArg1(list1.get(i).getArg1());
			curobject.setArg2(list1.get(i).getArg2());
			curobject.setArg3(list1.get(i).getArg3());
			curobject.setArg4(list1.get(i).getArg4());
			curobject.setArg5(list1.get(i).getArg5());
			curobject.setArg6(list1.get(i).getArg6());
			curobject.setArg7(list1.get(i).getArg7());
			curobject.setArg8(list1.get(i).getArg8());
			list.add(curobject);
		}
		  
		if(list1.size()>0){
			this.NewsList.clear();
			this.NewsList.addAll(list);//将详情里面的列表内容传递过去
		}
		
		/*
		if(list1.size()==0){//如果列表内容为空，则不会加载列表，所以不要设置了
		}
		*/
	}
	
	@Override
	public void onClick(View view) {
		if(R.id.wg_head_btn_return==view.getId()) {
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		 }
		 
		 if(R.id.wg_eventlist_btn2==view.getId()){
			 ToastUtils.show(this, "点击了列表");
		 }
		 
	}
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		Log.i("longhua","onItemClick");
		return;
	}
	
	/*
	 * 初始化内容面板
	 */
	private void initContentView(){
		contentView = View.inflate(this, R.layout.item_wg_event_detail,null);
		listView.addHeaderView(contentView);
	}
	
	/*
	private void initDetailerView(){
		detailerView = View.inflate(this, R.layout.item_spc_myorder_view_detail,null);
		listview.addFooterView(detailerView);
	}
	*/
	
	
	
}
