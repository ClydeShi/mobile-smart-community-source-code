package com.zhyq;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.util.common.StrEdit;
import com.zhyq.libs.Session;
import com.zhyq.libs.ToastUtils;
import com.zhyq.widget.LoadingRelativeLayout;


public class ZyPayResultActivity extends Activity implements OnClickListener {
	
	private LoadingRelativeLayout loading = null;
	
	private View includeTitle;
	 
	private String userCode = "";
	private String userName = "";
	private String userId = "";
	private int id = 0;
	private String name = "";
	private String amount = "";
	private String payWay = "";

	private ImageView iv_fkimage;
	private TextView tv_fkstatus;
    
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.setContentView(R.layout.activity_zy_pay_result);
		loading = LoadingRelativeLayout.getLoadingRelativeLayout(this);

		Session session = Session.getSession();
		String message = (String) session.get("message");
		String pay_result = (String) session.get("pay_result");
		String pay_amount = "￥"+(String) session.get("pay_amount");
		if(message==null){
			message = "";
		}
		if(pay_result==null){
			pay_result = "";
		}
		if(pay_amount==null){
			pay_amount = "";
		}

		iv_fkimage =(ImageView)this.findViewById(R.id.iv_pay_result_forhh);
		tv_fkstatus = (TextView)this.findViewById(R.id.tv_pay_result_forhh);

		((TextView)this.findViewById(R.id.tv_result_paymoney)).setText(pay_amount);

		if(pay_result.equals("success")){
			if(message!=null){
				tv_fkstatus.setText(message);
			}else{
				tv_fkstatus.setText("支付成功");
			}
			iv_fkimage.setImageResource(R.drawable.spc_bt_fk_chenggong);
		}else{
			if(message!=null){
				tv_fkstatus.setText(message);
			}else{
				tv_fkstatus.setText("支付失败");
			}
			iv_fkimage.setImageResource(R.drawable.hh_ic_zhifu_false);
		}

		//bt_pay_finish
		((Button)this.findViewById(R.id.bt_pay_finish)).setOnClickListener(this);
	}
	
	//onResume是在启动activity启动之后才能执行的，也就是恢复执行。程序正常启动：onCreate()->onStart()->onResume();
	@Override
	protected void onResume() {   //该函数，如果在打开了其他窗口，再关闭，这个函数也会执行
		super.onResume();
		
	}
	
	@Override
	public void onClick(View view) {

		if (R.id.bt_pay_finish == view.getId()) {
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		}

	}
 
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}

 
}
