package com.zhyq;


import android.os.Bundle;
import android.os.Handler;

import android.support.v4.app.FragmentActivity;

import android.view.View;
import android.view.View.OnClickListener; 
import android.widget.Button;
import android.widget.TextView;
import android.widget.ScrollView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.zhyq.libs.AndroidUtils;
import com.zhyq.libs.ToastUtils;
import com.zhyq.model.SysPassValue;

import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.sendOrderToServerForValueYTask;


public class WgEventViewActivity2 extends FragmentActivity  implements OnClickListener  {//只查看案件详情
	
	private View  childrenView=null;
	private PullToRefreshScrollView  mPullRefreshScrollView=null;
	private ScrollView mScrollView;
 
	private View  includeTitle;
	private String id = "";
	private Long userId = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_wg_event_detail2);		
		includeTitle = this.findViewById(R.id.wg_eventdetail_head);		
		((TextView)includeTitle.findViewById(R.id.wg_head_title)).setText("案件查看");		
		Button btreturn = (Button)includeTitle.findViewById(R.id.wg_head_btn_return);
		btreturn.setOnClickListener(this);
		
		if(HhApplication.getInstance(this).getHhCart().getUser()==null){
			userId =-1l;
		}else{
			userId = HhApplication.getInstance(this).getHhCart().getUser().getId();
		}
		id = this.getIntent().getStringExtra("id");
		
		mPullRefreshScrollView = (PullToRefreshScrollView)this.findViewById(R.id.wg_eventdetail_refresh_scrollview); 
		mScrollView = mPullRefreshScrollView.getRefreshableView();
		
		//刷新
		mPullRefreshScrollView.setOnRefreshListener(new OnRefreshListener<ScrollView>(){
			@Override 
			public void onRefresh(PullToRefreshBase<ScrollView> refreshView){ 
				readData();
			}
		});
		
		//延迟读取数据
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				onRefresh();
			}
		}, 100);
		
		/*		
		mLayout = new DefaultLoadingLayout(this, mValue);
		
		new Thread(new Runnable() {
            @Override
            public void run() {
                // 这里是模拟下载数据的耗时过程
            	SysPassValue temp = new SysPassValue();
        		temp.setFunctionName("getContactDetail");
        		temp.setId(String.valueOf(contactId));
        		temp.setUserId(String.valueOf(userId));
        		
        		new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
        			@Override
        			public void onPostExecute(String name, YGetTask<SysPassValue> task) {
        				//loading.hideLoading();
        				getData(task.getValue());
        			}
        		},temp).execute();
                // 数据下载完毕后,通知handler
                mHandler.sendEmptyMessage(1);
            }
        }).start();
        */
		
	}
	
	/*
	private Handler mHandler = new Handler(){
		@Override
        public void handleMessage(Message msg) {
            // 数据下载完成，转换状态，显示内容视图
            //mLayout.onDone();
        }
	};
	*/
	
	//onResume是在启动activity启动之后才能执行的，也就是恢复执行。程序正常启动：onCreate()->onStart()->onResume();
	@Override
	protected void onResume() {//该函数，如果在打开了其他窗口，再关闭，这个函数也会执行
		// TODO Auto-generated method stub
		super.onResume();
		//Log.i("huahua","taskId = "+taskId);
	}
	
	@Override
	public void onClick(View view) {
		
		if(R.id.wg_head_btn_return==view.getId()){
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		}
		
		return;
	}
	
	private void readData(){
		SysPassValue temp = new SysPassValue();
		temp.setFunctionName("getEventDetail");
		temp.setId(String.valueOf(id));
		temp.setUserId(String.valueOf(userId));
		
		new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
			@Override
			public void onPostExecute(String name, YGetTask<SysPassValue> task) {
				mPullRefreshScrollView.onRefreshComplete();
				getData(task.getValue());
			}
		},temp).execute();
	}
	
	private void getData(SysPassValue passValue) {
		if (passValue==null) {
			ToastUtils.show(this,"没有获取到信息");
			return;
		} else {
			
			if(childrenView==null){
				//加载内容面板
				initContentView();
				//设置监听
				//((Button)childrenView.findViewById(R.id.btn_recharge_zhuanzhang)).setOnClickListener(this);
				//vBt1 =   childrenView.findViewById(R.id.btn_shop_chongzhi_flow1);
				//btn_shop_chongzhi_flow1 = (Button)vBt1.findViewById(R.id.hh_btn_chongzhi);
				//btn_shop_chongzhi_flow1.setOnClickListener(this);
				//childrenView.findViewById(R.id.btn_recharge_vip_payway_zhifubao_right_circleimage).setOnClickListener(this);
				//intUI();
			}
			 /*((TextView)includeMaster.findViewById(R.id.tv_masterdetail_head_image_shopname)).setText(masterviewdetail.getArg1());  
			 AndroidUtils.setWebImageView(includeMaster, R.id.hh_masterdetail_head_image_shoplogimage, masterviewdetail.getArg2()); 
			 AndroidUtils.setWebImageView(includeMaster, R.id.hh_masterdetail_head_image_masterimage, masterviewdetail.getArg3());*/
			 
			String sid =  passValue.getId();
			String arg1 =  passValue.getArg1();
			String arg2 =  passValue.getArg2();
			String arg3 =  passValue.getArg3();
			String arg4 =  passValue.getArg4();
			String arg5 =  passValue.getArg5();
			String arg6 =  passValue.getArg6();
			String arg7 =  passValue.getArg7();
			String arg8 =  passValue.getArg8();
			String arg9 =  passValue.getArg9();
			String arg10 =  passValue.getArg10();
			String arg11 =  passValue.getArg11();
			String arg12 =  passValue.getArg12();
			
			AndroidUtils.setTextView(this, R.id.wg_eventdetail_textview1,arg1);
			AndroidUtils.setTextView(childrenView, R.id.wg_eventdetail_textview2,arg2);
			AndroidUtils.setTextView(childrenView, R.id.wg_eventdetail_textview3,arg3);
			AndroidUtils.setTextView(childrenView, R.id.wg_eventdetail_textview4,arg4);
			AndroidUtils.setTextView(childrenView, R.id.wg_eventdetail_textview5,arg5);
			AndroidUtils.setTextView(childrenView, R.id.wg_eventdetail_textview6,arg6);
			AndroidUtils.setTextView(childrenView, R.id.wg_eventdetail_textview7,arg7);
			//AndroidUtils.setTextView(childrenView, R.id.wg_eventdetail_textview8,arg8);
			AndroidUtils.setTextView(childrenView, R.id.wg_eventdetail_textview9,arg9);
			AndroidUtils.setTextView(childrenView, R.id.wg_eventdetail_textview10,arg10);
			AndroidUtils.setTextView(childrenView, R.id.wg_eventdetail_textview11,arg11);
			AndroidUtils.setTextView(childrenView, R.id.wg_eventdetail_textview12,arg12);
			//AndroidUtils.setWebImageView(this, R.id.wg_contact_round_img, arg1);
			//AndroidUtils.setWebImageCircleView(childrenView, R.id.wg_contact_round_img,arg1);
			
			//((ImageView)includeTitle.findViewById(R.id.iv_hh_head_masterdetail_right_saygoods)).setBackgroundResource(R.drawable.praisetech);
			 
		}
	}
	
	private void intUI(){
		
	}
	
	private void initContentView(){ 
		childrenView = View.inflate(this, R.layout.activity_wg_event_detail_inlayout2,null);
		 
		//mscrollView.addHeaderView(headerView);
		mPullRefreshScrollView.addView(childrenView);
	}
	
	private void onRefresh(){
		 mPullRefreshScrollView.setRefreshing(true); 
	     //首次加载任务数据 加载任务   
		 readData();
	}
	
	
	
}
