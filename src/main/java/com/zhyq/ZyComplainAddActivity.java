package com.zhyq;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.tools.PictureFileUtils;
import com.util.common.StrEdit;
import com.zhyq.adapter.GridImageAdapter;
import com.zhyq.libs.AndroidUtils;
import com.zhyq.libs.PreferenceUtils;
import com.zhyq.libs.ToastUtils;
import com.zhyq.libs.view.wheelview.JudgeDate;
import com.zhyq.libs.view.wheelview.ScreenInfo;
import com.zhyq.libs.view.wheelview.WheelMain;
import com.zhyq.model.SysPassValue;
import com.zhyq.model.User;
import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.sendOrderToServerForValueYTask;
import com.zhyq.widget.LoadingRelativeLayout;

import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class ZyComplainAddActivity extends Activity implements OnClickListener {

	static boolean active = false;

	private LoadingRelativeLayout loading=null;
	private View includeTitle ;
	private ImageView returnImage;
	private TextView returnText;
	private Button btSave;
	private Button topSave;
	private Button btDel;
	WheelMain wheelMain;
	private PopupWindow popupWindow;
	private Long addressId = 0l;
	private float alpha = 1f;

	TextView txtstartdate;
	TextView txtenddate;
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	private int id = 0;
	private Long userId = null;

	public static int request_Code = 9;
	public static int resultCode = 11;
	public static int resultCodeDelete = 12;

	private String usercode = "";

	private RecyclerView recyclerView;
	private GridImageAdapter adapter;
	private int maxSelectNum = 3;
	private List<LocalMedia> selectList = new ArrayList<>();
	private int themeId;

	private String uploadPictureIds = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.setContentView(R.layout.activity_zy_complain_add);
		themeId = R.style.picture_default_style;
		loading = LoadingRelativeLayout.getLoadingRelativeLayout(this);
		includeTitle = this.findViewById(R.id.zy_add_head);
		((TextView)includeTitle.findViewById(R.id.wg_head_add_title)).setText("投诉建议");
		Button btreturn = (Button)includeTitle.findViewById(R.id.wg_head_add_btn_return);
		btreturn.setOnClickListener(this);

		((TextView)includeTitle.findViewById(R.id.wg_util_head_text_add)).setText("提交");

		topSave =(Button)includeTitle.findViewById(R.id.wg_util_head_btn_add);
		topSave.setOnClickListener(this);

		btSave =(Button)this.findViewById(R.id.wg_planadd_btn_save);
		btSave.setOnClickListener(this);

		//(this.findViewById(R.id.zy_add_button_01)).setOnClickListener(this);
		//(this.findViewById(R.id.zy_add_button_02)).setOnClickListener(this);
		(this.findViewById(R.id.zy_add_button_picture)).setOnClickListener(this);

		txtstartdate = ((TextView)this.findViewById(R.id.zy_textview_04));
		this.findViewById(R.id.btn_start_date).setOnClickListener(this);

		recyclerView = (RecyclerView) findViewById(R.id.recycler);
		FullyGridLayoutManager manager = new FullyGridLayoutManager(ZyComplainAddActivity.this, 4, GridLayoutManager.VERTICAL, false);
		recyclerView.setLayoutManager(manager);
		adapter = new GridImageAdapter(ZyComplainAddActivity.this, onAddPicClickListener);
		adapter.setList(selectList);
		adapter.setSelectMax(maxSelectNum);
		recyclerView.setAdapter(adapter);
		adapter.setOnItemClickListener(new GridImageAdapter.OnItemClickListener() {
			@Override
			public void onItemClick(int position, View v) {
				if (selectList.size() > 0) {
					LocalMedia media = selectList.get(position);
					String pictureType = media.getPictureType();
					int mediaType = PictureMimeType.pictureToVideo(pictureType);
					switch (mediaType) {
						case 1:
							// 预览图片 可自定长按保存路径
							PictureSelector.create(ZyComplainAddActivity.this).externalPicturePreview(position, "/custom_file", selectList);
							PictureSelector.create(ZyComplainAddActivity.this).externalPicturePreview(position, selectList);
							break;
						case 2:
							// 预览视频
							PictureSelector.create(ZyComplainAddActivity.this).externalPictureVideo(media.getPath());
							break;
						case 3:
							// 预览音频
							PictureSelector.create(ZyComplainAddActivity.this).externalPictureAudio(media.getPath());
							break;
					}
				}
			}
		});

		/*(this.findViewById(R.id.wg_planadd5_btn)).setOnClickListener(this);
		(this.findViewById(R.id.wg_planadd6_btn)).setOnClickListener(this);
		(this.findViewById(R.id.wg_planadd7_btn)).setOnClickListener(this);

		txtenddate = ((TextView)this.findViewById(R.id.wg_date_end));
		this.findViewById(R.id.btn_end_date).setOnClickListener(this);*/

		id = StrEdit.StrToInt(this.getIntent().getStringExtra("id"));
		//Log.i("luktel","id"+id);

		userId = PreferenceUtils.getLong(this,"userid",-1);
		usercode = PreferenceUtils.getString(this,"usercode","");

		loading.showLoading();
		getData();

		//ColorStateList redColor=getResources().getColorStateList(R.color.red);//删除按钮字体设置为红色
		//btDel.setTextColor(redColor);
	}

	@Override
	protected void onResume() {
		super.onResume();

	}

	protected void getData() {
		SysPassValue temp = new SysPassValue();
		temp.setFunctionName("getUserDetails");
		temp.setId(String.valueOf(id));
		temp.setUserId(String.valueOf(userId));

		new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
			@Override
			public void onPostExecute(String name, YGetTask<SysPassValue> task) {
				showUserData(task.getValue());
				loading.hideLoading();
				//loading.hideLoading();
			}
		},temp).execute();
		/*SysPassValue temp = new SysPassValue();
		temp.setFunctionName("getWorkDetail");
		temp.setId(String.valueOf(id));
		temp.setUserId(String.valueOf(userId));

		new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
			@Override
			public void onPostExecute(String name, YGetTask<SysPassValue> task) {
				getDataNew(task.getValue());
				loading.hideLoading();
			}
		},temp).execute();*/
		/*new GetTaskDetailYTask(new AbstractYGetTaskListener<CommonData>() {
			@Override
			public void onPostExecute(String name, YGetTask<CommonData> task) {//onPostExecute是AbstractYGetTaskListener里的方法

				getData(task.getValue());//task.getValue()是CommonData定义的对象，这个是返回的结果
				loading.hideLoading();
			}
		},String.valueOf(planId),String.valueOf(this.userId)).execute();*/
	}

	private void getDataNew(SysPassValue passValue) {
		if (passValue==null) {
			ToastUtils.show(this,"没有获取到信息");
			return;
		} else {
			 /*((TextView)includeMaster.findViewById(R.id.tv_masterdetail_head_image_shopname)).setText(masterviewdetail.getArg1());
			 AndroidUtils.setWebImageView(includeMaster, R.id.hh_masterdetail_head_image_shoplogimage, masterviewdetail.getArg2());
			 AndroidUtils.setWebImageView(includeMaster, R.id.hh_masterdetail_head_image_masterimage, masterviewdetail.getArg3());*/

			String sid =  passValue.getId();
			String arg1 =  passValue.getArg1();
			String arg2 =  passValue.getArg2();
			String arg3 =  passValue.getArg3();
			String arg4 =  passValue.getArg4();
			String arg5 =  passValue.getArg5();
			String arg6 =  passValue.getArg6();
			String arg7 =  passValue.getArg7();
			String arg8 =  passValue.getArg8();
			String arg9 =  passValue.getArg9();
			String arg10 =  passValue.getArg10();

			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout2_textview,arg1);
			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout8_textview,arg7);
			//AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout9_textview,varValue9);
			//AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout10_textview,varValue10);

			//((ImageView)includeTitle.findViewById(R.id.iv_hh_head_masterdetail_right_saygoods)).setBackgroundResource(R.drawable.praisetech);
		}
	}

	private void showUserData(SysPassValue passValue) {
		if (passValue==null) {
			ToastUtils.show(this,"没有获取到信息");
			return;
		} else {
			 /*((TextView)includeMaster.findViewById(R.id.tv_masterdetail_head_image_shopname)).setText(masterviewdetail.getArg1());
			 AndroidUtils.setWebImageView(includeMaster, R.id.hh_masterdetail_head_image_shoplogimage, masterviewdetail.getArg2());
			 AndroidUtils.setWebImageView(includeMaster, R.id.hh_masterdetail_head_image_masterimage, masterviewdetail.getArg3());*/

			String sid =  passValue.getId();
			String arg1 =  passValue.getArg1();
			String arg2 =  passValue.getArg2();
			String arg3 =  passValue.getArg3();
			String arg4 =  passValue.getArg4();
			String arg5 =  passValue.getArg5();
			String arg6 =  passValue.getArg6();
			String arg7 =  passValue.getArg7();
			String arg8 =  passValue.getArg8();
			String arg9 =  passValue.getArg9();
			String arg10 =  passValue.getArg10();
			String arg11 =  passValue.getArg11();
			String arg12 =  passValue.getArg12();
			String arg13 =  passValue.getArg13();
			String arg20 =  passValue.getArg20();
			String arg42 =  passValue.getArg42();

			AndroidUtils.setTextView(this, R.id.zy_field_01id,arg42);
			AndroidUtils.setTextView(this, R.id.zy_field_01,arg20);
			AndroidUtils.setTextView(this, R.id.zy_field_03,arg3);

			//((ImageView)includeTitle.findViewById(R.id.iv_hh_head_masterdetail_right_saygoods)).setBackgroundResource(R.drawable.praisetech);
		}
	}

	@Override
	public void onClick(View view) {

		if(R.id.wg_head_add_btn_return==view.getId()){
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		}

		if(R.id.wg_planadd_btn_save==view.getId()){
			SavePlan();
			return;
		}

		if(R.id.wg_util_head_btn_add==view.getId()){
			SavePlan();
			return;
		}

		/*if(R.id.zy_add_button_picture==view.getId()){
			PictureSelector.create(ZyProblemAddActivity.this)
					.openGallery(PictureMimeType.ofImage())
					.maxSelectNum(3)
					.minSelectNum(1)
					.imageSpanCount(4)
					//.selectionMode(PictureConfig.SINGLE)
					.isCamera(true)
					.previewImage(true)
					//.enableCrop(true)
					//.compress(true)
					//.compressMode(PictureConfig.SYSTEM_COMPRESS_MODE)
					//.glideOverride(300,300)
					//.withAspectRatio(1,1)
					//.hideBottomControls(true)
					//.showCropFrame(true)
					//.cropCompressQuality(90)
					//.compressMaxKB(200)
					//.compressWH(300,300)
					//.cropWH(300,300)
					.forResult(PictureConfig.CHOOSE_REQUEST);

			return;
		}*/

		/*if(R.id.zy_add_button_01==view.getId()){//选择楼栋

			Long userId = PreferenceUtils.getLong(this, "userid", -1);
			String usercode = PreferenceUtils.getString(this,"usercode","");

			Intent intent = new Intent(this, ZySelectBuildingActivity.class);//WgPlanSelectCategoryActivity  WgPlanSelectWgUserNewActivity
			Bundle bundle = new Bundle();
			bundle.putString("clickfrom", "problemadd");
			bundle.putString("userId", String.valueOf(userId));
			intent.putExtras(bundle);
			startActivityForResult(intent,request_Code);//这里的request_Code要大于0，现在用的9是用来onActivityResult里判断由这里发出的新Activity，获取返回值的请求码
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);//设置新面板右边进入，原来面板左边退出
			return;
		}

		if(R.id.zy_add_button_02==view.getId()) {//紧急程度

			Intent intent = new Intent(this, ZyUrgentSelectActivity.class);
			Bundle bundle = new Bundle();
			bundle.putString("clickfrom", "problemadd");
			intent.putExtras(bundle);
			startActivityForResult(intent,request_Code);
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);//设置新面板右边进入，原来面板左边退出
			return;
		}*/

		/*if(R.id.wg_planadd5_btn==view.getId()){//选择网格

			Long userId = PreferenceUtils.getLong(this, "userid", -1);
			String usercode = PreferenceUtils.getString(this,"usercode","");

			TextView textview_street = ((TextView)this.findViewById(R.id.wg_planadd_layout4_textview3));//把街道传递过去
			String street = textview_street.getText().toString();
			if(street.equals("选择街道")){
				street = "";
				//ToastUtils.show(this, "请先选择街道！");
			}

			Intent intent=new Intent(this, WgPlanSelectWangGeActivity.class);
			Bundle bundle=new Bundle();
			bundle.putString("street", street);
			bundle.putString("clickfrom", "");
			intent.putExtras(bundle);
			startActivityForResult(intent,request_Code);
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);//设置新面板右边进入，原来面板左边退出
			return;
		}

		if(R.id.wg_planadd6_btn==view.getId()){//选择网格员

			Long userId = PreferenceUtils.getLong(this, "userid", -1);
			String usercode = PreferenceUtils.getString(this,"usercode","");

			TextView textview_street = ((TextView)this.findViewById(R.id.wg_planadd_layout4_textview3));//把街道传递过去
			String street = textview_street.getText().toString();
			if(street.equals("选择街道")){
				street = "";
			}

			Intent intent=new Intent(this, WgPlanSelectWgUserNewActivity.class);//WgPlanSelectWgUserActivity
			Bundle bundle=new Bundle();
			bundle.putString("street", street);
			bundle.putString("clickfrom", "");
			intent.putExtras(bundle);
			startActivityForResult(intent,request_Code);
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);//设置新面板右边进入，原来面板左边退出
			return;
		}

		if(R.id.wg_planadd7_btn==view.getId()){//选择检查人员

			Long userId = PreferenceUtils.getLong(this, "userid", -1);
			String usercode = PreferenceUtils.getString(this,"usercode","");

			Intent intent=new Intent(this, WgPlanSelectUserActivity.class);
			Bundle bundle=new Bundle();
			bundle.putString("userId", String.valueOf(userId));
			intent.putExtras(bundle);
			startActivityForResult(intent,request_Code);
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);//设置新面板右边进入，原来面板左边退出
			return;
		}*/

		if(R.id.btn_start_date==view.getId()){
			bottombirthdaywindow(txtstartdate, "txtstartdate");
			new Thread(new Runnable(){
                @Override
                public void run() {
                    while(alpha>0.5f){
                        try {
                            //4是根据弹出动画时间和减少的透明度计算
                            Thread.sleep(4);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Message msg =mHandler.obtainMessage();
                        msg.what = 1;
                        //每次减少0.01，精度越高，变暗的效果越流畅
                        alpha-=0.01f;
                        msg.obj =alpha ;
                        mHandler.sendMessage(msg);
                    }
                }

            }).start();
		}

		/*if(R.id.btn_end_date==view.getId()){
			bottombirthdaywindow(txtenddate, "txtenddate");
			new Thread(new Runnable(){
                @Override
                public void run() {
                    while(alpha>0.5f){
                        try {
                            //4是根据弹出动画时间和减少的透明度计算
                            Thread.sleep(4);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Message msg =mHandler.obtainMessage();
                        msg.what = 1;
                        //每次减少0.01，精度越高，变暗的效果越流畅
                        alpha-=0.01f;
                        msg.obj =alpha ;
                        mHandler.sendMessage(msg);
                    }
                }

            }).start();
		}*/
	}

	private GridImageAdapter.onAddPicClickListener onAddPicClickListener = new GridImageAdapter.onAddPicClickListener() {
		@Override
		public void onAddPicClick() {
			// 进入相册 以下是例子：不需要的api可以不写
			PictureSelector.create(ZyComplainAddActivity.this)
					.openGallery(PictureMimeType.ofImage())// 全部.PictureMimeType.ofAll()、图片.ofImage()、视频.ofVideo()、音频.ofAudio()
					.theme(themeId)// 主题样式设置 具体参考 values/styles   用法：R.style.picture.white.style
					.maxSelectNum(3)// 最大图片选择数量
					.minSelectNum(1)// 最小选择数量
					.imageSpanCount(4)// 每行显示个数
					.isCamera(true)
					.previewImage(true)
					.compress(true)
					.compressMode(PictureConfig.SYSTEM_COMPRESS_MODE)
					.glideOverride(300,300)
					.withAspectRatio(1,1)
					.hideBottomControls(true)
					.cropCompressQuality(90)
					.compressMaxKB(800)
					.compressWH(1024,768)
					.forResult(PictureConfig.CHOOSE_REQUEST);//结果回调onActivityResult code
		}

	};

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		//下面是针对 图片裁剪  IMAGE_REQUEST_CODE
		switch (requestCode) {

			case PictureConfig.CHOOSE_REQUEST:
				// 图片选择结果回调
				List<LocalMedia> selectListNew = new ArrayList<>();
				selectListNew = PictureSelector.obtainMultipleResult(data);
				if(selectListNew.size()>0){
					selectList.addAll(selectListNew);
					// 例如 LocalMedia 里面返回三种path
					// 1.media.getPath(); 为原图path
					// 2.media.getCutPath();为裁剪后path，需判断media.isCut();是否为true
					// 3.media.getCompressPath();为压缩后path，需判断media.isCompressed();是否为true
					// 如果裁剪并压缩了，已取压缩路径为准，因为是先裁剪后压缩的
					for (LocalMedia media : selectListNew) {
						Log.i("luktel","图片-----》"+media.getPath());
						String ls_imagename = media.getPath();
						ls_imagename = media.getCompressPath();
						String ls_face = usercode + userId + StrEdit.getDateForFileName() + "_" + media.getNum() + ".jpg";
						Log.i("luktel","ls_imagename="+ls_imagename);
						Log.i("luktel","ls_face="+ls_face);
						loading.showLoading();
						new ZyComplainAddActivity.FileupThread(ls_imagename,ls_face).start();
						//loading.hideLoading();
					}
					adapter.setList(selectList);
					adapter.notifyDataSetChanged();
				}
				break;

		}

		if (requestCode == request_Code) {

			if (resultCode == 1){//结果标识
				if (data != null) {
					Bundle b = data.getExtras();
					String categoryId = b.getString("id");
	         	 	String categoryName = b.getString("name");
	         	 	AndroidUtils.setTextView(this, R.id.zy_textview_01id,categoryId);
	            	AndroidUtils.setTextView(this, R.id.zy_textview_01,categoryName);
				}
			}

			if (resultCode == 2){//紧急程度
				if (data != null) {
					Bundle b = data.getExtras();
	         	 	String name = b.getString("name");
	         	 	if(!name.equals("")){
	         	 		AndroidUtils.setTextView(this, R.id.zy_textview_03,name);
	         	 	}
				}
			}

			if (resultCode == 3){//网格员
				if (data != null) {
					Bundle b = data.getExtras();
					//String id = b.getString("id");
					String name = b.getString("name");
					if(!name.equals("")){
						AndroidUtils.setTextView(this, R.id.wg_planadd_layout6_textview3,name);
					}
				}
			}

			if (resultCode == 5){//检查人员
				if (data != null) {
					Bundle b = data.getExtras();
					String id = b.getString("id");
					String name = b.getString("name");
					if(!name.equals("")){
						AndroidUtils.setTextView(this, R.id.wg_planadd_layout7_textview3_id,id);
		            	AndroidUtils.setTextView(this, R.id.wg_planadd_layout7_textview3,name);
					}
				}
			}

			if (resultCode == 10){//街道
				if (data != null) {
					Bundle b = data.getExtras();
	         	 	String name = b.getString("name");
	         	 	if(!name.equals("")){
	         	 		AndroidUtils.setTextView(this, R.id.wg_planadd_layout4_textview3,name);
	         	 	}
				}
			}

		}

		super.onActivityResult(requestCode, resultCode, data);
	}

	public void SavePlan(){
    	Long userid = null;
    	User curuser = new User();
    	curuser = HhApplication.getInstance(this).getHhCart().getUser();
    	if(curuser==null){
    		userid =-1l;
      	}else{
      		userid = HhApplication.getInstance(this).getHhCart().getUser().getId();
      	}
    	//Long userid = HhApplication.getInstance(this).getHhCart().getUser().getId();//未登录会出错

    	Long userId = PreferenceUtils.getLong(this,"userid",-1);
    	String userCode = PreferenceUtils.getString(this,"usercode","");
		String userName = PreferenceUtils.getString(this,"username","");

		//String buildingId = ((TextView)this.findViewById(R.id.zy_textview_01id)).getText().toString();
		//String buildingName = ((TextView)this.findViewById(R.id.zy_textview_01)).getText().toString();
		//String address = AndroidUtils.getEditText(this, R.id.zy_edittext_02);
		//String degree = ((TextView)this.findViewById(R.id.zy_textview_03)).getText().toString();
		//String date1 = ((TextView)this.findViewById(R.id.zy_textview_04)).getText().toString();
		String date1 = StrEdit.getDate("yyyy-MM-dd HH:mm:ss");

  	    /*String title = AndroidUtils.getEditText(this, R.id.wg_planadd_edittext_name);

  	    TextView txtcategory = ((TextView)this.findViewById(R.id.wg_planadd_layout3_textview3_id));
		String category = txtcategory.getText().toString();

	    TextView txtstreet = ((TextView)this.findViewById(R.id.wg_planadd_layout4_textview3));
  	    String street = txtstreet.getText().toString();

	    String wangge = ((TextView)this.findViewById(R.id.wg_planadd_layout5_textview3)).getText().toString();
	    String wanggeuser = ((TextView)this.findViewById(R.id.wg_planadd_layout6_textview3)).getText().toString();
	    String checkuserId = ((TextView)this.findViewById(R.id.wg_planadd_layout7_textview3_id)).getText().toString();
	    String checkuserName = ((TextView)this.findViewById(R.id.wg_planadd_layout7_textview3)).getText().toString();
	    String startdate = ((TextView)this.findViewById(R.id.wg_date_start)).getText().toString();
	    String enddate = ((TextView)this.findViewById(R.id.wg_date_end)).getText().toString();*/
		String zy_field_01id = ((TextView)this.findViewById(R.id.zy_field_01id)).getText().toString();
		String zy_field_01 = ((TextView)this.findViewById(R.id.zy_field_01)).getText().toString();
		String zy_field_03 = ((TextView)this.findViewById(R.id.zy_field_03)).getText().toString();
		String title = AndroidUtils.getEditText(this, R.id.zy_textview_05);
  	    String content = AndroidUtils.getEditText(this, R.id.zy_textview_06);

  	    /*if(buildingName.equals("") || buildingName.equals("请选择")){
  	    	ToastUtils.show(this, "请选择楼栋！");
  		    return;
  	    }*/

  	    if(title.equals("")){
	    	ToastUtils.show(this, "请输入投诉标题！");
		    return;
	    }

  	    /*if(street.equals("选择街道")){
  	    	ToastUtils.show(this, "请选择街道！");
  		    return;
  	    }

  	    if(checkuserName.equals("选择检查人员")){
	    	ToastUtils.show(this, "请选择检查人员！");
		    return;
	    }*/

  	    if(content.equals("")){
	    	ToastUtils.show(this, "请输入投诉内容！");
		    return;
	    }

  	    /*if(date1.equals("请选择")){
	    	ToastUtils.show(this, "请选择发现时间！");
		    return;
	    }*/

  	    /*if(enddate.equals("请选择")){
	    	ToastUtils.show(this, "请选择结束时间！");
		    return;
	    }*/

  	    loading.showLoading();

  	    SysPassValue temp = new SysPassValue();//buildingId buildingName address degree date1 title content
		temp.setFunctionName("saveComplainAdd");
		temp.setUserId(String.valueOf(userId));
		temp.setId(String.valueOf(id));
		temp.setArg1(title);
		temp.setArg2(content);
		temp.setArg3(zy_field_01id);
		temp.setArg4(zy_field_01);
		temp.setArg5(zy_field_03);
		temp.setArg6("");
		temp.setArg7("");
		temp.setArg8(userName);
		temp.setArg12(uploadPictureIds);
		/*temp.setArg8(startdate);
		temp.setArg9(enddate);
		temp.setArg10(content);*/
		//temp.setArg11("1");//shopid

		new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
			@Override
			public void onPostExecute(String name, YGetTask<SysPassValue> task) {
				loading.hideLoading();
				getData(task.getValue());
			}
		},temp).execute();
	}

	private void getData(SysPassValue passValue) {
		if (passValue==null) {
			ToastUtils.show(this, "保存失败！");
		} else {
			String returnResult = passValue.getReturnResult();
			String planId = "";
			if(returnResult.equals("1")){
				Intent intent=new Intent(this, ZySaveSuccessActivity.class);
				Bundle bundle=new Bundle();
				bundle.putString("id", String.valueOf(planId));
				intent.putExtras(bundle);
				startActivity(intent);
				this.finish();
				overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				return;
			}else{
				ToastUtils.show(this, "保存失败，请检查输入的文字！");
			}
		}
	}

    void bottomSelectwindow(TextView view) {
        if (popupWindow != null && popupWindow.isShowing()) {
            return;
        }

      //修改的话，这个地方要修改 *****************
        LinearLayout layout = (LinearLayout) getLayoutInflater().inflate(R.layout.activity_wg_street_popupwindow, null);
        popupWindow = new PopupWindow(layout,
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        //点击空白处时，隐藏掉pop窗口
        popupWindow.setFocusable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        //添加弹出、弹入的动画
        popupWindow.setAnimationStyle(R.style.Popupwindow);
        int[] location = new int[2];
        view.getLocationOnScreen(location);
        popupWindow.showAtLocation(view, Gravity.LEFT | Gravity.BOTTOM, 0, -location[1]);

        //修改的话，这个地方要修改 *****************
        //添加按键事件监听
        setSelectButtonListeners(layout);
        //添加pop窗口关闭事件，主要是实现关闭时改变背景的透明度
        popupWindow.setOnDismissListener(new poponDismissListener());
        backgroundAlpha(1f);
    }

    private void setSelectButtonListeners(LinearLayout layout) {

        Button btn_street_lh = (Button) layout.findViewById(R.id.btn_street_lh);
        Button btn_street_mz = (Button) layout.findViewById(R.id.btn_street_mz);
        Button btn_street_gh = (Button) layout.findViewById(R.id.btn_street_gh);
        Button btn_street_hc = (Button) layout.findViewById(R.id.btn_street_hc);
        Button btn_street_gl = (Button) layout.findViewById(R.id.btn_street_gl);
        Button btn_street_dl = (Button) layout.findViewById(R.id.btn_street_dl);
        Button bt_cancel = (Button) layout.findViewById(R.id.btn_sex_cancel);

        btn_street_lh.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                    //在此处添加你的按键处理
                    popupWindow.dismiss();
                    backgroundAlpha(1f);
                    //loading.showLoading();
                    userModifyData("1","龙华");
                    /*
        			user.setUserSex("男");
        			user.setUserModifyColumnType("2");

        			new HhUserModifyPersonInfoYTask(new AbstractYGetTaskListener<User>() {
        				@Override
        				public void onPostExecute(String name, YGetTask<User> task) {
        					loading.hideLoading();
        					userModifyData(task.getValue(),"2");
        				}
        			},user,"2").execute();
        			*/

                    return;
                }
            }
        });
        btn_street_mz.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                	 //在此处添加你的按键处理
                    popupWindow.dismiss();
                    backgroundAlpha(1f);
                    userModifyData("1","民治");
                    return;
                }
            }
        });
        btn_street_gh.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                	 //在此处添加你的按键处理
                    popupWindow.dismiss();
                    backgroundAlpha(1f);
                    userModifyData("1","观湖");
                    return;
                }
            }
        });
        btn_street_hc.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                	 //在此处添加你的按键处理
                    popupWindow.dismiss();
                    backgroundAlpha(1f);
                    userModifyData("1","福城");
                    return;
                }
            }
        });
        btn_street_gl.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                	 //在此处添加你的按键处理
                    popupWindow.dismiss();
                    backgroundAlpha(1f);
                    userModifyData("1","观澜");
                    return;
                }
            }
        });
        btn_street_dl.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                	 //在此处添加你的按键处理
                    popupWindow.dismiss();
                    backgroundAlpha(1f);
                    userModifyData("1","大浪");
                    return;
                }
            }
        });
        bt_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                    popupWindow.dismiss();
                }
            }
        });
    }

    private void userModifyData(String modifyType,String modifyValue) {

    	if(modifyType.equals("1")){
    		//TextView streetView =(TextView)this.findViewById(R.id.wg_planadd_layout4_textview3);
    		//streetView.setVisibility(View.INVISIBLE);//隐藏选择街道
  			AndroidUtils.setTextView(this, R.id.wg_planadd_layout4_textview3,modifyValue);
    	}

    }

    public String getNowDate(){
        Date currTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd",Locale.US);
        String nowdate = null;
        try {
            nowdate = new String(formatter.format(currTime).getBytes("iso-8859-1"));
        } catch (UnsupportedEncodingException ex) {
        }

        return nowdate;
    }

	//选择日期
	void bottombirthdaywindow(TextView view, String dateType) {
        if (popupWindow != null && popupWindow.isShowing()) {
            return;
        }
        LinearLayout timepickerview = (LinearLayout) getLayoutInflater().inflate(R.layout.timepicker, null);
        wheelMain = new WheelMain(timepickerview);
        ScreenInfo screenInfo = new ScreenInfo(ZyComplainAddActivity.this);
		wheelMain.screenheight = screenInfo.getHeight();
		String time = txtstartdate.getText().toString();
		Calendar calendar = Calendar.getInstance();
		if (JudgeDate.isDate(time, "yyyy-MM-dd")) {
			try {
				calendar.setTime(dateFormat.parse(time));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		wheelMain.initDateTimePicker(year, month, day);

        popupWindow = new PopupWindow(timepickerview,
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        //点击空白处时，隐藏掉pop窗口
        popupWindow.setFocusable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        //添加弹出、弹入的动画
        popupWindow.setAnimationStyle(R.style.Popupwindow);
        int[] location = new int[2];
        view.getLocationOnScreen(location);
        popupWindow.showAtLocation(view, Gravity.LEFT | Gravity.BOTTOM, 0, -location[1]);

        //修改的话，这个地方要修改 *****************
        //添加按键事件监听
        setBirthdayListeners(timepickerview, dateType);
        //添加pop窗口关闭事件，主要是实现关闭时改变背景的透明度
        popupWindow.setOnDismissListener(new poponDismissListener());
        backgroundAlpha(1f);
    }

	//监听日期
	private void setBirthdayListeners(LinearLayout layout, String dateType) {

    	Button bt_select_birthday_ok = (Button) layout.findViewById(R.id.btn_select_birthday_ok);
    	Button bt_select_birthday_cancel = (Button) layout.findViewById(R.id.btn_select_birthday_cancel);

    	ColorStateList whiteColor=getResources().getColorStateList(R.color.popwindowselectok);
    	bt_select_birthday_ok.setTextColor(whiteColor);

    	final String fdateType = dateType;

    	bt_select_birthday_ok.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                    //在此处添加你的按键处理 xxx

                	Log.i("luktel",wheelMain.getTime());

                    popupWindow.dismiss();
                    backgroundAlpha(1f);

                    //loading.showLoading();

                    if(fdateType.equals("txtstartdate")){
                    	txtstartdate.setText(wheelMain.getTime());
                    }

                    if(fdateType.equals("txtenddate")){
                    	txtenddate.setText(wheelMain.getTime());
                    }

                    //loading.hideLoading();
                    /*
        			new HhUserModifyPersonInfoYTask(new AbstractYGetTaskListener<User>() {
        				@Override
        				public void onPostExecute(String name, YGetTask<User> task) {

        					userModifyData(task.getValue(),"3");
        				}
        			},user,"3").execute();
        			*/

                }
            }
        });
    	bt_select_birthday_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                    //在此处添加你的按键处理 xxx
                	 //选照片相
                	popupWindow.dismiss();
                	backgroundAlpha(1f);

                    return;

                }
            }
        });

    	bt_select_birthday_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                    popupWindow.dismiss();
                }
            }
        });
    }
	
	/*
     * 设置添加屏幕的背景透明度
     * @param bgAlpha
     */
    public void backgroundAlpha(float bgAlpha)
    {
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.alpha = bgAlpha; //0.0-1.0
        getWindow().setAttributes(lp);           
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        
       /*
        *    WindowManager.LayoutParams lp = activity.getWindow().getAttributes();  
        lp.alpha = bgAlpha;  
        if (bgAlpha == 1) {  
            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);//不移除该Flag的话,在有视频的页面上的视频会出现黑屏的bug  
        } else {  
            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);//此行代码主要是解决在华为手机上半透明效果无效的bug  
        }  
        activity.getWindow().setAttributes(lp); 
        */
    }
	
	/*
     * 返回或者点击空白位置的时候将背景透明度改回来
     */
    class poponDismissListener implements PopupWindow.OnDismissListener{

        @Override
        public void onDismiss() {
            // TODO Auto-generated method stub
            new Thread(new Runnable(){
                @Override
                public void run() {
                    //此处while的条件alpha不能<= 否则会出现黑屏
                    while(alpha<1f){
                        try {
                            Thread.sleep(4);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Log.d("luktel","alpha:"+alpha);
                        Message msg =mHandler.obtainMessage();
                        msg.what = 1;
                        alpha+=0.01f;
                        msg.obj =alpha ;
                        mHandler.sendMessage(msg);
                    }
                }

            }).start();
        }
    }
    
    Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case 1: 
                	backgroundAlpha(alpha);
                    //backgroundAlpha((float)msg.obj);
                    break;
            }
        }
    };

	class FileupThread extends Thread{
		String ls_imagename;
		String ls_telephone;
		public FileupThread(String imagename,String telephone){
			ls_imagename = imagename;
			ls_telephone = telephone;
		}
		@Override
		public void run() {
			//这里写运行方法，可以直接调用参数 ;
			uploadFile(ls_imagename,ls_telephone) ;
		}
	}

	private void uploadFile(String upfile,String newName){
		//showDialog("开始上传图片");
		String end = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		String actionUrl = "http://139.9.1.194:82/zhyq/clientupfile";
		//String actionUrl = "http://112.74.57.196/hgc/clientupfile";

		try{
			//showDialog("11-上传成功");
			URL url =new URL(actionUrl);
			HttpURLConnection con=(HttpURLConnection)url.openConnection();
            /* 允许Input、Output，不使用Cache */
			con.setDoInput(true);
			con.setDoOutput(true);
			con.setUseCaches(false);
            /* 设置传送的method=POST */
			con.setRequestMethod("POST");
            /* setRequestProperty */
			con.setRequestProperty("Connection", "Keep-Alive");
			con.setRequestProperty("Charset", "UTF-8");
			con.setRequestProperty("Content-Type",
					"multipart/form-data;boundary="+boundary);
            /* 设置DataOutputStream */
			DataOutputStream ds =           new DataOutputStream(con.getOutputStream());
			ds.writeBytes(twoHyphens + boundary + end);
			ds.writeBytes("Content-Disposition: form-data; " +
					"name=\"file1\";filename=\"" +
					newName +"\"" + end);
			ds.writeBytes(end);

            /* 取得文件的FileInputStream */
			FileInputStream fStream = new FileInputStream(upfile);
            /* 设置每次写入1024bytes */
			int bufferSize = 1024;
			byte[] buffer = new byte[bufferSize];

			int length = -1;
            /* 从文件读取数据至缓冲区 */
			while((length = fStream.read(buffer)) != -1){
              /* 将资料写入DataOutputStream中 */
				ds.write(buffer, 0, length);
			}
			ds.writeBytes(end);
			ds.writeBytes(twoHyphens + boundary + twoHyphens + end);

            /* close streams */
			fStream.close();
			ds.flush();

            /* 取得Response内容 */
			InputStream is = con.getInputStream();
			int ch;
			StringBuffer b =new StringBuffer();
			while( ( ch = is.read() ) != -1 ){
				b.append( (char)ch );
			}
            /* 将Response显示于Dialog */
			//showDialog("上传成功"+b.toString().trim());
            /* 关闭DataOutputStream */
			ds.close();
		}catch(Exception e){
			showDialog("上传失败"+e);
			loading.hideLoading();
		}

		PictureFileUtils.deleteCacheDirFile(ZyComplainAddActivity.this);//清理缓存

		//更新数据库
		//Log.i("luktel","begin update");
		//user.setnetpic(newName) ;
		//user.setUserModifyColumnType("0");

		SysPassValue temp = new SysPassValue();//buildingId buildingName address degree date1 title content
		temp.setFunctionName("uploadPicture");
		temp.setUserId(String.valueOf(userId));
		temp.setId(String.valueOf(id));
		temp.setArg1("upload/picture/" + newName);
		temp.setArg2(usercode);
		temp.setArg3("1311");
		temp.setArg4("投诉建议");

		new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
			@Override
			public void onPostExecute(String name, YGetTask<SysPassValue> task) {
				loading.hideLoading();
				showData(task.getValue());
			}
		},temp).execute();

	}

	private void showDialog(String mess){
		Looper.prepare();
		Toast.makeText(getApplicationContext(), "test", Toast.LENGTH_LONG).show();
		new AlertDialog.Builder(ZyComplainAddActivity.this).setTitle("Message")
				.setMessage(mess)
				.setNegativeButton("确定",new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface dialog, int which)
					{
					}
				})
				.show();
		Looper.loop();
	}

	private void showData(SysPassValue passValue) {
		if (passValue==null) {
			ToastUtils.show(this, "数据错误，保存失败！");
			return;
		} else {

			String returnResult = passValue.getReturnResult();
			if(returnResult.equals("1")){
				String arg2 = passValue.getArg2();
				if(uploadPictureIds.equals("")){
					uploadPictureIds = arg2;
				}else{
					uploadPictureIds = uploadPictureIds + "," + arg2;
				}
				Log.i("luktel","uploadPictureIds:"+uploadPictureIds);
			}else{

				ToastUtils.show(this, "修改失败！");
				return;

			}

		}
	}
    
}
