package com.zhyq;

 
import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.zhyq.adapter.WgFeedbackListRecyclerAdapter;
import com.zhyq.libs.BaseActivity;

import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.sendOrderToServerForListYTask;
import com.zhyq.widget.LoadingRelativeLayout;

import android.widget.Button;
import android.content.Intent;

import com.zhyq.model.SysPassValue;
import com.zhyq.model.User;

/*
 * 此文件使用了新的android-support-v4.jar，需要把dex.force.jumbo=true添加到project.properties文件中
 * 使用SwipeRefreshLayout和RecyclerView加载列表，WgFeedbackListRecyclerAdapter中加载列表和底部面板，实现下拉刷新，上拉刷新，底部面板是用来显示加载状态，并延迟一点时间后隐藏
 */

public class WgFeedbackRecyclerActivity extends BaseActivity implements OnClickListener {
	
	private LoadingRelativeLayout loading = null;
	private View includeTitle;
    private SwipeRefreshLayout demo_swiperefreshlayout;
    private RecyclerView demo_recycler;
    private WgFeedbackListRecyclerAdapter adapter;
    private LinearLayoutManager linearLayoutManager;
    private int lastVisibleItem;
    private int currentPage = 1;
    private List<SysPassValue> dataList = new ArrayList<SysPassValue>();//数据
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.setContentView(R.layout.activity_wg_feedback_recyclerlist);
		loading=LoadingRelativeLayout.getLoadingRelativeLayout(this);
		
		includeTitle = this.findViewById(R.id.wg_util_head_feedback_list);
		((TextView)includeTitle.findViewById(R.id.wg_head_title)).setText("检查反馈");
		Button btreturn = (Button)includeTitle.findViewById(R.id.wg_head_btn_return);
		btreturn.setOnClickListener(this);
		
		demo_swiperefreshlayout=(SwipeRefreshLayout)this.findViewById(R.id.demo_swiperefreshlayout);
        demo_recycler=(RecyclerView)this.findViewById(R.id.demo_recycler);
        //设置刷新时动画的颜色，可以设置4个
        demo_swiperefreshlayout.setProgressBackgroundColorSchemeResource(android.R.color.white);
		//demo_swiperefreshlayout.setProgressBackgroundColor(android.R.color.white);
        demo_swiperefreshlayout.setColorSchemeResources(android.R.color.holo_blue_light,
                android.R.color.holo_red_light, android.R.color.holo_orange_light,
                android.R.color.holo_green_light);
        demo_swiperefreshlayout.setProgressViewOffset(false, 0, (int) TypedValue
                .applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, getResources()
                        .getDisplayMetrics()));
        linearLayoutManager=new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(OrientationHelper.VERTICAL);
        demo_recycler.setLayoutManager(linearLayoutManager);
        //添加分隔线 在布局里面添加了
        //demo_recycler.addItemDecoration(new AdvanceDecoration(this, OrientationHelper.VERTICAL));
        
        adapter = new WgFeedbackListRecyclerAdapter(this, dataList);
        adapter.setOnClick1(this);
        demo_recycler.setAdapter(adapter);
        getData();
        //SwipeRefreshLayout上拉监听
        demo_swiperefreshlayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
            	//Log.d("longhua", "invoke onRefresh...");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                    	
                    	currentPage = 1;
                    	getData();
                        demo_swiperefreshlayout.setRefreshing(false);
                    }
                }, 100);
            }
        });
        //RecyclerView滑动监听
        demo_recycler.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE && lastVisibleItem + 1 == adapter.getItemCount()) {
                	//adapter.showFootView();
                    adapter.changeMoreStatus(WgFeedbackListRecyclerAdapter.LOADING_MORE);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                        	currentPage++;
                        	getData();
                            adapter.changeMoreStatus(WgFeedbackListRecyclerAdapter.PULLUP_LOAD_MORE);
                            //adapter.hiddenFootView();
                        }
                    }, 100);
                }
            }
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
            }
        });
    }
	
	public void getData(){
		Long userid = null;
		User curuser = new User();
		curuser = HhApplication.getInstance(this).getHhCart().getUser();
		if(curuser==null){
			userid =-1l;
		}else{
			userid = HhApplication.getInstance(this).getHhCart().getUser().getId();
		}
		
		SysPassValue temp = new SysPassValue();
		temp.setFunctionName("getFeedbackList");//getContactList
		temp.setPageNumber(String.valueOf(this.currentPage));
		temp.setArg1(String.valueOf(userid));
		
		new sendOrderToServerForListYTask(new AbstractYGetTaskListener<List<SysPassValue>>() {
			@Override
			public void onPostExecute(String name, YGetTask<List<SysPassValue>> task) {
				
				showData(task.getValue());
			}
		},temp).execute();
    }
	
	public void showData(List<SysPassValue> list){
		
		View vnorecord = this.findViewById(R.id.activity_no_record_showpic);
		if (list==null){
			if(currentPage==1){
				this.dataList.clear(); 
				adapter.notifyDataSetChanged();
	    		vnorecord.setVisibility(View.VISIBLE);
	    		return;
			}//如果到了最后一页，再加载，接口返回来null了，就更新ui
			return;
		}
		vnorecord.setVisibility(View.GONE);
		
		//如果第一页则需要清除数据
		if(currentPage==1){
    		this.dataList.clear();
			this.dataList.addAll(list);
    	}else{
    		dataList.addAll(list);
		}
		adapter.notifyDataSetChanged();
		if(dataList.size()==0){
			vnorecord.setVisibility(View.VISIBLE);
		}
		
	}
	
	@Override
	public void onClick(View view) {
		
		if(R.id.wg_head_btn_return==view.getId()){
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		}
		
		if(R.id.item_btn1==view.getId()){
			
			Object tag = view.getTag();
			
			if (tag != null) {
				
				int position = (Integer) tag;
				//Log.d("longhua", "position:" + position);
				String taskId =  dataList.get(position).getId();
				String taskName =  dataList.get(position).getArg1();
			    Intent in=new Intent(view.getContext(),WgFeedbackViewActivity.class);
				in.putExtra("taskId", String.valueOf(taskId));
				in.putExtra("taskName", taskName);
				this.startActivity(in);
				overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				return;
			}
			
		}
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	
	
}
