package com.zhyq;
 
import com.zhyq.libs.AndroidUtils;
import com.zhyq.libs.PreferenceUtils;
import com.zhyq.libs.ToastUtils;
 
 
import com.zhyq.model.User;
import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.UserLoginYTask;
import com.zhyq.widget.LoadingRelativeLayout;

import android.app.Activity;


import android.content.Intent;
import android.os.Bundle;
 
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
 
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;



/*
 * 登录窗口
 */

public class WgLoginActivity extends Activity implements OnClickListener {
	public String target = null;
	private LoadingRelativeLayout loading = null;
	private User user = new User();
	
	public static final int request_login_result = 0;   //回传数据使用
	public static boolean active = false;               //回传数据使用
	
	private View includeTitle;
	private ImageView returnImage;
	private TextView returnText;
	private Button btnReturn;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.setContentView(R.layout.activity_wg_login);//activity_wg_login_company
		loading=LoadingRelativeLayout.getLoadingRelativeLayout(this);
		this.findViewById(R.id.hh_btn_logon).setOnClickListener(this);
		this.findViewById(R.id.hh_btn_register).setOnClickListener(this);
		this.findViewById(R.id.hh_forgetPassword).setOnClickListener(this);
		this.findViewById(R.id.hh_login_guest).setOnClickListener(this);
		
		/*
	    includeTitle = this.findViewById(R.id.hh_userLogin_head);
	    
	    btnReturn = (Button)includeTitle.findViewById(R.id.bt_hh_head_return);
	    btnReturn.setOnClickListener(this);
		
		((TextView)includeTitle.findViewById(R.id.tv_hh_head_title)).setText("登入"); 
		returnImage = (ImageView)includeTitle.findViewById(R.id.iv_hh_head_return);
		returnImage.setOnClickListener(this);
		
		returnText = (TextView)includeTitle.findViewById(R.id.tv_hh_head_return);
		returnText.setOnClickListener(this);
		*/
		
		target = this.getIntent().getStringExtra("target");
		String loginid = PreferenceUtils.getString(this,"usercode","");//正常登入的时候记忆帐号,密码自己输入
		EditText actv=(EditText)this.findViewById(R.id.wg_login_id);
		actv.setText(loginid);
		reSize();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if (this.getIntent().getStringExtra("target")!=null) {
			target=this.getIntent().getStringExtra("target");
		}
	}
	
	public void reSize(){
		
		//设置头部的高宽比例
		int defaultWidth = 640; 
		int defaultHeight = 71; 
		float scale = 0;
		int webHeight = 0;
	 
		//得到屏幕宽度
		DisplayMetrics dm = this.getResources().getDisplayMetrics();
		int screenWidth = dm.widthPixels;
		//int webWidth = webImg.getMaxWidth();
		
		/*
		View loginHeader = this.findViewById(R.id.hh_userLogin_head);
		
	    scale =  (float) defaultWidth / (float) defaultHeight;
		webHeight=  Math.round(screenWidth / scale);
		
		LinearLayout.LayoutParams params= (LinearLayout.LayoutParams) loginHeader.getLayoutParams();
		 
		params.width  = screenWidth;
		params.height = webHeight;
		loginHeader.setLayoutParams(params);
		*/
		
		//设置里面的图片的大小
		/*
		defaultHeight= 229;
		ImageView img_login_bigimg = (ImageView)this.findViewById(R.id.hh_img_login_bigimg);
		scale =  (float) defaultWidth / (float) defaultHeight;
		webHeight=  Math.round(screenWidth / scale);
		
		RelativeLayout.LayoutParams params2= (RelativeLayout.LayoutParams) img_login_bigimg.getLayoutParams();
		 
		params2.width  = screenWidth;
		params2.height = webHeight;
		img_login_bigimg.setLayoutParams(params2);
		*/
	 
		//设置人头像的大小  hh_img_bigimgbelow_personheader
		defaultHeight= 260;
		ImageView img_login_person = (ImageView)this.findViewById(R.id.wg_img_bigimgbelow_personheader);
		scale =  (float) defaultWidth / (float) defaultHeight;
		webHeight=  Math.round(screenWidth / scale);
		
		RelativeLayout.LayoutParams params3= (RelativeLayout.LayoutParams) img_login_person.getLayoutParams();
		 
		params3.width  = screenWidth;
		params3.height = webHeight;
		img_login_person.setLayoutParams(params3);
		
		//输入电话号码的登入窗口的 高度 和宽度  layout_logon_account
		defaultHeight = 89;
		View vlayout_logon_account = this.findViewById(R.id.layout_logon_account);
		
	    scale = (float) defaultWidth / (float) defaultHeight;
		webHeight = Math.round(screenWidth / scale);
		
		LinearLayout.LayoutParams param4= (LinearLayout.LayoutParams) vlayout_logon_account.getLayoutParams();
		 
		param4.width  = screenWidth;
		param4.height = webHeight;
		vlayout_logon_account.setLayoutParams(param4);
		
		//layout_logon_password 输入密码的高度
		defaultHeight = 89;
		View vlayout_logon_password = this.findViewById(R.id.layout_logon_password);
		
	    scale = (float) defaultWidth / (float) defaultHeight;
		webHeight = Math.round(screenWidth / scale);
		
		LinearLayout.LayoutParams param5 = (LinearLayout.LayoutParams) vlayout_logon_password.getLayoutParams();
		 
		param5.width = screenWidth;
		param5.height = webHeight;
		vlayout_logon_password.setLayoutParams(param5);
		
		//设置 登入的 宽度 和高度 hh_btn_logon
		//主要是BUTTON  的宽度和高度 比例需要 注意
		
	}

	@Override
	public void onClick(View view) {
		
		if (R.id.iv_hh_head_return==view.getId()) {
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		} else if (R.id.bt_hh_head_return==view.getId()){
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		} else if (R.id.tv_hh_head_return==view.getId()){
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		} else if (R.id.hh_btn_logon==view.getId()) {
			 
			 if(AndroidUtils.isNetworkAvailable(this)==false){
				 ToastUtils.show(this, "网络连接失败，请检查网络连接！");
				 return;
			 }
			 
			 String Logid = null;
			 String pwd = null;
			 
			 Logid = AndroidUtils.getEditText(this, R.id.wg_login_id);
			 pwd = AndroidUtils.getEditText(this, R.id.wg_login_password);
			 
			 if(Logid.equals("")){
				 ToastUtils.show(this, "账号不能为空");
				 return;
			 }
			 
			 if(pwd.equals("")){
				 ToastUtils.show(this, "密码不能为空");
				 return; 
			 }			 
			
			loading.showLoading();
			
			user.setCode(Logid);
			
			user.setPassword(pwd);
			
			new UserLoginYTask(new AbstractYGetTaskListener<User>() {
				@Override
				public void onPostExecute(String name, YGetTask<User> task) {
					loading.hideLoading();
					logined(task.getValue());
				}
			},user).execute();
			
		} else if (R.id.hh_btn_register==view.getId()) {
			 
			//AndroidUtils.start(this, HhRegisterActivity.class);
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
			
		} else if(R.id.hh_forgetPassword ==view.getId()){
			
			//AndroidUtils.start(this, HhForgetPwdActivity1.class);
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			return;
			
		} else if (R.id.hh_login_guest == view.getId()){
			
			HhApplication.getInstance(this).getHhCart().setUser(null);
			AndroidUtils.start(this, WgMainActivity.class);
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
			
		}
	}
	
	/*
	 * 登录
	 * 通过PreferenceUtils把用户信息写入设备中，不要写入密码
	 */
	private void logined(User user) {
		if (user==null) {
			ToastUtils.show(this, R.string.show_logon_error_msg);
		} else {
			
			HhApplication.getInstance(this).getHhCart().setUser(user);
			//AndroidUtils.start(this, SelectShopActivity.class);
			PreferenceUtils.setLong(this,"userid",user.getId());
			PreferenceUtils.setString(this,"usercode",user.getCode());
			PreferenceUtils.setString(this,"username",user.getName());
			PreferenceUtils.setString(this,"usertype",user.getUserType());
			PreferenceUtils.setString(this,"userrole",user.getUserRole());//功能权限
			
			Log.i("luktel","userid"+String.valueOf(user.getId()));//Android Monitor logcat显示输出日志
			
			/*
			Intent intent = new Intent(this,WgMainActivity.class);//注意： 必须是传递给 Activity ,不能传递给 我的 fragment,否则获取不到信息
			setResult(request_login_result, intent);
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
			*/

			AndroidUtils.start(this, WgMainActivity.class);//登录成功则打开主页，上面的方式是先打开主页，登录成功后返回
			this.finish();
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);//设置新面板右边进入，原来面板左边退出
			return;
		}
	}
	
	
	
}
