package com.zhyq;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.aspsine.irecyclerview.IRecyclerView;
import com.aspsine.irecyclerview.OnLoadMoreListener;
import com.aspsine.irecyclerview.OnRefreshListener;
import com.aspsine.irecyclerview.demo.model.Image;
import com.aspsine.irecyclerview.demo.ui.adapter.RecyclerAdapter;
import com.aspsine.irecyclerview.demo.ui.adapter.OnItemClickListener;
import com.aspsine.irecyclerview.demo.ui.widget.BannerView;
import com.aspsine.irecyclerview.demo.ui.widget.footer.LoadMoreFooterView;
import com.aspsine.irecyclerview.demo.ui.widget.header.BatVsSupperHeaderView;
import com.aspsine.irecyclerview.demo.ui.widget.header.ClassicRefreshHeaderView;
import com.aspsine.irecyclerview.demo.utils.DensityUtils;
import com.zhyq.model.SysPassValue;
import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.sendOrderToServerForListYTask;

import java.util.ArrayList;
import java.util.List;


public class TestIRecyclerActivity extends AppCompatActivity implements OnClickListener, OnItemClickListener<Image>, OnRefreshListener, OnLoadMoreListener {

    private IRecyclerView iRecyclerView;
    private BannerView bannerView;
    private LoadMoreFooterView loadMoreFooterView;

    private RecyclerAdapter mAdapter;

    private int mPage;

    private int currentPage = 1;
    private List<SysPassValue> dataList = new ArrayList<SysPassValue>();//数据

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_irecyclerview);
        iRecyclerView = (IRecyclerView) findViewById(R.id.iRecyclerView);
        iRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        toggleRefreshHeader();//切换上面刷新面板

        bannerView = (BannerView) LayoutInflater.from(this).inflate(R.layout.layout_irecyclerview_banner_view, iRecyclerView.getHeaderContainer(), false);
        iRecyclerView.addHeaderView(bannerView);

        loadMoreFooterView = (LoadMoreFooterView) iRecyclerView.getLoadMoreFooterView();

        mAdapter = new RecyclerAdapter(dataList);
        mAdapter.setOnClick1(this);
        iRecyclerView.setIAdapter(mAdapter);

        iRecyclerView.setOnRefreshListener(this);
        iRecyclerView.setOnLoadMoreListener(this);

        mAdapter.setOnItemClickListener(this);

        iRecyclerView.post(new Runnable() {
            @Override
            public void run() {
                iRecyclerView.setRefreshing(true);
            }
        });
    }

    public void getData(){

        SysPassValue temp = new SysPassValue();
        temp.setFunctionName("getContactList");//getContactList
        temp.setPageNumber(String.valueOf(this.currentPage));
        temp.setArg1("");

        new sendOrderToServerForListYTask(new AbstractYGetTaskListener<List<SysPassValue>>() {
            @Override
            public void onPostExecute(String name, YGetTask<List<SysPassValue>> task) {
                iRecyclerView.setRefreshing(false);
                showData(task.getValue());
            }
        },temp).execute();
    }

    public void showData(final List<SysPassValue> list){

        //View vnorecord = this.findViewById(R.id.activity_no_record_showpic);
        if (list==null){
            loadMoreFooterView.setStatus(LoadMoreFooterView.Status.ERROR);
            //Toast.makeText(this, "网络连接失败", Toast.LENGTH_SHORT).show();

            if(currentPage==1){
                this.dataList.clear();
                mAdapter.notifyDataSetChanged();
                //vnorecord.setVisibility(View.VISIBLE);
                return;
            }//如果到了最后一页，再加载，接口返回来null了，就更新ui
            return;
        }

        if(list.size()==0){
            loadMoreFooterView.setStatus(LoadMoreFooterView.Status.THE_END);
            //Toast.makeText(this, "已经到了最后一页", Toast.LENGTH_SHORT).show();
        }else{
            if(currentPage==1){
                this.dataList.clear();
                this.dataList.addAll(list);
            }else{

                loadMoreFooterView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mPage++;
                        loadMoreFooterView.setStatus(LoadMoreFooterView.Status.GONE);
                        dataList.addAll(list);
                    }
                }, 500);

                //loadMoreFooterView.setStatus(LoadMoreFooterView.Status.GONE);
                //dataList.addAll(list);
            }
            mAdapter.notifyDataSetChanged();
        }
        //vnorecord.setVisibility(View.GONE);

        //如果第一页则需要清除数据


        if(dataList.size()==0){
            //vnorecord.setVisibility(View.VISIBLE);
        }

        Log.d("luktel", "showData");

    }

    /*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_change_header, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_toggle_header) {
            toggleRefreshHeader();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    */

    public void onClick(View view) {

        if(R.id.item_btn1==view.getId()){

            Object tag = view.getTag();

            if (tag != null) {
                int position = (Integer) tag;
                //Log.d("longhua", "position:" + position);
                String taskId =  dataList.get(position).getId();
                String taskName =  dataList.get(position).getArg1();
                Intent in=new Intent(view.getContext(),WgFeedbackViewActivity.class);
                in.putExtra("taskId", String.valueOf(taskId));
                in.putExtra("taskName", taskName);
                this.startActivity(in);
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                return;
            }

        }
    }

    @Override
    public void onItemClick(int position, Image image, View v) {//可以参考原文件实现
        //mAdapter.remove(position);
        Toast.makeText(this, String.valueOf(position), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRefresh() {
        //loadBanner();
        loadMoreFooterView.setStatus(LoadMoreFooterView.Status.GONE);
        //refresh();
        currentPage = 1;
        getData();
    }

    public void onLoadMore() {
        if (loadMoreFooterView.canLoadMore() && mAdapter.getItemCount() > 0) {
            loadMoreFooterView.setStatus(LoadMoreFooterView.Status.LOADING);
            currentPage++;
            getData();
            //Toast.makeText(this, "已经到了"+currentPage, Toast.LENGTH_SHORT).show();
            //loadMore();
        }
    }

    private void toggleRefreshHeader() {//两个顶部刷新面板
        if (iRecyclerView.getRefreshHeaderView() instanceof BatVsSupperHeaderView) {
            ClassicRefreshHeaderView classicRefreshHeaderView = new ClassicRefreshHeaderView(this);
            classicRefreshHeaderView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, DensityUtils.dip2px(this, 80)));
            // we can set view
            iRecyclerView.setRefreshHeaderView(classicRefreshHeaderView);
            //Toast.makeText(this, "Classic style", Toast.LENGTH_SHORT).show();
        } else if (iRecyclerView.getRefreshHeaderView() instanceof ClassicRefreshHeaderView) {
            // we can also set layout
            iRecyclerView.setRefreshHeaderView(R.layout.layout_irecyclerview_refresh_header);
            //Toast.makeText(this, "Bat man vs Super man style", Toast.LENGTH_SHORT).show();
        }
    }

    /*
    private void loadBanner() {
        NetworkAPI.requestBanners(new NetworkAPI.Callback<List<Image>>() {
            @Override
            public void onSuccess(List<Image> images) {
                if (!ListUtils.isEmpty(images)) {
                    bannerView.setList(images);
                }
            }

            @Override
            public void onFailure(Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void refresh() {
        mPage = 1;
        NetworkAPI.requestImages(mPage, new NetworkAPI.Callback<List<Image>>() {
            @Override
            public void onSuccess(List<Image> images) {
                iRecyclerView.setRefreshing(false);
                if (ListUtils.isEmpty(images)) {
                    mAdapter.clear();
                } else {
                    mPage = 2;
                    mAdapter.setList(images);
                }
            }

            @Override
            public void onFailure(Exception e) {
                e.printStackTrace();
                iRecyclerView.setRefreshing(false);
                Toast.makeText(TestIRecyclerActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadMore() {
        NetworkAPI.requestImages(mPage, new NetworkAPI.Callback<List<Image>>() {
            @Override
            public void onSuccess(final List<Image> images) {
                if (ListUtils.isEmpty(images)) {
                    loadMoreFooterView.setStatus(LoadMoreFooterView.Status.THE_END);
                } else {
                    loadMoreFooterView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mPage++;
                            loadMoreFooterView.setStatus(LoadMoreFooterView.Status.GONE);
                            mAdapter.append(images);
                        }
                    }, 2000);
                }
            }

            @Override
            public void onFailure(Exception e) {
                e.printStackTrace();
                loadMoreFooterView.setStatus(LoadMoreFooterView.Status.ERROR);
                Toast.makeText(TestIRecyclerActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }
    */


}
