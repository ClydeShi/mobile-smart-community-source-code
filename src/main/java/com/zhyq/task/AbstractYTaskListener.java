package com.zhyq.task;

public abstract class AbstractYTaskListener implements YTaskListener {

	@Override
	public void execute(String name, YTask task, int level) {
		if (YTaskListener.POST==level) {
			onPostExecute(name,task);
		}
	}

	public abstract void onPostExecute(String name, YTask task);
}
