package com.zhyq.task;

public abstract class YGetTask<T> extends YTask {
	public YGetTask(YTaskListener l) {
		super(null, l);
	}
	@Override
	public String getName() {
		return this.getClass().getName();
	}
	public T getValue() {
		return (T)this.getAttribute(getName());
	}
	public abstract T _get() throws Throwable; 
	@Override
	public void onExecute() throws Throwable {
		T t=_get();
		this.setAttribute(getName(), t);
	}
	
	
	
}
