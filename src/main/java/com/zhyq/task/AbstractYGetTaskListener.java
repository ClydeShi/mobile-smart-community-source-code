package com.zhyq.task;

 
import com.zhyq.HhApplication;

import android.util.Log;
import android.widget.Toast;
 
 

public abstract class AbstractYGetTaskListener<T> extends AbstractYTaskListener {

	@Override
	public void onPostExecute(String name, YTask task) {
		try {
			onPostExecute(name,(YGetTask<T>)task);
		} catch(Throwable t) {
			Log.e("yimi",name+"error",t);
			
 			//message.showLong(, t.getLocalizedMessage());
			
			Toast.makeText(HhApplication.instance, t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
		}
	}

	public abstract void onPostExecute(String name, YGetTask<T> task);
}
