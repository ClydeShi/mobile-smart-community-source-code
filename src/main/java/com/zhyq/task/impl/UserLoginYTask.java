package com.zhyq.task.impl;

import com.zhyq.data.DataAPI;
import com.zhyq.model.User;
import com.zhyq.task.YGetTask;
import com.zhyq.task.YTaskListener;

public class UserLoginYTask extends YGetTask<User> {
	private User user=null;
	public UserLoginYTask(YTaskListener l,User user) {
		super(l);
		this.user=user;
	}
	@Override
	public User _get() throws Throwable {
		return DataAPI.userLogin(user);
	}

}