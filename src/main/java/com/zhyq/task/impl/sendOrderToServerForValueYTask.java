package com.zhyq.task.impl;



//通用数据结构 发送命令到服务器当中，获取服务器数据
import com.zhyq.data.DataAPI;
 
import com.zhyq.model.SysPassValue;
import com.zhyq.task.YGetTask;
import com.zhyq.task.YTaskListener;

public class sendOrderToServerForValueYTask extends YGetTask<SysPassValue> {
	private SysPassValue temp=null;
	public sendOrderToServerForValueYTask(YTaskListener l,SysPassValue temp) {
		super(l);
		this.temp=temp;
	}
	@Override
	public SysPassValue _get() throws Throwable {
		return DataAPI.sendOrderToServerForValue(temp);
	}
	
}