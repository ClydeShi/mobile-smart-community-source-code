package com.zhyq.task.impl;

import java.util.List;

import com.zhyq.data.DataAPI;

import com.zhyq.model.CommonList;
 
 
 
import com.zhyq.task.YGetTask;
import com.zhyq.task.YTaskListener;

public class GetTaskListYTask extends YGetTask<List<CommonList>> {
	 
	private String   userid;  //当前用户
	private String dttype;    //当前类型
	private int    curpager;  //当前页面
	public GetTaskListYTask(YTaskListener l,String   userid,String dttype,int    curpager) {
		super(l);
		this.userid = userid;
		this.dttype = dttype;
		this.curpager = curpager;
	}
	 
	@Override
	public List<CommonList>  _get() throws Throwable {
		 return DataAPI.getTaskList(userid,dttype,curpager);
	}


}