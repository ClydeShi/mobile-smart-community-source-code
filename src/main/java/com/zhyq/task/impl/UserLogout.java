package com.zhyq.task.impl;

import com.zhyq.data.DataAPI;
import com.zhyq.task.YTask;
import com.zhyq.task.YTaskListener;

public class UserLogout extends YTask {
	private Long id=null;
	public UserLogout(YTaskListener l,Long id) {
		super(UserLogout.class.getName(), l);
		this.id=id;
	}
	@Override
	public void onExecute() throws Throwable {
		DataAPI.userLogout(id);
	}

}
