package com.zhyq.task.impl;

import com.zhyq.data.DataAPI;
import com.zhyq.model.CommonData;
import com.zhyq.task.YGetTask;
import com.zhyq.task.YTaskListener;


public class GetTaskDetailYTask extends YGetTask<CommonData> {
	private String taskid=null; 
	private String userid=null;
	 
	public GetTaskDetailYTask(YTaskListener l,String taskid,String userid) {
		super(l);
		this.taskid= taskid; 
		this.userid = userid;
	}
	@Override
	public CommonData _get() throws Throwable {
		return DataAPI.getTaskDetail(taskid,userid);
	}

}