package com.zhyq.task;

public interface YTaskListener {
	public static final int PREPARE=0;
	public static final int EXECUTE=1;
	public static final int POST=2;
	public void execute(String name,YTask task,int level);
}
