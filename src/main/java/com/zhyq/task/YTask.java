package com.zhyq.task;

import java.util.HashMap;

import android.os.AsyncTask;
import android.util.Log;

public abstract class YTask extends AsyncTask<Void, Void, Void> {
	public final static String ERROR="_error";
	private String name=null;
	private String tags ="huahua";
	private YTaskListener l=null;
	private HashMap<String,Object> attribute=new HashMap<String,Object>();
	public YTask(String name, YTaskListener l) {
		super();
		this.name = name;
		this.l = l;
	}
	public String getName() {
		return name;
	}
	public void setAttribute(String name,Object value) {
		attribute.put(name, value);
	}
	public Object getAttribute(String name) {
		return attribute.get(name);
	}
	public void setError(Throwable t) {
		setAttribute(ERROR,t);
	}
	public Throwable getError() {
		return (Throwable)getAttribute(ERROR);
	}
	public boolean executeSuccessed() {
		return getError()==null;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		l.execute(getName(),this,YTaskListener.PREPARE);
		Log.i(tags,"YTask onPreExecute "+getName());
	}
	
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		l.execute(getName(),this,YTaskListener.POST);
		Log.i(tags,"YTask onPostExecute "+getName());
	}

	@Override
	protected Void doInBackground(Void... arg0) {
		Log.i(tags,"YTask doInBackground "+getName());
		try {
			onExecute();
		} catch (Throwable e) {
			Log.e(tags,getName()+" execute error",e);
			setError(e);
		}
		l.execute(getName(),this,YTaskListener.EXECUTE);
		return null;
	}
	
	public abstract void onExecute() throws Throwable;

}
