package com.zhyq;

 
 

import java.util.ArrayList;
import java.util.List;

import com.zhyq.adapter.WgMultiSelectAdapterCheckbox;
import com.zhyq.adapter.WgMultiSelectAdapterCheckbox.ViewHolder;
 
 
import com.zhyq.libs.PreferenceUtils;


import com.zhyq.model.SysPassValue;


import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
 
import com.zhyq.task.impl.sendOrderToServerForListYTask;
 
import com.zhyq.widget.LoadingRelativeLayout;


import android.app.Activity;
import android.content.Intent;
 
 
import android.os.Bundle;
 
 
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

/*
 * 使用checkbox全选功能 http://blog.csdn.net/lm_zp/article/details/51381392  http://blog.csdn.net/u012369373/article/details/49965215
 */

public class WgPlanSelectStreetActivity extends Activity implements OnItemClickListener,OnClickListener {
	
	private LoadingRelativeLayout loading = null;
	private View includeTitle;
	private int currentPage = 1;
    private WgMultiSelectAdapterCheckbox mAdapter;//数据适配器
    private List<SysPassValue> NewsList = new ArrayList<SysPassValue>();//数据
    private String street = "";
	private String clickfrom = "";
    private ListView listView;
    List<String> chooseIdList = new ArrayList<String>();
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.setContentView(R.layout.activity_wg_multiselect_listview);
		loading = LoadingRelativeLayout.getLoadingRelativeLayout(this);
		includeTitle = this.findViewById(R.id.wg_include_singleselect_head);
		Button btreturn = (Button)includeTitle.findViewById(R.id.wg_head_btn_return);
		btreturn.setOnClickListener(this);
		Button btconfirm = (Button)includeTitle.findViewById(R.id.wg_head_btn_confirm);
		btconfirm.setOnClickListener(this);
		((TextView)includeTitle.findViewById(R.id.wg_head_title)).setText("选择街道");
		
		((Button)this.findViewById(R.id.wg_singleselect1_btn)).setOnClickListener(this);
		street = this.getIntent().getStringExtra("street");
		clickfrom = this.getIntent().getStringExtra("clickfrom");
		
		listView = (ListView) findViewById(R.id.lv_data);
		mAdapter = new WgMultiSelectAdapterCheckbox(this, NewsList);
		//mAdapter.setOnClick1(this);
		
		listView.setAdapter(mAdapter);
		
        listView.setOnItemClickListener(new OnItemClickListener() {
        	
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                    long arg3) {
                // TODO Auto-generated method stub
                // 取得ViewHolder对象
                ViewHolder viewHolder = (ViewHolder) arg1.getTag();
                // 改变CheckBox的状态
                viewHolder.checkBox.toggle();
                // 将CheckBox的选中状况记录下来
                NewsList.get(arg2).setBo(viewHolder.checkBox.isChecked());
                // 调整选定条目
                if (viewHolder.checkBox.isChecked() == true) {
                    //num++;
                    //pric += Integer.parseInt(NewsList.get(arg2).getName());
                	/*if(wangge.equals("")){
                		wangge = NewsList.get(arg2).getArg2();
                	}else{
                		wangge = wangge + "," + NewsList.get(arg2).getArg2();
                	}*/
                	chooseIdList.add(NewsList.get(arg2).getArg2());
                } else {
                    //num--;
                    //pric -= Integer.parseInt(list.get(arg2).getName());
                	chooseIdList.remove(NewsList.get(arg2).getArg2());
                }
                // 用TextView显示
                //price.setText("一共选了" + num + "件," + "价格是" + pric + "元");
            }
        });
        
        onRefresh();
	}
	
	private void onRefresh(){
        //首次加载任务数据 加载任务  
        currentPage = 1;
        GetData(currentPage);
	}
	
	//onResume是在启动activity启动之后才能执行的，也就是恢复执行。程序正常启动：onCreate()->onStart()->onResume();
	@Override
	protected void onResume() {   //注意 该函数，如果在打开了其他窗口，再关闭，这个函数也会执行
		super.onResume();
		
	}
	
	public void GetData(int curpage){
		
		loading.showLoading();
		Long userId = PreferenceUtils.getLong(this, "userid", -1);
		String usercode = PreferenceUtils.getString(this,"usercode","");
		
		SysPassValue temp = new SysPassValue();
		temp.setFunctionName("getStreetList");
		temp.setUserId(String.valueOf(userId));
		temp.setArg1("1");
		temp.setArg2("");
		temp.setPageNumber(String.valueOf(curpage));
		
		//调用接口得到数据
		new sendOrderToServerForListYTask(new AbstractYGetTaskListener<List<SysPassValue>>() {
			@Override
			public void onPostExecute(String name, YGetTask<List<SysPassValue>> task) {
				
				showData(task.getValue());
				loading.hideLoading();
			}
		},temp).execute();
		
	}
	
	public void showData(List<SysPassValue> list) {
		
		//通过数据适配器获取 数据列表
		View vnorecord = this.findViewById(R.id.activity_no_record_showpic);
		if (list==null){
			if(currentPage==1){
				this.NewsList.clear(); 
	    		mAdapter.notifyDataSetChanged();
	    		vnorecord.setVisibility(View.VISIBLE);
	    		return;
			}
			return;
		}
		vnorecord.setVisibility(View.GONE);
		
		//如果是第一页则需要清楚数据   currentPage
		if(currentPage==1){
    		this.NewsList.clear();
			this.NewsList.addAll(list);
    	} else{
			NewsList.addAll(list);
		}
		mAdapter.notifyDataSetChanged();
		if(NewsList.size()==0){
			vnorecord.setVisibility(View.VISIBLE);
		}
		
	}
	
	@Override
	public void onClick(View view) {
		
		 if(R.id.wg_head_btn_return==view.getId()){
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		  }	
		  
		 if(R.id.wg_singleselect1_btn==view.getId()){
			 if(clickfrom.equals("taskfeedback")){
				 Intent intent = new Intent(this,WgTaskFeedback.class);
				 intent.putExtra("name", "全部");
				 setResult(4, intent);
			 }else{
				 Intent intent = new Intent(this,WgPlanAdd.class);
				 intent.putExtra("name", "全部");
				 setResult(10, intent);
			 }
	         overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			 this.finish();
			 return;
		 }
		 
		 if(R.id.wg_head_btn_confirm==view.getId()){
			 
			 String strTemp = "";
			 for(int i = 0;i < chooseIdList.size(); i ++){
				 if(strTemp.equals("")){
					 strTemp = chooseIdList.get(i);
				 }else{
					 strTemp = strTemp + "," + chooseIdList.get(i);
				 }
			 }

			 if(clickfrom.equals("taskfeedback")){
				 Intent intent = new Intent(this,WgTaskFeedback.class);
				 intent.putExtra("name", strTemp);
				 setResult(4, intent);
			 }else{
				 Intent intent = new Intent(this,WgPlanAdd.class);
				 intent.putExtra("name", strTemp);
				 setResult(10, intent);
			 }

		     overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			 this.finish();
			 return;
		 }
		 
	}
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		
		Log.i("longhua","onItemClick");
		if(arg3 == -1) {
	        // 点击的是headerView或者footerView
	        return;
	    }
		//arg2是当前item的ID。这个id根据你在适配器中的写法可以自己定义
		//ToastUtils.show(arg1.getContext(), R.string.show_logon_error_msg)
		//Object tag2 = arg1.getTag(); 
        int position = (Integer) arg2;
        position = position -1;
		return;
		
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}
	
}
