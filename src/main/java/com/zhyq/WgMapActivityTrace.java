package com.zhyq;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import com.baidu.mapapi.clusterutil.clustering.Cluster;
import com.baidu.mapapi.clusterutil.clustering.ClusterItem;
import com.baidu.mapapi.clusterutil.clustering.ClusterManager;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BaiduMap.OnMapLoadedCallback;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.map.Polyline;
import com.baidu.mapapi.map.PolylineOptions;
import com.baidu.mapapi.map.TextOptions;
import com.baidu.mapapi.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.zhyq.libs.ToastUtils;
import com.zhyq.model.SysPassValue;
import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.sendOrderToServerForListYTask;
import com.zhyq.widget.LoadingRelativeLayout;

/**
 * 实现功能如下:
 * 1. 人员画点，名字
 * 2. 画移动小车
 * 3. 画移动轨迹
 */
public class WgMapActivityTrace extends Activity implements OnMapLoadedCallback,OnClickListener {
	
	private LoadingRelativeLayout loading=null;
	private View includeTitle;
	private Long userId = null;
    MapView mMapView;
    BaiduMap mBaiduMap;
    MapStatus ms;
    
    //LocationClient mLocClient;
    //public MyLocationListenner myListener = new MyLocationListenner();
    //boolean isFirstLoc = true; // 是否首次定位
    
    private Polyline mPolyline;
    private Marker mMoveMarker;
    private Handler mHandler;
    
    // 通过设置间隔时间和距离可以控制速度和图标移动的距离
    private static final int TIME_INTERVAL = 80;
    private static final double DISTANCE = 0.00006;//0.00002
    
    private static double currentLatitude = 0.0;
    private static double currentLongitude = 0.0;
    
    private ClusterManager<MyItem> mClusterManager;
    
    WgMapThread thread = null;
    public LatLng[] trackLatLngs = null;
    
    public int tempUserId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Log.i("longhua", "WgMapActivityTrace");
        setContentView(R.layout.activity_wg_map);
        loading=LoadingRelativeLayout.getLoadingRelativeLayout(this);
        mMapView = (MapView) findViewById(R.id.bmapView);
        
        //currentLatitude = Double.valueOf(HhApplication.getInstance().getLatitude());
        //currentLongitude = Double.valueOf(HhApplication.getInstance().getLongitude());
        currentLatitude = 22.704807;
        currentLongitude = 114.049725;//设置在龙华
        //Log.i("longhua", "currentLatitude"+currentLatitude+" currentLongitude:"+currentLongitude);
        ms = new MapStatus.Builder().target(new LatLng(currentLatitude, currentLongitude)).zoom(8).build();//传入经度纬度参数定位，不使用下面的定位代码
        mBaiduMap = mMapView.getMap();
        
        /*
        //定位代码开始
        // 开启定位图层
        mBaiduMap.setMyLocationEnabled(true);
        // 定位初始化
        mLocClient = new LocationClient(this);
        mLocClient.registerLocationListener(myListener);
        LocationClientOption option = new LocationClientOption();
        option.setOpenGps(true); // 打开gps
        option.setCoorType("bd09ll"); // 设置坐标类型
        option.setScanSpan(1000);
        mLocClient.setLocOption(option);
        mLocClient.start();
        //定位代码结束
        */
        
        //画点代码开始
        mBaiduMap.setOnMapLoadedCallback(this);
        mBaiduMap.animateMapStatus(MapStatusUpdateFactory.newMapStatus(ms));
        // 定义点聚合管理类ClusterManager
        mClusterManager = new ClusterManager<MyItem>(this, mBaiduMap);
        // 添加Marker点
        addMarkers();
        // 设置地图监听，当地图状态发生改变时，进行点聚合运算
        mBaiduMap.setOnMapStatusChangeListener(mClusterManager);
        // 设置maker点击时的响应
        mBaiduMap.setOnMarkerClickListener(mClusterManager);

        mClusterManager.setOnClusterClickListener(new ClusterManager.OnClusterClickListener<MyItem>() {
            @Override
            public boolean onClusterClick(Cluster<MyItem> cluster) {
                Toast.makeText(WgMapActivityTrace.this, "有" + cluster.getSize() + "个点", Toast.LENGTH_SHORT).show();

                return false;
            }
        });
        mClusterManager.setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<MyItem>() {
            @Override
            public boolean onClusterItemClick(MyItem item) {
                //Toast.makeText(WgMapActivityTrace.this, "点击单个Item"+item.getPosition()+",用户id"+item.getUserId(), Toast.LENGTH_SHORT).show();
            	Toast.makeText(WgMapActivityTrace.this, "查看人员"+item.getUserName(), Toast.LENGTH_SHORT).show();
                onClickPerson(item.getUserId());

                return false;
            }
        });
        //画点代码结束
        
        //轨迹代码开始
        //mHandler = new Handler(Looper.getMainLooper());
        //drawPolyLine();
        //moveLooper();
        //轨迹代码结束
        
        includeTitle = this.findViewById(R.id.wg_map_head);
		((TextView)includeTitle.findViewById(R.id.wg_head_title)).setText("检查定位");
		Button btreturn = (Button)includeTitle.findViewById(R.id.wg_head_btn_return);
		btreturn.setOnClickListener(this);
		if(HhApplication.getInstance(this).getHhCart().getUser()==null){
			userId =-1l;
		}else{
			userId = HhApplication.getInstance(this).getHhCart().getUser().getId();
		}
    }
    
    public void onClickPerson(int puserId){
    	
    	this.tempUserId = puserId;
    	
    	if(mPolyline!=null){
    		mPolyline.remove();
    	}
    	if(mMoveMarker!=null){
    		mMoveMarker.remove();
    	}
    	if(mHandler!=null){
    		mHandler=null;
    	}
    	
    	if(thread!=null){
    		//Log.i("longhua", "interrupt");
    		thread.interrupt();
    	}
    	
    	//清除所有图层
    	//mMapView.getMap().clear();
    	
    	ToastUtils.show(this, "轨迹数据量比较大，请多等一会");
    	loading.showLoading();
    	new Thread(new ThreadDemo()).start();//使用线程方法获取轨迹数据
    	
    	//直接获取轨迹数据并在方法里面画图
    	//getLatLngByUserId(puserId);
    	
    	mHandler = new Handler(Looper.getMainLooper());
    	
    	/*
		LatLng[] puserlatlngs = getLatLngByUserId(puserId);//通过点击的用户id，来获取当天的轨迹
		
		
		if(puserlatlngs!=null){
			
			if(puserlatlngs.length>0){
				Log.i("longhua", "update user road map");
				ToastUtils.show(this, "加载人员轨迹！");
				drawPolyLine(puserlatlngs);
				//moveLooper(userlatlngs);  //内部线程方法，没办法停下来，所以用WgMapThread来控制
				
				ToastUtils.show(this, "加载移动轨迹！");
				thread = new WgMapThread(puserlatlngs,mMapView,mMoveMarker,mHandler);
				thread.start();
			}else{
				ToastUtils.show(this, "没有获取到轨迹数据啊！");
			}
			
		}else{
			ToastUtils.show(this, "没有获取到轨迹数据哦 null！");
		}
		*/
		//loading.hideLoading();
		
    }
    
    @Override
	public void onClick(View view) {
		
		if(R.id.wg_head_btn_return==view.getId()){
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		}
		
		return;
	}

    @Override
    protected void onPause() {
        mMapView.onPause();
        super.onPause();
    }

    @Override
    protected void onResume() {
        mMapView.onResume();
        super.onResume();
    }

    @Override
    protected void onDestroy() {
    	// 退出时销毁定位
        //mLocClient.stop();
        // 关闭定位图层
        mBaiduMap.setMyLocationEnabled(false);
        
        mMapView.onDestroy();
        
        mMapView = null;
        
        super.onDestroy();
        
        mBaiduMap.clear();
    }
    
    /**
     * 定位SDK监听函数
     */
    /*
    public class MyLocationListenner implements BDLocationListener {

        @Override
        public void onReceiveLocation(BDLocation location) {
            // map view 销毁后不在处理新接收的位置
            if (location == null || mMapView == null) {
                return;
            }
            MyLocationData locData = new MyLocationData.Builder()
                    .accuracy(location.getRadius())
                            // 此处设置开发者获取到的方向信息，顺时针0-360
                    .direction(100).latitude(location.getLatitude())
                    .longitude(location.getLongitude()).build();
            mBaiduMap.setMyLocationData(locData);
            if (isFirstLoc) {
                isFirstLoc = false;
                currentLatitude = location.getLatitude();
                currentLongitude = location.getLongitude();
                LatLng ll = new LatLng(currentLatitude,currentLongitude);
                MapStatus.Builder builder = new MapStatus.Builder();
                builder.target(ll).zoom(16.0f);
                mBaiduMap.animateMapStatus(MapStatusUpdateFactory.newMapStatus(builder.build()));
            }
        }

        public void onReceivePoi(BDLocation poiLocation) {
        }
    }
    */

    /**
     * 向地图添加Marker点和文字
     */
    public void addMarkers() {
    	
    	/*
        List<MyItem> items = new ArrayList<MyItem>();
        items.add(new MyItem(new LatLng(22.623906285082434, 114.04846839946887),1,"吴红梅"));//加入用户id
        items.add(new MyItem(new LatLng(22.626221288393879, 114.04492711935792),2,"马水发"));
        //items.add(new MyItem(new LatLng(22.652340944881272, 114.02117334037855),3,"吴红梅"));
        //items.add(new MyItem(new LatLng(22.653340477569255, 114.02717390361458),4,"吴红梅"));
        items.add(new MyItem(new LatLng(22.617501030855827, 114.05046432666856),5,"杨美华"));
        items.add(new MyItem(new LatLng(22.619325475186439, 114.04924531184582),6,"李永刚"));
        //items.add(new MyItem(new LatLng(22.657340477569255, 114.02917390361458),7,"吴红梅"));
        */
    	/*
    	WgMapData mapData = new WgMapData();
    	List<WgMapData> wgdata = mapData.getOnlineUser();//通过android连接服务器取数据没有返回，不知道为什么
    	List<MyItem> items = new ArrayList<MyItem>();
    	for (int m = 0; m < wgdata.size(); m++) {
    		Log.i("longhua", "username"+wgdata.get(m).getUserName());
    		items.add(new MyItem(new LatLng(wgdata.get(m).getBaiduY(), wgdata.get(m).getBaiduX()),wgdata.get(m).getUserId(),wgdata.get(m).getUserName()));
    	}
    	mClusterManager.addItems(items);
        addTexts(items);
    	*/
    	
    	ToastUtils.show(this, "获取人员位置，请稍等");
    	loading.showLoading();
    	
    	SysPassValue temp = new SysPassValue();
		temp.setFunctionName("getOnlineUserList");
		temp.setArg2(String.valueOf(currentLongitude));//baiduX，如果重新获取定位，第一次运行的时候，先执行了这个函数，然后才获取到当前经度纬度，应该是速度问题，所以没有传递当前经度纬度过去，第二次点开地图没问题
		temp.setArg3(String.valueOf(currentLatitude));
		temp.setArg2("");
		temp.setArg3("");
		
		new sendOrderToServerForListYTask(new AbstractYGetTaskListener<List<SysPassValue>>() {//获取在线网格员的当前位置
			@Override
			public void onPostExecute(String name, YGetTask<List<SysPassValue>> task) {
				 
				getUserList(task.getValue());
			}
		},temp).execute();
    }
    
    public void getUserList(List<SysPassValue> list) {
    	List<MyItem> items = new ArrayList<MyItem>();
    	
    	if(list!=null){
    		
    		int quantity = list.size();
    		
    		for (int m = 0; m < quantity; m++) {
        		//Log.i("longhua", "username"+list.get(m).getArg1());
        		double baiduX = Double.valueOf(list.get(m).getArg2());
        		double baiduY = Double.valueOf(list.get(m).getArg3());
        		items.add(new MyItem(new LatLng(baiduY, baiduX),Integer.valueOf(list.get(m).getId()),list.get(m).getArg1()));
        	}
        	
    		//添加点
            mClusterManager.addItems(items);
            
            //addTexts(items);
            //添加文字
            for (int index = 0; index < quantity; index++) {
            	
            	LatLng llText = items.get(index).getPosition();
            	String userName = items.get(index).getUserName();
                OverlayOptions ooText = new TextOptions().bgColor(0xAAFFFF00)
                        .fontSize(28).fontColor(0xFFFF00FF).text(userName).rotate(0)
                        .position(llText);
                mBaiduMap.addOverlay(ooText);
            }
            
            loading.hideLoading();
            ToastUtils.show(this, "获取到 "+quantity+" 个人员");
    		
    	}else{
    		loading.hideLoading();
    		ToastUtils.show(this, "没有获取到人员数据！");
    	}
    	
    }
    
    /*
     * 向地图添加文字
     */
    public void addTexts(List<MyItem> items){
    	
    	//List<MyItem> items = new ArrayList<MyItem>();
        for (int index = 0; index < items.size(); index++) {
        	
        	LatLng llText = items.get(index).getPosition();
        	String userName = items.get(index).getUserName();
            OverlayOptions ooText = new TextOptions().bgColor(0xAAFFFF00)
                    .fontSize(26).fontColor(0xFFFF00FF).text(userName).rotate(0)
                    .position(llText);
            mBaiduMap.addOverlay(ooText);
        }
    	
    }

    /**
     * 每个Marker点，包含Marker点坐标以及图标
     */
    public class MyItem implements ClusterItem {
        private final LatLng mPosition;
        private final int museId;
        private final String muserName;

        public MyItem(LatLng latLng, int useId, String userName) {
            mPosition = latLng;
            museId = useId;
            muserName = userName;
        }
        
        public int getUserId() {
            return museId;
        }
        
        public String getUserName() {
            return muserName;
        }
        
        @Override
        public LatLng getPosition() {
            return mPosition;
        }
        
        @Override
        public BitmapDescriptor getBitmapDescriptor() {
            return BitmapDescriptorFactory
                    .fromResource(R.drawable.icon_gcoding);
        }
    }

    @Override
    public void onMapLoaded() {
        // TODO Auto-generated method stub 添加定位代码后不需要以下功能了，传入经度纬度参数需要使用以下功能
        ms = new MapStatus.Builder().zoom(16).build();
        mBaiduMap.animateMapStatus(MapStatusUpdateFactory.newMapStatus(ms));
    }
    
    /* 画轨迹和小图 */
    private void drawPolyLine(LatLng[] userlatlngs) {

        List<LatLng> polylines = new ArrayList<LatLng>();
        for (int index = 0; index < userlatlngs.length; index++) {
            polylines.add(userlatlngs[index]);
        }
        
        OverlayOptions polylineOptions = new PolylineOptions().width(10).color(Color.RED).points(polylines);
        mPolyline = (Polyline) mBaiduMap.addOverlay(polylineOptions);
        
        OverlayOptions markerOptions;
        markerOptions = new MarkerOptions().flat(true).anchor(0.5f, 0.5f)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.arrow)).position(polylines.get(0))
                .rotate((float) getAngle(0));
        mMoveMarker = (Marker) mBaiduMap.addOverlay(markerOptions);
        
    }

    /**
     * 根据点获取图标转的角度
     */
    private double getAngle(int startIndex) {
        if ((startIndex + 1) >= mPolyline.getPoints().size()) {
            throw new RuntimeException("index out of bonds");
        }
        LatLng startPoint = mPolyline.getPoints().get(startIndex);
        LatLng endPoint = mPolyline.getPoints().get(startIndex + 1);
        return getAngle(startPoint, endPoint);
    }

    /**
     * 根据两点算取图标转的角度
     */
    private double getAngle(LatLng fromPoint, LatLng toPoint) {
        double slope = getSlope(fromPoint, toPoint);
        if (slope == Double.MAX_VALUE) {
            if (toPoint.latitude > fromPoint.latitude) {
                return 0;
            } else {
                return 180;
            }
        }
        float deltAngle = 0;
        if ((toPoint.latitude - fromPoint.latitude) * slope < 0) {
            deltAngle = 180;
        }
        double radio = Math.atan(slope);
        double angle = 180 * (radio / Math.PI) + deltAngle - 90;
        return angle;
    }

    /**
     * 根据点和斜率算取截距
     */
    private double getInterception(double slope, LatLng point) {

        double interception = point.latitude - slope * point.longitude;
        return interception;
    }

    /**
     * 算斜率
     */
    private double getSlope(LatLng fromPoint, LatLng toPoint) {
        if (toPoint.longitude == fromPoint.longitude) {
            return Double.MAX_VALUE;
        }
        double slope = ((toPoint.latitude - fromPoint.latitude) / (toPoint.longitude - fromPoint.longitude));
        return slope;

    }
    
    /**
     * 计算x方向每次移动的距离
     */
    private double getXMoveDistance(double slope) {
        if (slope == Double.MAX_VALUE) {
            return DISTANCE;
        }
        return Math.abs((DISTANCE * slope) / Math.sqrt(1 + slope * slope));
    }

    /**
     * 循环进行移动逻辑
     */
    public void moveLooper(final LatLng[] userlatlngs) {
        new Thread() {

            public void run() {

                while (true) {

                    for (int i = 0; i < userlatlngs.length - 1; i++) {


                        final LatLng startPoint = userlatlngs[i];
                        final LatLng endPoint = userlatlngs[i + 1];
                        mMoveMarker
                                .setPosition(startPoint);

                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                // refresh marker's rotate
                                if (mMapView == null) {
                                    return;
                                }
                                mMoveMarker.setRotate((float) getAngle(startPoint,
                                        endPoint));
                            }
                        });
                        double slope = getSlope(startPoint, endPoint);
                        // 是不是正向的标示
                        boolean isReverse = (startPoint.latitude > endPoint.latitude);

                        double intercept = getInterception(slope, startPoint);

                        double xMoveDistance = isReverse ? getXMoveDistance(slope) : -1 * getXMoveDistance(slope);


                        for (double j = startPoint.latitude; !((j > endPoint.latitude) ^ isReverse);
                             j = j - xMoveDistance) {
                            LatLng latLng = null;
                            if (slope == Double.MAX_VALUE) {
                                latLng = new LatLng(j, startPoint.longitude);
                            } else {
                                latLng = new LatLng(j, (j - intercept) / slope);
                            }

                            final LatLng finalLatLng = latLng;
                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if (mMapView == null) {
                                        return;
                                    }
                                    mMoveMarker.setPosition(finalLatLng);
                                }
                            });
                            try {
                                Thread.sleep(TIME_INTERVAL);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                }
            }

        }.start();
    }
    
    //轨迹的经度纬度
    private static final LatLng[] latlngs = new LatLng[] {
    	/*
            new LatLng(40.055826, 116.307917),
            new LatLng(40.055916, 116.308455),
        */
    };
    
    //直接获取数据
    public LatLng[] getLatLngByUserId(int userId){
    	
    	SysPassValue temp = new SysPassValue();
		temp.setFunctionName("getOnlineUserTrack");
		temp.setUserId(String.valueOf(userId));
		
		new sendOrderToServerForListYTask(new AbstractYGetTaskListener<List<SysPassValue>>() {//获取在线网格员的当前位置
			@Override
			public void onPostExecute(String name, YGetTask<List<SysPassValue>> task) {
				 
				trackLatLngs = getDataTrack(task.getValue());			
			}
		},temp).execute();
		
		return trackLatLngs;
    }
    
    /*
     * 获取数据后，画线和加载小车
     */
    public LatLng[] getDataTrack(List<SysPassValue> list) {
    	
    	if(list!=null){
			trackLatLngs = new LatLng[list.size()];//必须先填写好空间，不然会报错
			
			for (int m = 0; m < list.size(); m++) {
	    		double baiduX = Double.valueOf(list.get(m).getArg2());
	    		double baiduY = Double.valueOf(list.get(m).getArg3());
	    		Log.i("longhua", "baiduX"+baiduX);
				Log.i("longhua", "baiduY"+baiduY);
				trackLatLngs[m] = new LatLng(baiduY, baiduX);
	    	}
			
			loading.hideLoading();
			drawPolyLine(trackLatLngs);
			ToastUtils.show(this, "加载移动轨迹！");
			thread = new WgMapThread(trackLatLngs,mMapView,mMoveMarker,mHandler);
			thread.start();
			
		}else{
			loading.hideLoading();
    		ToastUtils.show(this, "getDataTrack没有获取到轨迹数据！");
    	}
    	
    	return trackLatLngs;
    }
    
    
    public void getTrackLatLng(int userId){
    	
    	SysPassValue temp = new SysPassValue();
		temp.setFunctionName("getOnlineUserTrack");
		//temp.setUserId(String.valueOf(398));
		temp.setUserId(String.valueOf(userId));
		temp.setArg2(String.valueOf(currentLongitude));//baiduX，第一次运行的时候，先执行了这个函数，然后才获取到当前经度纬度，应该是速度问题，所以没有传递当前经度纬度过去，第二次点开地图没问题
		temp.setArg3(String.valueOf(currentLatitude));
		
		new sendOrderToServerForListYTask(new AbstractYGetTaskListener<List<SysPassValue>>() {//获取在线网格员的当前位置
			@Override
			public void onPostExecute(String name, YGetTask<List<SysPassValue>> task) {
				//Log.i("longhua", "3 getDataTrack2");
				getUserTrack(task.getValue());
				
			}
		},temp).execute();
		
		//把画线和加载轨迹放到数据返回里面实现
		//WgMapData mapdata = new WgMapData();
		//trackLatLngs = mapdata.getLatLngByUserId(1);
		
    }
    
    public void getUserTrack(List<SysPassValue> list) {
    	
    	if(list!=null){
    		ToastUtils.show(this, "加载人员轨迹！");
    		trackLatLngs = new LatLng[list.size()];//必须先填写好空间，不然会报错
			
			for (int m = 0; m < list.size(); m++) {
	    		double baiduX = Double.valueOf(list.get(m).getArg2());
	    		double baiduY = Double.valueOf(list.get(m).getArg3());
	    		Log.i("longhua", "baiduX"+baiduX);
				Log.i("longhua", "baiduY"+baiduY);
				trackLatLngs[m] = new LatLng(baiduY, baiduX);
	    	}
			
			drawPolyLine(trackLatLngs);
			thread = new WgMapThread(trackLatLngs,mMapView,mMoveMarker,mHandler);
			thread.start();
            loading.hideLoading();
		}else{
    		ToastUtils.show(this, "没有获取到轨迹数据！");
    		loading.hideLoading();
    	}
    }
    
    class ThreadDemo implements Runnable {
        public void run() {
        	//Log.i("longhua", "1 run tempUserId"+tempUserId);
        	getTrackLatLng(tempUserId);
            myHandler.sendEmptyMessage(0);
        }
    }
    
    Handler myHandler = new Handler() {
    	
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case 0:
            	Log.i("longhua", "handleMessage有数据");
            	//drawPolyLine(trackLatLngs);
            	//thread = new WgMapThread(trackLatLngs,mMapView,mMoveMarker,mHandler);
				//thread.start();
                //loading.hideLoading();
                break;
            case 1:
                Log.i("longhua", "handleMessage无数据");
                loading.hideLoading();
                break;
            }
            super.handleMessage(msg);
        }
    };

}
