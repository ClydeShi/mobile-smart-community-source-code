package com.zhyq;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.zhyq.libs.ProgersssDialog;
import com.zhyq.libs.ToastUtils;
import com.zhyq.widget.LoadingRelativeLayout;


public class ZyCompanyServiceThree extends Activity implements OnClickListener {

	public ProgersssDialog dialog;

	private String userCode = "";
	private String userName = "";
	private Long userId = null;
	private int id = 0;

	private LoadingRelativeLayout loading = null;
	public int resultCode = 0;
	public static boolean active = false;

	private View includeTitle ;

	private String type = "";
	private String name = "";
	private String channelName = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		showProgressDialog();

		this.setContentView(R.layout.activity_zy_service_three);
		loading = LoadingRelativeLayout.getLoadingRelativeLayout(this);

		name = this.getIntent().getStringExtra("name");
		type = this.getIntent().getStringExtra("type");
		if(type==null){
			ToastUtils.show(this, "参数错误！");
		}
		if(type.equals("1")){
			name = "创投服务";
		}else if(type.equals("2")){
			name = "出行服务";
		}else if(type.equals("3")){
			name = "日常服务";
		}

		includeTitle = this.findViewById(R.id.zy_service_head);
		((TextView)includeTitle.findViewById(R.id.util_my_head_title)).setText(name);
		Button btnReturn = (Button)includeTitle.findViewById(R.id.util_my_head_btn_return);
		btnReturn.setOnClickListener(this);

		View util_my_head_relativelayout1 = (View)includeTitle.findViewById(R.id.util_my_head_relativelayout1);
		util_my_head_relativelayout1.setVisibility(View.VISIBLE);

		View util_my_head_relativelayout2 = (View)includeTitle.findViewById(R.id.util_my_head_relativelayout2);
		util_my_head_relativelayout2.setVisibility(View.INVISIBLE);

		(this.findViewById(R.id.service_btn1)).setOnClickListener(this);
		(this.findViewById(R.id.service_btn2)).setOnClickListener(this);
		(this.findViewById(R.id.service_btn3)).setOnClickListener(this);
		(this.findViewById(R.id.service_btn4)).setOnClickListener(this);
		(this.findViewById(R.id.service_btn5)).setOnClickListener(this);
		(this.findViewById(R.id.service_btn6)).setOnClickListener(this);
		(this.findViewById(R.id.service_btn7)).setOnClickListener(this);
		(this.findViewById(R.id.service_btn8)).setOnClickListener(this);
		(this.findViewById(R.id.service_btn9)).setOnClickListener(this);
		(this.findViewById(R.id.service_btn10)).setOnClickListener(this);
		(this.findViewById(R.id.service_btn11)).setOnClickListener(this);
		(this.findViewById(R.id.service_btn12)).setOnClickListener(this);
		(this.findViewById(R.id.service_btn13)).setOnClickListener(this);
		(this.findViewById(R.id.service_btn14)).setOnClickListener(this);

		new Handler().postDelayed(new Runnable() {//延迟读取数据
			@Override
			public void run() {
				dialog.dismiss();
			}
		}, 200);

	}

	public void showProgressDialog(){
		dialog = new ProgersssDialog(this);
		//dialog.show();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	public void onClick(View view) {
		if(R.id.util_my_head_btn_return==view.getId()){
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		}

		if(R.id.service_btn1==view.getId()){
			Intent intent=new Intent(view.getContext(), ZyCleanAddActivity.class);
			Bundle bundle=new Bundle();
			bundle.putString("channelId", "2");
			intent.putExtras(bundle);
			startActivity(intent);
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			return;
		}

		if(R.id.service_btn2==view.getId()){
			Intent intent=new Intent(view.getContext(), ZyCleanAddActivity.class);
			Bundle bundle=new Bundle();
			bundle.putString("channelId", "1");
			intent.putExtras(bundle);
			startActivity(intent);
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			return;
		}

		if(R.id.service_btn3==view.getId()){
			Toast.makeText(view.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
			return;
		}

		if(R.id.service_btn4==view.getId()){
			Intent intent=new Intent(view.getContext(), ZyVisitorAddActivity.class);
			Bundle bundle=new Bundle();
			bundle.putString("channelId", "1");
			intent.putExtras(bundle);
			startActivity(intent);
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			return;
		}

		if(R.id.service_btn5==view.getId()){
			Toast.makeText(view.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
			return;
		}

		if(R.id.service_btn6==view.getId()){
			Toast.makeText(view.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
			return;
		}

		if(R.id.service_btn7==view.getId()){
			Toast.makeText(view.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
			return;
		}

		if(R.id.service_btn8==view.getId()){
			Toast.makeText(view.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
			return;
		}

		if(R.id.service_btn9==view.getId()){
			Toast.makeText(view.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
			return;
		}

		if(R.id.service_btn10==view.getId()){
			Toast.makeText(view.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
			return;
		}

		if(R.id.service_btn11==view.getId()){
			Intent intent=new Intent(view.getContext(), ZyMeetingRoomListActivity.class);
			Bundle bundle=new Bundle();
			bundle.putString("channelId", "2");
			intent.putExtras(bundle);
			startActivity(intent);
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			return;
		}

		if(R.id.service_btn12==view.getId()){
			Intent intent=new Intent(view.getContext(), ZyMeetingRoomListActivity.class);
			Bundle bundle=new Bundle();
			bundle.putString("channelId", "1");
			intent.putExtras(bundle);
			startActivity(intent);
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			return;
		}

		if(R.id.service_btn13==view.getId()){
			Toast.makeText(view.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
			return;
		}

		if(R.id.service_btn14==view.getId()){
			Toast.makeText(view.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
			return;
		}


	}


}
