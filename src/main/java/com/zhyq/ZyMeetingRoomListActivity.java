package com.zhyq;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.aspsine.irecyclerview.IRecyclerView;
import com.aspsine.irecyclerview.OnLoadMoreListener;
import com.aspsine.irecyclerview.OnRefreshListener;
import com.aspsine.irecyclerview.demo.model.Image;
import com.aspsine.irecyclerview.demo.ui.adapter.OnItemClickListener;
import com.aspsine.irecyclerview.demo.ui.widget.footer.LoadMoreFooterView;
import com.zhyq.adapter.baseRecyleViewAdapter;
import com.zhyq.libs.AndroidUtils;
import com.zhyq.libs.BaseActivity;
import com.zhyq.libs.PreferenceUtils;
import com.zhyq.libs.ProgersssDialog;
import com.zhyq.model.SysPassValue;
import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.sendOrderToServerForListYTask;

import java.util.ArrayList;
import java.util.List;

/*
使用实现上拉下拉刷新，原来是继承AppCompatActivity，那样要在AndroidManifest Activity里面使用AppCompat的theme,如android:theme="@style/Theme.AppCompat.Light.NoActionBar"
使用android:theme="@style/Theme.AppCompat.Light.NoActionBar"的话，转圈显示绿色
继承BaseActivity也可以
 */

public class ZyMeetingRoomListActivity extends BaseActivity implements OnClickListener, OnItemClickListener<Image>, OnRefreshListener, OnLoadMoreListener {

    private View includeTitle;
    private IRecyclerView iRecyclerView;
    private View headerView = null;
    private View headerView2 = null;
    private LoadMoreFooterView loadMoreFooterView;

    private baseRecyleViewAdapter mAdapter;

    private int mPage;

    private int currentPage = 1;
    private List<SysPassValue> dataList = new ArrayList<SysPassValue>();//数据

    public ProgersssDialog dialog;

    private long userId = 0;
    private String userCode = "";
    private String userName = "";
    private String userRole = "";

    private EditText et_keyword = null;
    private Button bt_clear = null;

    private String channelId = "1";
    private String channelName = "申请会议室";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        showProgressDialog();

        setContentView(R.layout.activity_zy_common_list);
        includeTitle = this.findViewById(R.id.zy_util_head_common_list);

        channelId = this.getIntent().getStringExtra("channelId");
        if(channelId==null){
            channelId = "1";
        }
        if(channelId.equals("1")){
            channelName = "申请会议室";
        }else if(channelId.equals("2")){
            channelName = "申请场地";
        }
        //Log.i("luktel","channelName"+channelName);

        ((TextView) includeTitle.findViewById(R.id.wg_head_title)).setText(channelName);
        Button btnReturn = (Button) includeTitle.findViewById(R.id.util_head_btn_return);
        btnReturn.setOnClickListener(this);

        et_keyword = (EditText)this.findViewById(R.id.wg_div_input_keywords);
        bt_clear = (Button)this.findViewById(R.id.button_search_clear);
        bt_clear.setOnClickListener(this);

        et_keyword.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //Log.i("luktel","search");
                onRefresh();
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //textview.setText(edittext.getText());
            }
            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                //Log.i("spc","afterTextChanged");
                //computerRealAmt(et_keyword.getText().toString(),yuihuiAmt);
            }
        });

        iRecyclerView = (IRecyclerView) findViewById(R.id.zy_commonlist_recyclerview);
        iRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        //toggleRefreshHeader();//切换上拉刷新面板

        //bannerView = (BannerView) LayoutInflater.from(this).inflate(R.layout.layout_irecyclerview_banner_view, iRecyclerView.getHeaderContainer(), false);
        //iRecyclerView.addHeaderView(bannerView);

        //headerView = View.inflate(this, R.layout.item_wg_event_detail, null);
        //headerView = LayoutInflater.from(this).inflate(R.layout.item_wg_event_detail, null);
        headerView =  LayoutInflater.from(this).inflate(R.layout.item_wg_event_detail, iRecyclerView.getHeaderContainer(), false);//未加入面板
        //iRecyclerView.addHeaderView(headerView);
        //getDetailData();

        //headerView2 = View.inflate(this, R.layout.item_wg_task_header, null);
        //iRecyclerView.addHeaderView(headerView2);

        //解决卡顿问题
        iRecyclerView.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
        iRecyclerView.setFocusable(true);
        iRecyclerView.setFocusableInTouchMode(true);
        iRecyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                view.requestFocusFromTouch();
                return false;
            }
        });

        loadMoreFooterView = (LoadMoreFooterView) iRecyclerView.getLoadMoreFooterView();

        mAdapter = new baseRecyleViewAdapter(this,dataList,"item_zy_meetingroom_list");//通用adapter
        //mAdapter = new WgFeedbackListNewAdapter(dataList);
        mAdapter.setOnClick1(this);
        iRecyclerView.setIAdapter(mAdapter);

        iRecyclerView.setOnRefreshListener(this);
        iRecyclerView.setOnLoadMoreListener(this);

        //mAdapter.setOnItemClickListener(this);

        iRecyclerView.post(new Runnable() {
            @Override
            public void run() {
                iRecyclerView.setRefreshing(true);
            }
        });

        userId = PreferenceUtils.getLong(this, "userid", -1);
        userRole = PreferenceUtils.getString(this,"userrole","");
    }

    public void showProgressDialog(){
        dialog = new ProgersssDialog(this);
        //dialog.show();
    }

    @Override
    public void onClick(View view) {

        if (R.id.util_head_btn_return == view.getId()) {
            this.finish();
            overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
            return;
        }

        if(R.id.button_search_clear==view.getId()){
            et_keyword.setText("");
            onRefresh();
            return;
        }

        if(R.id.item_zy_list_btn==view.getId()){

            //传递参数过去
            Object tag = view.getTag(R.id.button_tag1);

            if (tag != null && tag instanceof Integer) { //解决问题：如何知道你点击的按钮是哪一个列表项中的，通过Tag的position
                //ToastUtils.show(v.getContext(), "22详情  NULL");
                int position = (Integer) tag;
                String ids =  dataList.get(position).getId();
                String name =  dataList.get(position).getArg1();
                //Toast.makeText(view.getContext(), "ids"+ids, Toast.LENGTH_SHORT).show();
                //Toast.makeText(view.getContext(), "name"+name, Toast.LENGTH_SHORT).show();
                //return;
                Intent intent = new Intent(view.getContext(), ZyMeetingRoomViewActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("id", ids);
                bundle.putString("name", name);
                bundle.putString("channelId", channelId);
                intent.putExtras(bundle);
                startActivity(intent);
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                return;
            }
        }
    }

    @Override
    public void onItemClick(int position, Image image, View v) {
        //Toast.makeText(this, String.valueOf(position), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRefresh() {
        //loadBanner();
        loadMoreFooterView.setStatus(LoadMoreFooterView.Status.GONE);
        currentPage = 1;
        getData();
    }

    @Override
    public void onLoadMore() {
        if (loadMoreFooterView.canLoadMore() && mAdapter.getItemCount() > 0) {
            loadMoreFooterView.setStatus(LoadMoreFooterView.Status.LOADING);
            getData();
        }
    }

    public void getData() {
        String keywords = et_keyword.getText().toString();
        SysPassValue temp = new SysPassValue();
        temp.setFunctionName("getMeetingRoomList");
        temp.setPageNumber(String.valueOf(this.currentPage));
        temp.setArg1(channelId);
        temp.setArg2(keywords);
        temp.setUserId(String.valueOf(userId));

        new sendOrderToServerForListYTask(new AbstractYGetTaskListener<List<SysPassValue>>() {
            @Override
            public void onPostExecute(String name, YGetTask<List<SysPassValue>> task) {
                iRecyclerView.setRefreshing(false);
                showData(task.getValue());
                dialog.dismiss();
            }
        }, temp).execute();
    }

    public void showData(final List<SysPassValue> list) {

        View vnoRecord = this.findViewById(R.id.activity_no_record_showpic);
        if (list == null) {
            loadMoreFooterView.setStatus(LoadMoreFooterView.Status.ERROR);
            //Toast.makeText(this, "网络连接失败", Toast.LENGTH_SHORT).show();

            if (currentPage == 1) {
                this.dataList.clear();
                mAdapter.notifyDataSetChanged();
                vnoRecord.setVisibility(View.INVISIBLE);
                return;
            }
            return;
        }

        if (list.size() == 0) {
            if (currentPage == 1) {
                this.dataList.clear();
                mAdapter.notifyDataSetChanged();
                vnoRecord.setVisibility(View.VISIBLE);//如果是第一页没有数据则显示无记录
            } else {
                loadMoreFooterView.setStatus(LoadMoreFooterView.Status.THE_END);
            }
            //Toast.makeText(this, "已经到了最后一页", Toast.LENGTH_SHORT).show();
        } else {
            if (currentPage == 1) {
                currentPage = 2;
                this.dataList.clear();
                this.dataList.addAll(list);
                vnoRecord.setVisibility(View.GONE);
            } else {

                loadMoreFooterView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        currentPage++;
                        loadMoreFooterView.setStatus(LoadMoreFooterView.Status.GONE);
                        dataList.addAll(list);
                    }
                }, 1000);

                //loadMoreFooterView.setStatus(LoadMoreFooterView.Status.GONE);
                //dataList.addAll(list);
            }
            mAdapter.notifyDataSetChanged();
        }
        //vnoRecord.setVisibility(View.GONE);

        //如果第一页则需要清除数据

        if (dataList.size() == 0) {
            //vnoRecord.setVisibility(View.VISIBLE);
        }

        //Log.d("luktel", "showData");

    }

    public void getDetailData() {
        AndroidUtils.setTextView(headerView, R.id.wg_eventdetail_textview2, "test");
        AndroidUtils.setTextView(headerView, R.id.wg_eventdetail_textview3, "test3");
        AndroidUtils.setTextView(headerView, R.id.wg_eventdetail_textview4, "test4");
        AndroidUtils.setTextView(headerView, R.id.wg_eventdetail_textview5, "test5");
    }


}
