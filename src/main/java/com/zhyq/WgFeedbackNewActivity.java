package com.zhyq;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aspsine.irecyclerview.IRecyclerView;
import com.aspsine.irecyclerview.OnLoadMoreListener;
import com.aspsine.irecyclerview.OnRefreshListener;
import com.aspsine.irecyclerview.demo.model.Image;
import com.aspsine.irecyclerview.demo.network.NetworkAPI;
import com.aspsine.irecyclerview.demo.ui.adapter.OnItemClickListener;
import com.aspsine.irecyclerview.demo.ui.widget.footer.LoadMoreFooterView;
import com.aspsine.irecyclerview.demo.ui.widget.header.BatVsSupperHeaderView;
import com.aspsine.irecyclerview.demo.ui.widget.header.ClassicRefreshHeaderView;
import com.aspsine.irecyclerview.demo.utils.DensityUtils;
import com.aspsine.irecyclerview.demo.utils.ListUtils;
import com.zhyq.adapter.WgFeedbackListNewAdapter;
import com.zhyq.libs.AndroidUtils;
import com.zhyq.libs.BaseActivity;
import com.zhyq.libs.ProgersssDialog;
import com.zhyq.model.SysPassValue;
import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.sendOrderToServerForListYTask;

import java.util.ArrayList;
import java.util.List;

/*
使用实现上拉下拉刷新，原来是继承AppCompatActivity，那样要在AndroidManifest Activity里面使用AppCompat的theme,如android:theme="@style/Theme.AppCompat.Light.NoActionBar"
使用android:theme="@style/Theme.AppCompat.Light.NoActionBar"的话，转圈显示绿色
继承BaseActivity也可以
 */

public class WgFeedbackNewActivity extends BaseActivity implements OnClickListener, OnItemClickListener<Image>, OnRefreshListener, OnLoadMoreListener {

    private View includeTitle;
    private IRecyclerView iRecyclerView;
    private View headerView = null;
    private View headerView2 = null;
    private LoadMoreFooterView loadMoreFooterView;

    private WgFeedbackListNewAdapter mAdapter;

    private int mPage;

    private int currentPage = 1;
    private List<SysPassValue> dataList = new ArrayList<SysPassValue>();//数据

    public ProgersssDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        showProgressDialog();

        setContentView(R.layout.activity_wg_feedback_newlist);
        includeTitle = this.findViewById(R.id.wg_util_head_feedback_list);
        ((TextView) includeTitle.findViewById(R.id.wg_head_title)).setText("检查反馈");
        Button btnReturn = (Button) includeTitle.findViewById(R.id.wg_head_btn_return);
        btnReturn.setOnClickListener(this);

        iRecyclerView = (IRecyclerView) findViewById(R.id.wg_feedback_recyclerView);
        iRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        //toggleRefreshHeader();//切换上拉刷新面板

        //bannerView = (BannerView) LayoutInflater.from(this).inflate(R.layout.layout_irecyclerview_banner_view, iRecyclerView.getHeaderContainer(), false);
        //iRecyclerView.addHeaderView(bannerView);

        //headerView = View.inflate(this, R.layout.item_wg_event_detail, null);
        //headerView = LayoutInflater.from(this).inflate(R.layout.item_wg_event_detail, null);
        headerView =  LayoutInflater.from(this).inflate(R.layout.item_wg_event_detail, iRecyclerView.getHeaderContainer(), false);
        //iRecyclerView.addHeaderView(headerView);
        //getDetailData();

        //headerView2 = View.inflate(this, R.layout.item_wg_task_header, null);
        //iRecyclerView.addHeaderView(headerView2);

        //解决卡顿问题
        iRecyclerView.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
        iRecyclerView.setFocusable(true);
        iRecyclerView.setFocusableInTouchMode(true);
        iRecyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                view.requestFocusFromTouch();
                return false;
            }
        });

        loadMoreFooterView = (LoadMoreFooterView) iRecyclerView.getLoadMoreFooterView();

        mAdapter = new WgFeedbackListNewAdapter(dataList);
        mAdapter.setOnClick1(this);
        iRecyclerView.setIAdapter(mAdapter);

        iRecyclerView.setOnRefreshListener(this);
        iRecyclerView.setOnLoadMoreListener(this);

        mAdapter.setOnItemClickListener(this);

        iRecyclerView.post(new Runnable() {
            @Override
            public void run() {
                iRecyclerView.setRefreshing(true);
            }
        });
    }

    public void showProgressDialog(){
        dialog = new ProgersssDialog(this);
        //dialog.show();
    }

    @Override
    public void onClick(View view) {

        if (R.id.wg_head_btn_return == view.getId()) {
            this.finish();
            overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
            return;
        }

        if (R.id.item_btn1 == view.getId()) {

            Object tag = view.getTag();

            if (tag != null) {
                int position = (Integer) tag;
                //Log.d("luktel", "position:" + position);
                String taskId = dataList.get(position).getId();
                String taskName = dataList.get(position).getArg1();
                Intent in = new Intent(view.getContext(), WgFeedbackViewActivity.class);
                in.putExtra("taskId", String.valueOf(taskId));
                in.putExtra("taskName", taskName);
                this.startActivity(in);
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                return;
            }

        }
    }

    @Override
    public void onItemClick(int position, Image image, View v) {//可以参考原文件实现
        //mAdapter.remove(position);
        Toast.makeText(this, String.valueOf(position), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRefresh() {
        //loadBanner();
        loadMoreFooterView.setStatus(LoadMoreFooterView.Status.GONE);
        refresh();//使用NetworkAPI获取数据,json数据里面的字段必须和SysPassValue的一样
        /*
        currentPage = 1;
        getData();
        */
    }

    @Override
    public void onLoadMore() {
        if (loadMoreFooterView.canLoadMore() && mAdapter.getItemCount() > 0) {
            loadMoreFooterView.setStatus(LoadMoreFooterView.Status.LOADING);
            /*
            getData();//使用HttpRequest获取数据
            */
            loadMore();//使用NetworkAPI获取数据
        }
    }

    public void getData() {

        SysPassValue temp = new SysPassValue();
        temp.setFunctionName("getFeedbackList");//getContactList
        temp.setPageNumber(String.valueOf(this.currentPage));
        temp.setArg1("");

        new sendOrderToServerForListYTask(new AbstractYGetTaskListener<List<SysPassValue>>() {
            @Override
            public void onPostExecute(String name, YGetTask<List<SysPassValue>> task) {
                iRecyclerView.setRefreshing(false);
                showData(task.getValue());
                dialog.dismiss();
            }
        }, temp).execute();
    }

    public void showData(final List<SysPassValue> list) {

        View vnoRecord = this.findViewById(R.id.activity_no_record_showpic);
        if (list == null) {
            loadMoreFooterView.setStatus(LoadMoreFooterView.Status.ERROR);
            //Toast.makeText(this, "网络连接失败", Toast.LENGTH_SHORT).show();

            if (currentPage == 1) {
                this.dataList.clear();
                mAdapter.notifyDataSetChanged();
                vnoRecord.setVisibility(View.INVISIBLE);
                return;
            }
            return;
        }

        if (list.size() == 0) {
            if (currentPage == 1) {
                this.dataList.clear();
                mAdapter.notifyDataSetChanged();
                vnoRecord.setVisibility(View.VISIBLE);//如果是第一页没有数据则显示无记录
            } else {
                loadMoreFooterView.setStatus(LoadMoreFooterView.Status.THE_END);
            }
            //Toast.makeText(this, "已经到了最后一页", Toast.LENGTH_SHORT).show();
        } else {
            if (currentPage == 1) {
                currentPage = 2;
                this.dataList.clear();
                this.dataList.addAll(list);
                vnoRecord.setVisibility(View.GONE);
            } else {

                loadMoreFooterView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        currentPage++;
                        loadMoreFooterView.setStatus(LoadMoreFooterView.Status.GONE);
                        dataList.addAll(list);
                    }
                }, 1000);

                //loadMoreFooterView.setStatus(LoadMoreFooterView.Status.GONE);
                //dataList.addAll(list);
            }
            mAdapter.notifyDataSetChanged();
        }
        //vnoRecord.setVisibility(View.GONE);

        //如果第一页则需要清除数据

        if (dataList.size() == 0) {
            //vnoRecord.setVisibility(View.VISIBLE);
        }

        //Log.d("luktel", "showData");

    }

    public void getDetailData() {
        AndroidUtils.setTextView(headerView, R.id.wg_eventdetail_textview2, "test");
        AndroidUtils.setTextView(headerView, R.id.wg_eventdetail_textview3, "test3");
        AndroidUtils.setTextView(headerView, R.id.wg_eventdetail_textview4, "test4");
        AndroidUtils.setTextView(headerView, R.id.wg_eventdetail_textview5, "test5");
    }

    private void toggleRefreshHeader() {//两个上拉刷新面板
        if (iRecyclerView.getRefreshHeaderView() instanceof BatVsSupperHeaderView) {
            ClassicRefreshHeaderView classicRefreshHeaderView = new ClassicRefreshHeaderView(this);
            classicRefreshHeaderView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, DensityUtils.dip2px(this, 80)));
            // we can set view
            iRecyclerView.setRefreshHeaderView(classicRefreshHeaderView);
            //Toast.makeText(this, "Classic style", Toast.LENGTH_SHORT).show();
        } else if (iRecyclerView.getRefreshHeaderView() instanceof ClassicRefreshHeaderView) {
            // we can also set layout
            iRecyclerView.setRefreshHeaderView(R.layout.layout_irecyclerview_refresh_header_batvssupper);
            //Toast.makeText(this, "Bat man vs Super man style", Toast.LENGTH_SHORT).show();
        }
    }

    private void refresh() {
        dialog.dismiss();
        mPage = 1;
        NetworkAPI.requestImages(mPage, new NetworkAPI.Callback<List<SysPassValue>>() {
            @Override
            public void onSuccess(List<SysPassValue> list) {
                iRecyclerView.setRefreshing(false);
                if (ListUtils.isEmpty(list)) {
                    mAdapter.clear();
                } else {
                    mPage = 2;
                    mAdapter.setList(list);
                }
            }

            @Override
            public void onFailure(Exception e) {
                e.printStackTrace();
                iRecyclerView.setRefreshing(false);
                Toast.makeText(WgFeedbackNewActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadMore() {
        NetworkAPI.requestImages(mPage, new NetworkAPI.Callback<List<SysPassValue>>() {
            @Override
            public void onSuccess(final List<SysPassValue> list) {
                if (ListUtils.isEmpty(list)) {
                    loadMoreFooterView.setStatus(LoadMoreFooterView.Status.THE_END);
                } else {
                    loadMoreFooterView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mPage++;
                            loadMoreFooterView.setStatus(LoadMoreFooterView.Status.GONE);
                            mAdapter.append(list);
                        }
                    }, 1000);
                }
            }

            @Override
            public void onFailure(Exception e) {
                e.printStackTrace();
                loadMoreFooterView.setStatus(LoadMoreFooterView.Status.ERROR);
                Toast.makeText(WgFeedbackNewActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /*
    private void loadBanner() {
        NetworkAPI.requestBanners(new NetworkAPI.Callback<List<Image>>() {
            @Override
            public void onSuccess(List<Image> images) {
                if (!ListUtils.isEmpty(images)) {
                    //headerView.setList(images);
                }
            }

            @Override
            public void onFailure(Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void refresh() {
        mPage = 1;
        NetworkAPI.requestImages(mPage, new NetworkAPI.Callback<List<Image>>() {
            @Override
            public void onSuccess(List<Image> images) {
                iRecyclerView.setRefreshing(false);
                if (ListUtils.isEmpty(images)) {
                    mAdapter.clear();
                } else {
                    mPage = 2;
                    mAdapter.setList(images);
                }
            }

            @Override
            public void onFailure(Exception e) {
                e.printStackTrace();
                iRecyclerView.setRefreshing(false);
                Toast.makeText(WgFeedbackNewActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadMore() {
        NetworkAPI.requestImages(mPage, new NetworkAPI.Callback<List<Image>>() {
            @Override
            public void onSuccess(final List<Image> images) {
                if (ListUtils.isEmpty(images)) {
                    loadMoreFooterView.setStatus(LoadMoreFooterView.Status.THE_END);
                } else {
                    loadMoreFooterView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mPage++;
                            loadMoreFooterView.setStatus(LoadMoreFooterView.Status.GONE);
                            mAdapter.append(images);
                        }
                    }, 2000);
                }
            }

            @Override
            public void onFailure(Exception e) {
                e.printStackTrace();
                loadMoreFooterView.setStatus(LoadMoreFooterView.Status.ERROR);
                Toast.makeText(WgFeedbackNewActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }
    */


}
