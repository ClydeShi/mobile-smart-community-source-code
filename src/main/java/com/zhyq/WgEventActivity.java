package com.zhyq;

 
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
 

import com.handmark.pulltorefresh.library.ILoadingLayout;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.zhyq.adapter.WgEventListAdapter;

import com.zhyq.model.SysPassValue;
import com.zhyq.model.User;
 

import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
 
import com.zhyq.task.impl.sendOrderToServerForListYTask;
 
import com.zhyq.widget.LoadingRelativeLayout;


import com.zhyq.libs.view.wheelview.JudgeDate;
import com.zhyq.libs.view.wheelview.ScreenInfo;
import com.zhyq.libs.view.wheelview.WheelMain;
import com.zhyq.libs.AndroidUtils;
 
 
 

import android.app.Activity;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.BitmapDrawable;
 
 
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;


import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;


public class WgEventActivity extends Activity implements OnItemClickListener,OnClickListener {
	
	private LoadingRelativeLayout loading = null;
	
	private View includeTitle;
	private ImageView bottomImage;
	private TextView bottomWord;
	
	private PullToRefreshListView mPullRefreshListView;
	private int currentPage = 1; 
	private String lastRefreshdt = "";
	
	private EditText et_keyword = null; 
	private Button bt_clear = null;
	 
	private String userCode = "";
	private String userName = "";
	private String userId = "";
	private String userface = "";
	 
	private PopupWindow popupWindow;
	private float alpha = 1f;
	 
	WheelMain wheelMain;
	TextView txtstartdate;
	TextView txtenddate;
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	/*
	 * 数据适配器
	 */
    private WgEventListAdapter mAdapter;
    
    /*
     * 数据
     */
    private List<SysPassValue> NewsList = new ArrayList<SysPassValue>();
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.setContentView(R.layout.activity_wg_event);
		loading=LoadingRelativeLayout.getLoadingRelativeLayout(this);
		//getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		includeTitle = this.findViewById(R.id.wg_event_head);		
		Button btreturn = (Button)includeTitle.findViewById(R.id.wg_head_search_btn_return);
		btreturn.setOnClickListener(this);		
		((TextView)includeTitle.findViewById(R.id.wg_head_search_title)).setText("案件查询");		
		Button btsearch = (Button)includeTitle.findViewById(R.id.wg_util_head_btn_search);
		btsearch.setOnClickListener(this);		
		//this.findViewById(R.id.wg_util_head_btn_search).setOnClickListener(this);		
		et_keyword = (EditText)this.findViewById(R.id.wg_event_edittext_name);
		//bt_clear    =(Button)this.findViewById(R.id.wg_contact_button_clear);
		//bt_clear.setOnClickListener(this);
		//bottomImage = (ImageView)this.findViewById(R.id.iv_feedbackaccount_emptydata_img);
		//bottomWord = (TextView)this.findViewById(R.id.iv_feedbackaccount_emptydata_text);
		
		txtstartdate = ((TextView)this.findViewById(R.id.wg_event_date_start));
		(this.findViewById(R.id.btn_start_date)).setOnClickListener(this);
		AndroidUtils.setTextView(this, R.id.wg_event_date_start,getNowDate());
		
		txtenddate = ((TextView)this.findViewById(R.id.wg_event_date_end));
		(this.findViewById(R.id.btn_end_date)).setOnClickListener(this);
		AndroidUtils.setTextView(this, R.id.wg_event_date_end,getNowDate());

		(this.findViewById(R.id.wg_util_head_btn_hsearch)).setOnClickListener(this);
		
		//设置数据
		/*
		et_keyword.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				GetData(currentPage);
				
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				//textview.setText(edittext.getText());
				
			}
			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
				//Log.i("spc","afterTextChanged");
				//computerRealAmt(et_keyword.getText().toString(),yuihuiAmt);
				
			}
		});
		*/
		
		//GetData( currentPage);

		/*
		 *  初始化数据适配器，传递Content 过去
		 */
		mAdapter = new WgEventListAdapter(this, NewsList);
		mAdapter.setOnClick1(this);
		
		/*
         * 初始化 
         */  
		mPullRefreshListView = (PullToRefreshListView) this.findViewById(R.id.xl_event_PulllistView);
		mPullRefreshListView.setMode(Mode.BOTH);
		//初始化值
		initIndicator();
		//设置下拉，上移动 得到效果
        mPullRefreshListView.setOnRefreshListener(new OnRefreshListener2<ListView>()
        {
        	//下拉效果
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView)
            {
            	refreshView.getLoadingLayoutProxy().setPullLabel("下拉刷新...");// 刚下拉时，显示的提示
            	refreshView.getLoadingLayoutProxy().setRefreshingLabel("更新中...");// 刷新时
            	refreshView.getLoadingLayoutProxy().setReleaseLabel("松开刷新...");// 下来达到一定距离时，显示的提示
                
        	   if(lastRefreshdt.equals("")) {
                   refreshView.getLoadingLayoutProxy().setLastUpdatedLabel("刚刚");//显示最后更新的时间
       	       } else {
                   refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(lastRefreshdt);//显示最后更新的时间
       	       }
        	   
        	   SimpleDateFormat    formatter    =   new    SimpleDateFormat    ("MM月dd日    HH:mm:ss     ");       
       	       Date    curDate    =   new    Date(System.currentTimeMillis());//获取当前时间       
       	       lastRefreshdt =    formatter.format(curDate);
       	       currentPage =1;
               GetData(currentPage);//加载任务
            }

            //上拉效果
            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView)
            {
                //加载任务
            	refreshView.getLoadingLayoutProxy().setPullLabel("上拉刷新...");// 刚上拉时，显示的提示
            	refreshView.getLoadingLayoutProxy().setRefreshingLabel("更新中...");// 刷新时
            	refreshView.getLoadingLayoutProxy().setReleaseLabel("松开刷新...");// 上来达到一定距离时，显示的提示
                currentPage++;
                GetData(currentPage);
            }
        });
		
		//获取控件并注册  这个是获取 listview 控件
        //ListView actualListView = mPullRefreshListView.getRefreshableView();
        //registerForContextMenu(actualListView); 
        //actualListView.setAdapter(mAdapter);
        
        mPullRefreshListView.setAdapter(mAdapter);
        onRefresh();
	}
	
	private void onRefresh(){
		mPullRefreshListView.setRefreshing(true);
        currentPage = 1;
        GetData(currentPage);
	}
	
	private void initIndicator(){
		ILoadingLayout startLabels = mPullRefreshListView.getLoadingLayoutProxy(true,false);
		
		//startLabels.setTextTypeface()
		startLabels.setPullLabel("下拉刷新...");// 刚下拉时，显示的提示
		startLabels.setRefreshingLabel("更新中...");// 刷新时
		startLabels.setReleaseLabel("松开刷新...");// 下来达到一定距离时，显示的提示
		
		ILoadingLayout endLabels = mPullRefreshListView.getLoadingLayoutProxy(false,true);
		endLabels.setPullLabel("上拉刷新...");// 刚下拉时，显示的提示
		endLabels.setRefreshingLabel("更新中...");// 刷新时
		endLabels.setReleaseLabel("松开刷新...");// 下来达到一定距离时，显示的提示
		endLabels.setLastUpdatedLabel("");
	}
	
	//onResume是在启动activity启动之后才能执行的，也就是恢复执行。程序正常启动：onCreate()->onStart()->onResume();
	@Override
	protected void onResume() {
		super.onResume();
		
	}
	
	public void GetData(int curpage){
		
		Long userid = null;
		User curuser = HhApplication.getInstance(this).getHhCart().getUser();
		if(curuser==null){
			userid =-1l;
		}else{
			userid = HhApplication.getInstance(this).getHhCart().getUser().getId();
		}
		
		String keyword = "";
		keyword = et_keyword.getText().toString();
		String startdate = txtstartdate.getText().toString();
		String enddate = txtenddate.getText().toString();
		SysPassValue temp = new SysPassValue();
		temp.setFunctionName("getEventBySearch");
		temp.setPageNumber(String.valueOf(curpage));
		temp.setArg1(String.valueOf(userid));
		temp.setArg2(keyword);
		temp.setArg3(startdate);//默认显示当天的案件
		temp.setArg4(enddate);
		
		new sendOrderToServerForListYTask(new AbstractYGetTaskListener<List<SysPassValue>>() {
			@Override
			public void onPostExecute(String name, YGetTask<List<SysPassValue>> task) {
				
				getData(task.getValue());
				loading.hideLoading();
				mPullRefreshListView.onRefreshComplete();
			}
		},temp).execute();
		
	}
	
	public void getData(List<SysPassValue> list) {
		
		//通过数据适配器获取 数据列表
		View vnorecord = this.findViewById(R.id.activity_no_record_showpic);
		if (list==null){
			if(currentPage==1){
				this.NewsList.clear(); 
	    		mAdapter.notifyDataSetChanged();
	    		vnorecord.setVisibility(View.VISIBLE);
	    		//bottomImage.setVisibility(View.VISIBLE);
	    		//bottomWord.setVisibility(View.VISIBLE);
	    		return;
			}
			return;
		}
		vnorecord.setVisibility(View.GONE);
		//bottomImage.setVisibility(View.GONE);
		//bottomWord.setVisibility(View.GONE);
		
		//如果是第一页则需要清楚数据   currentPage
		if(currentPage==1){
    		this.NewsList.clear();
			this.NewsList.addAll(list);
    	} else{
			NewsList.addAll(list);
		}
		mAdapter.notifyDataSetChanged();
		if(NewsList.size()==0){
			vnorecord.setVisibility(View.VISIBLE);
			//bottomImage.setVisibility(View.VISIBLE);
    		//bottomWord.setVisibility(View.VISIBLE);
		}
		
	}
	
	@Override
	public void onClick(View view) {
		
		 if(R.id.wg_head_search_btn_return==view.getId()) {
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		 }
		 
		 if(R.id.wg_util_head_btn_search==view.getId()) {
			 loading.showLoading();
			 GetData(currentPage);
		 }

		if(R.id.wg_util_head_btn_hsearch==view.getId()) {
			AndroidUtils.start(this, WgEventHighSearchActivity.class);
			this.finish();
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			return;
		}
		 
		 if(R.id.btn_start_date==view.getId()){
				bottombirthdaywindow(txtstartdate, "txtstartdate");
				new Thread(new Runnable(){
	                @Override
	                public void run() {
	                    while(alpha>0.5f){
	                        try {
	                            //4是根据弹出动画时间和减少的透明度计算
	                            Thread.sleep(4);
	                        } catch (InterruptedException e) {
	                            e.printStackTrace();
	                        }
	                        Message msg =mHandler.obtainMessage();
	                        msg.what = 1;
	                        //每次减少0.01，精度越高，变暗的效果越流畅
	                        alpha-=0.01f;
	                        msg.obj =alpha ;
	                        mHandler.sendMessage(msg);
	                    }
	                }
	                
	            }).start();
		 }
		 
		 if(R.id.btn_end_date==view.getId()){
				bottombirthdaywindow(txtenddate, "txtenddate");
				new Thread(new Runnable(){
	                @Override
	                public void run() {
	                    while(alpha>0.5f){
	                        try {
	                            //4是根据弹出动画时间和减少的透明度计算
	                            Thread.sleep(4);
	                        } catch (InterruptedException e) {
	                            e.printStackTrace();
	                        }
	                        Message msg =mHandler.obtainMessage();
	                        msg.what = 1;
	                        //每次减少0.01，精度越高，变暗的效果越流畅
	                        alpha-=0.01f;
	                        msg.obj =alpha ;
	                        mHandler.sendMessage(msg);
	                    }
	                }
	                
	            }).start();
		 }
		 
		 if(R.id.wg_eventlist_btn==view.getId()){//这行可以不用判断，因为已经设置wg_eventlist_btn监听多个重复id目前没有问题
			 
			 //使用UniteListAdapter的时候需要使用此方法，button_tag1是在UniteListAdapter设置的
			 //Object tag = view.getTag(R.id.button_tag1);
			 
			 Object tag = view.getTag();
			 
			 if (tag != null && tag instanceof Integer) {
					
					int position = (Integer) tag;//点击button所在Item中的位置，通过这个位置就可以得到Item中的值
					String id =  NewsList.get(position).getId();
					String title =  NewsList.get(position).getArg1();
				    Intent in=new Intent(view.getContext(),WgEventViewActivity.class);
					in.putExtra("id", String.valueOf(id));
					this.startActivity(in);
					overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
					return;
			 }
			 
		 }
		 
	}
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		
		Log.i("longhua","onItemClick");
		if(arg3 == -1) {
	        // 点击的是headerView或者footerView
	        return;
	    }
		
		//arg2是当前item的ID。这个id根据你在适配器中的写法可以自己定义
		//ToastUtils.show(arg1.getContext(), R.string.show_logon_error_msg);
		//Object tag2 = arg1.getTag(); 
        int position = (Integer) arg2;
        position = position -1;
		return;
	}
 
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	 		 
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	public String getNowDate(){
        Date currTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd",Locale.US);
        String nowdate = null;
        try {
            nowdate = new String(formatter.format(currTime).getBytes("iso-8859-1"));
        } catch (UnsupportedEncodingException ex) {
        }

        return nowdate;
    }
	
	//选择日期
	void bottombirthdaywindow(TextView view, String dateType) {
        if (popupWindow != null && popupWindow.isShowing()) {
            return;
        }
        LinearLayout timepickerview = (LinearLayout) getLayoutInflater().inflate(R.layout.timepicker, null);
        wheelMain = new WheelMain(timepickerview);
        ScreenInfo screenInfo = new ScreenInfo(WgEventActivity.this);
		wheelMain.screenheight = screenInfo.getHeight();
		String time = txtstartdate.getText().toString();
		Calendar calendar = Calendar.getInstance();
		if (JudgeDate.isDate(time, "yyyy-MM-dd")) {
			try {
				calendar.setTime(dateFormat.parse(time));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		wheelMain.initDateTimePicker(year, month, day);
		
        popupWindow = new PopupWindow(timepickerview,
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        //点击空白处时，隐藏掉pop窗口
        popupWindow.setFocusable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        //添加弹出、弹入的动画
        popupWindow.setAnimationStyle(R.style.Popupwindow);
        int[] location = new int[2];
        view.getLocationOnScreen(location);
        popupWindow.showAtLocation(view, Gravity.LEFT | Gravity.BOTTOM, 0, -location[1]);
       
        //修改的话，这个地方要修改 *****************
        //添加按键事件监听
        setBirthdayListeners(timepickerview, dateType);
        //添加pop窗口关闭事件，主要是实现关闭时改变背景的透明度
        popupWindow.setOnDismissListener(new poponDismissListener());
        backgroundAlpha(1f);
    }
	
	//监听日期
	private void setBirthdayListeners(LinearLayout layout, String dateType) {
    	
    	Button bt_select_birthday_ok = (Button) layout.findViewById(R.id.btn_select_birthday_ok);
    	Button bt_select_birthday_cancel = (Button) layout.findViewById(R.id.btn_select_birthday_cancel);
    	
    	ColorStateList whiteColor=getResources().getColorStateList(R.color.popwindowselectok);
    	bt_select_birthday_ok.setTextColor(whiteColor);
    	
    	final String fdateType = dateType;
    	
    	bt_select_birthday_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                    //在此处添加你的按键处理 xxx
                	
                	Log.i("longhua",wheelMain.getTime());
                	
                    popupWindow.dismiss(); 
                    backgroundAlpha(1f);
                    
                    //loading.showLoading();
                    
                    if(fdateType.equals("txtstartdate")){
                    	txtstartdate.setText(wheelMain.getTime());
                    }
                    
                    if(fdateType.equals("txtenddate")){
                    	txtenddate.setText(wheelMain.getTime());
                    }
                    
                    //loading.hideLoading();
                    /*
        			new HhUserModifyPersonInfoYTask(new AbstractYGetTaskListener<User>() {
        				@Override
        				public void onPostExecute(String name, YGetTask<User> task) {
        					
        					userModifyData(task.getValue(),"3");					
        				}
        			},user,"3").execute();
        			*/
                     
                }
            }
        });
    	bt_select_birthday_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                    //在此处添加你的按键处理 xxx
                	 //选照片相 
                	popupWindow.dismiss();
                	backgroundAlpha(1f);
                	
                    return;

                }
            }
        });
        
    	bt_select_birthday_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                    popupWindow.dismiss();
                }
            }
        });
    }
	
	/*
     * 设置添加屏幕的背景透明度
     * @param bgAlpha
     */
    public void backgroundAlpha(float bgAlpha)
    {
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.alpha = bgAlpha; //0.0-1.0
        getWindow().setAttributes(lp);           
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        
       /*
        *    WindowManager.LayoutParams lp = activity.getWindow().getAttributes();  
        lp.alpha = bgAlpha;  
        if (bgAlpha == 1) {  
            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);//不移除该Flag的话,在有视频的页面上的视频会出现黑屏的bug  
        } else {  
            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);//此行代码主要是解决在华为手机上半透明效果无效的bug  
        }  
        activity.getWindow().setAttributes(lp); 
        */
    }
	
	/*
     * 返回或者点击空白位置的时候将背景透明度改回来
     */
    class poponDismissListener implements PopupWindow.OnDismissListener{

        @Override
        public void onDismiss() {
            // TODO Auto-generated method stub
            new Thread(new Runnable(){
                @Override
                public void run() {
                    //此处while的条件alpha不能<= 否则会出现黑屏
                    while(alpha<1f){
                        try {
                            Thread.sleep(4);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Log.d("longhua","alpha:"+alpha);
                        Message msg =mHandler.obtainMessage();
                        msg.what = 1;
                        alpha+=0.01f;
                        msg.obj =alpha ;
                        mHandler.sendMessage(msg);
                    }
                }

            }).start();
        }
    }
    
    Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case 1: 
                	backgroundAlpha(alpha);
                    //backgroundAlpha((float)msg.obj);
                    break;
            }
        }
    };
    
    
    
}
