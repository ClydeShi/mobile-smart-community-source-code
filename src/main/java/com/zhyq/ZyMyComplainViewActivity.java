package com.zhyq;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.aspsine.irecyclerview.IRecyclerView;
import com.aspsine.irecyclerview.OnLoadMoreListener;
import com.aspsine.irecyclerview.OnRefreshListener;
import com.aspsine.irecyclerview.demo.model.Image;
import com.aspsine.irecyclerview.demo.ui.adapter.OnItemClickListener;
import com.aspsine.irecyclerview.demo.ui.widget.footer.LoadMoreFooterView;
import com.zhyq.adapter.baseRecyleViewAdapter;
import com.zhyq.libs.AndroidUtils;
import com.zhyq.libs.BaseActivity;
import com.zhyq.libs.PreferenceUtils;
import com.zhyq.libs.ProgersssDialog;
import com.zhyq.libs.ToastUtils;
import com.zhyq.libs.iosdialog.widget.MyAlertDialog;
import com.zhyq.model.SysPassValue;
import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.sendOrderToServerForListYTask;
import com.zhyq.task.impl.sendOrderToServerForValueYTask;
import com.zhyq.widget.LoadingRelativeLayout;

import java.util.ArrayList;
import java.util.List;

/*
使用实现上拉下拉刷新，原来是继承AppCompatActivity，那样要在AndroidManifest Activity里面使用AppCompat的theme,如android:theme="@style/Theme.AppCompat.Light.NoActionBar"
使用android:theme="@style/Theme.AppCompat.Light.NoActionBar"的话，转圈显示绿色
继承BaseActivity也可以
 */

public class ZyMyComplainViewActivity extends BaseActivity implements OnClickListener, OnItemClickListener<Image>, OnRefreshListener, OnLoadMoreListener {

    private LoadingRelativeLayout loading = null;
    private View includeTitle;
    private View includeData;
    private IRecyclerView iRecyclerView;
    private View headerView = null;
    private View childrenView = null;
    private View cv = null;
    private LoadMoreFooterView loadMoreFooterView;

    private FrameLayout frameLayout=null;

    private baseRecyleViewAdapter mAdapter;

    private int mPage;

    private int currentPage = 1;
    private List<SysPassValue> dataList = new ArrayList<SysPassValue>();//数据

    public ProgersssDialog dialog;

    private long userId = 0;
    private String userCode = "";
    private String userName = "";
    private String userRole = "";

    private String id = "";
    private String name = "";

    private String channelId = "1";
    private String channelName = "";
    private String companyId = "";
    private String itemList = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        showProgressDialog();

        setContentView(R.layout.activity_zy_my_complain_detail);
        loading = LoadingRelativeLayout.getLoadingRelativeLayout(this);
        includeTitle = this.findViewById(R.id.zy_util_head_news_list);
        //includeData = this.findViewById(R.id.xl_huodong_detail_record_pullrefreshrecycleView);

        id = this.getIntent().getStringExtra("id");
        channelId = this.getIntent().getStringExtra("channelId");
        companyId = this.getIntent().getStringExtra("companyId");
        if(channelId==null){
            channelId = "1";
        }
        if(channelId.equals("1")){
            channelName = "保洁申请详情";
            itemList = "item_zy_apply_list";
        }else if(channelId.equals("2")){
            channelName = "维修申请详情";
            itemList = "item_zy_apply_list";
        }else if(channelId.equals("4")){
            channelName = "投诉建议详情";
            itemList = "item_zy_apply_list";
        }
        //Log.i("luktel","id"+id);

        //channelId = this.getIntent().getStringExtra("channelId");
        //Log.i("luktel","channelName"+channelName);

        ((TextView) includeTitle.findViewById(R.id.wg_head_title)).setText(channelName);
        Button btnReturn = (Button) includeTitle.findViewById(R.id.util_head_btn_return);
        btnReturn.setOnClickListener(this);

        ((Button)this.findViewById(R.id.zy_my_clean_cancel)).setOnClickListener(this);
        ((Button)this.findViewById(R.id.zy_my_clean_finish)).setOnClickListener(this);

        //iRecyclerView = (IRecyclerView) includeData.findViewById(R.id.system_irecycleview);
        iRecyclerView = (IRecyclerView) findViewById(R.id.zy_common_detail_recyclerview);
        iRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        //toggleRefreshHeader();//切换上拉刷新面板

        //bannerView = (BannerView) LayoutInflater.from(this).inflate(R.layout.layout_irecyclerview_banner_view, iRecyclerView.getHeaderContainer(), false);
        //iRecyclerView.addHeaderView(bannerView);

        //headerView = View.inflate(this, R.layout.item_wg_event_detail, null);
        //headerView = LayoutInflater.from(this).inflate(R.layout.item_wg_event_detail, null);
        headerView =  LayoutInflater.from(this).inflate(R.layout.activity_zy_my_complain_detail_inlayout, iRecyclerView.getHeaderContainer(), false);//加入面板
        iRecyclerView.addHeaderView(headerView);

        //frameLayout = new FrameLayout(this);
        //iRecyclerView.addHeaderView(frameLayout);

        //解决卡顿问题
        iRecyclerView.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
        iRecyclerView.setFocusable(true);
        iRecyclerView.setFocusableInTouchMode(true);
        iRecyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                view.requestFocusFromTouch();
                return false;
            }
        });

        loadMoreFooterView = (LoadMoreFooterView) iRecyclerView.getLoadMoreFooterView();

        mAdapter = new baseRecyleViewAdapter(this,dataList,"item_imagelist");//通用adapter，如果加入了headerView，自动加载到headerView下面
        //mAdapter.setOnClick1(this);
        iRecyclerView.setIAdapter(mAdapter);

        iRecyclerView.setOnRefreshListener(this);
        iRecyclerView.setOnLoadMoreListener(this);

        //cv = getWindow().getDecorView();

        new Handler().postDelayed(new Runnable() {//延迟读取数据
            @Override
            public void run() {
                getDetailData();//头部信息
                onRefresh();//列表信息代替iRecyclerView.setRefreshing(true);
            }
        }, 0);

        //getDetailData();//头部信息
        //onRefresh();//列表信息代替iRecyclerView.setRefreshing(true);

        //mAdapter.setOnItemClickListener(this);

        /*iRecyclerView.post(new Runnable() {
            @Override
            public void run() {
                iRecyclerView.setRefreshing(true);
            }
        });*/

        userId = PreferenceUtils.getLong(this, "userid", -1);
        userRole = PreferenceUtils.getString(this,"userrole","");
    }

    public void showProgressDialog(){
        dialog = new ProgersssDialog(this);
        //dialog.show();
    }

    @Override
    public void onClick(View view) {

        if (R.id.util_head_btn_return == view.getId()) {
            this.finish();
            overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
            return;
        }

        if (R.id.zy_my_clean_cancel == view.getId()) {
            final MyAlertDialog dialog1 = new MyAlertDialog(ZyMyComplainViewActivity.this)
                    .builder()
                    .setTitle("请确认")
                    .setMsg("确认取消吗?")
                    .setNegativeButton("取消", new OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
            dialog1.setPositiveButton("确认", new OnClickListener() {
                @Override
                public void onClick(View v) {
                    setStatus("-1");
                }
            });
            dialog1.show();
        }

        if (R.id.zy_my_clean_finish == view.getId()) {
            final MyAlertDialog dialog1 = new MyAlertDialog(ZyMyComplainViewActivity.this)
                    .builder()
                    .setTitle("请确认")
                    .setMsg("确认已经完成吗?")
                    .setNegativeButton("取消", new OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
            dialog1.setPositiveButton("确认", new OnClickListener() {
                @Override
                public void onClick(View v) {
                    setStatus("6");
                }
            });
            dialog1.show();
        }

        /*if(R.id.item_zy_newslist_btn==view.getId()){

            //传递参数过去
            Object tag = view.getTag(R.id.button_tag1);

            if (tag != null && tag instanceof Integer) { //解决问题：如何知道你点击的按钮是哪一个列表项中的，通过Tag的position
                //ToastUtils.show(v.getContext(), "22详情  NULL");
                int position = (Integer) tag;
                String ids =  dataList.get(position).getId();
                //Toast.makeText(v.getContext(), "ids"+ids, Toast.LENGTH_SHORT).show();
                //Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
                //return;
                Intent intent = new Intent(view.getContext(), ZyNewsViewActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("id", ids);
                intent.putExtras(bundle);
                startActivity(intent);
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                return;
            }
        }*/
    }

    @Override
    public void onItemClick(int position, Image image, View v) {
        //Toast.makeText(this, String.valueOf(position), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRefresh() {
        //loadBanner();
        loadMoreFooterView.setStatus(LoadMoreFooterView.Status.GONE);
        currentPage = 1;
        getDetailData();
        getListData();
    }

    @Override
    public void onLoadMore() {
        if (loadMoreFooterView.canLoadMore() && mAdapter.getItemCount() > 0) {
            loadMoreFooterView.setStatus(LoadMoreFooterView.Status.LOADING);
            getDetailData();
            getListData();
        }
    }

    public void getDetailData() {
        SysPassValue temp = new SysPassValue();
        if(channelId.equals("1")){
            temp.setFunctionName("getCleanDetail");
        }else if(channelId.equals("2")){
            temp.setFunctionName("getCleanDetail");
        }else if(channelId.equals("4")){
            temp.setFunctionName("getCleanDetail");
        }
        temp.setId(String.valueOf(id));
        temp.setUserId(String.valueOf(userId));

        new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
            @Override
            public void onPostExecute(String name, YGetTask<SysPassValue> task) {
                showData(task.getValue());
                new Handler().postDelayed(new Runnable() {//延迟
                    @Override
                    public void run() {
                        dialog.dismiss();
                    }
                }, 200);
            }
        },temp).execute();
    }

    private void showData(SysPassValue passValue) {
        if (passValue==null) {
            ToastUtils.show(this,"没有获取到信息");
            return;
        } else {
            /*((TextView)includeMaster.findViewById(R.id.tv_masterdetail_head_image_shopname)).setText(masterviewdetail.getArg1());
            AndroidUtils.setWebImageView(includeMaster, R.id.hh_masterdetail_head_image_shoplogimage, masterviewdetail.getArg2());
            AndroidUtils.setWebImageView(includeMaster, R.id.hh_masterdetail_head_image_masterimage, masterviewdetail.getArg3());*/

            String priceShow = "";
            String sid =  passValue.getId();
            String arg1 =  passValue.getArg1();
            String arg2 =  passValue.getArg2();
            String arg3 =  passValue.getArg3();
            String arg4 =  passValue.getArg4();
            String arg5 =  passValue.getArg5();
            String arg6 =  passValue.getArg6();
            String arg7 =  passValue.getArg7();
            String arg8 =  passValue.getArg8();
            String arg9 =  passValue.getArg9();
            String arg10 =  passValue.getArg10();
            String arg11 =  passValue.getArg11();
            String arg12 =  passValue.getArg12();
            String arg13 =  passValue.getArg13();
            String arg14 =  passValue.getArg14();
            String arg15 =  passValue.getArg15();
            String arg16 =  passValue.getArg16();
            String arg17 =  passValue.getArg17();
            String arg18 =  passValue.getArg18();
            String arg19 =  passValue.getArg19();
            String arg20 =  passValue.getArg20();
            String arg21 =  passValue.getArg21();
            String arg22 =  passValue.getArg22();
            String arg23 =  passValue.getArg23();
            String arg24 =  passValue.getArg24();
            String arg25 =  passValue.getArg25();
            String arg26 =  passValue.getArg26();
            String arg27 =  passValue.getArg27();
            String arg28 =  passValue.getArg28();
            String arg29 =  passValue.getArg29();
            String arg30 =  passValue.getArg30();
            String arg38 =  passValue.getArg38();
            String arg39 =  passValue.getArg39();
            String arg40 =  passValue.getArg40();
            String arg41 =  passValue.getArg41();
            String arg42 =  passValue.getArg42();
            String arg43 =  passValue.getArg43();
            String arg44 =  passValue.getArg44();
            String arg45 =  passValue.getArg45();
            String arg46 =  passValue.getArg46();
            String arg50 =  passValue.getArg50();
            //ToastUtils.show(this,"arg38"+arg38);

            if(arg5.equals("1")) {
                arg5 = "未支付";
            }else if(arg5.equals("2")) {
                arg5 = "已支付";
            }

            View zy_relativelayout_my_clean_bottom = this.findViewById(R.id.zy_relativelayout_my_clean_bottom);
            View zy_relativelayout_my_clean_cancel = this.findViewById(R.id.zy_relativelayout_my_clean_cancel);
            View zy_relativelayout_my_clean_finish = this.findViewById(R.id.zy_relativelayout_my_clean_finish);
            if(arg6.equals("-1")) {
                arg6 = "已取消";
                zy_relativelayout_my_clean_bottom.setVisibility(View.GONE);//隐藏 并且不占用界面空间
                zy_relativelayout_my_clean_cancel.setVisibility(View.GONE);
                zy_relativelayout_my_clean_finish.setVisibility(View.GONE);
            }else if(arg6.equals("2")) {
                arg6 = "未回复";
                zy_relativelayout_my_clean_cancel.setVisibility(View.VISIBLE);
                zy_relativelayout_my_clean_finish.setVisibility(View.GONE);
            }else if(arg6.equals("4")) {
                arg6 = "已回复";
                zy_relativelayout_my_clean_bottom.setVisibility(View.GONE);
                zy_relativelayout_my_clean_cancel.setVisibility(View.GONE);
                zy_relativelayout_my_clean_finish.setVisibility(View.GONE);
            }
            if(arg7.equals("Y")) {
                arg7 = "免费";
                priceShow = "免费";
            }else{
                arg7 = "收费";
                priceShow = "￥"+arg41+"/"+arg4+"分钟";
            }

            name = arg1;

            View headInclude = headerView.findViewById(R.id.zy_include_top_picture);

            AndroidUtils.setWebImageView(headInclude, R.id.zy_top_picture, arg38);
            AndroidUtils.setTextView(headerView, R.id.detail_text_sn,arg13);
            AndroidUtils.setTextView(headerView, R.id.detail_text_01,arg1);
            AndroidUtils.setTextView(headerView, R.id.detail_text_02,arg3);
            AndroidUtils.setTextView(headerView, R.id.detail_text_03,arg17);
            AndroidUtils.setTextView(headerView, R.id.detail_text_06,arg44);
            AndroidUtils.setTextView(headerView, R.id.detail_text_08,arg43);
            AndroidUtils.setTextView(headerView, R.id.detail_text_07,arg8);
            AndroidUtils.setTextView(headerView, R.id.detail_text_09,arg6);
            AndroidUtils.setTextView(headerView, R.id.detail_text_10,arg5);

            //((ImageView)includeTitle.findViewById(R.id.iv_hh_head_masterdetail_right_saygoods)).setBackgroundResource(R.drawable.praisetech);
        }
    }

    public void getListData() {
        SysPassValue temp = new SysPassValue();
        temp.setFunctionName("getNull");//getImageList
        temp.setPageNumber(String.valueOf(this.currentPage));
        temp.setArg1("1041");
        temp.setArg2(String.valueOf(id));
        temp.setUserId(String.valueOf(userId));

        new sendOrderToServerForListYTask(new AbstractYGetTaskListener<List<SysPassValue>>() {
            @Override
            public void onPostExecute(String name, YGetTask<List<SysPassValue>> task) {
                iRecyclerView.setRefreshing(false);
                showListData(task.getValue());
                new Handler().postDelayed(new Runnable() {//延迟
                    @Override
                    public void run() {
                        dialog.dismiss();
                    }
                }, 200);

            }
        }, temp).execute();
    }

    public void showListData(final List<SysPassValue> list) {

        //View vnoRecord = this.findViewById(R.id.activity_no_record_showpic);
        if (list == null) {
            /*loadMoreFooterView.setStatus(LoadMoreFooterView.Status.ERROR);*///不用列表数据所以隐藏状态
            //Toast.makeText(this, "网络连接失败", Toast.LENGTH_SHORT).show();

            if (currentPage == 1) {
                this.dataList.clear();
                mAdapter.notifyDataSetChanged();
                //vnoRecord.setVisibility(View.INVISIBLE);
                return;
            }
            return;
        }

        if (list.size() == 0) {
            if (currentPage == 1) {
                this.dataList.clear();
                mAdapter.notifyDataSetChanged();
                //vnoRecord.setVisibility(View.VISIBLE);//如果是第一页没有数据则显示无记录
            } else {
                loadMoreFooterView.setStatus(LoadMoreFooterView.Status.THE_END);
            }
            //Toast.makeText(this, "已经到了最后一页", Toast.LENGTH_SHORT).show();
        } else {
            if (currentPage == 1) {
                currentPage = 2;
                this.dataList.clear();
                this.dataList.addAll(list);
                //vnoRecord.setVisibility(View.GONE);
            } else {

                loadMoreFooterView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        currentPage++;
                        loadMoreFooterView.setStatus(LoadMoreFooterView.Status.GONE);
                        dataList.addAll(list);
                    }
                }, 1000);

                //loadMoreFooterView.setStatus(LoadMoreFooterView.Status.GONE);
                //dataList.addAll(list);
            }
            mAdapter.notifyDataSetChanged();
        }
        //vnoRecord.setVisibility(View.GONE);

        //如果第一页则需要清除数据

        if (dataList.size() == 0) {
            //vnoRecord.setVisibility(View.VISIBLE);
        }

        //Log.d("luktel", "showData");

    }

    public void setStatus(String status){
        loading.showLoading();
        SysPassValue temp = new SysPassValue();
        temp.setFunctionName("setCleanStatus");
        temp.setUserId(String.valueOf(userId));
        temp.setId(this.id);
        temp.setArg1(status);

        new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
            @Override
            public void onPostExecute(String name, YGetTask<SysPassValue> task) {
                loading.hideLoading();
                getResult(task.getValue());
            }
        },temp).execute();
    }

    private void getResult(SysPassValue passValue) {
        if (passValue==null) {
            ToastUtils.show(this, "没有获取到数据！");
        } else {
            String returnResult = passValue.getReturnResult();
            String taskId = passValue.getId();
            if(returnResult.equals("1")){
                ToastUtils.show(this, "操作成功！");
                onRefresh();
                //this.finish();
                //overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
                //return;
            }else{
                ToastUtils.show(this, "操作失败！");
            }
        }
    }


}
