package com.zhyq;
 
import com.zhyq.libs.AndroidUtils;


import com.zhyq.widget.LoadingRelativeLayout;

import android.app.Activity;


import android.os.Bundle;

import android.view.View;
import android.view.View.OnClickListener;

import android.widget.TextView;


public class WgMainActivityTest extends Activity implements OnClickListener {
	public String target=null;
	private LoadingRelativeLayout loading=null;
	
	private View includeTitle;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.setContentView(R.layout.activity_wg_main);
		loading=LoadingRelativeLayout.getLoadingRelativeLayout(this);
		 
		this.findViewById(R.id.wg_main_btn).setOnClickListener(this);
		this.findViewById(R.id.wg_main_btn2).setOnClickListener(this);
		this.findViewById(R.id.wg_main_btn3).setOnClickListener(this);
		this.findViewById(R.id.wg_main_btn4).setOnClickListener(this);
		
	    includeTitle = this.findViewById(R.id.wg_main_head);
		
		((TextView)includeTitle.findViewById(R.id.tv_wg_head_add_title)).setText("案件监督检查系统");
		
		//Button btreturn = (Button)includeTitle.findViewById(R.id.bt_wg_head_right_add);
		//((Button)includeTitle.findViewById(R.id.bt_wg_head_right_add)).setText("曾先进");
		
		reSize();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
	}
	
	
	public void reSize(){
		
		//AndroidUtils.setLinearLayoutSize(includeTitle,R.id.wg_main_head,"LinearLayout","0",640,640,71,0,0,0,0);
		
		//View v1 = this.findViewById(R.id.wg_main_line);
		//AndroidUtils.setLinearLayoutSize(v1,R.id.wg_main_line,"RelativeLayout","0",640,640,1,0,0,0,0);
		
		/*
		int defaultWidth = 640;
		int defaultHeight= 71;
		float scale = 0;
		int webHeight=0;
		
		DisplayMetrics dm = this.getResources().getDisplayMetrics();
		int screenWidth = dm.widthPixels;
		//int webWidth = webImg.getMaxWidth();
		
		View loginHeader = this.findViewById(R.id.wg_main_head);
		
	    scale =  (float) defaultWidth / (float) defaultHeight;
		webHeight=  Math.round(screenWidth / scale);
		
		LinearLayout.LayoutParams params= (LinearLayout.LayoutParams) loginHeader.getLayoutParams();
		 
		params.width  = screenWidth;
		params.height = webHeight;
		loginHeader.setLayoutParams(params);
		*/
	}

	@Override
	public void onClick(View view) {
		
		if (R.id.wg_main_btn2 == view.getId()) {
			 
			AndroidUtils.start(this, WgMainActivity.class);
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
			
		} else if(R.id.wg_main_btn5 == view.getId()){
			 
			   
			AndroidUtils.start(this, WgMainActivityTest.class);
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			return ;
			
			 
		}else if (R.id.wg_main_btn6 == view.getId()){
			
			HhApplication.getInstance(this).getHhCart().setUser(null);
			AndroidUtils.start(this, WgMainActivity.class);
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
			 
		}
		
	}
	
}
