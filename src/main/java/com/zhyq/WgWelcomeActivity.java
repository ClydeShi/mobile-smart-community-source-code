package com.zhyq;

import java.lang.Thread.UncaughtExceptionHandler;

import com.zhyq.libs.AndroidUtils;
import com.zhyq.libs.PreferenceUtils;
import com.zhyq.libs.ToastUtils;
import com.zhyq.model.SysPassValue;
import com.zhyq.widget.LoadingRelativeLayout;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

/*
类名: WgWelcomeActivity 
作用: 在Manifest当中指定了APP的开始运行入口类 
     <activity android:name=".WgWelcomeActivity"  所以说开始运行的最早入口类，是可以变换的
         这里决定进入引导页面，主页还是登录页面
*/

public class WgWelcomeActivity extends Activity {
	
	private LoadingRelativeLayout loading = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.setContentView(R.layout.activity_wg_welcome);
		
		//延迟 2秒 跳转到主界面
		new Handler().postDelayed(new Runnable(){
			@Override
			public void run(){
				//跳转到 主界面  
				gotoNext();
			}
		}, 0);//2000
		Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler() {
			@Override
			public void uncaughtException(Thread arg0, Throwable arg1) {
				Log.e("luktel","uncaughtException",arg1);
			}
			
		});
	}
	
	public void gotoNext() {
		
		//get current version number 
		String firstKey=AndroidUtils.getVersionKey(this,"first_");
		
		//Log.i("longhua",firstKey);
		//d得到的是 first_5.0.1
		
		if (PreferenceUtils.getBoolean(this, firstKey, true)) {
			 //安装好之后第一次启动，打开新手页面
			PreferenceUtils.setBoolean(this, firstKey, false);
			
			//1. 打开新手页面
			//2. 在新手页面当中直接到主页面
			
			//AndroidUtils.start(this, WgGuideActivity.class);
			//Log.i("luktel","goto first1111");
			
			//WgMainActivity，本来安装后第一次先进入主页，主页可以登录，再通过setResult返回主页，现在是必须先登录，然后用AndroidUtils.start(this, WgMainActivity.class)打开主页
			AndroidUtils.start(this, WgLoginActivity.class);
			this.finish();
			return;
		} else {
			
			//得到上次登入的账号,如果有，直接默认登入，如果没有，则直接是guest登入
			
			long userId = 0l;
			userId = PreferenceUtils.getLong(this, "userid", -1);
			
			//String userId = PreferenceUtils.getString(this,"userid","");
			
			if (userId==-1) {
			//if (userId.equals("")) {
				//直接用guest的身份登入
				//PreferenceUtils.setLong(this, "userId", -1);
				
				/*
				AndroidUtils.start(this, WgMainActivity.class);
				this.finish();
				return;
				*/
				
				AndroidUtils.start(this, WgLoginActivity.class);//先登录，然后进入主页
				this.finish();
				return;
				
			} else {
				/*
				 * 读取登入信息 这里没有传密码过去，登录不了，可以只传用户名，然后更新登录时间
				 */
				
				AndroidUtils.start(this, WgMainActivity.class);
				this.finish();
				return;
				
				/*
				SysPassValue temp = new SysPassValue();
				temp.setFunctionName("loginById");
				temp.setUserId(String.valueOf(userId));
				
				new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
					@Override
					public void onPostExecute(String name, YGetTask<SysPassValue> task) {
						//loading.hideLoading();
						logined(task.getValue());
					}
				},temp).execute();
				*/
			}
		}
	}
	
	private void logined(SysPassValue passValue) {
		if (passValue==null) {
			ToastUtils.show(this, R.string.show_logon_error_msg);
		} else {
			//Log.i("luktel","onClick3");
			String returnResult = passValue.getReturnResult();
			if(returnResult.equals("1")){
				AndroidUtils.start(this, WgMainActivity.class);
				this.finish();
				return;
			}
			
			/*
			HhApplication.getInstance(this).getHhCart().setUser(user);
			AndroidUtils.start(this, WgMainActivity.class);
			//overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			this.finish();
			//overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			*/
		}
	}
	
}
