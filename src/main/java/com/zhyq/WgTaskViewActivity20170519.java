package com.zhyq;


import android.content.Intent;
import android.os.Bundle;

import android.support.v4.app.FragmentActivity;

import android.view.View;
import android.view.View.OnClickListener; 
import android.widget.Button;
import android.widget.ImageView; 
import android.widget.TextView;


import com.zhyq.libs.AndroidUtils;
 
import com.zhyq.libs.ToastUtils;
import com.zhyq.libs.iosdialog.widget.MyAlertDialog;
import com.zhyq.model.CommonData;
import com.zhyq.model.SysPassValue;
import com.zhyq.model.User;
 
import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.GetTaskDetailYTask;
import com.zhyq.task.impl.sendOrderToServerForValueYTask;
import com.zhyq.widget.LoadingRelativeLayout;



public class WgTaskViewActivity20170519 extends FragmentActivity  implements OnClickListener  { 
	
	private LoadingRelativeLayout loading=null;
	private View includeTitle;
	private View includeMaster;
	private ImageView returnImage;
	private TextView returnText;
	 
	
	private String curTitle = "全部";
	private int cur_position = 0;
	private String taskId = "";
	private String taskName = "";
	private String classType = "";
	private Long userId = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_wg_task_detail);
		loading=LoadingRelativeLayout.getLoadingRelativeLayout(this);
		loading.showLoading();
		includeTitle = this.findViewById(R.id.wg_taskdetail_head);
		((TextView)includeTitle.findViewById(R.id.wg_head_add_title)).setText("查看任务");
		Button btreturn = (Button)includeTitle.findViewById(R.id.wg_head_add_btn_return);
		btreturn.setOnClickListener(this);
		
		((TextView)includeTitle.findViewById(R.id.wg_util_head_text_add)).setText("反馈");
		Button btadd = (Button)includeTitle.findViewById(R.id.wg_util_head_btn_add);
		btadd.setOnClickListener(this);
		
		if(HhApplication.getInstance(this).getHhCart().getUser()==null){
			userId =-1l;
		}else{
			userId = HhApplication.getInstance(this).getHhCart().getUser().getId();
		}
		
		taskId = this.getIntent().getStringExtra("taskId");
		classType = this.getIntent().getStringExtra("classType");
		String taskName = this.getIntent().getStringExtra("taskName");
		AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout2_textview,taskName);
		
		if(classType.equals("未执行")){
			((TextView)includeTitle.findViewById(R.id.wg_util_head_text_add)).setText("执行");
		}
		
		if(classType.equals("已完成")){
			((TextView)includeTitle.findViewById(R.id.wg_util_head_text_add)).setText("");
		}
		
		//输出日志，设置log tag，手机设为调试模式，在logcat里面就可以看到了
		//Log.i("longhua","taskId"+taskId);
		
		/*
		masterid = this.getIntent().getStringExtra("masterid");
		String masterlevel = this.getIntent().getStringExtra("masterlevel");
		mastername = this.getIntent().getStringExtra("mastername");
		String masterexperience = this.getIntent().getStringExtra("masterexperience");
		String masterworktimes = this.getIntent().getStringExtra("masterworktimes");
		ihasdianzhan = this.getIntent().getStringExtra("ihasdianzhan");
		masterdianzhanqty = this.getIntent().getStringExtra("masterdianzhanqty");
		
		
		
		//我是否点赞
		if(ihasdianzhan.equals("Y")){
			//设置手为绿色
			((ImageView)includeTitle.findViewById(R.id.iv_hh_head_masterdetail_right_saygoods)).setBackgroundResource(R.drawable.praisetech); 
		}else{
			
			((ImageView)includeTitle.findViewById(R.id.iv_hh_head_masterdetail_right_saygoods)).setBackgroundResource(R.drawable.praise_black_empty);
		}
		//点赞数量
	    ((TextView)includeTitle.findViewById(R.id.tv_hh_head_right_masterdetail_qty)).setText(masterdianzhanqty);
		
	    ((TextView)includeMaster.findViewById(R.id.tv_masterdetail_head_image_mastername)).setText(mastername);
	    ((TextView)includeMaster.findViewById(R.id.masterdetail_head_image_master_level)).setText(masterlevel);
	    ((TextView)includeMaster.findViewById(R.id.masterdetail_head_image_master_experience)).setText(masterexperience);
	    ((TextView)includeMaster.findViewById(R.id.masterdetail_head_image_master_servertimes)).setText(masterworktimes+"次服务");
	    */
	    
	    
		/*
		 *
		 1. 调用com.unitesoft.task.impl.GetTaskDetailYTask，并传入taskId参数，GetTaskDetailYTask会调用com.unitesoft.data.DataAPI.getTaskDetail()
		 2. DataAPI的方法getTaskDetail会调用DemoDataInterface的方法getTaskDetail返回结果，因为DemoDataInterface是实现DataInterface接口，所以DataInterface也要定义方法getTaskDetail
		 3. 如果是调用远程数据，上面的DataAPI的方法getTaskDetail会调用ClientDataInterface，ClientDataInterface会带参数连接服务器，com.huahua.server.servelt.ClientApiServlet，ClientApiServlet通过判断参数，传入对象，分别调用com.huahua.server.data.DemoDataInterface里的方法，并返回对象
		 4. 使用SysPassValue来作为数据对象传递
		 */
		 
		 /*
		  * 
		  * GetTaskDetailYTask继承YGetTask，调用接口的时候执行构造函数GetTaskDetailYTask，传入YTaskListener和taskId参数
		  * 父类YGetTask会执行onExecute方法，方法里面有_get()，会通过DataAPI.getTaskDetail();返回结果
		  */
	    
	    
	    new GetTaskDetailYTask(new AbstractYGetTaskListener<CommonData>() {
			@Override
			public void onPostExecute(String name, YGetTask<CommonData> task) {//onPostExecute是AbstractYGetTaskListener里的方法
				
				getData(task.getValue());//task.getValue()是SysPassValue定义的List，这个是返回的结果
				loading.hideLoading();
			}
		},taskId,String.valueOf(this.userId)).execute();
		
        
        /*reSize();*/
        
        
	}
	
	//onResume是在启动activity启动之后才能执行的，也就是恢复执行。程序正常启动：onCreate()->onStart()->onResume();
	@Override
	protected void onResume() {//该函数，如果在打开了其他窗口，再关闭，这个函数也会执行
		// TODO Auto-generated method stub
		super.onResume();
		//Log.i("huahua","taskId = "+taskId);
	}
	
	@Override
	public void onClick(View view) {
		
		if(R.id.wg_head_add_btn_return==view.getId()){
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		}
		
		if(R.id.wg_util_head_btn_add==view.getId()){
			
			if(classType.equals("未执行")){
				final MyAlertDialog dialog1 = new MyAlertDialog(WgTaskViewActivity20170519.this)
				.builder()
				.setTitle("执行")
				.setMsg("执行当前任务，确定吗?")
				.setNegativeButton("取消", new OnClickListener() {
					@Override
					public void onClick(View v) {
						
					}
				});
				dialog1.setPositiveButton("确认", new OnClickListener() {
					@Override
					public void onClick(View v) {
						doTask();
					}
				});
				dialog1.show();
				
			}else if(classType.equals("执行中")){
				Intent intent=new Intent(this, WgTaskFeedback.class);
				Bundle bundle=new Bundle();
				Long userid = null;
				User curuser = new User();
				curuser = HhApplication.getInstance(this).getHhCart().getUser();
				if(curuser==null){
					userid =-1l;
				}else{
					userid = HhApplication.getInstance(this).getHhCart().getUser().getId();
				}
				
				bundle.putString("userId", String.valueOf(userid));
				bundle.putString("taskId", String.valueOf(taskId));
				bundle.putString("taskName", taskName);
				intent.putExtras(bundle);
				//startActivityForResult(intent, 1);//这里采用startActivityForResult来做跳转，此处的0为一个依据，可以写其他的值，但一定要>=0，用于开启新Activity，新Activity退出后，返回结果给旧Activity
				startActivity(intent);
				this.finish();
				overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			}else if(classType.equals("已完成")){
				
			}
			
			
		}
		
		return;
	}
	
	private void getData(CommonData taskdetail) {
		if (taskdetail==null) {
			ToastUtils.show(this,"没有获取到信息");
			return;
		} else {
			 /*((TextView)includeMaster.findViewById(R.id.tv_masterdetail_head_image_shopname)).setText(masterviewdetail.getArg1());  
			 AndroidUtils.setWebImageView(includeMaster, R.id.hh_masterdetail_head_image_shoplogimage, masterviewdetail.getArg2()); 
			 AndroidUtils.setWebImageView(includeMaster, R.id.hh_masterdetail_head_image_masterimage, masterviewdetail.getArg3());*/
			 
			String varValue1 = taskdetail.getVarValue1();
			String varValue2 = taskdetail.getVarValue2();
			String varValue3 = taskdetail.getVarValue3();
			String varValue4 = taskdetail.getVarValue4();
			String varValue5 = taskdetail.getVarValue5();
			String varValue6 = taskdetail.getVarValue6();
			String varValue7 = taskdetail.getVarValue7();
			String varValue8 = taskdetail.getVarValue8();
			String varValue9 = taskdetail.getVarValue9();
			String varValue10 = taskdetail.getVarValue10();
			
			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout2_textview,varValue2);
			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout3_textview,varValue3);
			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout4_textview,varValue4);
			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout5_textview,varValue5);
			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout6_textview,varValue6);
			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout7_textview,varValue7);
			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout8_textview,varValue8);
			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout9_textview,varValue9);
			AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout10_textview,varValue10);
			
			//((ImageView)includeTitle.findViewById(R.id.iv_hh_head_masterdetail_right_saygoods)).setBackgroundResource(R.drawable.praisetech);
			 
		}
	}
	
	public void doTask(){
		
		SysPassValue temp = new SysPassValue();
		temp.setFunctionName("startTask");
		temp.setUserId(String.valueOf(userId));
		temp.setId(this.taskId);
		temp.setArg1("5");
		
		new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
			@Override
			public void onPostExecute(String name, YGetTask<SysPassValue> task) {
				//loading.hideLoading();
				getResult(task.getValue());
			}
		},temp).execute();
	}
	
	private void getResult(SysPassValue passValue) {
		if (passValue==null) {
			ToastUtils.show(this, "没有获取到数据！");
		} else {
			String returnResult = passValue.getReturnResult();
			String taskId = passValue.getId();
			if(returnResult.equals("1")){
				
				this.finish();
				overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
				return;
			}else{
				ToastUtils.show(this, "提交执行失败！");
				loading.hideLoading();
			}
		}
	}
	
	/*
	private void rightdianZhan(Userdianzhan objdianzhan) {
	 
		if (objdianzhan==null) {
			ToastUtils.show(this,"点赞失败");
		} else { 
			 String newDianZhanQty =String.valueOf(objdianzhan.getdzqty());
			 String dzmark = objdianzhan.getdoaction(); 
				
			((TextView)includeTitle.findViewById(R.id.tv_hh_head_right_masterdetail_qty)).setText(newDianZhanQty);
			
			//Log.i("huahua",dzmark);
			
			if(dzmark.equals("Y")){
				ihasdianzhan="Y";
				//设置手为 绿色
				//设置背景图片为 background
				((ImageView)includeTitle.findViewById(R.id.iv_hh_head_masterdetail_right_saygoods)).setBackgroundResource(R.drawable.praisetech);
			}else{
				ihasdianzhan="C";
				((ImageView)includeTitle.findViewById(R.id.iv_hh_head_masterdetail_right_saygoods)).setBackgroundResource(R.drawable.praise_black_empty);
			}
			  
		}
	}
	*/	 
		

}
