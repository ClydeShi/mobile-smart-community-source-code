package com.zhyq.widget;

import com.zhyq.libs.AndroidUtils;
 
import com.zhyq.listener.OnFliperShowListener;

import com.zhyq.R;  //不是 android.R 否则找不到  自定义颜色值
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ViewFlipper;

public class FliperAgent implements OnClickListener {
	private OnFliperShowListener l=null;
	private View parent=null;
	private int fliperId=0;
	private int[] btnIds=null;
	private int[] textIds=null;
	private int[] lineIds=null;
	public FliperAgent(View parent, int fliperId, int[] btnIds, int[] textIds,
			int[] lineIds) {
		super();
		this.parent = parent;
		this.fliperId = fliperId;
		this.btnIds = btnIds;
		this.textIds = textIds;
		this.lineIds = lineIds;
		init();
	}
	
	private void init() {
		for (int btnId:btnIds) {
			View btn=parent.findViewById(btnId);
			btn.setClickable(true);
			btn.setOnClickListener(this);
		}
	}

	public void setOnFliperShowListener(OnFliperShowListener l) {
		this.l = l;
	}

	@Override
	public void onClick(View view) {
		int index=-1;
		for (int i=0;i<btnIds.length;i++) {
			if (btnIds[i]==view.getId()) {
				index=i;
				break;
			}
		}
		if (index!=-1) {
			setIndex(index);
			 
		}
	}
		
	public void setIndex(int index) {
		ViewFlipper vf=(ViewFlipper)parent.findViewById(fliperId);
		vf.setDisplayedChild(index);
		for (int lineId:lineIds) {
			parent.findViewById(lineId).setVisibility(View.INVISIBLE);
		}
		parent.findViewById(lineIds[index]).setVisibility(View.VISIBLE);
		for (int textId:textIds) {
			//因为R.color.pub_color_four 不能使用
			 AndroidUtils.setTextViewColor(parent, textId, R.color.pub_color_four);
			
		}
		 AndroidUtils.setTextViewColor(parent, textIds[index],R.color.pub_color_four);
		if (l!=null) {
			l.fliperShow(index);
		}
	}
}
