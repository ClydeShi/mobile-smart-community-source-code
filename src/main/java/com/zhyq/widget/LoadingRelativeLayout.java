package com.zhyq.widget;

import com.zhyq.libs.AndroidUtils;

import com.zhyq.*;
import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

public class LoadingRelativeLayout extends RelativeLayout {
	
	public static LoadingRelativeLayout getLoadingRelativeLayout(Activity activity) {
		return (LoadingRelativeLayout)activity.findViewById(R.id.RelativeLayout_loading);//relativelayout_loading_view.xml
	}
	public static LoadingRelativeLayout getLoadingRelativeLayout(View view) {
		return (LoadingRelativeLayout)view.findViewById(R.id.RelativeLayout_loading);
	}

	public LoadingRelativeLayout(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
	}

	public LoadingRelativeLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public LoadingRelativeLayout(Context context) {
		super(context);
	}
	
	private int showCount=7;
	public void showLoading() {
		showLoading(null,1);
	}
	public void showLoading(int showCount) {
		showLoading(null,showCount);
	}
	public void showLoading(String message) {
		showLoading(message,1);
	}
	public void showLoading(String message,int showCount) {
		this.showCount=showCount;
		if (message!=null) {
			AndroidUtils.setTextView(this, R.id.textView_name, message);
		}
		this.setVisibility(View.VISIBLE);
	}
	public void hideLoading() {
		
		 
		 
		this.showCount--;
		if (this.showCount<=0) {
			this.setVisibility(View.GONE);
		}
		 
	}
}
