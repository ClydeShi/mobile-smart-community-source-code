package com.zhyq;


import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.os.Handler;

import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;


import com.zhyq.adapter.WgTaskFeedbackDetailAdapter;
import com.zhyq.libs.CustomProgressDialog;
 
import com.zhyq.libs.ToastUtils;
import com.zhyq.model.CommonData;
import com.zhyq.model.SysPassValue;
import com.zhyq.model.User;
 
import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.GetTaskDetailYTask;
import com.zhyq.task.impl.sendOrderToServerForListYTask;
import com.zhyq.widget.LoadingRelativeLayout;

/*
 * 使用RecyclerView实现，增加下拉刷新
 */

public class WgFeedbackViewActivity extends FragmentActivity implements OnClickListener {
	
	private LoadingRelativeLayout loading = null;
	private View includeTitle;
	private SwipeRefreshLayout demo_swiperefreshlayout;
	private String taskId = "";
	private Long userId = null;
	private RecyclerView mRecyclerView;
    private WgTaskFeedbackDetailAdapter adapter;
    LinearLayoutManager layoutManager;
    private CommonData dataDetail = new CommonData();
    private List<SysPassValue> dataList = new ArrayList<SysPassValue>();
	
	public CustomProgressDialog dialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//showMyDialog();
		
		setContentView(R.layout.activity_wg_feedback_detail);
		loading=LoadingRelativeLayout.getLoadingRelativeLayout(this);
		loading.showLoading();
		includeTitle = this.findViewById(R.id.wg_feedbackdetail_head);
		
		((TextView)includeTitle.findViewById(R.id.wg_head_title)).setText("查看反馈");
		
		Button btreturn = (Button)includeTitle.findViewById(R.id.wg_head_btn_return);
		btreturn.setOnClickListener(this);
		
		if(HhApplication.getInstance(this).getHhCart().getUser()==null){
			userId =-1l;
		}else{
			userId = HhApplication.getInstance(this).getHhCart().getUser().getId();
		}
		
		taskId = this.getIntent().getStringExtra("taskId");
		String taskName = this.getIntent().getStringExtra("taskName");
		
		demo_swiperefreshlayout=(SwipeRefreshLayout)this.findViewById(R.id.demo_swiperefreshlayout);
		mRecyclerView=(RecyclerView)findViewById(R.id.rv_feedbacklist);
		
		//设置刷新时动画的颜色，可以设置4个
        demo_swiperefreshlayout.setProgressBackgroundColorSchemeResource(android.R.color.white);
		//demo_swiperefreshlayout.setProgressBackgroundColor(android.R.color.white);
        demo_swiperefreshlayout.setColorSchemeResources(android.R.color.holo_blue_light,
                android.R.color.holo_red_light, android.R.color.holo_orange_light,
                android.R.color.holo_green_light);
        demo_swiperefreshlayout.setProgressViewOffset(false, 0, (int) TypedValue
                .applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, getResources()
                        .getDisplayMetrics()));
        
        layoutManager=new LinearLayoutManager(this);//List布局
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager);
        adapter=new WgTaskFeedbackDetailAdapter(this, dataDetail, dataList);
        mRecyclerView.setAdapter(adapter);
		
		getDetailData();
	    getListData();
	    
	    demo_swiperefreshlayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
            	//Log.d("longhua", "invoke onRefresh...");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                    	
                    	getDetailData();
                	    getListData();
                        demo_swiperefreshlayout.setRefreshing(false);
                    }
                }, 100);
            }
        });
	    
	    loading.hideLoading();
        
	}
	
	private void getDetailData(){
		new GetTaskDetailYTask(new AbstractYGetTaskListener<CommonData>() {
			@Override
			public void onPostExecute(String name, YGetTask<CommonData> task) {//onPostExecute是AbstractYGetTaskListener里的方法
				
				getData(task.getValue());//task.getValue()是SysPassValue定义的List，这个是返回的结果
			}
		},taskId,String.valueOf(this.userId)).execute();
	}
	
	public void getListData(){
		Long userid = null;
		User curuser = new User();
		curuser = HhApplication.getInstance(this).getHhCart().getUser();
		if(curuser==null){
			userid =-1l;
		}else{
			userid = HhApplication.getInstance(this).getHhCart().getUser().getId();
		}
		
		SysPassValue temp = new SysPassValue();
		temp.setFunctionName("getTaskFeedbackList");
		temp.setPageNumber("1");
		temp.setId(taskId);
		temp.setArg1(String.valueOf(userid));
		
		new sendOrderToServerForListYTask(new AbstractYGetTaskListener<List<SysPassValue>>() {
			@Override
			public void onPostExecute(String name, YGetTask<List<SysPassValue>> task) {
				
				showData(task.getValue());
			}
		},temp).execute();
    }
	
	public void showData(List<SysPassValue> list){
		
		if (list==null){
			this.dataList.clear();
			adapter.notifyDataSetChanged();
			return;
		}
		
		this.dataList.clear();
		dataList.addAll(list);
		adapter.notifyDataSetChanged();
		
	}
	
	//onResume是在启动activity启动之后才能执行的，也就是恢复执行。程序正常启动：onCreate()->onStart()->onResume();
	@Override
	protected void onResume() {
		super.onResume();
	}
	
	public void showMyDialog(){
		dialog = new CustomProgressDialog(this);
		dialog.show();
		/*WindowManager windowManager = getWindowManager();
		Display display = windowManager.getDefaultDisplay();
		WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
		lp.width = (int)(display.getWidth()); //设置宽度
		dialog.getWindow().setAttributes(lp);*/
	}
	
	@Override
	public void onClick(View view) {
		
		if(R.id.wg_head_btn_return==view.getId()){
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		}
		
		return;
	}
	
	private void getData(CommonData taskdetail) {
		if (taskdetail==null) {
			ToastUtils.show(this,"没有获取到信息");
			dataDetail = null;
			adapter.notifyDataSetChanged();
			return;
		} else {
			
			//dataDetail = taskdetail;
			adapter.data = taskdetail;
			adapter.notifyDataSetChanged();
			
		}
	}
		
		
		
}
