package com.zhyq;


import android.os.Bundle;

import android.support.v4.app.FragmentActivity;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;


import com.zhyq.libs.AndroidUtils;
import com.zhyq.libs.CustomProgressDialog;
 
import com.zhyq.libs.ToastUtils;
import com.zhyq.model.CommonData;

import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.GetTaskDetailYTask;
import com.zhyq.widget.LoadingRelativeLayout;

//详情  

public class WgFeedbackViewActivity20170519 extends FragmentActivity  implements OnClickListener  { 
	
	private LoadingRelativeLayout loading = null;
	private View includeTitle;
	private String taskId = "";
	private Long userId = null;
	
	public CustomProgressDialog dialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//showMyDialog();
		
		setContentView(R.layout.activity_wg_feedback_detail);
		loading=LoadingRelativeLayout.getLoadingRelativeLayout(this);
		loading.showLoading();
		includeTitle = this.findViewById(R.id.wg_feedbackdetail_head);
		
		((TextView)includeTitle.findViewById(R.id.wg_head_title)).setText("查看反馈");
		
		Button btreturn = (Button)includeTitle.findViewById(R.id.wg_head_btn_return);
		btreturn.setOnClickListener(this);
		
		if(HhApplication.getInstance(this).getHhCart().getUser()==null){
			userId =-1l;
		}else{
			userId = HhApplication.getInstance(this).getHhCart().getUser().getId();
		}
		
		taskId = this.getIntent().getStringExtra("taskId");
		String taskName = this.getIntent().getStringExtra("taskName");
		AndroidUtils.setTextView(this, R.id.wg_feedbackdetail_layout2_textview,taskName);
		
		readData();
        
	}
	
	//onResume是在启动activity启动之后才能执行的，也就是恢复执行。程序正常启动：onCreate()->onStart()->onResume();
	@Override
	protected void onResume() {
		super.onResume();
	}
	
	public void showMyDialog(){
		dialog = new CustomProgressDialog(this);
		dialog.show();
		/*WindowManager windowManager = getWindowManager();
		Display display = windowManager.getDefaultDisplay();
		WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
		lp.width = (int)(display.getWidth()); //设置宽度
		dialog.getWindow().setAttributes(lp);*/
	}
	
	@Override
	public void onClick(View view) {
		
		if(R.id.wg_head_btn_return==view.getId()){
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		}
		
		return;
	}
	
	private void readData(){
		new GetTaskDetailYTask(new AbstractYGetTaskListener<CommonData>() {
			@Override
			public void onPostExecute(String name, YGetTask<CommonData> task) {
				
				getData(task.getValue());
				loading.hideLoading();
			}
		},taskId,String.valueOf(this.userId)).execute();
	}
	
	private void getData(CommonData taskdetail) {
		if (taskdetail==null) {
			ToastUtils.show(this,"没有获取到信息");
			return;
		} else {
			 /*((TextView)includeMaster.findViewById(R.id.tv_masterdetail_head_image_shopname)).setText(masterviewdetail.getArg1());  
			 AndroidUtils.setWebImageView(includeMaster, R.id.hh_masterdetail_head_image_shoplogimage, masterviewdetail.getArg2()); 
			 AndroidUtils.setWebImageView(includeMaster, R.id.hh_masterdetail_head_image_masterimage, masterviewdetail.getArg3());*/
			 
			
			String varValue2 = taskdetail.getVarValue2();
			String varValue3 = taskdetail.getVarValue3();
			String varValue4 = taskdetail.getVarValue4();
			String varValue5 = taskdetail.getVarValue5();
			String varValue6 = taskdetail.getVarValue6();
			String varValue7 = taskdetail.getVarValue7();
			String varValue8 = taskdetail.getVarValue8();
			String varValue9 = taskdetail.getVarValue9();
			String varValue10 = taskdetail.getVarValue10();
			String varValue21 = taskdetail.getVarValue21();
			String varValue22 = taskdetail.getVarValue22();
			String varValue23 = taskdetail.getVarValue23();
			String varValue24 = taskdetail.getVarValue24();
			String varValue25 = taskdetail.getVarValue25();
			String varValue26 = taskdetail.getVarValue26();
			String varValue27 = taskdetail.getVarValue27();
			String varValue28 = taskdetail.getVarValue28();
			String varValue29 = taskdetail.getVarValue29();
			String varValue30 = taskdetail.getVarValue30();
			
			AndroidUtils.setTextView(this, R.id.wg_feedbackdetail_layout2_textview,varValue2);
			AndroidUtils.setTextView(this, R.id.wg_feedbackdetail_layout3_textview,varValue3);
			AndroidUtils.setTextView(this, R.id.wg_feedbackdetail_layout4_textview,varValue4);
			AndroidUtils.setTextView(this, R.id.wg_feedbackdetail_layout5_textview,varValue5);
			AndroidUtils.setTextView(this, R.id.wg_feedbackdetail_layout6_textview,varValue6);
			AndroidUtils.setTextView(this, R.id.wg_feedbackdetail_layout7_textview,varValue7);
			AndroidUtils.setTextView(this, R.id.wg_feedbackdetail_layout8_textview,varValue8);
			AndroidUtils.setTextView(this, R.id.wg_feedbackdetail_layout9_textview,varValue9);
			AndroidUtils.setTextView(this, R.id.wg_feedbackdetail_layout10_textview,varValue10);
			AndroidUtils.setTextView(this, R.id.wg_feedbackdetail_layout11_textview,varValue21);
			AndroidUtils.setTextView(this, R.id.wg_feedbackdetail_layout12_textview,varValue22);
			AndroidUtils.setTextView(this, R.id.wg_feedbackdetail_layout13_textview,varValue23);
			AndroidUtils.setTextView(this, R.id.wg_feedbackdetail_layout14_textview,varValue24);
			AndroidUtils.setTextView(this, R.id.wg_feedbackdetail_layout15_textview,varValue25);
			AndroidUtils.setTextView(this, R.id.wg_feedbackdetail_layout16_textview,varValue26);
			AndroidUtils.setTextView(this, R.id.wg_feedbackdetail_layout17_textview,varValue27);
			AndroidUtils.setTextView(this, R.id.wg_feedbackdetail_layout18_textview,varValue28);
			AndroidUtils.setTextView(this, R.id.wg_feedbackdetail_layout19_textview,varValue29);
			AndroidUtils.setTextView(this, R.id.wg_feedbackdetail_layout20_textview,varValue30);
			
			/*
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					dialog.hide();
				}
			}, 1000);
			*/
			
			//((ImageView)includeTitle.findViewById(R.id.iv_hh_head_masterdetail_right_saygoods)).setBackgroundResource(R.drawable.praisetech);
			 
		}
	}
		
		
		
}
