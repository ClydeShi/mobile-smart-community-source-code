package com.zhyq;


import android.app.Activity;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;

import com.zhyq.libs.AndroidUtils;
import com.zhyq.libs.BarChartViewNew;
import com.zhyq.libs.PieChartView;
import com.zhyq.libs.ToastUtils;
import com.zhyq.libs.view.wheelview.JudgeDate;
import com.zhyq.libs.view.wheelview.ScreenInfo;
import com.zhyq.libs.view.wheelview.WheelMain;
import com.zhyq.model.SysPassValue;
import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.sendOrderToServerForListYTask;
import com.zhyq.widget.LoadingRelativeLayout;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class WgChartZongHeFenXiActivity extends Activity implements OnClickListener {

	private LoadingRelativeLayout loading = null;
	private View includeTitle;

	private PopupWindow popupWindow;
	private float alpha = 1f;

	WheelMain wheelMain;
	TextView txtstartdate;
	TextView txtenddate;
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	//PieChartView pcv;
	BarChartViewNew br;
	BarChartViewNew br_1;
	BarChartViewNew br4;
	BarChartViewNew br5;

	private String taskType = "getToday";

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_wg_chart_zonghefenxi);
		loading = LoadingRelativeLayout.getLoadingRelativeLayout(this);
		includeTitle = this.findViewById(R.id.wg_chart_head);
		Button btreturn = (Button)includeTitle.findViewById(R.id.wg_head_btn_return);
		btreturn.setOnClickListener(this);
		((TextView)includeTitle.findViewById(R.id.wg_head_title)).setText("数据分析");

		txtstartdate = ((TextView)this.findViewById(R.id.wg_event_date_start));
		(this.findViewById(R.id.btn_start_date)).setOnClickListener(this);

		txtenddate = ((TextView)this.findViewById(R.id.wg_event_date_end));
		(this.findViewById(R.id.btn_end_date)).setOnClickListener(this);

		this.findViewById(R.id.wg_btn_day).setOnClickListener(this);
		this.findViewById(R.id.wg_btn_week).setOnClickListener(this);
		this.findViewById(R.id.wg_btn_month).setOnClickListener(this);
		this.findViewById(R.id.wg_btn_season).setOnClickListener(this);
		this.findViewById(R.id.wg_btn_year).setOnClickListener(this);
		this.findViewById(R.id.wg_btn_date).setOnClickListener(this);

		this.findViewById(R.id.wg_chart_btn).setOnClickListener(this);
		this.findViewById(R.id.wg_chart_btn3).setOnClickListener(this);

		/*pcv = (PieChartView)findViewById(R.id.pie);*/

        br = (BarChartViewNew)findViewById(R.id.bar);
		br_1 = (BarChartViewNew)findViewById(R.id.bar_1);
		br4 = (BarChartViewNew)findViewById(R.id.bar4);
		br5 = (BarChartViewNew)findViewById(R.id.bar5);

        loading.showLoading();
        GetData(taskType);
	}

	public void GetData(String task){

		String startdate = txtstartdate.getText().toString();
		String enddate = txtenddate.getText().toString();

		SysPassValue temp = new SysPassValue();
		temp.setFunctionName("getCaseNumber");
		temp.setArg1(task);
		temp.setArg2(startdate);
		temp.setArg3(enddate);

		new sendOrderToServerForListYTask(new AbstractYGetTaskListener<List<SysPassValue>>() {//获取检查人员数量
			@Override
			public void onPostExecute(String name, YGetTask<List<SysPassValue>> task) {
				showCaseNumber(task.getValue());
				loading.hideLoading();
			}
		},temp).execute();

		SysPassValue temp2 = new SysPassValue();
		temp2.setFunctionName("getWgReportZongHeData2");
		temp2.setArg1(task);
		temp2.setArg2(startdate);
		temp2.setArg3(enddate);

		new sendOrderToServerForListYTask(new AbstractYGetTaskListener<List<SysPassValue>>() {//获取检查案件数量
			@Override
			public void onPostExecute(String name, YGetTask<List<SysPassValue>> task) {
				showEventData(task.getValue());
				loading.hideLoading();
			}
		},temp2).execute();

		SysPassValue temp3 = new SysPassValue();
		temp3.setFunctionName("getWgReportZongHeData3");
		temp3.setArg1(task);
		temp3.setArg2(startdate);
		temp3.setArg3(enddate);

		new sendOrderToServerForListYTask(new AbstractYGetTaskListener<List<SysPassValue>>() {//获取检查案件数量
			@Override
			public void onPostExecute(String name, YGetTask<List<SysPassValue>> task) {
				showDeviceData(task.getValue());
				loading.hideLoading();
			}
		},temp3).execute();
	}

	public void showCaseNumber(List<SysPassValue> list){

		float total = 0, data1 = 0, data2 = 0;
		if(list!=null){
			if(list.size()==2){
				data1 = Float.valueOf(list.get(0).getReturnResult());
				data2 = Float.valueOf(list.get(1).getReturnResult());

				/*data1 = 1278;
				data2 = 2115;*/
				//data3 = Float.valueOf(list.get(2).getReturnResult());
				//data4 = Float.valueOf(list.get(3).getReturnResult());
				//data5 = Float.valueOf(list.get(4).getReturnResult());
				//data6 = Float.valueOf(list.get(5).getReturnResult());
			}
		}

		total = data1 + data2;
		AndroidUtils.setTextView(this, R.id.wg_report_textview_totalcheckperson,"案件总量"+(int)total+"起");
		AndroidUtils.setTextView(this, R.id.wg_report_person_textview1,"事件量"+(int)data1+"起");
		AndroidUtils.setTextView(this, R.id.wg_report_person_textview2,"部件量"+(int)data2+"起");
		//AndroidUtils.setTextView(this, R.id.wg_report_person_textview3,"观湖"+(int)data3+"人");
		//AndroidUtils.setTextView(this, R.id.wg_report_person_textview4,"福城"+(int)data4+"人");
		//AndroidUtils.setTextView(this, R.id.wg_report_person_textview5,"观澜"+(int)data5+"人");
		//AndroidUtils.setTextView(this, R.id.wg_report_person_textview6,"大浪"+(int)data6+"人");

		PieChartView pieChartView = (PieChartView)findViewById(R.id.pie);
        PieChartView.PieItemBean[] items = new PieChartView.PieItemBean[]{
                new PieChartView.PieItemBean("事件量", data1),
                new PieChartView.PieItemBean("部件量", data2),
                //new PieChartView.PieItemBean("观湖", data3),
                //new PieChartView.PieItemBean("福城", data4),
                //new PieChartView.PieItemBean("观澜", data5),
                //new PieChartView.PieItemBean("大浪", data6)
        };
        pieChartView.setPieItems(items);

        /*
        PieChartView pieChartView = (PieChartView) findViewById(R.id.pie);
        PieChartView.PieItemBean[] items = new PieChartView.PieItemBean[]{
                new PieChartView.PieItemBean("龙华", 200),
                new PieChartView.PieItemBean("民治", 100),
                new PieChartView.PieItemBean("观湖", 120),
                new PieChartView.PieItemBean("福城", 160),
                new PieChartView.PieItemBean("观澜", 100),
                new PieChartView.PieItemBean("大浪", 480)
        };
        pieChartView.setPieItems(items);
        */

		/*
		pcv.setDataCount(6);//饼图
        pcv.setColor(new int[]{Color.YELLOW,Color.BLUE,Color.GRAY,Color.MAGENTA,Color.RED,Color.GREEN});

        float total = 0, data1 = 0, data2 = 0, data3 = 0, data4 = 0, data5 = 0, data6 = 0;
		if(list!=null){
			if(list.size()==6){
				data1 = Float.valueOf(list.get(0).getReturnResult());
				data2 = Float.valueOf(list.get(1).getReturnResult());
				data3 = Float.valueOf(list.get(2).getReturnResult());
				data4 = Float.valueOf(list.get(3).getReturnResult());
				data5 = Float.valueOf(list.get(4).getReturnResult());
				data6 = Float.valueOf(list.get(5).getReturnResult());
			}
		}

		total = data1 + data2 + data3 + data4 + data5 + data6;
		AndroidUtils.setTextView(this, R.id.wg_report_textview_totalcheckperson,"全区共检查"+(int)total+"人");

		//Log.i("longhua","data1"+data1);
		//Log.i("longhua","data1"+data2);
		//Log.i("longhua","data1"+data3);
		//pcv.setData(new float[]{200,700,45,190,409,100});
		pcv.setData(new float[]{data1,data2,data3,data4,data5,data6});
        pcv.setDataTitle(new String[]{"龙华","民治","观湖","福城","观澜","大浪"});
        //pcv.setSpecial(4);

        pcv.invalidate();//调用invalidate或者postInvalidate方法
        */
	}

	public void showEventData(List<SysPassValue> list){

		/*
		br.setGroupCount(6);//柱状图
        br.setDataCount(3);
        br.setGroupData(0, new float[]{80,160,260});
        br.setGroupData(1, new float[]{213,194,333});
        br.setGroupData(2, new float[]{193,645,858});
        br.setGroupData(3, new float[]{677,101,222});
        br.setGroupData(4, new float[]{213,194,244});
        br.setGroupData(5, new float[]{393,645,258});
        br.setBarColor(new int[]{Color.RED,Color.GREEN,Color.BLUE});
        br.setDataTitle(new String[]{"案件检查数","有效上报数","核实总数"});
        br.setGroupTitle(new String[]{"龙华","民治","观湖","福城","观澜","大浪"});
        */

		/*float data1 = 338;
		float data2 = 252;
		float data3 = 233;
		float data4 = 159;
		float data5 = 147;
		float data6 = 139;
		float total1 = 0;*/

		float total = 0, data1 = 0, data2 = 0, data3 = 0, data4 = 0, data5 = 0, data6 = 0;

		if(list!=null){
			int quantity = list.size();
			br.setGroupCount(quantity);//设置有多少组数据，这里是6组
	        //br.setDataCount(3);//每组3列数据
			br.setDataCount(1);//每组1列数据

			if(list.size()==6){
				data1 = Float.valueOf(list.get(0).getReturnResult());
				data2 = Float.valueOf(list.get(1).getReturnResult());
				data3 = Float.valueOf(list.get(2).getReturnResult());
				data4 = Float.valueOf(list.get(3).getReturnResult());
				data5 = Float.valueOf(list.get(4).getReturnResult());
				data6 = Float.valueOf(list.get(5).getReturnResult());
			}

			/*for (int m = 0; m < quantity; m++) {
				if(data1==0.0){//全部为0会报错
					br.setGroupData(m, new float[]{1});
				}else{
					br.setGroupData(m, new float[]{data1});
				}

			}*/

			total = data1+data2+data3+data4+data5+data6;

			br.setGroupData(0, new float[]{data1});
			br.setGroupData(1, new float[]{data2});
			br.setGroupData(2, new float[]{data3});
			br.setGroupData(3, new float[]{data4});
			br.setGroupData(4, new float[]{data5});
			br.setGroupData(5, new float[]{data6});

			//br.setBarColor(new int[]{Color.parseColor("#66e2e1"),Color.parseColor("#fc9d50"),Color.parseColor("#ea7ae9")});//可用使用Color.rgb(234, 122, 233)
			//br.setBarColor(new int[]{Color.parseColor("#fc9d50"), Color.parseColor("#66e2e1"), Color.parseColor("#ea7ae9"), Color.parseColor("#a3e077"), Color.parseColor("#fdef08"), Color.parseColor("#c0f719")});
			br.setBarColor(new int[]{Color.parseColor("#fc9d50")});
			br.setDataTitle(new String[]{"","",""});
	        //br.setGroupTitle(new String[]{"龙华","民治","观湖","福城","观澜","大浪"});
			br.setGroupTitle(new String[]{"龙华","民治","观澜","大浪","福城","观湖"});
	        br.invalidate();

		}else{
			ToastUtils.show(this, "没有获取到数据");
		}

        AndroidUtils.setTextView(this, R.id.wg_report_layout_textview2,(int)data1+"起");
        AndroidUtils.setTextView(this, R.id.wg_report_layout_textview4,(int)data2+"起");
        AndroidUtils.setTextView(this, R.id.wg_report_layout_textview6,(int)data3+"起");
		AndroidUtils.setTextView(this, R.id.wg_report_layout_textview8,(int)data4+"起");
		AndroidUtils.setTextView(this, R.id.wg_report_layout_textview10,(int)data5+"起");
		AndroidUtils.setTextView(this, R.id.wg_report_layout_textview12,(int)data6+"起");
		AndroidUtils.setTextView(this, R.id.wg_report_layout_textview14,(int)total+"起");

		//showDeviceData(list);
		showEventData4(list);
		showEventData5(list);
	}

	public void showDeviceData(List<SysPassValue> list){

		/*float data1 = 288;
		float data2 = 236;
		float data3 = 168;
		float data4 = 139;
		float data5 = 128;
		float data6 = 96;
		float total1 = 0;*/

		float total = 0, data1 = 0, data2 = 0, data3 = 0, data4 = 0, data5 = 0, data6 = 0;

		if(list!=null){
			int quantity = list.size();
			quantity = 6;
			br_1.setGroupCount(quantity);//设置有多少组数据，这里是6组
			//br.setDataCount(3);//每组3列数据
			br_1.setDataCount(1);//每组1列数据

			if(list.size()==6){
				data1 = Float.valueOf(list.get(0).getReturnResult());
				data2 = Float.valueOf(list.get(1).getReturnResult());
				data3 = Float.valueOf(list.get(2).getReturnResult());
				data4 = Float.valueOf(list.get(3).getReturnResult());
				data5 = Float.valueOf(list.get(4).getReturnResult());
				data6 = Float.valueOf(list.get(5).getReturnResult());
			}

			total = data1+data2+data3+data4+data5+data6;

			br_1.setGroupData(0, new float[]{data1});
			br_1.setGroupData(1, new float[]{data2});
			br_1.setGroupData(2, new float[]{data3});
			br_1.setGroupData(3, new float[]{data4});
			br_1.setGroupData(4, new float[]{data5});
			br_1.setGroupData(5, new float[]{data6});

			//br.setBarColor(new int[]{Color.parseColor("#66e2e1"),Color.parseColor("#fc9d50"),Color.parseColor("#ea7ae9")});//可用使用Color.rgb(234, 122, 233)
			//br.setBarColor(new int[]{Color.parseColor("#fc9d50"), Color.parseColor("#66e2e1"), Color.parseColor("#ea7ae9"), Color.parseColor("#a3e077"), Color.parseColor("#fdef08"), Color.parseColor("#c0f719")});
		    br_1.setBarColor(new int[]{Color.parseColor("#66e2e1")});
		    br_1.setDataTitle(new String[]{"","",""});
			//br.setGroupTitle(new String[]{"龙华","民治","观湖","福城","观澜","大浪"});
		    br_1.setGroupTitle(new String[]{"龙华","民治","观澜","大浪","福城","观湖"});
			br_1.invalidate();

		}else{
			ToastUtils.show(this, "没有获取到数据");
		}

		AndroidUtils.setTextView(this, R.id.wg_report_layout1_textview2,(int)data1+"起");
		AndroidUtils.setTextView(this, R.id.wg_report_layout1_textview4,(int)data2+"起");
		AndroidUtils.setTextView(this, R.id.wg_report_layout1_textview6,(int)data3+"起");
		AndroidUtils.setTextView(this, R.id.wg_report_layout1_textview8,(int)data4+"起");
		AndroidUtils.setTextView(this, R.id.wg_report_layout1_textview10,(int)data5+"起");
		AndroidUtils.setTextView(this, R.id.wg_report_layout1_textview12,(int)data6+"起");
		AndroidUtils.setTextView(this, R.id.wg_report_layout1_textview14,(int)total+"起");
	}

	public void showEventData4(List<SysPassValue> list){

		float data1 = 380;
		float data2 = 298;
		float data3 = 275;
		float data4 = 241;
		float data5 = 205;
		float data6 = 165;
		float total1 = 0;

		//if(list!=null){
		int quantity = list.size();
		quantity = 6;
		br4.setGroupCount(quantity);//设置有多少组数据，这里是6组
		//br.setDataCount(3);//每组3列数据
		br4.setDataCount(1);//每组1列数据
		for (int m = 0; m < quantity; m++) {
			//data1 = StrEdit.StrToFloat(list.get(m).getArg1())*10;

			//total1 = total1 + data1;

			//br.setGroupData(m, new float[]{80,160,260});

			if(data1==0.0){//全部为0会报错
				br4.setGroupData(m, new float[]{1});
			}else{
				br4.setGroupData(m, new float[]{data1});
			}

		}

		total1 = data1+data2+data3+data4+data5+data6;

		br4.setGroupData(0, new float[]{data1});
		br4.setGroupData(1, new float[]{data2});
		br4.setGroupData(2, new float[]{data3});
		br4.setGroupData(3, new float[]{data4});
		br4.setGroupData(4, new float[]{data5});
		br4.setGroupData(5, new float[]{data6});

		//br.setBarColor(new int[]{Color.parseColor("#66e2e1"),Color.parseColor("#fc9d50"),Color.parseColor("#ea7ae9")});//可用使用Color.rgb(234, 122, 233)
		//br.setBarColor(new int[]{Color.parseColor("#fc9d50"), Color.parseColor("#66e2e1"), Color.parseColor("#ea7ae9"), Color.parseColor("#a3e077"), Color.parseColor("#fdef08"), Color.parseColor("#c0f719")});
		br4.setBarColor(new int[]{Color.parseColor("#ea7ae9")});
		br4.setDataTitle(new String[]{"","",""});
		//br.setGroupTitle(new String[]{"龙华","民治","观湖","福城","观澜","大浪"});
		br4.setGroupTitle(new String[]{"无照经营","沿街晾挂","乱堆物堆料","店外经营","未经批准设置广告牌","暴露垃圾"});
		br4.invalidate();

		//}else{
		//	ToastUtils.show(this, "没有获取到数据");
		//}

		AndroidUtils.setTextView(this, R.id.wg_report_layout4_textview2,(int)data1+"起");
		AndroidUtils.setTextView(this, R.id.wg_report_layout4_textview4,(int)data2+"起");
		AndroidUtils.setTextView(this, R.id.wg_report_layout4_textview6,(int)data3+"起");
		AndroidUtils.setTextView(this, R.id.wg_report_layout4_textview8,(int)data4+"起");
		AndroidUtils.setTextView(this, R.id.wg_report_layout4_textview10,(int)data5+"起");
		AndroidUtils.setTextView(this, R.id.wg_report_layout4_textview12,(int)data6+"起");
	}

	public void showEventData5(List<SysPassValue> list){

		float data1 = 338;
		float data2 = 252;
		float data3 = 233;
		float data4 = 159;
		float data5 = 147;
		float data6 = 139;
		float total1 = 0;

		//if(list!=null){
		int quantity = list.size();
		quantity = 6;
		br5.setGroupCount(quantity);//设置有多少组数据，这里是6组
		//br.setDataCount(3);//每组3列数据
		br5.setDataCount(1);//每组1列数据
		for (int m = 0; m < quantity; m++) {
			//data1 = StrEdit.StrToFloat(list.get(m).getArg1())*10;

			//total1 = total1 + data1;

			//br.setGroupData(m, new float[]{80,160,260});

			if(data1==0.0){//全部为0会报错
				br5.setGroupData(m, new float[]{1});
			}else{
				br5.setGroupData(m, new float[]{data1});
			}

		}

		total1 = data1+data2+data3+data4+data5+data6;

		br5.setGroupData(0, new float[]{data1});
		br5.setGroupData(1, new float[]{data2});
		br5.setGroupData(2, new float[]{data3});
		br5.setGroupData(3, new float[]{data4});
		br5.setGroupData(4, new float[]{data5});
		br5.setGroupData(5, new float[]{data6});

		//br.setBarColor(new int[]{Color.parseColor("#66e2e1"),Color.parseColor("#fc9d50"),Color.parseColor("#ea7ae9")});//可用使用Color.rgb(234, 122, 233)
		//br.setBarColor(new int[]{Color.parseColor("#fc9d50"), Color.parseColor("#66e2e1"), Color.parseColor("#ea7ae9"), Color.parseColor("#a3e077"), Color.parseColor("#fdef08"), Color.parseColor("#c0f719")});
		br5.setBarColor(new int[]{Color.parseColor("#a3e077")});
		br5.setDataTitle(new String[]{"","",""});
		//br.setGroupTitle(new String[]{"龙华","民治","观湖","福城","观澜","大浪"});
		br5.setGroupTitle(new String[]{"雨水篦子","防撞柱","交通护栏","通讯井盖","污水井盖","路灯"});
		br5.invalidate();

		//}else{
		//	ToastUtils.show(this, "没有获取到数据");
		//}

		AndroidUtils.setTextView(this, R.id.wg_report_layout5_textview2,(int)data1+"起");
		AndroidUtils.setTextView(this, R.id.wg_report_layout5_textview4,(int)data2+"起");
		AndroidUtils.setTextView(this, R.id.wg_report_layout5_textview6,(int)data3+"起");
		AndroidUtils.setTextView(this, R.id.wg_report_layout5_textview8,(int)data4+"起");
		AndroidUtils.setTextView(this, R.id.wg_report_layout5_textview10,(int)data5+"起");
		AndroidUtils.setTextView(this, R.id.wg_report_layout5_textview12,(int)data6+"起");
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	public void onClick(View view) {

		if(R.id.wg_head_btn_return==view.getId()){
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		}

		if(R.id.wg_btn_day==view.getId()) {
			loading.showLoading();
			GetData("getToday");
		}

		if(R.id.wg_btn_week==view.getId()) {
			loading.showLoading();
			GetData("getWeek");
		}

		if(R.id.wg_btn_month==view.getId()) {
			loading.showLoading();
			GetData("getMonth");
		}

		if(R.id.wg_btn_season==view.getId()) {
			loading.showLoading();
			GetData("getSeason");
		}

		if(R.id.wg_btn_year==view.getId()) {
			loading.showLoading();
			GetData("getYear");
		}

		if(R.id.wg_btn_date==view.getId()) {

			String startdate = txtstartdate.getText().toString();
			String enddate = txtenddate.getText().toString();

			if(startdate.equals("开始日期")){
				ToastUtils.show(this, "请选择查询开始日期");
				return;
			}

			if(enddate.equals("结束日期")){
				ToastUtils.show(this, "请选择查询结束日期");
				return;
			}

			loading.showLoading();
			GetData("getBetweenDate");
		}

		if(R.id.btn_start_date==view.getId()){
			bottombirthdaywindow(txtstartdate, "txtstartdate");
			new Thread(new Runnable(){
                @Override
                public void run() {
                    while(alpha>0.5f){
                        try {
                            //4是根据弹出动画时间和减少的透明度计算
                            Thread.sleep(4);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Message msg =mHandler.obtainMessage();
                        msg.what = 1;
                        //每次减少0.01，精度越高，变暗的效果越流畅
                        alpha-=0.01f;
                        msg.obj =alpha ;
                        mHandler.sendMessage(msg);
                    }
                }

            }).start();
		}

		if(R.id.btn_end_date==view.getId()){
			bottombirthdaywindow(txtenddate, "txtenddate");
			new Thread(new Runnable(){
                @Override
                public void run() {
                    while(alpha>0.5f){
                        try {
                            //4是根据弹出动画时间和减少的透明度计算
                            Thread.sleep(4);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Message msg =mHandler.obtainMessage();
                        msg.what = 1;
                        //每次减少0.01，精度越高，变暗的效果越流畅
                        alpha-=0.01f;
                        msg.obj =alpha ;
                        mHandler.sendMessage(msg);
                    }
                }

            }).start();
		}

		if(R.id.wg_chart_btn==view.getId()) {
			AndroidUtils.start(this, WgChartActivity.class);
			this.finish();
			//overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			return;
		}

		if(R.id.wg_chart_btn3==view.getId()) {
			AndroidUtils.start(this, WgChartZongHeActivity.class);
			this.finish();
			//overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			return;
		}

		return;
    }

	public String getNowDate(){
        Date currTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd",Locale.US);
        String nowdate = null;
        try {
            nowdate = new String(formatter.format(currTime).getBytes("iso-8859-1"));
        } catch (UnsupportedEncodingException ex) {
        }

        return nowdate;
    }

	//选择日期
	void bottombirthdaywindow(TextView view, String dateType) {
        if (popupWindow != null && popupWindow.isShowing()) {
            return;
        }
        LinearLayout timepickerview = (LinearLayout) getLayoutInflater().inflate(R.layout.timepicker, null);
        wheelMain = new WheelMain(timepickerview);
        ScreenInfo screenInfo = new ScreenInfo(WgChartZongHeFenXiActivity.this);
		wheelMain.screenheight = screenInfo.getHeight();
		String time = txtstartdate.getText().toString();
		Calendar calendar = Calendar.getInstance();
		if (JudgeDate.isDate(time, "yyyy-MM-dd")) {
			try {
				calendar.setTime(dateFormat.parse(time));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		wheelMain.initDateTimePicker(year, month, day);

        popupWindow = new PopupWindow(timepickerview,
                LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT);
        //点击空白处时，隐藏掉pop窗口
        popupWindow.setFocusable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        //添加弹出、弹入的动画
        popupWindow.setAnimationStyle(R.style.Popupwindow);
        int[] location = new int[2];
        view.getLocationOnScreen(location);
        popupWindow.showAtLocation(view, Gravity.LEFT | Gravity.BOTTOM, 0, -location[1]);

        //修改的话，这个地方要修改 *****************
        //添加按键事件监听
        setBirthdayListeners(timepickerview, dateType);
        //添加pop窗口关闭事件，主要是实现关闭时改变背景的透明度
        popupWindow.setOnDismissListener(new poponDismissListener());
        backgroundAlpha(1f);
    }

	//监听日期
	private void setBirthdayListeners(LinearLayout layout, String dateType) {

    	Button bt_select_birthday_ok = (Button) layout.findViewById(R.id.btn_select_birthday_ok);
    	Button bt_select_birthday_cancel = (Button) layout.findViewById(R.id.btn_select_birthday_cancel);

    	ColorStateList whiteColor=getResources().getColorStateList(R.color.popwindowselectok);
    	bt_select_birthday_ok.setTextColor(whiteColor);

    	final String fdateType = dateType;

    	bt_select_birthday_ok.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                    //在此处添加你的按键处理 xxx

                	Log.i("longhua",wheelMain.getTime());

                    popupWindow.dismiss();
                    backgroundAlpha(1f);

                    //loading.showLoading();

                    if(fdateType.equals("txtstartdate")){
                    	txtstartdate.setText(wheelMain.getTime());
                    }

                    if(fdateType.equals("txtenddate")){
                    	txtenddate.setText(wheelMain.getTime());
                    }

                    //loading.hideLoading();
                    /*
        			new HhUserModifyPersonInfoYTask(new AbstractYGetTaskListener<User>() {
        				@Override
        				public void onPostExecute(String name, YGetTask<User> task) {

        					userModifyData(task.getValue(),"3");
        				}
        			},user,"3").execute();
        			*/

                }
            }
        });
    	bt_select_birthday_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                    //在此处添加你的按键处理 xxx
                	 //选照片相
                	popupWindow.dismiss();
                	backgroundAlpha(1f);

                    return;

                }
            }
        });

    	bt_select_birthday_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popupWindow != null && popupWindow.isShowing()) {
                    popupWindow.dismiss();
                }
            }
        });
    }

	/*
     * 设置添加屏幕的背景透明度
     * @param bgAlpha
     */
    public void backgroundAlpha(float bgAlpha)
    {
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.alpha = bgAlpha; //0.0-1.0
        getWindow().setAttributes(lp);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

       /*
        *    WindowManager.LayoutParams lp = activity.getWindow().getAttributes();  
        lp.alpha = bgAlpha;  
        if (bgAlpha == 1) {  
            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);//不移除该Flag的话,在有视频的页面上的视频会出现黑屏的bug  
        } else {  
            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);//此行代码主要是解决在华为手机上半透明效果无效的bug  
        }  
        activity.getWindow().setAttributes(lp);
        */
    }

	/*
     * 返回或者点击空白位置的时候将背景透明度改回来
     */
    class poponDismissListener implements OnDismissListener{

        @Override
        public void onDismiss() {
            // TODO Auto-generated method stub
            new Thread(new Runnable(){
                @Override
                public void run() {
                    //此处while的条件alpha不能<= 否则会出现黑屏
                    while(alpha<1f){
                        try {
                            Thread.sleep(4);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Log.d("longhua","alpha:"+alpha);
                        Message msg =mHandler.obtainMessage();
                        msg.what = 1;
                        alpha+=0.01f;
                        msg.obj =alpha ;
                        mHandler.sendMessage(msg);
                    }
                }

            }).start();
        }
    }
    
    Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case 1: 
                	backgroundAlpha(alpha);
                    //backgroundAlpha((float)msg.obj);
                    break;
            }
        }
    };
    
    /*
    class ThreadDemo implements Runnable {
        public void run() {
        	//Log.i("longhua", "1 run tempUserId"+tempUserId);
        	GetData(taskType);
            myHandler.sendEmptyMessage(0);
        }
    }
    
    Handler myHandler = new Handler() {
    	
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case 0:
            	Log.i("longhua", "handleMessage有数据");
            	//drawPolyLine(trackLatLngs);
            	//thread = new WgMapThread(trackLatLngs,mMapView,mMoveMarker,mHandler);
				//thread.start();
                //loading.hideLoading();
                break;
            case 1:
                Log.i("longhua", "handleMessage无数据");
                loading.hideLoading();
                break;
            }
            super.handleMessage(msg);
        }
    };
    */
		 	
}
