package com.zhyq;


import android.os.Bundle;

import android.support.v4.app.FragmentActivity;

import android.view.View;
import android.view.View.OnClickListener; 
import android.widget.Button;
import android.widget.TextView;


import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.map.Stroke;
import com.baidu.mapapi.model.LatLng;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.CircleOptions;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.MyLocationConfiguration;
import com.baidu.mapapi.map.MyLocationConfiguration.LocationMode;
import com.baidu.mapapi.map.MyLocationData;

import android.widget.Toast;


public class WgMapActivityDemo extends FragmentActivity  implements OnClickListener  {
 
	private View  includeTitle;
	private Long    userId = null;
	
	private MapView mMapView = null;
	private BaiduMap mBaiduMap;
	
	private Button addOverlayBtn;
	private Button locCurplaceBtn;
	private int isShowOverlay = 1;
	private boolean isFirstLoc = true;
	private LocationClient mLocClient;
	private LocationMode mCurrentMode;
	private BitmapDescriptor mCurrentMarker = null;
	private double latitude;
	private double longitude;
	
	public MyLocationListenner locListener = new MyLocationListenner();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//在使用SDK各组件之前初始化context信息，传入ApplicationContext
        //注意该方法要再setContentView方法之前实现  
        SDKInitializer.initialize(getApplicationContext());
		
		setContentView(R.layout.activity_wg_map);
		
		//获取地图控件引用
        mMapView = (MapView) findViewById(R.id.bmapView);
        
        //addOverlayBtn = (Button) findViewById(R.id.btn_add_overlay);
        //locCurplaceBtn = (Button) findViewById(R.id.btn_cur_place);
        
        //卫星地图
        //mBaiduMap.setMapType(BaiduMap.MAP_TYPE_SATELLITE);
        //开启交通图
        //mBaiduMap.setTrafficEnabled(true);
		
        //普通地图
		mBaiduMap = mMapView.getMap();
		mBaiduMap.setMapType(BaiduMap.MAP_TYPE_NORMAL);
		
		MapStatusUpdate msu = MapStatusUpdateFactory.zoomTo(16.0f);//设置地图缩放级别16
		mBaiduMap.setMapStatus(msu);
		
		mBaiduMap.setMyLocationEnabled(true);//开启定位图层
		mLocClient = new LocationClient(this);//定位初始化，注意: 实例化定位服务 LocationClient类必须在主线程中声明 并注册定位监听接口
		mLocClient.registerLocationListener(locListener);
		LocationClientOption option = new LocationClientOption();
		option.setOpenGps(true);//打开GPS
		option.setCoorType("bd09ll");//设置坐标类型
		option.setScanSpan(5000);//设置发起定位请求的间隔时间为5000ms
		mLocClient.setLocOption(option);//设置定位参数
		mLocClient.start();//调用此方法开始定位
		
		addMyLocation();//加标注
		addCircleOverlay();//添加覆盖物
		
		//ToastUtils.show(this, latitude+" "+longitude);
		
		//LatLng point = new LatLng(latitude, longitude);
		LatLng point = new LatLng(22.52958, 113.354539);
		BitmapDescriptor bitmap = BitmapDescriptorFactory
			    .fromResource(R.drawable.icon_markp);//icon_marka
			//构建MarkerOption，用于在地图上添加Marker
		OverlayOptions option2 = new MarkerOptions()
			    .position(point)
			    .icon(bitmap);
			//在地图上添加Marker，并显示
		mBaiduMap.addOverlay(option2);
		
		
		
		
		/*
		//几何图形覆盖物
		//定义多边形的五个顶点
		LatLng pt1 = new LatLng(39.93923, 116.357428);
		LatLng pt2 = new LatLng(39.91923, 116.327428);
        LatLng pt3 = new LatLng(39.89923, 116.347428);
		LatLng pt4 = new LatLng(39.89923, 116.367428);
		LatLng pt5 = new LatLng(39.91923, 116.387428);
		List<LatLng> pts = new ArrayList<LatLng>();
		pts.add(pt1);
		pts.add(pt2);
		pts.add(pt3);
		pts.add(pt4);
		pts.add(pt5);
		//构建用户绘制多边形的Option对象
		OverlayOptions polygonOption = new PolygonOptions().points(pts)
				.stroke(new Stroke(5, 0xAA00FF00))
				.fillColor(0xAAFFFF00);
		mBaiduMap.addOverlay(polygonOption);
		*/
        
        
		
		includeTitle = this.findViewById(R.id.wg_map_head);
		
		((TextView)includeTitle.findViewById(R.id.wg_head_title)).setText("检查定位");
		
		Button btreturn = (Button)includeTitle.findViewById(R.id.wg_head_btn_return);
		btreturn.setOnClickListener(this);
		
		if(HhApplication.getInstance(this).getHhCart().getUser()==null){
			userId =-1l;
		}else{
			userId = HhApplication.getInstance(this).getHhCart().getUser().getId();
		}
		
        
	}
	
	protected void onDestroy() {
        mLocClient.stop();                       //退出时销毁定位
        mBaiduMap.setMyLocationEnabled(false);   //关闭定位图层
        mMapView.onDestroy();
        mMapView = null;
        super.onDestroy();
    }
	
    @Override
    protected void onResume() {
        super.onResume();
        mMapView.onResume();
    }
    
    @Override
    protected void onPause() {
        super.onPause();
        mMapView.onPause();
    }
    
	@Override
	public void onClick(View view) {
		
		if(R.id.wg_head_btn_return==view.getId()){
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		}
		
		return;
	}
	
	public class MyLocationListenner implements BDLocationListener  {
		
		public void onReceivePoi(BDLocation location) {
		}
		
		@Override
		public void onReceiveLocation(BDLocation location) {
	        //mapview 销毁后不在处理新接收的位置
	        if (location == null || mBaiduMap == null) {
	            return;
	        }
	        //MyLocationData.Builder定位数据建造器
	        MyLocationData locData = new MyLocationData.Builder()
	                .accuracy(location.getRadius())
	                .direction(100)
	                .latitude(location.getLatitude())
	                .longitude(location.getLongitude())
	                .build();
	        //设置定位数据
	        mBaiduMap.setMyLocationData(locData);
	        mCurrentMode = LocationMode.NORMAL;
	        //获取经纬度
	        latitude = location.getLatitude();
	        longitude = location.getLongitude();
	        //Toast.makeText(getApplicationContext(), String.valueOf(latitude), Toast.LENGTH_SHORT).show();
	        //第一次定位的时候，那地图中心点显示为定位到的位置  
	        if (isFirstLoc) {
	            isFirstLoc = false;  
	            //地理坐标基本数据结构  
	            LatLng loc = new LatLng(location.getLatitude(),location.getLongitude());  
	            //MapStatusUpdate描述地图将要发生的变化  
	            //MapStatusUpdateFactory生成地图将要反生的变化  
	            MapStatusUpdate msu = MapStatusUpdateFactory.newLatLng(loc);  
	            mBaiduMap.animateMapStatus(msu);
	            Toast.makeText(getApplicationContext(), location.getAddrStr(), Toast.LENGTH_SHORT).show();  
	        }
		}
	}
		
		/*
		 * 定位并添加标注
		 */
		private void addMyLocation() {
	        //更新
	       mBaiduMap.setMyLocationConfigeration(new MyLocationConfiguration(
	               mCurrentMode, true, mCurrentMarker));
	       mBaiduMap.clear();
	       //addOverlayBtn.setEnabled(true);
	       //定义Maker坐标点
	       LatLng point = new LatLng(latitude, longitude);
	       //构建Marker图标
	       BitmapDescriptor bitmap = BitmapDescriptorFactory.fromResource(R.drawable.icon_marka);
	       //构建MarkerOption，用于在地图上添加Marker
	       OverlayOptions option = new MarkerOptions()
	           .position(point)
	           .icon(bitmap);
	       //在地图上添加Marker，并显示
	       mBaiduMap.addOverlay(option);
		}
		
		/*
		 * 添加覆盖物
		 */
		private void addCircleOverlay() {
	        if(isShowOverlay == 1) {//点击显示
	            mBaiduMap.clear();
	            isShowOverlay = 0;
	            //DotOptions 圆点覆盖物
	            LatLng pt = new LatLng(latitude, longitude);
	            CircleOptions circleOptions = new CircleOptions();
	            //circleOptions.center(new LatLng(latitude, longitude));
	            circleOptions.center(pt);                          //设置圆心坐标
	            circleOptions.fillColor(0xAAFFFF00);               //圆填充颜色
	            circleOptions.radius(250);                         //设置半径
	            circleOptions.stroke(new Stroke(5, 0xAA00FF00));   // 设置边框
	            mBaiduMap.addOverlay(circleOptions);
	        } else {
	        	mBaiduMap.clear();
	            isShowOverlay = 1;
	        }
		}
		
	}