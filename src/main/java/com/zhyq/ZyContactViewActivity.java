package com.zhyq;

import android.os.Bundle;
import android.os.Handler;

import android.support.v4.app.FragmentActivity;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ScrollView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.zhyq.libs.AndroidUtils;
import com.zhyq.model.SysPassValue;

import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.sendOrderToServerForValueYTask;

/*
使用实现上拉下拉刷新，原来是继承AppCompatActivity，那样要在AndroidManifest Activity里面使用AppCompat的theme,如android:theme="@style/Theme.AppCompat.Light.NoActionBar"
使用android:theme="@style/Theme.AppCompat.Light.NoActionBar"的话，转圈显示绿色
继承BaseActivity也可以
 */

public class ZyContactViewActivity extends FragmentActivity implements OnClickListener {

    private View childrenView = null;
    private PullToRefreshScrollView mPullRefreshScrollView = null;
    //private ScrollView mScrollView;
    private View includeTitle;
    private String contactId = "";
    private Long userId = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zy_contact_detail);
        includeTitle = this.findViewById(R.id.zy_contactdetail_head);
        ((TextView)includeTitle.findViewById(R.id.wg_head_title)).setText("通讯录");
        Button btreturn = (Button)includeTitle.findViewById(R.id.util_head_btn_return);
        btreturn.setOnClickListener(this);

        if(HhApplication.getInstance(this).getHhCart().getUser()==null){
            userId =-1l;
        }else{
            userId = HhApplication.getInstance(this).getHhCart().getUser().getId();
        }
        contactId = this.getIntent().getStringExtra("id");

        mPullRefreshScrollView = (PullToRefreshScrollView)this.findViewById(R.id.wg_contactdetail_refresh_scrollview);
        //mScrollView = mPullRefreshScrollView.getRefreshableView();//好像没什么用，所以屏蔽了
        mPullRefreshScrollView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ScrollView>(){//刷新
            @Override
            public void onRefresh(PullToRefreshBase<ScrollView> refreshView){
                readData();
            }
        });

        new Handler().postDelayed(new Runnable() {//延迟读取数据
            @Override
            public void run() {
                onRefresh();
            }
        }, 100);

    }

    private void onRefresh(){
        mPullRefreshScrollView.setRefreshing(true);
        readData();//首次加载任务数据加载任务
    }

    /*
     * onResume是在启动activity启动之后才能执行的，也就是恢复执行。程序正常启动：onCreate()->onStart()->onResume();
     * 该函数，如果在打开了其他窗口，再关闭，这个函数也会执行
     */
    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onClick(View view) {

        if(R.id.util_head_btn_return==view.getId()){
            this.finish();
            overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
            return;
        }

        return;
    }

    private void readData(){
        SysPassValue temp = new SysPassValue();
        temp.setFunctionName("getContactDetail");
        temp.setId(String.valueOf(contactId));
        temp.setUserId(String.valueOf(userId));

        new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
            @Override
            public void onPostExecute(String name, YGetTask<SysPassValue> task) {
                mPullRefreshScrollView.onRefreshComplete();
                getData(task.getValue());
            }
        },temp).execute();
    }

    private void getData(SysPassValue passValue) {

        View vnorecord = this.findViewById(R.id.activity_no_record_showpic);

        if (passValue==null) {
            vnorecord.setVisibility(View.VISIBLE);
            //ToastUtils.show(this,"没有获取到信息");
            return;
        } else {

            vnorecord.setVisibility(View.GONE);

            if(childrenView==null){
                //加载内容面板
                initContentView();
                //设置监听
                //((Button)childrenView.findViewById(R.id.btn_recharge_zhuanzhang)).setOnClickListener(this);
                //vBt1 =   childrenView.findViewById(R.id.btn_shop_chongzhi_flow1);
                //btn_shop_chongzhi_flow1 = (Button)vBt1.findViewById(R.id.hh_btn_chongzhi);
                //btn_shop_chongzhi_flow1.setOnClickListener(this);
                //childrenView.findViewById(R.id.btn_recharge_vip_payway_zhifubao_right_circleimage).setOnClickListener(this);
                //intUI();
            }
			 /*((TextView)includeMaster.findViewById(R.id.tv_masterdetail_head_image_shopname)).setText(masterviewdetail.getArg1());
			 AndroidUtils.setWebImageView(includeMaster, R.id.hh_masterdetail_head_image_shoplogimage, masterviewdetail.getArg2());
			 AndroidUtils.setWebImageView(includeMaster, R.id.hh_masterdetail_head_image_masterimage, masterviewdetail.getArg3());*/

            String sid =  passValue.getId();
            String arg1 =  passValue.getArg1();
            String arg2 =  passValue.getArg2();
            String arg3 =  passValue.getArg3();
            String arg4 =  passValue.getArg4();
            String arg5 =  passValue.getArg5();
            String arg6 =  passValue.getArg6();
            String arg7 =  passValue.getArg7();
            String arg8 =  passValue.getArg8();

            AndroidUtils.setTextView(this, R.id.wg_contactdetail_textview2,arg2);
            AndroidUtils.setTextView(childrenView, R.id.wg_contactdetail_textview3,arg3);
            AndroidUtils.setTextView(childrenView, R.id.wg_contactdetail_textview4,arg4);
            AndroidUtils.setTextView(childrenView, R.id.wg_contactdetail_textview5,arg5);
            AndroidUtils.setTextView(childrenView, R.id.wg_contactdetail_textview6,arg6);
            AndroidUtils.setTextView(childrenView, R.id.wg_contactdetail_textview7,arg7);
            AndroidUtils.setTextView(childrenView, R.id.wg_contactdetail_textview8,arg8);
            //AndroidUtils.setWebImageView(this, R.id.wg_contact_round_img, arg1);
            AndroidUtils.setWebImageCircleView(childrenView, R.id.wg_contact_round_img,arg1);

            //((ImageView)includeTitle.findViewById(R.id.iv_hh_head_masterdetail_right_saygoods)).setBackgroundResource(R.drawable.praisetech);

        }
    }

    private void intUI(){

    }

    private void initContentView(){
        childrenView = View.inflate(this, R.layout.activity_zy_contact_detail_inlayout,null);

        //mscrollView.addHeaderView(headerView);
        mPullRefreshScrollView.addView(childrenView);
    }



}
