package com.zhyq;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.zhyq.fragment.WgPlanFragment;
import com.zhyq.libs.CommonUtils;
import com.zhyq.libs.TabPageIndicator;
import com.zhyq.libs.ViewPagerScroller;
import com.zhyq.model.User;

import java.lang.reflect.Field;

/*
 * 计划列表
 * 主页WgHomeFragmentZhuye使用startActivity(intent);打开WgPlanNewActivity
 * WgTaskActivity设置栏目，导入栏目，调用WgTaskFragment获取列表内容，WgTaskFragmentList通过WgFeedbackListNewAdapter导入列表布局文件和数据设置触发
 */

public class WgPlanNewActivity extends FragmentActivity implements OnClickListener {

    private View includeTitle;
    private TabPageIndicator mIndicator;//栏目
    private ViewPager mViewPager;//列表
    private BasePagerAdapter adapter;

    private static final String[] TITLE = new String[]{"未执行", "执行中", "已完成"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        this.setContentView(R.layout.activity_wg_plan_list);

        includeTitle = this.findViewById(R.id.wg_util_head_plan_list);

        Button btreturn = (Button) includeTitle.findViewById(R.id.wg_head_add_btn_return);
        btreturn.setOnClickListener(this);

        ((TextView) includeTitle.findViewById(R.id.wg_head_add_title)).setText("检查计划");

        Button btadd = (Button) includeTitle.findViewById(R.id.wg_util_head_btn_add);
        btadd.setOnClickListener(this);

        //显示新增功能
        TextView addtext = (TextView) this.findViewById(R.id.wg_util_head_text_add);
        Button addbutton = (Button) this.findViewById(R.id.wg_util_head_btn_add);
        /*addtext.setVisibility(View.INVISIBLE);
        addbutton.setVisibility(View.INVISIBLE);*/

        initView();
    }

    @Override
    public void onClick(View view) {

        if (R.id.wg_head_add_btn_return == view.getId()) {
            this.finish();
            overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
            return;
        }

        if (R.id.wg_util_head_btn_add == view.getId()) {

            Intent intent = new Intent(this, WgPlanAdd.class);
            Bundle bundle = new Bundle();

            Long userid = null;
            User curuser = new User();
            curuser = HhApplication.getInstance(this).getHhCart().getUser();
            if (curuser == null) {
                userid = -1l;
            } else {
                userid = HhApplication.getInstance(this).getHhCart().getUser().getId();
            }

            //Long userid = HhApplication.getInstance(this).getHhCart().getUser().getId();

            bundle.putString("userid", String.valueOf(userid));
            intent.putExtras(bundle);

            startActivityForResult(intent, 1);//这里采用startActivityForResult来做跳转，此处的0为一个依据，可以写其他的值，但一定要>=0
            overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
        }

        return;
    }

    /*
     * 初始化栏目和列表，读取栏目id和列表id
     */
    private void initView() {
        mViewPager = (ViewPager) findViewById(R.id.vp_plan_list_view_pager);//内容框
        //initViewPagerScroll();//滚动的时候滑动慢一点的效果
        //mViewPager.setOffscreenPageLimit(0);

        mIndicator = (TabPageIndicator) findViewById(R.id.vpi_id_indicator_category);//切换栏目

        adapter = new BasePagerAdapter(getSupportFragmentManager());//获取列表内容

        mViewPager.setAdapter(adapter);
        mIndicator.setViewPager(mViewPager);
        setTabPagerIndicator();
    }

    class BasePagerAdapter extends FragmentPagerAdapter {
        String[] titles;

        WgPlanFragment fragment = null;
        WgPlanFragment mCurrentFragment = null;

        public BasePagerAdapter(FragmentManager fm) {
            super(fm);
            this.titles = TITLE;//CommonUtils.getStringArray(R.array.expand_titles);
        }

        @Override
        public Fragment getItem(int position) {

            //如果  Fragment 类别不同 ，在这个地方 position 进行判断
            fragment = new WgPlanFragment();
            Bundle args0 = new Bundle();
            args0.putString("TITLES", TITLE[position]);//切换的时候，用Bundle传递参数
            fragment.setArguments(args0);
            return fragment;
        }

        @Override
        public int getCount() {
            return titles.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }

        //ViewPager在切换的时候，如果频繁销毁和加载Fragment，就容易产生卡顿现象，
        //阻止Fragment的销毁可有效减缓卡顿现象。
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            //super.destroyItem(container, position, object);
        }

        //----------------------下面才是重点-----------------  
        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            mCurrentFragment = (WgPlanFragment) object;
            super.setPrimaryItem(container, position, object);
        }

        public WgPlanFragment getCurrentFragment() {
            return mCurrentFragment;
        }
    }

    public void retrieveByClass(String fenlei) {
        WgPlanFragment curfragment = adapter.getCurrentFragment();
        curfragment.setFenLei(fenlei);
        //mCurrentFragment.setFenLei(fenlei);
    }

    /*
     * 设置颜色字体
     */
    private void setTabPagerIndicator() {
        mIndicator.setIndicatorMode(TabPageIndicator.IndicatorMode.MODE_NOWEIGHT_EXPAND_NOSAME);// 设置模式，一定要先设置模式
        mIndicator.setDividerColor(Color.parseColor("#00bbcf"));// 设置分割线的颜色
        mIndicator.setDividerPadding(CommonUtils.dip2px(getApplicationContext(), 10));
        mIndicator.setIndicatorColor(Color.parseColor("#00b2f0"));// 设置底部导航线的颜色
        mIndicator.setTextColorSelected(Color.parseColor("#00b2f0"));// 设置tab标题选中的颜色
        mIndicator.setTextColor(Color.parseColor("#797979"));// 设置tab标题未被选中的颜色
        mIndicator.setTextSize(CommonUtils.sp2px(getApplicationContext(), 16));// 设置字体大小
    }

    /*
     * 设置ViewPager的滑动速度
     */
    private void initViewPagerScroll() {
        try {
            Field mScroller = null;
            mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            ViewPagerScroller scroller = new ViewPagerScroller(mViewPager.getContext());
            mScroller.set(mViewPager, scroller);
        } catch (NoSuchFieldException e) {
        } catch (IllegalArgumentException e) {
        } catch (IllegalAccessException e) {
        }

    }


}
