package com.zhyq;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.aspsine.irecyclerview.IRecyclerView;
import com.aspsine.irecyclerview.OnLoadMoreListener;
import com.aspsine.irecyclerview.OnRefreshListener;
import com.aspsine.irecyclerview.demo.model.Image;
import com.aspsine.irecyclerview.demo.ui.adapter.OnItemClickListener;
import com.aspsine.irecyclerview.demo.ui.widget.footer.LoadMoreFooterView;
import com.zhyq.adapter.baseRecyleViewAdapter;
import com.zhyq.libs.AndroidUtils;
import com.zhyq.libs.BaseActivity;
import com.zhyq.libs.PreferenceUtils;
import com.zhyq.libs.ProgersssDialog;
import com.zhyq.libs.ToastUtils;
import com.zhyq.model.SysPassValue;
import com.zhyq.task.AbstractYGetTaskListener;
import com.zhyq.task.YGetTask;
import com.zhyq.task.impl.sendOrderToServerForListYTask;
import com.zhyq.task.impl.sendOrderToServerForValueYTask;

import java.util.ArrayList;
import java.util.List;

/*
使用实现上拉下拉刷新，原来是继承AppCompatActivity，那样要在AndroidManifest Activity里面使用AppCompat的theme,如android:theme="@style/Theme.AppCompat.Light.NoActionBar"
使用android:theme="@style/Theme.AppCompat.Light.NoActionBar"的话，转圈显示绿色
继承BaseActivity也可以
 */

public class ZyMeetingRoomViewActivity extends BaseActivity implements OnClickListener, OnItemClickListener<Image>, OnRefreshListener, OnLoadMoreListener {

    private View includeTitle;
    private View includeData;
    private IRecyclerView iRecyclerView;
    private View headerView = null;
    private View childrenView = null;
    private View cv = null;
    private LoadMoreFooterView loadMoreFooterView;

    private FrameLayout frameLayout=null;

    private baseRecyleViewAdapter mAdapter;

    private int mPage;

    private int currentPage = 1;
    private List<SysPassValue> dataList = new ArrayList<SysPassValue>();//数据

    public ProgersssDialog dialog;

    private long userId = 0;
    private String userCode = "";
    private String userName = "";
    private String userRole = "";

    private String id = "";
    private String name = "";

    private String channelId = "1";
    private String channelName = "会议室详情";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        showProgressDialog();

        setContentView(R.layout.activity_zy_common_apply_detail);
        includeTitle = this.findViewById(R.id.zy_util_head_news_list);
        //includeData = this.findViewById(R.id.xl_huodong_detail_record_pullrefreshrecycleView);

        id = this.getIntent().getStringExtra("id");
        channelId = this.getIntent().getStringExtra("channelId");
        if(channelId==null){
            channelId = "1";
        }
        if(channelId.equals("1")){
            channelName = "会议室介绍";
        }else if(channelId.equals("2")){
            channelName = "场地介绍";
        }
        //Log.i("luktel","id"+id);

        //channelId = this.getIntent().getStringExtra("channelId");
        //Log.i("luktel","channelName"+channelName);

        ((TextView) includeTitle.findViewById(R.id.wg_head_title)).setText(channelName);
        Button btnReturn = (Button) includeTitle.findViewById(R.id.util_head_btn_return);
        btnReturn.setOnClickListener(this);

        //iRecyclerView = (IRecyclerView) includeData.findViewById(R.id.system_irecycleview);
        iRecyclerView = (IRecyclerView) findViewById(R.id.zy_common_recyclerview);
        iRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        //toggleRefreshHeader();//切换上拉刷新面板

        //bannerView = (BannerView) LayoutInflater.from(this).inflate(R.layout.layout_irecyclerview_banner_view, iRecyclerView.getHeaderContainer(), false);
        //iRecyclerView.addHeaderView(bannerView);

        //headerView = View.inflate(this, R.layout.item_wg_event_detail, null);
        //headerView = LayoutInflater.from(this).inflate(R.layout.item_wg_event_detail, null);
        headerView =  LayoutInflater.from(this).inflate(R.layout.activity_zy_meetingroom_detail_inlayout, iRecyclerView.getHeaderContainer(), false);//加入面板
        iRecyclerView.addHeaderView(headerView);

        //frameLayout = new FrameLayout(this);
        //iRecyclerView.addHeaderView(frameLayout);

        //headerView2 = View.inflate(this, R.layout.item_wg_task_header, null);
        //iRecyclerView.addHeaderView(headerView2);

        Button btshop = (Button) this.findViewById(R.id.bt_detail_apply);
        btshop.setOnClickListener(this);

        //解决卡顿问题
        iRecyclerView.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
        iRecyclerView.setFocusable(true);
        iRecyclerView.setFocusableInTouchMode(true);
        iRecyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                view.requestFocusFromTouch();
                return false;
            }
        });

        loadMoreFooterView = (LoadMoreFooterView) iRecyclerView.getLoadMoreFooterView();

        mAdapter = new baseRecyleViewAdapter(this,dataList,"item_imagelist");//通用adapter，如果加入了headerView，自动加载到headerView下面
        //mAdapter.setOnClick1(this);
        iRecyclerView.setIAdapter(mAdapter);

        iRecyclerView.setOnRefreshListener(this);
        iRecyclerView.setOnLoadMoreListener(this);

        //cv = getWindow().getDecorView();

        getDetailData();//头部信息
        onRefresh();//列表信息代替iRecyclerView.setRefreshing(true);

        //mAdapter.setOnItemClickListener(this);

        /*iRecyclerView.post(new Runnable() {
            @Override
            public void run() {
                iRecyclerView.setRefreshing(true);
            }
        });*/

        userId = PreferenceUtils.getLong(this, "userid", -1);
        userRole = PreferenceUtils.getString(this,"userrole","");
    }

    public void showProgressDialog(){
        dialog = new ProgersssDialog(this);
        //dialog.show();
    }

    @Override
    public void onClick(View view) {

        if (R.id.util_head_btn_return == view.getId()) {
            this.finish();
            overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
            return;
        }

        if (R.id.bt_detail_apply == view.getId()) {
            Intent intent = new Intent(view.getContext(), ZyMeetingRoomApplyActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("id", id);
            bundle.putString("name", name);
            bundle.putString("channelId", channelId);
            intent.putExtras(bundle);
            startActivity(intent);
            overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
            return;
        }

        /*if(R.id.item_zy_newslist_btn==view.getId()){

            //传递参数过去
            Object tag = view.getTag(R.id.button_tag1);

            if (tag != null && tag instanceof Integer) { //解决问题：如何知道你点击的按钮是哪一个列表项中的，通过Tag的position
                //ToastUtils.show(v.getContext(), "22详情  NULL");
                int position = (Integer) tag;
                String ids =  dataList.get(position).getId();
                //Toast.makeText(v.getContext(), "ids"+ids, Toast.LENGTH_SHORT).show();
                //Toast.makeText(v.getContext(), "开发中，敬请期待！", Toast.LENGTH_SHORT).show();
                //return;
                Intent intent = new Intent(view.getContext(), ZyNewsViewActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("id", ids);
                intent.putExtras(bundle);
                startActivity(intent);
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                return;
            }
        }*/
    }

    @Override
    public void onItemClick(int position, Image image, View v) {
        //Toast.makeText(this, String.valueOf(position), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRefresh() {
        //loadBanner();
        loadMoreFooterView.setStatus(LoadMoreFooterView.Status.GONE);
        currentPage = 1;
        getDetailData();
        getListData();
    }

    @Override
    public void onLoadMore() {
        if (loadMoreFooterView.canLoadMore() && mAdapter.getItemCount() > 0) {
            loadMoreFooterView.setStatus(LoadMoreFooterView.Status.LOADING);
            getDetailData();
            getListData();
        }
    }

    public void getDetailData() {
        SysPassValue temp = new SysPassValue();
        temp.setFunctionName("getMeetingRoomDetail");
        temp.setId(String.valueOf(id));
        temp.setUserId(String.valueOf(userId));

        new sendOrderToServerForValueYTask(new AbstractYGetTaskListener<SysPassValue>() {
            @Override
            public void onPostExecute(String name, YGetTask<SysPassValue> task) {
                showData(task.getValue());
                dialog.dismiss();
            }
        },temp).execute();
    }

    private void showData(SysPassValue passValue) {
        if (passValue==null) {
            ToastUtils.show(this,"没有获取到信息");
            return;
        } else {
			 /*((TextView)includeMaster.findViewById(R.id.tv_masterdetail_head_image_shopname)).setText(masterviewdetail.getArg1());
			 AndroidUtils.setWebImageView(includeMaster, R.id.hh_masterdetail_head_image_shoplogimage, masterviewdetail.getArg2());
			 AndroidUtils.setWebImageView(includeMaster, R.id.hh_masterdetail_head_image_masterimage, masterviewdetail.getArg3());*/

			 String priceShow = "";
             String sid =  passValue.getId();
             String arg1 =  passValue.getArg1();
             String arg2 =  passValue.getArg2();
             String arg3 =  passValue.getArg3();
             String arg4 =  passValue.getArg4();
             String arg5 =  passValue.getArg5();
             String arg6 =  passValue.getArg6();
             String arg7 =  passValue.getArg7();
             String arg8 =  passValue.getArg8();
             String arg9 =  passValue.getArg9();
             String arg10 =  passValue.getArg10();
             String arg11 =  passValue.getArg11();
             String arg12 =  passValue.getArg12();
             String arg38 =  passValue.getArg38();
             String arg41 =  passValue.getArg41();
             String arg43 =  passValue.getArg43();
             String arg50 =  passValue.getArg50();
             //ToastUtils.show(this,"arg38"+arg38);
            if(arg7.equals("Y")) {
                arg7 = "免费";
                priceShow = "免费";
            }else{
                arg7 = "收费";
                priceShow = "￥"+arg41+"/"+arg4+"分钟";
            }

             name = arg1;

             View headInclude = headerView.findViewById(R.id.zy_include_top_picture);

             AndroidUtils.setWebImageView(headInclude, R.id.zy_top_picture, arg38);

             AndroidUtils.setTextView(headerView, R.id.wg_contactdetail_textview2,arg1);
             AndroidUtils.setTextView(headerView, R.id.wg_contactdetail_textview3,arg2+arg3+"层");
             AndroidUtils.setTextView(headerView, R.id.wg_contactdetail_textview4,arg7);
             AndroidUtils.setTextView(headerView, R.id.wg_contactdetail_textview5,priceShow);
             AndroidUtils.setTextView(headerView, R.id.wg_contactdetail_textview6,arg11+"至"+arg12);
             AndroidUtils.setTextView(headerView, R.id.wg_contactdetail_textview7,arg50);
             AndroidUtils.setTextView(headerView, R.id.wg_contactdetail_textview8,arg43);
             //AndroidUtils.setWebImageView(this, R.id.wg_contact_round_img, arg1);

             /*AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout2_textview,arg1);
             AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout3_textview,arg2);
             AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout4_textview,arg3);
             AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout5_textview,arg4);
             AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout6_textview,arg5);
             AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout7_textview,arg6);
             AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout8_textview,arg7);
             AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout11_textview,arg8);
             AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout9_textview,arg9);
             AndroidUtils.setTextView(this, R.id.wg_taskdetail_layout10_textview,arg10);*/

             //((ImageView)includeTitle.findViewById(R.id.iv_hh_head_masterdetail_right_saygoods)).setBackgroundResource(R.drawable.praisetech);
        }
    }

    public void getListData() {
        SysPassValue temp = new SysPassValue();
        temp.setFunctionName("getImageList");
        temp.setPageNumber(String.valueOf(this.currentPage));
        temp.setArg1("1041");
        temp.setArg2(String.valueOf(id));
        temp.setUserId(String.valueOf(userId));

        new sendOrderToServerForListYTask(new AbstractYGetTaskListener<List<SysPassValue>>() {
            @Override
            public void onPostExecute(String name, YGetTask<List<SysPassValue>> task) {
                iRecyclerView.setRefreshing(false);
                showListData(task.getValue());
                dialog.dismiss();
            }
        }, temp).execute();
    }

    public void showListData(final List<SysPassValue> list) {

        //View vnoRecord = this.findViewById(R.id.activity_no_record_showpic);
        if (list == null) {
            /*loadMoreFooterView.setStatus(LoadMoreFooterView.Status.ERROR);*///不用列表数据所以隐藏状态
            //Toast.makeText(this, "网络连接失败", Toast.LENGTH_SHORT).show();

            if (currentPage == 1) {
                this.dataList.clear();
                mAdapter.notifyDataSetChanged();
                //vnoRecord.setVisibility(View.INVISIBLE);
                return;
            }
            return;
        }

        if (list.size() == 0) {
            if (currentPage == 1) {
                this.dataList.clear();
                mAdapter.notifyDataSetChanged();
                //vnoRecord.setVisibility(View.VISIBLE);//如果是第一页没有数据则显示无记录
            } else {
                loadMoreFooterView.setStatus(LoadMoreFooterView.Status.THE_END);
            }
            //Toast.makeText(this, "已经到了最后一页", Toast.LENGTH_SHORT).show();
        } else {
            if (currentPage == 1) {
                currentPage = 2;
                this.dataList.clear();
                this.dataList.addAll(list);
                //vnoRecord.setVisibility(View.GONE);
            } else {

                loadMoreFooterView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        currentPage++;
                        loadMoreFooterView.setStatus(LoadMoreFooterView.Status.GONE);
                        dataList.addAll(list);
                    }
                }, 1000);

                //loadMoreFooterView.setStatus(LoadMoreFooterView.Status.GONE);
                //dataList.addAll(list);
            }
            mAdapter.notifyDataSetChanged();
        }
        //vnoRecord.setVisibility(View.GONE);

        //如果第一页则需要清除数据

        if (dataList.size() == 0) {
            //vnoRecord.setVisibility(View.VISIBLE);
        }

        //Log.d("luktel", "showData");

    }


}
