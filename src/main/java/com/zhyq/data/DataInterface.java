package com.zhyq.data;
 
 
 
import java.util.List;
import com.zhyq.model.HhSysArgument;
import com.zhyq.model.User;
import com.zhyq.model.CommonData;
import com.zhyq.model.CommonList;
import com.zhyq.model.SysPassValue;

/*
 *  定义数据接口
*/
public interface DataInterface {
	
    //得到系统参数
	public  HhSysArgument  getHhSysArgument() throws Throwable;
	
	//通用函数 读取服务器数据  一个是 读字符 ，一个是读数据列表记录
	public SysPassValue sendOrderToServerForValue(SysPassValue curObject) throws Throwable;
	public List<SysPassValue> sendOrderToServerForList(SysPassValue curObject) throws Throwable;
	
	public User userLogin(User user) throws Throwable;
	public void userLogout(long id) throws Throwable;
	
	public List<CommonList> getTaskList(String userId,String masterLevel,int curpaper) throws Throwable;
	public CommonData getTaskDetail(String masterid,String userid) throws Throwable;
	
}