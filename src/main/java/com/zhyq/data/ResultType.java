package com.zhyq.data;

import java.lang.reflect.Type;
import java.util.List;

import com.google.gson.reflect.TypeToken;

import com.zhyq.libs.Result;

import com.zhyq.model.User;
import com.zhyq.model.CommonList;
import com.zhyq.model.CommonData;
import com.zhyq.model.HhSysArgument;
import com.zhyq.model.SysPassValue;

//定义 数据类型  

public class ResultType {
	
	//注意，所有的从服务器中读取数据都用下面的 类来读取
	public final static Type SysPassValue = new TypeToken<Result<SysPassValue>>(){}.getType();
	public final static Type SysPassValueList = new TypeToken<Result<List<SysPassValue>>>(){}.getType();
	
	public final static Type HhSysArgument=new TypeToken<Result<HhSysArgument>>(){}.getType();
	
	public final static Type User =new TypeToken<Result<User>>(){}.getType();
	
	public final static Type CommonList=new TypeToken<Result<List<CommonList>>>(){}.getType();
	//public final static Type CommonData=new TypeToken<Result<List<CommonData>>>(){}.getType();
	public final static Type CommonData =new TypeToken<Result<CommonData>>(){}.getType();
	
	public final static Type stringList=new TypeToken<Result<List<String>>>(){}.getType();
	
}