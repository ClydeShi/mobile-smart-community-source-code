package com.zhyq.data;

import java.util.List;
 
import com.zhyq.model.User;

import com.zhyq.libs.HttpRequest;
import com.zhyq.libs.Result;
import com.zhyq.libs.JsonUtils;
import com.zhyq.model.CommonData;
import com.zhyq.model.CommonList;
import com.zhyq.model.HhSysArgument;
import com.zhyq.model.SysPassValue;

/**
 * 此文件连接服务器，com.huahua.server.servelt.ClientApiServlet，调用com.huahua.server.data.DemoDataInterface
 * @author Shawn
 *
 */

public class ClientDataInterface implements DataInterface {
	//private static final String URL="http://test666.luktel.com/clientapi";
	private static final String URL="http://139.9.1.194:82/zhyq/clientapi";
	
	private void checkResult(Result r) throws Throwable {
		if (!r.isResult()) {
			throw new Exception(r.getError());
		}
	}

	//得到一串字符
	@Override
	public SysPassValue sendOrderToServerForValue(SysPassValue curObject)
			throws Throwable {
		String json=JsonUtils.toJson(curObject);
		Result<SysPassValue> r=HttpRequest.requestPost(URL, ResultType.SysPassValue, "method","sendOrderToServerForValue","curObject",json);
		checkResult(r);
		return r.getData();
	}
	
	//得到数据集合
	@Override
	public List<SysPassValue> sendOrderToServerForList(
			SysPassValue temp) throws Throwable {
		
		String json=JsonUtils.toJson(temp);
		Result<List<SysPassValue>> r=HttpRequest.requestPost(URL, ResultType.SysPassValueList, "method","sendOrderToServerForList","curObject",json);
		checkResult(r);
		return r.getData();
	}

	//得到系统参数  
	@Override
	public HhSysArgument getHhSysArgument() throws Throwable {
		Result<HhSysArgument> r = HttpRequest.requestPost(URL, ResultType.HhSysArgument, "method", "getHhSysArgument");
		checkResult(r);
		return r.getData();
	}

	//用户登入 18
	@Override
	public User userLogin(User user) throws Throwable {
		
		String json=JsonUtils.toJson(user);
		Result<User> r=HttpRequest.requestPost(URL, ResultType.User, "method","userLogin","user",json);
		checkResult(r);
		return r.getData();
	}
	
	public void userLogout(long id) throws Throwable {
		Result r=HttpRequest.requestPost(URL, Result.class, "method","userLogout","id",String.valueOf(id));
		checkResult(r);
	}

	@Override
	public List<CommonList> getTaskList(String userId,String shopType, int curpaper)
			throws Throwable {
		Result< List<CommonList>>   r=HttpRequest.requestPost(URL, ResultType.CommonList, "method","getTaskList","userId",userId,"shopType",shopType,"curpaper",String.valueOf(curpaper) );
		checkResult(r);
		return r.getData();
	}
	
	public CommonData getTaskDetail(String taskid,String userid) throws Throwable {
		Result<CommonData>   r=HttpRequest.requestPost(URL, ResultType.CommonData, "method","getTaskDetail","taskid",taskid,"userid",userid);
		checkResult(r);
		return r.getData();
	}
	
	
	
}
