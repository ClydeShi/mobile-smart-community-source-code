package com.zhyq.data;

import java.sql.ResultSet;

import com.alreadybird.project.DBUtil;
 

/* 测试数据  数据接口的是是实现 */

import java.util.ArrayList;
import java.util.List;

import com.zhyq.model.CommonList;
import com.zhyq.model.HhSysArgument;
import com.zhyq.model.User;

import com.zhyq.model.CommonData;
import com.zhyq.model.SysPassValue;

/*
 *   这个用来 模拟数据从服务器返回过来，测试的时候直接用 数组来返回值给客户端
 */

public class DemoDataInterface implements DataInterface {
	private DBUtil   mdb = null;
	private ResultSet rs = null;
	private String   sql = "";
	private String  weburl ="http://www.zb388.com/haigangcheng/";

	public User userLogin(User user) throws Throwable {
		if ("test".equals(user.getName()) || "1".equals(user.getPassword())|| (100 ==user.getId())) {
			user.setId(100L);   //其实是 100   L
			user.setName("曾先进");
			user.setUserSex("男");
			user.setCode("test");
			user.setUserType("1001");//1100
			user.setUserRole("");
			user.setProvince("广东省");
			user.setCity("中山市");
			user.setCounty("西区");
			user.setuserbirthday("1980-10-29");
			user.setdetailAddress("柏景台1栋23楼D座");
			user.setnetpic("http://test1.zb388.com/upload/face/13923344901.jpg");
			return user;
		} else {
			return null;
		}
	}
	
	//获取数据全部使用下面的关联
	//用来读取字符串命令
	//读取增加修改删除结果
	@Override
	public SysPassValue sendOrderToServerForValue(SysPassValue curObject) throws Throwable {
		
		//重要的是返回这个数据
		SysPassValue dbResult = new SysPassValue();

		// 函数名称  页码数量  其他参数根据不同的 功能名称 进行读取
		String funName    = curObject.getFunctionName();
		String pageNumber = curObject.getPageNumber();
		
		
		if(funName.equals("saveFeedback")){
			
			String id = curObject.getId();
			String userId = curObject.getUserId();
			String arg2 = curObject.getArg2();
			String arg3 = curObject.getArg3();
			String arg4 = curObject.getArg4();
			String arg5 = curObject.getArg5();
			String arg6 = curObject.getArg6();
			String arg7 = curObject.getArg7();
			String arg8 = curObject.getArg8();
			String arg9 = curObject.getArg9();
			String arg10 = curObject.getArg10();
			String arg11 = curObject.getArg11();
			dbResult.setReturnResult("1");
			return dbResult;
			
		}
		
		if(funName.equals("getContactDetail")){
			
			String id = curObject.getId();
			String userId = curObject.getUserId();
			
			dbResult.setId(id);
			dbResult.setArg1("http://test1.zb388.com/upload/face/13923344901.jpg");
			dbResult.setArg2("杨先生");
			dbResult.setArg3("18676167959");
			dbResult.setArg4("13715337715");
			dbResult.setArg5("0755-66666789");
			dbResult.setArg6("93633496");
			dbResult.setArg7("93633496");
			dbResult.setArg8("广东省深圳市福田区上沙");
			dbResult.setReturnResult("1");
			return dbResult;
			
		}
		
		if(funName.equals("")){
			
		}
		
		return null;
	}

	//这个是用来接收 数据结果集合
	@Override
	public List<SysPassValue> sendOrderToServerForList(SysPassValue curObject) throws Throwable {
		
		List<SysPassValue> list=new ArrayList<SysPassValue>();
		
	    // 函数名称  页码数量  其他参数根据不同的 功能名称 进行读取
		String funName    = curObject.getFunctionName();    //函数名称
		String pageNumber = curObject.getPageNumber();     //页码数量
		
		if(funName.equals("getPlanList")){
			  
			 
			String userid      = curObject.getArg1();   //用户内部ID  传入参数，注意 ,主消息
			String sbrandType  = curObject.getArg3();   //分类
			String type  = curObject.getArg2();   //栏目
			
			//得到的结果如下
			
			if(type.equals("未执行")){
				
				if(pageNumber.equals("1")){
					
					for(int i=1;i<=60;i++){
						SysPassValue valueObject1i = new SysPassValue();
						valueObject1i.setId(""+i);
						valueObject1i.setArg1("张书记检查任务情况"+i);
						valueObject1i.setArg2("检查社区采集员情况，即时解决问题");
						valueObject1i.setArg3("日常计划");
						valueObject1i.setArg4("龙华街道");
						list.add(valueObject1i);
					}
					
				}
				
				if(pageNumber.equals("2")){
					
					for(int i=100;i<=120;i++){
						SysPassValue valueObject1i = new SysPassValue();
						valueObject1i.setId(""+i);
						valueObject1i.setArg1("第二页张书记检查任务情况"+i);
						valueObject1i.setArg2("检查社区采集员情况，即时解决问题");
						valueObject1i.setArg3("日常计划");
						valueObject1i.setArg4("坂田街道");
						list.add(valueObject1i);
					}
					
				}
				
			}
			
			if(type.equals("执行中")){
				
				if(pageNumber.equals("1")){
					
					for(int i=1;i<=30;i++){
						SysPassValue valueObject1i = new SysPassValue();
						valueObject1i.setId(""+i);
						valueObject1i.setArg1("张书记检查任务情况"+i);
						valueObject1i.setArg2("检查社区采集员情况，即时解决问题");
						valueObject1i.setArg3("日常计划");
						valueObject1i.setArg4("龙华街道");
						list.add(valueObject1i);
					}
					
					SysPassValue valueObject1 = new SysPassValue();
					valueObject1.setId("1");
					valueObject1.setArg1("张书记检查任务情况2月3日");
					valueObject1.setArg2("检查社区采集员情况，即时解决问题");
					valueObject1.setArg3("日常计划");
					valueObject1.setArg4("龙华街道");
					list.add(valueObject1);
					
					SysPassValue valueObject2 = new SysPassValue();
					valueObject2.setId("2");
					valueObject2.setArg1("孙书记检查任务情况2月4日");
					valueObject2.setArg2("检查社区采集员情况，即时解决问题");
					valueObject2.setArg3("日常计划");
					valueObject2.setArg4("福城街道");
					list.add(valueObject2);
					
				}
				
				
				
			}
			
			if(type.equals("已完成")){
				
				if(pageNumber.equals("1")){
					
					for(int i=1;i<=30;i++){
						SysPassValue valueObject1i = new SysPassValue();
						valueObject1i.setId(""+i);
						valueObject1i.setArg1("张书记检查任务情况"+i);
						valueObject1i.setArg2("检查社区采集员情况，即时解决问题");
						valueObject1i.setArg3("日常计划");
						valueObject1i.setArg4("龙华街道");
						list.add(valueObject1i);
					}
					
				}
				
				
				
			}
			
			return list;
		}
		
		if(funName.equals("getFeedbackList")){
			  
			 
			String userid      = curObject.getArg1();
			String type  = curObject.getArg2();
			
			//得到的结果如下
			
			if(pageNumber.equals("1")){
				
				for(int i=1;i<=60;i++){
					SysPassValue valueObject1i = new SysPassValue();
					valueObject1i.setId(""+i);
					valueObject1i.setArg1("小张检查反馈情况"+i);
					valueObject1i.setArg2("检查社区采集员情况，即时解决问题");
					valueObject1i.setArg3("日常计划");
					valueObject1i.setArg4("龙华街道");
					list.add(valueObject1i);
				}
				
			}
			
			if(pageNumber.equals("2")){
				
				for(int i=100;i<=120;i++){
					SysPassValue valueObject1i = new SysPassValue();
					valueObject1i.setId(""+i);
					valueObject1i.setArg1("第二页张书记检查反馈情况"+i);
					valueObject1i.setArg2("检查社区采集员情况，即时解决问题");
					valueObject1i.setArg3("日常计划");
					valueObject1i.setArg4("坂田街道");
					list.add(valueObject1i);
				}
				
			}
			
			if(pageNumber.equals("3")){
				
				for(int i=200;i<=220;i++){
					SysPassValue valueObject1i = new SysPassValue();
					valueObject1i.setId(""+i);
					valueObject1i.setArg1("第三页张书记检查反馈情况"+i);
					valueObject1i.setArg2("检查社区采集员情况，即时解决问题");
					valueObject1i.setArg3("日常计划");
					valueObject1i.setArg4("坂田街道");
					list.add(valueObject1i);
				}
				
			}
			
			return list;
		}
		
		if(funName.equals("getContactList")){
			  
			 
			String userid      = curObject.getArg1();
			String keywords  = curObject.getArg2();
			
			//得到的结果如下
			
			if(pageNumber.equals("1")){
				
				if(keywords.equals("张")){
					SysPassValue valueObject = new SysPassValue();
					valueObject.setId("01");
					valueObject.setArg1("小张");
					valueObject.setArg2("18676167959");
					valueObject.setArg3("0755-66668888");
					valueObject.setArg4("93633496@qq.com");
					valueObject.setArg5("http://test1.zb388.com/upload/face/13923344901.jpg");
					list.add(valueObject);
				}else if(keywords.equals("刘")){
					SysPassValue valueObject = new SysPassValue();
					valueObject.setId("01");
					valueObject.setArg1("小张");
					valueObject.setArg2("13712345678");
					valueObject.setArg3("0755-66668888");
					valueObject.setArg4("93633496@qq.com");
					valueObject.setArg5("http://test1.zb388.com/upload/face/13923344901.jpg");
					list.add(valueObject);
				}else{
					for(int i=1;i<=20;i++){
						SysPassValue valueObject1i = new SysPassValue();
						valueObject1i.setId(""+i);
						valueObject1i.setArg1("小美"+i);
						valueObject1i.setArg2("18676167959");
						valueObject1i.setArg3("0755-66668888");
						valueObject1i.setArg4("93633496@qq.com");
						valueObject1i.setArg5("http://test1.zb388.com/upload/face/13923344901.jpg");
						list.add(valueObject1i);
					}
				}
				
				
			}
			
			if(pageNumber.equals("2")){
				
				for(int i=100;i<=120;i++){
					SysPassValue valueObject1i = new SysPassValue();
					valueObject1i.setId(""+i);
					valueObject1i.setArg1("刘飞龙"+i);
					valueObject1i.setArg2("18676167959");
					valueObject1i.setArg3("0755-66668888");
					valueObject1i.setArg4("93633496@qq.com");
					valueObject1i.setArg5("http://test1.zb388.com/upload/face/13923344901.jpg");
					list.add(valueObject1i);
				}
				
			}
			
			if(pageNumber.equals("3")){
				
				for(int i=200;i<=220;i++){
					SysPassValue valueObject1i = new SysPassValue();
					valueObject1i.setId(""+i);
					valueObject1i.setArg1("张晓娜"+i);
					valueObject1i.setArg2("18676167959");
					valueObject1i.setArg3("0755-66668888");
					valueObject1i.setArg4("93633496@qq.com");
					valueObject1i.setArg5("http://test1.zb388.com/upload/face/13923344901.jpg");
					list.add(valueObject1i);
				}
				
			}
			
			return list;
		}
		
		
		return null;
	}
		
		
	/*
	 * 读取 花花系统的 系统参数*
	 */
	@Override
	public HhSysArgument  getHhSysArgument() throws Throwable {
		
		//得到系统的图片
		HhSysArgument  DataObject=new HhSysArgument(1l,"sysname","",
				"http://test1.zb388.com/huahuajpg/selectshopflash.png","shop","1",
				"http://test1.zb388.com/huahuajpg/selectshopflash.png","shop","1",
				"http://test1.zb388.com/huahuajpg/serverflash1.png","shop","1", 
				"http://test1.zb388.com/huahuajpg/serverflash1.png","shop","1",
				"http://test1.zb388.com/huahuajpg/serverflash1.png","shop","1",
				"早班(自定)","中班","晚班",8.00,13.00,18.00);
		
		 return DataObject;
	}
	
	public List<String> getHhZhongShanAreaList() throws Throwable {
		
		List<String> listimage=new ArrayList<String>();
		listimage.add(new String("东区"));
		listimage.add(new String("南区"));
		listimage.add(new String("西区"));
		listimage.add(new String("北区"));  
		return listimage;
	}
	
	public List<CommonList> getTaskList(String userId,String type, int pageNumber) throws Throwable {
		
		List<CommonList> list =new ArrayList<CommonList>();
		
		if(type.equals("未执行")){
			
			if(pageNumber==1){
				
				for(int i=1;i<=30;i++){
					list.add(new CommonList(Long.valueOf(i),"刘书记检查任务情况"+i,"检查社区采集员情况，即时解决问题","日常计划","龙华街道","","","","","",""));
				}
				
			}
			
			if(pageNumber==2){
				
				for(int i=100;i<=130;i++){
					list.add(new CommonList(Long.valueOf(i),"刘书记检查任务情况"+i,"检查社区采集员情况，即时解决问题","日常计划","龙华街道","","","","","",""));
				}
				
			}
			
		}
		
		if(type.equals("执行中")){
			
			if(pageNumber==1){
				
				for(int i=1;i<=30;i++){
					list.add(new CommonList(Long.valueOf(i),"刘书记检查任务情况"+i,"检查社区采集员情况，即时解决问题","日常计划","龙华街道","","","","","",""));
				}
				
			}
			
			if(pageNumber==2){
				
				for(int i=100;i<=130;i++){
					list.add(new CommonList(Long.valueOf(i),"刘书记检查任务情况"+i,"检查社区采集员情况，即时解决问题","日常计划","龙华街道","","","","","",""));
				}
				
			}
			
		}
		
		if(type.equals("已完成")){
			
			if(pageNumber==1){
				
				for(int i=1;i<=30;i++){
					list.add(new CommonList(Long.valueOf(i),"刘书记检查任务情况"+i,"检查社区采集员情况，即时解决问题","日常计划","龙华街道","","","","","",""));
				}
				
			}
			
			if(pageNumber==2){
				
				for(int i=100;i<=130;i++){
					list.add(new CommonList(Long.valueOf(i),"刘书记检查任务情况"+i,"检查社区采集员情况，即时解决问题","日常计划","龙华街道","","","","","",""));
				}
				
			}
			
		}
		
		
		return list;
	}
	
	public CommonData getTaskDetail(String taskid,String userid) throws Throwable {
		// TODO Auto-generated method stub
		CommonData taskView = new CommonData();
		taskView.setId(Long.valueOf(taskid));//taskView.setId(Long.parseLong(taskid));
		taskView.setVarValue1("");
		taskView.setVarValue2("认真工作");
		taskView.setVarValue3("龙华街道");
		taskView.setVarValue4("F20");
		taskView.setVarValue5("杨美华");
		taskView.setVarValue6("曾先进");
		taskView.setVarValue7("认真工作");
		taskView.setVarValue8("认真");
		taskView.setVarValue9("2017-03-10");
		taskView.setVarValue10("2017-03-11");
		taskView.setVarValue21("已检查");
		taskView.setVarValue22("已检查");
		taskView.setVarValue23("已检查");
		taskView.setVarValue24("6");
		taskView.setVarValue25("6");
		taskView.setVarValue26("6");
		taskView.setVarValue27("6");
		taskView.setVarValue28("6");
		taskView.setVarValue29("6");
		taskView.setVarValue30("6");
		
		return taskView;
	}
	
	public void userLogout(long id) throws Throwable {
		// TODO Auto-generated method stub
		
	}
	
	
}
