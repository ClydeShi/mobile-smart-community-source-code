package com.zhyq.data;

/*直接对外 的API 函数 ，调用 */

import java.util.List;

import com.zhyq.model.HhSysArgument;
import com.zhyq.model.User;
import com.zhyq.model.CommonData;
import com.zhyq.model.CommonList;
import com.zhyq.model.SysPassValue;

/*
*
获取数据的类里调用HhGetShopMasterListYTask，通过DataAPI.getHhShopMasterList(userid,dttype,curpager)获取数据，DataAPI决定是调用服务器数据还是本地数据，ClientDataInterface是服务器数据，DemoDataInterface是本地数据，ClientDataInterface通过HttpRequest调用服务器数据。
1. 类里调用方法获取数据
2. DataAPI定义方法
3. 本地使用DemoDataInterface里面定义方法返回类的对象
4. 远程使用ClientDataInterface，带参数连接服务器，com.huahua.server.servelt.ClientApiServlet，ClientApiServlet通过判断参数，分别调用com.huahua.server.data.DemoDataInterface里的方法，并返回对象
*/

public class DataAPI {
	//下面指向的是服务器数据
	private static final DataInterface d = new ClientDataInterface();
	
	//下面指向的是本地的数据，使用本文件连接数据
	//private static final DataInterface d = new DemoDataInterface();
	
	public static SysPassValue sendOrderToServerForValue(SysPassValue curObject) throws Throwable {
		return d.sendOrderToServerForValue(curObject);
	}
	
	public static List<SysPassValue> sendOrderToServerForList(SysPassValue curObject) throws Throwable {
		return d.sendOrderToServerForList(curObject);
	}
	
	//得到系统参数信息
	public static HhSysArgument getHhSysArgument() throws Throwable {
		return d.getHhSysArgument();
	}

	public static User userLogin(User user) throws Throwable {
		return d.userLogin(user);
	}
	
	public static void userLogout(long id) throws Throwable {
		d.userLogout(id);
	}
	
	public static List<CommonList> getTaskList(String userId,String shopType,
			int curpaper)
			throws Throwable {
		return d.getTaskList(userId,shopType,curpaper);
	}
	
	public static CommonData getTaskDetail(String taskid,String userid)
			throws Throwable {
		return d.getTaskDetail(taskid,userid);
	}
}
