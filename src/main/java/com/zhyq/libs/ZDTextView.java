package com.zhyq.libs;

//com.unitesoft.libs.ZDTextView

import android.content.Context;  
import android.content.res.TypedArray;
 
import android.util.AttributeSet;
import android.widget.TextView;
import com.zhyq.R;

public class ZDTextView extends TextView   { 
	
	private static final String TAG = ZDTextView.class.getSimpleName();

    
	 
 
	public String sizeSpec ="8";  //扩充字的大小 
		
	 public ZDTextView(Context context,AttributeSet attrs, int defStyle){ 
		 super(context, attrs, defStyle); 
		 
		 
		 
		 
		 
	 } 
	 public ZDTextView(Context context, AttributeSet attrs){ 
		 super(context, attrs);
		 
		 TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.DesignTextSize);

		 int textSize = ta.getInteger(R.styleable.DesignTextSize_text_size,10);
         //int textAttr = ta.getInteger(R.styleable.test_text, -1);

         //Log.e(TAG, "text = " + text  );
         
         
         //this.setTextSize(18);
		  

         
         AndroidUtils.setTextSizeByPx(this,640,textSize);
         
         
         ta.recycle();

		 
		 
	 }
	 
	 
	 public ZDTextView(Context context){
		 super(context);
		 } 
 
 
 
}
