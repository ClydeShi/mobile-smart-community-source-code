package com.zhyq.libs;
import com.google.gson.Gson;
import java.lang.reflect.Type;
import org.json.JSONObject;
/*
  *  ���� jar �ķ��� �� �����ڹ��̵�Ŀ¼�� ���� libs Ȼ���룬ѡ�ļ� ��Ȼ���ڹ������������ü�JAR��ѡ�� libs
  *  ) toJson() �C ת��java ����JSON2) fromJson() �C ת��JSON��java����
  *  
*/

public class JsonUtils {
	private final static Gson gson=new Gson();
	public static <T> T toObject(String json,Type t) {
		return gson.fromJson(json, t);
	}
	public static <T> T toObject(String json,Class<T> c) {
		return gson.fromJson(json, c);
	}
	public static JSONObject toJson(String json) throws Exception {
		JSONObject data = new JSONObject(json);
		return data;
	}
	public static String toJson(Object obj) {
		return gson.toJson(obj);
	}
}