package com.zhyq.libs;

//com.unitesoft.libs.ZDTextView

import android.content.Context;  
import android.content.res.TypedArray;
import android.graphics.Canvas;
 
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.zhyq.R;

public class ZDLinearLayout extends LinearLayout { 
	
	private static final String TAG = ZDLinearLayout.class.getSimpleName();
	private Context mContext;

	private int marginleft ; //设计的参数
	private int marginright ;
	private int margintop ;
	private int marginbottom ;
	  
	private int width ;    //设计的参数
	private int height ;   //设计的参数
	private String dsuplayouttype; 
	 
     
		
	 public ZDLinearLayout(Context context,AttributeSet attrs, int defStyle){ 
		 super(context, attrs, defStyle); 
		
		  
	 } 
	 public ZDLinearLayout(Context context, AttributeSet attrs){ 
		 super(context, attrs);
		 mContext = context;
		 
		 //this.setBackgroundResource(R.drawable.bg_test_rect);
		 
		 getCustomAttributes(attrs);
		  
         
	 } 
	 
	 
	 
	 
	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas); 
		
		//一定要增加背景边框  android:background="@drawable/bg_test_rect" 
		
		AndroidUtils.setLinearLayoutSizeBypx(this,dsuplayouttype,640,width,height,marginleft,marginright,margintop,marginbottom);
        
		
		
	}
	/**
	     * 获得自定义控件属性值
	     *
	     * @param attrs
	*/
	    private void getCustomAttributes(AttributeSet attrs) {
	    	
	    	
	    	TypedArray ta = mContext.obtainStyledAttributes(attrs, R.styleable.DesignLinearLayoutSize);

			marginleft = ta.getInteger(R.styleable.DesignLinearLayoutSize_dsmarginLeft,0);
			marginright = ta.getInteger(R.styleable.DesignLinearLayoutSize_dsmarginRight,0);
			margintop = ta.getInteger(R.styleable.DesignLinearLayoutSize_dsmarginTop,0);
			marginbottom = ta.getInteger(R.styleable.DesignLinearLayoutSize_dsmarginBottom,0);
			 
			width  = ta.getInteger(R.styleable.DesignLinearLayoutSize_dswidth,0);
			height = ta.getInteger(R.styleable.DesignLinearLayoutSize_dsheight,0);
			  
			 
			dsuplayouttype = ta.getString(R.styleable.DesignLinearLayoutSize_dsuplayouttype);
			  
	       
			ta.recycle();
	    }

	    
	    
	 
	 public ZDLinearLayout(Context context){
		 super(context);
		 } 
 
 
 
}
