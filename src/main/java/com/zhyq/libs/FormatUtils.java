package com.zhyq.libs;

import java.text.SimpleDateFormat;
import java.util.Date;

public class FormatUtils {
	private static SimpleDateFormat f=new SimpleDateFormat("yyyy-MM-dd");
	public static String format(Date date) {
		return f.format(date);
	}
}
