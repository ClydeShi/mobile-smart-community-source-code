package com.zhyq.libs;

import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

public class HttpRequest {
	public static String host=null;
	public final static int GET=1;
	public final static int POST=2;
	private static DefaultHttpClient client=null;
	static {
		HttpParams params = new BasicHttpParams();
		ConnManagerParams.setTimeout(params, 10000);
		HttpConnectionParams.setConnectionTimeout(params, 10000);
		HttpConnectionParams.setSoTimeout(params, 60000);
		SchemeRegistry schReg = new SchemeRegistry();
		schReg.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
		/*schReg.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
		schReg.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 8080));
		schReg.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
		schReg.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 8443));*/
		ClientConnectionManager conMgr = new ThreadSafeClientConnManager(params,schReg);
		client=new DefaultHttpClient(conMgr, params);
	}
	public static void clearAuthInfo() {
		client.setCredentialsProvider(null);
	}
	public static void setAuthInfo(String user,String password) {
		BasicCredentialsProvider credsProvider = new BasicCredentialsProvider();  
		UsernamePasswordCredentials creds = new UsernamePasswordCredentials(user, password);
		credsProvider.setCredentials(new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT), creds);  
		client.setCredentialsProvider(credsProvider);
	}
	

	public static <T> T requestGet(String url,Type t,String... params) throws Exception {
		String json=requestGet(url,params);
		return JsonUtils.toObject(json, t);
	}
	public static <T> T requestPost(String url,Type t,String... params) throws Exception {
		String json=requestPost(url,params);
		return JsonUtils.toObject(json, t);
	}

	
	
	public static <T> T requestGet(String url,Class<T> c,String... params) throws Exception {
		String json=requestGet(url,params);
		return JsonUtils.toObject(json, c);
	}
	public static <T> T requestPost(String url,Class<T> c,String... params) throws Exception {
		String json=requestPost(url,params);
		return JsonUtils.toObject(json, c);
	}
	public static String requestGet(String url,String... params) throws Exception {
		return request(GET,url,params);
	}
	public static String requestPost(String url,String... params) throws Exception {
		return request(POST,url,params);
	}
	public static String request(int method,String url,String... params) throws Exception {
		if (!url.startsWith("http://")) {
			url=host+url;
		}
		HttpRequestBase  request=null;
		if (method==GET) {
			StringBuffer realUrl=new StringBuffer(url);
			if (params.length>0) {
				if (realUrl.indexOf("?")==-1) {
					realUrl.append("?");
				}
				for (int i=0;i<params.length;i+=2) {
					realUrl.append("&");
					realUrl.append(params[i]);
					realUrl.append("=");
					realUrl.append(URLEncoder.encode(getValue(params[i+1]),HTTP.UTF_8));
				}
			}
			request=new HttpGet(new String(realUrl));
		} else {
			request=new HttpPost(url);
			List<NameValuePair> ps=new ArrayList<NameValuePair>();
			for (int i=0;i<params.length;i+=2) {
				ps.add(new BasicNameValuePair(params[i],getValue(params[i+1])));
			}
			HttpEntity entity = new UrlEncodedFormEntity(ps, HTTP.UTF_8);
			((HttpPost)request).setEntity(entity);
		}
		
		HttpResponse response = client.execute(request);
		HttpEntity entity=response.getEntity();
		if (entity==null) {
			throw new Exception("empty response");
		} else {
			return EntityUtils.toString(entity,"UTF-8");
		}
	}
	private static String getValue(String value) {
		if (StringUtils.isBlank(value)) {
			return "";
		} else {
			return value;
		}
	}
}
