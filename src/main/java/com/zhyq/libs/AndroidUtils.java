package com.zhyq.libs;

 
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.httpclient.util.HttpURLConnection;

import com.allthelucky.common.view.network.NetworkApp;
import com.android.http.CircleNetworkImage;
import com.android.http.WebImageView;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
//import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.telephony.TelephonyManager;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.util.Log;

//import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import android.widget.FrameLayout.LayoutParams; 
import android.view.inputmethod.InputMethodManager;
//import android.widget.Adapter;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

 
public class AndroidUtils {
	
	
	
	public static String getVersionKey(Context context,String key) {
		try {
			return key+getVersionName(context);
		} catch (Exception e) {
			return key;
		}
	}
	
	/**
	 * 得到本地或者网络上的bitmap url - 网络或者本地图片的绝对路径,比如:
	 * 
	 * A.网络路径: url="http://blog.foreverlove.us/girl2.png" ;
	 * 
	 * B.本地路径:url="file://mnt/sdcard/photo/image.png";
	 * 
	 * C.支持的图片格式 ,png, jpg,bmp,gif等等
	 * 
	 * @param url
	 * @return
	 */
	public boolean fileIsExists(String filename){               
    	try{                        
    		File f=new File(filename);                       
    		if(!f.exists()){                                
    			return false;                        
    			}                                       
    		}catch (Exception e) {                        
    			// TODO: handle exception                       
    			return false;                }               
    	return true;       
    	} 
	
	public static Bitmap GetLocalOrNetBitmap(String url)
	{
		Bitmap bitmap = null;
		InputStream in = null;
		BufferedOutputStream out = null;
	 
		try
		{
			in = new BufferedInputStream(new URL(url).openStream(), Constant.IO_BUFFER_SIZE);
			final ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
			out = new BufferedOutputStream(dataStream, Constant.IO_BUFFER_SIZE);
			copy(in, out);
			out.flush();
			byte[] data = dataStream.toByteArray();
			bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
			data = null;
			return bitmap;
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	private static void copy(InputStream in, OutputStream out)
            throws IOException {
        byte[] b = new byte[2*1024];
        int read;
        while ((read = in.read(b)) != -1) {
            out.write(b, 0, read);
        }
    }



	public static String getVersionName(Context context) throws Exception {
	    PackageManager packageManager = context.getPackageManager();
	    PackageInfo packInfo = packageManager.getPackageInfo(context.getPackageName(),0);
	    String version = packInfo.versionName;
	    return version;
	}
	public static void setSpinner(Activity parent,int id,int arrayId,String text) {
		Spinner t=(Spinner)parent.findViewById(id);
		int s=-1;
		String[] items=t.getResources().getStringArray(arrayId);
		for (int i=0;i<items.length;i++) {
			if (items[i].equals(text)) {
				s=i;
				break;
			}
		}
		t.setSelection(s);
	}
	public static String getSpinner(Activity parent,int id) {
		Spinner t=(Spinner)parent.findViewById(id);
		return (String)t.getSelectedItem();
	}
	public static String getSpinner(View parent,int id) {
		Spinner t=(Spinner)parent.findViewById(id);
		return (String)t.getSelectedItem();
	}
	public static void setRadioButton(Activity parent,int id,boolean selected) {
		RadioButton t=(RadioButton)parent.findViewById(id);
		t.setChecked(selected);
	}
	public static boolean getRadioButtonSelected(Activity parent,int id) {
		RadioButton t=(RadioButton)parent.findViewById(id);
		return t.isChecked();
	}
	public static void setEditText(Activity parent,int id,String text) {
		EditText t=(EditText)parent.findViewById(id);
		t.setText(text==null?"":text);
	}
	public static void setEditText(View parent,int id,String text) {
		EditText t=(EditText)parent.findViewById(id);
		t.setText(text==null?"":text);
	}
	public static String getEditText(Activity parent,int id) {
		EditText t=(EditText)parent.findViewById(id);
		return t.getText().toString().trim();
	}
	public static String getEditText(View parent,int id) {
		EditText t=(EditText)parent.findViewById(id);
		return t.getText().toString().trim();
	}
	public static void setTextView(Activity parent,int id,String text) {
		TextView t=(TextView)parent.findViewById(id);
		t.setText(text==null?"":text);
	}
	public static void setTextView(View parent,int id,String text) {
		TextView t=(TextView)parent.findViewById(id);
		t.setText(text==null?"":text);
	}
	public static void setTextView(Activity parent,int id,Spanned text) {
		TextView t=(TextView)parent.findViewById(id);
		t.setText(text==null?"":text);
	}
	public static void setTextView(View parent,int id,Spanned text) {
		TextView t=(TextView)parent.findViewById(id);
		t.setText(text==null?"":text);
	}
	
	public static void setTextViewBKImage(View parent,int id,int resid) {
		TextView t=(TextView)parent.findViewById(id);
		t.setBackgroundResource(resid);
	}
		 
		
	
	
	
	public static void setCheckBoxListener(Activity parent,int id,OnCheckedChangeListener l) {
		CheckBox box=(CheckBox)parent.findViewById(id);
		box.setOnCheckedChangeListener(l);
	}
	public static void setCheckBoxListener(View parent,int id,OnCheckedChangeListener l) {
		CheckBox box=(CheckBox)parent.findViewById(id);
		box.setOnCheckedChangeListener(l);
	}
	public static void setCheckBox(Activity parent,int id,boolean value) {
		CheckBox box=(CheckBox)parent.findViewById(id);
		box.setChecked(value);
	}
	public static void setCheckBox(View parent,int id,boolean value) {
		CheckBox box=(CheckBox)parent.findViewById(id);
		box.setChecked(value);
	}
	public static boolean getCheckBox(Activity parent,int id) {
		CheckBox box=(CheckBox)parent.findViewById(id);
		return box.isChecked();
	}
	public static boolean getCheckBox(View parent,int id) {
		CheckBox box=(CheckBox)parent.findViewById(id);
		return box.isChecked();
	}
	
	 
	public static void setWebImageView(Activity parent,int id,String url) {
		WebImageView imageView=(WebImageView)parent.findViewById(id);
		imageView.setScaleType(ScaleType.FIT_XY);
 
		imageView.setImageUrl(url,NetworkApp.getImageLoader());
	}
	public static WebImageView setWebImageView(View parent,int id,String url) {
		WebImageView imageView=(WebImageView)parent.findViewById(id);
		//显示方式  把图片按照指定的大小在View中显示，
		//拉伸显示图片，不保持原比例，填满View
		imageView.setScaleType(ScaleType.FIT_XY);
		imageView.setImageUrl(url,NetworkApp.getImageLoader());
		return imageView;
	}
	
	 
	public static void setTextViewColor(Activity parent,int id,int colorId) {
		TextView t=(TextView)parent.findViewById(id);
		t.setTextColor(parent.getResources().getColor(colorId));
	}
	public static void setTextViewColor(View parent,int id,int colorId) {
		TextView t=(TextView)parent.findViewById(id);
		t.setTextColor(parent.getContext().getResources().getColor(colorId));
	}
	public static void setListViewAdapter(Activity parent,int id,ListAdapter adapter) {
		ListView listView=(ListView)parent.findViewById(id);
		listView.setAdapter(adapter);
	}
	public static void setListViewAdapter(View parent,int id,ListAdapter adapter) {
		ListView listView=(ListView)parent.findViewById(id);
		listView.setAdapter(adapter);
	}
	public static void setGridViewAdapter(Activity parent,int id,ListAdapter adapter) {
		GridView gridView=(GridView)parent.findViewById(id);
		gridView.setAdapter(adapter);
	}
	public static void setGridViewAdapter(View parent,int id,ListAdapter adapter) {
		GridView gridView=(GridView)parent.findViewById(id);
		gridView.setAdapter(adapter);
	}
	public static void setListViewOnItemClickListener(Activity parent,int id,OnItemClickListener listener) {
		ListView listView=(ListView)parent.findViewById(id);
		listView.setOnItemClickListener(listener);
	}
	public static void setListViewOnItemClickListener(View parent,int id,OnItemClickListener listener) {
		ListView listView=(ListView)parent.findViewById(id);
		listView.setOnItemClickListener(listener);
	}
	public static void setGridViewOnItemClickListener(Activity parent,int id,OnItemClickListener listener) {
		GridView gridView=(GridView)parent.findViewById(id);
		gridView.setOnItemClickListener(listener);
	}
	public static void setGridViewOnItemClickListener(View parent,int id,OnItemClickListener listener) {
		GridView gridView=(GridView)parent.findViewById(id);
		gridView.setOnItemClickListener(listener);
	}
	
	public static void hideInputKeybroad(Activity activity) {
		if (activity.getCurrentFocus()!=null) {
			((InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		}
	}
	/*
	public static SfxyApplication getSfxyApplication(Context context) {
		return (SfxyApplication)context.getApplicationContext();
	}
	public static SFXYService getSFXYService(Context context) {
		return getSfxyApplication(context).getService();
	}
	public static boolean isLogined(Context context) {
		
		return getSFXYService(context).isLogined();
	}
	public static User getUser(Context context) {
		return getSFXYService(context).getUser();
	}
	public static void setUser(Context context,User user) {
		getSFXYService(context).setUser(user);
	}*/
	
	
	public static Intent getImageURLIntent( String url )  
	  
	  {  
	  
	    Intent intent = new Intent("android.intent.action.VIEW");  
	  
	    intent.addCategory("android.intent.category.DEFAULT");  
	  
	    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);  
	  
	    Uri uri = Uri.parse(url);
	  
	    intent.setDataAndType(uri, "image/*");  
	  
	    return intent;  
	  
	  } 
	
	public static void openBrowser(Context context,String url) {
		Intent intent = new Intent();
		intent.setData(Uri.parse(url));
		intent.setAction(Intent.ACTION_VIEW);
		context.startActivity(intent);
	}
	
	/*public static void openYixin(Context context) {
			PackageManager packageManager = context.getPackageManager();
			Intent intent = new Intent();
			intent = packageManager.getLaunchIntentForPackage("im.yixin");
			if (intent == null) {
				Log.i("lyyc","APP not found!");
				openBrowser(context,"http://www.yixin.im/");
			} else {
				context.startActivity(intent);
			}
	}*/
	
	public static void start(Context context,Class cls) {
		Intent intent = new Intent(context,cls);		
		context.startActivity(intent);
	}
	public static void removeParentView(View view) {
		if (view!=null && view.getParent()!=null) {
			((ViewGroup)view.getParent()).removeView(view);
		}
	}
	
	//判断是否连网 AndroidUtils.isNetworkAvailable()
	public static boolean isNetworkAvailable(Context context) {   
        ConnectivityManager cm = (ConnectivityManager) context   
                .getSystemService(Context.CONNECTIVITY_SERVICE);   
        if (cm == null) {   
        } else {
        	//如果仅仅是用来判断网络连接
        	//则可以使用 cm.getActiveNetworkInfo().isAvailable();  
            NetworkInfo[] info = cm.getAllNetworkInfo();   
            if (info != null) {   
                for (int i = 0; i < info.length; i++) {   
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {   
                        return true;   
                    }   
                }
            }
        }
        return false;
	}
	
	//判断WIFI是否打开
	 public static boolean isWifiEnabled(Context context) {   
	        ConnectivityManager mgrConn = (ConnectivityManager) context   
	                .getSystemService(Context.CONNECTIVITY_SERVICE);   
	        TelephonyManager mgrTel = (TelephonyManager) context   
	                .getSystemService(Context.TELEPHONY_SERVICE);   
	        return ((mgrConn.getActiveNetworkInfo() != null && mgrConn   
	                .getActiveNetworkInfo().getState() == NetworkInfo.State.CONNECTED) || mgrTel   
	                .getNetworkType() == TelephonyManager.NETWORK_TYPE_UMTS);   
	    }
	 
	 //判断是否3G手机
	 public static boolean is3rd(Context context) {   
	        ConnectivityManager cm = (ConnectivityManager) context   
	                .getSystemService(Context.CONNECTIVITY_SERVICE);   
	        NetworkInfo networkINfo = cm.getActiveNetworkInfo();   
	        if (networkINfo != null   
	                && networkINfo.getType() == ConnectivityManager.TYPE_MOBILE) {   
	            return true;   
	        }   
	        return false;   
	    }  
	 
	 //判断是wifi还是3g网络,用户的体现性在这里了，wifi就可以建议下载或者在线播放。 
	 public static boolean isWifi(Context context) {   
         ConnectivityManager cm = (ConnectivityManager) context   
                 .getSystemService(Context.CONNECTIVITY_SERVICE);   
         NetworkInfo networkINfo = cm.getActiveNetworkInfo();   
         if (networkINfo != null   
                 && networkINfo.getType() == ConnectivityManager.TYPE_WIFI) {   
             return true;   
         }   
         return false;   
     } 
	 //像素 转DP
	 public static int px2dip(Context context, float pxValue){
    	final float scale = context.getResources().getDisplayMetrics().density;
    	return (int) (pxValue / scale + 0.5f);
    	}
	 
	 //dp 转px 
	 public static int dip2px(Context context, float dpValue){
	    	final float scale = context.getResources().getDisplayMetrics().density;
	    	return (int) (dpValue * scale + 0.5f);
	    	}
	 
	 
	 public static Bitmap returnBitMap(String url) { 
		 URL myFileUrl = null; 
		 Bitmap bitmap = null; 
		 try { 
		 myFileUrl = new URL(url); 
		 } catch (MalformedURLException e) { 
		 e.printStackTrace(); 
		 } 
		 try { 
		 HttpURLConnection conn = (HttpURLConnection) myFileUrl.openConnection(); 
		 conn.setDoInput(true); 
		 conn.connect(); 
		 InputStream is = conn.getInputStream(); 
		 bitmap = BitmapFactory.decodeStream(is); 
		 is.close(); 
		 } catch (IOException e) { 
			 e.printStackTrace(); 
			 } 
		 return bitmap; 
		 }	 
		
	// 获取网络图片，如果缓存里面有就从缓存里面获取
	 public static  String getImagePath(Context context, String url) {
		 
		String TAG="huahua";
	 	if(url == null )
	 		return "";
	 	String imagePath = "";
	 	String   fileName   = "";
	 		
	 	// 获取url中图片的文件名与后缀
	 	if(url!=null&&url.length()!=0){ 
	 		fileName  = url.substring(url.lastIndexOf("/")+1);
	 	}
	 	
	 	// 图片在手机本地的存放路径,注意：fileName为空的情况
	 	imagePath = context.getCacheDir() + "/" + fileName;
	 	Log.i(TAG,"imagePath = " + imagePath);
	 	File file = new File(context.getCacheDir(),fileName);// 保存文件,
	 	if(!file.exists())
	 	{
	 		Log.i(TAG, "file 不存在 ");
	 		try {
	 			byte[] data =  readInputStream(getRequest(url));
	 			Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0,
	 					data.length);
	 			
	 			bitmap.compress(CompressFormat.JPEG, 100, new FileOutputStream(
	 					file));
	 			
	 			imagePath = file.getAbsolutePath();
	 			Log.i(TAG,"imagePath : file.getAbsolutePath() = " +  imagePath);
	 			
	 		} catch (Exception e) {
	 			Log.e(TAG, e.toString());
	 		}
	 	}
	 	return imagePath;
	 } // getImagePath( )结束。
	 
	 public static InputStream getRequest(String path) throws Exception{    	
			URL url = new URL(path);
				HttpURLConnection conn = (HttpURLConnection)url.openConnection();
				conn.setRequestMethod("GET");
				conn.setConnectTimeout(5000); // 5秒
				if(conn.getResponseCode() == 200){
					return conn.getInputStream();
				}
			return null;

		}
	 
	 
	 public static byte[] readInputStream(InputStream inStream) throws Exception{
	        ByteArrayOutputStream outSteam = new ByteArrayOutputStream();
	        byte[] buffer = new byte[4096];
	        int len = 0;
	        while( (len = inStream.read(buffer)) != -1 ){
	            outSteam.write(buffer, 0, len);
	        }
	        outSteam.close();
	        inStream.close();
	        return outSteam.toByteArray();
	}
	 
	 // 布局大小尺寸设置 支持5种布局  View,LinearLayout,RelativeLayout,ImageView,WebImageView
	 // View   convertView     控件所在视图
	 // int    id              控件R.ID.xxxx 
	 // String uplayoutType    上一层的外框 类型 只有两种  一个是 LinearLayout,一个是 RelativeLayout
	 // int designScreentWidth = 640;  //设计屏款宽度 *
	 // int designControlWidth = 0;    //设计宽度 *
	 // int designControlHeigth= 0;    //设计高度 *   如果该值为 0 ，表示他的值是自动高度 主要应用文字内容 不确定的情况
	 // int desingMarginLeft   = 0;      //左边的空白
	 // int desingMarginRight  = 0;
	 // int desingMarginTop    = 0;
	 // int desingMarginBottom =0;
	 // String curlayoutType   0 表示view   1 表示 LinearLayout 2 表示 RelativeLayout 3 表示图片布局 ImageView
	 //                        4 表示 com.android.http.WebImageView
	 // 下面的函数是针对控件为  LinearLayout 为框架的
	 // AndroidUtils.setLinearLayoutSize(convertView,R.ID.xxxx,"LinearLayout",640,100,100,10,10,10,10)
	 // Context context,
	 public static void setLinearLayoutSize(View convertView,int id,String uplayoutType,String curlayoutType,
			 int designScreentWidth,int designControlWidth,int designControlHeigth,int desingMarginLeft,
			 int desingMarginRight,int desingMarginTop,int desingMarginBottom) {
		  
		
		if(designScreentWidth==0) return;
		if(designControlWidth==0) return;
		if(designControlHeigth==0) return;

		
		int curMarginLeft  =0;
		int curMarginRight =0; 
		int curMarginTop   =0;
		int curMarginBottom=0; 
		
		//得到屏幕宽度
		DisplayMetrics dm = convertView.getContext().getResources().getDisplayMetrics();
		int curScreenWidth = dm.widthPixels; //当前屏宽
		 
 
		
		float scale  = (float)curScreenWidth/(float)designScreentWidth;
		
		//当前控件的宽度和高度初始化
		int   curWidth=  0;
		int   curHeight= 0;
		
		//设计稿件尺寸 控件长 高 参数 传递
		//designControlWidth = 640;   //设计宽度 *
		//designControlHeigth= 280;   //设计高度 *
		
		//设计稿件尺寸 控件的边框像素 参数 传递
		//desingMarginLeft = 0;
		//desingMarginRight= 0;
		//desingMarginTop  = 0;
		//desingMarginBottom=0;
		
		//换算成当前屏的尺寸
		curWidth=  Math.round(designControlWidth * scale);
		curHeight= Math.round(designControlHeigth * scale);
		
		//Log.i("huahua","curHeight="+String.valueOf(curHeight));
		
		
  
		//换算成当前屏的尺寸
		curMarginLeft = Math.round(desingMarginLeft * scale);
		curMarginRight= Math.round(desingMarginRight * scale); 
		curMarginTop  = Math.round(desingMarginTop * scale);;
		curMarginBottom=Math.round(desingMarginBottom * scale);
		
		//Log.i("huahua",String.valueOf(curWidth));
		//Log.i("huahua",String.valueOf(curMarginRight));
		
		
		//设计整个外框的高度和宽度 
		//设置整体的高度和宽度  
		if(curlayoutType.equals("0")){
			if(uplayoutType.equals("LinearLayout")){
				View llControl= convertView.findViewById(id);
			    LinearLayout.LayoutParams params1= (LinearLayout.LayoutParams) llControl.getLayoutParams();
			    
			    params1.width  = curWidth;
			    if(curHeight>0){
			    	params1.height = curHeight;
			    }else{
			    	params1.height =LayoutParams.WRAP_CONTENT;
			    }
			    params1.leftMargin = curMarginLeft;
			    params1.rightMargin= curMarginRight;
			    params1.topMargin  = curMarginTop;
			    params1.bottomMargin=curMarginBottom; 
			    llControl.setLayoutParams(params1);
			}
			
			if(uplayoutType.equals("RelativeLayout")){
				View llControl= convertView.findViewById(id);
				RelativeLayout.LayoutParams params2= (RelativeLayout.LayoutParams) llControl.getLayoutParams();
			    
				params2.width  = curWidth;
			    if(curHeight>0){
			    	params2.height = curHeight;
			    }else{
			    	params2.height =LayoutParams.WRAP_CONTENT;
			    }
				params2.leftMargin = curMarginLeft;
				params2.rightMargin= curMarginRight;
				params2.topMargin  = curMarginTop;
				params2.bottomMargin=curMarginBottom; 
			    llControl.setLayoutParams(params2);
			    
			    
			}		
		}
		 
		//设计整个外框的高度和宽度 
		//设置整体的高度和宽度  
		if(curlayoutType.equals("1")){
			if(uplayoutType.equals("LinearLayout")){
				LinearLayout llControl=(LinearLayout)convertView.findViewById(id);
			    LinearLayout.LayoutParams params1= (LinearLayout.LayoutParams) llControl.getLayoutParams();
			    
			    params1.width  = curWidth;
			    if(curHeight>0){
			    	params1.height = curHeight;
			    }else{
			    	params1.height =LayoutParams.WRAP_CONTENT;
			    }
			    params1.leftMargin = curMarginLeft;
			    params1.rightMargin= curMarginRight;
			    params1.topMargin  = curMarginTop;
			    params1.bottomMargin=curMarginBottom; 
			    llControl.setLayoutParams(params1);
			    //Log.i("huahua","curMarginLeft");
			   // Log.i("huahua",String.valueOf(curMarginLeft));
			   // Log.i("huahua",String.valueOf(curMarginRight));
			}
			
			if(uplayoutType.equals("RelativeLayout")){
				LinearLayout llControl=(LinearLayout)convertView.findViewById(id);
				RelativeLayout.LayoutParams params2= (RelativeLayout.LayoutParams) llControl.getLayoutParams();
			    
				params2.width  = curWidth;
			    if(curHeight>0){
			    	params2.height = curHeight;
			    }else{
			    	params2.height =LayoutParams.WRAP_CONTENT;
			    }
				params2.leftMargin = curMarginLeft;
				params2.rightMargin= curMarginRight;
				params2.topMargin  = curMarginTop;
				params2.bottomMargin=curMarginBottom;
			    llControl.setLayoutParams(params2);
			}		
		}
		
		//相对布局文件尺寸大小
		if(curlayoutType.equals("2")){
			if(uplayoutType.equals("LinearLayout")){
				RelativeLayout llControl=(RelativeLayout)convertView.findViewById(id);
			    LinearLayout.LayoutParams params1= (LinearLayout.LayoutParams) llControl.getLayoutParams();
			    
			    params1.width  = curWidth;
			    if(curHeight>0){
			    	params1.height = curHeight;
			    }else{
			    	params1.height =LayoutParams.WRAP_CONTENT;
			    }
			    params1.leftMargin = curMarginLeft;
			    params1.rightMargin= curMarginRight;
			    params1.topMargin  = curMarginTop;
			    params1.bottomMargin=curMarginBottom; 
			    llControl.setLayoutParams(params1);
			}
			
			if(uplayoutType.equals("RelativeLayout")){
				RelativeLayout llControl=(RelativeLayout)convertView.findViewById(id);
				RelativeLayout.LayoutParams params2= (RelativeLayout.LayoutParams) llControl.getLayoutParams();
			    
				params2.width  = curWidth;
			    if(curHeight>0){
			    	params2.height = curHeight;
			    }else{
			    	params2.height =LayoutParams.WRAP_CONTENT;
			    }
				params2.leftMargin = curMarginLeft;
				params2.rightMargin= curMarginRight;
				params2.topMargin  = curMarginTop;
				params2.bottomMargin=curMarginBottom; 
			    llControl.setLayoutParams(params2);
			}		
		}	
		
		
		//图片布局大小
		if(curlayoutType.equals("3")){
			if(uplayoutType.equals("LinearLayout")){
				ImageView llControl=(ImageView)convertView.findViewById(id);
			    LinearLayout.LayoutParams params1= (LinearLayout.LayoutParams) llControl.getLayoutParams();
			    
			    params1.width  = curWidth;
			    if(curHeight>0){
			    	params1.height = curHeight;
			    }else{
			    	params1.height =LayoutParams.WRAP_CONTENT;
			    }
			    params1.leftMargin = curMarginLeft;
			    params1.rightMargin= curMarginRight;
			    params1.topMargin  = curMarginTop;
			    params1.bottomMargin=curMarginBottom; 
			    llControl.setLayoutParams(params1);
			}
			
			if(uplayoutType.equals("RelativeLayout")){
				ImageView llControl=(ImageView)convertView.findViewById(id);
				RelativeLayout.LayoutParams params2= (RelativeLayout.LayoutParams) llControl.getLayoutParams();
			    
				params2.width  = curWidth;
			    if(curHeight>0){
			    	params2.height = curHeight;
			    }else{
			    	params2.height =LayoutParams.WRAP_CONTENT;
			    }
				params2.leftMargin = curMarginLeft;
				params2.rightMargin= curMarginRight;
				params2.topMargin  = curMarginTop;
				params2.bottomMargin=curMarginBottom; 
			    llControl.setLayoutParams(params2);
			}		
		} 
		
		

		//WEB图片布局大小
		if(curlayoutType.equals("4")){
			if(uplayoutType.equals("LinearLayout")){
				com.android.http.WebImageView llControl=(com.android.http.WebImageView)convertView.findViewById(id);
			    LinearLayout.LayoutParams params1= (LinearLayout.LayoutParams) llControl.getLayoutParams();
			    
			    params1.width  = curWidth;
			    if(curHeight>0){
			    	params1.height = curHeight;
			    }else{
			    	params1.height =LayoutParams.WRAP_CONTENT;
			    }
			    params1.leftMargin = curMarginLeft;
			    params1.rightMargin= curMarginRight;
			    params1.topMargin  = curMarginTop;
			    params1.bottomMargin=curMarginBottom; 
			    llControl.setLayoutParams(params1);
			}
			
			if(uplayoutType.equals("RelativeLayout")){
				com.android.http.WebImageView llControl=(com.android.http.WebImageView)convertView.findViewById(id);
				RelativeLayout.LayoutParams params2= (RelativeLayout.LayoutParams) llControl.getLayoutParams();
			    
				params2.width  = curWidth;
			    if(curHeight>0){
			    	params2.height = curHeight;
			    }else{
			    	params2.height =LayoutParams.WRAP_CONTENT;
			    }
				params2.leftMargin = curMarginLeft;
				params2.rightMargin= curMarginRight;
				params2.topMargin  = curMarginTop;
				params2.bottomMargin=curMarginBottom; 
			    llControl.setLayoutParams(params2);
			}		
		}  
		
	 } 
	 
	 
	 //根据像素 转成SP ,重新设置字体的大小
	 public static void setTextSizeByPx(TextView convertView,int designScreentWidth,int px){
		  
		   
		//得到屏幕宽度
		DisplayMetrics dm = convertView.getContext().getResources().getDisplayMetrics();
		int curScreenWidth = dm.widthPixels; //当前屏宽
		 
		float scale  = (float)curScreenWidth/(float)designScreentWidth;
			
		 
			
		 //换算成当前屏的尺寸
		 int curPx=  Math.round(px * scale);
		 
		 
		 
		 int sp = px2sp(convertView.getContext(),curPx);
		 
		 //首先得到该字体的px 
		 convertView.setTextSize(sp);
		 
	 }
	 
	 
	 
	
	 //根据像素 转成SP ,重新设置字体的大小
	 public static void setTextSizeByPx(View convertView,int id,int designScreentWidth,int px){
		  
		 TextView tv = (TextView)convertView.findViewById(id);
		 
		 
		//得到屏幕宽度
		DisplayMetrics dm = convertView.getContext().getResources().getDisplayMetrics();
		int curScreenWidth = dm.widthPixels; //当前屏宽
		 
		float scale  = (float)curScreenWidth/(float)designScreentWidth;
			
		 
			
		 //换算成当前屏的尺寸
		 int curPx=  Math.round(px * scale);
		 
		 
		 
		 int sp = px2sp(convertView.getContext(),curPx);
		 
		 //首先得到该字体的px 
		 tv.setTextSize(sp);
		 
	 }
	    
	 
		 /** 
	      * 将px值转换为sp值，保证文字大小不变 
	      *  
	      * @param pxValue 
	      * @param fontScale 
	      *            （DisplayMetrics类中属性scaledDensity） 
	      * @return 
	      */  
	     public static int px2sp(Context context, float pxValue) {  
	         final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;  
	         return (int) (pxValue / fontScale + 0.5f);  
	     }  
	   
	     /** 
	      * 将sp值转换为px值，保证文字大小不变 
	      *  
	      * @param spValue 
	      * @param fontScale 
	      *            （DisplayMetrics类中属性scaledDensity） 
	      * @return 
	      */  
	     public static int sp2px(Context context, float spValue) {  
	         final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;  
	         return (int) (spValue * fontScale + 0.5f);  
	     }  
 
	 
	     //直接针 上层是相对布局 或者线性布局，当前层是对线性布局 设置
	     public static void setLinearLayoutSizeBypx(View convertView,String uplayoutType,
				 int designScreentWidth,int designControlWidth,int designControlHeigth,int desingMarginLeft,
				 int desingMarginRight,int desingMarginTop,int desingMarginBottom) {
			  
			
			if(designScreentWidth==0) return;
			if(designControlWidth==0) return;
			//if(designControlHeigth==0) return;
			
			
			
			if(convertView==null){
				Log.i("huahua"," convertView object is null" );
				return;
			}
			 
          
			
			int curMarginLeft  =0;
			int curMarginRight =0; 
			int curMarginTop   =0;
			int curMarginBottom=0; 
			
			//得到屏幕宽度
			DisplayMetrics dm = convertView.getContext().getResources().getDisplayMetrics();
			int curScreenWidth = dm.widthPixels; //当前屏宽
			 
	 
			
			float scale  = (float)curScreenWidth/(float)designScreentWidth;
			
			//当前控件的宽度和高度初始化
			int   curWidth=  0;
			int   curHeight= 0;
			
			//设计稿件尺寸 控件长 高 参数 传递
			//designControlWidth = 640;   //设计宽度 *
			//designControlHeigth= 280;   //设计高度 *
			
			//设计稿件尺寸 控件的边框像素 参数 传递
			//desingMarginLeft = 0;
			//desingMarginRight= 0;
			//desingMarginTop  = 0;
			//desingMarginBottom=0;
			
			//换算成当前屏的尺寸
			curWidth=  Math.round(designControlWidth * scale);
			curHeight= Math.round(designControlHeigth * scale);
			
			
			
	  
			//换算成当前屏的尺寸
			curMarginLeft = Math.round(desingMarginLeft * scale);
			curMarginRight= Math.round(desingMarginRight * scale); 
			curMarginTop  = Math.round(desingMarginTop * scale);;
			curMarginBottom=Math.round(desingMarginBottom * scale);
			
			 
			  
			if(uplayoutType.equals("LinearLayout")){
				
				//LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(100,100);
				
				//直接用 继承的LinearLayout 
				//LinearLayout llControl=(LinearLayout)convertView.findViewById(Controlid);
				LinearLayout.LayoutParams params1= (LinearLayout.LayoutParams) convertView.getLayoutParams();
			    
				if(params1==null){
					Log.i("huahua","params1 得到对象为空 " );
					return;
				}
				
				 
				
				
			    params1.width  = curWidth;
			    if(curHeight>0){
			    	params1.height = curHeight;
			    }else{
			    	params1.height =LayoutParams.WRAP_CONTENT;
			    }
			    
			    params1.leftMargin = curMarginLeft;
			    params1.rightMargin= curMarginRight;
			    params1.topMargin  = curMarginTop;
			    params1.bottomMargin=curMarginBottom; 
			    convertView.setLayoutParams(params1);
			}
			 
			
			if(uplayoutType.equals("RelativeLayout")){
				//com.android.http.WebImageView llControl=(com.android.http.WebImageView)convertView.findViewById(id);
				RelativeLayout.LayoutParams params2= (RelativeLayout.LayoutParams) convertView.getLayoutParams();
			     
				
				if(params2==null){
					Log.i("huahua","params2 得到对象为空 " );
					return;
				} 
				
				params2.width  = curWidth;
			    if(curHeight>0){
			    	params2.height = curHeight;
			    }else{
			    	params2.height =LayoutParams.WRAP_CONTENT;
			    }
				params2.leftMargin = curMarginLeft;
				params2.rightMargin= curMarginRight;
				params2.topMargin  = curMarginTop;
				params2.bottomMargin=curMarginBottom; 
				convertView.setLayoutParams(params2);
			}	
			 	
			
		 }     
	     
	     //结合下面的方法可以来或者随机并不重复的控件ID
	     private static final AtomicInteger sNextGeneratedId = new AtomicInteger(1);

	     public static int generateViewId() {
	         for (; ; ) {
	             final int result = sNextGeneratedId.get();
	             // aapt-generated IDs have the high byte nonzero; clamp to the range under that.
	             int newValue = result + 1;
	             if (newValue > 0x00FFFFFF) newValue = 1; // Roll over to 1, not 0.
	             if (sNextGeneratedId.compareAndSet(result, newValue)) {
	                 return result;
	             }
	         }
	     }

	     public static boolean isEmpty(String str) {
	         if (null == str || "".equals(str.trim())) {
	             return true;
	         }else{
	             return false;
	         }
	     }
	     
	     public static void setWebImageCircleView(View parent,int id,String url) { 
	    	 
	    	 CircleNetworkImage imageView=(CircleNetworkImage)parent.findViewById(id);
	    	 //imageView.setScaleType(ScaleType.FIT_XY);
	  
	    	 imageView.setImageUrl(url,NetworkApp.getImageLoader());
	     }
	     
	     
	     
}
