package com.zhyq.libs;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.widget.Checkable;
import android.widget.LinearLayout;

public class CheckableLinearLayout extends LinearLayout implements Checkable {
	private boolean mChecked;
	
	public CheckableLinearLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	public void setChecked(boolean checked) {
		mChecked = checked;
		setBackgroundDrawable(checked ? new ColorDrawable(0xffd1d1d1) : null);//当选中时颜色d1d1d1
	}
	
	@Override
	public boolean isChecked() {
		return mChecked;
	}
	
	@Override
	public void toggle() {
		setChecked(!mChecked);
	}
	
}
