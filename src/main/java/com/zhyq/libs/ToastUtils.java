package com.zhyq.libs;

import android.content.Context;
import android.widget.Toast;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Looper;

public class ToastUtils {
	static Handler mainHandler = new Handler(Looper.getMainLooper());
	static Toast toast;
	@SuppressLint("ShowToast")
	public static void init(Context context) {
		toast = Toast.makeText(context, "", Toast.LENGTH_SHORT);
	}

	public static void show (final String msg) {
		mainHandler.post(new Runnable() {
			@Override
			public void run() {
				toast.setText(msg);
				toast.show();
			}
		});
	}

	public static void show(Context context,int stringId) {
		show(context,context.getResources().getString(stringId));
	}

	public static void show(Context context,String message) {
		if (context!=null) {
			Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
		}
	}
}
