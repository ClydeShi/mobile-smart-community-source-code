package com.zhyq.libs;

  
import android.content.Context;
import android.graphics.Rect;
import android.os.Handler;
import android.util.AttributeSet;

import android.view.MotionEvent;
import android.view.View;

import android.widget.HorizontalScrollView;
//水平弹性滑动的ScrollView
 
public class ScrollListenerHorizontalScrollView extends HorizontalScrollView{
 
 
    //private int count;// 屏幕显示的标签个数
   //private int indicatorWidth;// 每个标签所占的宽度
    //private int currentIndicatorLeft = 0;// 当前所在标签页面的位移
	
	private int screenWidth = 750;
	private View inner;
	private float x;
	private Rect normal = new Rect();
	
	private int oldx=0;
	private int curx=0;

	private int position=1;
	
	@Override
	protected void onFinishInflate() {
		if (getChildCount() > 0) {
			inner = getChildAt(0); 
		}
	}
	
	public void getScreenWidth(int width){
		this.screenWidth = width;
	}
	
	public ScrollListenerHorizontalScrollView(Context context,
		AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
// TODO Auto-generated constructor stub
		
        //count = 4;
}
 
	public ScrollListenerHorizontalScrollView(Context context,
			AttributeSet attrs) {
		super(context, attrs);
// TODO Auto-generated constructor stub
}
 
   public ScrollListenerHorizontalScrollView(Context context) {
      super(context);
// TODO Auto-generated constructor stub
}
    
    
   public interface ScrollViewListener { 
 
      void onScrollChanged(ScrollType scrollType,int position); 
	 
   }
 
   private Handler mHandler;
   private ScrollViewListener scrollViewListener;
    /**
* 滚动状态 IDLE 滚动停止 TOUCH_SCROLL 手指拖动滚动 FLING滚动
* @version XHorizontalScrollViewgallery 
* @author DZC
* @Time 2014-12-7 上午11:06:52
*
*
*/
     public enum ScrollType{IDLE,TOUCH_SCROLL,FLING};
  
     /**
* 记录当前滚动的距离
*/
    private int currentX = -9999999;
    /**
* 当前滚动状态
*/
   private ScrollType scrollType = ScrollType.IDLE;
    /**
* 滚动监听间隔
*/
   private int scrollDealy = 50;
   /**
* 滚动监听runnable
*/
   private Runnable scrollRunnable = new Runnable() {
 
        @Override
        public void run() {
        // TODO Auto-generated method stub
        if(getScrollX()==currentX){
               //滚动停止 取消监听线程
                //Log.d("huahua", "停止滚动");
                scrollType = ScrollType.IDLE;
                if(scrollViewListener!=null){
                	curx =  getScrollX(); 
                	scrolltopositon(curx,3); 
                } 
                mHandler.removeCallbacks(this);
                return;
             }else{
                //手指离开屏幕 view还在滚动的时候 
                scrollType = ScrollType.FLING;
                
            }
        	curx =  getScrollX(); 
        	scrolltopositon(curx,3); 
        	//滚动监听间隔:milliseconds 
	        mHandler.postDelayed(this, scrollDealy);
        }
        
};


	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		
		if (inner == null) {
			return super.onTouchEvent(ev); 
		}
		 

		switch (ev.getAction()) {
			case MotionEvent.ACTION_MOVE:
				this.scrollType = ScrollType.TOUCH_SCROLL;
				//scrollViewListener.onScrollChanged(scrollType,position);
				//手指在上面移动的时候 取消滚动监听线程
				mHandler.removeCallbacks(scrollRunnable);
				 
				
				break;
			case MotionEvent.ACTION_UP:
				//手指移动的时候 
				mHandler.post(scrollRunnable);
				break;
			case MotionEvent.ACTION_DOWN:
				x = ev.getX();
				break;

		}
		return super.onTouchEvent(ev);
	}
	
	 
      
	

/**
* 必须先调用这个方法设置Handler 不然会出错
* 2014-12-7 下午3:55:39 
* @author DZC
* @return void
* @param handler 
* @TODO
*/
public void setHandler(Handler handler){
this.mHandler = handler;
}
/**
* 设置滚动监听
* 2014-12-7 下午3:59:51 
* @author DZC
* @return void
* @param listener 
* @TODO
*/
public void setOnScrollStateChangedListener(ScrollViewListener listener){
this.scrollViewListener = listener;
}

public void scrolltopositon(int curposition,int fenshu){
	
	int scrollX=0;
    //超过半屏 则转到下一个 第2个屏幕
    if((curposition<screenWidth/fenshu)){
    	scrollX =0 ;
    	position =1;
    }
  //表示从左往右边移动
	if((curposition>screenWidth/fenshu)&&(curposition<=screenWidth+screenWidth/fenshu)){
		scrollX =screenWidth ;
    	position =2;
    }
	
  //第3个屏幕
    if((curposition>screenWidth+screenWidth/fenshu)&&(curposition<=2*screenWidth+screenWidth/fenshu)){
    	scrollX =2*screenWidth ;
    	position =3;
    }
	
  //第4个屏
    if(curposition>2*screenWidth+screenWidth/fenshu) {
    	scrollX =3*screenWidth ;
    	position =4;
    } 

  //如果是从右往左边移动 的方向移动


//Log.i("huahua","curpostion="+String.valueOf(scrollX));
    smoothScrollTo(scrollX, 0);

    scrollViewListener.onScrollChanged(scrollType,position);
 
}

}
