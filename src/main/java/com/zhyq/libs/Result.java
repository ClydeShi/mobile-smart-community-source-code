package com.zhyq.libs;



//首先List<T> list是Java泛型的应用，其中T可以是任何对象，比如String，Int，Object....都可以
public class Result<T> {
	private boolean result=false;
	private String error=null;
	private T data=null;
	public Result() {
		super();
	}
	public Result(boolean result, String error, T data) {
		super();
		this.result = result;
		this.error = error;
		this.data = data;
	}
	public boolean isResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
}
