package com.zhyq.libs;

public class Constant {
	public static final int REFRESH=1000;
	public static final int IO_BUFFER_SIZE = 2*1024; 
	
	//Titles的标识
			public static  String[] TITLES = new String[]{"头条", "动态", "双色球", "大乐透","福彩其他","体彩其他","胜负彩","国际足球","中国足球","NBA","CBA","其他"};
			
			//Fragment的标识
			public static final String FRAGMENT_FLAG_MESSAGE = "消息"; 
			public static final String FRAGMENT_FLAG_CONTACTS = "联系人"; 
			public static final String FRAGMENT_FLAG_NEWS = "新闻"; 
			public static final String FRAGMENT_FLAG_SETTING = "设置"; 
			public static final String FRAGMENT_FLAG_SIMPLE = "simple"; 
	 
}
