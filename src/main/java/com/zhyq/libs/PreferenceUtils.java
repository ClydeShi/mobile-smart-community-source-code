package com.zhyq.libs;

import java.util.Set;


/* SharedPreferences  其存储位置在/data/data/<包名>/shared_prefs目录
 *  

除了SQLite数据库外，SharedPreferences也是一种轻型的数据存储方式，它的本质是基于XML文件存储key-value键值对数据，

通常用来存储一些简单的配置信息。其存储位置在/data/data/<包名>/shared_prefs目录下。SharedPreferences对象本身只能获取数据而不支持存储和修改，

存储修改是通过Editor对象实现。实现SharedPreferences存储的步骤如下：

　　一、根据Context获取SharedPreferences对象

　　二、利用edit()方法获取Editor对象。

　　三、通过Editor对象存储key-value键值对数据。

　　四、通过commit()方法提交数据。


 */



import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class PreferenceUtils {
	public static final String perfence="hh";
	public static boolean getBoolean(Context context,String key,boolean defaultValue) {
		SharedPreferences p=context.getSharedPreferences(perfence, Context.MODE_PRIVATE);
		return p.getBoolean(key, defaultValue);
	}
	public static void setBoolean(Context context,String key,boolean value) {
		SharedPreferences p=context.getSharedPreferences(perfence, Context.MODE_PRIVATE);
		Editor e=p.edit();
		e.putBoolean(key,value);
		e.commit();
	}
	public static String getString(Context context,String key,String defaultValue) {
		SharedPreferences p=context.getSharedPreferences(perfence, Context.MODE_PRIVATE);
		return p.getString(key, defaultValue);
	}
	public static void setString(Context context,String key,String value) {
		SharedPreferences p=context.getSharedPreferences(perfence, Context.MODE_PRIVATE);
		Editor e=p.edit();
		e.putString(key,value);
		e.commit();
	}
	public static int getInt(Context context,String key,int defaultValue) {
		SharedPreferences p=context.getSharedPreferences(perfence, Context.MODE_PRIVATE);
		return p.getInt(key, defaultValue);
	}
	public static void setInt(Context context,String key,int value) {
		SharedPreferences p=context.getSharedPreferences(perfence, Context.MODE_PRIVATE);
		Editor e=p.edit();
		e.putInt(key,value);
		e.commit();
	}
	public static long getLong(Context context,String key,long defaultValue) {
		SharedPreferences p=context.getSharedPreferences(perfence, Context.MODE_PRIVATE);
		return p.getLong(key, defaultValue);
	}
	public static void setLong(Context context,String key,long value) {
		SharedPreferences p=context.getSharedPreferences(perfence, Context.MODE_PRIVATE);
		Editor e=p.edit();
		e.putLong(key,value);
		e.commit();
	}
	public static Set<String> getSet(Context context,String key) {
		SharedPreferences p=context.getSharedPreferences(perfence, Context.MODE_PRIVATE);
		Set<String> sets=p.getStringSet(key, null);
		return sets;
	}
	public static void setSet(Context context,String key,Set<String> value) {
		SharedPreferences p=context.getSharedPreferences(perfence, Context.MODE_PRIVATE);
		Editor e=p.edit();
		e.putStringSet(key, value);
		e.commit();
	}
	
	/*
	 * 得到数组
	 */
	public static String[] getSharedPreference(Context mContext,String preferencetype,String key) {
		String regularEx = "#";
		String[] str = null;
		SharedPreferences sp = mContext.getSharedPreferences(preferencetype, Context.MODE_PRIVATE);
		String values; 
		values = sp.getString(key, ""); 
		str = values.split(regularEx);

		return str;
		}
	
	/*
	 *  写入数组
	 */
	public static void setSharedPreference(Context mContext,String preferencetype,String key, String[] values) {
		String regularEx = "#";
		String str = "";
		SharedPreferences sp = mContext.getSharedPreferences(preferencetype, Context.MODE_PRIVATE);
		if (values != null && values.length > 0) {
		for (String value : values) {
		str += value;
		str += regularEx;
		}
		Editor et = sp.edit();
		et.putString(key, str);
		et.commit();
		} 
		}
	
	/*
	 * 删除数据
	 */
	public static void clearSharedPreference(Context mContext,String preferencetype) {
	 
		SharedPreferences sp = mContext.getSharedPreferences(preferencetype, Context.MODE_PRIVATE);
		 
		Editor et = sp.edit();
		et.clear();
		et.commit(); 
		}
}
