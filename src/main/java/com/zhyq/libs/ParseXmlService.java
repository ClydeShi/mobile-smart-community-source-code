package com.zhyq.libs;
import java.io.InputStream;
import java.util.HashMap;

import org.xmlpull.v1.XmlPullParser;

import android.util.Xml;

/**
 *@author coolszy
 *@date 2012-4-26
 *@blog http://blog.92coding.com
 */
public class ParseXmlService
{
	public HashMap<String, String> parseXml(InputStream inStream) throws Exception
	{
		HashMap<String, String> hashMap = new HashMap<String, String>();
		 
		
		XmlPullParser  parser = Xml.newPullParser();    
	    parser.setInput(inStream, "utf-8");//设置解析的数据源    
	    int type = parser.getEventType();  
	    //UpdataInfo info = new UpdataInfo();//实体   
	    while(type != XmlPullParser.END_DOCUMENT ){  
	        switch (type) {  
	        case XmlPullParser.START_TAG:  
	            if("version".equals(parser.getName())){  
	               // info.setVersion(parser.nextText()); //获取版本号   
	            	hashMap.put("version",parser.nextText());
	            }else if ("url".equals(parser.getName())){  
	            	hashMap.put("name",parser.nextText());
	                //info.setUrl(parser.nextText()); //获取要升级的APK文件   
	            }else if ("description".equals(parser.getName())){  
	            	hashMap.put("name",parser.nextText());
	               // info.setDescription(parser.nextText()); //获取该文件的信息   
	            }  
	            break;  
	        }  
	        type = parser.next();  
	    }  
	    return hashMap;  

		
		
		
		
		/*
		 * 
		 * 下面代码适合 本地version
		
		// 实例化一个文档构建器工厂
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		// 通过文档构建器工厂获取一个文档构建器
		DocumentBuilder builder = factory.newDocumentBuilder();
		// 通过文档通过文档构建器构建一个文档实例
		Document document = builder.parse(inStream);
		//获取XML文件根节点
		Element root = document.getDocumentElement();
		//获得所有子节点
		NodeList childNodes = root.getChildNodes();
		for (int j = 0; j < childNodes.getLength(); j++)
		{
			//遍历子节点
			Node childNode = (Node) childNodes.item(j);
			if (childNode.getNodeType() == Node.ELEMENT_NODE)
			{
				Element childElement = (Element) childNode;
				//版本号
				if ("version".equals(childElement.getNodeName()))
				{
					hashMap.put("version",childElement.getFirstChild().getNodeValue());
				}
				//软件名称
				else if (("name".equals(childElement.getNodeName())))
				{
					hashMap.put("name",childElement.getFirstChild().getNodeValue());
				}
				//下载地址
				else if (("url".equals(childElement.getNodeName())))
				{
					hashMap.put("url",childElement.getFirstChild().getNodeValue());
				}
			}
		}
		*/
		//return hashMap;
	}
}
