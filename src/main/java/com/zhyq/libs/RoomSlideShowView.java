package com.zhyq.libs;
  
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
 
import com.zhyq.HhApplication;
import com.zhyq.R;
 


/**
 * ViewPager实现的轮播图广告自定义视图，如京东首页的广告轮播图效果；
 * 既支持自动轮播页面也支持手势滑动切换页面 
 * 
 *
 */

public class RoomSlideShowView extends FrameLayout {
	
	// 使用universal-image-loader插件读取网络图片，需要工程导入universal-image-loader-1.8.6-with-sources.jar
	private ImageLoader imageLoader = ImageLoader.getInstance();
	private String ls_url1 = null;
	private String ls_url2 = null;
	private String ls_url3  =null;
	private String ls_url4  =null;
	private String ls_url5  =null;
	
	private String ls_image_type1="";   //图片类型
	private String ls_image_type2="";
	private String ls_image_type3="";
	private String ls_image_type4="";
	private String ls_image_type5="";
	
	private String ls_image_value1="";   //图片值
	private String ls_image_value2="";
	private String ls_image_value3="";
	private String ls_image_value4="";
	private String ls_image_value5="";	
	
	
    //轮播图图片数量
    private final static int IMAGE_COUNT = 5;
    //自动轮播的时间间隔
    private final static int TIME_INTERVAL = 5;
    //自动轮播启用开关
    private final static boolean isAutoPlay = true; 
    
    //自定义轮播图的资源
    private String[] imageUrls;
    //放轮播图片的ImageView 的list
    private List<ImageView> imageViewsList;
    //放圆点的View的list
    private List<View> dotViewsList;
    
    private ViewPager viewPager;
    //当前轮播页
    private int currentItem  = 0;
    //定时任务
    private ScheduledExecutorService scheduledExecutorService;
    
    private Context context;
    
    //Handler
    private Handler handler = new Handler(){

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
            viewPager.setCurrentItem(currentItem);
        }
        
    };
     

    public RoomSlideShowView(Context context) {
        this(context,null);
        
 
		   
        // TODO Auto-generated constructor stub
    }
    public RoomSlideShowView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
        // TODO Auto-generated constructor stub
    }
    public RoomSlideShowView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context; 
          
        //得到图片的链接
        ls_url1 = HhApplication.getInstance(context).sysArgument.getShouye_lunbo1_picurl();
        ls_url2 = HhApplication.getInstance(context).sysArgument.getShouye_lunbo2_picurl();	 
        ls_url3 = HhApplication.getInstance(context).sysArgument.getShouye_lunbo3_picurl();
        ls_url4 = HhApplication.getInstance(context).sysArgument.getShouye_lunbo4_picurl();	 
        ls_url5 = HhApplication.getInstance(context).sysArgument.getShouye_lunbo5_picurl();
        
        //得到图片的类型
        ls_image_type1 =HhApplication.getInstance(context).sysArgument.getShouye_lunbo1_picurl_type();
        ls_image_type2 =HhApplication.getInstance(context).sysArgument.getShouye_lunbo2_picurl_type();
        ls_image_type3 =HhApplication.getInstance(context).sysArgument.getShouye_lunbo3_picurl_type();
        ls_image_type4 =HhApplication.getInstance(context).sysArgument.getShouye_lunbo4_picurl_type();
        ls_image_type5 =HhApplication.getInstance(context).sysArgument.getShouye_lunbo5_picurl_type();
        
        //得到对应的值
        ls_image_value1 =HhApplication.getInstance(context).sysArgument.getShouye_lunbo1_picurl_value();
        ls_image_value2 =HhApplication.getInstance(context).sysArgument.getShouye_lunbo2_picurl_value();
        ls_image_value3 =HhApplication.getInstance(context).sysArgument.getShouye_lunbo3_picurl_value();
        ls_image_value4 =HhApplication.getInstance(context).sysArgument.getShouye_lunbo4_picurl_value();
        ls_image_value5 =HhApplication.getInstance(context).sysArgument.getShouye_lunbo5_picurl_value();
        
        
        //http://test1.zb388.com/huahuajpg/selectshopflash.png
        
        //ls_url1 = "http://test1.zb388.com/huahuajpg/selectshopflash.png";
        //ls_url2 = "http://test1.zb388.com/huahuajpg/selectshopflash1.png";
        //ls_url3 = "http://test1.zb388.com/huahuajpg/selectshopflash2.png";
        //ls_url4 = "http://test1.zb388.com/huahuajpg/selectshopflash3.png";
        //ls_url5 = "http://test1.zb388.com/huahuajpg/selectshopflash4.png";
        
        if((ls_url1.equals(""))||(ls_url1==null)){
        	ls_url1 = "http://test1.zb388.com/huahuajpg/selectshopflash.png";
        }
        if((ls_url2.equals(""))||(ls_url2==null)){
        	ls_url2 = "http://test1.zb388.com/huahuajpg/selectshopflash.png";
        }
        if((ls_url3.equals(""))||(ls_url3==null)){
        	ls_url3 = "http://test1.zb388.com/huahuajpg/selectshopflash.png";
        }
        if((ls_url4.equals(""))||(ls_url4==null)){
        	ls_url4 = "http://test1.zb388.com/huahuajpg/selectshopflash.png";
        }
        if((ls_url5.equals(""))||(ls_url5==null)){
        	ls_url5 = "http://test1.zb388.com/huahuajpg/selectshopflash.png";
        }
        
        
         
        
        Log.i("imge  url ",ls_url1);
        
		initImageLoader(context);
		 
        initData();
        if(isAutoPlay){
            startPlay();
        }
        
    }
    /**
     * 开始轮播图切换
     */
    private void startPlay(){
        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        scheduledExecutorService.scheduleAtFixedRate(new SlideShowTask(), 1, 4, TimeUnit.SECONDS);
    }
    /**
     * 停止轮播图切换
     */
    private void stopPlay(){
        scheduledExecutorService.shutdown();
    }
    /**
     * 初始化相关Data
     */
    private void initData(){
        imageViewsList = new ArrayList<ImageView>();
        dotViewsList = new ArrayList<View>();

        // 一步任务获取图片
        new GetListTask().execute("");
    }
    /**
     * 初始化Views等UI
     */
    private void initUI(Context context){
    	
    	 
    	
    	if(imageUrls == null || imageUrls.length == 0)
    		return;
    	
        LayoutInflater.from(context).inflate(R.layout.layout_slideshow, this, true);
        
        LinearLayout dotLayout = (LinearLayout)findViewById(R.id.dotLayout);
        dotLayout.removeAllViews();
        
        // 热点个数与图片特殊相等
        for (int i = 0; i < imageUrls.length; i++) {
        	ImageView view =  new ImageView(context);
        	view.setTag(imageUrls[i]);
        	//if(i==0)//给一个默认图
        	view.setBackgroundResource(R.drawable.huahua_shouye_flash_default);
        	
        	
        	
        	
        	view.setScaleType(ScaleType.FIT_XY);
        	imageViewsList.add(view);
        	
        	ImageView dotView =  new ImageView(context);
        	LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
        	params.leftMargin = 5;
			params.rightMargin = 5;
			dotLayout.addView(dotView, params);
        	dotViewsList.add(dotView);
		}
        
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPager.setFocusable(true);
        
        viewPager.setAdapter(new MyPagerAdapter());
        viewPager.setOnPageChangeListener(new MyPageChangeListener());
        
         
    }
    
    /**
     * 填充ViewPager的页面适配器
     * 
     */
    private class MyPagerAdapter  extends PagerAdapter{

    	 
        @Override
        public void destroyItem(View container, int position, Object object) {
            // TODO Auto-generated method stub
            //((ViewPag.er)container).removeView((View)object);
            ((ViewPager)container).removeView(imageViewsList.get(position));
        }

        @Override
        public Object instantiateItem(View container, int position) {
        	ImageView imageView = imageViewsList.get(position);

			imageLoader.displayImage(imageView.getTag() + "", imageView);
        	
            ((ViewPager)container).addView(imageViewsList.get(position));
           
            //增加点击事件  
            
            //点击第一个图片
            if (position ==0) {
            	 View view = imageViewsList.get(position);
            	 view.setOnClickListener(new View.OnClickListener() {
            		 	@Override
            		 	public void onClick(View v){
            		 		ToastUtils.show(getContext(), "类型="+ls_image_type1+", 值="+ls_image_value1);
            		 		triggerclick(ls_image_type1,ls_image_value1);
        		 		 
            		 	}
            	 });
            }

          //点击第2个图片
            if (position ==1) {
            	 View view = imageViewsList.get(position);
            	 view.setOnClickListener(new View.OnClickListener() {
            		 	@Override
            		 	public void onClick(View v){
            		 		ToastUtils.show(getContext(), "类型="+ls_image_type2+", 值="+ls_image_value2);
            		 		triggerclick(ls_image_type2,ls_image_value2);
        		 		 
            		 	}
            	 });
            }
            
            //点击第3个图片
            if (position ==2) {
            	 View view = imageViewsList.get(position);
            	 view.setOnClickListener(new View.OnClickListener() {
            		 	@Override
            		 	public void onClick(View v){
            		 		ToastUtils.show(getContext(), "类型="+ls_image_type3+", 值="+ls_image_value3);
            		 		triggerclick(ls_image_type3,ls_image_value3);
        		 		 
            		 	}
            	 });
            }         
            //点击第4个图片
            if (position ==3) {
            	 View view = imageViewsList.get(position);
            	 view.setOnClickListener(new View.OnClickListener() {
            		 	@Override
            		 	public void onClick(View v){
            		 		ToastUtils.show(getContext(), "类型="+ls_image_type4+", 值="+ls_image_value4);
            		 		triggerclick(ls_image_type4,ls_image_value4);
        		 		 
            		 	}
            	 });
            }     
            
            //点击第5个图片
            if (position ==4) {
            	 View view = imageViewsList.get(position);
            	 view.setOnClickListener(new View.OnClickListener() {
            		 	@Override
            		 	public void onClick(View v){
            		 		ToastUtils.show(getContext(), "类型="+ls_image_type5+", 值="+ls_image_value5);
            		 		triggerclick(ls_image_type5,ls_image_value5);
        		 		 
            		 	}
            	 });
            }            
            return imageViewsList.get(position);
        }
        /* 轮播图片的类型  广告分为 1. 店铺 2.店铺产品 3.店铺活动  4.店铺服务  5.店铺技师*/
        public void triggerclick(String imagetype,String imagevalue){
        	/*
        	if(imagetype.equals("2")){  //产品 
		 			Intent intent=new Intent(getContext(), HhHuoDongViewActivity.class); 
					Bundle bundle=new Bundle();
					bundle.putString("id", imagevalue);
					bundle.putString("name", "");
					bundle.putString("saygoodqty", "0");
					bundle.putString("hasdianzhan", "N"); 
					intent.putExtras(bundle); 
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					HhApplication.getInstance(context).startActivity(intent);  //.startActivityForResult(intent, 2);//这里采用startActivityForResult来做跳转，此处的0为一个依据，可以写其他的值，但一定要>=0
					//getContext().getApplicationContext().overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
        	}
        	
        	if(imagetype.equals("5")){  //技师
	 			Intent intent=new Intent(getContext(), HhHuoDongViewActivity.class); 
				Bundle bundle=new Bundle(); 
				bundle.putString("masterid", imagevalue);
				bundle.putString("mastername", ""); 
				bundle.putString("masterlevel", ""); 
				bundle.putString("masterexperience",  ""); 		
				bundle.putString("masterworktimes",  ""); 
				
				bundle.putString("ihasdianzhan",  "N"); 
				bundle.putString("masterdianzhanqty",  ""); 
				intent.putExtras(bundle); 
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				HhApplication.getInstance(context).startActivity(intent);  //.startActivityForResult(intent, 2);//这里采用startActivityForResult来做跳转，此处的0为一个依据，可以写其他的值，但一定要>=0
				//getContext().getApplicationContext().overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
        	}
        	
        	if(imagetype.equals("3")){  //活动
	 			Intent intent=new Intent(getContext(), HhHuoDongViewActivity.class); 
				Bundle bundle=new Bundle(); 
				bundle.putString("id", imagevalue);
				bundle.putString("name", ""); 
				bundle.putString("saygoodqty", "");  
				bundle.putString("hasdianzhan",  "N");  
				intent.putExtras(bundle); 
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				HhApplication.getInstance(context).startActivity(intent);  //.startActivityForResult(intent, 2);//这里采用startActivityForResult来做跳转，此处的0为一个依据，可以写其他的值，但一定要>=0
				//getContext().getApplicationContext().overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
        	}
        	*/
        }
        
        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return imageViewsList.size();
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            // TODO Auto-generated method stub
            return arg0 == arg1;
        }
        @Override
        public void restoreState(Parcelable arg0, ClassLoader arg1) {
            // TODO Auto-generated method stub

        }

        @Override
        public Parcelable saveState() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public void startUpdate(View arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public void finishUpdate(View arg0) {
            // TODO Auto-generated method stub
            
        }
        
    }
    /**
     * ViewPager的监听器
     * 当ViewPager中页面的状态发生改变时调用
     * 
     */
    private class MyPageChangeListener implements OnPageChangeListener{

        boolean isAutoPlay = false;

        @Override
        public void onPageScrollStateChanged(int arg0) {
            // TODO Auto-generated method stub
            switch (arg0) {
            case 1:// 手势滑动，空闲中
                isAutoPlay = false;
                break;
            case 2:// 界面切换中
                isAutoPlay = true;
                break;
            case 0:// 滑动结束，即切换完毕或者加载完毕
                // 当前为最后一张，此时从右向左滑，则切换到第一张
                if (viewPager.getCurrentItem() == viewPager.getAdapter().getCount() - 1 && !isAutoPlay) {
                    viewPager.setCurrentItem(0);
                }
                // 当前为第一张，此时从左向右滑，则切换到最后一张
                else if (viewPager.getCurrentItem() == 0 && !isAutoPlay) {
                    viewPager.setCurrentItem(viewPager.getAdapter().getCount() - 1);
                }
                break;
        }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void onPageSelected(int pos) {
            // TODO Auto-generated method stub
            
            currentItem = pos;
            for(int i=0;i < dotViewsList.size();i++){
            	//需要2个小图片 一个是选中，一个是未选中
                if(i == pos){
                	//R.drawable.dot_black
                    ((View)dotViewsList.get(pos)).setBackgroundResource(R.drawable.enabletrue);
                }else {
                	//R.drawable.dot_white
                    ((View)dotViewsList.get(i)).setBackgroundResource(R.drawable.enablefalse);
                }
            }
        }
        
    }
    
    /**
     *执行轮播图切换任务
     *
     */
    private class SlideShowTask implements Runnable{

        @Override
        public void run() {
            // TODO Auto-generated method stub
            synchronized (viewPager) {
                currentItem = (currentItem+1)%imageViewsList.size();
                handler.obtainMessage().sendToTarget();
            }
        }
        
    }
    
    /**
     * 销毁ImageView资源，回收内存
     * 
     */
    private void destoryBitmaps() {

        for (int i = 0; i < IMAGE_COUNT; i++) {
            ImageView imageView = imageViewsList.get(i);
            Drawable drawable = imageView.getDrawable();
            if (drawable != null) {
                //解除drawable对view的引用
                drawable.setCallback(null);
            }
        }
    }
 

	/**
	 * 异步任务,获取数据
	 * 
	 */
	class GetListTask extends AsyncTask<String, Integer, Boolean> {

		@Override
		protected Boolean doInBackground(String... params) {
			try {
				// 这里一般调用服务端接口获取一组轮播图片，下面是从百度找的几个图片
				 
				imageUrls = new String[]{
						ls_url1,
						ls_url2,
						ls_url3,
						ls_url4,
						ls_url5
				}; 
				return true;
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {
		        initUI(context);
			}
		}
	}
	
	/**
	 * ImageLoader 图片组件初始化
	 * 
	 * @param context
	 */
	public static void initImageLoader(Context context) {
		// This configuration tuning is custom. You can tune every option, you
		// may tune some of them,
		// or you can create default configuration by
		// ImageLoaderConfiguration.createDefault(this);
		// method.
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context).threadPriority(Thread.NORM_PRIORITY - 2).denyCacheImageMultipleSizesInMemory().discCacheFileNameGenerator(new Md5FileNameGenerator()).tasksProcessingOrder(QueueProcessingType.LIFO).writeDebugLogs() // Remove
																																																																								// for
																																																																								// release
																																																																								// app
				.build();
		// Initialize ImageLoader with configuration.
		ImageLoader.getInstance().init(config);
	}
}
