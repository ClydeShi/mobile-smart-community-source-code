package com.zhyq.libs;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.zhyq.R;


public class CustomProgressDialog extends ProgressDialog {
	
	private AnimationDrawable mAnimation;
	private Context mContext;
	private ImageView mImageView;
	private TextView mLoadingTv;
	
	public CustomProgressDialog(Context context) {
		super(context);
		this.mContext = context;
		setCanceledOnTouchOutside(false);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initView();
		initData();
	}
	
	private void initData() {

		mImageView.setBackgroundResource(R.drawable.custom_loading_anim);
		// 通过ImageView对象拿到背景显示的AnimationDrawable
		mAnimation = (AnimationDrawable) mImageView.getBackground();
		// 为了防止在onCreate方法中只显示第一帧的解决方案之一
		mImageView.post(new Runnable() {
			@Override
			public void run() {
				Log.i("huahua","animation is running");
				mAnimation.start();

			}
		});
		mLoadingTv.setText("正在加载中");

	}
	
	public void setContent(String str) {
		mLoadingTv.setText(str);
	}
	
	private void initView() {
		setContentView(R.layout.custom_loading);
		mLoadingTv = (TextView) findViewById(R.id.loadingTv);
		mImageView = (ImageView) findViewById(R.id.loadingIv);
	}
	
	/*@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		// TODO Auto-generated method stub
		mAnimation.start(); 
		super.onWindowFocusChanged(hasFocus);
	}*/
}
