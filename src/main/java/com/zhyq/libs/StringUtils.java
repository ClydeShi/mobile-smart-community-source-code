package com.zhyq.libs;

 

public class StringUtils {
	public static boolean isNotBlank(String str) {
		return !(isBlank(str));
	}
	public static boolean isBlank(String str) {
		return str==null || str.trim().length()==0;
	}
}
