package com.zhyq.libs;

import java.text.DecimalFormat;

public class NumberFormat {
	private static DecimalFormat  f=new DecimalFormat("###,##0.00");
	public static String format(double d) {
		return f.format(d);
	}
}
