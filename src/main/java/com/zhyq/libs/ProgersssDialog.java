package com.zhyq.libs;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.zhyq.R;

public class ProgersssDialog extends Dialog {
    private Context context;
    private ImageView progress_img;
    private TextView progress_txt;

    public ProgersssDialog(Context context) {
        //super(context, R.style.custom_progress_dialog);//透明黑色加载框
        super(context, R.style.progress_dialog);//白色背景加载框
        this.context = context;
        //初始化页面
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.progress_dialog, null);
        progress_img = (ImageView) view.findViewById(R.id.progress_img);
        progress_txt = (TextView) view.findViewById(R.id.progress_txt);

        Animation anim = AnimationUtils.loadAnimation(context, R.anim.progress_dialog_loading);//添加动态循环
        progress_img.setAnimation(anim);
        //progress_txt.setText(R.string.progressbar_dialog_txt);

        //setCanceledOnTouchOutside(false);//用于透明黑色
        setContentView(view);//dialog填充视图

        show();
        //dismiss();
    }

	/*
    //屏蔽返回键
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			 return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	*/

}