package com.zhyq;
   
 
 
 
 
import java.util.ArrayList;
import java.util.List;

import com.zhyq.fragment.WgTaskListFragment;
import com.zhyq.libs.AndroidUtils;
 
 
import com.zhyq.widget.LoadingRelativeLayout;


import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
 
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;  
 
 
  
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
 
 
 
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;

import android.view.View.OnClickListener; 
import android.view.animation.TranslateAnimation;
 
 
 
import android.widget.Button;
 
import android.widget.ImageView;
import android.widget.TextView;
 
/*
 * WgTaskActivity调用WgTaskListFragment加载XListView WgTaskListFragment调用WgTaskListAdapter装载数据
 */

 
//首页同类型的店铺集合
public class WgTaskActivity2 extends FragmentActivity implements OnClickListener  {
	private LoadingRelativeLayout loading=null; 
  
	private String  shopType ="";    // 类别
	private String  taskType ="1";
	private int request_add_result = 1;  //增加请求码
	private int request_edit_result = 2;  //修改请求码
 
	private View  includeTitle ;
	private ImageView returnImage;
	private TextView  returnText;
	
	public  enum ScrollType{IDLE,TOUCH_SCROLL,FLING};
	private WgTaskListFragment  FragmentTask1=null;
	private WgTaskListFragment  FragmentTask2=null;
	private WgTaskListFragment  FragmentTask3=null;
	private WgTaskListFragment  FragmentTask4=null;
	
	private ViewPager mViewPager;
	private List<Fragment> fragmentList;
	private int currenttab = -1;
	
	//private com.unitesoft.libs.ScrollListenerHorizontalScrollView  hsv;  //控制 左右移动 内容
	private int screenWidth = 750;
	private Button bt1;
	private Button bt2;
	private Button bt3;
	private View imageviewOvertab;
	 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//店铺服务列表记录
		this.setContentView(R.layout.activity_wg_task_list2);
		
		taskType = this.getIntent().getStringExtra("taskType");
		
		includeTitle = this.findViewById(R.id.wg_util_head_task_list);
		 
		//tv_hh_head_title
		((TextView)includeTitle.findViewById(R.id.wg_head_title)).setText(taskType);

		returnImage = (ImageView)includeTitle.findViewById(R.id.wg_head_img_return);
		returnImage.setOnClickListener(this);
		
		//tv_hh_head_return 
		returnText = (TextView)includeTitle.findViewById(R.id.wg_head_text_return);
		returnText.setOnClickListener(this);
		returnText.setText("返回");
		 
		Button btreturn = (Button)includeTitle.findViewById(R.id.wg_head_btn_return);
		btreturn.setOnClickListener(this);
		
		//Button btadd = (Button)includeTitle.findViewById(R.id.wg_util_head_btn_add);
		//btadd.setOnClickListener(this);
		
		
		imageviewOvertab = this.findViewById(R.id.imgv_overtab);
		
		mViewPager = (ViewPager) findViewById(R.id.speciality_shoplist_viewpager);
        
		fragmentList = new ArrayList<Fragment>(); 
		
		/*
		FragmentShopList = new HhSpecilityShopListFrament();
		FragmentHuoDong  = new HhSpecilityHuodongFragment();
		FragmentDynamic  = new HhSpecilityDynamicNewsFragment();
		FragmentTask4   = new WgTaskListFragment();
		
		fragmentList.add(FragmentShopList);
		fragmentList.add(FragmentHuoDong);
		fragmentList.add(FragmentDynamic);
		fragmentList.add(FragmentTask4);
		*/
		
		FragmentTask1   = new WgTaskListFragment();
		FragmentTask2   = new WgTaskListFragment();
		FragmentTask3   = new WgTaskListFragment();
		
		fragmentList.add(FragmentTask1);
		fragmentList.add(FragmentTask2);
		fragmentList.add(FragmentTask3);
		
		
		//该函数是直接得到 周围店铺的控件句柄，用来直接控制 排序规则  hh_ll_productcard2column_price
		//下面的路径： HhSelectNearbyShopListFrament->HhShopListAdapte
		//            如果要排序 什么的, 用该句柄直接调用   HhShopListAdapte 当中定义的函数即可
		
		//设置类别
		/*
		FragmentShopList.setCurArguments("1");
		FragmentHuoDong.setCurArguments("2");
		FragmentDynamic.setCurArguments("3");
		FragmentTask4.setCurArguments("4");
		*/
		FragmentTask1.setCurArguments("1");
		FragmentTask2.setCurArguments("2");
		FragmentTask3.setCurArguments("3");
		//
		
		mViewPager.setAdapter(new MyFrageStatePagerAdapter(
				getSupportFragmentManager()));
		
		//下面是4个标签页的CLICK 事件
	    
	    bt1 = (Button)this.findViewById(R.id.bt_speciality_shoplist);
	    bt1.setOnClickListener(this);
	    
	    
	    bt2 = (Button)this.findViewById(R.id.bt_speciality_shophuodong);
	    bt2.setOnClickListener(this);   
	 
	    bt3 = (Button)this.findViewById(R.id.bt_speciality_shopdynamic);
	    bt3.setOnClickListener(this);	
		

	    //initControlClick(1,0);
	    
		//reSize();
 
	}
	  
	 
	@Override
	public View onCreateView(String name, @NonNull Context context,
			@NonNull AttributeSet attrs) {
		// TODO Auto-generated method stub
		
		
				
		return super.onCreateView(name, context, attrs);
	}


	public void reSize(){
		
		//标题
		AndroidUtils.setLinearLayoutSize(includeTitle,R.id.hh_browse_speciality_shop_news_list_head,"RelativeLayout","0",
				640,640,71,0,0,0,0);
		
		//一根线 hh_feedback_opinion_line
		View v1 = this.findViewById(R.id.hh_browse_speciality_shop_news_list_head_below_line);
		AndroidUtils.setLinearLayoutSize(v1,R.id.hh_browse_speciality_shop_news_list_head_below_line,"RelativeLayout","0",
				640,640,1,0,0,0,0);	 
		
		 
		  
		View iv1 = this.findViewById(R.id.imgv_overtab); 
	  
		//设置图片大小  
		AndroidUtils.setLinearLayoutSize(iv1,R.id.imgv_overtab,"RelativeLayout","0",
				640,160,0,0,0,0,0);
		 
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		//lGetgps.destroyAMapLocationListener();
	}
	 
 
	// int qiehuan 表示执行这个函数的时候，是否要切换到对应的fragment
	private void initControlClick(int curposition,int qiehuan){
		 
		 
	    bt2.setTextColor(Color.parseColor("#000000"));
	    bt3.setTextColor(Color.parseColor("#000000"));
	    
	    bt1.setTextSize(16);
	    bt2.setTextSize(16);
	    bt3.setTextSize(16);
	    
	     
	    int scrollX = 0 ;
		
		if(curposition==1){
			 
			bt1.setTextColor(Color.parseColor("#0e83ed"));
			scrollX = 0;
			if(qiehuan>0){
				//hsv.smoothScrollTo(scrollX, 0);
			}
			bt1.setTextSize(18);
		}
		if(curposition==2){
			 
			bt2.setTextColor(Color.parseColor("#0e83ed"));
			scrollX =screenWidth ;
			if(qiehuan>0){
				//hsv.smoothScrollTo(scrollX, 0);
			}
			bt2.setTextSize(18);
		}
		if(curposition==3){
			 
			bt3.setTextColor(Color.parseColor("#0e83ed"));
			scrollX =2*screenWidth ;
			if(qiehuan>0){
				//hsv.smoothScrollTo(scrollX, 0);
			}
			bt3.setTextSize(18);
		}
		
		
		
	}
	
	@Override
	public void onClick(View view) {
		
		if(R.id.wg_head_btn_return==view.getId()){
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return;
		}else if(R.id.wg_util_head_btn_add==view.getId()){//未使用
			/*
			Intent intent=new Intent(this, HhBrowseUserAddressActivity.class); 
			Bundle bundle=new Bundle();
			
			Long userid = HhApplication.getInstance(this).getHhCart().getUser().getId();//如果没有登录会报错
			 
			bundle.putString("userid", String.valueOf(userid));
			intent.putExtras(bundle);
			startActivity(intent);
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			return;
			*/
			
			Intent intent=new Intent(this, WgPlanAdd.class);
			Bundle bundle=new Bundle();
			
			Long userid = HhApplication.getInstance(this).getHhCart().getUser().getId();
			
			bundle.putString("userid", String.valueOf(userid));
			intent.putExtras(bundle);
			
			startActivityForResult(intent, request_add_result);//这里采用startActivityForResult来做跳转，此处的0为一个依据，可以写其他的值，但一定要>=0
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
		}else if(R.id.bt_speciality_shoplist ==view.getId()){
			changeView(0);
		}else if(R.id.bt_speciality_shophuodong ==view.getId()){ 
			changeView(1); 
		}else if(R.id.bt_speciality_shopdynamic ==view.getId()){ 
			changeView(2);
		}
	}
 
	/**
	 * 定义自己的ViewPager适配器。 也可以使用FragmentPagerAdapter。关于这两者之间的区别，可以自己去搜一下。
	 */
	class MyFrageStatePagerAdapter extends FragmentStatePagerAdapter {

		public MyFrageStatePagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			return fragmentList.get(position);
		}

		@Override
		public int getCount() {
			return fragmentList.size();
		}

		/**
		 * 每次更新完成ViewPager的内容后，调用该接口，此处复写主要是为了让导航按钮上层的覆盖层能够动态的移动
		 */
		@Override
		public void finishUpdate(ViewGroup container) {
			super.finishUpdate(container);// 这句话要放在最前面，否则会报错
			// 获取当前的视图是位于ViewGroup的第几个位置，用来更新对应的覆盖层所在的位置
			int currentItem = mViewPager.getCurrentItem();
			if (currentItem == currenttab) {
				return;
			}
			imageMove(mViewPager.getCurrentItem());
			currenttab = mViewPager.getCurrentItem();
		}

	}

	/**
	 * 移动覆盖层图片 初始化动画
	 * 
	 * @param moveToTab
	 *            目标Tab，也就是要移动到的导航选项按钮的位置 第一个导航按钮对应0，第二个对应1，以此类推
	 */
	private void imageMove(int moveToTab) {
		switch (moveToTab) {
		case 0:
			initAnimator(bt1);
			break;
		case 1:
			initAnimator(bt2);
			break;
		case 2:
			initAnimator(bt3);
			break;
		default:
			break;
		}
		
		
		anim.start();
		int startPosition = 0;
		int movetoPosition = 0;
		
		DisplayMetrics dm = this.getResources().getDisplayMetrics();
		int screenWidth = dm.widthPixels;
		
		//startPosition = (currenttab * (screenWidth / 3)) + (((screenWidth / 3) - dip2px(this,80)) / 2) - dip2px(this,10);
		//movetoPosition = moveToTab * (screenWidth / 3) + (((screenWidth / 3) - dip2px(this,80)) / 2) - dip2px(this,10);
		startPosition = (currenttab * (screenWidth / 3)) + (((screenWidth / 3) - dip2px(this,80)) / 2) - dip2px(this,18);
		movetoPosition = moveToTab * (screenWidth / 3) + (((screenWidth / 3) - dip2px(this,80)) / 2) - dip2px(this,18);
		// 平移动画
		TranslateAnimation translateAnimation = new TranslateAnimation(startPosition, movetoPosition, 0, 0);
		translateAnimation.setFillAfter(true);
		translateAnimation.setDuration(200);
		imageviewOvertab.startAnimation(translateAnimation);
		
	    bt1.setTextColor(Color.parseColor("#000000"));
	    bt2.setTextColor(Color.parseColor("#000000"));
	    bt3.setTextColor(Color.parseColor("#000000"));
	    
	    bt1.setTextSize(16);
	    bt2.setTextSize(16);
	    bt3.setTextSize(16);
	     
		if(moveToTab==0){
			
			bt1.setTextColor(Color.parseColor("#0e83ed")); 
			bt1.setTextSize(18);
		}
		if(moveToTab==1){
			 
			bt2.setTextColor(Color.parseColor("#0e83ed")); 
			bt2.setTextSize(18);
		}
		if(moveToTab==2){
			 
			bt3.setTextColor(Color.parseColor("#0e83ed")); 
			bt3.setTextSize(18);
		}
		
		
		
	}

	public static int dip2px(Context context,float dpValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dpValue * scale + 0.5f);
	}
	
	// 手动设置ViewPager要显示的视图
		private void changeView(int desTab) {
		   
			

		    bt1.setTextColor(Color.parseColor("#000000"));
		    bt2.setTextColor(Color.parseColor("#000000"));
		    bt3.setTextColor(Color.parseColor("#000000"));
		    
		    bt1.setTextSize(16);
		    bt2.setTextSize(16);
		    bt3.setTextSize(16);
		     
			if(desTab==0){
			 
				bt1.setTextColor(Color.parseColor("#0e83ed")); 
				bt1.setTextSize(18);
			}
			if(desTab==1){
				 
				bt2.setTextColor(Color.parseColor("#0e83ed")); 
				bt2.setTextSize(18);
			}
			if(desTab==2){
				 
				bt3.setTextColor(Color.parseColor("#0e83ed")); 
				bt3.setTextSize(18);
			}
			
			mViewPager.setCurrentItem(desTab, true);
		}

		private AnimatorSet anim;

		private void initAnimator(View view) {
			ObjectAnimator anim1 = ObjectAnimator.ofFloat(view, "scaleX", 1f, 0.3f);
			ObjectAnimator anim3 = ObjectAnimator.ofFloat(view, "scaleY", 1f, 0.3f);
			ObjectAnimator anim2 = ObjectAnimator.ofFloat(view, "scaleX", 0.3f, 1f);
			ObjectAnimator anim4 = ObjectAnimator.ofFloat(view, "scaleY", 0.3f, 1f);
			ObjectAnimator anim11 = ObjectAnimator.ofFloat(view, "scaleX", 1f, 0.8f);
			ObjectAnimator anim33 = ObjectAnimator.ofFloat(view, "scaleY", 1f, 0.8f);
			ObjectAnimator anim22 = ObjectAnimator.ofFloat(view, "scaleX", 0.8f, 1f);
			ObjectAnimator anim44 = ObjectAnimator.ofFloat(view, "scaleY", 0.8f, 1f);
			anim = new AnimatorSet();
			//anim.play(anim1).with(anim3);
			//anim.play(anim2).with(anim4);
			//anim.play(anim2).after(anim1);
			//anim.play(anim11).with(anim33);
			//anim.play(anim22).with(anim44);
			//anim.play(anim22).after(anim11);
			//anim.play(anim11).after(anim2);
			anim.setDuration(10);
		}
		
		/*
		 private void initAnimator(View view) {
			ObjectAnimator anim1 = ObjectAnimator.ofFloat(view, "scaleX", 1f, 0.3f);
			ObjectAnimator anim3 = ObjectAnimator.ofFloat(view, "scaleY", 1f, 0.3f);
			ObjectAnimator anim2 = ObjectAnimator.ofFloat(view, "scaleX", 0.3f, 1f);
			ObjectAnimator anim4 = ObjectAnimator.ofFloat(view, "scaleY", 0.3f, 1f);
			ObjectAnimator anim11 = ObjectAnimator
					.ofFloat(view, "scaleX", 1f, 0.8f);
			ObjectAnimator anim33 = ObjectAnimator
					.ofFloat(view, "scaleY", 1f, 0.8f);
			ObjectAnimator anim22 = ObjectAnimator
					.ofFloat(view, "scaleX", 0.8f, 1f);
			ObjectAnimator anim44 = ObjectAnimator
					.ofFloat(view, "scaleY", 0.8f, 1f);
			anim = new AnimatorSet();
			anim.play(anim1).with(anim3);
			anim.play(anim2).with(anim4);
			anim.play(anim2).after(anim1);
			anim.play(anim11).with(anim33);
			anim.play(anim22).with(anim44);
			anim.play(anim22).after(anim11);
			anim.play(anim11).after(anim2);
			anim.setDuration(100);
		}
		*/
		 
}
	 
